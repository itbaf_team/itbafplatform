/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.tiaxa.soap;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author javier
 */
public class SoapAdapter extends XmlAdapter<String, Object> {

    @Override
    public String unmarshal(String v) throws Exception {
        if (v == null) {
            return null;
        }
        return v.trim();
    }
/*
    @Override
    public String marshal(String v) throws Exception {
        if (v == null) {
            return null;
        }
        return v.trim();
    }*/

    @Override
    public String marshal(Object v) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
