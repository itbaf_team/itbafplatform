package com.itbaf.platform.paymenthub.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QOperator is a Querydsl query type for Operator
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOperator extends EntityPathBase<Operator> {

    private static final long serialVersionUID = 386280652L;

    public static final QOperator operator = new QOperator("operator");

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public QOperator(String variable) {
        super(Operator.class, forVariable(variable));
    }

    public QOperator(Path<? extends Operator> path) {
        super(path.getType(), path.getMetadata());
    }

    public QOperator(PathMetadata metadata) {
        super(Operator.class, metadata);
    }

}

