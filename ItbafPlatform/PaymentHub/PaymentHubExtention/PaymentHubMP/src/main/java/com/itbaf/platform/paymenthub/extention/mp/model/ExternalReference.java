/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class ExternalReference {

    public String externalId;
    public Long sopId;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("externalId", externalId)
                .add("sopId", sopId)
                .omitNullValues().toString();
    }
}
