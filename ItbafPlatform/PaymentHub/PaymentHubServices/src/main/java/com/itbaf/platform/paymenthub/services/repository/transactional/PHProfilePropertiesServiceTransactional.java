/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.paymenthub.model.ph.PHProfileProperties;
import com.itbaf.platform.paymenthub.repository.jpa.model.ph.JPAPHProfilePropertiesRepository;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class PHProfilePropertiesServiceTransactional implements PHProfilePropertiesService {

    private final JPAPHProfilePropertiesRepository profilePropertiesRepository;
    private final String profile;

    @Inject
    public PHProfilePropertiesServiceTransactional(@Named("guice.profile") String profile,
            final JPAPHProfilePropertiesRepository profilePropertiesRepository) {
        this.profile = profile;
        this.profilePropertiesRepository = Validate.notNull(profilePropertiesRepository, "A JPAPHProfilePropertiesRepository class must be provided");
    }

    @Override
    public String getGeneralProperty(String key) {
        return getValueByProfileProviderKey(profile, 1L, key);
    }

    @Override
    public String getCommonProperty(String key) {
        return getValueByProfileProviderKey("all", 1L, key);
    }

    @Override
    public String getCommonProperty(Long providerId, String key) {
        return getValueByProfileProviderKey("all", providerId, key);
    }

    @Override
    public String getValueByProviderKey(String provider, String key) {
        return getValueByProfileProviderKey(profile, provider, key);
    }

    @Override
    public String getValueByProviderKey(Long providerId, String key) {
        return getValueByProfileProviderKey(profile, providerId, key);
    }

    private String getValueByProfileProviderKey(String profile, Long providerId, String key) {
        Object aux = MainCache.memory5Hours().getIfPresent(profile + "_" + providerId + "_" + key);
        String value = null;
        if (aux == null) {
            PHProfileProperties prop = profilePropertiesRepository.getValueByProfileProviderKey(profile, providerId, key);
            if (prop != null) {
                value = prop.getValue();
                MainCache.memory5Hours().put(profile + "_" + providerId + "_" + key, value);
            }
        } else {
            value = (String) aux;
        }
        return value;
    }

    private String getValueByProfileProviderKey(String profile, String provider, String key) {
        Object aux = MainCache.memory5Hours().getIfPresent(profile + "_" + provider + "_" + key);
        String value = null;
        if (aux == null) {
            PHProfileProperties prop = profilePropertiesRepository.getValueByProfileProviderKey(profile, provider, key);
            if (prop != null) {
                value = prop.getValue();
                MainCache.memory5Hours().put(profile + "_" + provider + "_" + key, value);
            }
        } else {
            value = (String) aux;
        }
        return value;
    }
}
