/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class MainLocalCache {

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final Cache<Object, Object> memory = CacheBuilder.newBuilder()
            .expireAfterWrite(10, TimeUnit.MINUTES) //Tiempo de vida
            .build();

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final Cache<Object, Object> memory1Hour = CacheBuilder.newBuilder()
            .expireAfterWrite(1, TimeUnit.HOURS) //Tiempo de vida
            .build();

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final Cache<Object, Object> memory2Hours = CacheBuilder.newBuilder()
            .expireAfterWrite(2, TimeUnit.HOURS) //Tiempo de vida
            .build();

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final Cache<Object, Object> memory5Hours = CacheBuilder.newBuilder()
            .expireAfterWrite(5, TimeUnit.HOURS) //Tiempo de vida
            .build();

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final Cache<Object, Object> memory12Hours = CacheBuilder.newBuilder()
            .expireAfterWrite(12, TimeUnit.HOURS) //Tiempo de vida
            .build();

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final Cache<Object, Object> memory24Hours = CacheBuilder.newBuilder()
            .expireAfterWrite(24, TimeUnit.HOURS) //Tiempo de vida
            .build();

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final Cache<Object, Object> memory48Hours = CacheBuilder.newBuilder()
            .expireAfterWrite(48, TimeUnit.HOURS) //Tiempo de vida
            .build();

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final Cache<Object, Object> memory96Hours = CacheBuilder.newBuilder()
            .expireAfterWrite(96, TimeUnit.HOURS) //Tiempo de vida
            .build();

    public static Cache<Object, Object> memory() {
        return memory;
    }

    public static Cache<Object, Object> memory1Hour() {
        return memory1Hour;
    }

    public static Cache<Object, Object> memory2Hours() {
        return memory2Hours;
    }

    public static Cache<Object, Object> memory5Hours() {
        return memory5Hours;
    }

    public static Cache<Object, Object> memory12Hours() {
        return memory12Hours;
    }

    public static Cache<Object, Object> memory24Hours() {
        return memory24Hours;
    }

    public static Cache<Object, Object> memory48Hours() {
        return memory48Hours;
    }

    public static Cache<Object, Object> memory96Hours() {
        return memory96Hours;
    }

    public static synchronized void clearCache() {
        memory.invalidateAll();
        memory1Hour.invalidateAll();
        memory2Hours.invalidateAll();
        memory5Hours.invalidateAll();
        memory12Hours.invalidateAll();
        memory24Hours.invalidateAll();
        memory48Hours.invalidateAll();
        memory96Hours.invalidateAll();
    }

}
