package com.itbaf.platform.pgp.services.rest;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/resources")
@lombok.extern.log4j.Log4j2
public class FileRestService extends ServiceProcessorHandler {

    private final String maxCacheControl;

    @Inject
    public FileRestService(@Named("http.response.header.cache.control") String maxCacheControl) {
        this.maxCacheControl = "max-age=" + maxCacheControl;
    }

    @GET
    @Path("/web/{path}/{file}")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response getFile(@PathParam("path") final String path,
            @PathParam("file") final String fileName) {
        log.info("getFile. path: [" + path + "] - fileName: [" + fileName + "]");

        try {
            InputStream is = getClass().getResourceAsStream("/webapp/" + path + "/" + fileName);
            return Response.ok((Object) is)
                    .type(getType(fileName))
                    .header("Cache-Control", maxCacheControl)
                    .build();
        } catch (Exception ex) {
            log.error("Error al obtener el archivo. path: [" + path + "] - fileName: [" + fileName + "]. " + ex, ex);
        }

        return Response.status(Response.Status.FORBIDDEN).build();
    }

    @GET
    @Path("/web/i18n/{i18n}/{path}/{file}")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response i18nFile(@PathParam("i18n") final String i18n,
            @PathParam("path") final String path,
            @PathParam("file") final String fileName) {
        log.info("getFile. i18n: [" + i18n + "] - path: [" + path + "] - fileName: [" + fileName + "]");

        try {
            Object html = MainCache.memory48Hours().getIfPresent("File_" + i18n + "_" + path + "_" + fileName);
            if (html == null) {
                try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/webapp/" + path + "/" + fileName), "UTF-8"))) {
                    html = "";
                    String aux;
                    while ((aux = br.readLine()) != null) {
                        html = html + aux;
                    }
                    html = i18n(i18n, null, (String) html);
                    MainCache.memory48Hours().put("File_" + i18n + "_" + path + "_" + fileName, html);
                } catch (Exception ex) {
                }
            }

            return Response.ok(html)
                    .type(getType(fileName))
                    .header("Cache-Control", maxCacheControl)
                    .build();
        } catch (Exception ex) {
            log.error("Error al obtener el archivo. i18n: [" + i18n + "] - path: [" + path + "] - fileName: [" + fileName + "]. " + ex, ex);
        }

        return Response.status(Response.Status.FORBIDDEN).build();
    }

    @GET
    @Path("/download/{path}/{file}")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response download(@PathParam("path") final String path,
            @PathParam("file") final String fileName) {
        log.info("getCSS. path: [" + path + "] - fileName: [" + fileName + "]");

        try {
            InputStream is = getClass().getResourceAsStream("/webapp/" + path + "/" + fileName);
            return Response.ok((Object) is)
                    .type(getType(fileName))
                    .header("Content-Disposition", "attachment; filename=" + fileName)
                    .build();
        } catch (Exception ex) {
            log.error("Error al obtener el archivo. path: [" + path + "] - fileName: [" + fileName + "]. " + ex, ex);
        }

        return Response.status(Response.Status.FORBIDDEN).build();
    }

    private String getType(String fileName) {
        String type = "text/plain";
        if (fileName.endsWith(".png") || fileName.endsWith(".jpg") || fileName.endsWith(".jpeg") || fileName.endsWith(".gif")) {
            String aux[] = fileName.split("\\.");
            type = "image/" + aux[aux.length - 1];
        } else if (fileName.endsWith(".svg")) {
            type = "image/svg+xml";
        } else if (fileName.endsWith(".pdf")) {
            type = "application/pdf";
        } else if (fileName.endsWith(".css") || fileName.endsWith(".html")) {
            String aux[] = fileName.split("\\.");
            type = "text/" + aux[aux.length - 1];
        } else if (fileName.endsWith(".js")) {
            type = "application/javascript";
        }
        return type;
    }

}
