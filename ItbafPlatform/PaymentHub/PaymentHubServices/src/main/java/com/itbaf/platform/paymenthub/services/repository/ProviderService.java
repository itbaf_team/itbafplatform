/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.services.service.TestService;
import com.itbaf.platform.paymenthub.model.Provider;
import java.util.List;

/**
 *
 * @author javier
 */
public interface ProviderService extends TestService<Provider> {

    @Override
    public List<Provider> getAll();

    public Provider findById(Long id) throws Exception;
    
    public Provider findByName(String name);
    
    public void saveOrUpdate(Provider provider) throws Exception;

    public Provider findOrCreateByName(String name) throws Exception;
    
}
