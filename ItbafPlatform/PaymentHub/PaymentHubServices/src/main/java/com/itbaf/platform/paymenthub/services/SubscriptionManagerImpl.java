/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import com.itbaf.platform.commons.cache.CacheMessage;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.messaging.queue.wallet.CurrencyRequest;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.paymenthub.commons.resources.FacadeCache;
import com.itbaf.platform.paymenthub.commons.sync.NormalizerService;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.repository.CountryService;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.ProviderService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class SubscriptionManagerImpl extends NormalizerService implements SubscriptionManager {

    private final ObjectMapper mapper;
    private final SopService sopService;
    private final FacadeCache facadeCache;
    private final CountryService countryService;
    private final ProviderService providerService;
    private final RabbitMQProducer rabbitMQProducer;
    private final InstrumentedObject instrumentedObject;
    private final SubscriptionService subscriptionService;
    private final PHProfilePropertiesService profilePropertiesService;
    private final SubscriptionRegistryService subscriptionRegistryService;
    private static final PaymentHubMessage.NotificationType notificationType = PaymentHubMessage.NotificationType.SUBSCRIPTION;

    @Inject
    public SubscriptionManagerImpl(final ObjectMapper mapper,
            final SopService sopService,
            final FacadeCache facadeCache,
            final CountryService countryService,
            final ProviderService providerService,
            final RabbitMQProducer rabbitMQProducer,
            final InstrumentedObject instrumentedObject,
            final SubscriptionService subscriptionService,
            final PHProfilePropertiesService profilePropertiesService,
            final SubscriptionRegistryService subscriptionRegistryService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.sopService = Validate.notNull(sopService, "A SopService class must be provided");
        this.facadeCache = Validate.notNull(facadeCache, "A FacadeCache class must be provided");
        this.countryService = Validate.notNull(countryService, "A CountryService class must be provided");
        this.providerService = Validate.notNull(providerService, "A ProviderService class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.subscriptionService = Validate.notNull(subscriptionService, "A SubscriptionService class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
        this.subscriptionRegistryService = Validate.notNull(subscriptionRegistryService, "A SubscriptionRegistryService class must be provided");
    }

    @Override
    public Provider getProviderByName(String name) {
        return providerService.findByName(name);
    }

    @Override
    public Subscription subscriptionProcess(Subscription s) {
        return subscriptionProcess(s, 0);
    }

    public Subscription subscriptionProcess(Subscription s, int cont) {
        RLock lock = instrumentedObject.lockObject(s.getUserAccount() + "_SP_" + s.getSop().getId(), 60);

        try {
            if (s.getSubscriptionRegistry() == null && s.getId() != null) {
                log.error("Se pretende guardar una suscripcion sin SubscriptionRegistry. [" + s + "]");
                if (s.getId() == null) {
                    return s;
                }
                SubscriptionRegistry sr = subscriptionRegistryService.getLastRegisterBySubscription(s.getId(), false);
                if (sr == null) {
                    return s;
                } else {
                    // log.info("Subscription Registry encontrada: [" + sr + "]");
                    s.setSubscriptionRegistry(sr);
                    sr.setSubscription(s);
                }
            }

            try {
                if (s.getSubscriptionRegistry().getSop() == null) {
                    s.getSubscriptionRegistry().setSop(s.getSop());
                }
                if (s.getSubscriptionRegistry().getUserAccount() == null) {
                    s.getSubscriptionRegistry().setUserAccount(s.getUserAccount());
                }
                if (s.getExternalUserAccount() != null) {
                    Subscription aux = null;

                    List<Subscription> auxL = subscriptionService.getByExternalUserAccount(s.getExternalUserAccount(), s.getSop());
                    if (auxL != null && !auxL.isEmpty()) {
                        // log.info("Suscripciones para el ExternalUserAccount: [" + s.getExternalUserAccount() + "] - SOP: [" + s.getSop().getId() + "] - [" + Arrays.toString(auxL.toArray()) + "]");
                        aux = auxL.get(0);
                        for (Subscription ss : auxL) {
                            if (!ss.getId().equals(s.getId())) {
                                aux = ss;
                                break;
                            }
                        }
                    }
                    /*else {
                        log.info("No se encontraron suscripciones para el ExternalUserAccount: [" + s.getExternalUserAccount() + "] - SOP: [" + s.getSop().getId() + "]");
                    }*/

                    if (aux != null) {
                        if (s.getId() != null && !s.getId().equals(aux.getId())) {
                            log.info("Ya existe en la DB la suscripcion para el ExternalUserAccount. Subscription: s: [" + s + "] - aux: [" + aux + "]");

                            try {
                                Long.parseLong(aux.getUserAccount());
                                s.setUserAccount(aux.getUserAccount());
                                s.getSubscriptionRegistry().setUserAccount(aux.getUserAccount());
                            } catch (Exception ex) {
                            }

                            if (s.getId() > aux.getId()) {
                                Long auxl = s.getId();
                                s.setId(aux.getId());
                                aux.setId(auxl);
                                s.getSubscriptionRegistry().setSubscription(s);
                            }

                            try {
                                List<SubscriptionRegistry> srList = subscriptionRegistryService.getRegistersBySubscription(aux.getId());
                                log.info("Registros encontrados: [" + srList.size() + "]");
                                for (SubscriptionRegistry sr : srList) {
                                    sr.setSubscription(s);
                                    subscriptionRegistryService.saveOrUpdate(sr);
                                }
                                aux.setSubscriptionRegistry(null);
                                this.subscriptionService.delete(aux.getId());
                            } catch (Exception ex) {
                                log.error("Error al procesar lista de subscription_registry para aux: [" + aux + "] - s: [" + s + "]. " + ex);
                            }
                        } else {
                            s.setId(aux.getId());
                        }
                    }
                }

                if (s.getId() == null) {
                    try {
                        Subscription ss = subscriptionService.findByUserAccount(s.getUserAccount(), s.getSop());
                        if (ss != null) {
                            log.warn("Suscripcion duplicada en DB. sdb: [" + ss + "]. sd: [" + s + "]");
                            return ss;
                        }
                    } catch (Exception ex) {
                    }
                }

                if (s.getUserAccount() != null && s.getSubscriptionRegistry().getExternalId() != null) {
                    Subscription aux = getSubscriptionByExternalId(s.getSubscriptionRegistry().getExternalId(), s.getSop());
                    if (aux != null) {
                        if (s.getId() != null && !s.getId().equals(aux.getId())) {
                            log.info("Ya existe en la DB la suscripcion para el ExternalId. Subscription: [" + aux + "]");
                            aux.getSubscriptionRegistry().setSubscription(null);
                            aux.getSubscriptionRegistry().setUnsubscriptionDate(new Date());
                            aux.getSubscriptionRegistry().setOriginUnsubscription("Duplicate_subscription");
                            this.subscriptionRegistryService.saveOrUpdate(aux.getSubscriptionRegistry());
                            this.subscriptionService.delete(aux.getId());
                        } else {
                            s.setId(aux.getId());
                            s.getSubscriptionRegistry().setId(aux.getSubscriptionRegistry().getId());
                            if (aux.getSubscriptionRegistry().getOriginSubscription() != null) {
                                s.getSubscriptionRegistry().setOriginSubscription(aux.getSubscriptionRegistry().getOriginSubscription());
                            }
                            if (aux.getSubscriptionRegistry().getAdnetworkTracking() != null && s.getSubscriptionRegistry().getAdnetworkTracking() == null) {
                                s.getSubscriptionRegistry().setAdnetworkTracking(aux.getSubscriptionRegistry().getAdnetworkTracking());
                            }
                            if (aux.getSubscriptionRegistry().getExtra() != null && s.getSubscriptionRegistry().getExtra() == null) {
                                s.getSubscriptionRegistry().setExtra(aux.getSubscriptionRegistry().getExtra());
                            }
                        }
                    } else {
                        try {
                            SubscriptionRegistry sr = subscriptionRegistryService.findByExternalId(s.getSubscriptionRegistry().getExternalId());
                            if (sr != null && !sr.getId().equals(s.getSubscriptionRegistry().getId()) && s.getId() != null && s.getId().equals(sr.getSubscription().getId())) {
                                log.error("Ya existe un SubscriptionRegistry con ExternalID: [" + s.getSubscriptionRegistry().getExternalId() + "]. SR: [" + sr + "] - S: [" + s + "]");
                                processExternalId(s, sr);
                            }
                        } catch (Exception ex) {
                            log.error("Error al actualizar SubscriptionRegistry. S: [" + s + "]. " + ex, ex);
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("Error al obtener suscripcion por externalId. " + ex, ex);
            }

            try {
                Date d = new Date();
                switch (s.getStatus()) {
                    case RETENTION:
                    case TRIAL:
                    case ACTIVE:
                    case PENDING:
                        FacadeRequest facadeRequest = facadeCache.getFacadeRequest(s.getSubscriptionRegistry().getExternalId(),
                                s.getUserAccount(), s.getExternalUserAccount(), s.getSop().getId());
                        if (facadeRequest != null) {
                            if (s.getSubscriptionRegistry().getAdnetworkTracking() == null && facadeRequest.adTracking != null) {
                                s.getSubscriptionRegistry().setAdnetworkTracking(facadeRequest.adTracking);
                            } else {
                                try {
                                    if (facadeRequest.adTracking != null && s.getSubscriptionRegistry().getAdnetworkTracking() != null) {
                                        HashMap map = (HashMap) s.getSubscriptionRegistry().getAdnetworkTracking();
                                        if (map.isEmpty()) {
                                            s.getSubscriptionRegistry().setAdnetworkTracking(facadeRequest.adTracking);
                                        }
                                    }
                                } catch (Exception ex) {
                                }
                            }

                            if (s.getSubscriptionRegistry().getExternalId() == null) {
                                s.getSubscriptionRegistry().setExternalId(facadeRequest.externalId);
                            }
                            if (s.getSubscriptionRegistry().getChannelIn() == null) {
                                s.getSubscriptionRegistry().setChannelIn(facadeRequest.channel);
                            }
                        }
                        s.getSubscriptionRegistry().setChannelOut(null);
                        s.getSubscriptionRegistry().setOriginUnsubscription(null);
                        s.setUnsubscriptionDate(null);
                        s.getSubscriptionRegistry().setUnsubscriptionDate(null);
                        break;
                    case CANCELLED:
                    case REMOVED:
                    case BLACKLIST:
                    case LOCKED:
                    case PORTING:
                        if (s.getUnsubscriptionDate() == null) {
                            s.setUnsubscriptionDate(d);
                        }
                        if (s.getSubscriptionRegistry().getUnsubscriptionDate() == null) {
                            s.getSubscriptionRegistry().setUnsubscriptionDate(d);
                        }
                        break;
                }
            } catch (Exception ex) {
            }

            if (s.getSubscriptionDate() == null) {
                s.setSubscriptionDate(new Date());
            }
            if (s.getSubscriptionRegistry().getSubscriptionDate() == null) {
                s.getSubscriptionRegistry().setSubscriptionDate(s.getSubscriptionDate());
            }

            try {
                SubscriptionRegistry sra = null;
                if (s.getSubscriptionRegistry().getId() == null) {
                    sra = s.getSubscriptionRegistry();
                    s.setSubscriptionRegistry(null);
                }

                subscriptionService.saveOrUpdate(s);
                if (sra != null) {
                    s.setSubscriptionRegistry(sra);
                    sra.setSubscription(s);
                }

                try {
                    s.getSubscriptionRegistry().setSubscription(s);
                    subscriptionRegistryService.saveOrUpdate(s.getSubscriptionRegistry());
                } catch (Exception ex) {
                    log.error("Error al guardar SubscriptionRegistry [" + s.getSubscriptionRegistry() + "]. " + ex, ex);
                }

                subscriptionService.saveOrUpdate(s);
                log.info("Suscripcion guardada: [" + s + "]");
                // subscriptionService.saveOrUpdate(s);
                try {
                    String jsonResponse = mapper.writeValueAsString(s);
                    if (jsonResponse != null) {
                        rabbitMQProducer.messageSender(profilePropertiesService.getCommonProperty("rabbitmq.paymenthub.adn.subscriptions"), jsonResponse);
                    }
                } catch (Exception ex) {
                    log.error("Error al enviar mensaje de suscripcion. " + ex, ex);
                }
                try {
                    if (Subscription.Status.ACTIVE.equals(s.getStatus())) {
                        //Envio a PG.Server las altas confirmadas.
                        if (Subscription.Status.ACTIVE.equals(s.getStatus())) {
                            Tariff t = s.getSop().getMainTariff();
                            CurrencyRequest cr = new CurrencyRequest();
                            cr.countryId = s.getSop().getProvider().getCountry().getId();
                            cr.sopId = s.getSop().getId();
                            if (t == null) {
                                log.error("La tarifa no se encuentra definida para sop con identificador "  + s.getSop().getId());
                                return s;
                            } else {
                                cr.amount = t.getFullAmount();
                                cr.currency = t.getCurrency();
                                cr.tariffId = t.getId();
                                cr.localAmount = t.getLocalAmount();
                                cr.localCurrency = t.getLocalCurrency();
                            }
                            // cr.description = "Suscripcion: " + s.getSop().getServiceMatcher().getGenericName();
                            cr.transactionId = "PH_S_" + s.getId();
                            cr.userAccount = s.getUserAccount();
                            cr.tracking = s.getSubscriptionRegistry().getAdnetworkTracking();
                            cr.date = s.getSubscriptionDate();
                            String jsonRequest = mapper.writeValueAsString(cr);
                            rabbitMQProducer.messageSender(profilePropertiesService.getCommonProperty("rabbitmq.pg.wallet.funds"), jsonRequest);
                        }
                    } else if (Subscription.Status.CANCELLED.equals(s.getStatus()) || Subscription.Status.REMOVED.equals(s.getStatus())) {
                        Tariff t = s.getSop().getMainTariff();
                        CurrencyRequest cr = new CurrencyRequest();
                        cr.countryId = s.getSop().getProvider().getCountry().getId();
                        cr.sopId = s.getSop().getId();
                        cr.amount = BigDecimal.ZERO;
                        if (t == null) {
                            log.error("La tarifa no se encuentra definida para sop con identificador "  + s.getSop().getId());
                            return s;
                        } else {
                            cr.currency = t.getCurrency();
                            cr.tariffId = t.getId();
                            cr.localAmount = t.getLocalAmount();
                            cr.localCurrency = t.getLocalCurrency();
                        }
                        // cr.description = "Suscripcion: " + s.getSop().getServiceMatcher().getGenericName();
                        cr.transactionId = "PH_UNS_" + s.getSubscriptionRegistry().getId();
                        cr.userAccount = s.getUserAccount();
                        cr.tracking = s.getSubscriptionRegistry().getAdnetworkTracking();
                        cr.date = s.getSubscriptionDate();
                        String jsonRequest = mapper.writeValueAsString(cr);
                        rabbitMQProducer.messageSender(profilePropertiesService.getCommonProperty("rabbitmq.pg.wallet.funds"), jsonRequest);
                    }
                } catch (Exception ex) {
                    log.error("Error al enviar mensaje de suscripcion a PG.Server. " + ex, ex);
                }

                try {
                    if (s.getSubscriptionRegistry() != null && s.getSubscriptionRegistry().getExternalId() != null) {
                        List<SubscriptionRegistry> srl = subscriptionRegistryService.getRegistersBySubscription(s.getId());
                        SubscriptionRegistry au = null;
                        for (SubscriptionRegistry sr : srl) {
                            if (sr.getId() < s.getSubscriptionRegistry().getId() && sr.getExternalId() != null
                                    && sr.getExternalId().equals(s.getSubscriptionRegistry().getExternalId())
                                    && (au == null || sr.getId() < au.getId())) {
                                au = sr;
                            }
                        }
                        if (au != null) {
                            processExternalId(s, au);
                            subscriptionService.saveOrUpdate(s);
                            subscriptionRegistryService.saveOrUpdate(s.getSubscriptionRegistry());
                        }

                        for (SubscriptionRegistry sr : srl) {
                            if (sr.getId() > s.getSubscriptionRegistry().getId() && sr.getExternalId() != null
                                    && sr.getExternalId().equals(s.getSubscriptionRegistry().getExternalId())) {
                                if (sr.getUnsubscriptionDate() == null) {
                                    sr.setUnsubscriptionDate(new Date());
                                    sr.setOriginUnsubscription("Duplicate_ExternalId");
                                    log.info("Actualizando SubscriptionRegistry: [" + mapper.writeValueAsString(sr) + "]");
                                    subscriptionRegistryService.saveOrUpdate(sr);
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                    log.error("Error al borrar SubscriptionRegistry. S: [" + s + "]. " + ex, ex);
                }

            } catch (Exception ex) {
                log.error("Error al guardar/actualizar suscripcion [" + s + "]. " + ex, ex);
            }

            if (s.getSubscriptionRegistry() == null) {
                log.fatal("No se guardo el SubscriptionRegistry. [" + s + "]");
                SubscriptionRegistry sr = subscriptionRegistryService.getLastRegisterBySubscription(s.getId(), false);
                if (sr == null) {
                    log.error("No se encontro un SubscriptionRegistry. [" + s + "]");
                } else {
                    // log.info("Subscription Registry encontrada: [" + sr + "]");
                    s.setSubscriptionRegistry(sr);
                    sr.setSubscription(s);
                    try {
                        subscriptionService.saveOrUpdate(s);
                    } catch (Exception ex) {
                        log.error("Error al guardar suscripcion. [" + s + "]. " + ex, ex);
                    }
                }
            } else {
                try {
                    Subscription aux = subscriptionService.findById(s.getId());
                    if (aux.getSubscriptionRegistry() == null) {
                        log.warn("Suscripcion guardada sin SubscriptionRegistry. AUX: [" + aux + "] - S: [" + s + "]");
                        try {
                            subscriptionService.saveOrUpdate(s);
                        } catch (Exception ex) {
                            log.error("Error al guardar suscripcion. [" + s + "]. " + ex, ex);
                        }
                    }
                } catch (Exception ex) {
                    log.error("Error al validar suscripcion. [" + s + "]. " + ex, ex);
                }
            }
        } finally {
            instrumentedObject.unLockObject(lock);
        }
        return s;
    }

    private void processExternalId(Subscription s, SubscriptionRegistry sr) {
        s.getSubscriptionRegistry().setId(sr.getId());
        s.setSubscriptionDate(sr.getSubscriptionDate());
        s.getSubscriptionRegistry().setSubscriptionDate(s.getSubscriptionDate());
        if (sr.getChannelIn() != null) {
            s.getSubscriptionRegistry().setChannelIn(sr.getChannelIn());
        }
        if (sr.getAdnetworkTracking() != null) {
            s.getSubscriptionRegistry().setAdnetworkTracking(sr.getAdnetworkTracking());
        }
        if (sr.getOriginSubscription() != null) {
            s.getSubscriptionRegistry().setOriginSubscription(sr.getOriginSubscription());
        }
        if (sr.getExtra() != null && (Subscription.Status.ACTIVE.equals(s.getStatus()) || Subscription.Status.PENDING.equals(s.getStatus()))) {
            sr.getExtra().statusChanged.remove(Subscription.Status.CANCELLED);
            sr.getExtra().statusChanged.remove(Subscription.Status.REMOVED);
            if (sr.getExtra().firstStatus != null && Subscription.Status.ACTIVE.equals(sr.getExtra().firstStatus)) {
                sr.getExtra().statusChanged.remove(Subscription.Status.PENDING);
            }
        }
        s.getSubscriptionRegistry().setExtra(sr.getExtra());
    }

    @Override
    public Subscription getSubscriptionByExternalId(String externalId, SOP sop) {
        return subscriptionService.findByExternalId(externalId, sop);
    }

    @Override
    public Subscription getSubscriptionByUserAccount(String userAccount, SOP sop) {
        return subscriptionService.findByUserAccount(userAccount, sop);
    }

    @Override
    public Subscription getSubscriptionByExternalUserAccount(String externalUserAccount, SOP sop) {
        return subscriptionService.findByExternalUserAccount(externalUserAccount, sop);
    }

    @Override
    public Subscription getSubscription(String externalId, String userAccount, String externalUserAccount, String externalCustomer, SOP sop) {
        Subscription s = null;
        try {
            if (externalId != null) {
                s = getSubscriptionByExternalId(externalId, sop);
            }
            if (s == null && externalUserAccount != null) {
                s = getSubscriptionByExternalUserAccount(externalUserAccount, sop);
            }
            if (s == null && userAccount != null) {
                s = getSubscriptionByUserAccount(userAccount, sop);
            }
            if (s == null && externalCustomer != null) {
                s = subscriptionService.findByExternalCustomer(externalCustomer, sop);
            }
        } catch (Exception ex) {
            log.error("Error al obtener suscripcion. " + ex, ex);
        }

        if (s != null && s.getSubscriptionRegistry() == null) {
            SubscriptionRegistry sr = subscriptionRegistryService.getLastRegisterBySubscription(s.getId(), false);
            if (sr == null) {
                log.error("Una suscripcion sin SubscriptionRegistry. [" + s + "]");
            } else {
                //  log.info("Subscription Registry encontrada: [" + sr + "]");
                s.setSubscriptionRegistry(sr);
                sr.setSubscription(s);
            }
        }

        return s;
    }

    @Override
    public SOP getOrCreateSOP(String serviceMatcher, String operator, String provider) throws Exception {
        return sopService.findOrCreateByNames(serviceMatcher, operator, provider);
    }

    @Override
    public List<Subscription> getSyncSubscriptions(Provider provider) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR_OF_DAY, - 4);
        c.add(Calendar.MONTH, - 1);
        return subscriptionService.getSyncSubscriptions(provider, c.getTime());
    }

    @Override
    public void updateSubscriptionStatus(Subscription s, Subscription.Status status) {
        Date d = new Date();
        s.setStatus(status);
        if (!Subscription.Status.ACTIVE.equals(status) && !Subscription.Status.PENDING.equals(status)) {
            s.setUnsubscriptionDate(d);
            s.getSubscriptionRegistry().setUnsubscriptionDate(d);
            s.getSubscriptionRegistry().setOriginUnsubscription("SYNC:STATUS");
            // log.info("Cancelada suscripcion: [" + s + "]");
        }
        s.getSubscriptionRegistry().setSyncDate(d);
        subscriptionProcess(s);
    }

    @Override
    public SOP getSOP(String service, String operator, String provider) {
        return sopService.findByNames(service, operator, provider);
    }

    @Override
    public List<Subscription> getSubscriptionsByUserAccount(String userAccout, String serviceCode) {
        return subscriptionService.findByUserAccount(userAccout, serviceCode);
    }

    @Override
    public Subscription getSubscriptionStatusByService(String userAccout, String serviceCode, List<Subscription> slist) {

        Subscription s = null;

        List<Subscription> ls = getSubscriptionsByUserAccount(userAccout, serviceCode);

        if (ls == null || ls.isEmpty()) {
            s = new Subscription();
            s.setUserAccount(userAccout);
            s.setStatus(Subscription.Status.REMOVED);
        } else {
            for (Subscription ss : ls) {
                if (Subscription.Status.BLACKLIST.equals(ss.getStatus())) {
                    s = ss;
                    slist.add(ss);
                } else if (Subscription.Status.PENDING.equals(ss.getStatus()) || Subscription.Status.ACTIVE.equals(ss.getStatus())) {
                    slist.add(ss);
                    if (s == null || !Subscription.Status.ACTIVE.equals(s.getStatus())) {
                        s = ss;
                    }
                }
            }
        }

        return s;
    }

    @Override
    public List<SOP> getSOPsByService(String serviceCode, Provider provider) {
        return sopService.getSOPsByService(serviceCode, provider);
    }

    @Override
    public Subscription getSubscriptionsByUserAccountAndCountry(String userAccount, String countryCode) {
        try {
            Long.parseLong(userAccount);
            Country c = countryService.findByCode(countryCode);
            //Retorna la ultima suscripcion realizada (cambio de proveedor)
            return subscriptionService.findByMSISDNAndCountry(userAccount, c);
        } catch (Exception ex) {
        }

        return null;
    }

    @Override
    public List<SOP> getSOPsByCountry(String countryCode) {
        return sopService.getSOPsByCountry(countryCode);
    }

    @Override
    public SubscriptionRegistry getSubscriptionRegistry(Long SubscriptionRegistryId) throws Exception {
        return subscriptionRegistryService.findById(SubscriptionRegistryId);
    }

    @Override
    public Subscription getSubscription(Long SubscriptionId) throws Exception {
        return subscriptionService.findById(SubscriptionId);
    }

    @Override
    public Subscription getSubscriptionStatusFacadeRequest(String userName, SOP sop) throws Exception {
        return subscriptionService.getSubscriptionStatusFacadeRequest(userName, sop);
    }

    @Override
    public List<Subscription> getSubscriptionsWithoutSubscriptionRegistry(Provider provider) {
        return subscriptionService.getSubscriptionsWithoutSubscriptionRegistry(provider);
    }

    @Override
    public SubscriptionRegistry getLastRegisterBySubscription(Long subscriptionId, boolean isPenult) {
        return subscriptionRegistryService.getLastRegisterBySubscription(subscriptionId, isPenult);
    }

    @Override
    public void updateMemoryCache() {
        CacheMessage cm = new CacheMessage();
        cm.type = CacheMessage.CacheMessageType.UPDATE;

        try {
            rabbitMQProducer.messageSender(CHACHE_MEMORY_UPDATE, mapper.writeValueAsString(cm));
        } catch (Exception ex) {
            log.error("No fue posible enviar al Rabbit el mensaje: [" + cm + "]. " + ex, ex);
        }
    }
}
