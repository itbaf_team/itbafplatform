/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.bn.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.itbaf.platform.paymenthub.commons.bn.model.BorderNotificationMessage;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import org.apache.commons.lang3.Validate;
import static com.itbaf.platform.commons.CommonFunction.BORDER_NOTIFICATION;
import com.itbaf.platform.commons.RequestClient;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler {

    private final String smtStatusUrl;
    private final ObjectMapper mapper;
    private final RequestClient requestClient;
    private final RabbitMQProducer rabbitMQProducer;

    @Inject
    public ServiceProcessorHandler(@Named("ph.smt.endpoint.status") final String smtStatusUrl,
            final ObjectMapper mapper,
            final RequestClient requestClient,
            final RabbitMQProducer rabbitMQProducer) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RabbitMQProducer class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.smtStatusUrl = Validate.notNull(smtStatusUrl, "A 'ph.smt.endpoint.status' String class must be provided");
    }

    public void processNotification(String queueName, BorderNotificationMessage notification) {
        if (queueName != null && queueName.length() > 0 && notification != null && (notification.notification != null || notification.message != null)) {
            try {
                rabbitMQProducer.messageSender(BORDER_NOTIFICATION + queueName, mapper.writeValueAsString(notification));
            } catch (Exception ex) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex1) {
                }
                try {
                    rabbitMQProducer.messageSender("bn." + queueName, mapper.writeValueAsString(notification));
                } catch (Exception ex1) {
                    log.error("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
                }
            }
        }
    }

    public Map<String, Object> mDivulgaGetSubscriptionStatus(String countryCode, String basic, String msisdn, String servicecode) {
        String url = smtStatusUrl.replace("{country}", countryCode);
        Map<String, String> headers = new HashMap();
        headers.put("Authorization", basic);
        return requestClient.requestGet(url, "msisdn=" + msisdn + "&servicecode=" + servicecode, MediaType.TEXT_XML_TYPE, MediaType.TEXT_XML_TYPE, headers);
    }
}
