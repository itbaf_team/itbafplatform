/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.paymenthub.repository.jpa.model.ph.JPAPHInstanceRepository;
import com.itbaf.platform.paymenthub.services.repository.PHInstanceService;
import org.apache.commons.lang3.Validate;


/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class PHInstanceServiceTransactional implements PHInstanceService {

    private final JPAPHInstanceRepository phInstanceRepository;

    @Inject
    public PHInstanceServiceTransactional(final JPAPHInstanceRepository phInstanceRepository) {
        this.phInstanceRepository = Validate.notNull(phInstanceRepository, "A JPAPHInstanceRepository class must be provided");
    }

    @Override
    public PHInstance findById(Long id) {
        try {
            if (id != null) {
                Object aux = MainCache.memory().getIfPresent(id);
                PHInstance instance;
                if (aux == null) {
                    instance = phInstanceRepository.findById(id);
                    MainCache.memory().put(id, instance);
                } else {
                    instance = (PHInstance) aux;
                }
                return instance;
            }
        } catch (Exception ex) {
            log.error("Error al obtener PHInstance para ID: [" + id + "]. " + ex, ex);
        }
        return null;
    }

}
