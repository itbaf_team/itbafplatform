/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.model;

import com.google.common.base.MoreObjects;
import java.io.Serializable;

/**
 *
 * @author javier
 */
@lombok.Setter
@lombok.Getter
public class ValidateSubscription implements Serializable {

    private static final long serialVersionUID = 2019010911031L;

    public enum ValidateSubscriptionStatus {
        created, //PIN enviado
        pending, //Evaluando suscripcion por parte de NPAY
        active, //Suscripcion activa
        pending_renewal, //Activo sin cobro
        successful_renewal, //Activo con cobro
        canceling, //Proceso de baja
        canceled, //Suscripcion cancelada
        none, //Imagino q no esta suscripto
        blacklist, //Usuario no puede suscribirse
        locked_P001, //Limite de suscripciones al mismo servicio
        locked_P002, //Limite de suscripciones a diferentes servicios mismo pais
        locked_P003 //Maximo de suscripciones alcanzadas
    }

    private Meta meta;
    private SubscriptionData data;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("meta", meta)
                .add("data", data)
                .omitNullValues().toString();
    }

    public class Meta {

        public String code;
        public String status;
        public String description;

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("code", code)
                    .add("status", status)
                    .add("description", description)
                    .omitNullValues().toString();
        }
    }

}
