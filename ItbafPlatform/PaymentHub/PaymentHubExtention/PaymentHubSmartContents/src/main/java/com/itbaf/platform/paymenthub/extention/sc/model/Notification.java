/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.sc.model;

import com.google.common.base.MoreObjects;
import java.math.BigDecimal;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Notification {

    private String id;
    private String sid;
    private String user_id;
    private String subscription_id;
    private String product_id;
    private String plan_id;
    private String campaign_id;
    private String channel;
    private String Channel_source;
    private BigDecimal price;
    private String action;
    private String charged;
    private String provider;
    private String created_at;
    private String updated_at;
    public String thread = Thread.currentThread().getName();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("sid", sid)
                .add("user_id", user_id)
                .add("subscription_id", subscription_id)
                .add("product_id", product_id)
                .add("plan_id", plan_id)
                .add("campaign_id", campaign_id)
                .add("channel", channel)
                .add("Channel_source", Channel_source)
                .add("price", price)
                .add("action", action)
                .add("charged", charged)
                .add("provider", provider)
                .add("created_at", created_at)
                .add("updated_at", updated_at)
                .add("thread", thread)
                .omitNullValues().toString();
    }

}
