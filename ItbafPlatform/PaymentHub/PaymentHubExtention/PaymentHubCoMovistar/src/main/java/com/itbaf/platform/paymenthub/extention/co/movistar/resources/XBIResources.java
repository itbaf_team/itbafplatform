/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.co.movistar.resources;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.co.movistar.model.DirectDebitEvent;
import com.itbaf.platform.paymenthub.model.SOP;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class XBIResources {

    private final XmlMapper xmlMapper;
    private final RequestClient requestClient;

    private static final String VASP_ID = "16044";
    private static final String XBI_URL = "http://190.13.110.118:52053/xbi";

    //curl -v 'http://190.13.110.118:52053/xbi/directDebitEventReqNew?idVasp=16044&subscriberId=573173428017&serviceCode=12176&subServiceCode=5491&requestId=273669'
    @Inject
    public XBIResources(final XmlMapper xmlMapper,
            final RequestClient requestClient) {
        this.xmlMapper = Validate.notNull(xmlMapper, "A XmlMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
    }

    public DirectDebitEvent directDebitEventReqNew(String msisdn, SOP sop, String serviceCode, String subServiceCode, String requestId) {

        DirectDebitEvent dde = null;
        String url = XBI_URL + "/directDebitEventReqNew?vaspId=" + VASP_ID + "&subscriberId=" + msisdn + "&serviceCode=" + serviceCode + "&subServiceCode=" + subServiceCode + "&requestId=" + requestId;
        String result = null;
        try {
           // result = requestClient.requestSimpleGet(url);
            result = "directDebitEventRes&" + requestId + "&" + requestId + System.currentTimeMillis() + "&11";
            log.info("directDebitEventReqNew. Request: [" + url + "] - SOP: [" + sop.getId() + "]. Response: [" + result + "]");
            if (result != null) {
                dde = new DirectDebitEvent(VASP_ID, msisdn, serviceCode, subServiceCode, sop.getIntegrationSettings().get("finalURL"), result);
            }
        } catch (Exception ex) {
            log.error("Error vaspConsSubsReq. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Response: [" + result + "]. " + ex, ex);
        }

        return dde;
    }

}
