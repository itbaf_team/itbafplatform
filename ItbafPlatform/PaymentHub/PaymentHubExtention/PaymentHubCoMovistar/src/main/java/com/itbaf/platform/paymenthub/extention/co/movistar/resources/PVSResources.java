/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.co.movistar.resources;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.co.movistar.model.RequestResponse;
import com.itbaf.platform.paymenthub.model.SOP;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class PVSResources {

    private final XmlMapper xmlMapper;
    private final RequestClient requestClient;

    private static final String VASP_ID = "16044";
    private static final String PVS_URL = "http://190.13.110.121:46100/pvs";
    
    @Inject
    public PVSResources(final XmlMapper xmlMapper,
            final RequestClient requestClient) {
        this.xmlMapper = Validate.notNull(xmlMapper, "A XmlMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
    }

    public ResponseMessage.Status vaspConsSubsReq(String msisdn, SOP sop, Holder msg) {

        ResponseMessage.Status status = ResponseMessage.Status.ERROR;
        String url = PVS_URL + "/vaspConsSubsReq?vaspId=" + VASP_ID + "&msisdn=" + msisdn;
        String result = null;
        try {
            result = requestClient.requestSimpleGet(url);
            log.info("vaspConsSubsReq. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Response: [" + result + "]");
            if (result != null) {
                RequestResponse rr = xmlMapper.readValue(result, RequestResponse.class);
                status = processRequestResponse(rr);
                if (ResponseMessage.Status.ERROR.equals(status)) {
                    log.error("Error msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Identificar la causa del error: [" + rr.getParams().getResultCode() + "][" + rr.getParams().getDesc() + "]");
                    status = null;
                    msg.value = rr.getParams().getDesc();
                } else if (ResponseMessage.Status.OK.equals(status)) {
                    status = ResponseMessage.Status.REMOVED;
                    String suscriptionId = sop.getIntegrationSettings().get("id_suscripcion");
                    for (com.itbaf.platform.paymenthub.extention.co.movistar.model.Subscription s : rr.getParams().getSubscriptions()) {
                        if (s.getSubscriptionId().equals(suscriptionId)) {
                            status = ResponseMessage.Status.ACTIVE;
                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Error vaspConsSubsReq. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Response: [" + result + "]. " + ex, ex);
        }

        return status;
    }

    public ResponseMessage.Status vaspAddSubsReq(String msisdn, SOP sop, String channelIn, Holder msg) {
        ResponseMessage.Status status = ResponseMessage.Status.ERROR;

        Long subscriptionId = Long.parseLong(sop.getIntegrationSettings().get("id_suscripcion"));
        String source = channelIn == null ? "WEB" : channelIn.trim().toUpperCase();
        String url = PVS_URL + "/vaspAddSubsReq?vaspId=" + VASP_ID + "&subscriptionId=" + subscriptionId + "&msisdn=" + msisdn + "&source=" + source;

        String result = null;
        try {
            result = requestClient.requestSimpleGet(url);
            log.info("vaspAddSubsReq. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Response: [" + result + "]");
            if (result != null) {
                RequestResponse rr = xmlMapper.readValue(result, RequestResponse.class);
                status = processRequestResponse(rr);
                if (ResponseMessage.Status.ERROR.equals(status)) {
                    log.error("Error msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Identificar la causa del error: [" + rr.getParams().getResultCode() + "][" + rr.getParams().getDesc() + "]");
                    status = null;
                    msg.value = rr.getParams().getDesc();
                }
            }
        } catch (Exception ex) {
            log.error("Error vaspAddSubsReq. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Response: [" + result + "]. " + ex, ex);
        }
        return status;
    }

    public ResponseMessage.Status vaspConfSubsReq(String msisdn, SOP sop, String pin, Holder msg) {
        ResponseMessage.Status status = ResponseMessage.Status.ERROR;

        Long subscriptionId = Long.parseLong(sop.getIntegrationSettings().get("id_suscripcion"));
        String url = PVS_URL + "/vaspConfSubsReq?vaspId=" + VASP_ID + "&subscriptionId=" + subscriptionId + "&msisdn=" + msisdn + "&pin=" + pin;

        String result = null;
        try {
            result = requestClient.requestSimpleGet(url);
            log.info("vaspConfSubsReq. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Response: [" + result + "]");
            if (result != null) {
                RequestResponse rr = xmlMapper.readValue(result, RequestResponse.class);
                status = processRequestResponse(rr);
                if (ResponseMessage.Status.ERROR.equals(status)) {
                    log.error("Error msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Identificar la causa del error: [" + rr.getParams().getResultCode() + "][" + rr.getParams().getDesc() + "]");
                    status = null;
                    msg.value = rr.getParams().getDesc();
                }
            }
        } catch (Exception ex) {
            log.error("Error vaspConfSubsReq. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Response: [" + result + "]. " + ex, ex);
        }
        return status;
    }

    public ResponseMessage.Status vaspDelSubsReq(String msisdn, SOP sop, Holder msg) {
        ResponseMessage.Status status = ResponseMessage.Status.ERROR;

        Long subscriptionId = Long.parseLong(sop.getIntegrationSettings().get("id_suscripcion"));
        String url = PVS_URL + "/vaspDelSubsReq?vaspId=" + VASP_ID + "&subscriptionId=" + subscriptionId + "&msisdn=" + msisdn;

        String result = null;
        try {
            result = requestClient.requestSimpleGet(url);
            log.info("vaspDelSubsReq. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Response: [" + result + "]");
            if (result != null) {
                RequestResponse rr = xmlMapper.readValue(result, RequestResponse.class);
                status = processRequestResponse(rr);
                if (ResponseMessage.Status.ERROR.equals(status)) {
                    log.error("Error msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Identificar la causa del error: [" + rr.getParams().getResultCode() + "][" + rr.getParams().getDesc() + "]");
                    status = null;
                    msg.value = rr.getParams().getDesc();
                }
            }
        } catch (Exception ex) {
            log.error("Error vaspDelSubsReq. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Response: [" + result + "]. " + ex, ex);
        }
        return status;
    }

    public RequestResponse vaspGetActionsReq() {
        log.info("Buscando ultimos movimientos Altas/Bajas");
        RequestResponse rr = null;
        String url = PVS_URL + "/vaspGetActionsReq?vaspId=" + VASP_ID;
        String result = null;
        try {
            //result = requestClient.requestSimpleGet(url);
            result = "<?xml version=\"1.0\"?>"
                    + "<methodResponse>"
                    + "<params>"
                    + "<resultCode>0</resultCode>"
                    + "<desc>Ok</desc>"
                    + "<transactionId>180</transactionId>"
                    + "<subscription>"
                    + "<subscriptionId>1880</subscriptionId>"
                    + "<media>WEBSERVICE</media>"
                    + "<msisdn>573900000004</msisdn>"
                    + "<datetime>20170501163045</datetime>"
                    + "<action>ALTA</action>"
                    + "</subscription>"
                    + "<subscription>"
                    + "<subscriptionId>1880</subscriptionId>"
                    + "<media>SMSMO</media>"
                    + "<msisdn>573900000002</msisdn>"
                    + "<datetime>20170501163045</datetime>"
                    + "<action>BAJA</action>"
                    + "</subscription>"
                    + "<subscription>"
                    + "<subscriptionId>1880</subscriptionId>"
                    + "<media>SMSMO</media>"
                    + "<msisdn>573900000003</msisdn>"
                    + "<datetime>20170501170112</datetime>"
                    + "<action>BAJAATT</action>"
                    + "</subscription>"
                    + "</params>"
                    + "</methodResponse>";
            log.info("vaspGetActionsReq. Response: [" + result + "]");
            if (result != null) {
                rr = xmlMapper.readValue(result, RequestResponse.class);
            }
        } catch (Exception ex) {
            log.error("Error vaspGetActionsReq. Response: [" + result + "]. " + ex, ex);
        }
        return rr;
    }

    public void vaspConfActionsReq(String transactionId) throws Exception {
        log.info("Confirmando ultimos movimientos Altas/Bajas: [" + transactionId + "]");

        String response = null;
        String url = PVS_URL + "/vaspConfActionsReq?vaspId=" + VASP_ID + "&transactionId=" + transactionId;
        RequestResponse rr = null;
        try {
            //response = requestClient.requestSimpleGet(url);
            response = "<?xml version=\"1.0\"?>"
                    + "<methodResponse>"
                    + "<params>"
                    + "<resultCode>0</resultCode>"
                    + "<desc>Ok</desc>"
                    + "</params>"
                    + "</methodResponse>";
            log.info("vaspConfActionsReq. Response: [" + response + "]");
            if (response != null) {
                rr = xmlMapper.readValue(response, RequestResponse.class);
            }
        } catch (Exception ex) {
            log.error("Error vaspGetActionsReq. Response: [" + response + "]. " + ex, ex);
        }

        if (rr != null && 0L == (rr.getParams().getResultCode())) {
            log.info("Se ha confirmado el transactionId: [" + transactionId + "]");
        } else {
            log.error("Error al confirmar el transactionId: [" + transactionId + "] - Error: [" + rr.getParams().getDesc() + "]");
        }
    }

    private ResponseMessage.Status processRequestResponse(RequestResponse rr) {
        ResponseMessage.Status status = ResponseMessage.Status.ERROR;
        switch (rr.getParams().getResultCode()) {
            case 0:
            case 1120:
                status = ResponseMessage.Status.OK;
                break;
            case 1100:
            case 1101:
            case 1102:
            case 1103:
            case 1104:
            case 1105:
            case 1106:
            case 1108:
            case 1128:
            case 1999:
                break;
            case 1107:
                status = ResponseMessage.Status.ACTIVE;
                break;
            case 1130:
                status = ResponseMessage.Status.REMOVED;
                break;
            case 1200:
                status = ResponseMessage.Status.BLACKLIST;
                break;
            case 1110:
            case 1111:
            case 1112:
                status = ResponseMessage.Status.PIN_ERROR;
            default:
                log.error("No existe un ResultCode valido para: [" + rr.getParams().getResultCode() + "]");
        }
        return status;
    }

}
