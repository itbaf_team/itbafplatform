/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.repository.jpa.model;

import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.itbaf.platform.pgp.model.GenericProperty;
import com.itbaf.platform.pgp.model.GenericProperty.Profile;
import com.itbaf.platform.pgp.model.QGenericProperty;
import com.querydsl.jpa.impl.JPAQuery;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAGenericPropertyRepository extends AbstractJPARepositorySession<GenericProperty, Long> {

    @Inject
    public JPAGenericPropertyRepository(final Provider<EntityManager> entityManagerProvider) {
        super(GenericProperty.class, entityManagerProvider);
    }

    public List<GenericProperty> findByProfile(Profile profile) {
        if (profile == null) {
            String error = "Uno de los parametros es nulo!! profile: [" + profile + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QGenericProperty pp = QGenericProperty.genericProperty;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<GenericProperty> pro = query.select(pp).from(pp).
                where(pp.profile.eq(profile)).fetch();

        if (pro == null) {
            return new ArrayList();
        }

        return pro;
    }

    public GenericProperty getValueByProfileKey(Profile profile, String key) {
        if (profile == null || key == null) {
            String error = "Uno de los parametros es nulo!! profile: [" + profile + "] - key: [" + key + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QGenericProperty pp = QGenericProperty.genericProperty;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        GenericProperty pro = query.select(pp).from(pp).
                where(ObjectPredicates.combine(pp.profile.eq(profile), pp.key.eq(key)))
                .fetchFirst();

        return pro;
    }

}
