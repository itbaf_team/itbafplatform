/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Preference {

    public enum Status {
        pending, authorized, paused, cancelled
    }

    private String id;
    private String processing_modes;
    private Boolean binary_mode;
    private String payment_methods;
    private Long collector_id;
    private Payment.OperationType operation_type;
    private Item[] items;
    private Payer payer;
    private BackUrls backUrls;
    private String auto_return;
    private String client_id;
    private String marketplace;
    private BigDecimal marketplace_fee;
    private Shipments shipments;
    private String notification_url;
    private String external_reference;
    private String additional_info;
    private Boolean expires;
    private String expiration_date_from;
    private String expiration_date_to;
    private String date_created;
    private String init_point;
    private String sandbox_init_point;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("processing_modes", processing_modes)
                .add("binary_mode", binary_mode)
                .add("payment_methods", payment_methods)
                .add("collector_id", collector_id)
                .add("operation_type", operation_type)
                .add("items", items)
                .add("payer", payer)
                .add("backUrls", backUrls)
                .add("auto_return", auto_return)
                .add("client_id", client_id)
                .add("marketplace", marketplace)
                .add("marketplace_fee", marketplace_fee)
                .add("sandbox_init_point", sandbox_init_point)
                .add("shipments", shipments)
                .add("notification_url", notification_url)
                .add("external_reference", external_reference)
                .add("additional_info", additional_info)
                .add("expires", expires)
                .add("expiration_date_from", expiration_date_from)
                .add("expiration_date_to", expiration_date_to)
                .add("date_created", date_created)
                .add("init_point", init_point)
                .add("sandbox_init_point", sandbox_init_point)
                .omitNullValues().toString();
    }

}
