/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.persistence.util.HibernateEquality;
import com.itbaf.platform.pgp.model.BaseEntity;
import com.itbaf.platform.pgp.model.Product;
import com.itbaf.platform.pgp.model.oauth.properties.CredentialUserProperty;
import com.itbaf.platform.pgp.model.oauth.properties.ExternalDataUserProperty;
import com.itbaf.platform.pgp.model.oauth.properties.ProfileUserProperty;
import com.itbaf.platform.pgp.model.oauth.properties.SessionUserProperty;
import com.itbaf.platform.pgp.model.oauth.properties.UserProperty;
import com.itbaf.platform.pgp.model.oauth.properties.UserProperty.Topic;
import com.querydsl.core.annotations.Config;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import com.itbaf.platform.pgp.model.secured.Role;
import com.itbaf.platform.pgp.model.secured.Permission;
import javax.persistence.Transient;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@Entity(name = "OAUTH_USER")
@Table(name = "OAUTH_USER")
@Cacheable(true)
@Cache(region = "oauth", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors = true)
public class User extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 20170914135901L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Status status;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "OAUTH_USER_PROPERTY",
            joinColumns = @JoinColumn(name = "OAUTH_USER_ID"))
    @MapKeyColumn(name = "TOPIC")
    @MapKeyEnumerated(EnumType.STRING)
    @Type(type = "json")
    @Column(name = "VALUE", columnDefinition = "json")
    private Map<Topic, UserProperty> userProperties;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "OAUTH_USER_PRODUCT", joinColumns = {
        @JoinColumn(name = "OAUTH_USER_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "PRODUCT_ID", nullable = false, updatable = false)})
    private Set<Product> products;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID")
    //Cuenta principal si esta es una subcuenta
    private User principal;

    @OneToMany(mappedBy = "principal", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    //SubAccounts si esta es la cuenta principal
    private Set<User> users;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "OAUTH_ROLE_ID")
    private Role role;

    @Transient
    private Map<String, Permission> permissions;
    @Transient
    private Map<String, String> limitPermissions;
    @Transient
    public RLock lock;

    public User() {
        this.status = Status.ENABLE;
        userProperties = new HashMap();
        userProperties.put(Topic.SESSION, new SessionUserProperty());
        userProperties.put(Topic.CREDENTIAL, new CredentialUserProperty());
        userProperties.put(Topic.PROFILE, new ProfileUserProperty());
        userProperties.put(Topic.EXTERNAL_DATA, new ExternalDataUserProperty());
    }

    @JsonIgnore
    public Map<String, Permission> getPermissions() {
        if (permissions == null && role != null && role.getPermissions() != null && !role.getPermissions().isEmpty()) {
            permissions = new HashMap();
            for (Permission p : role.getPermissions()) {
                permissions.put(p.getName(), p);
            }
        }
        if (role == null || role.getPermissions() == null || role.getPermissions().isEmpty()) {
            permissions = null;
        }

        return permissions;
    }

    @JsonIgnore
    public Map<String, String> getLimitPermissions() {
        if (limitPermissions == null && role != null && role.getPermissions() != null && !role.getPermissions().isEmpty()) {
            limitPermissions = new HashMap();
            for (Permission p : role.getPermissions()) {
                limitPermissions.put(p.getName(), p.getName());
            }
        }
        if (role == null || role.getPermissions() == null || role.getPermissions().isEmpty()) {
            limitPermissions = null;
        }

        return limitPermissions;
    }

    @JsonIgnore
    public CredentialUserProperty getCredentialUserProperty() {
        UserProperty up = userProperties.get(Topic.CREDENTIAL);
        if (up == null) {
            up = new CredentialUserProperty();
            userProperties.put(Topic.CREDENTIAL, up);
        }
        return (CredentialUserProperty) up;
    }

    @JsonIgnore
    public SessionUserProperty getSessionUserProperty() {
        UserProperty up = userProperties.get(Topic.SESSION);
        if (up == null) {
            up = new SessionUserProperty();
            userProperties.put(Topic.SESSION, up);
        }
        return (SessionUserProperty) up;
    }

    @JsonIgnore
    public ProfileUserProperty getProfileUserProperty() {
        UserProperty up = userProperties.get(Topic.PROFILE);
        if (up == null) {
            up = new ProfileUserProperty();
            userProperties.put(Topic.PROFILE, up);
        }
        return (ProfileUserProperty) up;
    }

    @JsonIgnore
    public ExternalDataUserProperty getExternalDataUserProperty() {
        UserProperty up = userProperties.get(Topic.EXTERNAL_DATA);
        if (up == null) {
            up = new ExternalDataUserProperty();
            userProperties.put(Topic.EXTERNAL_DATA, up);
        }
        return (ExternalDataUserProperty) up;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("status", status)
                .add("role", role)
                .add("userProperties", userProperties)
                .add("products", products)
                .add("users", users)
                .add("principal", (principal == null ? null : principal.getId()))
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status, userProperties, products, role);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<User>()
                .over(User::getId)
                .over(User::getStatus)
                .over(User::getUserProperties)
                .over(User::getProducts)
                .over(User::getRole)
                .asserts(this, obj);
    }
}
