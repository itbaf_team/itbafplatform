/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.model.billing.BillerAttempt;
import com.itbaf.platform.paymenthub.model.billing.BillerAttemptTransaction;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPABillerAttemptTransactionRepository;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPABillerAttemptRepository;
import com.itbaf.platform.paymenthub.services.repository.BillerAttemptService;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.paymenthub.services.repository.BillerAttemptTransactionService;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class BillerAttemptTransactionServiceTransactional implements BillerAttemptTransactionService {

    private final JPABillerAttemptTransactionRepository billerAttemptRepository;

    @Inject
    public BillerAttemptTransactionServiceTransactional(final JPABillerAttemptTransactionRepository billerAttemptRepository) {
        this.billerAttemptRepository = Validate.notNull(billerAttemptRepository, "A JPABillerAttemptTransactionRepository class must be provided");
    }

    @Override
    public BillerAttemptTransaction findById(Long id) throws Exception {
        return billerAttemptRepository.findById(id);
    }

    @Override
    public void saveOrUpdate(BillerAttemptTransaction billerAttempt) throws Exception {

        if (billerAttempt.getId() == null) {
            billerAttemptRepository.save(billerAttempt);
        } else {
            billerAttemptRepository.merge(billerAttempt);
        }
    }

}
