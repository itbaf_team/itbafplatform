/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.persist.PersistService;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.repository.connection.BasicDBConnectionProvider;
import com.itbaf.platform.persistence.cache.CustomRedisRegionFactory;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.persistence.util.json.JacksonUtil;

/**
 *
 * @author jordonez
 */
public final class JPAInitializer {

    private final BasicDataBaseConnection dataBaseConnection;
    private final InstrumentedObject instrumentedObject;
    private final ObjectMapper objectMapper;
    private final PersistService service;

    @Inject
    JPAInitializer(PersistService service, final BasicDataBaseConnection dataBaseConnection, final InstrumentedObject instrumentedObject, final ObjectMapper objectMapper) {
        this.dataBaseConnection = dataBaseConnection;
        this.instrumentedObject = instrumentedObject;
        this.objectMapper = objectMapper;
        this.service = service;
       // startService();
    }

    public void startService() {
        BasicDBConnectionProvider.dataBaseConnection = dataBaseConnection;
        CustomRedisRegionFactory.instrumentedObject = instrumentedObject;
        JacksonUtil.OBJECT_MAPPER = objectMapper;
        service.start();
    }

    public void stopService() {
        service.stop();
    }
}
