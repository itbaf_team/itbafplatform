package com.itbaf.platform.paymenthub.extention.co.movistar.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.extention.co.movistar.handler.ServiceProcessorHandler;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.ws.InnerService;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/inner")
@Singleton
@lombok.extern.log4j.Log4j2
public class InnerRestService implements InnerService {

    private final ObjectMapper mapper;
    private final XmlMapper xmlMapper;
    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public InnerRestService(final ObjectMapper mapper, XmlMapper xmlMapper,
            final ServiceProcessorHandler serviceProcessorHandler) {
        this.xmlMapper = xmlMapper;
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");

    }

    @Override
    @POST
    @Path("/subscription/status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptionStatus(Subscription subscription) {
        //log.info("SubscriptionStatus [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }

        ResponseMessage rm = new ResponseMessage();
        try {
            rm = serviceProcessorHandler.getSubscriptionStatus(subscription);
        } catch (Exception ex) {
            rm.message = "Error al procesar status." + ex.getMessage();
            log.error(rm.message, ex);
        }
        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscribe")
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscribe(Subscription subscription) {
        // log.info("Subscribe [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        rm.message = "Metodo no implementado en Movistar Colombia para dar de alta usuarios.";

        //Validar status en la telco antes de dar de alta
        return Response.status(Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscribe/pin/send")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendPin(Subscription subscription) {
        // log.info("SendPin [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        try {
            rm = serviceProcessorHandler.sendPin(subscription);
        } catch (JsonProcessingException ex) {
            rm.message = "Error al sendPin." + ex.getMessage();
            log.error(rm.message, ex);
        }
        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscribe/pin/validate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validatePin(Subscription subscription, @QueryParam("pin") final String pin) {
        //log.info("ValidatePIN [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        try {
            rm = serviceProcessorHandler.validatePin(subscription, pin);
        } catch (JsonProcessingException ex) {
            rm.message = "Error al ValidatePIN." + ex.getMessage();
            log.error(rm.message, ex);
        }
        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/unsubscribe")
    @Produces(MediaType.APPLICATION_JSON)
    public Response unsubscribe(Subscription subscription) {

        // log.info("Unsubscribe subscription: [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        try {
            rm = serviceProcessorHandler.unsubscribe(subscription);
        } catch (JsonProcessingException ex) {
            rm.message = "Error al unsubscribe." + ex.getMessage();
            log.error(rm.message, ex);
        }
        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscription/normalizer")
    @Produces(MediaType.APPLICATION_JSON)
    public Response userAccountNormalizer(Subscription subscription) {
        //  log.info("userAccountNormalizer [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm = serviceProcessorHandler.userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.message = "Error al procesar userAccountNormalizer. " + ex.getMessage();
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR_403;
        }
        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscription/send/sms")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendSMS(Subscription subscription) {
        //     log.info("SendSMS [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        rm.message = "Metodo no implementado en Movistar Colombia para enviar SMSs.";

        //Validar status en la telco antes de dar de alta
        return Response.status(Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    public Response charge(OnDemandRegistry or) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
