/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import java.util.List;
import java.util.Map;

/**
 *
 * @author javier
 */
public interface SopService {

    public SOP findById(Long id);

    public SOP findByNames(String serviceMatcher, String operator, String provider);

    public SOP findOrCreateByNames(String serviceMatcher, String operator, String provider) throws Exception;

    public SOP findBySettings(String key, String value);

    public SOP findBySettings(Map<String, String> settings);

    public SOP findBySettings(String provider, String key, String value);

    public SOP findBySettings(String provider, Map<String, String> settings);

    public List<SOP> findAllBySettings(String provider, Map<String, String> settings);

    public SOP findWithTariffBySettings(String provider, String key, String value);

    public SOP findWithTariffBySettings(String provider, Map<String, String> settings);

    public List<SOP> getMatchByProviderName(String name);

    public List<SOP> getSOPsByService(String serviceCode, Provider provider);

    public List<SOP> getSOPsByProviderName(String providerName);

    public List<SOP> getSOPsByCountry(String countryCode);

    public SOP getSOPByTariff(String tariffExternalid);

    public void saveOrUpdate(SOP sop) throws Exception;
}
