/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.ondemand;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.paymenthub.model.BaseEntity;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
@Entity(name = "ONDEMAND_REGISTRY")
@Table(name = "ONDEMAND_REGISTRY", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class OnDemandRegistry extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 20180411114101L;

    public enum Status {
        OPTIN, // Se espera una respuesta de confirmacion del usuario
        PENDING, // Se espera una respuesta del proveedor
        CHARGED, // Se realizo un cobro efectivo al usuario
        ERROR, // Hubo algun error al intentar realizar el cobro al usuario
        ERROR_CHARGE // Usuario sin credito o limite por parte del proveedor
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    private String id;
    @Column(name = "USER_ACCOUNT")
    private String userAccount;
    @OneToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private Status status;
    @Column(name = "STATUS_MESSAGE")
    private String statusMessage;
    @Column(name = "TRANSACTION_ID")
    private String transactionId;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ONDEMAND_DATE")
    private Date ondemandDate;
    @Column(name = "ORIGIN_ONDEMAND")
    private String originOndemand;
    @Type(type = "json")
    @Column(name = "TRANSACTION_TRACKING", columnDefinition = "json")
    private FacadeRequest transactionTracking;
    @Transient
    private Object lock;
    @Transient
    private String thread = Thread.currentThread().getName();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("userAccount", userAccount)
                .add("status", status)
                .add("statusMessage", statusMessage)
                .add("sop", sop)
                .add("transactionId", transactionId)
                .add("ondemandDate", ondemandDate)
                .add("originOndemand", originOndemand)
                .add("transactionTracking", transactionTracking)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userAccount, status, sop, statusMessage, transactionId, ondemandDate, originOndemand, transactionTracking);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<OnDemandRegistry>()
                .over(OnDemandRegistry::getId)
                .over(OnDemandRegistry::getUserAccount)
                .over(OnDemandRegistry::getStatus)
                .over(OnDemandRegistry::getSop)
                .over(OnDemandRegistry::getStatusMessage)
                .over(OnDemandRegistry::getOndemandDate)
                .over(OnDemandRegistry::getOriginOndemand)
                .over(OnDemandRegistry::getTransactionId)
                .over(OnDemandRegistry::getTransactionTracking)
                .asserts(this, obj);
    }
}
