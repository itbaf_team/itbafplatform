/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.wallet.WalletTransaction;
import com.itbaf.platform.pgp.repository.jpa.model.wallet.JPAWalletTransactionRepository;
import com.itbaf.platform.pgp.services.repository.WalletTransactionService;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class WalletTransactionServiceTransactional implements WalletTransactionService {

    private final JPAWalletTransactionRepository walletTransactionRepository;

    @Inject
    public WalletTransactionServiceTransactional(final JPAWalletTransactionRepository walletTransactionRepository) {
        this.walletTransactionRepository = Validate.notNull(walletTransactionRepository, "A JPAWalletTransactionRepository class must be provided");
    }

    @Override
    public void saveOrUpdate(WalletTransaction walletTransaction) throws Exception {
        if (walletTransaction.getId() == null) {
            walletTransactionRepository.save(walletTransaction);
        } else {
            walletTransactionRepository.merge(walletTransaction);
        }
    }

    @Override
    public List<WalletTransaction> getWalletTransaction(User user) {
        return walletTransactionRepository.getWalletTransaction(user);
    }
    
    @Override
    public List<WalletTransaction> getWalletTransaction(User user, Date from, Date to) {
        return walletTransactionRepository.getWalletTransaction(user, from, to);
    }

    @Override
    public WalletTransaction findById(Long id) throws Exception {
        return walletTransactionRepository.findById(id);
    }

    @Override
    public WalletTransaction findByTransactionId(String transactionId) {
       return walletTransactionRepository.findByTransactionId(transactionId);
    }

    @Override
    public WalletTransaction getLastWithdraw(User user, Long sopId, Date from, Date to) {
        return walletTransactionRepository.findLatsWithdraw(user, sopId, from, to);
    }
}
