/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.terra.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.terra.resources.TerraNotification;
import com.itbaf.platform.paymenthub.extention.terra.resources.TerraResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final TerraResources terraResources;

    @Inject
    public ServiceProcessorHandler(final TerraResources npayResources) {
        this.terraResources = Validate.notNull(npayResources, "A TerraResources class must be provided");
    }

    public void processNotification(TerraNotification notification, String countryCode) {
        try {
            if (notification.fechaLocale == null) {
                notification.fechaLocale = locateDate(notification.fecha, countryCode);
            }
        } catch (Exception ex) {
            log.error("Error al convertir fecha: [" + notification + "]");
            notification.fechaLocale = new Date();
        }
        final SOP sop = sopService.findBySettings("terraIdSusc", notification.idsusc);
        if (sop == null) {
            //Si sucede, hacer como en NPAY con: p = providers.get(rand.nextInt(providers.size())); 
            log.error("No existe un SOP configurado para el terraIdSusc: [" + notification.idsusc + "] - Notificacion: [" + notification + "]");
            return;
        }

        notification.countryCode = countryCode;

        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notification);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + sop.getProvider().getName(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notification);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + sop.getProvider().getName(), jsonResponse);
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {

        TerraNotification notification;
        try {
            notification = mapper.readValue(message, TerraNotification.class);
            Thread.currentThread().setName(notification.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }

        final SOP sop = sopService.findBySettings("terraIdSusc", notification.idsusc);
        if (sop == null) {
            log.error("No existe un SOP configurado para el terraIdSusc: [" + notification.idsusc + "] - Notificacion: [" + notification + "]");
            return null;
        }
        final String threadName = Thread.currentThread().getName() + "-" + sop.getProvider().getName() + ".n";
        Thread.currentThread().setName(threadName);
        PaymentHubMessage msg = null;

        switch (notification.evento.toLowerCase()) {
            case "alta":
                if ("1".equals(notification.status)) {
                    //  log.info("Notificacion de alta [" + notification.evento + "]. MSISDN: [" + notification.movil + "] - IdSusc: [" + notification.idsusc + "]");
                    msg = processSubscriptionNotification(notification, sop, true);
                    return processBillingNotification(notification, msg, sop);
                }
                break;
            case "renovacion": //  billing
                if ("1".equals(notification.status)) {
                    // log.info("Notificacion de renovacion. MSISDN: [" + notification.movil + "] - IdSusc: [" + notification.idsusc + "]");
                    msg = processSubscriptionNotification(notification, sop, false);
                    return processBillingNotification(notification, msg, sop);
                }
                break;
            case "baja": // unsubscribe
                if ("1".equals(notification.status)) {
                    // log.info("Notificacion de baja [" + notification.evento + "]. MSISDN: [" + notification.movil + "] - IdSusc: [" + notification.idsusc + "]");
                    return processUnsubscriptionNotification(notification, sop);
                }
                break;
            case "cambio_movil":
                // log.info("Notificacion de cambio_movil. Ignorada");
                break;
            case "habilitar":
                // log.info("Notificacion que pide habilitar ya q despues d mucho tiempo se logro cobrar. Ignorada");
                break;
            case "deshabilitar":
                // log.info("Notificacion que pide deshabilitar servicio ya que tiene muchos cobros pendientes. Ignorada");
                break;
            case "alta_en_preactiva_cplan":
            case "alta_en_preactiva":
                log.warn("Notificacion de alta preactiva. Chequear status [" + notification + "] Ignorada");
                break;
            default:
                log.error("Evento inesperado: [" + notification.evento + "] - Notificacion: [" + notification + "]");
        }

        return msg == null ? null : true;
    }

    private PaymentHubMessage processSubscriptionNotification(TerraNotification notification, SOP sop, boolean toServer) {

        Provider provider = sop.getProvider();
        PaymentHubMessage msg = new PaymentHubMessage();

        Tariff t = sop.getMainTariff();

        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(notification.fechaLocale);
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(MessageType.SUBSCRIPTION_ACTIVE);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(null);
            msg.setTransactionId(notification.idtrx);
            msg.setChannelIn(notification.origen);
            msg.setUserAccount(notification.movil);

            Extra extra = new Extra();
            if (notification.idservicio != null) {
                extra.stringData.put("idservicio", notification.idservicio);
            }
            if (notification.oper_id != null) {
                extra.stringData.put("oper_id", notification.oper_id);
            }
            if (notification.idnotificacion != null) {
                extra.stringData.put("idnotificacion", notification.idnotificacion);
            }
            if (notification.plataforma != null) {
                extra.stringData.put("plataforma", notification.plataforma);
            }
            if (notification.keyword != null) {
                extra.stringData.put("keyword", notification.keyword);
            }
            if (!extra.stringData.isEmpty()) {
                msg.setExtra(extra);
            }

            if (toServer) {
                String jsonResponse = mapper.writeValueAsString(msg);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
                }
            }

            return msg;
        } catch (Exception ex) {
            log.error("Error al procesar TerraNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    private Boolean processBillingNotification(TerraNotification notification, PaymentHubMessage msg, SOP sop) {

        if (msg == null) {
            return null;
        }

        BigDecimal fullAmount = null;
        if (notification.precio != null && notification.precio.length() > 0 && !"0".equals(notification.precio)) {
            fullAmount = new BigDecimal(notification.precio);
        }

        if (fullAmount == null || fullAmount.doubleValue() <= 0) {
            //  log.info("No se procesara la notificacion de cobro con valor CERO");
            return true;
        }

        //  log.info("Procesando un cobro: [" + msg.getUserAccount() + "] - Amount: [" + notification.precio + "] - SOP: [" + msg.getSopId() + "]");
        try {
            Tariff t = sop.findTariffByFullAmount(fullAmount);
            if (t == null) {
                //Verificamos si es una tarifa principal nueva
                t = updateMainTariff(sop, fullAmount, null);
                if (t == null) {
                    //Creamos o actualizamos la tarifa hija
                    t = updateFullAmountChildrenTariff(sop, fullAmount, notification.idsusc);
                }
            }

            if (t == null) {
                log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
                return null;
            }

            fullAmount = t.getFullAmount();
            BigDecimal netAmount = t.getNetAmount();

            if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }

            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setChargedDate(notification.fechaLocale);
            msg.setTariffId(t.getId());
            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar notificacion de cobro TerraNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private Boolean processUnsubscriptionNotification(TerraNotification notification, SOP sop) {

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            Provider provider = sop.getProvider();
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(null);
            msg.setToDate(notification.fechaLocale);
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(sop.getMainTariff().getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(null);
            msg.setTransactionId(notification.idtrx);
            msg.setChannelOut(notification.origen);
            msg.setUserAccount(notification.movil);

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar TerraNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        rm.status = ResponseMessage.Status.OK_NS;
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        rm = terraResources.validateSubscriptionByMsisdn(subscription.getUserAccount(), subscription.getSop());
        switch (rm.status) {
            case ACTIVE:
                subscription.setStatus(Subscription.Status.ACTIVE);
                break;
            case PENDING:
                subscription.setStatus(Subscription.Status.PENDING);
                break;
            case REMOVED:
                subscription.setStatus(Subscription.Status.REMOVED);
                break;
            case BLACKLIST:
                subscription.setStatus(Subscription.Status.BLACKLIST);
                break;
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        if (subscription.getObject() == null) {
            rm.status = ResponseMessage.Status.PIN_ERROR;
            rm.message = "Es necesario un PIN para realizar la baja.";
            rm.data = mapper.writeValueAsString(subscription);
            return rm;
        }
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        if (subscription.getSubscriptionRegistry().getId() != null) {
            rm = terraResources.removeSubscription(subscription);
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage sendPin(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        rm = terraResources.sendSubscriptionPin(subscription.getUserAccount(), subscription.getSop());
        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage validatePinSubscription(Subscription subscription, String pin) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);
        switch (aux.status) {
            case ACTIVE:
            case PENDING:
            case BLACKLIST:
                rm.status = aux.status;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case REMOVED:
                ResponseMessage result = terraResources.validatePinSubscription(subscription.getUserAccount(),
                        subscription.getSop(), pin);
                if (result != null) {
                    rm.status = result.status;
                    rm.message = result.message;
                }
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            try {
                rm.data = mapper.writeValueAsString(subscription);
            } catch (JsonProcessingException ex1) {
            }
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    public ResponseMessage sendSMS(Subscription subscription, String message) {
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;

        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        try {
            rm = terraResources.sendSMS(subscription.getUserAccount(),
                    subscription.getSop(), message);
        } catch (Exception ex) {
            rm.message = "Error al enviar SMS. " + ex;
            log.error(rm.message, ex);
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (Exception ex) {
        }
        return rm;
    }

    public Date locateDate(String date_yyyyMMddHHmmSS, String countryCode) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        TimeZone timeZone = null;
        switch (countryCode.toUpperCase()) {
            case "EC":
                timeZone = TimeZone.getTimeZone("America/Bogota");
                break;
        }
        sdf.setTimeZone(timeZone);
        Date d = sdf.parse(date_yyyyMMddHHmmSS);
        Calendar auxx = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        auxx.setTimeInMillis(d.getTime());
        return auxx.getTime();
    }
}
