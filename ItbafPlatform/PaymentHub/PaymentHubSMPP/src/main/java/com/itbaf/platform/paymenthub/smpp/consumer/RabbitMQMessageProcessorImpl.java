/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.smpp.consumer;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.messaging.smpp.PaymentHubSMPPMessage;
import com.itbaf.platform.paymenthub.messaging.smpp.SMPPMessageType;
import com.itbaf.platform.rmq.consumer.MessageProcessor;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQMessageProcessorImpl implements RabbitMQMessageProcessor {

    private static final Cache<String, String> notificationMemory = CacheBuilder.newBuilder()
            .expireAfterWrite(3, TimeUnit.SECONDS) //Tiempo de vida
            .build();

    private final Map<SMPPMessageType, MessageProcessor> messsageProcessorMap;

    @Inject
    public RabbitMQMessageProcessorImpl(final Set<MessageProcessor> messageProcessorSet) {
        messsageProcessorMap = new HashMap();
        if (messageProcessorSet != null) {
            log.info("messageProcessorSet size: " + messageProcessorSet.size());
            for (MessageProcessor nr : messageProcessorSet) {
                //   log.info("JF- MessageProcessor item: [" + nr.getSMPPMessageType() + "] - [" + nr.getClass().getSimpleName() + "]");
                //   messsageProcessorMap.put(nr.getSMPPMessageType(), nr);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {
        try {
            String tid = Thread.currentThread().getName() + ".smpp";
            Thread.currentThread().setName(tid);
        } catch (Exception ex) {
        }

        PaymentHubSMPPMessage msg = null;

        /*  try {
            msg = mapper.readValue(message, PaymentHubSMPPMessage.class);


           try {
                String memoryKey = msg.getUserAccount() + "_" + msg.getInternalId();
                while (notificationMemory.getIfPresent(memoryKey) != null) {
                    logger.info("Esperando a que se procese otra notificacion... [" + memoryKey + "]");
                    try {
                        Thread.sleep(1000);
                    } catch (Exception ex) {
                    }
                }
                notificationMemory.put(memoryKey, memoryKey);
            } catch (Exception ex) {
            }

            notification = new Notification();
            notification.setNotificationDate(msg.getFromDate());
            notification.setNotificationType(msg.getMessageType());
            notification.setOriginalNotification(msg.getNotificationString());
            notification.setInternalId(msg.getInternalId());
            notification.setSop(new SOP(msg.getCodeCountry(), msg.getService(), msg.getOperator(), msg.getProvider()));
            notification.setExternalId(msg.getSuscriptionId());
            notification.setTransactionId(msg.getTransactionId());
            notification.setUserAccount(msg.getUserAccount());
            notification = notificationService.saveNotification(notification);
        } catch (Exception ex) {
            logger.error("Error procesando mensaje", ex);
        }*/

 /*    try {
            notificationProcessorMap.get(msg.getNotificationType()).process(msg, notification);
        } catch (Exception ex) {
            logger.error("Error procesando notificacion: [" + msg.getNotificationType() + "]", ex);
        }*/
        return null;
    }

}
