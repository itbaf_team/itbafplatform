package com.itbaf.platform.paymenthub.extention.store.model.recobro;

import com.google.common.base.MoreObjects;
import java.math.BigDecimal;

public class InfotainmentChargeReportEntry {

    public String CSP;
    public String TID;
    public String COD_AUT;
    public String SER_TM;
    public String SER_TH;
    public String MSISDN;
    public String CANAL;
    public BigDecimal DEUDA;
    public String ITEM_ID;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("CSP", CSP)
                .add("TID", TID)
                .add("COD_AUT", COD_AUT)
                .add("SER_TM", SER_TM)
                .add("SER_TH", SER_TH)
                .add("MSISDN", MSISDN)
                .add("CANAL", CANAL)
                .add("DEUDA", DEUDA)
                .add("itemId", ITEM_ID)
                .omitNullValues().toString();
    }

}
