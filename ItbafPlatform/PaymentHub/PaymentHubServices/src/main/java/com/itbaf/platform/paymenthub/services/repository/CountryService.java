/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Status;
import java.util.List;

/**
 *
 * @author javier
 */
public interface CountryService {

    public void saveOrUpdate(Country country) throws Exception;

    public List<Country> getByStatus(Status status);

    public Country findById(Long id) throws Exception;

    public Country findByCode(String code);

    public Country findByInternationalNumberPhone(String phone);

    public Country getCountryByCurrencyCode(String currencyCode);
}
