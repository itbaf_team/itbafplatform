/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth.filter;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jordonez
 */
@Singleton
@lombok.extern.log4j.Log4j2
public class BasicAuthenticationRequestFilter implements Filter {

    public final static String REDIRECT_URL_404 = "https://planeta.guru";

    @Inject
    public BasicAuthenticationRequestFilter() {

    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {

        filterChain.doFilter(request, response);

        HttpServletResponse resp = (HttpServletResponse) response;
        if (resp.getStatus() == 404) {
            resp.sendRedirect(REDIRECT_URL_404);
        }
    }

    @Override
    public void destroy() {
    }

}
