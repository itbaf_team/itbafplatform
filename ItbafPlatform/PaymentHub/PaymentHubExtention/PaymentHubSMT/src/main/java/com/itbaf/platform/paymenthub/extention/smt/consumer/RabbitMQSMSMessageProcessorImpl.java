/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.smt.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.extention.smt.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.extention.smt.resources.SMSMessage;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQSMSMessageProcessorImpl implements RabbitMQMessageProcessor {

    private final ServiceProcessorHandler serviceProcessor;
    private final ObjectMapper mapper;

    @Inject
    public RabbitMQSMSMessageProcessorImpl(final ObjectMapper mapper,
            final ServiceProcessorHandler serviceProcessor) {
        this.serviceProcessor = Validate.notNull(serviceProcessor, "A ServiceProcessorHandler class must be provided");
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");

    }

    @Override
    public Boolean processMessage(String message) {
        try {
            String tid = Thread.currentThread().getName() + ".smt.sms.mo";
            Thread.currentThread().setName(tid);
        } catch (Exception ex) {
        }

        try {
            SMSMessage notification = mapper.readValue(message, SMSMessage.class);
            return serviceProcessor.processNotifySmsReception(notification, notification.provider);
        } catch (Exception ex) {
            log.error("Error al procesar el SMS: [" + message + "]. " + ex, ex);
        }
        return null;
    }

}
