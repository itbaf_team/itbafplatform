/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.mdivulga.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
@JacksonXmlRootElement(localName = "response")
public class MDivulgaResponse {

    public String status;
    public String description;
    public BodyResponse body;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("status", status)
                .add("description", description)
                .add("body", body)
                .omitNullValues().toString();
    }
}
