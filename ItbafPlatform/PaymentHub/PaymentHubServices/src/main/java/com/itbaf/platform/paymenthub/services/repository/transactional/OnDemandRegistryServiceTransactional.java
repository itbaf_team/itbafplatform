/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPAOnDemandRegistryRepository;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.paymenthub.services.repository.OnDemandRegistryService;
import java.util.List;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class OnDemandRegistryServiceTransactional implements OnDemandRegistryService {

    private final JPAOnDemandRegistryRepository ondemandRegistryRepository;

    @Inject
    public OnDemandRegistryServiceTransactional(final JPAOnDemandRegistryRepository ondemandRegistryRepository) {
        this.ondemandRegistryRepository = Validate.notNull(ondemandRegistryRepository, "A JPAOnDemandRegistryRepository class must be provided");
    }

    @Override
    public void saveOrUpdate(OnDemandRegistry or) throws Exception {
        try {
            ondemandRegistryRepository.merge(or);
        } catch (Exception ex) {
            try {
                ondemandRegistryRepository.save(or);
            } catch (Exception exx) {
                log.error("Error al guardar OnDemandRegistry: [" + or + "]. " + exx, exx);
            }
        }
    }

    @Override
    public OnDemandRegistry findByTransactionId(Long sopId, String transactionId) {
        return ondemandRegistryRepository.findByTransactionId(sopId, transactionId);
    }

    @Override
    public OnDemandRegistry getAvailableByStatusOptin(String userAccout, List<SOP> sops) {
        return ondemandRegistryRepository.getAvailableByUserAccount(userAccout, sops, OnDemandRegistry.Status.OPTIN);
    }

    @Override
    public OnDemandRegistry getAvailableByStatusPending(String userAccout, List<SOP> sops) {
        return ondemandRegistryRepository.getAvailableByUserAccount(userAccout, sops, OnDemandRegistry.Status.PENDING);
    }

    @Override
    public OnDemandRegistry findById(String id) throws Exception {
        return ondemandRegistryRepository.findById(id);
    }
}
