/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.model;

import com.google.cloud.logging.HttpRequest;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.message.Message;
import org.threeten.bp.Duration;

/**
 *
 * @author JF
 */
public class LogMessage implements Message {

    public ConsoleHttpRequest httpRequest;
    public Map<String, Object> parameters = new HashMap();

    public LogMessage(String message) {
        parameters.put("message", message);
    }

    public LogMessage(String message, HttpServletRequest request, HttpServletResponse response, Long durationMillis) {
        String ip;
        ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() < 1) {
            ip = request.getRemoteAddr();
        }
        String referer = null;
        try {
            referer = request.getHeader("referer");
        } catch (Exception ex) {
        }

        String queryString = null;
        try {
            queryString = request.getQueryString();
        } catch (Exception ex) {
        }
        if (queryString == null) {
            queryString = "";
        } else {
            queryString = "?" + queryString;
        }

        httpRequest = new ConsoleHttpRequest();
        httpRequest.setServerIp(request.getServerName());
        httpRequest.setRequestUrl(request.getRequestURL().toString());
        httpRequest.setRequestMethod(request.getMethod());
        httpRequest.setRemoteIp(ip);
        httpRequest.setReferer(referer);
        try {
            if (durationMillis != null && response != null) {
                Duration d = Duration.ofMillis(durationMillis);
                httpRequest.setLatency(d.getSeconds() + "." + d.getNano() + "s");
            }
        } catch (Exception ex) {
        }
        httpRequest.setRequestSize(Long.parseLong(request.getContentLength() + ""));
        httpRequest.setUserAgent(request.getHeader("User-Agent"));

        try {
            if (response != null) {
                httpRequest.setStatus(response.getStatus());
                httpRequest.setResponseSize(Long.parseLong(response.getBufferSize() + ""));
                parameters.put(CommonFunction.HTTP_MESSAGE_TYPE, "httpResponseMessage");
            } else {
                parameters.put(CommonFunction.HTTP_MESSAGE_TYPE, "httpRequestMessage");
            }
        } catch (Exception ex) {
        }
        parameters.put("message", message);
        parameters.put(CommonFunction.HTTP_REQUEST_URL, httpRequest.getRequestUrl() + queryString);
    }

    public LogMessage(String message, ClientRequest cr, ClientResponse response, Long durationMillis) {

        httpRequest = new ConsoleHttpRequest();
        String url = cr.getURI().toString();
        String fullUrl = url;
        if (url.contains("?")) {
            url = url.substring(0, url.indexOf("?"));
        }
        httpRequest.setRequestUrl(url);
        httpRequest.setRequestMethod(cr.getMethod());
        try {
            if (durationMillis != null && response != null) {
                Duration d = Duration.ofMillis(durationMillis);
                httpRequest.setLatency(d.getSeconds() + "." + d.getNano() + "s");
            }
        } catch (Exception ex) {
        }
        try {
            if (response != null) {
                httpRequest.setStatus(response.getStatus());
                httpRequest.setResponseSize(Long.parseLong(response.getLength()+""));
                parameters.put(CommonFunction.HTTP_MESSAGE_TYPE, "httpResponseMessage");
            } else {
                parameters.put(CommonFunction.HTTP_MESSAGE_TYPE, "httpRequestMessage");
            }
        } catch (Exception ex) {
        }
        parameters.put("message", message);
        parameters.put(CommonFunction.HTTP_REQUEST_URL, fullUrl);
    }

    @Override
    public String getFormattedMessage() {
        try {
            return (String) parameters.get("message");
        } catch (Exception ex) {
            try {
                return InstrumentedObject.mapper.writeValueAsString(parameters);
            } catch (Exception exx) {
                return Arrays.toString(parameters.entrySet().toArray());
            }
        }
    }

    @Override
    public String getFormat() {
        return getFormattedMessage();
    }

    @Override
    public Object[] getParameters() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Throwable getThrowable() {
        return null;
    }

}
