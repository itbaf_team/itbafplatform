package com.itbaf.platform.paymenthub.extention.smsc.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import java.math.BigDecimal;
import java.util.Objects;

@lombok.Getter
@lombok.Setter

public class CTR {

    private String idCTR;
    private String msisdn;
    private String fecha;
    private Integer idEvento;
    private String idSuscripcion;
    private Integer idServicio;
    private Integer NC;
    private Integer idCarrier;
    private String msg;
    private BigDecimal precio;
    private Integer idOrigen;
    private Integer reintento;
    private String codTas;
    @JsonProperty("xInfo")
    private String xInfo;

    public CTR() {
    }

    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("idCtr", idCTR)
                .add("msisdn", msisdn)
                .add("fecha", fecha)
                .add("idEvento", idEvento)
                .add("idSuscripcion", idSuscripcion)
                .add("idServicio", idServicio)
                .add("NC", NC)
                .add("idCarrier", idCarrier)
                .add("precio", precio)
                .add("idOrigen", idOrigen)
                .add("reintento", reintento)
                .add("codTas", codTas)
                .add("xInfo", xInfo)
                .add("msg", msg)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCTR, msisdn, fecha, idEvento, idSuscripcion, idServicio, NC, idCarrier, msg, precio, idOrigen, reintento, reintento, codTas, xInfo);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CTR other = (CTR) obj;
        if (!Objects.equals(this.idCTR, other.idCTR)) {
            return false;
        }
        if (!Objects.equals(this.msisdn, other.msisdn)) {
            return false;
        }
        if (!Objects.equals(this.fecha, other.fecha)) {
            return false;
        }
        if (this.idEvento != other.idEvento) {
            return false;
        }
        if (this.idSuscripcion != other.idSuscripcion) {
            return false;
        }
        if (this.idServicio != other.idServicio) {
            return false;
        }
        if (this.NC != other.NC) {
            return false;
        }
        if (this.idCarrier != other.idCarrier) {
            return false;
        }
        if (!Objects.equals(this.msg, other.msg)) {
            return false;
        }
        if (!Objects.equals(this.precio, other.precio)) {
            return false;
        }
        if (this.idOrigen != other.idOrigen) {
            return false;
        }
        if (this.reintento != other.reintento) {
            return false;
        }
        if (!Objects.equals(this.codTas, other.codTas)) {
            return false;
        }
        if (!Objects.equals(this.xInfo, other.xInfo)) {
            return false;
        }
        return true;
    }

}
