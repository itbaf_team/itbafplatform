package com.itbaf.platform.pgp.oauth.api.message;

public class AuthenticationAPI {

    private String username;
    private String password;
    private boolean rememberMe;

    public AuthenticationAPI() {
    }

    public AuthenticationAPI(String username, String password, boolean rememberMe) {
        this.password = password;
        this.rememberMe = rememberMe;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean getRememberMe() {
        return rememberMe;
    }
}
