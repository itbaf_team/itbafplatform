/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties.attributes;

import com.google.common.base.MoreObjects;
import java.io.Serializable;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class OauthProvider implements Serializable {

    private static final long serialVersionUID = 20181207172505L;

    //Google FireBase
    private Object userRecord;
    //Google API
    private Object person;
    //Facebook API
    private Object user;
    //Personal SSO
    private Object userPersonal;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userRecord", userRecord)
                .add("person", person)
                .add("user", user)
                .add("userPersonal", userPersonal)
                .omitNullValues().toString();
    }
}
