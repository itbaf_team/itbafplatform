/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.net.HttpHeaders;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.commons.resources.ClientCache;
import com.itbaf.platform.pgp.commons.resources.CookieFilter;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Client;
import com.itbaf.platform.pgp.model.oauth.properties.session.SessionGP;
import com.itbaf.platform.pgp.services.repository.GenericPropertyService;
import com.itbaf.platform.pgp.services.repository.ProductService;
import com.itbaf.platform.pgp.services.repository.UserService;
import io.jsonwebtoken.impl.DefaultClaims;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.concurrent.TimeUnit;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.ext.Provider;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author jordonez
 */
@Provider
@Singleton
@lombok.extern.log4j.Log4j2
public class ViewRequestFilter implements Filter {

    private final String appName;
    private final ObjectMapper mapper;
    private final UserService userService;
    private final ClientCache clientCache;
    private final GenericPropertyService genericPropertyService;

    /**
     * Cache temporal para la clase PermissionFilter.java
     */
    public static final Cache<String, String> memory = CacheBuilder.newBuilder()
            .expireAfterWrite(15, TimeUnit.SECONDS) //Tiempo de vida
            .build();

    public static String productURL;

    @Inject
    public ViewRequestFilter(@Named("app.name") String appName,
            final ObjectMapper mapper,
            final UserService userService,
            final ClientCache clientCache,
            final ProductService productService,
            final GenericPropertyService genericPropertyService) {
        this.mapper = mapper;
        this.appName = appName;
        this.userService = userService;
        this.clientCache = clientCache;
        this.genericPropertyService = genericPropertyService;

        try {
            productURL = productService.getAll().get(0).getProductSettings().get("product.url");
        } catch (Exception ex) {
            productURL = "https://planeta.guru";
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;

        String code = httpRequest.getParameter("u_code");
        String codeChallenge = httpRequest.getParameter("code_challenge");
        String redirectUri = httpRequest.getParameter("redirect_uri");
        String productId = httpRequest.getParameter("product_id");
        String pt = null;
        DefaultClaims dc = null;

        User client = null;
        User user = null;
        String cookie = null;
        CookieFilter cf;

        //Validamos por cookie
        Cookie[] cooks = httpRequest.getCookies();
        if (cooks != null) {
            String ua = httpRequest.getHeader("User-Agent");
            try {
                for (Cookie c : cooks) {
                    if ("Ax_PG".equals(c.getName())) {
                        cookie = c.getValue();
                        break;
                    }
                }
            } catch (Exception ex) {
                log.error("Error al validar cookie. " + ex, ex);
            }
        }
        if (cookie != null) {
            try {
                byte[] decodedBytes = java.util.Base64.getDecoder().decode(cookie);
                cf = mapper.readValue(new String(decodedBytes), CookieFilter.class);
                if (cf != null) {
                    request.setAttribute(CommonAttribute.U_COOKIE, cf);
                    if (redirectUri == null) {
                        redirectUri = cf.ru;
                        productId = cf.pi;
                    }
                    dc = Client.validateToken(genericPropertyService.getValueByKey("user.sign.secret"), cf.at);
                    if (cf.ui.toString().equals(dc.getIssuer())
                            && cf.ui.toString().equals(Client.validateToken(genericPropertyService.getValueByKey("client.sign.secret"), cf.ut).getIssuer())) {
                        client = userService.findById(cf.ci);
                        user = userService.findById(cf.ui);
                        memory.put(cookie, user.getId().toString());
                        SessionGP sgp = user.getSessionUserProperty().getSessions().get(client.getId().toString());
                        if (sgp == null || sgp.getCookieTokens().get(cf.at) == null) {
                            client = null;
                            user = null;
                            cf.pt = null;
                            dc = null;
                        }
                        if (cf.pt == null || Client.validateToken(genericPropertyService.getValueByKey("client.sign.secret") + user.getCredentialUserProperty().getPassword(), cf.pt) == null) {
                            client = null;
                            user = null;
                        }
                    }
                    if (dc != null) {
                        request.setAttribute(CommonAttribute.U_DEFAULT_CLAIMS, dc);
                    }
                }
            } catch (Exception ex) {
                client = null;
                user = null;
            }
        }

        cookie = null;
        cf = null;

        //Validamos por parametros
        if (code != null && codeChallenge != null && redirectUri != null && client == null && user == null) {
            try {
                String requestToken = clientCache.getTemporalData(code + "_acc_token");
                //El token existe y es valido
                if (requestToken != null && !"xx".equals(requestToken)) {
                    clientCache.setTemporalData(code + "_acc_token", "xx", 15);
                    RequestTokenMessage tm = mapper.readValue(requestToken, RequestTokenMessage.class);
                    dc = Client.validateToken(genericPropertyService.getValueByKey("user.sign.secret"), tm.accessToken);
                    if (getSHA256Hash(tm.codeVerifier).equals(codeChallenge) && dc != null) {
                        client = userService.findById(Long.parseLong(tm.clientId));
                        user = userService.findById(Long.parseLong(tm.userId));
                        if (client != null && user != null) {
                            SessionGP sgp = user.getSessionUserProperty().getSessions().get(client.getId().toString());
                            if (sgp == null || sgp.getCookieTokens().get(tm.accessToken) == null) {
                                cookie = null;
                                user = null;
                                client = null;
                            } else {
                                cf = new CookieFilter();
                                cf.at = tm.accessToken;
                                cf.ut = Client.getNewToken(genericPropertyService.getValueByKey("client.sign.secret"), user, null, 1);
                                cf.ui = user.getId();
                                cf.ci = client.getId();
                                cf.ru = redirectUri;
                                cf.pi = productId;
                                cf.pt = Client.getNewToken(genericPropertyService.getValueByKey("client.sign.secret") + user.getCredentialUserProperty().getPassword(), user, null, 1);
                                request.setAttribute(CommonAttribute.U_COOKIE, cf);
                                request.setAttribute(CommonAttribute.U_DEFAULT_CLAIMS, dc);
                                cookie = mapper.writeValueAsString(cf);
                                cookie = java.util.Base64.getEncoder().withoutPadding().encodeToString(cookie.getBytes());
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                cookie = null;
                user = null;
                client = null;
            }
        }

        if (redirectUri != null && (client == null || user == null)) {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.sendRedirect(redirectUri);
            return;
        }

        if (client == null || user == null) {
            log.warn("Temp View Request - not client or user 401");
            log.warn(client);
            log.warn(user);
            log.warn(code);
            log.warn(codeChallenge);
            log.warn(redirectUri);
            log.warn(productId);
            setReAuthenticate401((HttpServletResponse) response);
            return;
        }

        /*  if (!authorize(client)) {
            setForbidden403((HttpServletResponse) response);
            return;
        }*/
        request.setAttribute(CommonAttribute.PRINCIPAL, client);
        request.setAttribute(CommonAttribute.U_PRINCIPAL, user);
        request.setAttribute(CommonAttribute.U_REDIRECT_URI, redirectUri);
        request.setAttribute(CommonAttribute.PRODUCT_ID, productId);

        if (code != null) {
            memory.put(code, user.getId().toString());
        }

        if (cookie != null) {
            Cookie ce = null;
            try {
                HttpServletResponse httpResponse = (HttpServletResponse) response;
                ce = new Cookie("Ax_PG", cookie);
                ce.setMaxAge(60 * 24 * 60 * 60);
                ce.setPath("/");
                ce.setComment("ACC_PG_TK");

                ce.setDomain(httpRequest.getServerName());
                httpResponse.addCookie(ce);
            } catch (Exception ex) {
                log.error("Error al adicionar cookie: [" + ce + "]. " + ex, ex);
            }
        }

        filterChain.doFilter(request, response);

    }

    private String getSHA256Hash(String data) {

        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data.getBytes("UTF-8"));
            return Base64.encodeBase64URLSafeString(hash); // make it printable
        } catch (Exception ex) {
            log.error("Error al generar SHA256. " + ex, ex);
        }

        return result;
    }

    @Override
    public void destroy() {
    }

    private void setRedirect302(HttpServletResponse response) throws IOException {
        response.sendRedirect(productURL);
    }

    private void setReAuthenticate401(HttpServletResponse response) throws IOException {
        String msg = String.format("Bearer realm=\"%s\"", appName);
        response.setHeader("pg-location", productURL);
        response.setHeader(HttpHeaders.WWW_AUTHENTICATE, msg);
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }

    private void setForbidden403(HttpServletResponse response) throws IOException {
        String msg = String.format("Bearer realm=\"%s\"", appName);
        response.setHeader(HttpHeaders.WWW_AUTHENTICATE, msg);
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
    }

    /**
     * Evalua si tiene los permisos para ejecutar este request
     */
    private boolean authorize(User client) {
        return true;
    }

}
