package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;
import com.google.gson.JsonObject;
import com.mercadopago.resources.datastructures.payment.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Payment {

    public enum OperationType {
        regular_payment,
        money_transfer,
        recurring_payment,
        account_fund,
        payment_addition,
        cellphone_recharge,
        pos_payment
    }

    public enum Status {
        pending,
        approved,
        authorized,
        in_process,
        in_mediation,
        rejected,
        cancelled,
        refunded,
        charged_back
    }

    public enum PaymentTypeId {
        account_money,
        ticket,
        bank_transfer,
        atm,
        credit_card,
        debit_card,
        prepaid_card
    }

    private String id;
    private String site_id;
    private Date date_created;
    private Date date_approved;
    private Date date_last_updated;
    private Date money_release_date;
    private Date last_modified;
    private Integer collector_id;
    private String authorization_code;
    private OperationType operation_type;
    private Payer payer;
    private Boolean binary_mode;
    private Boolean live_mode;
    private Order order;
    private String order_id;
    private String external_reference;
    private String merchant_order_id;
    private String reason;
    private String description;
    private JsonObject metadata;
    private String currency_id;
    private BigDecimal transaction_amount;
    private BigDecimal net_received_amount;
    private BigDecimal transaction_amount_refunded;
    private BigDecimal amount_refunded;
    private BigDecimal shipping_cost;
    private BigDecimal total_paid_amount;
    private BigDecimal coupon_amount;
    private BigDecimal coupon_fee;
    private BigDecimal finance_fee;
    private BigDecimal discount_fee;
    private BigDecimal marketplace_fee;
    private Integer campaign_id;
    private String coupon_code;
    private TransactionDetails transaction_details;
    private ArrayList<FeeDetail> fee_details;
    private Integer differential_pricing_id;
    private BigDecimal application_fee;
    private Status status;
    private String status_detail;
    private Boolean capture;
    private Boolean captured;
    private String call_for_authorizeId;
    private String payment_method;
    private String issuer_id;
    private Integer installment_amount;
    private String deferred_period;
    private PaymentTypeId payment_type;
    private String token;
    private Card card;
    private String statement_descriptor;
    private Integer installments;
    private String notification_url;
    private ArrayList<Refund> refunds;
    private AdditionalInfo additiona_info;
    private String marketplace;
    private String transaction_order_id;
    private Cardholder cardholder;
    private String last_four_digits;
    private String deduction_schema;
    private String last_modified_by_admin;
    private String api_version;
    private String concept_id;
    private BigDecimal concept_amount;
    private Payer collector;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("site_id", site_id)
                .add("date_created", date_created)
                .add("date_approved", date_approved)
                .add("date_last_updated", date_last_updated)
                .add("money_release_date", money_release_date)
                .add("last_modified", last_modified)
                .add("collector_id", collector_id)
                .add("authorization_code", authorization_code)
                .add("operation_type", operation_type)
                .add("payer", payer)
                .add("binary_mode", binary_mode)
                .add("live_mode", live_mode)
                .add("order", order)
                .add("order_id", order_id)
                .add("external_reference", external_reference)
                .add("merchant_order_id", merchant_order_id)
                .add("reason", reason)
                .add("description", description)
                .add("metadata", metadata)
                .add("currency_id", currency_id)
                .add("transaction_amount", transaction_amount)
                .add("net_received_amount", net_received_amount)
                .add("transaction_amount_refunded", transaction_amount_refunded)
                .add("amount_refunded", amount_refunded)
                .add("shipping_cost", shipping_cost)
                .add("total_paid_amount", total_paid_amount)
                .add("coupon_amount", coupon_amount)
                .add("coupon_fee", coupon_fee)
                .add("finance_fee", finance_fee)
                .add("discount_fee", discount_fee)
                .add("marketplace_fee", marketplace_fee)
                .add("campaign_id", campaign_id)
                .add("coupon_code", coupon_code)
                .add("transaction_details", transaction_details)
                .add("fee_details", fee_details)
                .add("differential_pricing_id", differential_pricing_id)
                .add("application_fee", application_fee)
                .add("status", status)
                .add("status_detail", status_detail)
                .add("capture", capture)
                .add("captured", captured)
                .add("call_for_authorizeId", call_for_authorizeId)
                .add("payment_method", payment_method)
                .add("issuer_id", issuer_id)
                .add("installment_amount", installment_amount)
                .add("deferred_period", deferred_period)
                .add("payment_type", payment_type)
                .add("token", token)
                .add("card", card)
                .add("statement_descriptor", statement_descriptor)
                .add("installments", installments)
                .add("notification_url", notification_url)
                .add("refunds", refunds)
                .add("additiona_info", additiona_info)
                .add("marketplace", marketplace)
                .add("transaction_order_id", transaction_order_id)
                .add("cardholder", cardholder)
                .add("last_four_digits", last_four_digits)
                .add("deduction_schema", deduction_schema)
                .add("last_modified_by_admin", last_modified_by_admin)
                .add("api_version", api_version)
                .add("concept_id", concept_id)
                .add("concept_amount", concept_amount)
                .add("collector", collector)
                .omitNullValues().toString();
    }
}
