/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author javier
 */
public abstract class CommonDaemon {

    public Map<String, String> args = new HashMap();

    public final void init(String args[]) throws Exception {
        Thread.currentThread().setName(CommonFunction.getTID("main"));

        java.security.Security.setProperty("networkaddress.cache.ttl", "30");
        java.security.Security.setProperty("networkaddress.cache.negative.ttl", "3");

        this.args.put("guice.profiles.active", "dev");
        this.args.put("guice.instance", "1");
        this.args.put("guice.container", "test-" + System.currentTimeMillis());
        this.args.put("influxdb.disabled", "true");

        if (args != null && args.length > 0) {
            System.out.println("args[" + Arrays.toString(args) + "]");
            try {
                for (String x : args) {
                    if (x != null) {
                        String[] aux = x.split("=");
                        this.args.put(aux[0], aux[1]);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        String aux = System.getenv("APP_ENV");
        if (aux != null) {
            this.args.put("guice.profiles.active", aux);
        }
        aux = System.getenv("INSTANCE_ID");
        if (aux != null) {
            this.args.put("guice.instance", aux);
        }
        aux = System.getenv("HOSTNAME");
        if (aux != null) {
            this.args.put("guice.container", aux);
        }
        aux = System.getenv("INFLUXDB_DISABLED");
        if (aux != null) {
            this.args.put("influxdb.disabled", aux);
        }

        Map<String, String> env = System.getenv();
        for (String key : env.keySet()) {
            if (key.startsWith("META_")) {
                aux = env.get(key);
                if (aux != null) {
                    this.args.put(key, aux);
                }
            }
        }

        this.args.put("guice.profile", this.args.get("guice.profiles.active"));
    }

    public final synchronized Map<String, String> getData() {
        Map<String, String> data = new HashMap();
        data.put("profile", args.get("guice.profile"));
        data.put("instance", args.get("guice.instance"));
        data.put("container", args.get("guice.container"));
        return data;
    }

    public abstract void start() throws Exception;

    public abstract void stop() throws Exception;

}
