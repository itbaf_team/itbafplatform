/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth.api.message;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
public class ResponseBasicMessage {

    public String tid;
    public String username;
    public String location; // AR, CO, BR, MX
    public String country; // AR, CO, BR, MX
    public Integer ifExistsResult; //El usuario existe?
    public Credentials credentials = new Credentials();
    public String redirectUrl;
    public RequestBasicMessage.UsernameType usernameType;
    public String kmsi;
    public String uct;
    public String locale;
    public String path;
    public String html;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("tid", tid)
                .add("usernameType", usernameType)
                .add("username", username)
                .add("location", location)
                .add("country", country)
                .add("ifExistsResult", ifExistsResult)
                .add("credentials", credentials)
                .add("html", html)
                .add("redirectUrl", redirectUrl)
                .add("kmsi", kmsi)
                .add("userCookieToken", uct)
                .add("locale", locale)
                .add("path", path)
                .omitNullValues().toString();
    }
}
