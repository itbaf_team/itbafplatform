/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.smpp;

import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain {

    private final ExecutorService cachedThreadPool;
    private final ScheduledExecutorService scheduledThreadPool;
    private final RabbitMQMessageProcessor rabbitMQMessageProcessor;
    private final RabbitMQConsumer rabbitMQConsumer;
    private final BasicDataBaseConnection dataBaseConnection;
    private final PHInstance instance;

    @Inject
    public PaymentHubMain(final PHInstance instance,
            final ExecutorService cachedThreadPool,
            final ScheduledExecutorService scheduledThreadPool,
            final RabbitMQConsumer rabbitMQConsumer,
            final RabbitMQMessageProcessor rabbitMQMessageProcessor,
            final BasicDataBaseConnection dataBaseConnection) {
        this.dataBaseConnection = Validate.notNull(dataBaseConnection, "A BasicDataBaseConnection class must be provided");
        this.scheduledThreadPool = Validate.notNull(scheduledThreadPool, "A ScheduledExecutorService class must be provided");
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "An ExecutorService class must be provided");
        this.rabbitMQMessageProcessor = Validate.notNull(rabbitMQMessageProcessor, "A RabbitMQMessageProcessor class must be provided");
        this.rabbitMQConsumer = Validate.notNull(rabbitMQConsumer, "A RabbitMQConsumer class must be provided");
        this.instance = Validate.notNull(instance, "A PHInstance class must be provided");
    }

    public void start() {
        log.info("Initializing app... PaymentHubSMPP");
        Set<Provider> providers = instance.getConfigurations().keySet();
        for (Provider p : providers) {
            ChannelQuantityProvider cqp = instance.getConfigurations().get(p);

            if (cqp.getSubscriptionConsumerQuantity() > 0) {
                createConsumer(p.getName() + NOTIFICATION_SUBSCRIPTION, cqp.getSubscriptionConsumerQuantity());
            }
            if (cqp.getBillingConsumerQuantity() > 0) {
                createConsumer(p.getName() + NOTIFICATION_BILLING, cqp.getBillingConsumerQuantity());
            }

        }
        log.info("Initializing app OK. PaymentHubSMPP");
    }

    private RabbitMQConsumer createConsumer(String exchangeName, Integer consumerQuantity) {
        rabbitMQConsumer.createChannel(exchangeName, consumerQuantity, rabbitMQMessageProcessor);
        return rabbitMQConsumer;
    }

    public void stop() {
        rabbitMQConsumer.stopConsumers();
        stopThreads();
        dataBaseConnection.shutdownConnectionsPool();
        log.info("App destroyed. PaymentHubSMPP");
    }

    public void stopThreads() {
        log.info("Finalizando Threads en 5 segundos... ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }
        try {
            log.info("Finalizando scheduledThreadPool ");
            scheduledThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            log.info("Esperando Threads por 2 segundos... ");
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        try {
            scheduledThreadPool.shutdownNow();
            log.info("Finalizado scheduledThreadPool ");
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdownNow();
        } catch (Exception ex) {
        }
    }

}
