/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.persistence.connection;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.inject.Inject;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.persistence.cache.CustomRedisRegionFactory;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author jordoñez
 */
@lombok.extern.log4j.Log4j2
public class BasicDataBaseConnection implements DataBaseConnection {

    private final BasicDBConnectionConfig repositoryConfig;
    private volatile static boolean closeConnection = false;
    private static ScheduledExecutorService schExService;
    private static String appName;

    public static final String PAYMENTHUB_PERSISTENT_UNIT_NAME_DOMAIN = "paymentHub";
    public static final String PGB_PERSISTENT_UNIT_NAME_DOMAIN = "pg_schema";
    public static final String DW_PERSISTENT_UNIT_NAME_DOMAIN = "dw_schema";
    public static final String JUEGOS_PERSISTENT_UNIT_NAME_DOMAIN = "juegos";
    public static final String JUEGOS_V2_PERSISTENT_UNIT_NAME_DOMAIN = "juegos_v2";
    private static final Long MAX_CONNECTION_AGE = 3600000l;//Una hora de vida

    /*Controla el pedido de conexiones sin cerrar*/
    private static final Cache<String, Connection> openConnectionsMemory = CacheBuilder.newBuilder()
            .expireAfterAccess(15, TimeUnit.MINUTES) //Tiempo de vida
            .removalListener(new RemovalListener() {
                @Override
                public void onRemoval(RemovalNotification notification) {
                    closeOpenConnections(notification);
                }
            }).build();

    //Connections<URL,<ID,Connection>>
    private final static Object connectionsToken = new Object();
    private static final Cache<String, Cache<String, Connection>> connections = CacheBuilder.newBuilder()
            .build();

    private static final Cache<String, Long> connectionsCreated = CacheBuilder.newBuilder()
            .expireAfterAccess(2, TimeUnit.HOURS) //Tiempo de vida
            .build();

    @Inject
    public BasicDataBaseConnection(final BasicDBConnectionConfig repositoryConfig) {
        this.repositoryConfig = Validate.notNull(repositoryConfig, "A BasicDBConnectionConfig class must be provided");
        closeConnection = false;
        BasicDataBaseConnection.appName = repositoryConfig.getAppName();
        if (CustomRedisRegionFactory.instrumentedObject == null) {
            log.info("Creando cache de conexiones a la DB");
            for (DBConnectionConfig config : repositoryConfig.getConfig().values()) {
                createDBCache(config.getDbUrl());
            }
            synchronized (openConnectionsMemory) {
                if (schExService == null) {
                    schExService = Executors.newScheduledThreadPool(1);
                    schExService.scheduleWithFixedDelay(new Runnable() {
                        @Override
                        public void run() {
                            Thread.currentThread().setName(System.currentTimeMillis() + " - DBConnection - ");

                            if (closeConnection) {
                                return;
                            }
                            log.debug("Validando conexiones cerradas...");
                            try {
                                openConnectionsMemory.cleanUp();
                            } catch (Exception ex) {
                            }
                            try {
                                for (Cache<String, Connection> aux : connections.asMap().values()) {
                                    aux.cleanUp();
                                    try {
                                    } catch (Exception ex) {
                                        log.error("Error al realizar cleanUp. " + ex, ex);
                                    }
                                }
                            } catch (Exception ex) {
                                log.error("Error al obtener pool de conexiones de memoria. " + ex, ex);
                            }
                        }
                    }, 1, 3, TimeUnit.MINUTES);
                }
            }
        }
    }

    private static void createDBCache(String url) {
        synchronized (connectionsToken) {
            if (connections.getIfPresent(url) == null) {
                Cache<String, Connection> auxConnection = CacheBuilder.newBuilder()
                        .expireAfterAccess(2, TimeUnit.MINUTES) //Tiempo de vida
                        .removalListener(new RemovalListener() {
                            @Override
                            public void onRemoval(RemovalNotification notification) {
                                closeFullConnection(notification);
                            }
                        }).build();
                connections.put(url, auxConnection);
            }
        }
    }

    public boolean isCloseConnection() {
        return closeConnection;
    }

    /**
     * Se debe invocar a este metodo una vez finalizada/detenida la aplicacion
     */
    @Override
    public void shutdownConnectionsPool() {
        closeConnection = true;
        try {
            log.info("Finalizando ScheduledThreadPool DB ");
            schExService.shutdownNow();
            log.info("Cerrando conexiones DB... 4 segundos... ");
            try {
                Thread.sleep(4000);
            } catch (InterruptedException ex) {
                log.error("Error al pausar final. " + ex, ex);
            }
            schExService.shutdown();
            log.info("Finalizado ScheduledThreadPool DB");
        } catch (Exception ex) {
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Set<String> urls = connections.asMap().keySet();
                    for (String url : urls) {
                        try {
                            Cache<String, Connection> aux = connections.getIfPresent(url);
                            if (aux != null) {
                                log.info("[" + url + "] - Pool quantity: ["
                                        + aux.asMap().keySet().toArray().length + "]");
                                aux.invalidateAll();
                            } else {
                                log.warn(url + " pool is NULL.... ");
                            }
                        } catch (Exception ex) {
                            log.error("Error al limpiar memoria en: [" + url + "]. " + ex, ex);
                        }
                    }
                } catch (Exception ex) {
                    log.error("Error al obtener pool de conexiones de memoria. " + ex, ex);
                }
            }
        }).start();

        log.info("Cerrando conexiones DB... 5 segundos... ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            log.error("Error al pausar final. " + ex, ex);
        }
    }

    private Connection getConnectionFromPool(String name, String url) {
        Connection conn = null;

        synchronized (connectionsToken) {
            try {
                Cache<String, Connection> auxCache = connections.getIfPresent(url);
                Set<String> aux = auxCache.asMap().keySet();
                String id = aux.iterator().next();
                conn = auxCache.getIfPresent(id);
                auxCache.invalidate(id);
            } catch (Exception ex) {
            }
            try {
                Long time = connectionsCreated.getIfPresent("conn_" + conn.hashCode());
                //No retorna conexiones con tiempo de creacion mayor a MAX_CONNECTION_AGE
                if (time != null && (System.currentTimeMillis() - time) > MAX_CONNECTION_AGE) {
                    log.info("Cerrando conexion creada hace: [" + time + "]ms");
                    try {
                        conn.close();
                    } catch (Exception ex) {
                    }
                    conn = null;
                }
            } catch (Exception ex) {
            }
        }
        return conn;
    }

    public Connection getDBConnection(String unitNameDomain) {
        Connection conn = null;
        if (closeConnection) {
            log.warn("Se ha invocado la detencion del POOL de conexiones y se pretende obtener una conexion a la DB");
            return conn;
        }

        DBConnectionConfig config = repositoryConfig.getConfig().get(unitNameDomain);
        conn = getConnectionFromPool(unitNameDomain, config.getDbUrl());

        if (conn == null) {
            try {
                conn = getConnection(config.getDbDriver(), config.getDbUrl(),
                        config.getDbUsername(), config.getDbPassword());
            } catch (Exception ex) {
                log.error("Error al cargar el driver DB MySQL. " + ex, ex);
            }
        }

        try {
            if (!conn.isValid(100) || conn.isClosed()) {
                conn = getDBConnection(unitNameDomain);
            }
        } catch (Exception ex) {
        }

        putConnection(conn);
        return conn;
    }

    public Connection getDBPaymentHubConnection() {
        return getDBConnection(PAYMENTHUB_PERSISTENT_UNIT_NAME_DOMAIN);
    }

    public Connection getDBPlanetaGuruConnection() {
        return getDBConnection(PGB_PERSISTENT_UNIT_NAME_DOMAIN);
    }

    public Connection getDBJuegosConnection() {
        return getDBConnection(JUEGOS_PERSISTENT_UNIT_NAME_DOMAIN);
    }

    public Connection getDBDWConnection() {
        return getDBConnection(DW_PERSISTENT_UNIT_NAME_DOMAIN);
    }

    public Connection getDBJuegosV2Connection() {
        return getDBConnection(JUEGOS_V2_PERSISTENT_UNIT_NAME_DOMAIN);
    }

    /*
     ApplicationName - The name of the application currently utilizing the connection
     ClientUser - The name of the user that the application using the connection is performing work for. This may not be the same as the user name that was used in establishing the connection.
     ClientHostname - The hostname of the computer the application using the connection is running on.
     */
    public synchronized Connection getConnection(String clazzForName, String url, String username, String password) {
        return getConnection(clazzForName, url, username, password, 0);
    }

    private Connection getConnection(String clazzForName, String url, String username, String password, int cont) {
        Connection conn = null;
        try {
            Class.forName(clazzForName);
        } catch (ClassNotFoundException ex) {
        }
        Properties jdbcProperties = new Properties();
        jdbcProperties.put("user", username);
        jdbcProperties.put("password", password);

        try {
            String connectionAttributes = "";
            connectionAttributes = connectionAttributes + "session_program:" + appName;
            String aux = ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
            connectionAttributes = connectionAttributes + ",session_osuser:" + aux + "@" + System.getProperty("user.name").toString();
            connectionAttributes = connectionAttributes + ",session_machine:" + InetAddress.getLocalHost().getHostAddress();
            jdbcProperties.put("connectionAttributes", connectionAttributes);
        } catch (Exception ex) {
        }

        try {
            DriverManager.setLoginTimeout(600);
            conn = DriverManager.getConnection(url, jdbcProperties);
        } catch (Exception e) {
            cont++;
            log.warn("getConnection... Reintento: [" + cont + "] " + e);
            if (cont < 4) {
                try {
                    Thread.sleep(700);
                } catch (Exception exx) {
                }
                return getConnection(clazzForName, url, username, password, cont);
            }
            try {
                conn = DriverManager.getConnection(url, jdbcProperties);
            } catch (SQLException ex) {
                try {
                    log.error("No fue posible establecer una conexion a la DB: {" + url + "} - Error: " + ex);
                    InstrumentedObject.commonDaemon.stop();
                } catch (Exception ex1) {
                }
                log.info("Finalizada aplicacion....");
                System.exit(0);
            }
        }
        putConnection(conn);
        return conn;
    }

    public void closeDBConnection(Connection conn, String unitNameDomain) {
        boolean control = false;
        String key = null;
        if (conn == null) {
            return;
        }
        try {
            if (conn != null && !conn.isClosed() && conn.isValid(100)) {
                control = true;
                key = "conn_" + conn.hashCode();
                openConnectionsMemory.invalidate(key);
            }
        } catch (Exception e) {
            log.error("Error al intentar cerrar la conexion. Detalle: " + e, e);
        }
        try {
            if (control) {
                if (isDBConnection(conn, unitNameDomain)) {
                    returnConnection(repositoryConfig.getConfig().get(unitNameDomain).getDbUrl(), key, conn);
                } else {
                    try {
                        log.fatal("La conexion a la DB no corresponde a: [" + unitNameDomain + "][" + conn.getMetaData().getURL() + "]");
                    } catch (Exception ex) {
                        log.fatal("La conexion a la DB no corresponde a: [" + unitNameDomain + "]");
                    }
                    conn.close();
                }
            } else {
                if (conn != null) {
                    conn.close();
                }
            }
        } catch (Exception ex) {
            log.error("Error al cerrar conexion. [" + key + "]. " + ex, ex);
        }
    }

    public void closePaymentHubConnection(Connection conn) {
        closeDBConnection(conn, PAYMENTHUB_PERSISTENT_UNIT_NAME_DOMAIN);
    }

    public void closePlanetaGuruConnection(Connection conn) {
        closeDBConnection(conn, PGB_PERSISTENT_UNIT_NAME_DOMAIN);
    }

    public void closeJuegosConnection(Connection conn) {
        closeDBConnection(conn, JUEGOS_PERSISTENT_UNIT_NAME_DOMAIN);
    }

    public void closeJuegosV2Connection(Connection conn) {
        closeDBConnection(conn, JUEGOS_V2_PERSISTENT_UNIT_NAME_DOMAIN);
    }

    public void closeDWConnection(Connection conn) {
        closeDBConnection(conn, DW_PERSISTENT_UNIT_NAME_DOMAIN);
    }

    private static void returnConnection(String url, String key, Connection conn) {
        synchronized (connectionsToken) {
            try {
                connections.getIfPresent(url).put(key, conn);
            } catch (Exception ex) {
            }
        }
    }

    private static void closeFullConnection(RemovalNotification notification) {
        if (notification.wasEvicted() || closeConnection) {
            String id = (String) notification.getKey();
            Connection conn = (Connection) notification.getValue();

            try {
                // if (conn != null && conn.isValid(500)) {
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                    log.debug("Close Connection. id [" + id + "]");
                }
            } catch (Exception e) {
                log.error("Error al intentar cerrar la conexion. id [" + id + "]. " + e, e);
            }
        }
    }

    private static void closeOpenConnections(RemovalNotification notification) {
        if (notification.wasEvicted() || closeConnection) {
            String key = (String) notification.getKey();
            try {
                Connection conn = (Connection) notification.getValue();

                try {
                    if (conn != null && !conn.isClosed()) {
                        conn.close();
                        log.warn("Hey!!! Habia una conexion a la DB abierta.. buscar en el log: [" + key + "]");
                    }
                } catch (Exception e) {
                    log.error("Error al intentar cerrar la conexion. Hey!!! Habia una conexion a la DB abierta.. buscar en el log: [" + key + "]. " + e, e);
                }
            } catch (Exception ex) {
            }
        }
    }

    public boolean isDBConnection(Connection conn, String unitNameDomain) {
        String url;
        String userName;
        try {
            url = conn.getMetaData().getURL();
            userName = conn.getMetaData().getUserName().toLowerCase();
        } catch (SQLException ex) {
            return false;
        }
        DBConnectionConfig config = repositoryConfig.getConfig().get(unitNameDomain);
        if (config == null) {
            return false;
        }
        return config.getDbUrl().equalsIgnoreCase(url) && userName != null
                && userName.contains(config.getDbUsername().toLowerCase());
    }

    private String putConnection(Connection conn) {
        if (conn != null) {
            try {
                String key = "conn_" + conn.hashCode();
                if (openConnectionsMemory.getIfPresent(key) == null) {
                    openConnectionsMemory.put(key, conn);
                }
                if (connectionsCreated.getIfPresent(key) == null) {
                    connectionsCreated.put(key, System.currentTimeMillis());
                }
            } catch (Exception ex) {
            }
        }
        return "";
    }
}
