
package com.itbaf.platform.paymenthub.extention.store.ws.subscription;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Filter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Filter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nameField" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="exactValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="minValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="maxValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="negation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Filter", propOrder = {
    "nameField",
    "exactValue",
    "minValue",
    "maxValue",
    "negation"
})
public class Filter {

    @XmlElement(required = true)
    protected String nameField;
    @XmlElement(required = true)
    protected String exactValue;
    @XmlElement(required = true)
    protected String minValue;
    @XmlElement(required = true)
    protected String maxValue;
    protected boolean negation;

    /**
     * Gets the value of the nameField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameField() {
        return nameField;
    }

    /**
     * Sets the value of the nameField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameField(String value) {
        this.nameField = value;
    }

    /**
     * Gets the value of the exactValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExactValue() {
        return exactValue;
    }

    /**
     * Sets the value of the exactValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExactValue(String value) {
        this.exactValue = value;
    }

    /**
     * Gets the value of the minValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinValue() {
        return minValue;
    }

    /**
     * Sets the value of the minValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinValue(String value) {
        this.minValue = value;
    }

    /**
     * Gets the value of the maxValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxValue() {
        return maxValue;
    }

    /**
     * Sets the value of the maxValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxValue(String value) {
        this.maxValue = value;
    }

    /**
     * Gets the value of the negation property.
     * 
     */
    public boolean isNegation() {
        return negation;
    }

    /**
     * Sets the value of the negation property.
     * 
     */
    public void setNegation(boolean value) {
        this.negation = value;
    }

}
