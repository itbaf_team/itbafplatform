package com.itbaf.platform.paymenthub.extention.wau.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "response_data")
@JsonInclude(Include.NON_NULL)
public class ResponseSubscriptionStatus {

    @JacksonXmlElementWrapper
    private String result_code; //SVC0021

    @JacksonXmlElementWrapper
    private String result_description; //Subscription has been initiated and optIn pin sent to the user. [ip-172-31-4-30.ec2.internal]

    @JacksonXmlElementWrapper
    private String created_date;

    @JacksonXmlElementWrapper
    private String expiration_date;

    @JacksonXmlElementWrapper
    private String identifier;

    @JacksonXmlElementWrapper
    private String product_id;

    @JacksonXmlElementWrapper
    private String subscription_id;

    @JacksonXmlElementWrapper
    private String subscription_status;

    public ResponseSubscriptionStatus() {
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public String getResult_description() {
        return result_description;
    }

    public void setResult_description(String result_description) {
        this.result_description = result_description;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getSubscription_id() {
        return subscription_id;
    }

    public void setSubscription_id(String subscription_id) {
        this.subscription_id = subscription_id;
    }

    public String getSubscription_status() {
        return subscription_status;
    }

    public void setSubscription_status(String subscription_status) {
        this.subscription_status = subscription_status;
    }

    
}
