
import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { PGMessageRequest, PGMessageResponse, BoHttpClient } from '../utils/bo-http-client';

@Injectable()
export class SubscriptionService extends BoHttpClient {

    component: any;

    constructor(private http: HttpClient) {
        super();
    }

    setComponent(component){
        this.component=component;
    }
    
    responseProcess(response){
        if(this.component){
        this.component.remoteSubscriptionProcess(response.data);}
    }
    
    responseBillingHistoryProcess(response){
        if(this.component){
        this.component.remoteBillingHistoryProcess(response.data);}
    }
    
    getSubscriptions(userAccount, searchProvider): Observable<PGMessageResponse> {
      let pgMessage  = new PGMessageRequest();
      pgMessage.description="getSubscriptions";
      pgMessage.userAccount=userAccount;
      pgMessage.searchProvider=searchProvider;
      return this.http.post<PGMessageResponse>(environment.apiUrl + "/bo/server/rest/api/subscription/search", pgMessage, this.httpOptions)
          .pipe(tap((response: PGMessageResponse) => this.responseProcess(response)),
            catchError(this.handleError<PGMessageResponse>('getSubscriptions'))
          );
    }
    
    deleteSubscription(susbcriptionRegistryId, userAccount): Observable<PGMessageResponse> {
        let pgMessage  = new PGMessageRequest();
        pgMessage.description="deleteSubscription";
        pgMessage.userAccount=userAccount;
        pgMessage.susbcriptionRegistryId=susbcriptionRegistryId;
        return this.http.post<PGMessageResponse>(environment.apiUrl + "/bo/server/rest/api/subscription/delete", pgMessage, this.httpOptions)
            .pipe(tap((response: PGMessageResponse) => this.responseProcess(response)),
              catchError(this.handleError<PGMessageResponse>('deleteSubscription'))
            );
    }
    
    getBillingHistory(userAccount): Observable<PGMessageResponse> {
      let pgMessage  = new PGMessageRequest();
      pgMessage.description="searchBillingHistory";
      pgMessage.userAccount=userAccount;
      return this.http.post<PGMessageResponse>(environment.apiUrl + "/bo/server/rest/api/subscription/billing/history", pgMessage, this.httpOptions)
          .pipe(tap((response: PGMessageResponse) => this.responseBillingHistoryProcess(response)),
            catchError(this.handleError<PGMessageResponse>('searchBillingHistory'))
          );
    }
}
