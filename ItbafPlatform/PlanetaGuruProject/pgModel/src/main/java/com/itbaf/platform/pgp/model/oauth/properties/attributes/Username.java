/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties.attributes;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Provider;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class Username implements Serializable {

    private static final long serialVersionUID = 20181207172506L;

    private String nickname;
    private String avatarURL;
    private Boolean nicknameEdited;
    private Boolean primaryProcessed;
    private Map<String, Email> emails;
    private Map<String, Msisdn> msisdns;
    private Map<String, Email> oldEmails;
    private Map<String, Msisdn> oldMsisdns;
    public static final String DEFAULT_AVATAR_URL = "https://assets.planeta.guru/images/oauth/avatar.png";

    public Username() {
        this.nicknameEdited = false;
        this.emails = new HashMap();
        this.msisdns = new HashMap();
        this.oldMsisdns = new HashMap();
        this.oldEmails = new HashMap();
        this.avatarURL = DEFAULT_AVATAR_URL;
        this.primaryProcessed = false;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("nickname", nickname)
                .add("avatarURL", avatarURL)
                .add("nicknameEdited", nicknameEdited)
                .add("primaryProcessed", primaryProcessed)
                .add("emails", emails)
                .add("msisdns", msisdns)
                .add("oldEmails", oldEmails)
                .add("oldMsisdns", oldMsisdns)
                .omitNullValues().toString();
    }

    /////////// ----- ///////////
    public void setEmail(String email, boolean confirmed) {
        setEmail(new Email(email, confirmed));
    }

    public void setEmail(Email email) {
        if (email != null) {
            getEmails().put(email.getEmail(), email);
        }
    }

    public Email getEmail(String email) {
        return getEmails().get(email);
    }

    /////////// ----- ///////////
    public void setMsisdn(String msisdn, boolean confirmed, Country country, Provider provider) {
        setMsisdn(new Msisdn(msisdn, confirmed, country, provider));
    }

    public void setMsisdn(Msisdn msisdn) {
        if (msisdn != null) {
            getMsisdns().put(msisdn.getMsisdn(), msisdn);
        }
    }

    public Msisdn getMsisdn(String msisdn) {
        msisdn = msisdn.replace("+", "");
        return getMsisdns().get(msisdn);
    }
    /////////// ----- ///////////

    @Override
    public int hashCode() {
        return Objects.hash(nickname, avatarURL, emails, msisdns);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Username other = (Username) obj;
        if (!Objects.equals(this.nickname, other.nickname)) {
            return false;
        }
        if (!Objects.equals(this.avatarURL, other.avatarURL)) {
            return false;
        }
        if (!Objects.equals(this.emails, other.emails)) {
            return false;
        }
        if (!Objects.equals(this.msisdns, other.msisdns)) {
            return false;
        }
        return true;
    }

}
