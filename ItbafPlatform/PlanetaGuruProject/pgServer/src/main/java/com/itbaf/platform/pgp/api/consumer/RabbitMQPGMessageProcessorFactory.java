/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.consumer;

import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessorFactory;

/**
 *
 * @author javier
 * @param <M>
 */
public interface RabbitMQPGMessageProcessorFactory extends RabbitMQMessageProcessorFactory <PGMessageProcessor>{

}
