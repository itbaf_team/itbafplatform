package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Address {

    private String zip_code;
    private String street_name;
    private Integer street_number;
    private String neighborhood;
    private String city;
    private String federal_unit;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("zip_code", zip_code)
                .add("street_name", street_name)
                .add("street_number", street_number)
                .add("neighborhood", neighborhood)
                .add("city", city)
                .add("federal_unit", federal_unit)
                .omitNullValues().toString();
    }
}
