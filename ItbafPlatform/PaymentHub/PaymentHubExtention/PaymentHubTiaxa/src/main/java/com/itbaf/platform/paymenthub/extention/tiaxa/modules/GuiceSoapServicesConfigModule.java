package com.itbaf.platform.paymenthub.extention.tiaxa.modules;

import com.google.inject.Singleton;
import com.google.inject.servlet.ServletModule;
import com.itbaf.platform.commons.filter.RequestFilter;
import com.itbaf.platform.paymenthub.extention.tiaxa.soap.NotificationSoapService;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import java.util.HashMap;

public class GuiceSoapServicesConfigModule extends ServletModule {

    @Override
    protected void configureServlets() {

        HashMap<String, String> options = new HashMap();
        options.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

        //Filters
        this.filter("/soap/*").through(RequestFilter.class);
        //Servlets
        serve("/soap/*").with(GuiceContainer.class, options);
        
        bind(NotificationSoapService.class).in(Singleton.class);
    }
}
