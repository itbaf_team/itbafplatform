package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class AddressReceiver extends Address {

    private String floor;
    private String apartment;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("floor", floor)
                .add("apartment", apartment)
                .omitNullValues().toString();
    }
}
