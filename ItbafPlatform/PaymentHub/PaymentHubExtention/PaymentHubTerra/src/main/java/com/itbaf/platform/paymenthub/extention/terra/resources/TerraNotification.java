/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.terra.resources;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.google.common.base.MoreObjects;
import java.util.Date;

/**
 *
 * @author JF
 */
@JacksonXmlRootElement(localName = "terraData")
public class TerraNotification {

    @JacksonXmlElementWrapper(localName = "fecha")
    public String fecha;
    @JacksonXmlElementWrapper(localName = "idservicio")
    public String idservicio;
    @JacksonXmlElementWrapper(localName = "idsusc")
    public String idsusc;
    @JacksonXmlElementWrapper(localName = "movil_anterior")
    public String movil_anterior;
    @JacksonXmlElementWrapper(localName = "movil")
    public String movil;
    @JacksonXmlElementWrapper(localName = "operador")
    public String operador;
    @JacksonXmlElementWrapper(localName = "oper_id")
    public String oper_id;
    @JacksonXmlElementWrapper(localName = "idtrx")
    public String idtrx;
    @JacksonXmlElementWrapper(localName = "idnotificacion")
    public String idnotificacion;
    @JacksonXmlElementWrapper(localName = "precio")
    public String precio;
    @JacksonXmlElementWrapper(localName = "origen")
    public String origen;
    @JacksonXmlElementWrapper(localName = "evento")
    public String evento;
    @JacksonXmlElementWrapper(localName = "plataforma")
    public String plataforma;
    @JacksonXmlElementWrapper(localName = "keyword")
    public String keyword;
    @JacksonXmlElementWrapper(localName = "tipousuario")
    public String tipousuario;
    @JacksonXmlElementWrapper(localName = "status")
    public String status;

    public Date fechaLocale;
    public String countryCode;
    public String thread = Thread.currentThread().getName();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("fecha", fecha)
                .add("idservicio", idservicio)
                .add("idsusc", idsusc)
                .add("movil_anterior", movil_anterior)
                .add("movil", movil)
                .add("operador", operador)
                .add("oper_id", oper_id)
                .add("idtrx", idtrx)
                .add("idnotificacion",idnotificacion)
                .add("precio", precio)
                .add("origen", origen)
                .add("evento", evento)
                .add("plataforma", plataforma)
                .add("keyword", keyword)
                .add("tipousuario", tipousuario)
                .add("status", status)
                .add("fechaLocale", fechaLocale)
                .add("countryCode", countryCode)
                .add("thread", thread)
                .omitNullValues().toString();
    }
}
