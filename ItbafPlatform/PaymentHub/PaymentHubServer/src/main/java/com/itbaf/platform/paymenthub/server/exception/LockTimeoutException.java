/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.server.exception;

/**
 *
 * @author JF
 */
public class LockTimeoutException extends Exception {
    
    public LockTimeoutException(String message) {
        super(message);
    }
    
}
