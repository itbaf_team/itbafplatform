package com.itbaf.platform.paymenthub.extention.bn.modules.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.commons.bn.model.BorderNotificationMessage;
import com.itbaf.platform.paymenthub.extention.bn.handler.ServiceProcessorHandler;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.paymenthub.commons.bn.model.TimweNotification;
import com.itbaf.platform.paymenthub.extention.bn.model.timwe.PartnerNotifResponse;
import java.io.IOException;
import java.util.ArrayList;

@Path("/timwe")
@Singleton
@lombok.extern.log4j.Log4j2
public class TimweNotificationRestService {

    private final ObjectMapper mapper;
    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public TimweNotificationRestService(final ObjectMapper mapper,
            ServiceProcessorHandler serviceProcessorHandler) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
    }

    private PartnerNotifResponse getPartnerNotifResponseOK(TimweNotification notification) {
        PartnerNotifResponse pnr = new PartnerNotifResponse();
        pnr.code = "SUCCESS";
        pnr.inError = false;
        pnr.message = "SUCCESS";
        pnr.requestId = notification.getExternal_tx_id();

        pnr.responseData = null;
        /*
        if (notification.getTransactionUUID() != null) {
            pnr.responseData.add(notification.getTransactionUUID());
        }
        if (notification.getExternal_tx_id() != null) {
            pnr.responseData.add(notification.getExternal_tx_id());
        }
         */
        return pnr;
    }

    private PartnerNotifResponse getPartnerNotifResponseERROR(TimweNotification notification) {
        if (notification == null) {
            notification = new TimweNotification();
        }
        PartnerNotifResponse pnr = getPartnerNotifResponseOK(notification);
        pnr.code = "INTERNAL_ERROR";
        pnr.inError = true;
        pnr.message = "INTERNAL_ERROR";

        return pnr;
    }

    private TimweNotification getNotification(String json, String notificationType,
            String partnerRole, String countryCode, String telco, HttpServletRequest request) throws IOException {
        TimweNotification notification = mapper.readValue(json.replaceFirst("[{]", "{\"topic\":\"TimweNotification\","), TimweNotification.class);
        notification.setNotificationType(notificationType);
        notification.setPartnerRole(partnerRole);
        notification.setProvider(countryCode.toLowerCase() + "." + telco.toLowerCase() + ".timwe");
        notification.setExternal_tx_id(request.getHeader("external-tx-id"));
        notification.setAuthentication(request.getHeader("authentication"));
        notification.setApikey(request.getHeader("apikey"));

        return notification;
    }

    @POST
    @Path("/{country}/{telco}/notification/{notificationType}/{partnerRole}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscriptionNotification(final @PathParam("country") String countryCode,
            final @PathParam("telco") String telco,
            final @PathParam("notificationType") String notificationType,
            final @PathParam("partnerRole") String partnerRole,
            final String json, @Context HttpServletRequest request) {

        log.info("Notificacion de Timwe. CountryCode: [" + countryCode + "] - Telco: [" + telco + "] - NotificationType: [" + notificationType + "] - Notification: [" + json + "]");

        TimweNotification notification = null;
        try {
            notification = getNotification(json, notificationType, partnerRole, countryCode, telco, request);
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.telco = telco;
            nm.notification = notification;
            serviceProcessorHandler.processNotification(notification.getProvider(), nm);
            return Response.status(Status.OK).entity(getPartnerNotifResponseOK(notification)).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al procesar Notificacion de Timwe. CountryCode: [" + countryCode + "] - Telco: [" + telco + "] - NotificationType: [" + notificationType + "] - Notification: [" + json + "]. " + ex, ex);
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(getPartnerNotifResponseERROR(notification)).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/{country}/{telco}/notification/mt/dn/{partnerRole}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response mtDeliveryNotification(final @PathParam("country") String countryCode,
            final @PathParam("telco") String telco,
            final @PathParam("partnerRole") String partnerRole,
            final String json, @Context HttpServletRequest request) {

        log.info("Notificacion de Timwe. CountryCode: [" + countryCode + "] - Telco: [" + telco + "] - NotificationType: [mt-dn] - Notification: [" + json + "]");
        TimweNotification notification = null;
        try {
            notification = getNotification(json, "mt-dn", partnerRole, countryCode, telco, request);

            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.telco = telco;
            nm.notification = notification;
            serviceProcessorHandler.processNotification(notification.getProvider(), nm);
            return Response.status(Status.OK).entity(getPartnerNotifResponseOK(notification)).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al procesar Notificacion de Timwe. CountryCode: [" + countryCode + "] - Telco: [" + telco + "] - NotificationType: [mt-dn] - Notification: [" + notification + "]. " + ex, ex);
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(getPartnerNotifResponseERROR(notification)).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/{country}/{telco}/{realm}/charge/dob/{partnerRole}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response directBillingcharge(final @PathParam("country") String countryCode,
            final @PathParam("telco") String telco,
            final @PathParam("realm") String realm,
            final @PathParam("partnerRole") String partnerRole,
            final String json, @Context HttpServletRequest request) {

        log.info("Notificacion de Timwe. CountryCode: [" + countryCode + "] - Telco: [" + telco + "] - Realm: [" + realm + "] - NotificationType: [charge-dob] - Notification: [" + json + "]");
        TimweNotification notification = null;
        try {
            notification = getNotification(json, "charge-dob", partnerRole, countryCode, telco, request);
            notification.setRealm(realm);

            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.telco = telco;
            nm.notification = notification;
            serviceProcessorHandler.processNotification(notification.getProvider(), nm);
            return Response.status(Status.OK).entity(getPartnerNotifResponseOK(notification)).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al procesar Notificacion de Timwe. CountryCode: [" + countryCode + "] - Telco: [" + telco + "] - Realm: [" + realm + "] - NotificationType: [charge-dob] - Notification: [" + notification + "]. " + ex, ex);

        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(getPartnerNotifResponseERROR(notification)).type(MediaType.APPLICATION_JSON).build();
    }
}
