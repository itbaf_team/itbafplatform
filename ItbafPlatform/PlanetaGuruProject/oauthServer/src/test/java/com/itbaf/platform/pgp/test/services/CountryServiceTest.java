package com.itbaf.platform.pgp.test.services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.pgp.test.instance.GuiceInstance;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Language;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.paymenthub.services.repository.CountryService;
import com.itbaf.platform.pgp.test.services.resources.CountryList;
import com.itbaf.platform.pgp.test.services.resources.CountryRest;
import com.itbaf.platform.pgp.test.services.resources.LanguageRest;
import java.io.IOException;
import java.util.HashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author JF
 */
public class CountryServiceTest {

    private CountryService countryService;
    private RequestClient requestClient;
    private ObjectMapper mapper;

    public CountryServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        countryService = GuiceInstance.getInstance().getInstance(CountryService.class);
        requestClient = GuiceInstance.getInstance().getInstance(RequestClient.class);
        mapper = GuiceInstance.getInstance().getInstance(ObjectMapper.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    @Ignore
    public void getCountryByCode() {

        Country country = countryService.findByCode("AM");

        assertNotNull(country);
        assertTrue("AM".equalsIgnoreCase(country.getCode()));
        System.out.println(country);
    }

    @Test
    @Ignore
    public void getCountryByInternationalMSISDN() {
        String msisdn = "+541121845870";
        Country country = countryService.findByInternationalNumberPhone(msisdn);

        assertNotNull(country);
        assertTrue("AR".equalsIgnoreCase(country.getCode()));
    }

    @Test
    @Ignore
    public void updateCountries() throws IOException {

        String json = requestClient.requestJsonGet("https://restcountries.eu/rest/v2/all");
        json = "{\"countryList\":" + json + "}";

        CountryList cl = mapper.readValue(json, CountryList.class);

        for (CountryRest cr : cl.countryList) {
            Country c = countryService.findByCode(cr.alpha2Code);
            if (c == null) {
                System.out.println("No existe... Country con code: [" + cr.alpha2Code + "]");
                c = new Country();
                c.setCode(cr.alpha2Code);
                c.setCurrency(cr.currencies.get(0).code);
                c.setStatus(Status.DISABLE);
                c.setName(cr.name);
                c.setNativeView(true);
            }
            if (cr.languages != null) {
                if (c.getLanguages() == null) {
                    c.setLanguages(new HashSet());
                }
                for (LanguageRest lr : cr.languages) {
                    Language l = new Language();
                    if (lr.iso639_1 == null) {
                        l.setCode(lr.iso639_2);
                    } else {
                        l.setCode(lr.iso639_1);
                    }
                    l.setCode3(lr.iso639_2);
                    l.setName(lr.name);
                    l.setNativeName(lr.nativeName);
                    if (l.getCode() != null) {
                        c.getLanguages().add(l);
                    }
                }
            }

            c.setNativeName(cr.nativeName);
            c.setSymbolCurrency(cr.currencies.get(0).symbol);
            try {
                countryService.saveOrUpdate(c);

                System.out.println("");
            } catch (Exception ex) {
                System.out.println("Error al guardar Country: [" + c + "]. " + ex);
                ex.printStackTrace();
                break;
            }
        }

        /*  Country country = countryService.findByInternationalNumberPhone(msisdn);

        assertNotNull(country);
        assertTrue("AR".equalsIgnoreCase(country.getCode()));*/
    }
}
