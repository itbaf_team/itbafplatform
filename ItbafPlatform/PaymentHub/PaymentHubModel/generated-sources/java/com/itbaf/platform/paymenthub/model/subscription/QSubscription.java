package com.itbaf.platform.paymenthub.model.subscription;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSubscription is a Querydsl query type for Subscription
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSubscription extends EntityPathBase<Subscription> {

    private static final long serialVersionUID = -179280026L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSubscription subscription = new QSubscription("subscription");

    public final StringPath externalCustomer = createString("externalCustomer");

    public final StringPath externalUserAccount = createString("externalUserAccount");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    protected com.itbaf.platform.paymenthub.model.QSOP sop;

    public final EnumPath<Subscription.Status> status = createEnum("status", Subscription.Status.class);

    public final DateTimePath<java.util.Date> subscriptionDate = createDateTime("subscriptionDate", java.util.Date.class);

    protected QSubscriptionRegistry subscriptionRegistry;

    public final DateTimePath<java.util.Date> unsubscriptionDate = createDateTime("unsubscriptionDate", java.util.Date.class);

    public final StringPath userAccount = createString("userAccount");

    public QSubscription(String variable) {
        this(Subscription.class, forVariable(variable), INITS);
    }

    public QSubscription(Path<? extends Subscription> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSubscription(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSubscription(PathMetadata metadata, PathInits inits) {
        this(Subscription.class, metadata, inits);
    }

    public QSubscription(Class<? extends Subscription> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sop = inits.isInitialized("sop") ? new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"), inits.get("sop")) : null;
        this.subscriptionRegistry = inits.isInitialized("subscriptionRegistry") ? new QSubscriptionRegistry(forProperty("subscriptionRegistry"), inits.get("subscriptionRegistry")) : null;
    }

    public com.itbaf.platform.paymenthub.model.QSOP sop() {
        if (sop == null) {
            sop = new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"));
        }
        return sop;
    }

    public QSubscriptionRegistry subscriptionRegistry() {
        if (subscriptionRegistry == null) {
            subscriptionRegistry = new QSubscriptionRegistry(forProperty("subscriptionRegistry"));
        }
        return subscriptionRegistry;
    }

}

