package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPASmsSopMultipleRepository;
import com.itbaf.platform.paymenthub.services.repository.SmsSopMultipleService;
import org.apache.commons.lang3.Validate;


@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class SmsSopMultipleServiceTransactional implements SmsSopMultipleService {
    private final JPASmsSopMultipleRepository smsSopMulpipleRepository;

    @Inject
    public SmsSopMultipleServiceTransactional(final JPASmsSopMultipleRepository smsSopMulpipleRepository) {
        this.smsSopMulpipleRepository = Validate.notNull(smsSopMulpipleRepository, "A JPASmsSopMultipleRepository class must be provided");;
    }
    @Override
    public boolean isMultipleMessage(Long sop_id){
        Boolean isMultipleSms = false;
        try {
            isMultipleSms = smsSopMulpipleRepository.isMultipleMessage(sop_id);
        } catch (Exception ex) {
            log.info("No se pudo consultar si el sop tiene multiples mensajes: ["+ ex.getCause().getCause().getMessage() +"]");
        }

        return isMultipleSms;
    }
}
