package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;
import java.util.Date;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class AdditionalInfoPayer {

    private String first_name;
    private String last_name;
    private Phone phone;
    private Address address;
    private Date registration_date;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("first_name", first_name)
                .add("last_name", last_name)
                .add("phone", phone)
                .add("address", address)
                .add("registration_date", registration_date)
                .omitNullValues().toString();
    }
}
