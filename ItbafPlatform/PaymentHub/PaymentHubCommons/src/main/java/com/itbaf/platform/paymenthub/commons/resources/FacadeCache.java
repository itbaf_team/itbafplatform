/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.resources;

import com.google.inject.Inject;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import java.util.concurrent.TimeUnit;
import org.redisson.api.RMapCache;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class FacadeCache {

    @Inject
    public FacadeCache() {
    }

    public void setFacadeRequest(final String externalId, final String userAccount,
            final String externalUserAccount, final Long sopId, final FacadeRequest facadeRequest) {

        if (facadeRequest == null) {
            return;
        }

        RMapCache<String, FacadeRequest> map = InstrumentedObject.redis.getMapCache("facadeRequestMapCacheJF");

        String key = null;
        try {
            if (userAccount != null) {
                key = userAccount + "_" + sopId;
                map.put(key, facadeRequest, 18, TimeUnit.HOURS);
            }
        } catch (Exception ex) {
            log.warn("Error setFacadeRequest Cache. [" + key + "] " + ex, ex);
        }
        try {
            if (externalId != null) {
                key = externalId + "_" + sopId;
                map.put(key, facadeRequest, 18, TimeUnit.HOURS);
            }
        } catch (Exception ex) {
            log.warn("Error setFacadeRequest Cache. [" + key + "] " + ex, ex);
        }
        try {
            if (externalUserAccount != null) {
                key = externalUserAccount + "_" + sopId;
                map.put(key, facadeRequest, 18, TimeUnit.HOURS);
            }
        } catch (Exception ex) {
            log.warn("Error setFacadeRequest Cache. [" + key + "] " + ex, ex);
        }
    }

    public FacadeRequest getFacadeRequest(final String externalId, final String userAccount,
            final String externalUserAccount, final Long sopId) {

        RMapCache<String, FacadeRequest> map = InstrumentedObject.redis.getMapCache("facadeRequestMapCacheJF");
        String key;
        FacadeRequest value = null;
        if (externalId != null) {
            key = externalId + "_" + sopId;
            value = map.get(key);
            if (value != null) {
                return value;
            }
        }
        if (userAccount != null) {
            key = userAccount + "_" + sopId;
            value = map.get(key);
            if (value != null) {
                return value;
            }
        }
        if (externalUserAccount != null) {
            key = externalUserAccount + "_" + sopId;
            value = map.get(key);
            if (value != null) {
                return value;
            }
        }

        key = "externalId: " + externalId + " - userAccount: " + userAccount + " - externalUserAccount: " + externalUserAccount + " - sopId: " + sopId;
        log.info("No se encontro un valor de FacadeRequest en el cache para: [" + key + "]  ");
        return value;

    }

    public void setTemporalData(final String key, final String data, long ttlMinutes) {
        if (key == null || data == null) {
            return;
        }

        RMapCache<String, String> map = InstrumentedObject.redis.getMapCache("temporalDataCache");
        log.info("setTemporalData Cache: [" + key + "][" + data + "]");
        map.put(key, data, ttlMinutes, TimeUnit.MINUTES);
    }

    public String getTemporalData(final String key) {

        RMapCache<String, String> map = InstrumentedObject.redis.getMapCache("temporalDataCache");
        String data = map.get(key);

        log.info("getTemporalData Cache: [" + key + "][" + data + "]");
        return data;
    }
}
