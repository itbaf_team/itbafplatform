/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.modules;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.modules.GuiceDefaultConfigModule;
import com.itbaf.platform.paymenthub.commons.bn.BNProcessor;
import com.itbaf.platform.paymenthub.commons.sync.RabbitMQSyncMessageProcessorImpl;
import com.itbaf.platform.paymenthub.commons.sync.ServiceProcessor;
import com.itbaf.platform.paymenthub.extention.timwe.handler.ATTMxServiceProcessorHandler;
import com.itbaf.platform.paymenthub.extention.timwe.handler.BNProcessorHandler;
import com.itbaf.platform.paymenthub.extention.timwe.handler.EntelPeServiceProcessorHandler;
import com.itbaf.platform.paymenthub.extention.timwe.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.repository.module.GuiceRepositoryConfigModule;
import com.itbaf.platform.paymenthub.services.module.GuiceServiceConfigModule;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import com.itbaf.platform.rmq.module.GuiceRabbitMQConfigModule;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author jordonez
 */
public class GuiceConfigModule extends GuiceDefaultConfigModule {

    public GuiceConfigModule(final Map<String, String> args, CommonDaemon commonDaemon) {
        super(args, commonDaemon);
    }

    @Override
    protected void configure() {
        super.configure();

        //Binds
        bind(BNProcessor.class).to(BNProcessorHandler.class);
        bind(ServiceProcessor.class).to(EntelPeServiceProcessorHandler.class); //Para mantener compatibilidad
        bind(RabbitMQMessageProcessor.class).to(RabbitMQSyncMessageProcessorImpl.class);

        final Multibinder<ServiceProcessorHandler> multi = Multibinder.newSetBinder(binder(), ServiceProcessorHandler.class);
        multi.addBinding().to(EntelPeServiceProcessorHandler.class).asEagerSingleton();
        multi.addBinding().to(ATTMxServiceProcessorHandler.class).asEagerSingleton();

        //Modulos de aplicaciones
        install(new GuiceRepositoryConfigModule());
        install(new GuiceServiceConfigModule());    
        install(new GuiceRabbitMQConfigModule());
        install(new GuiceRestServicesConfigModule());

        log.info("Configuracion iniciada. " + Arrays.toString(args.entrySet().toArray()));
    }

    @Provides
    @Singleton
    Map<String, ServiceProcessorHandler> serviceProcessorHandlerMap(Set<ServiceProcessorHandler> servicesHandler) {
        Map<String, ServiceProcessorHandler> map = new HashMap();

        for (ServiceProcessorHandler sph : servicesHandler) {
            map.put(sph.provider.toLowerCase(), sph);
        }

        return map;
    }
}
