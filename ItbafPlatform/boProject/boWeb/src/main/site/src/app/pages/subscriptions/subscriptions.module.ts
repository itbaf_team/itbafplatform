import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { SubscriptionsRoutingModule } from './subscriptions-routing.module';
import { SubscriptionsComponent } from './subscriptions.component';
import { SearchModule } from './search/search.module';


const components = [
  SubscriptionsComponent,
]; 

@NgModule({
  imports: [
    ThemeModule,
    SubscriptionsRoutingModule,
    SearchModule,
  ],
  declarations: [
    ...components,
  ],
  entryComponents: [
  ],
})
export class SubscriptionsModule { }
