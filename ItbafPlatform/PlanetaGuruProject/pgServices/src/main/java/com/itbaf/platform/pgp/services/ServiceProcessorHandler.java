/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage;
import com.itbaf.platform.messaging.rest.oauth.ResponseAuthMessage;
import com.itbaf.platform.paymenthub.commons.sync.NormalizerService;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Language;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.paymenthub.services.repository.CountryService;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import com.itbaf.platform.pgp.commons.api.message.udi.UdiMessage;
import com.itbaf.platform.pgp.commons.resources.ClientCache;
import com.itbaf.platform.pgp.model.Product;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Friend;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.oauth.properties.session.SessionGP;
import com.itbaf.platform.pgp.model.oauth.properties.session.Token;
import com.itbaf.platform.pgp.model.wallet.WalletUser;
import com.itbaf.platform.pgp.model.wallet.properties.Credit;
import com.itbaf.platform.pgp.services.repository.GenericPropertyService;
import com.itbaf.platform.pgp.services.repository.UserService;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.impl.TextCodec;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import org.apache.commons.codec.binary.Base64;
import org.redisson.api.RLock;
import com.itbaf.platform.pgp.services.repository.LocaleService;
import com.itbaf.platform.pgp.services.repository.ProductService;
import com.itbaf.platform.pgp.services.repository.WalletUserService;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Random;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public abstract class ServiceProcessorHandler extends NormalizerService {

    @Inject
    private @Named("guice.profile")
    String profile;
    @Inject
    protected ObjectMapper mapper;
    @Inject
    private ClientCache clientCache;
    @Inject
    protected UserService userService;
    @Inject
    protected RequestClient requestClient;
    @Inject
    protected LocaleService localeService;
    @Inject
    protected ProductService productService;
    @Inject
    protected CountryService countryService;
    @Inject
    protected ExecutorService cachedThreadPool;
    @Inject
    protected WalletUserService walleUserService;
    @Inject
    protected InstrumentedObject instrumentedObject;
    @Inject
    protected SubscriptionService subscriptionService;
    @Inject
    protected GenericPropertyService genericPropertyService;
    @Inject
    private PHProfilePropertiesService phprofilePropertiesService;

    public final User authenticateUser(User client, String userToken, DefaultClaims defaultClaims) {
        return userService.authenticateUser(client, userToken, defaultClaims);
    }

    public final <T extends Object> T readObjectFromJson(String json, Class<T> valueType) {
        try {
            return mapper.readValue(json, valueType);
        } catch (Exception ex) {
            log.error("Error al procesar JSON: [" + json + "]. " + ex, ex);
        }
        return null;
    }

    protected User funUsers(List<User> users) {
        User user = null;

        for (User u : users) {
            if (u.getId() != null) {
                user = u;
            }
            if (u.getPrincipal() == null && u.getId() != null) {
                user = u;
                break;
            }
        }
        if (user == null) {
            log.error("funUsers. Debe existir un USER con ID valido.");
            return null;
        }
        if (user.getCredentialUserProperty().getUsername().getEmails() == null) {
            user.getCredentialUserProperty().getUsername().setEmails(new HashMap());
        }
        if (user.getCredentialUserProperty().getUsername().getMsisdns() == null) {
            user.getCredentialUserProperty().getUsername().setMsisdns(new HashMap());
        }
        if (user.getUsers() == null) {
            user.setUsers(new HashSet());
        }

        RLock locku = instrumentedObject.lockObject("wallet_" + user.getId(), 30);
        WalletUser wu = walleUserService.findById(user.getId());
        if (wu == null) {
            // log.info("No existe un WalletUser: [" + user.getId() + "]");
            wu = new WalletUser();
            wu.setId(user.getId());
        }
        /*else {
            log.info("WalletUser: [" + wu + "]");
        }*/

        for (User u : users) {
            if (user.getId().equals(u.getId())) {
                continue;
            }
            if (u.getProfileUserProperty().isProcessed()) {
                continue;
            }
            if (u.getRole() != null && user.getRole() == null) {
                user.setRole(u.getRole());
            }

            u.getProfileUserProperty().setProcessed(true);
            if (u.getCredentialUserProperty().getUsername().getEmails() != null
                    && !u.getCredentialUserProperty().getUsername().getEmails().isEmpty()) {
                user.getCredentialUserProperty().getUsername().getEmails().putAll(u.getCredentialUserProperty().getUsername().getEmails());
                u.getCredentialUserProperty().getUsername().getEmails().clear();
            }
            if (u.getCredentialUserProperty().getUsername().getMsisdns() != null
                    && !u.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                user.getCredentialUserProperty().getUsername().getMsisdns().putAll(u.getCredentialUserProperty().getUsername().getMsisdns());
                u.getCredentialUserProperty().getUsername().getMsisdns().clear();
            }
            if (u.getProfileUserProperty().getFriends() != null && !u.getProfileUserProperty().getFriends().isEmpty()) {
                for (Friend f : u.getProfileUserProperty().getFriends().values()) {
                    try {
                        User uf = userService.findById(f.getUserId());
                        while (uf.getPrincipal() != null) {
                            uf = uf.getPrincipal();
                        }
                        Long lf = f.getUserId();
                        f.setUserId(uf.getId());
                        if (Status.ACTIVE.equals(f.getStatus()) && !f.getUserId().equals(user.getId())
                                && user.getProfileUserProperty().getFriends().get(f.getUserId()) == null) {
                            user.getProfileUserProperty().getFriends().put(f.getUserId(), f);
                            if (uf.getProfileUserProperty().getFriends().get(user.getId()) == null) {
                                Friend aux = new Friend();
                                aux.setStatus(Status.ACTIVE);
                                aux.setUserId(user.getId());
                                uf.getProfileUserProperty().getFriends().put(aux.getUserId(), aux);
                            }
                        }
                        if (!lf.equals(f.getUserId())) {
                            uf.getProfileUserProperty().getFriends().remove(lf);
                        }
                        if (!Status.ACTIVE.equals(f.getStatus())) {
                            uf.getProfileUserProperty().getFriends().remove(f.getUserId());
                        }
                        userService.saveOrUpdate(uf);
                    } catch (Exception ex) {
                        log.error("Error al procesar Friend: [" + f + "]. " + ex, ex);
                    }
                }
                user.getProfileUserProperty().getFriends().putAll(u.getProfileUserProperty().getFriends());
                u.getProfileUserProperty().getFriends().clear();
            }
            if (u.getId() != null) {
                u.setPrincipal(user);
                user.getUsers().add(u);
            }
            if (u.getCredentialUserProperty().getUsername().getNickname() != null && user.getCredentialUserProperty().getUsername().getNickname() == null) {
                user.getCredentialUserProperty().getUsername().setNickname(u.getCredentialUserProperty().getUsername().getNickname());
                u.getCredentialUserProperty().getUsername().setNickname(null);
            }

            if (u.getProfileUserProperty().getAddress() != null && user.getProfileUserProperty().getAddress() == null) {
                user.getProfileUserProperty().setAddress(u.getProfileUserProperty().getAddress());
            }

            if (u.getProfileUserProperty().getAgeRange() != null && user.getProfileUserProperty().getAgeRange() == null) {
                user.getProfileUserProperty().setAgeRange(u.getProfileUserProperty().getAgeRange());
            }

            if (u.getProfileUserProperty().getBirthdate() != null && user.getProfileUserProperty().getBirthdate() == null) {
                user.getProfileUserProperty().setBirthdate(u.getProfileUserProperty().getBirthdate());
            }

            if (u.getProfileUserProperty().getGender() != null && user.getProfileUserProperty().getGender() == null) {
                user.getProfileUserProperty().setGender(u.getProfileUserProperty().getGender());
            }

            if (u.getProfileUserProperty().getLanguage() != null && user.getProfileUserProperty().getLanguage() == null) {
                user.getProfileUserProperty().setLanguage(u.getProfileUserProperty().getLanguage());
            }

            if (u.getProfileUserProperty().getLastName() != null && user.getProfileUserProperty().getLastName() == null) {
                user.getProfileUserProperty().setLastName(u.getProfileUserProperty().getLastName());
            }

            if (u.getProfileUserProperty().getMiddleName() != null && user.getProfileUserProperty().getMiddleName() == null) {
                user.getProfileUserProperty().setMiddleName(u.getProfileUserProperty().getMiddleName());
            }

            if (u.getProfileUserProperty().getFirstName() != null && !"User".equals(u.getProfileUserProperty().getFirstName())
                    && (user.getProfileUserProperty().getFirstName() == null || "User".equals(u.getProfileUserProperty().getFirstName()))) {
                user.getProfileUserProperty().setFirstName(u.getProfileUserProperty().getFirstName());
            }

            if (u.getProfileUserProperty().getState() != null && user.getProfileUserProperty().getState() == null) {
                user.getProfileUserProperty().setState(u.getProfileUserProperty().getState());
            }

            if (u.getProfileUserProperty().getZip() != null && user.getProfileUserProperty().getZip() == null) {
                user.getProfileUserProperty().setZip(u.getProfileUserProperty().getZip());
            }

            if (u.getProfileUserProperty().getOccupation() != null && user.getProfileUserProperty().getOccupation() == null) {
                user.getProfileUserProperty().setOccupation(u.getProfileUserProperty().getOccupation());
            }

            if (u.getProfileUserProperty().getSiteURL() != null && user.getProfileUserProperty().getSiteURL() == null) {
                user.getProfileUserProperty().setSiteURL(u.getProfileUserProperty().getSiteURL());
            }

            if (u.getProfileUserProperty().isUserPhoto() && !user.getProfileUserProperty().isUserPhoto()) {
                user.getProfileUserProperty().setPhotoURL(u.getProfileUserProperty().getPhotoURL());
            }
            ///////////////////////
            if (u.getId() != null) {
                RLock lock = instrumentedObject.lockObject("wallet_" + u.getId(), 30);
                WalletUser wuu = walleUserService.findById(u.getId());
                if (wuu == null) {
                    log.info("No existe un WalletUser: [" + u.getId() + "]");
                } else if (wuu.getCredits() != null && !wuu.getCredits().isEmpty()) {
                    try {
                        Set<String> keys = wuu.getCredits().keySet();
                        for (String key : keys) {
                            Credit cuu = wuu.getCredits().get(key);
                            Credit cu = wu.getCredits().get(key);
                            if (cu != null) {
                                Set<String> vkeys = cuu.getValues().keySet();
                                for (String vkey : vkeys) {
                                    BigDecimal vuu = cuu.getValues().get(vkey);
                                    BigDecimal vu = cu.getValues().get(vkey);
                                    if (vu != null) {
                                        vuu = vuu.add(vu);
                                    }
                                    cu.getValues().put(vkey, vuu);
                                }
                            } else {
                                wu.getCredits().put(key, cuu);
                            }
                        }
                        wuu.getCredits().clear();
                        walleUserService.saveOrUpdate(wuu);
                    } catch (Exception ex) {
                        log.error("Error al actualizar walletUser: [" + wuu + "]. " + ex, ex);
                    }
                }
                instrumentedObject.unLockObject(lock);
            }
            ///////////////////////

            try {
                if (u.getId() != null) {
                    userService.saveOrUpdate(u);
                }
            } catch (Exception ex) {
                log.error("Error al actualizar usuario: [" + u + "]. " + ex, ex);
            }
        }
        try {
            walleUserService.saveOrUpdate(wu);
        } catch (Exception ex) {
            log.error("Error al actualizar walletUser principal: [" + wu + "]. " + ex, ex);
        }
        instrumentedObject.unLockObject(locku);

        try {
            userService.saveOrUpdate(user);
        } catch (Exception ex) {
            log.error("Error al actualizar usuario principal: [" + user + "]. " + ex, ex);
        }

        return user;
    }

    /**
     * Genera un codigo de validacion de lenght caracteres para enviar al
     * usuario
     */
    protected String generateCode(int lenght) {

        String code;
        int num1 = 18;
        int num2 = 126;
        String key = "";
        for (int i = 1; i <= 15; i++) {
            int x = (int) Math.floor(Math.random() * (num2 - num1) + num1);
            key = key + (char) x;
        }
        code = TextCodec.BASE64.encode(key.getBytes()).substring(0, lenght);
        code = code.replace("+", "J").replace("\\", "F").replace("\\.", "j").replace("/", "f");
        return code;
    }

    /**
     * Genera un codigo de validacion de lenght digitos para enviar al usuario
     */
    protected String generateDigitCode(int lenght) {
        Random rand = new Random();
        int div = (int) Math.pow(10, lenght);
        String code = (rand.nextInt(div - 1)) + "";
        while (code == null || code.length() < lenght) {
            code = (System.currentTimeMillis() % div) + "";
        }

        return code;
    }

    protected final String getSHA256Hash(String data) {

        String result = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data.getBytes("UTF-8"));
            return Base64.encodeBase64URLSafeString(hash);
        } catch (Exception ex) {
            log.error("Error al generar SHA256. [" + data + "]. " + ex, ex);
        }

        return result;
    }

    /**
     * Internacionalizacion basica.
     *
     * @param i18n en-US
     * @param providerId
     * @param html
     * @return
     */
    public String i18n(String i18n, String providerId, String html) {

        String key = i18n + "_xx_" + providerId + "_" + html;
        Object auxs = MainCache.memory12Hours().getIfPresent(key);
        if (auxs != null) {
            return (String) auxs;
        }

        try {
            i18n = i18n.replace("_", "-");
            String[] aux = i18n.split("-");
            i18n = aux[0].toLowerCase();
            i18n = i18n + "-" + aux[1].toUpperCase();
        } catch (Exception ex) {
        }
        html = html.replace("<locale/>", i18n);
        html = html.replace("<year/>", Calendar.getInstance().get(Calendar.YEAR) + "");

        html = localeService.i18n(i18n, providerId, html);
        MainCache.memory12Hours().put(key, html);

        return html;
    }

    protected final String readHeaderResource(String path, User client, User user, String productId) {
        String html = readResource("/tpl/html/header.html", client, productId);
        String adminSiteDisplay = "display:none !important";
        String adminSiteURL = "";

        if (user != null && user.getRole() != null && user.getRole().getPermissions() != null) {
            adminSiteDisplay = "";
            adminSiteURL = genericPropertyService.getValueByKey("bo.web.url") + "/bo/admin/";
        }

        html = html.replace("<adminSiteDisplay/>", adminSiteDisplay).replace("<adminSiteURL/>", adminSiteURL);

        return html;
    }

    protected final String readResource(String path, User client, String productId) {
        Map<String, String> prop = new HashMap();
        prop.put("clientId", client.getCredentialUserProperty().getClient().getClientId());
        prop.put("productId", productId);
        return readResource(path, prop);
    }

    protected final String readResource(String path, Map<String, String> prop) {
        String clientId = prop.get("clientId");
        Long productId = Long.parseLong(prop.get("productId"));
        User client = userService.findByclientId(clientId);
        while (client.getPrincipal() != null) {
            client = client.getPrincipal();
        }
        Product pr = null;
        for (Product p : client.getProducts()) {
            if (p.getId().equals(productId)) {
                prop.put("oauth.name", p.getName());
                prop.put("oauth.code", p.getProductSettings().get("oauth.code"));
                pr = p;
                break;
            }
        }
        String html = "";
        Object obj = MainCache.memory48Hours().getIfPresent("readResource_" + path + "_" + clientId + "_" + productId);
        if (obj == null) {
            try ( BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(path), "UTF-8"))) {
                String aux;
                while ((aux = br.readLine()) != null) {
                    html = html + aux;
                }
                if (html != null && pr != null) {
                    html = html.replace("<pglogo/>", pr.getProductSettings().get("oauth.logo"))
                            .replace("jf-pgbackground-jf", pr.getProductSettings().get("oauth.image.background"))
                            .replace("</tmg>", pr.getProductSettings().get("oauth.gtm"));
                    MainCache.memory48Hours().put("readResource_" + path + "_" + clientId + "_" + productId, html);
                }
                return html;
            } catch (Exception ex) {
            }
        } else {
            return (String) obj;
        }

        return html;
    }

    protected final String readResource(String path) {
        String html = "";
        Object obj = MainCache.memory48Hours().getIfPresent("readResource_" + path);
        if (obj == null) {
            try ( BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(path), "UTF-8"))) {
                String aux;
                while ((aux = br.readLine()) != null) {
                    html = html + aux;
                }
                if (html != null) {
                    MainCache.memory48Hours().put("readResource_" + path, html);
                }
                return html;
            } catch (Exception ex) {
            }
        } else {
            return (String) obj;
        }

        return html;
    }

    /**
     * Lista de productos del menu
     */
    protected final String getMenuProducts(String html, String productId, String redirectUri) {

        //Lista de productos
        try {
            String products = "";
            String redirectProductName = null;

            List<Product> productList = productService.getAll();
            int i = 0;
            for (Product p : productList) {
                i++;
                String url = p.getProductSettings().get("product.url");
                if (redirectUri != null && p.getId().toString().equals(productId)) {
                    url = redirectUri;
                    redirectProductName = p.getProductSettings().get("oauth.code");
                }
                products = products + "<li><a id=\"shellmenu_" + i + "\" class=\"js-subm-uhf-nav-link\" href=\"" + url + "\">" + p.getName() + "</a></li>";
            }
            html = html.replace("<product-item/>", products);
            if (redirectProductName != null) {
                html = html.replace("<returnToPage/>", "<i18n>i18n.ix.px.account.header.return." + redirectProductName.toLowerCase().trim() + "</i18n>").
                        replace("<returnToPageURL/>", redirectUri);
            }
        } catch (Exception ex) {
        }

        return html;
    }

    protected final String getYears() {
        try {
            Object obj = MainCache.memory24Hours().getIfPresent("years_view");
            if (obj == null) {
                int year = Calendar.getInstance().get(Calendar.YEAR) - 4;
                String years = "";
                for (int i = year; i >= year - 100; i--) {
                    years = years + "</option><option value=\"" + i + "\">" + i + "</option>";
                }
                MainCache.memory24Hours().put("years_view", years);
                return years;
            }
            return (String) obj;
        } catch (Exception ex) {
        }
        return "";
    }

    /**
     * Inhabilita el o los tokens de usuario
     *
     * @param tm
     * @return
     */
    public ResponseAuthMessage deleteUserToken(RequestTokenMessage tm) {
        ResponseAuthMessage rm = new ResponseAuthMessage();

        if (tm.clientId == null || tm.accessToken == null || tm.scope == null) {
            log.warn("Alguno de los parametros es nulo. clientId: [" + tm.clientId + "] - accessToken: [" + tm.accessToken + "] - scope: [" + tm.scope + "]");
            rm.error = ResponseAuthMessage.Error.invalid_request;
            return rm;
        }
        User client = userService.findByclientId(tm.clientId);
        rm.error = ResponseAuthMessage.Error.invalid_client;
        if (client != null) {
            User user = authenticateUser(client, tm.accessToken, null);
            if (user != null) {
                //  log.info("User: [" + user.getId() + "] - Dando de baja el token: [" + tm.accessToken + "] para scope: [" + tm.scope + "]");
                SessionGP sgp = user.getSessionUserProperty().getSessions().get(client.getId().toString());
                if (sgp != null) {
                    boolean control = false;
                    switch (tm.scope) {
                        case SLO:
                            Token tc = sgp.getCookieTokens().remove(tm.accessToken);
                            if (tc != null) {
                                removeSLOToken(tc.getToken(), client, user);
                            }
                            control = true;
                            break;
                        case SLOC:
                            List<Token> cookieTokens = new ArrayList();
                            cookieTokens.addAll(sgp.getCookieTokens().values());

                            for (Token t : cookieTokens) {
                                removeSLOToken(t.getToken(), client, user);
                            }
                            if (!sgp.getCookieTokens().isEmpty()) {
                                log.warn("No deberia haber ingresado aca...");
                                sgp.getCookieTokens().clear();
                                sgp.getUserTokens().clear();
                            }
                            control = true;
                            break;
                        case SSO:
                            user.getSessionUserProperty().getSessions().clear();
                            control = true;
                            break;
                    }

                    if (control) {
                        try {
                            userService.saveOrUpdate(user);
                            rm.message = "OK";
                            rm.error = null;
                        } catch (Exception ex) {
                            log.error("Error al actualizar user: [" + user + "]. " + ex, ex);
                        }
                    }
                }
            }
        }
        return rm;
    }

    public final String getUdiURL() {
        return genericPropertyService.getCommonProperty("udi.url.product.default");
    }

    public final String getUdiURL(String productId) {
        String url = genericPropertyService.getCommonProperty("udi.url.product." + productId);
        if (url == null) {
            log.error("No existe una URL de UDI configurada para: [udi.url.product." + productId + "]. ");
            url = getUdiURL();
        }
        return url;
    }

    private void removeSLOToken(String cookieToken, User client, User user) {
        SessionGP sgp;
        User auxClient = client;
        while (auxClient.getPrincipal() != null) {
            auxClient = auxClient.getPrincipal();
        }
        Token ut;
        sgp = user.getSessionUserProperty().getSessions().get(auxClient.getId().toString());
        if (sgp != null) {
            ut = sgp.getUserTokens().remove(cookieToken);
            if (ut != null) {
                sgp.getCookieTokens().remove(ut.getToken());
            }
        }

        if (auxClient.getUsers() != null) {
            for (User ac : auxClient.getUsers()) {
                sgp = user.getSessionUserProperty().getSessions().get(ac.getId().toString());
                if (sgp != null) {
                    ut = sgp.getUserTokens().remove(cookieToken);
                    if (ut != null) {
                        sgp.getCookieTokens().remove(ut.getToken());
                    }
                }
            }
        }
    }

    protected final String getFrecuency(String aux) {
        if (aux == null) {
            return aux;
        }
        switch (aux) {
            case "days":
                return "<i18n>i18n.ix.px.text.daily</i18n>";
            case "week":
                return "<i18n>i18n.ix.px.text.weekly</i18n>";
            case "month":
                return "<i18n>i18n.ix.px.text.monthly</i18n>";
            case "year":
                return "<i18n>i18n.ix.px.text.yearly</i18n>";
        }
        return "";
    }

    public final String getLanguage(String ip) {
        String lang = "es";
        if (ip == null) {
            return lang;
        }

        try {
            Country c = getCountryByIP(ip);
            if (c != null && !c.getLanguages().isEmpty()) {
                if (c.getLanguages().size() > 1) {
                    Set<Language> aux = new TreeSet();
                    aux.addAll(c.getLanguages());
                    lang = aux.iterator().next().getCode().toLowerCase().trim() + "-" + c.getCode().toUpperCase().trim();
                } else {
                    lang = c.getLanguages().iterator().next().getCode().toLowerCase().trim() + "-" + c.getCode().toUpperCase().trim();
                }
            }

        } catch (Exception ex) {
        }
        return lang;
    }

    public final String getCountryCodeByIP(String ip) {
        String code = clientCache.getTemporalData(ip + "_ip");
        if (code == null) {
            try {
                String json = requestClient.requestJsonGet(getUdiURL() + "/ip/" + ip);
                if (json != null) {
                    code = mapper.readValue(json, UdiMessage.class).geo.country.iso_code;
                }
            } catch (Exception ex) {
            }
            if (code == null) {
                try {
                    String json = requestClient.requestJsonGet("https://ipinfo.io/" + ip);
                    FacadeRequest fr = mapper.readValue(json, FacadeRequest.class);
                    code = fr.country;
                } catch (Exception ex) {
                }
            }
            if (code != null) {
                clientCache.setTemporalData(ip + "_ip", code, 350);
            }
        }
        return code;
    }

    public final Country getCountryByIP(String ip) {
        Country c = null;
        try {
            String code = getCountryCodeByIP(ip);
            if (code != null) {
                c = countryService.findByCode(code);
            }
        } catch (Exception ex) {
        }
        return c;
    }

    protected void syncSubscriptionsRun(User user) {
        final String th = Thread.currentThread().getName();
        cachedThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setName(th + ".async.subscr." + user.getId());
                RLock lock = instrumentedObject.lockObject("USER_SYNC_" + user.getId(), 10);
                long aux = instrumentedObject.getAtomicLong("USER_SYNC_A_" + user.getId(), 5);
                if (aux == 0) {
                    instrumentedObject.getAndIncrementAtomicLong("USER_SYNC_A_" + user.getId(), 5);
                    instrumentedObject.getAndIncrementAtomicLong("USER_SYNC_A_" + user.getId(), 5);
                    try {
                        if (!user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                            if (user.getCredentialUserProperty().getUsername().getMsisdns().size() > 1) {
                                Map<String, Msisdn> map = new HashMap();
                                for (Msisdn m : user.getCredentialUserProperty().getUsername().getMsisdns().values()) {
                                    if (m.getCountry() != null) {
                                        String localLength = phprofilePropertiesService.getCommonProperty("msisdn." + m.getCountry().getCode().toLowerCase() + ".length.local");
                                        if (localLength != null) {
                                            String msisdn = m.getMsisdn();
                                            Integer msisdnLengthLocal = Integer.parseInt(localLength);
                                            String ani = m.getCountry().getCode() + msisdn.substring(Math.max(0, msisdn.length() - msisdnLengthLocal), msisdn.length());
                                            Msisdn tt = map.get(ani);
                                            if (tt == null || m.getConfirmed()) {
                                                map.put(ani, m);
                                            } else {
                                                user.getCredentialUserProperty().getUsername().setPrimaryProcessed(Boolean.FALSE);
                                                if (m.getConfirmed()) {
                                                    map.put(ani, m);
                                                }
                                            }
                                        }
                                    }
                                }
                                user.getCredentialUserProperty().getUsername().getMsisdns().clear();
                                for (Msisdn m : map.values()) {
                                    user.getCredentialUserProperty().getUsername().getMsisdns().put(m.getMsisdn(), m);
                                }
                                try {
                                    userService.saveOrUpdate(user);
                                } catch (Exception ex) {
                                }
                            }

                            List<Msisdn> msisdns = new ArrayList();
                            msisdns.addAll(user.getCredentialUserProperty().getUsername().getMsisdns().values());
                            Map<String, String> data = new HashMap();
                            for (Msisdn m : msisdns) {
                                if (m.getCountry() != null) {
                                    data.put(m.getMsisdn(), m.getCountry().getCode());
                                    log.info("Data SYNC " +data);
                                }
                            }
                            if (!data.isEmpty()) {
                                subscriptionService.getSubscriptionCountryFacadeRequest(data);
                            }
                        }
                        //   log.info("Estado sincronizado.");
                    } catch (Exception ex) {
                    }
                }
                instrumentedObject.unLockObject(lock);
            }
        });
    }

    public String filterCaractersToLowerCase(String word) {
        if (word != null) {
            word = word.trim().toLowerCase().replace("á", "a").replace("é", "e").replace("í", "i").replace("ó", "o").replace("ú", "u");
        }

        return word;
    }
}
