/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgp.commons.resources.ClientCache;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ApiAccountServiceProcessorHandler extends ServiceProcessorHandler {

    private final ClientCache clientCache;

    @Inject
    public ApiAccountServiceProcessorHandler(final ClientCache clientCache) {
        this.clientCache = Validate.notNull(clientCache, "A ClientCache class must be provided");
    }

    public PGMessageResponse getUserAccountToken(RequestTokenMessage tm, User client, User user) throws Exception {
        PGMessageResponse rm = new PGMessageResponse();

        if (tm.codeVerifier == null) {
            log.warn("Alguno de los parametros es nulo. codeChallenge: [" + tm.codeVerifier + "]");
            rm.data.message = "Parametros invalidos. Forzar el inicio de sesion de usuario. ";
            rm.meta.code = 400;
            rm.meta.status = Meta.Status.invalid_request;
            return rm;
        }

        String ccode = client.getId() + "_" + user.getId() + "_" + generateCode(20);
        tm.clientId = client.getId().toString();
        tm.userId = user.getId().toString();
        clientCache.setTemporalData(ccode + "_acc_token", mapper.writeValueAsString(tm), 2);

        rm.meta.code = 200;
        rm.meta.status = Meta.Status.ok;
        rm.data.message = tm.grantType.toString();
        rm.data.object = mapper.readTree("{\"u_code\":\"" + ccode + "\"}");

        return rm;
    }

}
