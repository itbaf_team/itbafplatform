
var host = window.location.protocol + "//" + window.location.host;
var canonical = $('link[rel=canonical]')[0].href;
var router = new Navigo(host, false);

!function () {
    router.on({'/account/transactions': transactionsInfo,
        '/pg/account/transactions': transactionsInfo
    }).resolve();
}();


function transactionsInfo(params, query, isSelect) {
    if (!isSelect) {
        $("#trans-ti").selectmenu({
            change: function (event, ui) {
                var params = {};
                params.userId = uparams.userId;
                params.result = ui.item.value;
                $.ajax({
                    type: "POST",
                    url: canonical + "/orders",
                    contentType: 'application/json; charset=UTF-8',
                    data: JSON.stringify(params),
                    success: function (data) {

                        if (data && data.data && data.data.object) {
                            uparams.transactions = data.data.object.transactions;
                            transactionsInfo(null, null, true);
                            dataTableStart();
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    },
                    dataType: "json"
                });

            }
        });
    }
    var html = tmpl($("#transactionsModuleScript").html(), {"params": uparams});
    $("#transactions-module").html(html);

    $.each(uparams.transactions, function (index, value) {
        $("#viewId" + value.tid).data("j", value);
    });

    var dialog = $("#dialogTrans").dialog({
        autoOpen: false,
        //  height: 400,
        width: $(window).width() * 0.9,
        maxWidth: 550,
        modal: true,
        resizable: false,
        draggable: true,
        position: {my: "center", at: "center", of: window},
        buttons: {
            Close: function () {
                $(this).dialog("close");
            }
        },
        open: function (event, ui) {
            var mw = $(this).dialog("option", "maxWidth");
            var w = $(window).width() * 0.9;

            if (w > mw) {
                w = mw;
            }

            $(this).dialog("option", "width", w);
        },
        close: function (event, ui) {
            uparams.isOpen = false;
        }
    });

    var height = $(window).height();
    var Width = $(window).width();
    uparams.isOpen = false;

    // responsive width & height
    var resize = function () {
        if ($(window).width() !== Width && uparams.isOpen) {
            Width = $(window).width();
            dialog.dialog("close");
            dialog.dialog("open");
            uparams.isOpen = true;
        }
    };

    // resize on window resize
    $(window).on("resize", function () {
        resize();
    });

    // resize on orientation change
    if (window.addEventListener) {  // Add extra condition because IE8 doesn't support addEventListener (or orientationchange)
        window.addEventListener("orientationchange", function () {
            resize();
        });
    }
}

function viewDialog(e, tid) {
    e.preventDefault();
    var value = $("#" + tid).data("j");
    $("#transaction").html(value.tid);
    $("#date").html(value.date);
    $("#type").html(value.typeI8N);
    var amount = (value.symbol?value.symbol:'$')+" 0.00";
    if (value.amount) {
        amount = (value.symbol?value.symbol+" ":'')+value.amount + " " + value.currency;
    }
    $("#charged").html(" " + amount);
    $("#credits").html((value.type === 'PURCHASE' ? "-" : "+") + value.localAmount + ".00 " + value.localCurrency);
    $("#description").html(value.description);
    
    uparams.isOpen = true;
    $("#dialogTrans").dialog("open");
}

function redirectTransaction(e, ru) {
    e.preventDefault();
    var url = ru;
    if (!ru) {
        url = canonical;
    }
    window.location = url;
}