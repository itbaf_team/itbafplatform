/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties.session;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class SessionGP implements Serializable {

    private static final long serialVersionUID = 20181207172701L;

    private Long clientId;
    @Deprecated
    private List<Token> tokens;
    /**
     * <cookieToken,userToken>
     */
    private Map<String, Token> userTokens;
    /**
     * <userToken,cookieToken>
     */
    private Map<String, Token> cookieTokens;

    private Date createdDate;//Fecha creacion de la session
    private boolean active;//El cliente puede utilizar este token?

    public SessionGP() {
        userTokens = new HashMap();
        cookieTokens = new HashMap();
        createdDate = new Date();
        active = true;
    }
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("clientId", clientId)
                .add("createdDate", createdDate)
                .add("active", active)
                .add("tokens", tokens)
                .add("userTokens", userTokens)
                .add("cookieTokens", cookieTokens)
                .omitNullValues().toString();
    }

    @Deprecated
    public List<Token> getTokens() {
        return null;
    }

    @Deprecated
    public void setTokens(List<Token> tokens) {
        this.tokens = null;
    }

}
