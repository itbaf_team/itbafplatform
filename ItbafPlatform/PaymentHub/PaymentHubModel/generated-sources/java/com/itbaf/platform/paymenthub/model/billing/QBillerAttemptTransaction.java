package com.itbaf.platform.paymenthub.model.billing;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBillerAttemptTransaction is a Querydsl query type for BillerAttemptTransaction
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBillerAttemptTransaction extends EntityPathBase<BillerAttemptTransaction> {

    private static final long serialVersionUID = -1774938080L;

    public static final QBillerAttemptTransaction billerAttemptTransaction = new QBillerAttemptTransaction("billerAttemptTransaction");

    public final com.itbaf.platform.paymenthub.model.QBaseEntity _super = new com.itbaf.platform.paymenthub.model.QBaseEntity(this);

    public final SimplePath<Object> event = createSimple("event", Object.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> sopId = createNumber("sopId", Long.class);

    public final NumberPath<Long> subscriptionRegistryId = createNumber("subscriptionRegistryId", Long.class);

    public final StringPath transactionId = createString("transactionId");

    public QBillerAttemptTransaction(String variable) {
        super(BillerAttemptTransaction.class, forVariable(variable));
    }

    public QBillerAttemptTransaction(Path<? extends BillerAttemptTransaction> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBillerAttemptTransaction(PathMetadata metadata) {
        super(BillerAttemptTransaction.class, metadata);
    }

}

