/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.model;

import com.itbaf.platform.commons.resources.InstrumentedObject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.message.Message;

/**
 *
 * @author JF
 */
public class FunnelLogMessage implements Message {

    public ConsoleHttpRequest httpRequest;
    public Map<String, Object> parameters = new HashMap();

    public FunnelLogMessage(String message, String path, Object facadeRequest) {
        Map<String, Object> context = new HashMap();
        parameters.put("message",message);
        parameters.put("context", context);
        context.put("path", path);
        context.put("facade_request", facadeRequest);
    }

    @Override
    public String getFormattedMessage() {
        try {
            return (String) parameters.get("message")+" "+parameters.get("context");
        } catch (Exception ex) {
            try {
                return InstrumentedObject.mapper.writeValueAsString(parameters);
            } catch (Exception exx) {
                return Arrays.toString(parameters.entrySet().toArray());
            }
        }
    }

    @Override
    public String getFormat() {
        return getFormattedMessage();
    }

    @Override
    public Object[] getParameters() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Throwable getThrowable() {
        return null;
    }

}
