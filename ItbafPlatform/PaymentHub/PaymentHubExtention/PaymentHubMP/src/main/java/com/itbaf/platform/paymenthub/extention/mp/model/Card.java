package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;
import java.util.Date;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Card {

    private Number id;
    private String last_four_digits;
    private String first_six_digits;
    private Integer expiration_year;
    private Integer expiration_month;
    private Date date_created;
    private Date date_last_updated;
    private Cardholder cardholder;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("last_four_digits", last_four_digits)
                .add("first_six_digits", first_six_digits)
                .add("expiration_year", expiration_year)
                .add("expiration_month", expiration_month)
                .add("date_created", date_created)
                .add("date_last_updated", date_last_updated)
                .add("cardholder", cardholder)
                .omitNullValues().toString();
    }
}
