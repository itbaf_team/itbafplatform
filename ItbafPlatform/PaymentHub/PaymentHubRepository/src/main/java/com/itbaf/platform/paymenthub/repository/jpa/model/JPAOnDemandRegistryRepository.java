/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.model.ondemand.QOnDemandRegistry;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.querydsl.jpa.impl.JPAQuery;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

//Para validar activos subscription.subscriptionDate.dayOfMonth().mod(10).eq(7)
/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAOnDemandRegistryRepository extends AbstractJPARepositorySession<OnDemandRegistry, String> {

    @Inject
    public JPAOnDemandRegistryRepository(final javax.inject.Provider<EntityManager> entityManagerProvider) {
        super(OnDemandRegistry.class, entityManagerProvider);
    }

    public OnDemandRegistry getAvailableByUserAccount(String userAccout, List<SOP> sops, OnDemandRegistry.Status status) {
        if (userAccout == null || sops == null || sops.isEmpty()) {
            String error = "Uno de los parametros es nulo!! userAccout: [" + userAccout + "] - sops: [" + sops + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        OnDemandRegistry s = null;
        QOnDemandRegistry ondemandRegistry = QOnDemandRegistry.onDemandRegistry;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<Long> ids = new ArrayList();
        for (SOP sop : sops) {
            ids.add(sop.getId());
        }

        List<OnDemandRegistry> sl = query.select(ondemandRegistry).from(ondemandRegistry).
                where(ObjectPredicates.combine(ondemandRegistry.userAccount.eq(userAccout),
                        ondemandRegistry.sop().id.in(ids),
                        ondemandRegistry.status.eq(status)))
                .orderBy(ondemandRegistry.ondemandDate.desc())
                .fetch();

        if (sl != null && sl.size() > 0) {
            s = sl.get(0);
        }

        return s;
    }

    public OnDemandRegistry findByTransactionId(Long sopId, String transactionId) {
        if (transactionId == null || sopId == null) {
            String error = "Uno de los parametros es nulo!! transactionId: [" + transactionId + "] - sop: [" + sopId + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        OnDemandRegistry s = null;
        QOnDemandRegistry ondemandRegistry = QOnDemandRegistry.onDemandRegistry;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<OnDemandRegistry> sl = query.select(ondemandRegistry).from(ondemandRegistry).
                where(ObjectPredicates.combine(ondemandRegistry.transactionId.eq(transactionId),
                        ondemandRegistry.sop().id.eq(sopId)))
                .fetch();

        if (sl != null && sl.size() > 0) {
            s = sl.get(0);
        }

        return s;
    }
}
