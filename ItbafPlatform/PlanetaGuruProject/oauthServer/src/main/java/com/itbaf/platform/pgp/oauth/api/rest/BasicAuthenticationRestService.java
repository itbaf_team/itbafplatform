package com.itbaf.platform.pgp.oauth.api.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.pgp.oauth.api.handler.AutenticationServiceProcessorHandler;
import com.itbaf.platform.pgp.oauth.api.message.RequestBasicMessage;
import com.itbaf.platform.pgp.oauth.api.message.ResponseBasicMessage;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/auth/basic")
@Singleton
@lombok.extern.log4j.Log4j2
public class BasicAuthenticationRestService extends CommonRestService {

    private final AutenticationServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public BasicAuthenticationRestService(AutenticationServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "An AutenticationServiceProcessorHandler class must be provided");
    }

    @POST
    @Path("/credential/type")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCredentialType(@Context HttpServletRequest request,
            RequestBasicMessage requestBasicMessage) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-auth.basic.credential.type");
        } catch (Exception ex) {
        }
        log.info("getCredentialType: [" + requestBasicMessage + "]");

        Map<String, String> prop = null;
        try {
            prop = serviceProcessorHandler.validateTID(requestBasicMessage.tid);
        } catch (Exception ex) {
            log.warn("TID Invalido: [" + requestBasicMessage + "]" + ex, ex);
        }
        if (prop != null) {
            try {
                ResponseBasicMessage rm;
                rm = serviceProcessorHandler.getUserInformation(requestBasicMessage, prop, false, request);
                return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
            } catch (Exception ex) {
                log.error("Error al procesar informacion de usuario. " + ex, ex);
            }
        }
        return Response.status(Status.UNAUTHORIZED).cacheControl(getCacheControl()).build();
    }

}
