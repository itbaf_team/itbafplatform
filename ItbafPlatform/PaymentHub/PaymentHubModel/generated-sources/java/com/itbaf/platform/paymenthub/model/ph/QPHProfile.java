package com.itbaf.platform.paymenthub.model.ph;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPHProfile is a Querydsl query type for PHProfile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPHProfile extends EntityPathBase<PHProfile> {

    private static final long serialVersionUID = 888412195L;

    public static final QPHProfile pHProfile = new QPHProfile("pHProfile");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final SetPath<PHProfileProperties, QPHProfileProperties> properties = this.<PHProfileProperties, QPHProfileProperties>createSet("properties", PHProfileProperties.class, QPHProfileProperties.class, PathInits.DIRECT2);

    public QPHProfile(String variable) {
        super(PHProfile.class, forVariable(variable));
    }

    public QPHProfile(Path<? extends PHProfile> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPHProfile(PathMetadata metadata) {
        super(PHProfile.class, metadata);
    }

}

