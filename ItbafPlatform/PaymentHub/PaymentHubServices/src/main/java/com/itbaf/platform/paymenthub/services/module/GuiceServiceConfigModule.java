/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.module;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.itbaf.platform.paymenthub.services.repository.*;
import com.itbaf.platform.paymenthub.services.repository.transactional.*;
import com.itbaf.platform.services.service.PropertyManagerService;
import com.itbaf.platform.services.service.TestService;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.paymenthub.services.OnDemandManager;
import com.itbaf.platform.paymenthub.services.OnDemandManagerImpl;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.SubscriptionManagerImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class GuiceServiceConfigModule extends AbstractModule {

    @Override
    protected void configure() {

        //Binds services
        bind(SubscriptionManager.class).to(SubscriptionManagerImpl.class);
        bind(ProviderService.class).to(ProviderServiceTransactional.class);
        bind(SubscriptionService.class).to(SubscriptionServiceTransactional.class);
        bind(SubscriptionRegistryService.class).to(SubscriptionRegistryServiceTransactional.class);
        bind(SubscriptionBillingService.class).to(SubscriptionBillingServiceTransactional.class);
        bind(PHProfilePropertiesService.class).to(PHProfilePropertiesServiceTransactional.class);
        bind(PHInstanceService.class).to(PHInstanceServiceTransactional.class);
        bind(SopService.class).to(SopServiceTransactional.class);
        bind(SmsSopMultipleService.class).to(SmsSopMultipleServiceTransactional.class);
        bind(SmsSopService.class).to(SmsSopServiceTransactional.class);
        bind(CountryService.class).to(CountryServiceTransactional.class);
        bind(AdnetworkNotifierService.class).to(AdnetworkNotifierServiceTransactional.class);
        bind(OnDemandBillingService.class).to(OnDemandBillingServiceTransactional.class);
        bind(OnDemandRegistryService.class).to(OnDemandRegistryServiceTransactional.class);
        bind(OnDemandManager.class).to(OnDemandManagerImpl.class);
        bind(BillerAttemptService.class).to(BillerAttemptServiceTransactional.class);
        bind(BillerAttemptTransactionService.class).to(BillerAttemptTransactionServiceTransactional.class);
        bind(TariffService.class).to(TariffServiceTransactional.class);
        bind(AdnetworkNotifierSimpleService.class).to(AdnetworkNotifierSimpleServiceTransactional.class);
        bind(AdapterSmsLogReceivedService.class).to(AdapterSmsLogReceivedServiceTransactional.class);
        bind(AdapterSmsLogSentService.class).to(AdapterSmsLogSentServiceTransactional.class);
    }

    @Provides
    @Singleton
    PropertyManagerService propertyManager(PHProfilePropertiesService profile) {
        return profile;
    }

    @Provides
    @Singleton
    TestService testService(ProviderService providerService) {
        return providerService;
    }

    @Provides
    @Singleton
    Set<Provider> providerSet(@Named("guice.instance") Long instanceId,
            final PHInstanceService instanceService) {
        PHInstance instance = instanceService.findById(instanceId);
        return instance.getConfigurations().keySet();
    }

    @Provides
    @Singleton
    Map<String, Provider> providerMap(Set<Provider> providerList) {

        Map<String, Provider> providerMap = new HashMap();
        String aux = "\n----- ----- \nProviders List\n----- -----\n";
        for (Provider p : providerList) {
            providerMap.put(p.getCountry().getCode(), p);
            aux = aux + p + "\n";
        }
        aux = aux + "----- ----- ";
        log.info(aux);

        return providerMap;
    }

    @Provides
    @Singleton
    PHInstance phInstance(@Named("guice.instance") Long instanceId, PHInstanceService instanceService) {
        return instanceService.findById(instanceId);
    }
}
