/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.server.notification;

import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage.NotificationType;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.rmq.consumer.MessageProcessor;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;

/**
 *
 * @author javier
 */
public interface NotificationProcessor extends MessageProcessor {

    public NotificationType getNotificationType();

    /**
     * @see RabbitMQMessageProcessor
     * @return NULL. Por alguna razon no se proceso message. Se reencola........
     * TRUE.Mensaje procesado satisfactoriamente................................
     * FALSE. Mensaje procesado y descartable...................................
     */
    public Boolean process(String smsg, PaymentHubMessage msg, SOP sop);
}
