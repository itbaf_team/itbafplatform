/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository;

import com.itbaf.platform.pgp.model.Locale;

/**
 *
 * @author javier
 */
public interface LocaleService {

    public void saveOrUpdate(Locale lp) throws Exception;

    public String getValueByKey(String key);

    public String getValueByKey(String languageCode, String key);

    public String getValueByKey(String languageCode, String countryCode, String key);

    public String getValueByKey(String languageCode, String countryCode, Long providerId, String key);

    /**
     * Internacionalizacion basica.
     *
     * @param i18n es-AR, es-CO
     * @param providerId
     * @param html
     * @return
     */
    public String i18n(String i18n, Long providerId, String html);

    public String i18n(String i18n, String providerId, String html);
}
