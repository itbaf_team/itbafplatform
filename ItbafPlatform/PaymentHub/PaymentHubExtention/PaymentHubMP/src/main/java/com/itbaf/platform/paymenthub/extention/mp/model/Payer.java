package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Payer {

    public enum Type {
        customer,
        registered,
        guest,
        anonymous
    }

    private String entity_type;
    private Type type;
    private String id;
    private String email;
    private Identification identification;
    private PayerPhone phone;
    private String name;
    private String surname;
    private String first_name;
    private String last_name;
    private Address address;
    private String nickname;
    private String date_created;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("entity_type", entity_type)
                .add("type", type)
                .add("id", id)
                .add("email", email)
                .add("identification", identification)
                .add("phone", phone)
                .add("name", name)
                .add("surname", surname)
                .add("first_name", first_name)
                .add("last_name", last_name)
                .add("address", address)
                .add("nickname", nickname)
                .add("date_created", date_created)
                .omitNullValues().toString();
    }
}
