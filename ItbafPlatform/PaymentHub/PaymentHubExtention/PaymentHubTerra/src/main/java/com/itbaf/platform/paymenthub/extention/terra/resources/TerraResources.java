package com.itbaf.platform.paymenthub.extention.terra.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.MediaType;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class TerraResources {

    private final ObjectMapper mapper;
    private final RequestClient requestClient;
    private final PHProfilePropertiesService profilePropertiesService;

    @Inject
    public TerraResources(final RequestClient requestClient,
            final ObjectMapper mapper,
            final PHProfilePropertiesService profilePropertiesService) {
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    private String getTokenHeader(SOP sop) throws Exception {

        String username = profilePropertiesService.getCommonProperty("terra.service.username");
        String password = profilePropertiesService.getCommonProperty("terra.service.password");
        String product = sop.getIntegrationSettings().get("terraProductId");

        String url = "https://wstcb-mobile.terra.com/wstcb/Token.asmx/Gen";
        String params = "u=" + username + "&c=" + password + "&p=" + product + "&f=json";

        String response = requestClient.requestPost(url, params, MediaType.APPLICATION_FORM_URLENCODED_TYPE, MediaType.APPLICATION_JSON_TYPE, null);
        log.info("getTokenHeader. SOP: [" + sop.getId() + "]. Terra response: [" + response + "]");

        TerraResponse tt = mapper.readValue(response, TerraResponse.class);

        return tt.RESPONSE[0].TOKEN;
    }

    private String encode(String text) throws Exception {
        byte[] keyBytes = profilePropertiesService.getCommonProperty("terra.service.encode.key").getBytes();
        byte[] IV = profilePropertiesService.getCommonProperty("terra.service.encode.iv").getBytes();
        byte[] plainbytes = text.getBytes();
        if (plainbytes.length % 16 != 0) {
            plainbytes = Arrays.copyOf(plainbytes, (plainbytes.length / 16 + 1) * 16);
        }

        Cipher aes = Cipher.getInstance("AES/CBC/NoPadding");
        aes.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(keyBytes, "AES"), new IvParameterSpec(IV));
        byte[] cipherbytes = aes.doFinal(plainbytes);
        String resultEnc = Base64.encodeBase64String(cipherbytes);
        return resultEnc;
    }

    public ResponseMessage validateSubscriptionByMsisdn(String msisdn, SOP sop) {
        ResponseMessage rm = new ResponseMessage();
        try {
            TerraRequest tr = new TerraRequest();
            tr.REQUEST[0].MSISDN = Long.parseLong(msisdn);
            tr.REQUEST[0].IDPRODUCT = Long.parseLong(sop.getIntegrationSettings().get("terraProductId"));
            tr.REQUEST[0].SERVICE = sop.getIntegrationSettings().get("terraService");
            tr.REQUEST[0].IDOPERATOR = Long.parseLong(profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "terra.service.operator.id"));
            tr.REQUEST[0].DEVICEID = "" + sop.getId() + "." + msisdn;
            String url = "https://wstcb-mobile.terra.com/wstcb/Services.asmx/VrfSuc";
            String queryString = "data=" + encode(mapper.writeValueAsString(tr));
            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Bearer " + encode(getTokenHeader(sop)));

            String response = requestClient.requestPost(url, queryString, MediaType.APPLICATION_FORM_URLENCODED_TYPE, MediaType.APPLICATION_JSON_TYPE, headers);
            log.info("validateSubscriptionByMsisdn. ANI: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Terra Response: [" + response + "]");

            TerraResponse tt = mapper.readValue(response, TerraResponse.class);
            TerraResponse tres = tt.RESPONSE[0];
            switch (tres.STATUS) {
                case "OK"://suscrito
                    rm.status = ResponseMessage.Status.ACTIVE;
                    break;
                case "RV"://renovacion
                    rm.message = "La Suscripcion del MSISDN al Servicio se encuentra en renovacion.";
                    rm.status = ResponseMessage.Status.ACTIVE;
                    break;
                case "NO"://cancelado
                    rm.status = ResponseMessage.Status.REMOVED;
                    break;
                case "OP"://retencion, renovacion
                    rm.message = "La Suscripcion del MSISDN al Servicio se encuentra en proceso.";
                    rm.status = ResponseMessage.Status.PENDING;
                    break;
                case "NA"://Usuario se encuentra en lista negra del Operador.
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    rm.message = "El MSISDN se encuentra en Blacklist";
                    break;
                case "FD":
                    rm.message = "Se ha superado la cantidad limite de Dispositivos activos para el Servicio.";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
                case "NOK":
                    rm.message = "No fue posible verificar el estado de la Suscripcion.";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
                case "DER":
                    rm.message = "Error en datos de entrada";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
                case "ERR":
                    rm.message = "Ocurrio un error en la Plataforma Terra Carrier Billing que impidio el procesamiento de la Solicitud.";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
                case "FA":
                    rm.message = "Ejecucion del metodo no permitida por no poder identificar Partner (Token invalido/expirado, e IP no reconocida).";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
            }
            if (tres.MESSAGE != null) {
                rm.message = tres.MESSAGE;
            }
        } catch (Exception ex) {
            rm.message = "Error al validar estado de ANI: [" + msisdn + "] - SOP: [" + sop.getId() + "]. " + ex;
            log.error(rm.message, ex);
        }
        return rm;
    }

    public ResponseMessage sendSubscriptionPin(String msisdn, SOP sop) {
        ResponseMessage rm = new ResponseMessage();
        try {
            TerraRequest tr = new TerraRequest();
            tr.REQUEST[0].MSISDN = Long.parseLong(msisdn);
            tr.REQUEST[0].IDPRODUCT = Long.parseLong(sop.getIntegrationSettings().get("terraProductId"));
            tr.REQUEST[0].IDOPERATOR = Long.parseLong(profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "terra.service.operator.id"));
            tr.REQUEST[0].SERVICE = sop.getIntegrationSettings().get("terraService");
            tr.GETSHORTCODE = 1;

            String url = "https://wstcb-mobile.terra.com/wstcb/Services.asmx/GetPin";
            String queryString = "data=" + encode(mapper.writeValueAsString(tr));
            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Bearer " + encode(getTokenHeader(sop)));

            String response = requestClient.requestPost(url, queryString, MediaType.APPLICATION_FORM_URLENCODED_TYPE, MediaType.APPLICATION_JSON_TYPE, headers);
            log.info("sendSubscriptionPin. ANI: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Terra Response: [" + response + "]");

            TerraResponse tt = mapper.readValue(response, TerraResponse.class);
            TerraResponse tres = tt.RESPONSE[0];
            switch (tres.STATUS) {
                case "OK":
                    rm.status = ResponseMessage.Status.OK;
                    break;
                case "NOK":
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "No fue posible enviar el Pin de Autentificacion.";
                    break;
                case "NA"://Usuario se encuentra en lista negra del Operador.
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    rm.message = "El MSISDN se encuentra en Blacklist";
                    break;
                case "DER":
                    rm.message = "Error en datos de entrada";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
                case "ERR":
                    rm.message = "Ocurrio un error en la Plataforma Terra Carrier Billing que impidio el procesamiento de la Solicitud.";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
                case "FA":
                    rm.message = "Ejecucion del metodo no permitida por no poder identificar Partner (Token invalido/expirado, e IP no reconocida).";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
            }
            if (tres.MESSAGE != null) {
                rm.message = tres.MESSAGE;
            }

        } catch (Exception ex) {
            rm.message = "Error al enviar la solicitud de PIN. ANI: [" + msisdn + "] - SOP: [" + sop.getId() + "]. " + ex.getMessage();
            log.error(rm.message, ex);
        }
        return rm;
    }

    public ResponseMessage validatePinSubscription(String msisdn, SOP sop, String pin) {

        ResponseMessage rm = new ResponseMessage();
        try {
            TerraRequest tr = new TerraRequest();
            tr.REQUEST[0].PIN = Long.parseLong(pin);
            tr.REQUEST[0].ORDER = Thread.currentThread().getName().split("-")[0];
            tr.REQUEST[0].MSISDN = Long.parseLong(msisdn);
            tr.REQUEST[0].IDPRODUCT = Long.parseLong(sop.getIntegrationSettings().get("terraProductId"));
            tr.REQUEST[0].IDOPERATOR = Long.parseLong(profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "terra.service.operator.id"));
            tr.REQUEST[0].SERVICE = sop.getIntegrationSettings().get("terraService");
            tr.REQUEST[0].DESCRIPTION = "Suscripcion " + sop.getServiceMatcher().getService().getName();
            tr.REQUEST[0].DEVICEID = "" + sop.getId() + "." + msisdn;
            tr.GETSHORTCODE = 1;

            String url = "https://wstcb-mobile.terra.com/wstcb/Services.asmx/SetPin";
            String queryString = "data=" + encode(mapper.writeValueAsString(tr));
            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Bearer " + encode(getTokenHeader(sop)));

            String response = requestClient.requestPost(url, queryString, MediaType.APPLICATION_FORM_URLENCODED_TYPE, MediaType.APPLICATION_JSON_TYPE, headers);
            log.info("validatePinSubscription. ANI: [" + msisdn + "] - SOP: [" + sop.getId() + "] - PIN: [" + pin + "]. Terra Response: [" + response + "]");

            TerraResponse tt = mapper.readValue(response, TerraResponse.class);
            TerraResponse tres = tt.RESPONSE[0];
            switch (tres.STATUS) {
                case "OK"://La Suscripcion se realizo correctamente.
                    rm.status = ResponseMessage.Status.OK;
                    break;
                case "AS"://EL MSISDN ya se encuentra Suscrito al Servicio
                    rm.status = ResponseMessage.Status.ACTIVE;
                    break;
                case "OP"://retencion, en proceso
                case "RV":// En renovacion
                    rm.status = ResponseMessage.Status.PENDING;
                    break;
                case "NEQ"://El Pin enviado no corresponde con el generado
                case "NAP"://No existe un Pin activo para la pareja MSISDN / Producto
                    rm.status = ResponseMessage.Status.PIN_ERROR;
                    break;
                case "NA"://Usuario se encuentra en lista negra del Operador.
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    break;
                case "SR"://Usuario se encuentra en lista negra del Operador.
                    rm.status = ResponseMessage.Status.PIN_ERROR;
                    rm.message = "Falta el codigo SECURE!!!!!";
                    log.error(rm.message);
                    break;
            }
            if (tres.MESSAGE != null) {
                rm.message = tres.MESSAGE;
            }
        } catch (Exception ex) {
            rm.message = "Error al validar PIN. ANI: [" + msisdn + "] - SOP: [" + sop + "] - PIN: [" + pin + "]. " + ex.getMessage();
            log.error(rm.message, ex);
        }
        return rm;
    }

    public ResponseMessage removeSubscription(Subscription subscription) {

        String msisdn = subscription.getUserAccount();
        SOP sop = subscription.getSop();
        ResponseMessage rm = new ResponseMessage();
        try {
            TerraRequest tr = new TerraRequest();
            tr.REQUEST[0].MSISDN = Long.parseLong(msisdn);
            tr.REQUEST[0].PIN = Long.parseLong(subscription.getObject().toString());
            tr.REQUEST[0].IDPRODUCT = Long.parseLong(sop.getIntegrationSettings().get("terraProductId"));
            tr.REQUEST[0].IDOPERATOR = Long.parseLong(profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "terra.service.operator.id"));
            tr.REQUEST[0].SERVICE = sop.getIntegrationSettings().get("terraService");
            tr.REQUEST[0].ORDER = Thread.currentThread().getName().split("-")[0];
            tr.REQUEST[0].DESCRIPTION = "Suscripcion " + sop.getServiceMatcher().getService().getName();

            String url = "https://wstcb-mobile.terra.com/wstcb/Services.asmx/RmvSusc";
            String queryString = "data=" + encode(mapper.writeValueAsString(tr));
            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Bearer " + encode(getTokenHeader(sop)));

            String response = requestClient.requestPost(url, queryString, MediaType.APPLICATION_FORM_URLENCODED_TYPE, MediaType.APPLICATION_JSON_TYPE, headers);
            log.info("removeSubscription. ANI: [" + msisdn + "] - SOP: [" + sop.getId() + "]. Terra Response: [" + response + "]");

            TerraResponse tt = mapper.readValue(response, TerraResponse.class);
            TerraResponse tres = tt.RESPONSE[0];
            switch (tres.STATUS) {
                case "OK"://La baja se realizo correctamente.
                case "NS":
                    rm.status = ResponseMessage.Status.OK;
                    rm.message = "El Numero Telefonico no estaba suscrito al Servicio";
                    break;
                case "NOK":
                    rm.message = "El Numero Telefonico no pudo ser dado de baja del Servicio";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
                case "DER":
                    rm.message = "Error en datos de entrada";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
                case "ERR":
                    rm.message = "Ocurrio un error en la Plataforma Terra Carrier Billing que impidio el procesamiento de la Solicitud.";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
            }
            if (tres.MESSAGE != null) {
                rm.message = tres.MESSAGE;
            }
        } catch (Exception ex) {
            log.error("Error al dar de baja la suscripcion. " + ex, ex);
        }
        return rm;
    }

    public ResponseMessage sendSMS(String msisdn, SOP sop, String message) {
        ResponseMessage rm = new ResponseMessage();
        try {
            TerraRequest tr = new TerraRequest();
            tr.REQUEST[0].MSISDN = Long.parseLong(msisdn);
            tr.REQUEST[0].SMSTEXT = message;
            tr.REQUEST[0].IDPRODUCT = Long.parseLong(sop.getIntegrationSettings().get("terraProductId"));
            tr.REQUEST[0].IDOPERATOR = Long.parseLong(profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "terra.service.operator.id"));

            String url = "https://wstcb-mobile.terra.com/wstcb/Services.asmx/SndSms";
            String queryString = "data=" + encode(mapper.writeValueAsString(tr));
            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Bearer " + encode(getTokenHeader(sop)));

            String response = requestClient.requestPost(url, queryString, MediaType.APPLICATION_FORM_URLENCODED_TYPE, MediaType.APPLICATION_JSON_TYPE, headers);
            log.info("sendSMS. ANI: [" + msisdn + "] - SOP: [" + sop.getId() + "] - SMS: [" + message + "]. Terra Response: [" + response + "]");

            TerraResponse tt = mapper.readValue(response, TerraResponse.class);
            TerraResponse tres = tt.RESPONSE[0];
            switch (tres.STATUS) {
                case "OK"://SMS enviado
                    rm.status = ResponseMessage.Status.OK;
                    rm.message = "El SMS se envio exitosamente.";
                    break;
                case "NA":
                    rm.message = "El MSISDN se encuentra en Blacklist.";
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    break;
                case "NOK":
                    rm.message = "No es posible enviar SMS.";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
                case "DER":
                    rm.message = "Error en datos de entrada";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
                case "ERR":
                    rm.message = "Ocurrio un error en la Plataforma Terra Carrier Billing que impidio el procesamiento de la Solicitud.";
                    rm.status = ResponseMessage.Status.ERROR;
                    break;
            }
            if (tres.MESSAGE != null) {
                rm.message = tres.MESSAGE;
            }
        } catch (Exception ex) {
            log.error("Error al enviar el SMS. ANI: [" + msisdn + "] - SOP: [" + sop + "] - SMS: ["+message+"]" + ex, ex);
        }
        return rm;
    }

    public TerraResponse getServiceInformation(SOP sop) {
        try {
            Object obj = MainCache.memory5Hours().getIfPresent("terraServiceInformation_" + sop.getId());

            if (obj == null) {
                TerraRequest tr = new TerraRequest();
                tr.REQUEST[0].IDPRODUCT = Long.parseLong(sop.getIntegrationSettings().get("terraProductId"));
                tr.REQUEST[0].IDOPERATOR = Long.parseLong(profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "terra.service.operator.id"));
                tr.REQUEST[0].SERVICE = sop.getIntegrationSettings().get("terraService");

                String url = "https://wstcb-mobile.terra.com/wstcb/Services.asmx/GetSrv";
                String queryString = "data=" + encode(mapper.writeValueAsString(tr));
                Map<String, String> headers = new HashMap();
                headers.put("Authorization", "Bearer " + encode(getTokenHeader(sop)));

                String response = requestClient.requestPost(url, queryString, MediaType.APPLICATION_FORM_URLENCODED_TYPE, MediaType.APPLICATION_JSON_TYPE, headers);
                log.info("getServiceInformation. SOP: ["+sop.getId()+"]. Terra Response: [" + response + "]");

                TerraResponse tt = mapper.readValue(response, TerraResponse.class);
                if (tt.RESPONSE != null && tt.RESPONSE.length > 0) {
                    tt = tt.RESPONSE[0];
                    MainCache.memory5Hours().put("terraServiceInformation_" + sop.getId(), tt);
                    return tt;
                }
            } else {
                return (TerraResponse) obj;
            }
        } catch (Exception ex) {
            log.error("Error al obtener informacion del servicio. SOP: [" + sop + "]. " + ex, ex);
        }
        return null;

    }
}
