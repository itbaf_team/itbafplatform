/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.handler;

    import com.google.inject.Inject;
    import com.itbaf.platform.messaging.rest.ResponseMessage;
    import com.itbaf.platform.paymenthub.commons.bn.model.TimweNotification;
    import com.itbaf.platform.paymenthub.extention.timwe.resources.TimweSMSRequest;
    import com.itbaf.platform.paymenthub.model.SOP;
    import com.itbaf.platform.paymenthub.model.subscription.Subscription;
    import javax.xml.ws.Holder;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ATTMxServiceProcessorHandler extends ServiceProcessorHandler {

    @Inject
    public ATTMxServiceProcessorHandler() {
        super("mx.att.timwe");
    }

    @Override
    protected Boolean processBillingNotification(TimweNotification notification, SOP sop, Subscription s) {

        Boolean b = null;
        super.processBillingNotification(notification, sop, s);

        try {
            Holder<Subscription.Status> status = new Holder();
            b = timweResources.sendSMS(notification.getMsisdn(), sop,
                    TimweSMSRequest.Context.STATELESS, TimweSMSRequest.Priority.HIGH,
                    sop.getIntegrationSettings().get("sms.message.renew"), status);

            if (b == null) {
                log.error("No fue posible enviar el MT de renovacion para: [" + notification + "]");
            }

           /* if (status.value != null) {
                PaymentHubMessage phm = new PaymentHubMessage();
                PaymentHubMessage msg = new PaymentHubMessage();
                try {
                    msg.setCodeCountry(sop.getProvider().getCountry().getCode());
                    msg.setToDate(new Date());

                    switch (status.value) {
                        case BLACKLIST:
                            msg.setMessageType(MessageType.SUBSCRIPTION_BLACKLIST);
                            break;
                        case CANCELLED:
                            msg.setMessageType(MessageType.SUBSCRIPTION_CANCELLATION);
                            break;
                    }

                    msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
                    msg.setTariffId(sop.getMainTariff().getId());
                    msg.setFullAmount(sop.getMainTariff().getFullAmount());
                    msg.setNetAmount(sop.getMainTariff().getNetAmount());
                    msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
                    msg.setSopId(sop.getId());
                    msg.setExternalId(notification.getTransactionUUID());
                    msg.setTransactionId(notification.getExternal_tx_id());
                    //Verificar si el channel q llega es el correspondiente al canal de baja
                    msg.setChannelOut("SMS_MT");
                    msg.setUserAccount(notification.getMsisdn());
                    msg.setRealUserAccount(true);
                    msg.setToDateOverride(false);

                    Extra extra = new Extra();
                    extra.stringData = new HashMap();
                    if (notification.getExternal_tx_id() != null) {
                        extra.stringData.put("External_tx_id." + notification.getExternal_tx_id(), notification.getExternal_tx_id());
                    }
                    if (notification.getApikey() != null) {
                        extra.stringData.put("Apikey." + notification.getApikey(), notification.getApikey());
                    }
                    if (notification.getAuthentication() != null) {
                        extra.stringData.put("Authentication." + notification.getApikey(), notification.getAuthentication());
                    }
                    if (notification.getText() != null) {
                        extra.stringData.put("Text." + notification.getText(), notification.getText());
                    }
                    if (notification.getRealm() != null) {
                        extra.stringData.put("Realm." + notification.getRealm(), notification.getRealm());
                    }
                    if (notification.getTags() != null && !notification.getTags().isEmpty()) {
                        String aux = Arrays.toString(notification.getTags().toArray());
                        extra.stringData.put("Tags." + aux, aux);
                    }
                    if (!extra.stringData.isEmpty()) {
                        msg.setExtra(extra);
                    }

                    String jsonResponse = mapper.writeValueAsString(msg);
                    if (jsonResponse != null) {
                        return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
                    }
                } catch (Exception ex) {
                    log.error("Error al procesar WauNotification: [" + notification + "]. " + ex, ex);
                }
            }*/
        } catch (Exception ex) {
            log.error("Error al enviar el SMS de renovacion. [" + notification + "]. " + ex, ex);
        }

        return b;
    }

    @Override
    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResponseMessage sendSMS(Subscription subscription) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
