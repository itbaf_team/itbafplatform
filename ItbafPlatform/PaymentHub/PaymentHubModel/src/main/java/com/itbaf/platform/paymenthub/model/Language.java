/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import javax.persistence.Cacheable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
@Entity(name = "LANGUAGE")
@Table(name = "LANGUAGE", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class Language implements Serializable, Comparable<Language> {

    private static final long serialVersionUID = 20180118153801L;

    @Id
    @Column(name = "CODE_2", nullable = false, unique = true)
    private String code;

    @Column(name = "CODE_3", nullable = false, unique = true)
    private String code3;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NATIVE_NAME")
    private String nativeName;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("code", code)
                .add("code3", code3)
                .add("name", name)
                .add("nativeName", nativeName)
                .omitNullValues().toString();
    }

    public Language() {
    }

    public Language(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, code3, name, nativeName);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<Language>()
                .over(Language::getCode)
                .over(Language::getCode3)
                .over(Language::getName)
                .over(Language::getNativeName)
                .asserts(this, obj);
    }

    @Override
    public int compareTo(Language o) {
        return code.compareTo(o.code);
    }
}
