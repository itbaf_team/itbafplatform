/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class TrustCerts {

    private static final String KEYSTORE = "smtStore.keystore";
    private static final String PASSWORD = "password";
    private static final Logger log = LogManager.getLogger(TrustCerts.class);

    /**
     * De no funcionar, copiar el contenido en el metodo getTokenHeader() de la
     * claseSMTResources
     */
    public static void initWithSSL() throws Exception {
        System.setProperty("jdk.tls.client.protocols","TLSv1,TLSv1.1,TLSv1.2,SSLv3,SSLv2Hello");
        log.info("Seteando protocolos =" +System.getProperties());
        init();
    }

    public static void init() throws Exception {

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
            }
        }
        };
        SSLContext sc = SSLContext.getInstance("SSL");
        log.info("Seteando protocolos =" +System.getProperties() +" "+ "instancia= " + sc.getProtocol() + " "+ sc.getProvider());
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

    @Deprecated
    public static void createKeystore() {
        System.setProperty("jdk.tls.client.protocols", "SSLv3,SSLv2Hello,TLSv1,TLSv1.1,TLSv1.2");
        try {
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            char[] password = PASSWORD.toCharArray();
            ks.load(null, password);

            FileOutputStream fos = new FileOutputStream(KEYSTORE);
            ks.store(fos, password);
            fos.close();

            FileInputStream is = new FileInputStream(KEYSTORE);
            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
            keystore.load(is, PASSWORD.toCharArray());

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream subscribeCertstream = TrustCerts.class.getResourceAsStream("/wsdl/subscribeproduct/crt/subscribeCert.crt");
            InputStream smsCertstream = TrustCerts.class.getResourceAsStream("/wsdl/messaging/crt/smsCert.crt");
            InputStream chargeCertstream = TrustCerts.class.getResourceAsStream("/wsdl/charging/crt/chargeCert.crt");
            Certificate subscribeCert = cf.generateCertificate(subscribeCertstream);
            Certificate smsCert = cf.generateCertificate(smsCertstream);
            Certificate chargeCert = cf.generateCertificate(chargeCertstream);
            File keystoreFile = new File(KEYSTORE);
            FileInputStream in = new FileInputStream(keystoreFile);
            keystore.load(in, password);
            in.close();
            keystore.setCertificateEntry("subscribeCert", subscribeCert);
            keystore.setCertificateEntry("smsCert", smsCert);
            keystore.setCertificateEntry("chargeCert", chargeCert);

            FileOutputStream out = new FileOutputStream(keystoreFile);
            keystore.store(out, password);
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error al crear KEYSTORE. " + ex);
        }
        //-Djdk.tls.client.protocols=SSLv3,SSLv2Hello,TLSv1,TLSv1.1,TLSv1.2
        System.setProperty("jdk.tls.client.protocols", "SSLv3,SSLv2Hello,TLSv1,TLSv1.1,TLSv1.2");
        System.setProperty("javax.net.ssl.keyStore", KEYSTORE);
        System.setProperty("javax.net.ssl.keyStorePassword", PASSWORD);
        System.setProperty("javax.net.ssl.keyStoreType", "JKS");
        System.setProperty("javax.net.ssl.trustStore", KEYSTORE);
        System.setProperty("javax.net.ssl.trustStorePassword", PASSWORD);

        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {

                if (hostname.equals("hub.americamovil.com")) {
                    return true;
                } else if (hostname.equals("200.95.168.211")) {
                    return true;
                }
                return false;
            }
        });
    }
}
