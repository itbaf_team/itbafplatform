/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.commons;

/**
 *
 * @author JF
 */
public class CommonAttribute {
    
    public static final String PRINCIPAL = "client";
    public static final String U_PRINCIPAL = "user";
    public static final String U_COOKIE = "cookie";
    public static final String U_REDIRECT_URI = "redirectUri";
    public static final String PRODUCT_ID = "productId";
    public static final String U_DEFAULT_CLAIMS = "DefaultClaims";
}
