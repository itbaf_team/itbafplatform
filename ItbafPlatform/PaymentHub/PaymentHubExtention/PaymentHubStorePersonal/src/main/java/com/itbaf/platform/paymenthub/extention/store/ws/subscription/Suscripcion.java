package com.itbaf.platform.paymenthub.extention.store.ws.subscription;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Java class for suscripcion complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="suscripcion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numerosCorto" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="suscripcionId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nombreSuscripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombreFantasia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descripcionSuscripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="proveedorId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "suscripcion", namespace = "urn:SendatelSuscripcionesService", propOrder = {
    "numerosCorto",
    "suscripcionId",
    "nombreSuscripcion",
    "nombreFantasia",
    "descripcionSuscripcion",
    "estado",
    "proveedorId",
    "tipoServicio",
    "descargas"
})
public class Suscripcion {

    protected List<String> numerosCorto;
    protected int suscripcionId;
    @XmlElement(required = true)
    protected String nombreSuscripcion;
    @XmlElement(required = true)
    protected String nombreFantasia;
    @XmlElement(required = true)
    protected String descripcionSuscripcion;
    protected int estado;
    protected int proveedorId;
    protected int tipoServicio;
    protected int descargas;

    /**
     * Gets the value of the numerosCorto property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the numerosCorto property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNumerosCorto().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link String }
     *
     *
     */
    public List<String> getNumerosCorto() {
        if (numerosCorto == null) {
            numerosCorto = new ArrayList<String>();
        }
        return this.numerosCorto;
    }

    /**
     * Gets the value of the suscripcionId property.
     *
     */
    public int getSuscripcionId() {
        return suscripcionId;
    }

    /**
     * Sets the value of the suscripcionId property.
     *
     */
    public void setSuscripcionId(int value) {
        this.suscripcionId = value;
    }

    /**
     * Gets the value of the nombreSuscripcion property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNombreSuscripcion() {
        return nombreSuscripcion;
    }

    /**
     * Sets the value of the nombreSuscripcion property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setNombreSuscripcion(String value) {
        this.nombreSuscripcion = value;
    }

    /**
     * Gets the value of the nombreFantasia property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNombreFantasia() {
        return nombreFantasia;
    }

    /**
     * Sets the value of the nombreFantasia property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setNombreFantasia(String value) {
        this.nombreFantasia = value;
    }

    /**
     * Gets the value of the descripcionSuscripcion property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDescripcionSuscripcion() {
        return descripcionSuscripcion;
    }

    /**
     * Sets the value of the descripcionSuscripcion property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setDescripcionSuscripcion(String value) {
        this.descripcionSuscripcion = value;
    }

    /**
     * Gets the value of the estado property.
     *
     */
    public int getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     *
     */
    public void setEstado(int value) {
        this.estado = value;
    }

    /**
     * Gets the value of the proveedorId property.
     *
     */
    public int getProveedorId() {
        return proveedorId;
    }

    /**
     * Sets the value of the proveedorId property.
     *
     */
    public void setProveedorId(int value) {
        this.proveedorId = value;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public int getDescargas() {
        return descargas;
    }

    public void setDescargas(int descargas) {
        this.descargas = descargas;
    }

}
