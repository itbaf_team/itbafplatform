/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.model;

import com.google.common.base.MoreObjects;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author JF
 */

public class Birthdate {

    public int year;
    public int month;
    public int day;

    public Birthdate() {
    }

    public Birthdate(final Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        this.year = c.get(Calendar.YEAR);
        this.month = c.get(Calendar.MONTH) + 1;
        this.day = c.get(Calendar.DATE);
    }
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("year", year)
                .add("month", month)
                .add("day", day)
                .omitNullValues().toString();
    }
}
