/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.ph;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
//@Immutable
@Entity(name = "PH_PROFILE_PROPERTIES")
@Table(name = "PH_PROFILE_PROPERTIES", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class PHProfileProperties implements Serializable {

    private static final long serialVersionUID = 20170809155410L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "KEY", nullable = false)
    private String key;

    @Column(name = "VALUE", nullable = false)
    private String value;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne(optional = false)
    @JoinColumn(name = "PROVIDER_ID")
    private Provider provider;

    @ManyToOne
    @JoinColumn(name = "PH_PROFILE_ID")
    private PHProfile phProfile;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("key", key)
                .add("value", value)
                .add("description", description)
                .add("provider", provider)
                .add("phProfile", phProfile != null ? phProfile.getId() : null)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, key, value, description, provider, phProfile != null ? phProfile.getId() : null);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<PHProfileProperties>()
                .over(PHProfileProperties::getId)
                .over(PHProfileProperties::getKey)
                .over(PHProfileProperties::getValue)
                .over(PHProfileProperties::getDescription)
                .over(PHProfileProperties::getProvider)
                .over(PHProfileProperties::getPhProfile)
                .asserts(this, obj);
    }

}
