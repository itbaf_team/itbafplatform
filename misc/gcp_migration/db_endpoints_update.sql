UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-server.ph.svc.cluster.local:8070/rest/facade/' WHERE `ID` = '200'; -- 'paymenthub.server.facade'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'rabbitmq.messages.svc.cluster.local' WHERE `ID` = '110'; -- 'rabbitmq.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-mp.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '202'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-npay.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '208'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-npay.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '203'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-npay.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '214'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-npay.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '229'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-tigo.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '211'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-spiralis.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '220'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-npay.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '217'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-smt.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '223'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-cdag.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '238'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-cdag.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '239'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-smsc.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '201'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-npay.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '241'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-wau.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '204'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-terra.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '226'; -- 'internal.service.url 
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-npay.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '232'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-npay.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '235'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-npay.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '207'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-npay.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '206'; -- 'internal.service.url'
UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'http://ph-sc.ph.svc.cluster.local:8070/rest/inner/' WHERE `ID` = '209'; -- 'internal.service.url'
-- UPDATE `paymentHub`.`PH_PROFILE_PROPERTIES` SET `VALUE` = 'email-smtp.us-east-1.amazonaws.com' WHERE `ID` = '120'; -- 'mail.smtp.host'

UPDATE `pg_schema`.`GENERIC_PROPERTIES` SET `VALUE_P`=? WHERE `ID`='3';
UPDATE `pg_schema`.`GENERIC_PROPERTIES` SET `VALUE_P`=? WHERE `ID`='6';
UPDATE `pg_schema`.`GENERIC_PROPERTIES` SET `VALUE_P`=? WHERE `ID`='52';
