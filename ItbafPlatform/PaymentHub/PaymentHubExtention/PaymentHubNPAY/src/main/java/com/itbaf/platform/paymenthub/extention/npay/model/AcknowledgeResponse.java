/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.npay.model;

import com.google.common.base.MoreObjects;
import java.io.Serializable;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class AcknowledgeResponse implements Serializable {

    private static final long serialVersionUID = 2019010911021L;

    private Meta meta;
    private Data data;

    public class Meta implements Serializable {

        private static final long serialVersionUID = 2019010911022L;

        public String code;
        public String status;
        public String description;

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("code", code)
                    .add("status", status)
                    .add("description", description)
                    .omitNullValues().toString();
        }
    }

    public class Data implements Serializable {

        private static final long serialVersionUID = 2019010911023L;

        public String message;

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("message", message)
                    .omitNullValues().toString();
        }
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("meta", meta)
                .add("data", data)
                .omitNullValues().toString();
    }
}
