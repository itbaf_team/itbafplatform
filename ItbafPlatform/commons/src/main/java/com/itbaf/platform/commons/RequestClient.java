/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons;

import com.itbaf.platform.commons.filter.ClientRequestFilter;
import com.sun.jersey.api.client.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.MediaType;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author javier
 */
public class RequestClient {

    private static final Logger log = LogManager.getLogger(RequestClient.class);
    public static final Integer TIMEOUT_MS_15000 = 15000;
    public static final Integer TIMEOUT_MS_10000 = 10000;
    public static final Integer TIMEOUT_MS_120000 = 120000;
    public static final Integer TIMEOUT_MS_7000 = 7000;
    public static final Integer TIMEOUT_MS_SOAP_60000 = 60000;

    public String requestSimpleGet(String url, Integer timeout) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(timeout);
        client.setConnectTimeout(timeout);

        try {
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.get(ClientResponse.class);

            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error request: [" + url + "]. " + ex);
        }

        return result;
    }

    public String requestSimpleGet(String url) {
        return requestSimpleGet(url, TIMEOUT_MS_15000);
    }

    public String requestSimplePost(String url, String data, MediaType mediaTypeRequest, MediaType mediaTypeResponse, Map<String, String> headers) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_10000);
        client.setConnectTimeout(TIMEOUT_MS_15000);
        try {
            WebResource webResource = client.resource(url);
            WebResource.Builder builder = webResource.getRequestBuilder();

            if (mediaTypeRequest != null) {
                builder.type(mediaTypeRequest);
            }
            if (mediaTypeResponse != null) {
                builder.accept(mediaTypeResponse);
            }
            if (headers != null && !headers.isEmpty()) {
                for (String h : headers.keySet()) {
                    builder.header(h, headers.get(h));
                }
            }

            ClientResponse response = builder.post(ClientResponse.class, data);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (ClientHandlerException ex) {
            log.warn("Request [" + url + "][" + data + "]. " + ex);
        } catch (Exception ex) {
            log.error("Error request: [" + url + "][" + data + "]. " + ex);
        }

        return result;
    }

    public String requestJsonGetNPAY(String url) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);

        try {
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .get(ClientResponse.class);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error request: [" + url + "]. " + ex);
        }

        return result;
    }

    public String requestJsonGet(String url) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);

        try {
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                    .get(ClientResponse.class);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error request: [" + url + "]. " + ex);
        }

        return result;
    }

    public String requestJsonDeleteStorePersonal(String url) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);

        try {
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON)
                    .delete(ClientResponse.class);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (UniformInterfaceException ex) {
               String  codeStatus = String.valueOf(ex.getResponse().getStatus());

               return codeStatus;
        } catch (Exception ex) {
            log.error("Error request: [" + url + "]. " + ex);
        }

        return result;
    }

    public String requestOKGet(String url) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);

        try {
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                    .get(ClientResponse.class);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
            if (!(response.getStatus() + "").startsWith("2")) {
                return null;
            }
        } catch (Exception ex) {
            log.error("Error request: [" + url + "]. " + ex);
        }

        return result;
    }

    public String requestJSONPut(String url, String json) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);

        try {
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                    .put(ClientResponse.class, json);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error request: [" + url + "] - JSON: [" + json + "]. " + ex);
        }

        return result;
    }

    public String requestJSONPost(String url, String json) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);
        if (json == null) {
            json = "";
        }

        try {
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, json);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error request: [" + url + "] - JSON: [" + json + "]. " + ex);
        }

        return result;
    }
    
    public String requestJSONPost(String url, String json, String user, String password) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);
        if (json == null) {
            json = "";
        }

        try {
            String encoded = Base64.getEncoder().encodeToString((user + ":" + password).getBytes(StandardCharsets.UTF_8));
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).header("Authorization", "Basic " + encoded).accept(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, json);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error request TIAXA: [" + url + "] - JSONTIAXA: [" + json + "]. " + ex);
        }

        return result;
    }

    public String requestJSONPostFacade(String url, String json) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);
        if (json == null) {
            json = "";
        }

        try {
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class, json);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
           // log.error("Error request requestJSONPostFacade: [" + url + "] - JSON: [" + json + "]. " + ex);
        }

        return result;
    }

    public String requestJSONPost(String url, String json, Map<String, String> headers) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_120000);
        client.setConnectTimeout(TIMEOUT_MS_120000);
        if (json == null) {
            json = "";
        }

        try {
            WebResource webResource = client.resource(url);
            WebResource.Builder builder = webResource.getRequestBuilder()
                    .type(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON);

            if (headers != null && !headers.isEmpty()) {
                for (String h : headers.keySet()) {
                    builder.header(h, headers.get(h));
                }
            }

            ClientResponse response = builder.post(ClientResponse.class, json);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error request: [" + url + "] - JSON: [" + json + "]. " + ex);
        }

        return result;
    }

    public Map<String, Object> requestGet(String url, String queryString, MediaType mediaTypeRequest, MediaType mediaTypeResponse, Map<String, String> headers) {
        Map<String, Object> ret = new HashMap();
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);

        try {
            WebResource webResource = client.resource(url + "?" + queryString);
            WebResource.Builder builder = webResource.getRequestBuilder();

            if (mediaTypeRequest != null) {
                builder.type(mediaTypeRequest);
            }
            if (mediaTypeResponse != null) {
                builder.accept(mediaTypeResponse);
            }
            if (headers != null && !headers.isEmpty()) {
                for (String h : headers.keySet()) {
                    builder.header(h, headers.get(h));
                }
            }

            ClientResponse response = builder.get(ClientResponse.class);
            ret.put("status", response.getStatus());
            ret.put("statusInfo", response.getStatusInfo());
            result = response.getEntity(String.class);
            ret.put("result", result);
        } catch (Exception ex) {
            log.error("Error request: [" + url + "]. " + ex);
        }

        return ret;
    }

    public String requestPost(String url, String queryString, MediaType mediaTypeRequest, MediaType mediaTypeResponse, Map<String, String> headers) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);

        try {
            WebResource webResource = client.resource(url + "?" + queryString);
            WebResource.Builder builder = webResource.getRequestBuilder();

            if (mediaTypeRequest != null) {
                builder.type(mediaTypeRequest);
            }
            if (mediaTypeResponse != null) {
                builder.accept(mediaTypeResponse);
            }
            if (headers != null && !headers.isEmpty()) {
                for (String h : headers.keySet()) {
                    builder.header(h, headers.get(h));
                }
            }

            ClientResponse response = builder.post(ClientResponse.class, queryString);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error request: [" + url + "]. " + ex);
        }

        return result;
    }

    public String requestPost(String url, String queryString) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);

        try {
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.post(ClientResponse.class, queryString);

            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error request: [" + url + "]. " + ex);
        }

        return result;
    }

    public String requestPostGoogle(String url, String queryString) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);

        try {
            WebResource webResource = client.resource(url + "?" + queryString);
            ClientResponse response = webResource.post(ClientResponse.class, queryString);

            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error request: [" + url + "]. " + ex);
        }

        return result;
    }

    public String requestSOAP(String url, String soap) {
        String result = null;
        try {
            Client client = Client.create();
            client.addFilter(new ClientRequestFilter());
            client.setReadTimeout(TIMEOUT_MS_SOAP_60000);
            client.setConnectTimeout(TIMEOUT_MS_SOAP_60000);
            WebResource webResource = client.resource(url);

            ClientResponse response = webResource.type(MediaType.TEXT_XML).accept(MediaType.TEXT_XML)
                    .post(ClientResponse.class, soap);

            log.info("requestSOAP response= " + response);

            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error. URL: [" + url + "] -  SOAP: [" + soap + "]. " + ex);
        }

        return result;
    }

    public String requestSOAPWithHeader(String url, String soap, Map<String, String> headers) {
        String result = null;
        Client client = Client.create();
        client.addFilter(new ClientRequestFilter());
        client.setReadTimeout(TIMEOUT_MS_15000);
        client.setConnectTimeout(TIMEOUT_MS_15000);

        try {
            WebResource webResource = client.resource(url);
            WebResource.Builder builder = webResource.getRequestBuilder();


            if (headers != null && !headers.isEmpty()) {
                for (String h : headers.keySet()) {
                    builder.header(h, headers.get(h));
                }
            }

            ClientResponse response = builder.post(ClientResponse.class, soap);
            result = response.getEntity(String.class);
            if (result == null) {
                result = "";
            }
        } catch (Exception ex) {
            log.error("Error. URL: [" + url + "] -  SOAP: [" + soap + "]. " + ex);
        }

        return result;
    }
}
