package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class BackUrls {

    private String success;
    private String pending;
    private String failure;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("success", success)
                .add("pending", pending)
                .add("failure", failure)
                .omitNullValues().toString();
    }
}
