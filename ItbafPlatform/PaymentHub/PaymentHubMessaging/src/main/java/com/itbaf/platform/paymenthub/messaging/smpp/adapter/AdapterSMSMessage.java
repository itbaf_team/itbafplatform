/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.messaging.smpp.adapter;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.RcvSmsRequest;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.SendSmsRequest;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.SendSmsResponse;
import java.util.Date;

/**
 *
 * @author JF
 */
public class AdapterSMSMessage {

    private static final long serialVersionUID = 2018112314480L;

    public Long sopId;
    public String serviceName;
    public String operatorName;
    public String providerName;
    public RcvSmsRequest rcvSmsRequest;
    public SendSmsRequest sendSmsRequest;
    public SendSmsResponse sendSmsResponse;
    public Integer SendSmsResponseAttempt = 0;
    public Date createdDate = new Date();
    public final String thread = Thread.currentThread().getName();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("sopId", sopId)
                .add("serviceName", serviceName)
                .add("operatorName", operatorName)
                .add("providerName", providerName)
                .add("rcvSmsRequest", rcvSmsRequest)
                .add("sendSmsRequest", sendSmsRequest)
                .add("sendSmsResponse", sendSmsResponse)
                .add("SendSmsResponseAttempt", SendSmsResponseAttempt)
                .add("createdDate", createdDate)
                .add("thread", thread)
                .omitNullValues().toString();
    }
}
