/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.store.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.sync.ServiceProcessor;
import com.itbaf.platform.paymenthub.extention.store.resources.StoreResources;
import com.itbaf.platform.paymenthub.extention.store.resources.StoreResponse;
import com.itbaf.platform.paymenthub.extention.store.ws.notification.Notification;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.OnDemandManager;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import org.apache.commons.lang3.Validate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING_OD;

@lombok.extern.log4j.Log4j2
public class OnDemandProcessorHandler extends ServiceProcessor {

    private final OnDemandManager ondemandManager;
    private final StoreResources storeResources;
    private final SopService sopService;

    @Inject
    public OnDemandProcessorHandler(final OnDemandManager ondemandManager, final StoreResources storeResources, final SopService sopService) {
        this.ondemandManager = Validate.notNull(ondemandManager, "An OnDemandManager class must be provided");
        this.storeResources = Validate.notNull(storeResources, "A StoreResources class must be provided");
        this.sopService = Validate.notNull(sopService, "An sopService class must be provided");
    }

    @Override
    public void syncSubscriptions(Provider provider) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    //se suscribe
    public ResponseMessage charge(OnDemandRegistry or) throws Exception {
        SOP sop = or.getSop();

        ResponseMessage rm = new ResponseMessage();
        try {
            or.setUserAccount(userAccountNormalizer(or.getUserAccount(), sop.getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            try {
                rm.data = mapper.writeValueAsString(or);
            } catch (JsonProcessingException ex1) {
            }
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        //agreagar consulta a la API
     /*   try {
            StoreResponse tr = storeResources.validateSubscribe(subscription.getFacadeRequest().externalId, sop);
            if (tr == null || tr.status == null) {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "No fue posible realizar el alta con Store";
            } else {
                if (tr.isActive && tr.status.equals("PURCHASED")) {
                    rm.status = ResponseMessage.Status.OK;
                    rm.message = "Transaccion exitosa. El usuario tiene el servicio activo";
                } else {
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "No fue posible realizar el alta. El usuario no tiene el servicio activo en Store: [ isActive: " + tr.isActive + " status: " + tr.status + "] ";
                }
            }
        } catch (Exception ex) {
            log.error("Error en Store al realizar el alta [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Error en Store. " + ex.getStackTrace();
        }*/

        try {
            //Ignoramos el amount que viene y seteamos el configurado en el SOP
            Tariff t = sop.getMainTariff();
            or.getTransactionTracking().amount = t.getNetAmount();
            or.getTransactionTracking().currency = t.getCurrency();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            long x = instrumentedObject.getAndIncrementAtomicLong("Store-Atomic", 2) % 1000;
            or.setTransactionId("Store" + sdf.format(new Date()) + sop.getProvider().getCountry().getCode() + x);
            or.getTransactionTracking().transactionId = or.getTransactionId();

            or.setStatus(OnDemandRegistry.Status.PENDING);
        } catch (Exception ex) {
            log.error("Error al procesar cobro on demmand de Store. [" + or + "]. " + ex, ex);
            rm.message = "Error al procesar cobro on demmand de Store. " + ex;
            return rm;
        }

        rm.data = mapper.writeValueAsString(or);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }


    //se genera cobro
    public Boolean processBillingNotification(Notification notification, Provider provider) throws Exception {
        Map<String, String> settings = new HashMap();
        settings.put("store_serviceId", notification.getContentId());
        List<SOP> sops = sopService.findAllBySettings(provider.getName(), settings);
        if (sops.size() < 1) {
            log.error("No existe un SOP configurado para el store_serviceId: [" + notification.getContentId() + "] - Provider: [" + provider.getName() + "]");
            return null;
        }

        OnDemandRegistry or = ondemandManager.getAvailableByStatusOptin(notification.getCustomer(), sops);
        SOP sop = or != null ? or.getSop() : sops.get(0);
        Tariff t = sop.getMainTariff(); //TODO como controlo el monto de tarifa

        String configuration = sop.getIntegrationSettings().get("configuration." + notification.getContentId());
        if (configuration == null) {
            log.fatal("No existe una configuracion para: [configuration." + notification.getContentId() + "] en el SOP: [" + sop + "]");
            return null;
        }

        if (or == null) {
            log.info("No existe un ONDEMAND_REGISTRY para msisdn: [" + notification.getCustomer() + "] - ContentId: [" + notification.getContentId() + "].");

            FacadeRequest transactionTracking = mapper.readValue(configuration, FacadeRequest.class);
            or = new OnDemandRegistry();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            long x = instrumentedObject.getAndIncrementAtomicLong("Store-Atomic", 2) % 1000;
            or.setTransactionId("Store" + sdf.format(new Date()) + sop.getProvider().getCountry().getCode() + x);
            or.setId(sop.getId() + ".TID." + or.getTransactionId());
            or.setOndemandDate(new Date());
            or.setSop(sop);
            or.setStatus(OnDemandRegistry.Status.PENDING);
            or.setStatusMessage("A la espera de generar cobro.");
            transactionTracking.transactionId = or.getTransactionId();
            transactionTracking.amount = t.getNetAmount();
            transactionTracking.currency = t.getCurrency();
            or.setTransactionTracking(transactionTracking);
            or.setUserAccount(notification.getCustomer());

            try {
                ondemandManager.ondemandRegistryProcess(or);
            } catch (Exception ex) {
                log.error("Error al actualizar OnDemandRegistry: [" + or + "]. " + ex, ex);
            }
        }

        //mas abajo
        if (or != null) {
            or.setStatus(OnDemandRegistry.Status.CHARGED);
            or.setStatusMessage("SUCCESS");

            try {
                ondemandManager.ondemandRegistryProcess(or);
            } catch (Exception ex) {
                log.error("Error al actualizar OnDemandRegistry: [" + or + "]. " + ex, ex);
            }

            try {
                PaymentHubMessage msg = new PaymentHubMessage();
                msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
                msg.setMessageType(MessageType.ONDEMAND_TOTAL);
                msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
                msg.setInternalId(or.getId());
                msg.setUserAccount(or.getUserAccount());
                msg.setSopId(or.getSop().getId());
                msg.setTransactionId(or.getTransactionId());
                msg.setFromDate(new Date());
                msg.setCurrencyId(or.getTransactionTracking().currency);
                msg.setFullAmount(t.getFullAmount());
                msg.setNetAmount(or.getTransactionTracking().amount);
                msg.setTransactionTracking(or.getTransactionTracking());
                msg.setTariffId(t.getId());

                String jsonResponse = mapper.writeValueAsString(msg);
                if (jsonResponse != null) {
                    return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING_OD, jsonResponse);
                }
            } catch (Exception ex) {
                log.error("Error al generar Registro de cobro. " + ex, ex);
            }
        }

        return null;
    }

    @Override
    public ChannelQuantityProvider getChannelQuantityProvider(Provider p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean processMessage(String message) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void syncSubscription(Subscription s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
