/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons;

import com.google.inject.Inject;
import com.itbaf.platform.services.service.PropertyManagerService;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author JF
 */

public class EmailUtility {

    private final PropertyManagerService propertyManagerService;

    @Inject
    public EmailUtility(final PropertyManagerService propertyManagerService) {
        this.propertyManagerService = Validate.notNull(propertyManagerService, "A PropertyManagerService class must be provided");
    }

    private Session getSmtpSession() {
        String user = propertyManagerService.getCommonProperty("mail.smtp.user");
        String password = propertyManagerService.getCommonProperty("mail.smtp.password");

        Properties properties = System.getProperties();
        properties.put("mail.smtp.host", propertyManagerService.getCommonProperty("mail.smtp.host"));
        properties.put("mail.smtp.port", propertyManagerService.getCommonProperty("mail.smtp.port"));
        properties.put("mail.smtp.auth", propertyManagerService.getCommonProperty("mail.smtp.auth"));
        properties.put("mail.smtp.starttls.enable", propertyManagerService.getCommonProperty("mail.smtp.starttls.enable"));

        return Session.getDefaultInstance(properties, new SmtpAuthenticator(user, password));
    }

    public void sendTextEmail(String from, String to, String subject, String textMessage) throws Exception {

        Session session = getSmtpSession();

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setText(textMessage);

            // Send message
            Transport.send(message);
        } catch (MessagingException ex) {
            throw new Exception("Error al enviar email. TO: [" + to + "] - FROM: [" + from + "]. " + ex, ex);
        }
    }

    public void sendHTMLEmail(String from, String to, String subject, String htmlMessage) throws Exception {

        Session session = getSmtpSession();

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setContent(htmlMessage, "text/html; charset=utf-8");

            // Send message
            Transport.send(message);
        } catch (MessagingException ex) {
            throw new Exception("Error al enviar email. TO: [" + to + "] - FROM: [" + from + "]. " + ex, ex);
        }
    }

    public class SmtpAuthenticator extends Authenticator {

        private final String username;
        private final String password;

        public SmtpAuthenticator(String username, String password) {
            super();
            this.username = username;
            this.password = password;
        }

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            if ((username != null) && (username.length() > 0) && (password != null)
                    && (password.length() > 0)) {

                return new PasswordAuthentication(username, password);
            }

            return null;
        }
    }
}
