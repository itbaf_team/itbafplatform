/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.QCountry;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPACountryRepository extends AbstractJPARepositorySession<Country, Long> {

    @Inject
    public JPACountryRepository(final Provider<EntityManager> entityManagerProvider) {
        super(Country.class, entityManagerProvider);
    }

    public Country findByCode(String code) {
        if (code == null) {
            String error = "El code de Country es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }
        code = code.toUpperCase();
        QCountry country = QCountry.country;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Country c = query.select(country).from(country).
                where(country.code.eq(code)).fetchFirst();

        return c;
    }

    public List<Country> getByStatus(Status status) {
        QCountry country = QCountry.country;
        JPAQuery<?> query = new JPAQuery(getEntityManager());
        List<Country> c;
        if (status == null) {
            c = query.select(country).from(country)
                    .where(country.id.ne(1L))
                    .orderBy(country.name.asc())
                    .fetch();
        } else {
            c = query.select(country).from(country).
                    where(ObjectPredicates.combine(country.status.eq(status)), country.id.ne(1L))
                    .orderBy(country.name.asc())
                    .fetch();
        }
        return c;
    }

    public Country findByInternationalNumberPhone(String phone) {

        if (phone == null) {
            String error = "El MSISDN de Country es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }

        phone = phone.replace("+", "");

        QCountry country = QCountry.country;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Country c = query.select(country).from(country).
                where(ObjectPredicates.combine(country.phoneCode.isNotNull(),
                        Expressions.asString(phone).like(country.phoneCode.stringValue().concat("%"))))
                .fetchFirst();

        return c;
    }

    public Country getCountryByCurrencyCode(String currencyCode) {
        if (currencyCode == null) {
            String error = "El currencyCode de Country es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }
        currencyCode = currencyCode.toUpperCase();
        QCountry country = QCountry.country;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Country c = query.select(country).from(country).
                where(country.currency.eq(currencyCode))
                .fetchFirst();

        return c;
    }
}
