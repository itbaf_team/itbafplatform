/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module com.itbaf.platform.services {
    exports com.itbaf.platform.services;
    exports com.itbaf.platform.services.service;
    requires jersey.guice;
    requires javax.inject;
    requires jersey.server;
    requires jersey.core;
    requires jersey.client;
    requires jersey.servlet;
    requires jersey.json;
    requires jettison;
    requires jackson.core.asl;
    requires jackson.mapper.asl;
    requires jackson.jaxrs;
    requires jackson.xc;
    requires jersey.multipart;
    requires itbaf.jaxb;
}
