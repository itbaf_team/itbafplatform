/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.model.sms.AdapterSmsLogReceived;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPAAdapterSmsLogReceivedRepository;
import com.itbaf.platform.paymenthub.services.repository.AdapterSmsLogReceivedService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class AdapterSmsLogReceivedServiceTransactional implements AdapterSmsLogReceivedService {

    private final JPAAdapterSmsLogReceivedRepository adapterSmsLogReceivedRepository;

    @Inject
    public AdapterSmsLogReceivedServiceTransactional(final JPAAdapterSmsLogReceivedRepository adapterSmsLogReceivedRepository) {
        this.adapterSmsLogReceivedRepository = Validate.notNull(adapterSmsLogReceivedRepository, "A JPAAdapterSmsLogReceivedRepository class must be provided");
    }

    @Override
    public void saveOrUpdate(AdapterSmsLogReceived adapterSmsLogReceived) throws Exception {
        if (adapterSmsLogReceived.getId() != null) {
            adapterSmsLogReceivedRepository.merge(adapterSmsLogReceived);
        } else {
            adapterSmsLogReceivedRepository.save(adapterSmsLogReceived);
            if (adapterSmsLogReceived.getId() == null) {
                log.error("Adapter. No se registro el ID para: [" + adapterSmsLogReceived + "]");
                adapterSmsLogReceivedRepository.save(adapterSmsLogReceived);
            }
        }
    }

}
