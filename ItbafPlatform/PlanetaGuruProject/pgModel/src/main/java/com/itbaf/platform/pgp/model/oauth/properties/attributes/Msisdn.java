/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties.attributes;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Provider;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class Msisdn extends UsernameAttribute implements Serializable {

    private static final long serialVersionUID = 20181207172504L;

    private String msisdn;
    private Country country;
    private Provider provider;

    public Msisdn() {
        date = new Date();
        this.primary = false;
        this.confirmed = false;
    }

    public Msisdn(String msisdn, Boolean confirmed, Country country, Provider provider) {
        date = new Date();
        this.msisdn = msisdn.replace("+", "");
        this.confirmed = confirmed;
        this.country = country;
        this.provider = provider;
        this.primary = false;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("msisdn", msisdn)
                .add("confirmed", confirmed)
                .add("primary", primary)
                .add("country", country)
                .add("provider", provider)
                .omitNullValues().toString();
    }

    public void setCountry(Country country) {
        this.country = Country.basicClone(country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(msisdn, confirmed, country, provider, primary);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Msisdn other = (Msisdn) obj;
        if (!Objects.equals(this.msisdn, other.msisdn)) {
            return false;
        }
        if (!Objects.equals(this.confirmed, other.confirmed)) {
            return false;
        }
        if (!Objects.equals(this.primary, other.primary)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.provider, other.provider)) {
            return false;
        }
        return true;
    }

}
