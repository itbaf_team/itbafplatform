/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.subscription;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.itbaf.platform.paymenthub.model.*;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import com.querydsl.core.annotations.Config;
import java.util.List;
import javax.persistence.Transient;

/**
 *
 * @author javier
 */
@Entity(name = "SUBSCRIPTION_REGISTRY")
@Table(name = "SUBSCRIPTION_REGISTRY", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors = true)
public class SubscriptionRegistry extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 20170809155413L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    /* 
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE", nullable = true)
    private Date createdDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATE", nullable = true)
    private Date lastUpdate;
    */
    @Column(name = "USER_ACCOUNT")
    private String userAccount;
    @ManyToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;
    @JsonIgnore
    @ManyToOne(optional = false)
    @JoinColumn(name = "SUBSCRIPTION_ID")
    private Subscription subscription;
    @Column(name = "CHANNEL_IN")
    private String channelIn;
    @Column(name = "CHANNEL_OUT")
    private String channelOut;
    // Identificador unico de la suscripcion en el Proveedor
    @Column(name = "EXTERNAL_ID")
    private String externalId;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SUBSCRIPTION_DATE")
    private Date subscriptionDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UNSUBSCRIPTION_DATE")
    private Date unsubscriptionDate;
    @Column(name = "ORIGIN_SUBSCRIPTION")
    private String originSubscription;
    @Column(name = "ORIGIN_UNSUBSCRIPTION")
    private String originUnsubscription;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SYNC_DATE")
    private Date syncDate;
    @Type(type = "json")
    @Column(name = "ADNETWORK_TRACKING", columnDefinition = "json")
    private Object adnetworkTracking;
    @Type(type = "json")
    @Column(name = "EXTRA", columnDefinition = "json")
    private Extra extra;

    @Transient
    private List<SubscriptionBilling> subscriptionBillings;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("userAccount", userAccount)
                .add("subscriptionDate", subscriptionDate)
                .add("originSubscription", originSubscription)
                .add("originUnsubscription", originUnsubscription)
                .add("sop", sop != null ? sop.getId() : null)
                .add("channelIn", channelIn)
                .add("channelOut", channelOut)
                .add("externalId", externalId)
                .add("subscription", subscription != null ? subscription.getId() : null)
                .add("unsubscriptionDate", unsubscriptionDate)
                .add("syncDate", syncDate)
                .add("adnetworkTracking", adnetworkTracking)
                .add("extra", extra)
                .add("subscriptionBillings", subscriptionBillings)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, originSubscription, originUnsubscription, syncDate,
                externalId, sop, channelIn, channelOut, subscription,
                subscriptionDate, extra, unsubscriptionDate, userAccount, adnetworkTracking);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<SubscriptionRegistry>()
                .over(SubscriptionRegistry::getId)
                .over(SubscriptionRegistry::getOriginSubscription)
                .over(SubscriptionRegistry::getOriginUnsubscription)
                .over(SubscriptionRegistry::getChannelIn)
                .over(SubscriptionRegistry::getChannelOut)
                .over(SubscriptionRegistry::getExternalId)
                .over(SubscriptionRegistry::getSop)
                .over(SubscriptionRegistry::getSubscription)
                .over(SubscriptionRegistry::getSubscriptionDate)
                .over(SubscriptionRegistry::getExtra)
                .over(SubscriptionRegistry::getUnsubscriptionDate)
                .over(SubscriptionRegistry::getUserAccount)
                .over(SubscriptionRegistry::getAdnetworkTracking)
                .over(SubscriptionRegistry::getSyncDate)
                .asserts(this, obj);
    }
}
