/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adn;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.paymenthub.adn.consumer.RabbitMQAdnetworkMessageProcessorFactory;
import com.itbaf.platform.paymenthub.adn.processor.AdnetworkProcessor;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.rmq.services.RabbitMQMemoryProcessorImpl;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain {

    private final PHInstance instance;
    private final ExecutorService cachedThreadPool;
    private final RabbitMQConsumer rabbitMQConsumer;
    private final BasicDataBaseConnection dataBaseConnection;
    private final RabbitMQMemoryProcessorImpl memoryProcessor;
    private final ScheduledExecutorService scheduledThreadPool;
    private final PHProfilePropertiesService profilePropertiesService;
    private final RabbitMQAdnetworkMessageProcessorFactory rabbitMQMessageProcessorFactory;
    private final Map<PaymentHubMessage.NotificationType, AdnetworkProcessor> notificationProcessorMap;

    @Inject
    public PaymentHubMain(final PHInstance instance,
            final ExecutorService cachedThreadPool,
            final ScheduledExecutorService scheduledThreadPool,
            final RabbitMQConsumer rabbitMQConsumer,
            final BasicDataBaseConnection dataBaseConnection,
            final RabbitMQMemoryProcessorImpl memoryProcessor,
            final PHProfilePropertiesService profilePropertiesService,
            final RabbitMQAdnetworkMessageProcessorFactory rabbitMQMessageProcessorFactory,
            final Set<AdnetworkProcessor> adnetworkProcessorSet) {
        this.instance = Validate.notNull(instance, "A PHInstance class must be provided");
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "An ExecutorService class must be provided");
        this.rabbitMQConsumer = Validate.notNull(rabbitMQConsumer, "A RabbitMQConsumer class must be provided");
        this.memoryProcessor = Validate.notNull(memoryProcessor, "A RabbitMQMemoryProcessorImpl class must be provided");
        this.dataBaseConnection = Validate.notNull(dataBaseConnection, "A BasicDataBaseConnection class must be provided");
        this.scheduledThreadPool = Validate.notNull(scheduledThreadPool, "A ScheduledExecutorService class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A ProfilePropertiesService class must be provided");
        this.rabbitMQMessageProcessorFactory = Validate.notNull(rabbitMQMessageProcessorFactory, "A RabbitMQMessageProcessorFactory class must be provided");

        notificationProcessorMap = new HashMap();
        if (adnetworkProcessorSet != null) {
            log.info("adnetworkProcessorSet size: " + adnetworkProcessorSet.size());
            for (AdnetworkProcessor nr : adnetworkProcessorSet) {
                log.info("JF- AdnetworkProcessor item: [" + nr.getNotificationType() + "] - [" + nr.getClass().getSimpleName() + "]");
                notificationProcessorMap.put(nr.getNotificationType(), nr);
            }
        }
    }

    public void start() {
        log.info("Initializing app... AdNotifier");
        try {
            rabbitMQConsumer.createFanoutChannel(CHACHE_MEMORY_UPDATE, memoryProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer '" + CHACHE_MEMORY_UPDATE + "'. " + ex, ex);
        }

        Set<Provider> providers = instance.getConfigurations().keySet();
        for (Provider p : providers) {
            ChannelQuantityProvider cqp = instance.getConfigurations().get(p);

            if (cqp.getSubscriptionConsumerQuantity() > 0) {
                createConsumer(profilePropertiesService.getCommonProperty("rabbitmq.paymenthub.adn.subscriptions"),
                        cqp.getSubscriptionConsumerQuantity(),
                        notificationProcessorMap.get(PaymentHubMessage.NotificationType.SUBSCRIPTION));
                createConsumer(CommonFunction.BORDER_NOTIFICATION + CommonFunction.PARTIAL_ADN_NOTIFICATION,
                        cqp.getSubscriptionConsumerQuantity(),
                        notificationProcessorMap.get(PaymentHubMessage.NotificationType.ADN_SIMPLE));

            }
            if (cqp.getBillingConsumerQuantity() > 0) {
                createConsumer(profilePropertiesService.getCommonProperty("rabbitmq.paymenthub.adn.billings"),
                        cqp.getBillingConsumerQuantity(),
                        notificationProcessorMap.get(PaymentHubMessage.NotificationType.BILLING));
            }
            if (cqp.getBillingConsumerQuantity() > 0) {
                createConsumer(profilePropertiesService.getCommonProperty("rabbitmq.paymenthub.adn.billings.od"),
                        cqp.getBillingConsumerQuantity(),
                        notificationProcessorMap.get(PaymentHubMessage.NotificationType.ONDEMAND_BILLING));
            }
        }
        log.info("Initializing app OK. AdNotifier");
    }

    private RabbitMQConsumer createConsumer(String exchangeName, Integer consumerQuantity, AdnetworkProcessor notificationProcessor) {
        rabbitMQConsumer.createChannel(exchangeName, consumerQuantity, createRabbitMQMessageProcessor(notificationProcessor));
        return rabbitMQConsumer;
    }

    private RabbitMQMessageProcessor createRabbitMQMessageProcessor(AdnetworkProcessor messageProcessor) {
        return rabbitMQMessageProcessorFactory.create(messageProcessor);
    }

    public void stop() {
        rabbitMQConsumer.stopConsumers();
        stopThreads();
        dataBaseConnection.shutdownConnectionsPool();
        log.info("App destroyed. AdNotifier");
    }

    public void stopThreads() {
        log.info("Finalizando Threads en 5 segundos... ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }
        try {
            log.info("Finalizando scheduledThreadPool ");
            scheduledThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            log.info("Esperando Threads por 2 segundos... ");
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        try {
            scheduledThreadPool.shutdownNow();
            log.info("Finalizado scheduledThreadPool ");
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdownNow();
        } catch (Exception ex) {
        }
    }

}
