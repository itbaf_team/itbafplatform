package com.itbaf.platform.paymenthub.extention.spiralis.modules;

import com.google.inject.Singleton;
import com.itbaf.platform.commons.filter.RequestFilter;
import com.itbaf.platform.paymenthub.commons.rest.ManagerRestServiceImpl;
import com.itbaf.platform.paymenthub.extention.spiralis.rest.InnerRestService;
import com.itbaf.platform.paymenthub.extention.spiralis.rest.NotificationRestService;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import java.util.HashMap;

public class GuiceRestServicesConfigModule extends JerseyServletModule {

    @Override
    protected void configureServlets() {

        bind(InnerRestService.class).in(Singleton.class);
        bind(ManagerRestServiceImpl.class).in(Singleton.class);
        bind(NotificationRestService.class).in(Singleton.class);
        HashMap<String, String> options = new HashMap();
        options.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

        //Filters
        this.filter("/rest/*").through(RequestFilter.class);

        //Servlets
        serve("/rest/*").with(GuiceContainer.class, options);
    }
}
