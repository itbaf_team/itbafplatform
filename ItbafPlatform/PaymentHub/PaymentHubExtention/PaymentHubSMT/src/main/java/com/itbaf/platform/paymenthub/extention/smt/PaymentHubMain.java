/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.smt;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.PaymentHubBasic;
import com.itbaf.platform.paymenthub.extention.smt.consumer.RabbitMQSMSMessageProcessorImpl;
import com.itbaf.platform.paymenthub.extention.smt.handler.BNSMSProcessorHandler;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.paymenthub.services.repository.PHInstanceService;
import org.apache.commons.lang3.Validate;
import static com.itbaf.platform.commons.CommonFunction.BORDER_NOTIFICATION;
import com.itbaf.platform.paymenthub.extention.smt.handler.MDivulgaProcessorHandler;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain extends PaymentHubBasic {

    private final PHInstance instance;
    private final MDivulgaProcessorHandler mDivulgaProcessorHandler;
    private final RabbitMQSMSMessageProcessorImpl smsMessageProcessor;
    private final BNSMSProcessorHandler bnSMSProcessorHandler;
    public static final String QUEUE_SMT_SMS_MO = "ph.smt.sms.mo";

    @Inject
    public PaymentHubMain(final PHInstance instance,
            final PHInstanceService instanceService,
            final MDivulgaProcessorHandler mDivulgaProcessorHandler,
            final RabbitMQSMSMessageProcessorImpl smsMessageProcessor,
            final BNSMSProcessorHandler bnSMSProcessorHandler) {
        this.instance = Validate.notNull(instance, "A PHInstance class must be provided");
        this.smsMessageProcessor = smsMessageProcessor;
        this.mDivulgaProcessorHandler=mDivulgaProcessorHandler;
        this.bnSMSProcessorHandler = bnSMSProcessorHandler;
    }

    @Override
    protected void startActions() {
        super.startActions();
        try {
            for (Provider p : providerSet) {
                try {
                    ChannelQuantityProvider cqp = instance.getConfigurations().get(p);
                    rabbitMQConsumer.createChannel(PaymentHubMain.QUEUE_SMT_SMS_MO+"."+p.getName(), cqp.getSmppConsumerQuantity(), smsMessageProcessor);
                } catch (Exception ex) {
                    log.error("Error al crear consumer para provider: [" + p + "]. " + ex, ex);
                }
                try {
                    ChannelQuantityProvider cqp = instance.getConfigurations().get(p);
                    rabbitMQConsumer.createChannel(BORDER_NOTIFICATION + p.getName() + ".sms", cqp.getSmppConsumerQuantity(), bnSMSProcessorHandler);
                } catch (Exception ex) {
                    log.error("Error al crear consumer para provider: [" + p + "]. " + ex, ex);
                }
                try {
                    ChannelQuantityProvider cqp = instance.getConfigurations().get(p);
                    rabbitMQConsumer.createChannel(BORDER_NOTIFICATION + p.getName() + ".mdivulga", cqp.getSubscriptionConsumerQuantity(), mDivulgaProcessorHandler);
                } catch (Exception ex) {
                    log.error("Error al crear consumer para provider: [" + p + "]. " + ex, ex);
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar sincronizacion. " + ex, ex);
        }
    }
}
