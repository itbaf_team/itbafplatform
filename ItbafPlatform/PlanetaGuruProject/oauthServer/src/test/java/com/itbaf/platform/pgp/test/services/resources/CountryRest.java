/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.test.services.resources;

import java.util.List;
import java.util.Set;

/**
 *
 * @author JF
 */
public class CountryRest {

    public String name;
    public String alpha2Code;
    public String nativeName;
    public List<Currency> currencies;
    public Set<LanguageRest> languages;
}
