/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.messaging.rest.pg;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class PGMessageResponse {

    public Meta meta = new Meta();
    public Data data = new Data();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("meta", meta)
                .add("data", data)
                .omitNullValues().toString();
    }
}
