package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Phone {

    private String area_code;
    private String number;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("area_code", area_code)
                .add("number", number)
                .omitNullValues().toString();
    }
}
