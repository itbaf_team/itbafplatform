#!/bin/bash
docker run --rm -p 9815:9815 \
--name=ph-bn \
-e "paymenthub.datasource.url=jdbc:mysql://itbafmysql:3306/paymentHub?verifyServerCertificate=false&useSSL=false&requireSSL=false&useUnicode=true&characterEncoding=UTF-8&characterSetResult=UTF-8" \
-v "$PWD":/usr/src/myapp \
-w /usr/src/myapp \
--network=paymenthub_itbaf_red \
openjdk:11-jre java -jar PaymentHub/PaymentHubExtention/PaymentHubBN/target/PaymentHubBN.jar