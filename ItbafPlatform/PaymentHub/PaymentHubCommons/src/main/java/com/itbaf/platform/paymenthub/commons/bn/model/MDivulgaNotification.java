/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.bn.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
@lombok.Getter
@lombok.Setter
public class MDivulgaNotification extends BNNotification {

    private String msisdn;
    private String timestamp;
    private String product;
    private String servicecode;
    private String name;
    public String thread = Thread.currentThread().getName();

    public MDivulgaNotification() {
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("msisdn", msisdn)
                .add("timestamp", timestamp)
                .add("product", product)
                .add("servicecode", servicecode)
                .add("name", name)
                .add("thread", thread)
                .omitNullValues().toString();
    }
}
