#!/bin/bash
# mvn -B -e --also-make  --projects PaymentHub/PaymentHubServer/ -s ./settings.xml -Dmaven.test.skip=true  clean compile install package
#-v ~/.m2:/var/maven/.m2
docker run -it --rm  \
-v "$(pwd)"/dev/.m2:/root/.m2 \
-v "$(pwd)":/usr/src  \
-w /usr/src maven:3-jdk-11 \
mvn -B -e --projects PaymentHub/PaymentHubModel/ -s ./settings.xml -Dmaven.test.skip=true clean install #clean compile install package
