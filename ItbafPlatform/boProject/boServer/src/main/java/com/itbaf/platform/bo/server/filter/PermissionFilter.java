/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.bo.server.filter;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.services.filter.ViewRequestFilter;
import com.itbaf.platform.pgp.services.repository.UserService;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import java.util.Collection;
import java.util.Map;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.Priorities;
import javax.annotation.Priority;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.pgp.model.secured.Permission;

/**
 *
 * @author JF
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
@lombok.extern.log4j.Log4j2
public final class PermissionFilter implements ContainerRequestFilter {

    private final UserService userService;
    private final ImmutableSet<String> requiredPermissions;

    public PermissionFilter(final UserService userService,
            final ImmutableSet<String> requiredPermissions) {
        this.userService = Validate.notNull(userService, "An UserService class must be provided");
        this.requiredPermissions = Validate.notNull(requiredPermissions, "A ImmutableSet<String> class must be provided");
    }

    @Override
    public ContainerRequest filter(ContainerRequest request) {

        try {
            String aux = request.getQueryParameters().getFirst("u_code");
            if (aux == null) {
                aux = request.getCookies().get("Ax_PG").getValue();
            }
            String userId = ViewRequestFilter.memory.getIfPresent(aux);
            User user = userService.findById(Long.parseLong(userId));
            boolean control = false;
            Map<String, Permission> permissions = user.getPermissions();
            if (permissions == null || permissions.isEmpty()) {
                aux = Joiner.on(", ").join(requiredPermissions);
                control = true;
            } else {
                final Collection<String> missingRequiredParams = Sets.difference(requiredPermissions, permissions.keySet());
                if (!missingRequiredParams.isEmpty()) {
                    aux = Joiner.on(", ").join(missingRequiredParams);
                    control = true;
                }
            }
            if (control) {
                // setBadRequest(aux);
                setRedirect302();
            }

            return request;
        } catch (Exception ex) {
        }

        setBadRequest("account-login");
        // setRedirect302();
        return null;
    }

    private void setRedirect302() {
        java.net.URI location = null;
        try {
            location = new java.net.URI(ViewRequestFilter.productURL);
        } catch (Exception ex) {
        }
        final Response response = Response.status(302).location(location).build();

        throw new WebApplicationException(response);
    }

    private void setBadRequest(String text) {
        final String message = "Required permission missing: [account-login]";
        final Response response = Response.status(Response.Status.BAD_REQUEST).entity(message)
                .header("pg-location", ViewRequestFilter.productURL)
                .build();
        throw new WebApplicationException(response);
    }

}
