
package org.csapi.schema.parlayx.subscribeproduct.v1_0.local;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.SubScriptionInfoType;


/**
 * <p>Java class for getSubScriptionListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getSubScriptionListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="subScriptionInfos" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}SubScriptionInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSubScriptionListResponse", propOrder = {
    "resultCode",
    "subScriptionInfos"
})
public class GetSubScriptionListResponse {

    protected int resultCode;
    protected List<SubScriptionInfoType> subScriptionInfos;

    /**
     * Gets the value of the resultCode property.
     * 
     */
    public int getResultCode() {
        return resultCode;
    }

    /**
     * Sets the value of the resultCode property.
     * 
     */
    public void setResultCode(int value) {
        this.resultCode = value;
    }

    /**
     * Gets the value of the subScriptionInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subScriptionInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubScriptionInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubScriptionInfoType }
     * 
     * 
     */
    public List<SubScriptionInfoType> getSubScriptionInfos() {
        if (subScriptionInfos == null) {
            subScriptionInfos = new ArrayList<SubScriptionInfoType>();
        }
        return this.subScriptionInfos;
    }

}
