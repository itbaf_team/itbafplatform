/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.bn.model;

import com.google.common.base.MoreObjects;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class TimweNotification extends BNNotification {

    private String provider;
    private String external_tx_id;//external-tx-id -> Transaction ID
    private String partnerRole;
    private String realm;
    private String notificationType;
    private Long productId;
    private Long pricepointId;
    private String mcc;
    private String mnc;
    private String text;
    private String msisdn;
    private String userIdentifier;
    private String largeAccount;//Shortcode
    private String transactionUUID;
    private String mnoDeliveryCode;
    private String entryChannel;
    private String apikey;
    private String authentication;
    private List<String> tags;
    private String trackingId;

    public String thread = Thread.currentThread().getName();

    public TimweNotification() {

    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("provider", provider)
                .add("external_tx_id", external_tx_id)
                .add("partnerRole", partnerRole)
                .add("realm", realm)
                .add("notificationType", notificationType)
                .add("productId", productId)
                .add("pricepointId", pricepointId)
                .add("mcc", mcc)
                .add("mnc", mnc)
                .add("msisdn", msisdn)
                .add("userIdentifier", userIdentifier)
                .add("largeAccount", largeAccount)
                .add("text", text)
                .add("transactionUUID", transactionUUID)
                .add("mnoDeliveryCode", mnoDeliveryCode)
                .add("entryChannel", entryChannel)
                .add("apikey", apikey)
                .add("authentication", authentication)
                .add("tags", (tags == null ? tags : Arrays.toString(tags.toArray())))
                .add("thread", thread)
                .add("trackingId", trackingId)
                .omitNullValues().toString();
    }

}
