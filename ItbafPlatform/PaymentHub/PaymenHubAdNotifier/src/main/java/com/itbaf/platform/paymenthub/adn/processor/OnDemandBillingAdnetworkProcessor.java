/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adn.processor;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class OnDemandBillingAdnetworkProcessor extends AdnetworkProcessor {

    private static final PaymentHubMessage.NotificationType notificationType = PaymentHubMessage.NotificationType.ONDEMAND_BILLING;

    @Inject
    public OnDemandBillingAdnetworkProcessor() {

    }

    @Override
    public PaymentHubMessage.NotificationType getNotificationType() {
        return notificationType;
    }

    @Override
    public Boolean process(String msg) throws Exception {
        log.info("No se procesara ADN OnDemand Billing [" + msg + "]");
        return false;
    }

}
