/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.persistence.connection;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author javier
 */
public class BasicDBConnectionConfig {

    private final String appName;
    private final Map<String, DBConnectionConfig> config = new HashMap();

    public BasicDBConnectionConfig(String appName) {
        this.appName = appName;
    }

    public Map<String, DBConnectionConfig> getConfig() {
        return config;
    }

    public String getAppName() {
        return appName;
    }

    
}
