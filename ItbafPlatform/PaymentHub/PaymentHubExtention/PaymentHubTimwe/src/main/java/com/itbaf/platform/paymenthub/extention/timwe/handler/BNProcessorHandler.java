/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.handler;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.bn.BNProcessor;
import com.itbaf.platform.paymenthub.commons.bn.model.BorderNotificationMessage;
import com.itbaf.platform.paymenthub.commons.bn.model.TimweNotification;
import java.util.Map;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class BNProcessorHandler extends BNProcessor {

    @Inject
    private Map<String, ServiceProcessorHandler> sphMap;

    @Override
    public Boolean processMessage(String message) {
        try {
            BorderNotificationMessage bm = mapper.readValue(message, BorderNotificationMessage.class);
            TimweNotification tn = (TimweNotification) bm.notification;
            sphMap.get(tn.getProvider()).processNotification((TimweNotification) bm.notification);
            return true;
        } catch (Exception ex) {
            log.error("No fue posible interpretar la notificacion: [" + message + "]. " + ex, ex);
        }

        return null;
    }

}
