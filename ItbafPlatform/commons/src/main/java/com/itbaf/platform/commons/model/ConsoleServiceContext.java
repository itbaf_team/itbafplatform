package com.itbaf.platform.commons.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JF
 */
@lombok.Getter
@lombok.Setter
public class ConsoleServiceContext {

    private String service;
    private String version;

    public ConsoleServiceContext(String service) {
        this.service = service;
    }
}
