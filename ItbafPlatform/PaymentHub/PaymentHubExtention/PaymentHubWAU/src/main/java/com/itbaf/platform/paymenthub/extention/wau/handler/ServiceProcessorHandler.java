/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.wau.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.bn.model.WAUNotification;
import com.itbaf.platform.paymenthub.extention.wau.model.ResponseSubscribePin;
import com.itbaf.platform.paymenthub.extention.wau.model.ResponseSubscriptionStatus;
import com.itbaf.platform.paymenthub.extention.wau.model.Subscriber;
import com.itbaf.platform.paymenthub.extention.wau.resources.WauResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import io.jsonwebtoken.lang.Collections;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;
import org.redisson.api.RMapCache;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final WauResources wauResources;

    @Inject
    public ServiceProcessorHandler(final WauResources wauResources) {
        this.wauResources = Validate.notNull(wauResources, "A WauResources class must be provided");
    }

    public void processNotification(final WAUNotification notification) throws Exception {

        SOP sop = sopService.findBySettings("wau_productId", notification.product_id);
        if (sop == null) {
            log.fatal("No hay configurado un SOP configurado para: [" + notification.product_id + "]");
            return;
        }

        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notification);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + sop.getProvider().getName(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notification);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + sop.getProvider().getName(), jsonResponse);
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {
        WAUNotification notification;
        try {
            notification = mapper.readValue(message, WAUNotification.class);
            Thread.currentThread().setName(notification.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }

        SOP sop = sopService.findBySettings("wau_productId", notification.product_id);
        if (sop == null) {
            log.error("No hay configurado un SOP configurado para: [" + notification.product_id + "]");
            return null;
        }

        final String threadName = Thread.currentThread().getName() + "-" + sop.getProvider().getName() + ".n";
        Thread.currentThread().setName(threadName);

        Boolean response = null;
        try {
            notification.mobile_number = this.userAccountNormalizer(notification.mobile_number, sop.getProvider());

            switch (notification.type) {
                case SubscriptionResult: // Notificacion de Alta
                    // log.info("Notificacion de alta. [" + notification.mobile_number + "]");
                    response = processSubscriptionNotification(notification, sop);
                    if (response != null) {
                        response = processBillingNotification(notification, sop);
                    }
                    break;
                case SubscriptionEvent: // Notificacion de otro tipo
                    switch (notification.event) {
                        case renew: //The subscription has been renewed
                        case partial: //Subscription has been charged for fraction/partial charge.
                            if (notification.tariff == null) {
                                // log.info("Notificacion de cobro. Ignorada");
                                return true;
                            }
                            //  log.info("Notificacion de cobro. [" + notification.mobile_number + "]");
                            response = processBillingNotification(notification, sop);
                            break;
                        case optout: //The subscription has been cancelled by user by sending the optout keywords or by calling del carrier call center
                        case cancelled: //Subscription has been cancelled by the platform. 
                        case expired: //Subscription has been cancelled by subscription expired. 
                            //  log.info("Notificacion de baja. [" + notification.mobile_number + "]");
                            response = processUnsubscriptionNotification(notification, sop);
                            break;
                        case pendingcharge: //Subscription could not be charged, but the platform will try to retry to charge the user later
                            // log.info("Notificacion de cobro pendiente. Ignorada");
                            response = true;
                            break;
                    }
                    break;
            }
        } catch (Exception ex) {
            log.error("Error al procesar notificacion: [" + notification + "]. " + ex, ex);
        }
        return response;
    }

    private Boolean processSubscriptionNotification(WAUNotification notification, SOP sop) {

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(sop.getProvider().getCountry().getCode());
            msg.setFromDate(new Date());
            if (notification.status == null && notification.tariff != null) {
                notification.status = "active";
            } else if (notification.status == null) {
                notification.status = "pendingcharge";
            }

            switch (notification.status.toLowerCase()) {
                case "active":
                case "partial":
                    msg.setMessageType(MessageType.SUBSCRIPTION_ACTIVE);
                    break;
                case "free":
                    msg.setMessageType(MessageType.SUBSCRIPTION_TRIAL);
                    break;
                case "pendingcharge":
                    msg.setMessageType(MessageType.SUBSCRIPTION_PENDING);
                    break;
            }
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(sop.getMainTariff().getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.subscription_id);
            msg.setTransactionId(notification.request_id);
            msg.setChannelIn(notification.getChannel());
            msg.setUserAccount(notification.mobile_number);
            Extra extra = new Extra();
            extra.keyword = notification.keyword;
            msg.setExtra(extra);

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar WauNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    private Boolean processUnsubscriptionNotification(WAUNotification notification, SOP sop) {

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(sop.getProvider().getCountry().getCode());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setToDate(locateDate("yyyy-MM-dd HH:mm:ss", notification.event_date, TimeZone.getTimeZone("UTC")));
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setTariffId(sop.getMainTariff().getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.subscription_id);
            msg.setChannelOut(notification.getChannel());
            msg.setUserAccount(notification.mobile_number);

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar WauNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private Boolean processBillingNotification(WAUNotification notification, SOP sop) {

        if (notification.tariff == null) {
            return false;
        }
        if (notification.status != null) {
            switch (notification.status.toLowerCase()) {
                case "free":
                case "pendingcharge":
                    return false;
            }
        }

        Set<Tariff> set = sop.getTariffs();
        Tariff t = null;

        for (Tariff aux : set) {
            if (aux.getExternalId().equals(notification.tariff)) {
                t = aux;
                break;
            }
        }

        if (t == null) {
            log.error("No existe una tarifa configurada para: [" + notification.tariff + "]");
            return null;
        }

        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(sop.getProvider().getCountry().getCode());
            msg.setCurrencyId(sop.getProvider().getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            if (notification.event_date == null) {
                msg.setFromDate(new Date());
                msg.setTransactionId(notification.product_id + "_" + notification.tariff + "_" + notification.subscription_id);
            } else {
                msg.setFromDate(locateDate("yyyy-MM-dd HH:mm:ss", notification.event_date, TimeZone.getTimeZone("UTC")));
                msg.setTransactionId(notification.product_id + "_" + notification.tariff + "_" + notification.subscription_id + "_" + msg.getFromDate().getTime());
            }
            msg.setChargedDate(msg.getFromDate());
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setSopId(sop.getId());
            msg.setUserAccount(notification.mobile_number);
            msg.setExternalId(notification.subscription_id);

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar WauNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {
        throw new Exception("No implementado");
    }

    @Override
    public void syncSubscriptions(final Provider provider) {
        try {
            ChannelQuantityProvider cqp = instance.getConfigurations().get(provider);
            if (!cqp.isSyncConsumer()) {
                log.info("Provider tiene Sync deshabilitado");
                return;
            }

            new Thread(() -> {
                try {
                    Thread.currentThread().setName(CommonFunction.getTID("sync.full.T." + provider.getName()));
                    RLock lock = instrumentedObject.lockObject("DAVID_SYNC_" + provider.getName(), 60);
                    long aux = instrumentedObject.getAtomicLong("DAVID_SYNC_A_" + provider.getName(), 1400);

                    if (aux == 0) {
                        instrumentedObject.getAndIncrementAtomicLong("DAVID_SYNC_A_" + provider.getName(), 1400);
                        instrumentedObject.getAndIncrementAtomicLong("DAVID_SYNC_A_" + provider.getName(), 1400);
                        try {
                            syncWauSubscriptions(provider);
                        } catch (Exception ex) {
                            log.error("Error al sincronizar la DB local con la DB de Wau. " + ex, ex);
                        }
                    }
                    instrumentedObject.unLockObject(lock);
                } catch (Exception ex) {
                }
            }).start();
        } catch (Exception ex) {
        }

        super.syncSubscriptions(provider);
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseSubscriptionStatus rs = wauResources.subscriptionStatus(subscription, rm);
        if (rs != null) {
            rm.status = ResponseMessage.Status.REMOVED;
            if (rs.getSubscription_status() != null) {
                switch (rs.getSubscription_status()) {
                    case "1":
                        subscription.setStatus(Subscription.Status.ACTIVE);
                        rm.status = ResponseMessage.Status.ACTIVE;
                        processStatus(subscription, rs);
                        break;
                    case "4": // Pendiente de cobro. En cualquier momento
                        if (!Subscription.Status.ACTIVE.equals(subscription.getStatus())
                                || (rs.getSubscription_id() != null && subscription.getSubscriptionRegistry() != null
                                && !rs.getSubscription_id().equals(subscription.getSubscriptionRegistry().getExternalId()))) {
                            subscription.setStatus(Subscription.Status.PENDING);
                            rm.status = ResponseMessage.Status.PENDING;
                        } else {
                            rm.status = ResponseMessage.Status.ACTIVE;
                        }
                        processStatus(subscription, rs);
                        break;
                    case "0":
                    case "2":
                    case "3":
                    case "5":
                        subscription.setStatus(Subscription.Status.REMOVED);
                        rm.status = ResponseMessage.Status.REMOVED;
                        processStatus(subscription, rs);
                        if (subscription.getSubscriptionRegistry() != null) {
                            if (subscription.getSubscriptionRegistry().getOriginUnsubscription() == null) {
                                subscription.getSubscriptionRegistry().setOriginUnsubscription("FACADE:STATUS");
                            }
                            if (subscription.getSubscriptionRegistry().getUnsubscriptionDate() == null) {
                                subscription.getSubscriptionRegistry().setUnsubscriptionDate(new Date());
                            }
                        }
                }
            }

            if ("SVC013".equals(rs.getResult_code())) {
                subscription.setStatus(Subscription.Status.BLACKLIST);
                rm.status = ResponseMessage.Status.BLACKLIST;
                processStatus(subscription, rs);
                if (subscription.getSubscriptionRegistry() != null) {
                    if (subscription.getSubscriptionRegistry().getOriginUnsubscription() == null) {
                        subscription.getSubscriptionRegistry().setOriginUnsubscription("FACADE:STATUS");
                    }
                    if (subscription.getSubscriptionRegistry().getUnsubscriptionDate() == null) {
                        subscription.getSubscriptionRegistry().setUnsubscriptionDate(new Date());
                    }
                }
            }
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        return rm;
    }

    private void processStatus(Subscription s, ResponseSubscriptionStatus rs) {
        if (s.getSubscriptionRegistry() == null && !Subscription.Status.REMOVED.equals(s.getStatus())
                && !Subscription.Status.CANCELLED.equals(s.getStatus())) {
            s.setSubscriptionRegistry(new SubscriptionRegistry());
            s.getSubscriptionRegistry().setUserAccount(s.getUserAccount());
            s.getSubscriptionRegistry().setSop(s.getSop());
            s.setSubscriptionDate(new Date());
            s.getSubscriptionRegistry().setSubscriptionDate(s.getSubscriptionDate());
            s.getSubscriptionRegistry().setOriginSubscription("FACADE:STATUS");
        }
        if (rs.getCreated_date() != null) {
            try {
                s.setSubscriptionDate(locateDate("yyyy-MM-dd HH:mm", rs.getCreated_date(), TimeZone.getTimeZone("UTC")));
                s.getSubscriptionRegistry().setSubscriptionDate(s.getSubscriptionDate());
            } catch (Exception ex) {
                try {
                    s.setSubscriptionDate(locateDate("yyyy-MM-dd HH:mm:ss", rs.getCreated_date(), TimeZone.getTimeZone("UTC")));
                    s.getSubscriptionRegistry().setSubscriptionDate(s.getSubscriptionDate());
                } catch (ParseException ex1) {
                    log.error("No fue posible parsear la fecha: [" + rs.getCreated_date() + "]");
                }

            }
        }
        if (rs.getSubscription_id() != null) {
            if (s.getSubscriptionRegistry().getExternalId() != null) {
                s.getSubscriptionRegistry().setId(null);
            }
            s.getSubscriptionRegistry().setExternalId(rs.getSubscription_id());
        }

    }

    public ResponseMessage sendPin(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        ResponseSubscribePin result = wauResources.subscribeOptIn(subscription, rm);
        if (result != null) {
            rm.message = result.getResult_description();
            if ("SVC0021".equals(result.getResult_code()) && result.getRequest_id() != null && result.getRequest_id().length() > 2) {
                rm.status = ResponseMessage.Status.OK;
                try {
                    RMapCache<String, String> map = instrumentedObject.redis.getMapCache("mx.movistar.pin.map");
                    map.put(subscription.getSop().getId() + "_" + subscription.getUserAccount(), result.getRequest_id(), 1, TimeUnit.HOURS);
                } catch (Exception ex) {
                    log.error("Error al enviar a Redis el Request_id: [" + result.getRequest_id() + "]. " + ex);
                }
            } else {
                rm.status = ResponseMessage.Status.ERROR;
            }
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        throw new Exception("No implementado");
    }

    public ResponseMessage validatePin(Subscription subscription, String pin) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        String requestId = null;
        try {
            RMapCache<String, String> map = instrumentedObject.redis.getMapCache("mx.movistar.pin.map");
            requestId = map.get(subscription.getSop().getId() + "_" + subscription.getUserAccount());
        } catch (Exception ex) {
            log.error("Error al obtener de Redis el Request_id para: [" + subscription.getSop().getId() + "_" + subscription.getUserAccount() + "]. " + ex);
        }

        if (requestId == null) {
            requestId = System.currentTimeMillis() + "" + subscription.getSop().getId();
        }

        ResponseSubscribePin result = wauResources.validatePin(subscription, pin, requestId, rm);
        if (result != null) {
            rm.message = result.getResult_description();
            if ("SVC000".equals(result.getResult_code()) || "SVC008".equals(result.getResult_code())) {
                rm.status = ResponseMessage.Status.OK;
                subscription.setStatus(Subscription.Status.PENDING);
                try {
                    ResponseSubscriptionStatus rs = wauResources.subscriptionStatus(subscription, rm);
                    if (rs != null) {
                        if (rs.getSubscription_status() != null && rs.getSubscription_id() != null
                                && ("1".equals(rs.getSubscription_status()) || "4".equals(rs.getSubscription_status()))) {
                            subscription.getSubscriptionRegistry().setExternalId(rs.getSubscription_id());
                            if ("1".equals(rs.getSubscription_status())) {
                                subscription.setStatus(Subscription.Status.ACTIVE);
                            }
                        }
                        if (rs.getCreated_date() != null) {
                            try {
                                subscription.setSubscriptionDate(locateDate("yyyy-MM-dd HH:mm", rs.getCreated_date(), TimeZone.getTimeZone("UTC")));
                                subscription.getSubscriptionRegistry().setSubscriptionDate(subscription.getSubscriptionDate());
                            } catch (Exception ex) {
                                try {
                                    subscription.setSubscriptionDate(locateDate("yyyy-MM-dd HH:mm:ss", rs.getCreated_date(), TimeZone.getTimeZone("UTC")));
                                    subscription.getSubscriptionRegistry().setSubscriptionDate(subscription.getSubscriptionDate());
                                } catch (ParseException ex1) {
                                    log.error("No fue posible parsear la fecha: [" + rs.getCreated_date() + "]");
                                }
                            }
                        }
                    }
                } catch (Exception ex) {
                }

            } else if ("SVC013".equals(result.getResult_code())) {
                rm.status = ResponseMessage.Status.BLACKLIST;
                subscription.setStatus(Subscription.Status.BLACKLIST);
                subscription.setUnsubscriptionDate(new Date());
                subscription.getSubscriptionRegistry().setUnsubscriptionDate(subscription.getUnsubscriptionDate());
            } else {
                rm.status = ResponseMessage.Status.PIN_ERROR;
            }
        }

        rm.data = mapper.writeValueAsString(subscription);

        return rm;
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    private void syncWauSubscriptions(Provider p) {
        log.info("Sincronizacion de suscripciones con WAU para provider: [" + p.getName() + "]");

        ChannelQuantityProvider cqp = instance.getConfigurations().get(p);
        if (!cqp.isSyncConsumer()) {
            log.info("Provider tiene Sync deshabilitado");
            return;
        }

        List<SOP> sops = sopService.getMatchByProviderName(p.getName());

        if (Collections.isEmpty(sops)) {
            log.info("No hay SOPs configurados para provider: [" + p.getName() + "]");
            return;
        }
        for (SOP sop : sops) {
            try {
                List<Subscriber> subscribers = wauResources.getmany(sop);
                if (subscribers != null) {
                    log.info("Suscripciones encontradas para el SOP: [" + sop.getId() + "] --> [" + subscribers.size() + "]");
                    if (subscribers.isEmpty()) {
                        return;
                    }
                    for (Subscriber sr : subscribers) {
                        try {
                            sr.setIdentifier(userAccountNormalizer(sr.getIdentifier(), p));
                            Subscription s = subscriptionManager.getSubscriptionByUserAccount(sr.getIdentifier(), sop);
                            if (s == null || (!Subscription.Status.ACTIVE.equals(s.getStatus()) && !Subscription.Status.PENDING.equals(s.getStatus()))) {
                                subscriptionManager.getSubscriptionStatusFacadeRequest(sr.getIdentifier(), sop);
                            } else if (sr.getSubscription_id() != null && !sr.getSubscription_id().equals((s.getSubscriptionRegistry().getExternalId()))) {
                                if (s.getSubscriptionRegistry().getExternalId() != null) {
                                    s.getSubscriptionRegistry().setId(null);
                                }
                                s.getSubscriptionRegistry().setExternalId(sr.getSubscription_id());
                                if (sr.getCreated_date() != null) {
                                    try {
                                        s.setSubscriptionDate(locateDate("yyyy-MM-dd HH:mm", sr.getCreated_date(), TimeZone.getTimeZone("UTC")));
                                        s.getSubscriptionRegistry().setSubscriptionDate(s.getSubscriptionDate());
                                    } catch (Exception ex) {
                                        try {
                                            s.setSubscriptionDate(locateDate("yyyy-MM-dd HH:mm:ss", sr.getCreated_date(), TimeZone.getTimeZone("UTC")));
                                            s.getSubscriptionRegistry().setSubscriptionDate(s.getSubscriptionDate());
                                        } catch (ParseException ex1) {
                                            log.error("No fue posible parsear la fecha: [" + sr.getCreated_date() + "]");
                                        }
                                    }
                                }
                                subscriptionManager.subscriptionProcess(s);
                            }
                        } catch (Exception ex) {
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("Error al sincronizar con SMSC el SOP: [" + sop + "]. " + ex, ex);
            }
        }
    }

}
