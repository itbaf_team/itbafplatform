package com.itbaf.platform.paymenthub.services.repository;

public interface SmsSopMultipleService {

    public boolean isMultipleMessage(Long sop_id);

}

