package com.itbaf.platform.paymenthub.extention.spiralis.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.commons.bn.model.SpiralisNotification;
import com.itbaf.platform.paymenthub.extention.spiralis.handler.ServiceProcessorHandler;
import java.util.Arrays;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.lang3.Validate;

@Path("/notification")
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationRestService {

    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public NotificationRestService(ServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
    }
    
    @GET
    @Path("/{country}/spiralis/subscription")
    @Produces(MediaType.TEXT_PLAIN)
    public Response spiralisSubscriptionNotificationGet(final @PathParam("country") String countryCode,
            final @Context UriInfo uriInfo) {

        log.info("Notificacion de Spiralis. CountryCode [" + countryCode + "] - Notification: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]");

        SpiralisNotification notification = new SpiralisNotification(uriInfo.getQueryParameters().get("id").get(0),
                uriInfo.getQueryParameters().getFirst("msisdn"),
                uriInfo.getQueryParameters().getFirst("service"),
                uriInfo.getQueryParameters().getFirst("created"),
                uriInfo.getQueryParameters().getFirst("type"),
                uriInfo.getQueryParameters().getFirst("status"),
                uriInfo.getQueryParameters().getFirst("channelIn"),
                uriInfo.getQueryParameters().getFirst("channelOut"),
                uriInfo.getQueryParameters().getFirst("keywordIn"),
                uriInfo.getQueryParameters().getFirst("keywordOut"),
                uriInfo.getQueryParameters().getFirst("category"),
                uriInfo.getQueryParameters().getFirst("fee"),
                uriInfo.getQueryParameters().getFirst("adProvider"));

        serviceProcessorHandler.processNotification(countryCode, notification);
        return Response.status(Status.OK).entity("OK").type(MediaType.TEXT_PLAIN).build();
    }

}
