/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.bo.server;

import com.google.inject.Injector;
import com.itbaf.platform.bo.server.modules.GuiceConfigModule;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.rest.ManagerRestService;
import com.itbaf.platform.commons.servlets.ServletServer;

/**
 * 
 * @author javier
 */
public class BoServerInstance extends CommonDaemon {

    private BoServerMain daemonMain;
    private ServletServer servletServer;
    private static BoServerInstance serverDaemon;

    public static void main(String args[]) throws Exception {
        BoServerInstance.getInstance().init(args);
        BoServerInstance.getInstance().start();
    } 

    public synchronized static BoServerInstance getInstance() {
        if (serverDaemon == null) {
            serverDaemon = new BoServerInstance();
            ManagerRestService.paymentHubDaemon = serverDaemon;
        }
        return serverDaemon;
    }

    @Override
    public void start() throws Exception {
        servletServer = new ServletServer(new GuiceConfigModule(args, this));
        final Injector injector = servletServer.getInject();
        daemonMain = injector.getInstance(BoServerMain.class);
        daemonMain.start();
        servletServer.startServer();
    }

    @Override
    public void stop() throws Exception {
        try {
            servletServer.stopServer();
        } catch (Exception ex) {
        }
        try {
            Thread.sleep(100);
        } catch (Exception ex) {
        }
        try {
            daemonMain.stop();
        } catch (Exception ex) {
        }
    }

}
