package com.itbaf.platform.pgp.oauth.api.rest.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.oauth.api.handler.AutenticationServiceProcessorHandler;
import com.itbaf.platform.pgp.oauth.api.handler.ViewAutenticationServiceProcessorHandler;
import com.itbaf.platform.pgp.oauth.api.message.RequestBasicMessage;
import com.itbaf.platform.pgp.oauth.api.message.ResponseBasicMessage;
import io.jsonwebtoken.impl.DefaultClaims;
import org.apache.commons.lang3.Validate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.ws.Holder;
import java.net.URI;
import java.util.Arrays;
import java.util.Map;

@Path("/signin")
@Singleton
@lombok.extern.log4j.Log4j2
public class SignInViewAuthenticationRestService {

    private final String profile;
    private final AutenticationServiceProcessorHandler autenticationServiceProcessorHandler;
    private final ViewAutenticationServiceProcessorHandler viewAutenticationServiceProcessorHandler;

    @Inject
    public SignInViewAuthenticationRestService(@Named("guice.profile") String profile,
            ViewAutenticationServiceProcessorHandler viewAutenticationServiceProcessorHandler,
            final AutenticationServiceProcessorHandler autenticationServiceProcessorHandler) {
        this.profile = profile;
        this.autenticationServiceProcessorHandler = Validate.notNull(autenticationServiceProcessorHandler, "An AutenticationServiceProcessorHandler class must be provided");
        this.viewAutenticationServiceProcessorHandler = Validate.notNull(viewAutenticationServiceProcessorHandler, "A ViewAutenticationServiceProcessorHandler class must be provided");
    }

    /* @GET
    @Path("/login")
    @Produces(MediaType.TEXT_HTML)
    public Response login() throws URISyntaxException {
        java.net.URI location = new java.net.URI("https://planeta.guru");
        return Response.status(302).location(location).build();
    }*/

    @GET
    @Path("/login/{tid}")
    @Produces(MediaType.TEXT_HTML)
    public Response login(@Context HttpServletRequest request,
            @PathParam("tid") String tid,
            @QueryParam("token") String udiToken,
            @QueryParam("aniFound") String aniFound,
            @QueryParam("locale") String locale) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-signin.login");
        } catch (Exception ex) {
        }
        log.info("Login. TID: [" + tid + "] - locale: [" + locale + "] - udiToken: [" + udiToken + "] - aniFound: [" + aniFound + "]");
        if (locale == null) {
            locale = "es";
        }
        tid = autenticationServiceProcessorHandler.getTID(request, tid);
        Map<String, String> prop = null;
        if (tid != null) {
            try {
                prop = autenticationServiceProcessorHandler.validateTID(tid);
            } catch (Exception ex) {
                log.warn("TID Invalido: [" + tid + "]" + ex, ex);
            }
        }
        log.info("SignInViewAuthenticationRestService - login prop:[" + prop + "]");
        if (prop != null) {
            try {
                if ("true".equals(prop.get("redirectPG"))) {
                    return Response.status(302).location(new URI(prop.get("redirectUri"))).build();
                }
            } catch (Exception ex) {
            }


            User udiUser = null;
            Holder<Provider> prov = new Holder();
            try {
                String urlRedirect = null;
                if ("true".equals(aniFound)) {
                    Holder<String> ur = new Holder();
                    udiUser = autenticationServiceProcessorHandler.getUserFromUdiMessage(udiToken, aniFound, tid, prop, prov, ur);
                    urlRedirect = ur.value;
                } else {
                    urlRedirect = autenticationServiceProcessorHandler.ClaroVrValidation(prop);
                }
                if (urlRedirect != null) {
                    return Response.status(302).location(new URI(urlRedirect)).build();
                }
            } catch (Exception ex) {
            }

            Cookie[] cooks = request.getCookies();
            if (cooks != null) {
                log.info("Cookies: [" + Arrays.toString(cooks) + "]");
                for (Cookie c : cooks) {
                    try {
                        if ("iPlanetDirectoryPro".equals(c.getName())) {
                            // login con cookie de SSO Personal Argentina
                            ResponseBasicMessage rm = autenticationServiceProcessorHandler.validateUserCookieTokenArPersonal(prop, tid, c.getValue(), request, udiUser, prov, request);
                            if (rm != null && rm.redirectUrl != null) {
                                log.info("Login por Cookie - CDAG Argentina [" + rm.redirectUrl + "]");
                                return Response.status(302).location(new URI(rm.redirectUrl)).build();
                            }
                        } else if ("Ux_PG".equals(c.getName())) {
                            // login con cookie de oauth ITBAF
                            ResponseBasicMessage rm = autenticationServiceProcessorHandler.validateUserCookieToken(prop, tid, c.getValue(), request, udiUser);
                            if (rm != null && rm.redirectUrl != null) {
                                log.info("Login por Cookie - ITBAF [" + rm.redirectUrl + "]");
                                return Response.status(302).location(new URI(rm.redirectUrl)).build();
                            }
                        }
                    } catch (Exception ex) {
                        log.error("Error al validar cookie. " + ex, ex);
                    }
                }
            }

            try {
                ResponseBasicMessage rm = autenticationServiceProcessorHandler.startUdiSession(prop, tid, udiUser, request, prov);
                if (rm.redirectUrl != null) {
                    log.info("Login por udiUser");
                    return Response.status(302).location(new URI(rm.redirectUrl)).build();
                } else if (rm.html != null) {
                    return Response.status(Status.OK).entity(rm.html).type(MediaType.TEXT_HTML).build();
                }
            } catch (Exception ex) {
                log.error("Error al procesar inicio de sesion para udiUser: [" + udiUser + "]. " + ex, ex);
            }
            // Si no realizo ningun redirect arma el html del login
            String html = viewAutenticationServiceProcessorHandler.getUserLoginHtml(prop, tid, request);
            html = viewAutenticationServiceProcessorHandler.i18n(locale, prop.get("prvdr"), html);
            return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
        }
        return Response.status(Status.UNAUTHORIZED).build();
    }

    @GET
    @Path("/resetPassword/{tid}")
    @Produces(MediaType.TEXT_HTML)
    public Response resetPassword(@Context HttpServletRequest request,
            @PathParam("tid") String tid,
            @QueryParam("un") final String username,
            @QueryParam("locale") String locale) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-signin.resetPassword");
        } catch (Exception ex) {
        }
        log.info("ResetPassword. TID: [" + tid + "] - Username: [" + username + "] - locale: [" + locale + "]");
        if (locale == null) {
            locale = "es";
        }

        tid = autenticationServiceProcessorHandler.getTID(request, tid);

        Map<String, String> prop = null;
        if (tid != null) {
            try {
                prop = autenticationServiceProcessorHandler.validateTID(tid);
            } catch (Exception ex) {
                log.warn("TID Invalido: [" + tid + "]" + ex, ex);
            }
        }
        if (prop != null) {
            String html = viewAutenticationServiceProcessorHandler.getResetPasswordHtml(prop, tid);
            html = viewAutenticationServiceProcessorHandler.i18n(locale, prop.get("prvdr"), html);
            return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
        }
        return Response.status(Status.UNAUTHORIZED).build();
    }

    @POST
    @Path("/reset/password")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response resetPasswordSend(@Context HttpServletRequest request,
            @FormParam("tid") final String tid,
            @FormParam("usernameType") final RequestBasicMessage.UsernameType usernameType,
            @FormParam("phoneCountry") final String location,
            @FormParam("iSigninName") final String username,
            @FormParam("g-recaptcha-response") final String gResponse,
            @FormParam("locale") String locale) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-signin.reset.password");
        } catch (Exception ex) {
        }
        RequestBasicMessage requestBasicMessage = new RequestBasicMessage();
        requestBasicMessage.gResponse = gResponse;
        requestBasicMessage.location = location;
        requestBasicMessage.tid = tid;
        requestBasicMessage.username = username;
        requestBasicMessage.usernameType = usernameType;

        log.info("ResetPasswordSend. RequestBasicMessage: [" + requestBasicMessage + "] - locale: [" + locale + "]");
        if (locale == null) {
            locale = "es";
        }

        requestBasicMessage.tid = autenticationServiceProcessorHandler.getTID(request, requestBasicMessage.tid);

        requestBasicMessage.locale = locale;

        Map<String, String> prop = null;
        try {
            prop = autenticationServiceProcessorHandler.validateTID(tid);
        } catch (Exception ex) {
            log.warn("TID Invalido: [" + requestBasicMessage + "]" + ex, ex);
        }
        if (prop != null) {
            try {
                if (!autenticationServiceProcessorHandler.googleRecaptchaValidation(gResponse)) {
                    String url = "/signin/resetPassword/" + tid + "?locale=" + locale;
                    return Response.status(302).location(new URI(url)).build();
                }
                ResponseBasicMessage rm = autenticationServiceProcessorHandler.getUserInformation(requestBasicMessage, prop, true, request);
                rm.tid = tid;
                rm.usernameType = usernameType;
                String html = viewAutenticationServiceProcessorHandler.getResetPasswordHtml(prop, tid);
                html = html.replace("var srt", "pparams=" + viewAutenticationServiceProcessorHandler.toJson(rm));
                html = viewAutenticationServiceProcessorHandler.i18n(locale, prop.get("prvdr"), html);
                return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
            } catch (Exception ex) {
                log.error("Error al procesar informacion de usuario. " + ex, ex);
            }
        }
        return Response.status(Status.UNAUTHORIZED).build();
    }

    @GET
    @Path("/secure/verify/{path}")
    @Produces(MediaType.TEXT_HTML)
    public Response secureVerifyGet(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            @QueryParam("locale") String locale) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-signin.secure.verify." + path);
        } catch (Exception ex) {
        }
        RequestBasicMessage requestBasicMessage = new RequestBasicMessage();
        requestBasicMessage.path = path;
        if (locale == null) {
            locale = "es";
        }
        requestBasicMessage.locale = locale;

        log.info("secureVerifyGet. RequestBasicMessage: [" + requestBasicMessage + "]");

        return secureVerify(request, requestBasicMessage);
    }

    @POST
    @Path("/secure/verify/{path}")
    @Produces(MediaType.TEXT_HTML)
    public Response secureVerifyPost(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            @FormParam("code") final String code,
            @FormParam("locale") String locale) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-signin.secure.verify." + path);
        } catch (Exception ex) {
        }
        RequestBasicMessage requestBasicMessage = new RequestBasicMessage();
        requestBasicMessage.path = path;
        requestBasicMessage.code = code;
        if (locale == null) {
            locale = "es";
        }
        requestBasicMessage.locale = locale;
        log.info("secureVerifyPost. RequestBasicMessage: [" + requestBasicMessage + "]");
        return secureVerify(request, requestBasicMessage);
    }

    private Response secureVerify(HttpServletRequest request, RequestBasicMessage requestBasicMessage) {

        DefaultClaims prop = null;
        try {
            requestBasicMessage.path = autenticationServiceProcessorHandler.getTID(request, requestBasicMessage.path);
            prop = autenticationServiceProcessorHandler.validateToken(requestBasicMessage.path);
        } catch (Exception ex) {
            log.warn("Token invalido: [" + requestBasicMessage.path + "]. " + ex, ex);
        }
        if (prop != null) {
            try {
                ResponseBasicMessage rm = new ResponseBasicMessage();
                rm.locale = requestBasicMessage.locale;
                rm.path = requestBasicMessage.path;
                String html = viewAutenticationServiceProcessorHandler.verifyEmail(prop, requestBasicMessage, rm, request);

                if (rm.redirectUrl != null) {
                    return Response.status(302).location(new URI(rm.redirectUrl)).build();
                }
                if (html != null) {
                    html = html.replace("var srt", "pparams=" + viewAutenticationServiceProcessorHandler.toJson(rm));
                    String prvdr = null;
                    try {
                        prvdr = prop.get("prvdr", String.class);
                    } catch (Exception ex) {
                    }
                    html = viewAutenticationServiceProcessorHandler.i18n(rm.locale, prvdr, html);
                    return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
                }
            } catch (Exception ex) {
                log.error("Error al crear email de verificacion. " + ex, ex);
            }
        }

        return Response.status(Status.FORBIDDEN).build();
    }

    @GET
    @Path("/secure/{path}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response secureValidationGet(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            @QueryParam("tid") final String tid,
            @QueryParam("code") final String code,
            @QueryParam("locale") String locale) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-signin.secure." + path);
        } catch (Exception ex) {
        }
        RequestBasicMessage requestBasicMessage = new RequestBasicMessage();
        requestBasicMessage.tid = tid;
        requestBasicMessage.code = code;
        requestBasicMessage.path = path;
        if (locale == null) {
            locale = "es";
        }
        requestBasicMessage.locale = locale;
        requestBasicMessage.tid = autenticationServiceProcessorHandler.getTID(request, requestBasicMessage.tid);
        return secureValidation(request, requestBasicMessage);
    }

    @POST
    @Path("/secure/{path}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response secureValidationPost(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            @FormParam("tid") final String tid,
            @FormParam("usernameType") final RequestBasicMessage.UsernameType usernameType,
            @FormParam("username") final String username,
            @FormParam("passwd") final String passwd,
            @FormParam("rpasswd") final String rpasswd,
            @FormParam("KMSI") final String kmsi,
            @FormParam("code") final String code,
            @FormParam("vcode") final String vcode,
            @FormParam("tokenCode") final String tokenCode,
            @FormParam("ptokenCode") final String ptokenCode,
            @FormParam("provider") final String provider,
            @FormParam("locale") String locale) {
        RequestBasicMessage requestBasicMessage = new RequestBasicMessage();
        requestBasicMessage.tid = tid;
        requestBasicMessage.username = username;
        requestBasicMessage.usernameType = usernameType;
        requestBasicMessage.password = passwd;
        requestBasicMessage.rpassword = rpasswd;
        requestBasicMessage.code = code;
        requestBasicMessage.vcode = vcode;
        requestBasicMessage.tokenCode = tokenCode;
        requestBasicMessage.ptokenCode = ptokenCode;
        requestBasicMessage.path = path;
        requestBasicMessage.kmsi = kmsi;
        requestBasicMessage.provider = provider;
        if (locale == null) {
            locale = "es";
        }
        requestBasicMessage.locale = locale;
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-signin.secure." + path);
        } catch (Exception ex) {
        }
        requestBasicMessage.tid = autenticationServiceProcessorHandler.getTID(request, requestBasicMessage.tid);
        return secureValidation(request, requestBasicMessage);
    }

    private Response secureValidation(HttpServletRequest request,
            RequestBasicMessage requestBasicMessage) {

        log.info("SecureValidation. RequestBasicMessage: [" + requestBasicMessage + "]");
        if (requestBasicMessage.tid == null) {
            String referer = request.getHeader("referer");
            log.error("TID NULL. Se hara un redirect a: [" + referer + "]");
            try {
                return Response.status(302).location(new URI(referer)).build();
            } catch (Exception ex) {
                log.fatal("TID NULL. No se hizo el redirect a: [" + referer + "]");
            }
        }

        Map<String, String> prop = null;
        try {
            prop = autenticationServiceProcessorHandler.validateTID(requestBasicMessage.tid);
        } catch (Exception ex) {
            log.warn("TID Invalido: [" + requestBasicMessage + "]" + ex, ex);
        }
        if (prop != null) {
            try {
                String html = "";
                ResponseBasicMessage rm;
                rm = viewAutenticationServiceProcessorHandler.validatePasswordOrCode(prop, requestBasicMessage, request);
                if (rm != null && rm.redirectUrl != null) {
                    if (requestBasicMessage.kmsi != null && rm.uct != null) {
                        NewCookie cookie = new NewCookie("Ux_PG", rm.uct, "/",
                                null, "UID_PG_TK", 365 * 24 * 60 * 60, !"dev".equals(profile));
                        return Response.status(302).location(new URI(rm.redirectUrl)).cookie(cookie).build();
                    }
                    return Response.status(302).location(new URI(rm.redirectUrl)).build();
                }
                rm.tid = requestBasicMessage.tid;
                rm.usernameType = requestBasicMessage.usernameType;

                switch (requestBasicMessage.path) {
                    case "login":
                    case "provider":
                        html = viewAutenticationServiceProcessorHandler.getUserLoginHtml(prop, requestBasicMessage.tid, request);
                        break;
                    case "code":
                    case "reset":
                        html = viewAutenticationServiceProcessorHandler.getResetPasswordHtml(prop, requestBasicMessage.tid);
                        break;
                    case "spin":
                        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                rm.kmsi = requestBasicMessage.kmsi;
                html = html.replace("var srt", "pparams=" + viewAutenticationServiceProcessorHandler.toJson(rm));
                html = viewAutenticationServiceProcessorHandler.i18n(requestBasicMessage.locale, prop.get("prvdr"), html);
                return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
            } catch (Exception ex) {
                log.error("Error al procesar informacion de usuario. " + ex, ex);
            }
        }

        return Response.status(Status.BAD_REQUEST).build();
    }

    @GET
    @Path("/resource/{page}")
    @Produces(MediaType.TEXT_HTML)
    public Response resource(@PathParam("page") final String page,
            @QueryParam("mkt") String i18n) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-signin.resource." + page);
        } catch (Exception ex) {
        }
        log.info("Resource. page: [" + page + "] - i18n: [" + i18n + "]");
        if (i18n == null) {
            i18n = "es";
        } else {
            i18n = i18n.toLowerCase();
        }

        String html = viewAutenticationServiceProcessorHandler.getHtmlResourse(page);

        if (html != null) {
            html = viewAutenticationServiceProcessorHandler.i18n(i18n, null, html);
            return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
        }

        return Response.status(Status.UNAUTHORIZED).build();
    }

}
