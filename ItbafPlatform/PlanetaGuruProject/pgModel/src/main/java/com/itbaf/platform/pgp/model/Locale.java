/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model;

import com.google.common.base.MoreObjects;
import com.querydsl.core.annotations.Config;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author javier
 */
@Entity(name = "LOCALE")
@Table(name = "LOCALE")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class Locale implements Serializable {

    private static final long serialVersionUID = 20180730095701L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "LANGUAGE_CODE", nullable = false)
    private String languageCode;
    @Column(name = "COUNTRY_CODE", nullable = false)
    private String countryCode;
    @Column(name = "PROVIDER_ID", nullable = false)
    private Long providerId;
    @Column(name = "KEY_P", nullable = false)
    private String key;
    @Column(name = "VALUE_P", nullable = false)
    private String value;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("languageCode", languageCode)
                .add("countryCode", countryCode)
                .add("providerId", providerId)
                .add("key", key)
                .add("value", value)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.languageCode);
        hash = 97 * hash + Objects.hashCode(this.countryCode);
        hash = 97 * hash + Objects.hashCode(this.providerId);
        hash = 97 * hash + Objects.hashCode(this.key);
        hash = 97 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Locale other = (Locale) obj;
        if (!Objects.equals(this.languageCode, other.languageCode)) {
            return false;
        }
        if (!Objects.equals(this.countryCode, other.countryCode)) {
            return false;
        }
        if (!Objects.equals(this.key, other.key)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.providerId, other.providerId)) {
            return false;
        }
        return true;
    }

    
}
