package com.itbaf.platform.pgp.test.services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.pgp.commons.api.message.udi.UdiMessage;
import com.itbaf.platform.pgp.test.instance.GuiceInstance;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author JF
 */
public class JsonTest {

    private ObjectMapper mapper;

    public JsonTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        mapper = GuiceInstance.getInstance().getInstance(ObjectMapper.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    @Ignore
    public void jsonTest() throws Exception {
        String json = "{\"ip\":\"::ffff:190.104.131.93\",\"geo\":{\"city\":{\"geoname_id\":3436616,\"names\":{\"en\":\"Ygatimi\",\"es\":\"Ygatimi\"}},\"continent\":{\"code\":\"SA\",\"geoname_id\":6255150,\"names\":{\"de\":\"S\\u00fcdamerika\",\"en\":\"South America\",\"es\":\"Sudam\\u00e9rica\",\"fr\":\"Am\\u00e9rique du Sud\",\"ja\":\"\\u5357\\u30a2\\u30e1\\u30ea\\u30ab\",\"pt-BR\":\"Am\\u00e9rica do Sul\",\"ru\":\"\\u042e\\u0436\\u043d\\u0430\\u044f \\u0410\\u043c\\u0435\\u0440\\u0438\\u043a\\u0430\",\"zh-CN\":\"\\u5357\\u7f8e\\u6d32\"}},\"country\":{\"geoname_id\":3437598,\"iso_code\":\"PY\",\"names\":{\"de\":\"Paraguay\",\"en\":\"Paraguay\",\"es\":\"Paraguay\",\"fr\":\"Paraguay\",\"ja\":\"\\u30d1\\u30e9\\u30b0\\u30a2\\u30a4\\u5171\\u548c\\u56fd\",\"pt-BR\":\"Paraguai\",\"ru\":\"\\u041f\\u0430\\u0440\\u0430\\u0433\\u0432\\u0430\\u0439\",\"zh-CN\":\"\\u5df4\\u62c9\\u572d\"}},\"location\":{\"accuracy_radius\":20,\"latitude\":-24.0833,\"longitude\":-55.5,\"time_zone\":\"America\\/Asuncion\"},\"registered_country\":{\"geoname_id\":3437598,\"iso_code\":\"PY\",\"names\":{\"de\":\"Paraguay\",\"en\":\"Paraguay\",\"es\":\"Paraguay\",\"fr\":\"Paraguay\",\"ja\":\"\\u30d1\\u30e9\\u30b0\\u30a2\\u30a4\\u5171\\u548c\\u56fd\",\"pt-BR\":\"Paraguai\",\"ru\":\"\\u041f\\u0430\\u0440\\u0430\\u0433\\u0432\\u0430\\u0439\",\"zh-CN\":\"\\u5df4\\u62c9\\u572d\"}},\"subdivisions\":[{\"geoname_id\":3439216,\"iso_code\":\"14\",\"names\":{\"en\":\"Departamento de Canindeyu\"}}]},\"asn\":{\"autonomous_system_number\":27895,\"autonomous_system_organization\":\"N\\u00facleo S.A.\"},\"phone\":{\"carrier\":\"py.personal.cdag\",\"ani\":\"595971501134\"},\"language\":{\"country\":[{\"name\":\"Spanish\",\"native\":\"Espa\\u00f1ol\",\"code2\":\"es\",\"code3\":\"spa\"},{\"name\":\"Guaran\\u00ed\",\"native\":\"Ava\\u00f1e'\\u1ebd\",\"code2\":\"gn\",\"code3\":\"grn\"}],\"accepts\":[\"es-ES\",\"es\"]}}";
        UdiMessage jo = mapper.readValue(json, UdiMessage.class);
        System.out.println(jo);
    }
    
    @Test
    @Ignore
    public void jsonTest2() throws Exception {
        String json = "{\"userAccount\":\"CDAG.7dI8caiIbsfxaqUuAvGFic\",\"status\":\"PENDING\",\"sop\":{\"id\":54,\"status\":\"ENABLE\",\"serviceMatcher\":{\"id\":201,\"name\":\"JuegosDiarios\",\"genericName\":\"Planeta Guru Juegos\",\"service\":{\"id\":23,\"code\":\"PGJ\",\"name\":\"Planeta Guru Juegos\",\"description\":\"Servicio que agrupa servicios de Planeta Guru Juegos\"}},\"operator\":{\"id\":1,\"name\":\"ITBAF\",\"description\":\"ITBAF Industries\"},\"provider\":{\"id\":19,\"name\":\"py.personal.cdag\",\"genericName\":\"Personal\",\"description\":\"Servicios de suscripcion con la telco Personal Paraguay mediante CDAG\",\"country\":{\"id\":109,\"name\":\"Paraguay\",\"nativeName\":\"Paraguay\",\"code\":\"PY\",\"currency\":\"PYG\",\"symbolCurrency\":\"₲\",\"phoneCode\":595,\"status\":\"ENABLE\",\"nativeView\":false,\"languages\":[{\"code\":\"gn\",\"code3\":\"grn\",\"name\":\"Guaraní\",\"nativeName\":\"Avañe'ẽ\"},\"Spanish\"]}},\"tariffs\":[{\"id\":92,\"tariffType\":\"MAIN\",\"status\":\"ENABLE\",\"externalId\":\"py.personal.cdag.3398\",\"frecuency\":1,\"frecuencyType\":\"days\",\"currency\":\"PYG\",\"fullAmount\":1500.0,\"netAmount\":1350.0,\"percentage\":100.0,\"localCurrency\":\"GUR\",\"localAmount\":300.0,\"fromDate\":\"2018-11-26T20:19:52.000Z\",\"toDate\":\"2019-11-26T20:19:52.000Z\"}],\"integrationSettings\":{\"subscription.service.user\":\"PlnGuru\",\"cdag_renewal_serviceId\":\"3399\",\"shortcode\":\"7755\",\"cdag_serviceId\":\"3398\",\"keywordBaja\":\"BAJA\",\"fullAmount\":\"1500\",\"keywordAlta\":\"ALTA\",\"subscription.service.password\":\"G797perpy\",\"providerId\":\"47\",\"frequencyType\":\"days\",\"currency\":\"PYG\",\"subscription.service.externalUser\":\"biller\",\"request.useraccount.normalizer\":\"true\"}},\"subscriptionRegistry\":{\"userAccount\":\"CDAG.7dI8caiIbsfxaqUuAvGFic\",\"sop\":{\"id\":54,\"status\":\"ENABLE\",\"serviceMatcher\":{\"id\":201,\"name\":\"JuegosDiarios\",\"genericName\":\"Planeta Guru Juegos\",\"service\":{\"id\":23,\"code\":\"PGJ\",\"name\":\"Planeta Guru Juegos\",\"description\":\"Servicio que agrupa servicios de Planeta Guru Juegos\"}},\"operator\":{\"id\":1,\"name\":\"ITBAF\",\"description\":\"ITBAF Industries\"},\"provider\":{\"id\":19,\"name\":\"py.personal.cdag\",\"genericName\":\"Personal\",\"description\":\"Servicios de suscripcion con la telco Personal Paraguay mediante CDAG\",\"country\":{\"id\":109,\"name\":\"Paraguay\",\"nativeName\":\"Paraguay\",\"code\":\"PY\",\"currency\":\"PYG\",\"symbolCurrency\":\"₲\",\"phoneCode\":595,\"status\":\"ENABLE\",\"nativeView\":false,\"languages\":[{\"code\":\"gn\",\"code3\":\"grn\",\"name\":\"Guaraní\",\"nativeName\":\"Avañe'ẽ\"},\"Spanish\"]}},\"tariffs\":[{\"id\":92,\"tariffType\":\"MAIN\",\"status\":\"ENABLE\",\"externalId\":\"py.personal.cdag.3398\",\"frecuency\":1,\"frecuencyType\":\"days\",\"currency\":\"PYG\",\"fullAmount\":1500.0,\"netAmount\":1350.0,\"percentage\":100.0,\"localCurrency\":\"GUR\",\"localAmount\":300.0,\"fromDate\":\"2018-11-26T20:19:52.000Z\",\"toDate\":\"2019-11-26T20:19:52.000Z\"}],\"integrationSettings\":{\"subscription.service.user\":\"PlnGuru\",\"cdag_renewal_serviceId\":\"3399\",\"shortcode\":\"7755\",\"cdag_serviceId\":\"3398\",\"keywordBaja\":\"BAJA\",\"fullAmount\":\"1500\",\"keywordAlta\":\"ALTA\",\"subscription.service.password\":\"G797perpy\",\"providerId\":\"47\",\"frequencyType\":\"days\",\"currency\":\"PYG\",\"subscription.service.externalUser\":\"biller\",\"request.useraccount.normalizer\":\"true\"}},\"channelIn\":\"WEB\",\"subscriptionDate\":\"2019-04-17T17:42:07.862Z\",\"originSubscription\":\"FACADE:PUT_SUBSCRIPTION\",\"adnetworkTracking\":{\"source\":\"adw\",\"medium\":\"pgj_py_pers\",\"campaign\":\"juegos\",\"term\":\"\",\"content\":\"\"}},\"subscriptionDate\":\"2019-04-17T17:42:07.862Z\",\"thread\":\"20190417.174207.803.2583.facade.s\",\"facadeRequest\":{\"userAccount\":\"CDAG.7dI8caiIbsfxaqUuAvGFic\",\"service\":\"JuegosDiarios\",\"operator\":\"ITBAF\",\"provider\":\"py.personal.cdag\",\"serviceCode\":\"PGJ\",\"adTracking\":{\"source\":\"adw\",\"medium\":\"pgj_py_pers\",\"campaign\":\"juegos\",\"term\":\"\",\"content\":\"\"},\"creationDate\":\"2019-04-17T17:42:07.804Z\",\"channel\":\"WEB\",\"action\":\"RequestModule\"},\"charged\":false}";
        Subscription s = mapper.readValue(json, Subscription.class);
        System.out.println(s);
    }

}
