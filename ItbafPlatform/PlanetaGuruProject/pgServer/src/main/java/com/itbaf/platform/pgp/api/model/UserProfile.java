/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.pgp.model.oauth.properties.ProfileUserProperty;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.AgeRange;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Friend;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.wallet.PaymentMethod;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class UserProfile {

    private Long userId;
    private List<Long> userIds;
    private String nickname;
    private String firstName;
    private String middleName;
    private String lastName;
    private String photoURL;
    private String avatarURL;
    private Boolean userPhoto;
    private boolean isNicknameEdited;
    private Birthdate birthdate;
    private AgeRange ageRange;
    private ProfileUserProperty.Gender gender;
    private Country country;
    private Country localCountry;
    private String state;
    private String address;
    private String zip;
    private String language;
    private String occupation;
    private Set<Email> emails;
    private Set<Msisdn> phones;
    private Set<Friend> friends;
    private List<WalletTransactionView> transactions;
    private String siteURL;
    private Object clientData;
    private Map<String, Map<String, BigDecimal>> creditsByProduct;
    private Map<String, BigDecimal> credits;
    private Object result;
    private Object subscriptions;
    private Object unsubscriptions;
    private List<PaymentMethod> paymentMethodsSubscription;
    private List<PaymentMethod> paymentMethodsOndemand;
    private String[] codes;
    private Integer autoCancel = 0;
    private String srIdTransaction;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userId", userId)
                .add("userIds", userIds)
                .add("nickname", nickname)
                .add("firstName", firstName)
                .add("middleName", middleName)
                .add("lastName", lastName)
                .add("photoURL", photoURL)
                .add("avatarURL", avatarURL)
                .add("userPhoto", userPhoto)
                .add("isNicknameEdited", isNicknameEdited)
                .add("birthdate", birthdate)
                .add("ageRange", ageRange)
                .add("gender", gender)
                .add("country", country)
                .add("localCountry", localCountry)
                .add("state", state)
                .add("address", address)
                .add("zip", zip)
                .add("language", language)
                .add("occupation", occupation)
                .add("emails", emails)
                .add("phones", phones)
                .add("friends", friends)
                .add("transactions", transactions)
                .add("siteURL", siteURL)
                .add("clientData", clientData)
                .add("creditsByProduct", creditsByProduct)
                .add("credits", credits)
                .add("result", result)
                .add("subscriptions", subscriptions)
                .add("unsubscriptions", unsubscriptions)
                .add("paymentMethodsSubscription", paymentMethodsSubscription)
                .add("paymentMethodsOndemand", paymentMethodsOndemand)
                .add("codes", codes)
                .add("autoCancel", autoCancel)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.userId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserProfile other = (UserProfile) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        return true;
    }

}
