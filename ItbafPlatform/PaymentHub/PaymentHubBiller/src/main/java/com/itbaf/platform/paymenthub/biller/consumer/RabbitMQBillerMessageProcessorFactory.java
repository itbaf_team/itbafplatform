/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.biller.consumer;

import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessorFactory;
import com.itbaf.platform.paymenthub.biller.processor.BillerMessageProcessor;

/**
 *
 * @author javier
 */
public interface RabbitMQBillerMessageProcessorFactory extends RabbitMQMessageProcessorFactory <BillerMessageProcessor>{

}
