/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.servlets;

import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.name.Names;
import com.google.inject.servlet.GuiceFilter;
import org.apache.logging.log4j.LogManager;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.server.handler.gzip.GzipHandler;

/**
 *
 * @author jordonez
 */
public class ServletServer {

    private final int port;
    private final Server server;
    private final ServletContextHandler sch;
    private final ServletListener servletListener;
    private final org.apache.logging.log4j.Logger log;

    public ServletServer(Module module) {
        this.servletListener = new ServletListener(module);
        this.log = LogManager.getLogger(ServletServer.class);
        this.port = servletListener.getInjector().getInstance(Key.get(Integer.class, Names.named("server.port")));
        this.server = servletListener.getInjector().getInstance(Server.class);
        this.sch = servletListener.getInjector().getInstance(ServletContextHandler.class);
    }

    public void startGzipServer(String... includedPaths) {
        try {
            GzipHandler gh = new GzipHandler();
            gh.addIncludedPaths(includedPaths);
            sch.setGzipHandler(gh);
            startServer();
        } catch (Exception ex) {
            log.error("Error al iniciar el server. [http://localhost:" + port + "]. " + ex, ex);
        }
    }

    public void startServer() {
        try {
            sch.addEventListener(servletListener);
            sch.addFilter(GuiceFilter.class, "/*", null);
            sch.addServlet(DefaultServlet.class, "/");
            //server.setSendServerVersion(false);
            server.start();
            //  log.info("\n ----- ----- -----\n\n\n\n");
            server.join();
        } catch (Exception ex) {
            log.error("Error al iniciar el server. [http://localhost:" + port + "]. " + ex, ex);
        }
    }

    public Injector getInject() {
        return servletListener.getInjector();
    }

    public void stopServer() {
        try {
            log.info("Deteniendo .... Servidor Port: [" + port + "]");
            server.stop();
            log.info("Servidor detenido");
        } catch (Exception ex) {
            log.error("Error al detener el server. " + ex, ex);
        }
    }

}
