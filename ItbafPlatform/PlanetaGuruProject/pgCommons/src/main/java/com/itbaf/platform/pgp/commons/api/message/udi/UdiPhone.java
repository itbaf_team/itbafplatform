/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.commons.api.message.udi;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class UdiPhone {

    public String carrier;
    public String ani;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("carrier", carrier)
                .add("ani", ani)
                .omitNullValues().toString();
    }
}
