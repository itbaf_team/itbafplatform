/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.modules;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/**
 *
 * @author javier
 */
public class GuiceThreadsConfigModule extends AbstractModule {

    @Override
    public void configure() {
        bind(ThreadFactory.class).toInstance(new ThreadFactoryBuilder().build());
      //  bind(EventBus.class).to(AsyncEventBus.class);
      //  bind(Executor.class).to(ExecutorService.class);
    }

   /* @Provides
    @Singleton
    AsyncEventBus providesAsyncEventBus(Executor executor) {
        return new AsyncEventBus(executor);
    }*/

    @Provides
    @Singleton
    ExecutorService cachedThreadPool(ThreadFactory factory) {
        return Executors.newCachedThreadPool(factory);
    }

    @Provides
    @Singleton
    ScheduledExecutorService scheduledThreadPool(ThreadFactory factory, @Named("threads.pool.corePoolSize") Integer corePoolSize) {
        return Executors.newScheduledThreadPool(corePoolSize, factory);
    }
}
