/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class AutoRecurring {

    private Integer frequency;
    private String frequency_type;
    private BigDecimal transaction_amount;
    private String currency_id;
    private Date start_date;
    private Date end_date;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("frequency", frequency)
                .add("frequency_type", frequency_type)
                .add("transaction_amount", transaction_amount)
                .add("currency_id", currency_id)
                .add("start_date", start_date)
                .add("end_date", end_date)
                .omitNullValues().toString();
    }

}
