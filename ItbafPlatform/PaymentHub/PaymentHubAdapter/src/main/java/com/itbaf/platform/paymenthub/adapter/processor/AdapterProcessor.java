/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adapter.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.rmq.consumer.MessageProcessor;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public abstract class AdapterProcessor implements MessageProcessor {

    public enum AdapterType {
        MO_ADAPTER,
        MT_ADAPTER
    }

    @Inject
    protected ObjectMapper mapper;
    @Inject
    protected RabbitMQProducer rabbitMQProducer;

    public final AdapterType adapterType;

    public AdapterProcessor(AdapterType adapterType) {
        this.adapterType = adapterType;
    }

    public abstract Boolean process(String msg) throws Exception;

}
