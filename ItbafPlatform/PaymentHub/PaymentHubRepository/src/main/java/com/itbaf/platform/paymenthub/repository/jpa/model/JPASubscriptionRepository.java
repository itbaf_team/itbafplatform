/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.QSubscriptionBilling;
import com.itbaf.platform.paymenthub.model.subscription.QSubscription;
import com.itbaf.platform.paymenthub.model.subscription.QSubscriptionRegistry;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.querydsl.jpa.impl.JPAQuery;
import io.jsonwebtoken.lang.Collections;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Calendar;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPASubscriptionRepository extends AbstractJPARepositorySession<Subscription, Long> {

    @Inject
    public JPASubscriptionRepository(final javax.inject.Provider<EntityManager> entityManagerProvider) {
        super(Subscription.class, entityManagerProvider);
    }

    /**
     * Busca la existencia de una suscripcion en DB sin importar el estado de la
     * misma con base en el codigo de Servicio
     *
     * @param userAccout
     * @param serviceCode
     * @return
     */
    public List<Subscription> findByUserAccount(String userAccout, String serviceCode) {
        if (userAccout == null || serviceCode == null) {
            String error = "Uno de los parametros es nulo!! userAccout: [" + userAccout + "] - serviceCode: [" + serviceCode + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<Subscription> s = query.select(subscription).from(subscription)
                .where(ObjectPredicates.combine(subscription.userAccount.eq(userAccout),
                        subscription.sop().serviceMatcher().service().code.equalsIgnoreCase(serviceCode)))
                .fetch();

        return s;
    }

    /**
     * Busca la existencia de una suscripcion en DB sin importar el estado de la
     * misma
     *
     * @param userAccout
     * @param sop
     * @return
     */
    public Subscription findByUserAccount(String userAccout, SOP sop) {
        if (userAccout == null || sop == null || sop.getId() == null) {
            String error = "Uno de los parametros es nulo!! userAccout: [" + userAccout + "] - sop: [" + sop + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Subscription s = query.select(subscription).from(subscription)
                .where(ObjectPredicates.combine(subscription.userAccount.eq(userAccout),
                        subscription.sop().id.eq(sop.getId())))
                .fetchFirst();
        return s;
    }

    public Subscription findByExternalId(String externalId, SOP sop) {
        if (externalId == null || sop == null || sop.getId() == null) {
            String error = "Uno de los parametros es nulo!! externalId: [" + externalId + "] - sop: [" + sop + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());
        Subscription s = query.select(subscription).from(subscription)
                .where(subscription.subscriptionRegistry().externalId.eq(externalId),
                        subscription.subscriptionRegistry().sop().eq(sop))
                .fetchFirst();

        return s;
    }

    public Subscription findByExternalUserAccount(String externalUserAccount, SOP sop) {
        if (externalUserAccount == null || sop == null || sop.getId() == null) {
            String error = "Uno de los parametros es nulo!! externalUserAccount: [" + externalUserAccount + "] - sop: [" + sop + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Subscription s = query.select(subscription).from(subscription)
                .where(subscription.externalUserAccount.eq(externalUserAccount),
                        subscription.sop().eq(sop))
                .fetchFirst();

        return s;
    }

    public Subscription findByExternalCustomer(String externalCustomer, SOP sop) {
        if (externalCustomer == null || sop == null || sop.getId() == null) {
            String error = "Uno de los parametros es nulo!! externalCustomer: [" + externalCustomer + "] - sop: [" + sop + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Subscription s = query.select(subscription).from(subscription)
                .where(subscription.externalCustomer.eq(externalCustomer),
                        subscription.sop().eq(sop))
                .fetchFirst();

        return s;
    }

    public List<Subscription> getByExternalUserAccount(String externalUserAccount, SOP sop) {
        if (externalUserAccount == null || sop == null || sop.getId() == null) {
            String error = "Uno de los parametros es nulo!! externalUserAccount: [" + externalUserAccount + "] - sop: [" + sop + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<Subscription> s = query.select(subscription).from(subscription)
                .where(subscription.externalUserAccount.eq(externalUserAccount),
                        subscription.sop().eq(sop))
                .fetch();

        return s;
    }

    public List<Subscription> getSyncSubscriptions(Provider provider, Date time) {
        QSubscription subscription = QSubscription.subscription;
        List<Subscription> sl = new ArrayList();

        JPAQuery<?> query = new JPAQuery(getEntityManager());
        //Estados aun pendientes.. 
        List<Subscription> sla = query.select(subscription).from(subscription)
                .where(ObjectPredicates.combine(subscription.sop().provider().id.eq(provider.getId()),
                        subscription.status.eq(Subscription.Status.PENDING),
                        ObjectPredicates.or(subscription.subscriptionRegistry().syncDate.isNull(),
                                subscription.subscriptionDate.hour().eq(time.getHours()))))
                .orderBy(subscription.subscriptionRegistry().syncDate.asc())
                .limit(1000)
                .fetch();

        if (sla != null) {
            sl.addAll(sla);
        }

        JPAQuery<?> query2 = new JPAQuery(getEntityManager());
        //Activos sin sincronizar
        sla = query2.select(subscription).from(subscription)
                .where(ObjectPredicates.combine(subscription.sop().provider().id.eq(provider.getId()),
                        subscription.status.eq(Subscription.Status.ACTIVE),
                        subscription.subscriptionRegistry().syncDate.isNull()))
                .orderBy(subscription.subscriptionRegistry().syncDate.asc())
                .limit(1500)
                .fetch();

        if (sla != null) {
            sl.addAll(sla);
        }

        JPAQuery<?> query3 = new JPAQuery(getEntityManager());
        //Activos sincronizados
        sla = query3.select(subscription).from(subscription)
                .where(ObjectPredicates.combine(subscription.sop().provider().id.eq(provider.getId()),
                        subscription.status.eq(Subscription.Status.ACTIVE),
                        subscription.subscriptionRegistry().syncDate.month().loe(time.getMonth() + 1)
                ))
                .orderBy(subscription.subscriptionRegistry().syncDate.asc())
                .limit(1500)
                .fetch();

        if (sla != null) {
            sl.addAll(sla);
        }

        JPAQuery<?> query4 = new JPAQuery(getEntityManager());
        //Activos sincronizados del ultimo year
        sla = query4.select(subscription).from(subscription)
                .where(ObjectPredicates.combine(subscription.sop().provider().id.eq(provider.getId()),
                        subscription.status.eq(Subscription.Status.ACTIVE),
                        subscription.subscriptionRegistry().syncDate.year().lt(time.getYear() + 1900)
                ))
                .orderBy(subscription.subscriptionRegistry().syncDate.asc())
                .limit(1500)
                .fetch();

        if (sla != null) {
            sl.addAll(sla);
        }

        return sl;
    }

    public Subscription findByMSISDNAndCountry(String msisdn, Country country) {
        List<Subscription> sl = getByMSISDNAndCountry(msisdn, country);

        Subscription s = null;
        if (sl != null && !sl.isEmpty()) {
            s = sl.get(0);
        }

        return s;
    }

    public List<Subscription> getByMSISDNAndCountry(String msisdn, Country country) {
        if (msisdn == null || country == null || country.getId() == null) {
            String error = "Uno de los parametros es nulo!! msisdn: [" + msisdn + "] - country: [" + country + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<Subscription> sl = query.select(subscription).from(subscription)
                .where(ObjectPredicates.combine(subscription.sop().provider().country().id.eq(country.getId()),
                        subscription.userAccount.like("%" + msisdn)))
                .orderBy(subscription.subscriptionDate.desc())
                .fetch();

        return sl;
    }

    public List<Subscription> findSubscriptionsByProvider(String providerName, Date from, Date to) {
        if (providerName == null || from == null || to == null) {
            String error = "Uno de los parametros es nulo!! providerName: [" + providerName + "] - from: [" + from + "] - end: [" + to + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<Subscription> sl = query.select(subscription).from(subscription)
                .where(ObjectPredicates.combine(subscription.sop().provider().name.eq(providerName),
                        subscription.subscriptionDate.between(from, to)))
                .fetch();

        return sl;
    }

    public List<Subscription> findSubscriptionsByUserAccount(String userAccount) {
        if (userAccount == null) {
            String error = "Uno de los parametros es nulo!! userAccount: [" + userAccount + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<Subscription> sl = query.select(subscription).from(subscription)
                .where(subscription.userAccount.eq(userAccount))
                .fetch();

        return sl;
    }

    public List<Subscription> getActiveSubscriptions(List<String> userAccounts, Collection<SOP> sops) {

        if (Collections.isEmpty(userAccounts)) {
            return null;
        }

        if (sops == null || sops.isEmpty() || userAccounts.isEmpty()) {
            String error = "Algunos de los parametros son incorrectos... userAccounts: [" + userAccounts + "] - sops: [" + sops + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<Subscription> sl = query.select(subscription).from(subscription)
                .where(subscription.userAccount.in(userAccounts),
                        subscription.sop().in(sops), subscription.status.eq(Subscription.Status.ACTIVE))
                .fetch();

        return sl;
    }

    public Subscription getLastSubscription(List<String> userAccounts, Collection<SOP> sops) {

        if (userAccounts == null || userAccounts.isEmpty()) {
            return null;
        }

        if (sops == null || sops.isEmpty() || userAccounts.isEmpty()) {
            String error = "Algunos de los parametros son incorrectos... userAccounts: [" + userAccounts + "] - sops: [" + sops + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Subscription s = query.select(subscription).from(subscription)
                .where(subscription.userAccount.in(userAccounts), subscription.sop().in(sops))
                .orderBy(subscription.subscriptionDate.desc())
                .fetchFirst();

        return s;
    }

    public List<Subscription> getSubscriptionsWithoutSubscriptionRegistry(Provider provider) {
        if (provider == null || provider.getId() == null) {
            String error = "Algunos de los parametros son incorrectos... provider: [" + provider + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<Subscription> sl = query.select(subscription).from(subscription)
                .where(subscription.sop().provider().id.eq(provider.getId()),
                        subscription.subscriptionRegistry().isNull())
                .fetch();

        return sl;
    }

    public List<Subscription> getActiveSubscriptions(String userAccount, String providerName) {
        if (providerName == null || userAccount == null) {
            String error = "Algunos de los parametros son incorrectos... userAccount:[" + userAccount + "] - providerName: [" + providerName + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<Subscription> sl = query.select(subscription).from(subscription)
                .where(ObjectPredicates.combine(subscription.userAccount.eq(userAccount),
                        subscription.sop().provider().name.eq(providerName),
                        ObjectPredicates.or(subscription.status.eq(Subscription.Status.ACTIVE),
                                subscription.status.eq(Subscription.Status.PENDING))))
                .fetch();

        return sl;
    }

    /**
     * Retorna las suscripciones activas a las que se les debe realizar el envio
     * del SMS por renovacion de servicio
     */
    public List<BigInteger> getSubscriptionsRenew(SOP sop, Integer frecuencyDays) {
        if (sop == null || frecuencyDays == null) {
            String error = "Algunos de los parametros son incorrectos... sop:[" + sop + "] - frecuency: [" + frecuencyDays + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        // log.info("Buscando suscripciones activas para: [" + sop.getId() + "][" + frecuencyDays + "]");
        String sql = "SELECT S.ID FROM paymentHub.SUBSCRIPTION S, paymentHub.SUBSCRIPTION_REGISTRY SR "
                + "WHERE S.SUBSCRIPTION_REGISTRY_ID=SR.ID "
                + "AND S.`STATUS`='ACTIVE' AND S.SOP_ID = ? "
                + "AND DAYOFWEEK(SYSDATE())=DAYOFWEEK(S.SUBSCRIPTION_DATE) "
                + "AND (( DATE(SYSDATE()) >=  DATE(DATE_ADD(S.SUBSCRIPTION_DATE, INTERVAL ? DAY)) AND SR.LAST_MT_SENT IS NULL) "
                + "OR (SR.LAST_MT_SENT IS NOT NULL AND DATE(SYSDATE()) >=  DATE(DATE_ADD(SR.LAST_MT_SENT, INTERVAL ? DAY)))) "
                + "ORDER BY S.SUBSCRIPTION_DATE, SR.LAST_MT_SENT "
                + "LIMIT 2000 ";

        EntityManager em = getEntityManager();
        Query q = em.createNativeQuery(sql);
        q.setParameter(1, sop.getId());
        q.setParameter(2, frecuencyDays);
        q.setParameter(3, frecuencyDays);
        List<BigInteger> result = (List<BigInteger>) q.getResultList();
        return result;
    }

    /**
     * Retorna las suscripciones activas a las que se les debe realizar el envio
     * del SMS por renovacion de servicio
     */
    public List<BigInteger> getSubscriptionsMidRenew(SOP sop, Integer frecuencyDays) {
        if (sop == null || frecuencyDays == null) {
            String error = "Algunos de los parametros son incorrectos... sop:[" + sop + "] - frecuency: [" + frecuencyDays + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        // log.info("Buscando suscripciones activas para: [" + sop.getId() + "][" + frecuencyDays + "]");
        String sql = "SELECT S.ID FROM paymentHub.SUBSCRIPTION S, paymentHub.SUBSCRIPTION_REGISTRY SR "
                + "WHERE S.SUBSCRIPTION_REGISTRY_ID=SR.ID "
                + "AND S.`STATUS`='ACTIVE' AND S.SOP_ID = ? "
                + "AND DAYOFWEEK(SYSDATE())=(DAYOFWEEK(S.SUBSCRIPTION_DATE) ?) "
                + "AND SR.LAST_MT_SENT IS NOT NULL AND DATE(SYSDATE()) >=  DATE(DATE_ADD(SR.LAST_MT_SENT, INTERVAL ? DAY)) "
                + "ORDER BY S.SUBSCRIPTION_DATE, SR.LAST_MT_SENT "
                + "LIMIT 2000  ";

        EntityManager em = getEntityManager();
        Query q = em.createNativeQuery(sql);
        q.setParameter(1, sop.getId());
        q.setParameter(2, frecuencyDays);
        q.setParameter(3, frecuencyDays);
        List<BigInteger> result = (List<BigInteger>) q.getResultList();
        return result;
    }

    /**
     * Retorna las suscripciones activas a las que se les debe realizar el envio
     * del SMS por renovacion de servicio dos veces a la semana
     */
    public List<BigInteger> getSubscriptionsMidRenewTwiceWeek(SOP sop) {
        if (sop == null) {
            String error = "Algunos de los parametros son incorrectos... sop:[" + sop + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        // log.info("Buscando suscripciones activas para: [" + sop.getId() + "][" + frecuencyDays + "]");
        String sql = "SELECT" +
                "       ID  " +
                " FROM (" +
                "                  SELECT" +
                "                        SYSDATE()                                      as NOW_DATE," +
                "                        WEEKDAY(SYSDATE())                             as WEEK_NOW," +
                "                         S.ID," +
                "                         LAST_MT_SENT," +
                "                         DATE_ADD(S.SUBSCRIPTION_DATE, INTERVAL 3 DAY) as FIRST_SEND," +
                "                         WEEKDAY(S.SUBSCRIPTION_DATE) as SEND_DAY1," +
                "                         WEEKDAY(DATE_ADD(S.SUBSCRIPTION_DATE, INTERVAL 3 DAY)) as SEND_DAY2" +
                "                  FROM (" +
                "                           SELECT ID, USER_ACCOUNT, SUBSCRIPTION_DATE" +
                "                           FROM paymentHub.SUBSCRIPTION" +
                "                           WHERE" +
                "                             STATUS = 'ACTIVE'" +
                "                             AND" +
                "                             SOP_ID = :sop " +
                "                       ) as S" +
                "                    Inner Join" +
                "                       (" +
                "                           SELECT" +
                "                                  R.SUBSCRIPTION_ID," +
                "                                  R.LAST_MT_SENT" +
                "                           From" +
                "                                SUBSCRIPTION_REGISTRY R" +
                "                            Inner Join" +
                "                                (" +
                "                                    SELECT" +
                "                                           MAX(CREATED_DATE) as MAX_CREATED_DATE," +
                "                                           SUBSCRIPTION_ID" +
                "                                    FROM" +
                "                                         SUBSCRIPTION_REGISTRY" +
                "                                    WHERE" +
                "                                          SOP_ID = :sop " +
                "                                      AND UNSUBSCRIPTION_DATE IS NULL" +
                "                                    GROUP BY SUBSCRIPTION_ID" +
                "                                ) as LR On" +
                "                                        LR.SUBSCRIPTION_ID = R.SUBSCRIPTION_ID" +
                "                                        AND" +
                "                                        LR.MAX_CREATED_DATE = R.CREATED_DATE" +
                "                       ) as RE ON" +
                "                           RE.SUBSCRIPTION_ID = S.ID" +
                "              ) as data " +
                " WHERE" +
                "    LAST_MT_SENT Is NULL AND NOW_DATE >= FIRST_SEND" +
                "    OR" +
                "    (" +
                "        NOT LAST_MT_SENT IS NULL " +
                "            AND LAST_MT_SENT < NOW_DATE " +
                "            AND NOT( YEAR(LAST_MT_SENT) = YEAR(NOW_DATE) AND MONTH(LAST_MT_SENT)=MONTH(NOW_DATE) AND DAY(LAST_MT_SENT) = DAY(NOW_DATE) )" +
                "            AND " +
                "        (" +
                "            WEEK_NOW = SEND_DAY1" +
                "        )" +
                "    ) " +
                " LIMIT 2000  ";

        EntityManager em = getEntityManager();
        Query q = em.createNativeQuery(sql);
        q.setParameter("sop", sop.getId());
        // log.info("SMS renews: [Twice week][query][" + q.toString() + "]");
        List<BigInteger> result = (List<BigInteger>) q.getResultList();
        // log.info("SMS renews: [Twice week][count][" + result.size() + "]");
        return result;
    }

    public List<Subscription> getRetentionSubscriptionsBeforeDays(String providerName, int days) {
        if (providerName == null || days <=0 ) {
            String error = "Algunos de los parametros son incorrectos... providerName: [" + providerName + "] - days: [" + days + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -days);
        Date date = c.getTime();

        return query.select(subscription).from(subscription)
                .where(ObjectPredicates.combine(subscription.status.eq(Subscription.Status.RETENTION),
                        subscription.sop().provider().name.eq(providerName),
                        subscription.subscriptionDate.before(date)))
                .fetch();
    }

    public List<Subscription> getActived24hsAndWithoutBilling() {
        QSubscription subscription = QSubscription.subscription;
        QSubscriptionRegistry subscriptionRegistry = QSubscriptionRegistry.subscriptionRegistry;
        QSubscriptionBilling qSubscriptionBilling = QSubscriptionBilling.subscriptionBilling;

        JPAQuery<?> query = new JPAQuery(getEntityManager());

        //24 horas que se creo
        Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, -24);
        Date date = c.getTime();

        List<Subscription> sl = query.select(subscription).from(subscription)
                .innerJoin(subscriptionRegistry).on(subscriptionRegistry.id.eq(subscription.subscriptionRegistry().id))
                .leftJoin(qSubscriptionBilling).on(qSubscriptionBilling.subscriptionRegistry().id.eq(subscription.subscriptionRegistry().id))
                .where(ObjectPredicates.combine(subscription.status.eq(Subscription.Status.ACTIVE),
                        qSubscriptionBilling.id.isNull(),
                        subscriptionRegistry.originSubscription.contains("ACTIVATED-FIRST-24HRS"),
                        subscription.subscriptionDate.before(date)))
                .fetch();

        return sl;
    }

}
