
package org.csapi.wsdl.parlayx.subscribeproduct.v1_0.service;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Holder;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.ConsumeHistoryInfoType;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.ConsumeRefundHistoryInfo;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.NamedParameterList;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.SubInfoType;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.SubScriptionInfoType;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.UserID;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "SubscribeProduct", targetNamespace = "http://www.csapi.org/wsdl/parlayx/subscribeproduct/v1_0/interface")
@XmlSeeAlso({
    org.csapi.schema.parlayx.common.v2_1.ObjectFactory.class,
    org.csapi.schema.parlayx.subscribeproduct.v1_0.ObjectFactory.class,
    org.csapi.schema.parlayx.subscribeproduct.v1_0.local.ObjectFactory.class
})
public interface SubscribeProduct {


    /**
     * 
     * @param extensionInfo
     * @param subInfo
     * @param version
     * @param userID
     * @param channelID
     * @return
     *     returns int
     * @throws PolicyException
     * @throws ServiceException
     */
    @WebMethod
    @WebResult(name = "resultCode", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
    @RequestWrapper(localName = "subscibeProduct", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.SubscibeProduct")
    @ResponseWrapper(localName = "subscibeProductResponse", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.SubscibeProductResponse")
    public int subscibeProduct(
        @WebParam(name = "version", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String version,
        @WebParam(name = "userID", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        UserID userID,
        @WebParam(name = "subInfo", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        SubInfoType subInfo,
        @WebParam(name = "channelID", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        int channelID,
        @WebParam(name = "extensionInfo", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        NamedParameterList extensionInfo)
        throws PolicyException, ServiceException
    ;

    /**
     * 
     * @param extensionInfo
     * @param subInfo
     * @param version
     * @param userID
     * @param channelID
     * @return
     *     returns int
     * @throws PolicyException
     * @throws ServiceException
     */
    @WebMethod
    @WebResult(name = "resultCode", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
    @RequestWrapper(localName = "unSubscribeProduct", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.UnSubscribeProduct")
    @ResponseWrapper(localName = "unSubscribeProductResponse", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.UnSubscribeProductResponse")
    public int unSubscribeProduct(
        @WebParam(name = "version", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String version,
        @WebParam(name = "userID", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        UserID userID,
        @WebParam(name = "subInfo", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        SubInfoType subInfo,
        @WebParam(name = "channelID", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        int channelID,
        @WebParam(name = "extensionInfo", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        NamedParameterList extensionInfo)
        throws PolicyException, ServiceException
    ;

    /**
     * 
     * @param actionType
     * @param extensionInfo
     * @param resultCode
     * @param subScriptionInfos
     * @param version
     * @param userID
     * @throws PolicyException
     * @throws ServiceException
     */
    @WebMethod
    @RequestWrapper(localName = "getSubScriptionList", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.GetSubScriptionList")
    @ResponseWrapper(localName = "getSubScriptionListResponse", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.GetSubScriptionListResponse")
    public void getSubScriptionList(
        @WebParam(name = "version", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String version,
        @WebParam(name = "userID", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        UserID userID,
        @WebParam(name = "actionType", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        int actionType,
        @WebParam(name = "extensionInfo", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        NamedParameterList extensionInfo,
        @WebParam(name = "resultCode", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", mode = WebParam.Mode.OUT)
        Holder<Integer> resultCode,
        @WebParam(name = "subScriptionInfos", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", mode = WebParam.Mode.OUT)
        Holder<List<SubScriptionInfoType>> subScriptionInfos)
        throws PolicyException, ServiceException
    ;

    /**
     * 
     * @param fromDate
     * @param noteNo
     * @param productID
     * @param extensionInfo
     * @param startNote
     * @param toDate
     * @param resultCode
     * @param eventType
     * @param version
     * @param userID
     * @param recordNum
     * @param consumeHistoryInfos
     * @throws PolicyException
     * @throws ServiceException
     */
    @WebMethod
    @RequestWrapper(localName = "getConsumeHistoryList", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.GetConsumeHistoryList")
    @ResponseWrapper(localName = "getConsumeHistoryListResponse", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.GetConsumeHistoryListResponse")
    public void getConsumeHistoryList(
        @WebParam(name = "version", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String version,
        @WebParam(name = "userID", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        UserID userID,
        @WebParam(name = "fromDate", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String fromDate,
        @WebParam(name = "toDate", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String toDate,
        @WebParam(name = "startNote", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        int startNote,
        @WebParam(name = "noteNo", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        int noteNo,
        @WebParam(name = "eventType", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        Integer eventType,
        @WebParam(name = "productID", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String productID,
        @WebParam(name = "extensionInfo", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        NamedParameterList extensionInfo,
        @WebParam(name = "resultCode", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", mode = WebParam.Mode.OUT)
        Holder<Integer> resultCode,
        @WebParam(name = "recordNum", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", mode = WebParam.Mode.OUT)
        Holder<Integer> recordNum,
        @WebParam(name = "consumeHistoryInfos", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", mode = WebParam.Mode.OUT)
        Holder<List<ConsumeHistoryInfoType>> consumeHistoryInfos)
        throws PolicyException, ServiceException
    ;

    /**
     * 
     * @param actionType
     * @param extensionInfo
     * @param resultCode
     * @param subScriptionInfos
     * @param version
     * @param userID
     * @throws PolicyException
     * @throws ServiceException
     */
    @WebMethod
    @RequestWrapper(localName = "getSubScriptionListASP", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.GetSubScriptionListASP")
    @ResponseWrapper(localName = "getSubScriptionListASPResponse", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.GetSubScriptionListASPResponse")
    public void getSubScriptionListASP(
        @WebParam(name = "version", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String version,
        @WebParam(name = "userID", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        UserID userID,
        @WebParam(name = "actionType", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        int actionType,
        @WebParam(name = "extensionInfo", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        NamedParameterList extensionInfo,
        @WebParam(name = "resultCode", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", mode = WebParam.Mode.OUT)
        Holder<Integer> resultCode,
        @WebParam(name = "subScriptionInfos", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", mode = WebParam.Mode.OUT)
        Holder<List<SubScriptionInfoType>> subScriptionInfos)
        throws PolicyException, ServiceException
    ;

    /**
     * 
     * @param fromDate
     * @param consumeRefundHistoryInfos
     * @param noteNo
     * @param productID
     * @param extensionInfo
     * @param startNote
     * @param toDate
     * @param resultCode
     * @param eventType
     * @param version
     * @param userID
     * @param recordNum
     * @throws PolicyException
     * @throws ServiceException
     */
    @WebMethod
    @RequestWrapper(localName = "getConsumeRefundHistoryList", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.GetConsumeRefundHistoryList")
    @ResponseWrapper(localName = "getConsumeRefundHistoryListResponse", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", className = "org.csapi.schema.parlayx.subscribeproduct.v1_0.local.GetConsumeRefundHistoryListResponse")
    public void getConsumeRefundHistoryList(
        @WebParam(name = "version", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String version,
        @WebParam(name = "userID", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        UserID userID,
        @WebParam(name = "fromDate", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String fromDate,
        @WebParam(name = "toDate", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String toDate,
        @WebParam(name = "startNote", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        int startNote,
        @WebParam(name = "noteNo", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        int noteNo,
        @WebParam(name = "eventType", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        Integer eventType,
        @WebParam(name = "productID", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        String productID,
        @WebParam(name = "extensionInfo", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local")
        NamedParameterList extensionInfo,
        @WebParam(name = "resultCode", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", mode = WebParam.Mode.OUT)
        Holder<Integer> resultCode,
        @WebParam(name = "recordNum", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", mode = WebParam.Mode.OUT)
        Holder<Integer> recordNum,
        @WebParam(name = "consumeRefundHistoryInfos", targetNamespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", mode = WebParam.Mode.OUT)
        Holder<List<ConsumeRefundHistoryInfo>> consumeRefundHistoryInfos)
        throws PolicyException, ServiceException
    ;

}
