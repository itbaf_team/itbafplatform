package com.itbaf.platform.bo.web.modules;

import com.google.inject.Singleton;
import com.itbaf.platform.bo.web.rest.MainRestService;
import com.itbaf.platform.commons.filter.RequestFilter;
import com.itbaf.platform.pgp.commons.rest.ManagerRestServiceImpl;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import java.util.HashMap;

public class GuiceRestServicesConfigModule extends JerseyServletModule {

    @Override
    protected void configureServlets() {

        HashMap<String, String> options = new HashMap();
        options.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

        //Filters
        this.filter("/").through(RequestFilter.class);
        //this.filter("/bo/admin/*").through(RequestFilter.class);

        //Servlets
        serve("/bo/admin/*").with(GuiceContainer.class, options);

        bind(MainRestService.class).in(Singleton.class);
        bind(ManagerRestServiceImpl.class).in(Singleton.class);
    }
}
