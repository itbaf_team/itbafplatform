/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.smt.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING_OD;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.sync.ServiceProcessor;
import com.itbaf.platform.paymenthub.extention.smt.resources.SMTResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.OnDemandManager;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class OnDemandProcessorHandler extends ServiceProcessor {

    private final SMTResources smtResources;
    private final OnDemandManager ondemandManager;

    @Inject
    public OnDemandProcessorHandler(final SMTResources smtResources,
            final OnDemandManager ondemandManager) {
        this.smtResources = Validate.notNull(smtResources, "A SMTResources class must be provided");
        this.ondemandManager = Validate.notNull(ondemandManager, "An OnDemandManager class must be provided");
    }

    @Override
    public void syncSubscriptions(Provider provider) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ResponseMessage charge(OnDemandRegistry or) throws Exception {
        SOP sop = or.getSop();
        ResponseMessage rm = new ResponseMessage();
        try {
            or.setUserAccount(userAccountNormalizer(or.getUserAccount(), sop.getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            try {
                rm.data = mapper.writeValueAsString(or);
            } catch (JsonProcessingException ex1) {
            }
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        try {
            //Ignoramos el amount que viene y seteamos el configurado en el SOP
            Tariff t = sop.getMainTariff();
            or.getTransactionTracking().amount = t.getNetAmount();
            or.getTransactionTracking().currency = t.getCurrency();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            long x = instrumentedObject.getAndIncrementAtomicLong("SMT-Atomic", 2) % 1000;
            or.setTransactionId("SMT" + sdf.format(new Date()) + sop.getProvider().getCountry().getCode() + x);
            or.getTransactionTracking().transactionId = or.getTransactionId();

            String contentName = or.getTransactionTracking().contentName;
            String dText = sop.getIntegrationSettings().get("acepto.sms.text");
            if ((contentName + dText).length() > 160) {
                int lg = contentName.length() + dText.length() - 160;
                contentName = contentName.substring(0, contentName.length() - lg - 1);
            }
            dText = dText.replace("<name>", contentName).replace("<nameUpper>", contentName.toUpperCase())
                    .replace("<amount>", t.getFullAmount().longValue() + "");
            try {
                smtResources.sendSms(sop.getIntegrationSettings().get("shortcode"), or.getUserAccount(), dText, sop);
                or.setStatus(OnDemandRegistry.Status.OPTIN);
                or.setStatusMessage(sop.getIntegrationSettings().get("status.message.optin"));
            } catch (Exception ex) {
                or.setStatus(OnDemandRegistry.Status.ERROR);
                or.setStatusMessage(sop.getIntegrationSettings().get("status.message.error.sms"));
                rm.message = "Error al enviar el SMS de confirmacion de cobro. " + ex;
            }
        } catch (Exception ex) {
            log.error("Error al enviar SMS de confirmacion de cobro. [" + or + "]. " + ex, ex);
            rm.message = "Error al enviar SMS de confirmacion de cobro. " + ex;
            return rm;
        }

        rm.data = mapper.writeValueAsString(or);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    void chargeAccount(String shortcode, String msisdn, String ondemandKey, String ondemandType, List<SOP> sops) throws Exception {
        //  smtResources.sendSms("5725223", "573224402738", "123456789A123456789B123456789C123456789D123456789E123456789F123456789G123456789H123456789I123456789J123456789K123456789L123456789M123456789N123456789O123456789P123456789Q123456789R123456789S123456789T123456789U123456789V123456789W123456789X123456789Y123456789Z123456789a123456789b123456789c", sop);
        //  smtResources.startSMSNotification(sop);
        //  smtResources.stopSMSNotification(sop);

        SOP sop = sops.get(0);
        OnDemandRegistry or = null;
        switch (ondemandType) {
            case "ON_DEMAND_PRE":
                //Debe existir un ONDEMAND_REGISTRY previamente para hacer el cobro
                or = ondemandManager.getAvailableByStatusOptin(msisdn, sops);
                if (or != null) {
                    sop = or.getSop();
                }
                break;
            case "ON_DEMAND_POST":
                //Se realiza el cobro directamente
                String configuration = sop.getIntegrationSettings().get("configuration." + ondemandKey);

                if (configuration == null) {
                    log.fatal("No existe una configuracion para: [configuration." + ondemandKey + "] en el SOP: [" + sop + "]");
                    or = null;
                    break;
                }
                Tariff t = sop.getMainTariff();

                FacadeRequest transactionTracking = mapper.readValue(configuration, FacadeRequest.class);
                or = new OnDemandRegistry();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
                long x = instrumentedObject.getAndIncrementAtomicLong("SMT-Atomic", 2) % 1000;
                or.setTransactionId("SMT" + sdf.format(new Date()) + sop.getProvider().getCountry().getCode() + x);
                transactionTracking.transactionId = or.getTransactionId();
                transactionTracking.amount = t.getNetAmount();
                transactionTracking.currency = t.getCurrency();
                or.setId(sop.getId() + ".TID." + or.getTransactionId());
                or.setOndemandDate(new Date());
                or.setSop(sop);
                or.setStatus(OnDemandRegistry.Status.PENDING);
                or.setStatusMessage("A la espera de generar cobro.");
                or.setTransactionTracking(transactionTracking);
                or.setUserAccount(msisdn);
                try {
                    ondemandManager.ondemandRegistryProcess(or);
                } catch (Exception ex) {
                    log.error("Error al actualizar OnDemandRegistry: [" + or + "]. " + ex, ex);
                }
                break;
            default:
                log.error("No existe un valor valido de [ondemandKey][ondemandType]: [" + ondemandKey + "][" + ondemandType + "]. msisdn: [" + msisdn + "] - SOP: [" + sop + "]");
                return;
        }

        boolean smsError = false;

        if (or != null) {
            if (or.getOriginOndemand() == null) {
                or.setOriginOndemand("SMS:" + ondemandKey);
            } else {
                or.setOriginOndemand(or.getOriginOndemand() + "|" + "SMS:" + ondemandKey);
            }
            Holder<String> resultMessage = new Holder();
            String result = smtResources.charge(or.getUserAccount(), sop, or.getTransactionTracking(), ondemandKey, resultMessage);
            or.setStatus(OnDemandRegistry.Status.ERROR);
            or.setStatusMessage("No se pudo realizar el cobro.");
            if (result != null) {
                switch (result) {
                    case "0":
                    case "SVC0000":
                        or.setStatus(OnDemandRegistry.Status.CHARGED);
                        or.setStatusMessage("SUCCESS");
                        break;
                    case "SVC0001":
                    case "SVC0270":
                        Tariff t = sop.getMainTariff();
                        String text = sop.getIntegrationSettings().get("sms.message.error.charge");
                        text = text.replace("<name>", or.getTransactionTracking().contentName)
                                .replace("<nameUpper>", or.getTransactionTracking().contentName.toUpperCase())
                                .replace("<amount>", t.getFullAmount().longValue() + "");
                        or.setStatusMessage(text);
                        or.setStatus(OnDemandRegistry.Status.ERROR_CHARGE);
                        break;
                    default:
                        or.setStatusMessage(resultMessage.value);
                }
                try {
                    ondemandManager.ondemandRegistryProcess(or);
                } catch (Exception ex) {
                    log.error("Error al actualizar OnDemandRegistry: [" + or + "]. " + ex, ex);
                }
                switch (or.getStatus()) {
                    case CHARGED:
                        Tariff t = sop.getMainTariff();
                        try {
                            PaymentHubMessage msg = new PaymentHubMessage();
                            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
                            msg.setMessageType(MessageType.ONDEMAND_TOTAL);
                            msg.setOrigin(PaymentHubMessage.OriginMessage.SMS);
                            msg.setInternalId(or.getId());
                            msg.setUserAccount(or.getUserAccount());
                            msg.setSopId(or.getSop().getId());
                            msg.setTransactionId(or.getTransactionId());
                            msg.setFromDate(new Date());
                            msg.setCurrencyId(or.getTransactionTracking().currency);
                            msg.setFullAmount(t.getFullAmount());
                            msg.setNetAmount(or.getTransactionTracking().amount);
                            msg.setTransactionTracking(or.getTransactionTracking());
                            msg.setTariffId(t.getId());

                            String jsonResponse = mapper.writeValueAsString(msg);
                            if (jsonResponse != null) {
                                rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING_OD, jsonResponse);
                            }
                        } catch (Exception ex) {
                            log.error("Error al generar Registro de cobro. " + ex, ex);
                        }
                        try {
                            or.getTransactionTracking().contentName = or.getTransactionTracking().contentName
                                    .replace("á", "a")
                                    .replace("é", "e")
                                    .replace("í", "i")
                                    .replace("ó", "o")
                                    .replace("ú", "u")
                                    .replace("ü", "u")
                                    .replace("Á", "A")
                                    .replace("É", "E")
                                    .replace("Í", "I")
                                    .replace("Ó", "O")
                                    .replace("Ú", "U");
                            
                            String text = sop.getIntegrationSettings().get("sms.message.charge");
                            text = text.replace("<name>", or.getTransactionTracking().contentName)
                                    .replace("<nameUpper>", or.getTransactionTracking().contentName.toUpperCase())
                                    .replace("<link>", or.getTransactionTracking().contentUrl)
                                    .replace("<amount>", t.getFullAmount().longValue() + "");
                            smtResources.sendSms(sop.getIntegrationSettings().get("shortcode"), or.getUserAccount(), text, sop);
                        } catch (Exception ex) {
                            log.error("Error al enviar SMS. [" + or.getUserAccount() + "]. " + ex, ex);
                        }
                        break;
                    case ERROR:
                    case ERROR_CHARGE:
                        smsError = true;
                        break;
                }
            } else {
                try {
                    ondemandManager.ondemandRegistryProcess(or);
                } catch (Exception ex) {
                    log.error("Error al actualizar OnDemandRegistry: [" + or + "]. " + ex, ex);
                }
                smsError = true;
            }
        } else {
            log.info("No existe un ONDEMAND_REGISTRY para msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "] - Se envia un SMS: [sms.message.default]");
            try {
                smtResources.sendSms(sop.getIntegrationSettings().get("shortcode"), msisdn, sop.getIntegrationSettings().get("sms.message.default"), sop);
            } catch (Exception ex) {
            }
        }

        if (smsError) {
            try {
                String text = sop.getIntegrationSettings().get("sms.message.error.charge");
                text = text.replace("<name>", or.getTransactionTracking().contentName)
                        .replace("<nameUpper>", or.getTransactionTracking().contentName.toUpperCase());
                smtResources.sendSms(sop.getIntegrationSettings().get("shortcode"), or.getUserAccount(), text, sop);
            } catch (Exception ex) {
                log.error("Error al enviar SMS. [" + or.getUserAccount() + "]. " + ex, ex);
            }
        }
    }

    @Override
    public ChannelQuantityProvider getChannelQuantityProvider(Provider p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean processMessage(String message) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void syncSubscription(Subscription s) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
