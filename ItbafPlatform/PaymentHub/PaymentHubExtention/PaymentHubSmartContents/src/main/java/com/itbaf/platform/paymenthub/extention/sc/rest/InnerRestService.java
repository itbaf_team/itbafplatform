package com.itbaf.platform.paymenthub.extention.sc.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.sc.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.ws.InnerService;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/inner")
@Singleton
@lombok.extern.log4j.Log4j2
public class InnerRestService implements InnerService {

    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public InnerRestService(ServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
    }

    @POST
    @Path("/unsubscribe")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response unsubscribe(Subscription subscription) {
        //  log.info("Unsubscribe [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm = serviceProcessorHandler.unsubscribe(subscription);
            return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al dar de baja. [" + subscription + "]. " + ex, ex);
            rm.message = "Error al dar de baja. " + ex.getMessage();
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscribe")
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscribe(Subscription subscription) {
        //  log.info("Subscribe [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        try {
            rm = serviceProcessorHandler.subscribe(subscription);
            return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al suscribir. [" + subscription + "]. " + ex, ex);
            rm.message = "Error al suscribir. " + ex.getMessage();
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscribe/pin/send")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendPin(Subscription subscription) {
        //  log.info("SendPin [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm = serviceProcessorHandler.sendPin(subscription);
            return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al enviar PIN/OPTIN. [" + subscription + "]. " + ex, ex);
            rm.message = "al enviar PIN/OPTIN. " + ex.getMessage();
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscribe/pin/validate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validatePin(Subscription subscription, @QueryParam("pin") final String pin) {
        //  log.info("ValidatePin [" + subscription + "] - PIN: [" + pin + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            String xpin = pin;
            if (subscription.getObject() != null) {
                xpin = (String) subscription.getObject();
                xpin = xpin.trim();
            }
            rm = serviceProcessorHandler.validatePin(subscription, xpin);
            return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al validar validatePin/OPTIN. [" + subscription + "]. " + ex, ex);
            rm.message = "al validar validatePin/OPTIN. " + ex.getMessage();
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscription/status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptionStatus(Subscription subscription) {
        //  log.info("SubscriptionStatus [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm = serviceProcessorHandler.getSubscriptionStatus(subscription);
        } catch (Exception ex) {
            rm.message = "Error al procesar status." + ex.getMessage();
            log.error(rm.message, ex);
        }
        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscription/normalizer")
    @Produces(MediaType.APPLICATION_JSON)
    public Response userAccountNormalizer(Subscription subscription) {

        //  log.info("userAccountNormalizer [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm = serviceProcessorHandler.userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.message = "Error al procesar userAccountNormalizer. " + ex.getMessage();
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR_403;
        }
        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscription/send/sms")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendSMS(Subscription subscription) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Response charge(OnDemandRegistry or) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
