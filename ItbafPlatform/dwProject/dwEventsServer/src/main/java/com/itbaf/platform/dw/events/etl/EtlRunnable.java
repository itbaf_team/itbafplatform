/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events.etl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.services.stats.StatsService;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.itbaf.platform.dw.events.etl.model.EtlQuery;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.pgp.commons.resources.ClientCache;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import java.util.concurrent.ExecutorService;
import org.redisson.api.RLock;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class EtlRunnable implements Runnable {

    private final ObjectMapper mapper;
    private final ClientCache clientCache;
    private final RabbitMQProducer rabbitMQProducer;
    private final InstrumentedObject instrumentedObject;
    private final BasicDataBaseConnection dataBaseConnection;
    private final String ETL_QUERY = "SELECT EQ.ID,EQ.TYPE,EQ.QUERY FROM ETL_QUERY EQ  ";

    @Inject
    public EtlRunnable(final ObjectMapper mapper,
            final ClientCache clientCache,
            final StatsService statsService,
            final ExecutorService cachedThreadPool,
            final RabbitMQProducer rabbitMQProducer,
            final InstrumentedObject instrumentedObject,
            final BasicDataBaseConnection dataBaseConnection) {
        this.mapper = mapper;
        this.clientCache = clientCache;
        this.rabbitMQProducer = rabbitMQProducer;
        this.instrumentedObject = instrumentedObject;
        this.dataBaseConnection = dataBaseConnection;
    }

    @Override
    public void run() {
        String threadName = CommonFunction.getTID("etl");
        Thread.currentThread().setName(threadName);
        List<EtlQuery> queries = getQueries();
        for (EtlQuery etl : queries) {
            String mm = "etl_" + etl;
            RLock lock = instrumentedObject.lockObject(mm, 60L);
            try {
                String dm = clientCache.getTemporalData(mm);
                if (dm != null) {
                    // log.info("Ignorando etl_query: [" + etl + "]");
                    continue;
                }
                clientCache.setTemporalData(mm, etl.toString(), 1300L);
                rabbitMQProducer.messageSender(CommonFunction.PROCESS_DW_EVENTS_QUEUE + ElasticData.ElasticType.query, mapper.writeValueAsString(etl));
            } catch (Exception ex) {
                log.error("Error al procesar etl_query [" + etl + "]. " + ex, ex);
            }
            instrumentedObject.unLockObject(lock);
        }

    }

    private List<EtlQuery> getQueries() {
        List<EtlQuery> queries = new ArrayList();
        Connection conn = null;
        try {
            conn = dataBaseConnection.getDBDWConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(ETL_QUERY);

            while (rs.next()) {
                EtlQuery etl = new EtlQuery();
                etl.id = rs.getLong("ID");
                etl.type = rs.getString("TYPE").toLowerCase();
                etl.query = rs.getString("QUERY");
                queries.add(etl);
            }
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex) {
            }
        } catch (Exception ex) {
            log.error("Error al obtener Queries ETL. " + ex, ex);
        } finally {
            dataBaseConnection.closeDWConnection(conn);
        }
        return queries;
    }

}
