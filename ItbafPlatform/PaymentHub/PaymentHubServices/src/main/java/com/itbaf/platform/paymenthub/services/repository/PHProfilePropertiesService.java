/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.services.service.PropertyManagerService;






/**
 *
 * @author javier
 */
public interface PHProfilePropertiesService extends PropertyManagerService {

    public String getValueByProviderKey(Long providerId, String key);

    public String getValueByProviderKey(String provider, String key);
}
