package com.itbaf.platform.paymenthub.extention.store.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultClaims;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class StoreResources {

    private final ObjectMapper mapper;
    private final RequestClient requestClient;

    @Inject
    public StoreResources(final ObjectMapper mapper,
                          final RequestClient requestClient) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = requestClient;
    }

    public ResponseMessage.Status deleteSubscription(String msisdn, SOP sop) throws Exception  {
        String providerToken = sop.getIntegrationSettings().get("providerToken");
        String contentId = sop.getIntegrationSettings().get("store_serviceId");
        String endpoint = sop.getIntegrationSettings().get("store_endpoint");
        StoreResponse storeResponse = null;

        String response = requestClient.requestJsonDeleteStorePersonal(endpoint + msisdn + "/subscriptions/" + contentId + "?providerToken=" + providerToken);

        log.info("deleteSubscription. ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. Respuesta de Store: [" + response + "]");

        if( response == null || !response.equals("204") ){
            return ResponseMessage.Status.ERROR;
        }

        StoreResponse tr = this.consultStatusContent(msisdn, contentId, sop);
        if (tr == null || tr.status == null) {
            return ResponseMessage.Status.ERROR;
        } else {
            if ( !tr.status.equals("CANCELLED")) {
                return ResponseMessage.Status.ERROR;
            }
        }

        return ResponseMessage.Status.OK;
    }

    public StoreResponse validateSubscribe(String token, SOP sop) throws Exception {
        if(token == null || token.equals("")){
            log.info("Token vacío");
            return null;
        }

        String providerToken = sop.getIntegrationSettings().get("providerToken");
        if(providerToken == null || providerToken.equals("")){
            log.info("No existe un providerToken configurado");
            return null;
        }

        try {
            DefaultClaims claim =  CommonFunction.validateToken(providerToken, token);
            return this.consultStatusContent(claim,sop);
        } catch (ExpiredJwtException ex) {
            log.info("Token expirado", ex);
            DefaultClaims claim  = (DefaultClaims) ex.getClaims();
            return this.consultStatusContent(claim,sop);
        } catch (SignatureException ex) {
            log.error("El providerToken '" + providerToken  + "' no es una firma válida", ex);
        } catch (Exception ex) {
            log.info("Error al validar firma", ex);
        }

        return null;
    }

    private StoreResponse consultStatusContent(String msisdn, String contentId, SOP sop) throws Exception {
        String providerToken = sop.getIntegrationSettings().get("providerToken");
        String endpoint = sop.getIntegrationSettings().get("store_endpoint");

        //Se consulta a Backoffice sobre el estado de la suscripción
        String response = requestClient.requestJsonGet(endpoint + msisdn + "/contents/" + contentId + "?providerToken=" + providerToken);

        if (response != null && !response.equals("{\"result\":[]}") ) {
            if( response.contains("{\"result\":[[") ){
                response = response.replace("{\"result\":[[","");
                response = response.replace("]]}","");
            } else if( response.contains("{\"result\":[") ){
                response = response.replace("{\"result\":[","");
                response = response.replace("]}","");
            }

            return mapper.readValue(response, StoreResponse.class);
        }

        return null;
    }

    private StoreResponse consultStatusContent(DefaultClaims claim, SOP sop) throws Exception {
        String msisdn = (String) claim.get("sub");
        String contentId = (String) claim.get("service");

        return this.consultStatusContent(msisdn, contentId, sop);
    }

    public StoreResponse consultStatusContent(String msisdn, SOP sop) throws Exception {
        String contentId = sop.getIntegrationSettings().get("store_serviceId");

        return this.consultStatusContent(msisdn, contentId, sop);
    }

}
