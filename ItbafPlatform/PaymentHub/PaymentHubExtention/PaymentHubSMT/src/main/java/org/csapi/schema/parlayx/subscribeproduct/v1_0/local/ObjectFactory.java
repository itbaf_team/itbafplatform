
package org.csapi.schema.parlayx.subscribeproduct.v1_0.local;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.csapi.schema.parlayx.subscribeproduct.v1_0.local package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetConsumeHistoryListResponse_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "getConsumeHistoryListResponse");
    private final static QName _GetConsumeRefundHistoryList_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "getConsumeRefundHistoryList");
    private final static QName _UnSubscribeProduct_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "unSubscribeProduct");
    private final static QName _GetConsumeHistoryList_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "getConsumeHistoryList");
    private final static QName _GetSubScriptionList_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "getSubScriptionList");
    private final static QName _GetSubScriptionListASP_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "getSubScriptionListASP");
    private final static QName _GetSubScriptionListResponse_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "getSubScriptionListResponse");
    private final static QName _SubscibeProductResponse_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "subscibeProductResponse");
    private final static QName _UnSubscribeProductResponse_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "unSubscribeProductResponse");
    private final static QName _SubscibeProduct_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "subscibeProduct");
    private final static QName _GetSubScriptionListASPResponse_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "getSubScriptionListASPResponse");
    private final static QName _GetConsumeRefundHistoryListResponse_QNAME = new QName("http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", "getConsumeRefundHistoryListResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.csapi.schema.parlayx.subscribeproduct.v1_0.local
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SubscibeProduct }
     * 
     */
    public SubscibeProduct createSubscibeProduct() {
        return new SubscibeProduct();
    }

    /**
     * Create an instance of {@link GetSubScriptionListASPResponse }
     * 
     */
    public GetSubScriptionListASPResponse createGetSubScriptionListASPResponse() {
        return new GetSubScriptionListASPResponse();
    }

    /**
     * Create an instance of {@link GetConsumeHistoryListResponse }
     * 
     */
    public GetConsumeHistoryListResponse createGetConsumeHistoryListResponse() {
        return new GetConsumeHistoryListResponse();
    }

    /**
     * Create an instance of {@link GetConsumeRefundHistoryList }
     * 
     */
    public GetConsumeRefundHistoryList createGetConsumeRefundHistoryList() {
        return new GetConsumeRefundHistoryList();
    }

    /**
     * Create an instance of {@link UnSubscribeProduct }
     * 
     */
    public UnSubscribeProduct createUnSubscribeProduct() {
        return new UnSubscribeProduct();
    }

    /**
     * Create an instance of {@link GetConsumeHistoryList }
     * 
     */
    public GetConsumeHistoryList createGetConsumeHistoryList() {
        return new GetConsumeHistoryList();
    }

    /**
     * Create an instance of {@link GetSubScriptionList }
     * 
     */
    public GetSubScriptionList createGetSubScriptionList() {
        return new GetSubScriptionList();
    }

    /**
     * Create an instance of {@link GetSubScriptionListASP }
     * 
     */
    public GetSubScriptionListASP createGetSubScriptionListASP() {
        return new GetSubScriptionListASP();
    }

    /**
     * Create an instance of {@link GetSubScriptionListResponse }
     * 
     */
    public GetSubScriptionListResponse createGetSubScriptionListResponse() {
        return new GetSubScriptionListResponse();
    }

    /**
     * Create an instance of {@link SubscibeProductResponse }
     * 
     */
    public SubscibeProductResponse createSubscibeProductResponse() {
        return new SubscibeProductResponse();
    }

    /**
     * Create an instance of {@link GetConsumeRefundHistoryListResponse }
     * 
     */
    public GetConsumeRefundHistoryListResponse createGetConsumeRefundHistoryListResponse() {
        return new GetConsumeRefundHistoryListResponse();
    }

    /**
     * Create an instance of {@link UnSubscribeProductResponse }
     * 
     */
    public UnSubscribeProductResponse createUnSubscribeProductResponse() {
        return new UnSubscribeProductResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsumeHistoryListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "getConsumeHistoryListResponse")
    public JAXBElement<GetConsumeHistoryListResponse> createGetConsumeHistoryListResponse(GetConsumeHistoryListResponse value) {
        return new JAXBElement<GetConsumeHistoryListResponse>(_GetConsumeHistoryListResponse_QNAME, GetConsumeHistoryListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsumeRefundHistoryList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "getConsumeRefundHistoryList")
    public JAXBElement<GetConsumeRefundHistoryList> createGetConsumeRefundHistoryList(GetConsumeRefundHistoryList value) {
        return new JAXBElement<GetConsumeRefundHistoryList>(_GetConsumeRefundHistoryList_QNAME, GetConsumeRefundHistoryList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnSubscribeProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "unSubscribeProduct")
    public JAXBElement<UnSubscribeProduct> createUnSubscribeProduct(UnSubscribeProduct value) {
        return new JAXBElement<UnSubscribeProduct>(_UnSubscribeProduct_QNAME, UnSubscribeProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsumeHistoryList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "getConsumeHistoryList")
    public JAXBElement<GetConsumeHistoryList> createGetConsumeHistoryList(GetConsumeHistoryList value) {
        return new JAXBElement<GetConsumeHistoryList>(_GetConsumeHistoryList_QNAME, GetConsumeHistoryList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSubScriptionList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "getSubScriptionList")
    public JAXBElement<GetSubScriptionList> createGetSubScriptionList(GetSubScriptionList value) {
        return new JAXBElement<GetSubScriptionList>(_GetSubScriptionList_QNAME, GetSubScriptionList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSubScriptionListASP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "getSubScriptionListASP")
    public JAXBElement<GetSubScriptionListASP> createGetSubScriptionListASP(GetSubScriptionListASP value) {
        return new JAXBElement<GetSubScriptionListASP>(_GetSubScriptionListASP_QNAME, GetSubScriptionListASP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSubScriptionListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "getSubScriptionListResponse")
    public JAXBElement<GetSubScriptionListResponse> createGetSubScriptionListResponse(GetSubScriptionListResponse value) {
        return new JAXBElement<GetSubScriptionListResponse>(_GetSubScriptionListResponse_QNAME, GetSubScriptionListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubscibeProductResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "subscibeProductResponse")
    public JAXBElement<SubscibeProductResponse> createSubscibeProductResponse(SubscibeProductResponse value) {
        return new JAXBElement<SubscibeProductResponse>(_SubscibeProductResponse_QNAME, SubscibeProductResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnSubscribeProductResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "unSubscribeProductResponse")
    public JAXBElement<UnSubscribeProductResponse> createUnSubscribeProductResponse(UnSubscribeProductResponse value) {
        return new JAXBElement<UnSubscribeProductResponse>(_UnSubscribeProductResponse_QNAME, UnSubscribeProductResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubscibeProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "subscibeProduct")
    public JAXBElement<SubscibeProduct> createSubscibeProduct(SubscibeProduct value) {
        return new JAXBElement<SubscibeProduct>(_SubscibeProduct_QNAME, SubscibeProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSubScriptionListASPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "getSubScriptionListASPResponse")
    public JAXBElement<GetSubScriptionListASPResponse> createGetSubScriptionListASPResponse(GetSubScriptionListASPResponse value) {
        return new JAXBElement<GetSubScriptionListASPResponse>(_GetSubScriptionListASPResponse_QNAME, GetSubScriptionListASPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsumeRefundHistoryListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local", name = "getConsumeRefundHistoryListResponse")
    public JAXBElement<GetConsumeRefundHistoryListResponse> createGetConsumeRefundHistoryListResponse(GetConsumeRefundHistoryListResponse value) {
        return new JAXBElement<GetConsumeRefundHistoryListResponse>(_GetConsumeRefundHistoryListResponse_QNAME, GetConsumeRefundHistoryListResponse.class, null, value);
    }

}
