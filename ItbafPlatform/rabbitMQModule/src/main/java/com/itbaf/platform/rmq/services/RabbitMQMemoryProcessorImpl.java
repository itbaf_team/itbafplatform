/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.rmq.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.cache.CacheMessage;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQMemoryProcessorImpl implements RabbitMQMessageProcessor {

    @Inject
    private ObjectMapper mapper;

    @Override
    public Boolean processMessage(String message) {
        try {
            CacheMessage cm = mapper.readValue(message, CacheMessage.class);
            switch (cm.type) {
                case UPDATE:
                    MainCache.clearCache();
                    log.info("MainCache actualizado.");
                    break;
            }
        } catch (Exception ex) {
            log.error("Error al actualizar memoria: [" + message + "]. " + ex, ex);
        }
        return true;
    }

}
