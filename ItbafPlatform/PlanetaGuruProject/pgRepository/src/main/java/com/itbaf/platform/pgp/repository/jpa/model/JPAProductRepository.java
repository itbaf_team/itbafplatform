/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.repository.jpa.model;

import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.pgp.model.Product;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAProductRepository extends AbstractJPARepositorySession<Product, Long> {

    @Inject
    public JPAProductRepository(final Provider<EntityManager> entityManagerProvider) {
        super(Product.class, entityManagerProvider);
    }

}
