/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.etl.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.stats.model.AdnetworkNotifierSimpleElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.BaseResumenElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.OauthUserSubscriptionElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.PGJContentElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.StatsMessage;
import com.itbaf.platform.paymenthub.commons.stats.model.SubscriptionBillingElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.SubscriptionRegistryElastic;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class BasicDataBaseClient {

    private final ObjectMapper mapper;
    private final BasicDataBaseConnection dataBaseConnection;
    private static final String SQL_MODE = "set @@sql_mode='no_engine_substitution'";

    @Inject
    public BasicDataBaseClient(final ObjectMapper mapper,
            final BasicDataBaseConnection dataBaseConnection) {
        this.mapper = mapper;
        this.dataBaseConnection = dataBaseConnection;
    }

    //set @@sql_mode='no_engine_substitution';
    public Boolean saveOrUpdateSubscriptionRegistry(StatsMessage sm) {
        String sql = "INSERT INTO dw_schema.SUBSCRIPTION_REGISTRY (ID, LAST_UPDATE_MILLIS, SUBSCRIPTION_ID, MESSAGE) VALUES (?,?,?,?) "
                + "ON DUPLICATE KEY UPDATE "
                + "SUBSCRIPTION_ID = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(SUBSCRIPTION_ID),SUBSCRIPTION_ID),"
                + "MESSAGE = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(MESSAGE),MESSAGE),"
                + "LAST_UPDATE_MILLIS = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(LAST_UPDATE_MILLIS),LAST_UPDATE_MILLIS) ";

        Connection conn = dataBaseConnection.getDBDWConnection();
        PreparedStatement ps = null;
        PreparedStatement psMode = null;
        try {
            SubscriptionRegistryElastic sre = (SubscriptionRegistryElastic) sm.elasticData;
            conn.setAutoCommit(false);
            psMode = conn.prepareStatement(SQL_MODE);
            ps = conn.prepareStatement(sql);
            ps.setLong(1, Long.parseLong(sre.id));
            ps.setLong(2, sm.lastUpdateMillis);
            ps.setLong(3, sre.subscriptionId);
            ps.setObject(4, mapper.writeValueAsString(sm));
            psMode.executeUpdate();
            ps.executeUpdate();
            conn.commit();
            return true;
        } catch (SQLException e) {
            if (conn != null) {
                try {
                    log.error("SQLException. Transaction is being rolled back. " + e, e);
                    conn.rollback();
                } catch (SQLException ex) {
                    log.error("Error al hacer rollback. " + ex, ex);
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar SQL: [" + sql + "]. " + ex, ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (psMode != null) {
                    psMode.close();
                }
            } catch (Exception ex) {
            }
            dataBaseConnection.closeDWConnection(conn);
        }

        return null;
    }

    public Boolean saveOrUpdateSubscriptionBilling(StatsMessage sm) {
        String sql = "INSERT INTO dw_schema.SUBSCRIPTION_BILLING (ID, LAST_UPDATE_MILLIS, SUBSCRIPTION_ID, SUBSCRIPTION_REGISTRY_ID, MESSAGE) VALUES (?,?,?,?,?) "
                + "ON DUPLICATE KEY UPDATE "
                + "SUBSCRIPTION_ID = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(SUBSCRIPTION_ID),SUBSCRIPTION_ID),"
                + "SUBSCRIPTION_REGISTRY_ID = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(SUBSCRIPTION_REGISTRY_ID),SUBSCRIPTION_REGISTRY_ID),"
                + "MESSAGE = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(MESSAGE),MESSAGE),"
                + "LAST_UPDATE_MILLIS = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(LAST_UPDATE_MILLIS),LAST_UPDATE_MILLIS) ";

        Connection conn = dataBaseConnection.getDBDWConnection();
        PreparedStatement ps = null;
        PreparedStatement psMode = null;
        try {
            SubscriptionBillingElastic sbe = (SubscriptionBillingElastic) sm.elasticData;
            conn.setAutoCommit(false);
            psMode = conn.prepareStatement(SQL_MODE);
            ps = conn.prepareStatement(sql);
            ps.setLong(1, Long.parseLong(sbe.id));
            ps.setLong(2, sm.lastUpdateMillis);
            ps.setLong(3, sbe.subscriptionId);
            ps.setLong(4, sbe.subscriptionRegistryId);
            ps.setObject(5, mapper.writeValueAsString(sm));
            psMode.executeUpdate();
            ps.executeUpdate();
            conn.commit();
            return true;
        } catch (SQLException e) {
            if (conn != null) {
                try {
                    log.error("SQLException. Transaction is being rolled back. " + e, e);
                    conn.rollback();
                } catch (SQLException ex) {
                    log.error("Error al hacer rollback. " + ex, ex);
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar SQL: [" + sql + "]. " + ex, ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (psMode != null) {
                    psMode.close();
                }
            } catch (Exception ex) {
            }
            dataBaseConnection.closeDWConnection(conn);
        }

        return null;
    }

    public Boolean saveOrUpdateAdnetworkNotifierSimple(StatsMessage sm) {
        String sql = "INSERT INTO dw_schema.ADNETWORK_NOTIFIER_SIMPLE (TRANSACTION_ID, LAST_UPDATE_MILLIS, MESSAGE) VALUES (?,?,?) "
                + "ON DUPLICATE KEY UPDATE "
                + "MESSAGE = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(MESSAGE),MESSAGE),"
                + "LAST_UPDATE_MILLIS = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(LAST_UPDATE_MILLIS),LAST_UPDATE_MILLIS) ";

        Connection conn = dataBaseConnection.getDBDWConnection();
        PreparedStatement ps = null;
        PreparedStatement psMode = null;
        try {
            AdnetworkNotifierSimpleElastic adn = (AdnetworkNotifierSimpleElastic) sm.elasticData;
            conn.setAutoCommit(false);
            psMode = conn.prepareStatement(SQL_MODE);
            ps = conn.prepareStatement(sql);
            ps.setString(1, adn.transactionId);
            ps.setLong(2, sm.lastUpdateMillis);
            ps.setObject(3, mapper.writeValueAsString(sm));
            psMode.executeUpdate();
            ps.executeUpdate();
            conn.commit();
            return true;
        } catch (SQLException e) {
            if (conn != null) {
                try {
                    log.error("SQLException. Transaction is being rolled back. " + e, e);
                    conn.rollback();
                } catch (SQLException ex) {
                    log.error("Error al hacer rollback. " + ex, ex);
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar SQL: [" + sql + "]. " + ex, ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (psMode != null) {
                    psMode.close();
                }
            } catch (Exception ex) {
            }
            dataBaseConnection.closeDWConnection(conn);
        }

        return null;
    }

    public Boolean saveOrUpdateBaseResumen(StatsMessage sm) {
        String sql = "INSERT INTO dw_schema.BASE_RESUMEN (ID, F_TYPE, LAST_UPDATE_MILLIS, SUBSCRIPTION_ID, MESSAGE) VALUES (?,?,?,?,?) "
                + "ON DUPLICATE KEY UPDATE "
                + "SUBSCRIPTION_ID = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(SUBSCRIPTION_ID),SUBSCRIPTION_ID),"
                + "MESSAGE = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(MESSAGE),MESSAGE),"
                + "LAST_UPDATE_MILLIS = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(LAST_UPDATE_MILLIS),LAST_UPDATE_MILLIS) ";

        Connection conn = dataBaseConnection.getDBDWConnection();
        PreparedStatement ps = null;
        PreparedStatement psMode = null;
        try {
            BaseResumenElastic bre = (BaseResumenElastic) sm.elasticData;
            conn.setAutoCommit(false);
            psMode = conn.prepareStatement(SQL_MODE);
            ps = conn.prepareStatement(sql);
            ps.setLong(1, Long.parseLong(bre.subscriptionBillingElastic == null ? bre.subscriptionRegistryElastic.id : bre.subscriptionBillingElastic.id));
            ps.setString(2, bre.functionType.name());
            ps.setLong(3, sm.lastUpdateMillis);
            ps.setLong(4, bre.subscriptionId);
            ps.setObject(5, mapper.writeValueAsString(sm));
            psMode.executeUpdate();
            ps.executeUpdate();
            conn.commit();
            return true;
        } catch (SQLException e) {
            if (conn != null) {
                try {
                    log.error("SQLException. Transaction is being rolled back. " + e, e);
                    conn.rollback();
                } catch (SQLException ex) {
                    log.error("Error al hacer rollback. " + ex, ex);
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar SQL: [" + sql + "]. " + ex, ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (psMode != null) {
                    psMode.close();
                }
            } catch (Exception ex) {
            }
            dataBaseConnection.closeDWConnection(conn);
        }

        return null;
    }

    public Boolean saveOrUpdatePGJContent(StatsMessage sm) {
        String sql = "INSERT INTO dw_schema.PGJ_CONTENT (ID, F_TYPE, LAST_UPDATE_MILLIS, MESSAGE) VALUES (?,?,?,?) "
                + "ON DUPLICATE KEY UPDATE "
                + "MESSAGE = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(MESSAGE),MESSAGE),"
                + "LAST_UPDATE_MILLIS = IF(LAST_UPDATE_MILLIS<VALUES(LAST_UPDATE_MILLIS),VALUES(LAST_UPDATE_MILLIS),LAST_UPDATE_MILLIS) ";

        Connection conn = dataBaseConnection.getDBDWConnection();
        PreparedStatement ps = null;
        PreparedStatement psMode = null;
        try {
            PGJContentElastic pce = (PGJContentElastic) sm.elasticData;
            conn.setAutoCommit(false);
            psMode = conn.prepareStatement(SQL_MODE);
            ps = conn.prepareStatement(sql);
            ps.setLong(1, Long.parseLong(pce.id));
            ps.setString(2, pce.functionType.name());
            ps.setLong(3, sm.lastUpdateMillis);
            ps.setObject(4, mapper.writeValueAsString(sm));
            psMode.executeUpdate();
            ps.executeUpdate();
            conn.commit();
            return true;
        } catch (SQLException e) {
            if (conn != null) {
                try {
                    log.error("SQLException. Transaction is being rolled back. " + e, e);
                    conn.rollback();
                } catch (SQLException ex) {
                    log.error("Error al hacer rollback. " + ex, ex);
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar SQL: [" + sql + "]. " + ex, ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (psMode != null) {
                    psMode.close();
                }
            } catch (Exception ex) {
            }
            dataBaseConnection.closeDWConnection(conn);
        }

        return null;
    }

    public Boolean saveOauthUserSubscription(StatsMessage sm) {
        String sql = "INSERT INTO dw_schema.OAUTH_USER_SUBSCRIPTION (ID, USER_ID, USER_ACCOUNT, SUBSCRIPTION_ID, SUBSCRIPTION_REGISTRY_ID) VALUES (?,?,?,?,?) "
                + "ON DUPLICATE KEY UPDATE "
                + "USER_ACCOUNT = USER_ACCOUNT ";

        Connection conn = dataBaseConnection.getDBDWConnection();
        PreparedStatement ps = null;
        PreparedStatement psMode = null;
        try {
            OauthUserSubscriptionElastic bre = (OauthUserSubscriptionElastic) sm.elasticData;
            conn.setAutoCommit(false);
            psMode = conn.prepareStatement(SQL_MODE);
            ps = conn.prepareStatement(sql);
            ps.setString(1, bre.id);
            ps.setLong(2, bre.userId);
            ps.setString(3, bre.userAccount);
            if (bre.subscriptionId == null) {
                ps.setNull(4, java.sql.Types.BIGINT);
            } else {
                ps.setLong(4, bre.subscriptionId);
            }
            if (bre.SubscriptionRegistryId == null) {
                ps.setNull(5, java.sql.Types.BIGINT);
            } else {
                ps.setLong(5, bre.SubscriptionRegistryId);
            }
            psMode.executeUpdate();
            ps.executeUpdate();
            conn.commit();
            return true;
        } catch (SQLException e) {
            if (conn != null) {
                try {
                    log.error("SQLException. Transaction is being rolled back. " + e, e);
                    conn.rollback();
                } catch (SQLException ex) {
                    log.error("Error al hacer rollback. " + ex, ex);
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar SQL: [" + sql + "]. " + ex, ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (psMode != null) {
                    psMode.close();
                }
            } catch (Exception ex) {
            }
            dataBaseConnection.closeDWConnection(conn);
        }

        return null;
    }
}
