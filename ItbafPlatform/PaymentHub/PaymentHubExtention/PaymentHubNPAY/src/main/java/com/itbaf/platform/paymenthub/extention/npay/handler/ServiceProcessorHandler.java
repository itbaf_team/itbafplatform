/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.npay.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.bn.model.NPAYNotification;
import com.itbaf.platform.paymenthub.extention.npay.model.ServiceInformationResponse;
import com.itbaf.platform.paymenthub.extention.npay.model.SubscriptionData;
import com.itbaf.platform.paymenthub.extention.npay.model.ValidateSubscription;
import com.itbaf.platform.paymenthub.extention.npay.resources.NPAYResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.billing.Tariff.TariffType;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import com.itbaf.platform.paymenthub.services.repository.PHInstanceService;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.paymenthub.services.repository.TariffService;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import org.apache.commons.lang3.Validate;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final List<Provider> providers;
    private final Random rand = new Random();
    private final NPAYResources npayResources;
    private final PHProfilePropertiesService profilePropertiesService;

    @Inject
    public ServiceProcessorHandler(final Set<Provider> providers,
            final NPAYResources npayResources,
            final PHProfilePropertiesService profilePropertiesService) {
        this.providers = new ArrayList();
        this.providers.addAll(Validate.notNull(providers, "A Set<Provider> class must be provided"));
        this.npayResources = Validate.notNull(npayResources, "A NPAYResources class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    public void processNotification(final NPAYNotification notification) {

        Provider p;
        final SOP sop = sopService.findBySettings("npay_id_service", notification.getId_service());
        if (sop == null) {
            log.error("No existe un SOP configurado para el id_service: [" + notification.getId_service() + "] - Notificacion: [" + notification + "]");
            p = providers.get(rand.nextInt(providers.size()));
        } else {
            p = sop.getProvider();
        }

        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notification);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notification);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
            }
        }

    }

    @Override
    public Boolean processMessage(String message) {

        NPAYNotification notification;
        try {
            notification = mapper.readValue(message, NPAYNotification.class);
            Thread.currentThread().setName(notification.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }

        final SOP sop = sopService.findBySettings("npay_id_service", notification.getId_service());

        if (sop == null) {
            log.error("No existe un SOP configurado para el id_service: [" + notification.getId_service() + "] - Notificacion: [" + notification + "]");
            return null;
        }
        final String threadName = Thread.currentThread().getName() + "-" + sop.getProvider().getName() + ".n";
        Thread.currentThread().setName(threadName);

        String subscriptionId = notification.getId_subscription();
        PaymentHubMessage msg = null;
        switch (notification.getIpn_type()) {
            case "subscription.new":
                // log.info("Notificacion de alta. subscriptionId: [" + subscriptionId + "] - id_customer: [" + notification.getId_customer() + "]");
                msg = processSubscriptionNotification(notification, sop, true);
                msg = processBillingNotification(notification, msg, sop);
                break;
            case "charge.failed":
                // log.info("Notificacion de alta sin cobro. subscriptionId: [" + subscriptionId + "] - id_customer: [" + notification.getId_customer() + "]");
                msg = processSubscriptionNotification(notification, sop, true, MessageType.SUBSCRIPTION_PENDING);
                break;
            case "subscription.renew":
                // log.info("Notificacion de renovacion. subscriptionId: [" + subscriptionId + "] - id_customer: [" + notification.getId_customer() + "]");
                msg = processSubscriptionNotification(notification, sop, false);
                msg = processBillingNotification(notification, msg, sop);
                break;
            case "subscription.failed":
                // log.info("Notificacion de renovacion pendiente. subscriptionId: [" + subscriptionId + "] - id_customer: [" + notification.getId_customer() + "]. Ignorada");
                return true;
            case "subscription.cancel":
                // log.info("Notificacion de baja. subscriptionId: [" + subscriptionId + "] - id_customer: [" + notification.getId_customer() + "]");
                return processUnsubscriptionNotification(notification, sop, 0);
            case "inapp.success":
            case "checkout.success":
                //  log.info("Notificacion Ondemand. subscriptionId: [" + subscriptionId + "] - id_customer: [" + notification.getId_customer() + "]. Ignorada por el momento");
                return true;
            default:
                String aux = "Notificacion desconocida. subscriptionId: [" + subscriptionId + "] - id_customer: [" + notification.getId_customer() + "]";
                log.error(aux);
                throw new IllegalArgumentException(aux);
        }
        return msg == null ? null : true;
    }

    private PaymentHubMessage processSubscriptionNotification(NPAYNotification notification, SOP sop, boolean isNew) {
        return processSubscriptionNotification(notification, sop, isNew, MessageType.SUBSCRIPTION_ACTIVE);
    }

    private PaymentHubMessage processSubscriptionNotification(NPAYNotification notification, SOP sop, boolean isNew, MessageType messageType) {
        return processSubscriptionNotification(notification, sop, isNew, messageType, 0);
    }

    private PaymentHubMessage processSubscriptionNotification(NPAYNotification notification, SOP sop, boolean isNew, MessageType messageType, int qu) {

        PaymentHubMessage msg = new PaymentHubMessage();

        try {
            ValidateSubscription vse = npayResources.validateSubscriptionByExternalCustomer(notification.getId_customer(), sop);
            if (vse != null && "200".equals(vse.getMeta().code) && "OK".equals(vse.getMeta().status)) {
                for (SubscriptionData sd : vse.getData().data) {
                    for (SubscriptionData s : sd.subscriptions) {
                        if (notification.getId_subscription().equals(s.id_subscription)) {
                            msg.setExternalUserAccount(sd.id_account);
                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Error al obtener el id_account para la notificacion: [" + notification + "]. " + ex);
        }

        if (msg.getExternalUserAccount() == null) {
            log.warn("No fue posible obtener el id_account para la notificacion: [" + notification + "]");
            if (qu <= 5) {
                qu++;
                try {
                    Thread.sleep(5000);
                } catch (Exception ex) {
                }
                return processSubscriptionNotification(notification, sop, isNew, messageType, qu);
            }
            log.error("No fue posible obtener el id_account para la notificacion: [" + notification + "]");
            return null;
        } else {
            npayResources.sendAcknowledge(notification, sop);
        }

        Provider provider = sop.getProvider();
        ServiceInformationResponse sir = npayResources.subscriptionServiceInformation(sop);
        BigDecimal fullAmount = null;
        BigDecimal netAmount;
        if (sir == null) {
            // fullAmount = new BigDecimal(notification.getAmount());
            netAmount = new BigDecimal(notification.getAmount());
            if ("ARS".equals(notification.getCurrency())) {
                netAmount = netAmount.divide(new BigDecimal(10000));
            }
        } else {
            if (sir.getData().hub_details == null) {
                fullAmount = new BigDecimal(sir.getData().smt_details.amount);
                netAmount = new BigDecimal(sir.getData().smt_details.subtotal);
            } else {
                fullAmount = new BigDecimal(sir.getData().hub_details.amount);
                netAmount = new BigDecimal(sir.getData().hub_details.subtotal);
            }
        }
        if (fullAmount != null) {
            fullAmount = fullAmount.divide(new BigDecimal(profilePropertiesService.getCommonProperty(provider.getId(), "npay.amount.divisor")));
        }
        netAmount = netAmount.divide(new BigDecimal(profilePropertiesService.getCommonProperty(provider.getId(), "npay.amount.divisor")));

        Tariff t = sop.getMainTariff();
        if (t != null && netAmount.compareTo(t.getNetAmount()) > 0) {
            t = updateMainTariff(sop, fullAmount, netAmount);
        }

        if (t == null) {
            log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
            return null;
        }

        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(notification.getCurrency());
            if (sir != null && sir.getData().hub_details == null) {
                msg.setFrecuency(sir.getData().smt_details.interval_count != null ? Integer.parseInt(sir.getData().smt_details.interval_count) : null);
                msg.setFrequencyType(sir.getData().smt_details.interval != null ? sir.getData().smt_details.interval : null);
            } else {
                msg.setFrecuency(sir != null && sir.getData().hub_details.interval_count != null ? Integer.parseInt(sir.getData().hub_details.interval_count) : null);
                msg.setFrequencyType(sir != null && sir.getData().hub_details.interval != null ? sir.getData().hub_details.interval : null);
            }
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(Long.parseLong(notification.getCreated() + "000"));
            msg.setFromDate(c.getTime());
            msg.setFullAmount(fullAmount);
            msg.setMessageType(messageType);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalCustomer(notification.getId_customer());
            msg.setNetAmount(netAmount);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getId_subscription());
            msg.setTransactionId(notification.getId_event());
            msg.setChannelIn(notification.getChannel());

            if (notification.getTracking_id() != null && notification.getTracking_id().length() > 0) {
                String aux = URLDecoder.decode(notification.getTracking_id(), "UTF-8");
                aux = aux.replace("campaign", "npayCampaign");
                processParams(msg, aux);
            }
            if (notification.getMeta() != null && notification.getMeta().length() > 0) {
                String aux = URLDecoder.decode(notification.getMeta(), "UTF-8");
                aux = aux.replace("itcampaign", "campaign");
                processParams(msg, aux);
            }

            if (isNew) {
                String jsonResponse = mapper.writeValueAsString(msg);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
                }
            }
            return msg;
        } catch (Exception ex) {
            log.error("Error al procesar NPAYNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    private Boolean processUnsubscriptionNotification(NPAYNotification notification, SOP sop, int qu) {
        PaymentHubMessage msg = new PaymentHubMessage();

        try {
            ValidateSubscription vse = npayResources.validateSubscriptionByExternalCustomer(notification.getId_customer(), sop);
            if (vse != null && "200".equals(vse.getMeta().code) && "OK".equals(vse.getMeta().status)) {
                for (SubscriptionData sd : vse.getData().data) {
                    for (SubscriptionData s : sd.subscriptions) {
                        if (notification.getId_subscription().equals(s.id_subscription)) {
                            msg.setExternalUserAccount(sd.id_account);
                            break;
                        }
                    }
                }
                if (msg.getExternalUserAccount() == null) {
                    msg.setExternalUserAccount(notification.getId_subscription());
                }
            }
        } catch (Exception ex) {
            log.error("Error al obtener el id_account para la notificacion: [" + notification + "]. " + ex, ex);
        }

        if (msg.getExternalUserAccount() == null) {
            log.warn("No fue posible obtener el id_account para la notificacion: [" + notification + "]");
            if (qu <= 5) {
                qu++;
                try {
                    Thread.sleep(5000);
                } catch (Exception ex) {
                }
                return processUnsubscriptionNotification(notification, sop, qu);
            }
            log.error("No fue posible obtener el id_account para la notificacion: [" + notification + "]");
            return null;
        } else {
            npayResources.sendAcknowledge(notification, sop);
        }

        Provider provider = sop.getProvider();
        ServiceInformationResponse sir = npayResources.subscriptionServiceInformation(sop);
        BigDecimal fullAmount = null;
        BigDecimal netAmount;
        if (sir == null) {
            // fullAmount = new BigDecimal(BigInteger.ZERO);
            netAmount = new BigDecimal(BigInteger.ZERO);
        } else {
            if (sir.getData().hub_details == null) {
                fullAmount = new BigDecimal(sir.getData().smt_details.amount);
                netAmount = new BigDecimal(sir.getData().smt_details.subtotal);
            } else {
                fullAmount = new BigDecimal(sir.getData().hub_details.amount);
                netAmount = new BigDecimal(sir.getData().hub_details.subtotal);
            }
        }
        if (fullAmount != null) {
            fullAmount = fullAmount.divide(new BigDecimal(profilePropertiesService.getCommonProperty(provider.getId(), "npay.amount.divisor")));
        }
        netAmount = netAmount.divide(new BigDecimal(profilePropertiesService.getCommonProperty(provider.getId(), "npay.amount.divisor")));

        Tariff t = sop.getMainTariff();
        if (t != null && netAmount.compareTo(t.getNetAmount()) > 0) {
            t = updateMainTariff(sop, fullAmount, netAmount);
        }

        if (t == null) {
            log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
            return null;
        }

        try {
            ValidateSubscription vs = npayResources.validateSubscriptionByExternalId(notification.getId_subscription(), sop);
            if (vs != null && "200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status) && vs.getData().canceled != null) {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(Long.parseLong(vs.getData().canceled + "000"));
                msg.setToDate(c.getTime());
            }
        } catch (Exception ex) {
        }

        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(notification.getCurrency());
            if (sir != null && sir.getData().hub_details == null) {
                msg.setFrecuency(sir.getData().smt_details.interval_count != null ? Integer.parseInt(sir.getData().smt_details.interval_count) : null);
                msg.setFrequencyType(sir.getData().smt_details.interval != null ? sir.getData().smt_details.interval : null);
            } else {
                msg.setFrecuency(sir != null && sir.getData().hub_details.interval_count != null ? Integer.parseInt(sir.getData().hub_details.interval_count) : null);
                msg.setFrequencyType(sir != null && sir.getData().hub_details.interval != null ? sir.getData().hub_details.interval : null);
            }
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(Long.parseLong(notification.getCreated() + "000"));
            msg.setFromDate(c.getTime());
            if (msg.getToDate() == null) {
                msg.setToDate(new Date());
            }
            msg.setFullAmount(t.getFullAmount());
            msg.setNetAmount(t.getNetAmount());
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setExternalCustomer(notification.getId_customer());
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getId_subscription());
            msg.setTransactionId(notification.getId_event());
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setChannelOut(notification.getChannel());

            if (notification.getTracking_id() != null && notification.getTracking_id().length() > 0) {
                String aux = URLDecoder.decode(notification.getTracking_id(), "UTF-8");
                aux = aux.replace("campaign", "npayCampaign");
                processParams(msg, aux);
            }
            if (notification.getMeta() != null && notification.getMeta().length() > 0) {
                String aux = URLDecoder.decode(notification.getMeta(), "UTF-8");
                aux = aux.replace("itcampaign", "campaign");
                processParams(msg, aux);
            }

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar NPAYNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private void processParams(PaymentHubMessage msg, String params) {
        try {
            String[] aux = params.split(",");
            for (String s : aux) {
                try {
                    String[] x = s.split(":");
                    if ("json".equals(x[0])) {
                        String json = new String(Base64.decodeBase64(x[1]));
                        JsonNode jn = mapper.readTree(json);

                        Iterator<String> names = jn.fieldNames();
                        while (names.hasNext()) {
                            String name = names.next();
                            JsonNode j = jn.findValue(name);
                            msg.getTrackingParams().put(name, j);
                        }
                    } else if ("adnTrackingId".equals(x[0])) {
                        msg.getTrackingParams().put(x[0], new String[]{x[1]});
                    } else {
                        msg.getTrackingParams().put(x[0], x[1]);
                    }
                } catch (Exception ex) {
                }
            }
        } catch (Exception ex) {
            log.error("Error al parsear parametros [" + params + "]. " + ex, ex);
        }
    }

    private PaymentHubMessage processBillingNotification(NPAYNotification notification, PaymentHubMessage msg, SOP sop) {
        if (msg == null) {
            return msg;
        }
        // log.info("Procesando un cobro.");
        Provider provider = sop.getProvider();
        try {
            BigDecimal amount = new BigDecimal(notification.getAmount());
            if ("ARS".equals(notification.getCurrency())) {
                amount = amount.divide(new BigDecimal(10000));
            }
            amount = amount.divide(new BigDecimal(profilePropertiesService.getCommonProperty(provider.getId(), "npay.amount.divisor")));
            if (amount.compareTo(BigDecimal.ZERO) == 0) {
                //  log.info("No se procesara la notificacion de cobro con valor CERO");
                return msg;
            }
            Tariff t = sop.findTariffByNetAmount(amount);

            if (t == null) {
                t = updateNetAmountChildrenTariff(sop, amount, sop.getIntegrationSettings().get("npay_id_service"));
            }

            if (t == null) {
                log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
                return null;
            }

            if (TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }

            msg.setNetAmount(t.getNetAmount());
            msg.setFullAmount(t.getFullAmount());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setTariffId(t.getId());
            Calendar c = Calendar.getInstance();
            if (notification.getUpdated() != null) {
                c.setTimeInMillis(Long.parseLong(notification.getUpdated() + "000"));
            }
            msg.setChargedDate(c.getTime());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
            }
            return msg;
        } catch (Exception ex) {
            log.error("Error al procesar notificacion de cobro NPAYNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
             subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);
        switch (aux.status) {
            case ACTIVE:
            case PENDING:
            case BLACKLIST:
            case LOCKED:
                rm.status = aux.status;
                rm.message = aux.message;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case REMOVED:
                if ("1".equals(subscription.getSop().getIntegrationSettings().get("version"))) {
                    subscription.setStatus(Subscription.Status.PENDING);
                    rm.status = ResponseMessage.Status.OK;
                } else {
                    subscription.setStatus(Subscription.Status.PENDING);
                    rm.status = ResponseMessage.Status.OK_NS;
                }
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {
        String msisdn = null;

        try {
            msisdn = userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider());
        } catch (Exception ex) {
        }

        ResponseMessage rm = new ResponseMessage();
        //Por MSISDN
        if (msisdn != null && subscription.getUserAccount() != null && subscription.getUserAccount().length() > 2) {
            ValidateSubscription vs = npayResources.validateSubscriptionByMsisdn(subscription.getUserAccount(), subscription.getSop());

            if ("404".equals(vs.getMeta().code) && "Not Found".equals(vs.getMeta().status)) {
                subscription.setStatus(Subscription.Status.REMOVED);
                rm.status = ResponseMessage.Status.REMOVED;
                rm.message = vs.getMeta().description;
            } else if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                rm.message = vs.getMeta().description;

                if (vs.getData().data.size() > 1) {
                    log.fatal("Buscando por MSISDN... Como puede ser posible esto???? Trae todo lo q tiene el Customer? " + (Arrays.toString(vs.getData().data.toArray())));
                }
                if (vs.getData().data == null || vs.getData().data.isEmpty()) {
                    rm.status = ResponseMessage.Status.REMOVED;
                    rm.message = vs.getMeta().description;
                    subscription.setStatus(Subscription.Status.REMOVED);
                    try {
                        rm.data = mapper.writeValueAsString(subscription);
                    } catch (JsonProcessingException ex1) {
                    }
                    return rm;
                }

                SubscriptionData sdx = null;
                for (SubscriptionData sd : vs.getData().data) {
                    String al = sd.alias.substring(sd.alias.length() - 4);

                    if (!subscription.getUserAccount().endsWith(al)) {
                        //  log.info("Alias: [" + sd.alias + "] no coincide con: [" + subscription.getUserAccount() + "]");
                        continue;
                    }
                    subscription.setExternalUserAccount(sd.id_account);
                    sdx = sd;
                    break;
                }
                processSubscriptionsData(subscription, sdx, true, rm);
            } else {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = vs.getMeta().description;
            }
        } else if (subscription.getExternalCustomer() != null && subscription.getExternalCustomer().length() > 2
                && subscription.getExternalUserAccount() != null && subscription.getExternalUserAccount().length() > 2
                && !subscription.getExternalCustomer().equals(subscription.getExternalUserAccount())) {
            ValidateSubscription vs = npayResources.validateSubscriptionByExternalCustomer(subscription.getExternalCustomer(), subscription.getSop());
            if (vs != null) {
                if ("404".equals(vs.getMeta().code) && "Not Found".equals(vs.getMeta().status)) {
                    subscription.setStatus(Subscription.Status.REMOVED);
                    rm.status = ResponseMessage.Status.REMOVED;
                    rm.message = vs.getMeta().description;
                } else if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                    rm.message = vs.getMeta().description;
                    if (vs.getData().data == null || vs.getData().data.isEmpty()) {
                        rm.status = ResponseMessage.Status.REMOVED;
                        rm.message = vs.getMeta().description;
                        subscription.setStatus(Subscription.Status.REMOVED);
                        try {
                            rm.data = mapper.writeValueAsString(subscription);
                        } catch (JsonProcessingException ex1) {
                        }
                        return rm;
                    }
                    SubscriptionData sdx = null;
                    for (SubscriptionData sd : vs.getData().data) {
                        if (!subscription.getExternalUserAccount().equals(sd.id_account)) {
                            // log.info("ExternalUserAccount: [" + subscription.getExternalUserAccount() + "] no coincide con id_account: [" + sd.id_account + "]");
                            continue;
                        }
                        subscription.setExternalUserAccount(sd.id_account);
                        sdx = sd;
                    }
                    processSubscriptionsData(subscription, sdx, false, rm);
                } else {
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = vs.getMeta().description;
                }
            }
        } else if ((subscription.getSubscriptionRegistry() != null
                && subscription.getSubscriptionRegistry().getExternalId() != null
                && subscription.getSubscriptionRegistry().getExternalId().length() > 2)
                || (subscription.getUserAccount() != null && subscription.getUserAccount().length() > 20)) {

            // Esto si el facade utiliza el get status con id_subscription
            String externalId = subscription.getUserAccount();

            if (subscription.getSubscriptionRegistry() != null && subscription.getSubscriptionRegistry().getExternalId() != null
                    && subscription.getSubscriptionRegistry().getExternalId().length() > 2) {
                externalId = subscription.getSubscriptionRegistry().getExternalId();
            }
            ValidateSubscription vs = npayResources.validateSubscriptionByExternalId(externalId, subscription.getSop());
            if ("404".equals(vs.getMeta().code) && "Not Found".equals(vs.getMeta().status)) {
                try {
                    //Esto si el facade utiliza el get status con ExternalCustomer
                    vs = npayResources.validateSubscriptionByExternalCustomer(externalId, subscription.getSop());
                    if (vs != null) {
                        if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                            if (vs.getData().data == null || vs.getData().data.isEmpty()) {
                                rm.status = ResponseMessage.Status.REMOVED;
                                rm.message = vs.getMeta().description;
                                subscription.setStatus(Subscription.Status.REMOVED);
                                try {
                                    rm.data = mapper.writeValueAsString(subscription);
                                } catch (JsonProcessingException ex1) {
                                }
                                return rm;
                            }

                            for (SubscriptionData sd : vs.getData().data) {
                                for (SubscriptionData s : sd.subscriptions) {
                                    if (subscription.getSop().getIntegrationSettings().get("npay_id_service").equals(s.id_service)) {
                                        vs.getData().id_account = sd.id_account;
                                        break;
                                    }
                                }
                            }
                            SubscriptionData sdx = null;
                            for (SubscriptionData sd : vs.getData().data) {
                                if (!vs.getData().id_account.equals(sd.id_account)) {
                                    // log.info("ExternalUserAccount: [" + vs.getData().id_account + "] no coincide con id_account: [" + sd.id_account + "]");
                                    continue;
                                }
                                subscription.setExternalUserAccount(sd.id_account);
                                sdx = sd;
                            }
                            processSubscriptionsData(subscription, sdx, false, rm);
                            if (subscription.getSubscriptionRegistry() != null) {
                                subscription.getSubscriptionRegistry().setId(null);
                            }
                        } else {
                            rm.status = ResponseMessage.Status.ERROR;
                            subscription.setStatus(Subscription.Status.REMOVED);
                            rm.message = vs.getMeta().description;
                        }
                    }
                } catch (Exception ex) {
                    log.error("Error al obtener el estado de la suscripcion por ExternalCustomer [" + vs.getData().id_customer + "]. " + ex, ex);
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "Error al obtener el estado de la suscripcion por ExternalCustomer";
                }
            } else if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                rm.message = vs.getMeta().description;
                try {
                    ValidateSubscription vse = npayResources.validateSubscriptionByExternalCustomer(vs.getData().id_customer, subscription.getSop());
                    if (vse != null && "200".equals(vse.getMeta().code) && "OK".equals(vse.getMeta().status)) {
                        if (vse.getData().data == null || vse.getData().data.isEmpty()) {
                            rm.status = ResponseMessage.Status.REMOVED;
                            rm.message = vse.getMeta().description;
                            subscription.setStatus(Subscription.Status.REMOVED);
                            try {
                                rm.data = mapper.writeValueAsString(subscription);
                            } catch (JsonProcessingException ex1) {
                            }
                            return rm;
                        }

                        for (SubscriptionData sd : vse.getData().data) {
                            for (SubscriptionData s : sd.subscriptions) {
                                if (externalId.equals(s.id_subscription)) {
                                    vs.getData().id_account = sd.id_account;
                                    break;
                                }
                            }
                        }

                        if (vs.getData().id_account == null) {
                            String info = "subscription_id no encontrado en NPAY. Se asume CANCELADO.";
                            // log.info(info);
                            rm.status = ResponseMessage.Status.REMOVED;
                            rm.message = info;
                        } else {
                            SubscriptionData sdx = null;
                            for (SubscriptionData sd : vse.getData().data) {
                                if (!vs.getData().id_account.equals(sd.id_account)) {
                                    //  log.info("ExternalUserAccount: [" + vs.getData().id_account + "] no coincide con id_account: [" + sd.id_account + "]");
                                    continue;
                                }
                                subscription.setExternalUserAccount(sd.id_account);
                                sdx = sd;
                            }

                            processSubscriptionsData(subscription, sdx, false, rm);
                            if (subscription.getSubscriptionRegistry() != null) {
                                subscription.getSubscriptionRegistry().setId(null);
                            }
                        }
                    } else {
                        log.error("Error al obtener el estado de la suscripcion por ExternalCustomer [" + vs.getData().id_customer + "]");
                        rm.status = ResponseMessage.Status.ERROR;
                        rm.message = "Error al obtener el estado de la suscripcion por ExternalCustomer";
                    }
                } catch (Exception ex) {
                    log.error("Error al obtener el estado de la suscripcion por ExternalCustomer [" + vs.getData().id_customer + "]. " + ex, ex);
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "Error al obtener el estado de la suscripcion por ExternalCustomer";
                }
            } else {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = vs.getMeta().description;
            }
        } else {
            rm.message = "No es posible evaluar el estado de la suscripcion.";
            log.fatal(rm.message + " [" + subscription + "]");
        }

        try {
            if (subscription.getSubscriptionRegistry() != null && subscription.getExternalUserAccount() != null) {
                try {
                    msisdn = null;
                    try {
                        msisdn = userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider());
                    } catch (Exception ex) {
                    }
                } catch (Exception ex) {
                }
                try {
                    if (msisdn == null) {
                        Subscription aux = subscriptionManager.getSubscriptionByExternalUserAccount(subscription.getExternalUserAccount(), subscription.getSop());
                        if (aux != null && aux.getUserAccount() != null) {
                            msisdn = userAccountNormalizer(aux.getUserAccount(), aux.getSop().getProvider());
                            subscription.setUserAccount(aux.getUserAccount());
                            subscription.getSubscriptionRegistry().setUserAccount(aux.getUserAccount());
                        }
                    }
                } catch (Exception ex) {
                }
            }

            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        return rm;
    }

    private void processSubscriptionsData(Subscription subscription, SubscriptionData sdx,
            boolean isMsisdn, ResponseMessage rm) throws UnsupportedEncodingException {
        SubscriptionData subs = null;
        for (SubscriptionData s : sdx.subscriptions) {
            if (ValidateSubscription.ValidateSubscriptionStatus.active.equals(s.status)
                    || ValidateSubscription.ValidateSubscriptionStatus.successful_renewal.equals(s.status)
                    || ValidateSubscription.ValidateSubscriptionStatus.pending_renewal.equals(s.status)) {
                subs = s;
                subs.id_account = sdx.id_account;
                rm.message = s.status_desc;
                break;
            }
        }
        if (subs != null) {
            rm.status = ResponseMessage.Status.ACTIVE;
            subscription.setStatus(Subscription.Status.ACTIVE);
            processSubscriptionData(subs, subscription, isMsisdn);
        } else {
            for (SubscriptionData s : sdx.subscriptions) {
                if (ValidateSubscription.ValidateSubscriptionStatus.pending.equals(s.status)
                        || ValidateSubscription.ValidateSubscriptionStatus.pending_renewal.equals(s.status)) {
                    subs = s;
                    subs.id_account = sdx.id_account;
                    rm.message = s.status_desc;
                    break;
                }
            }
            rm.status = ResponseMessage.Status.REMOVED;
            if (subs != null) {
                rm.status = ResponseMessage.Status.PENDING;
                subscription.setStatus(Subscription.Status.PENDING);
                processSubscriptionData(subs, subscription, isMsisdn);
            } else {
                for (SubscriptionData s : sdx.subscriptions) {
                    if (ValidateSubscription.ValidateSubscriptionStatus.locked_P001.equals(s.status)
                            || ValidateSubscription.ValidateSubscriptionStatus.locked_P002.equals(s.status)
                            || ValidateSubscription.ValidateSubscriptionStatus.locked_P003.equals(s.status)) {

                        Date d = npayResources.locateDate(s.created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                        Date date = new Date();
                        long ms = 20 * 60 * 60 * 1000;//h,m,s

                        if (ValidateSubscription.ValidateSubscriptionStatus.locked_P003.equals(s.status)) {
                            ms = 10 * 60 * 60 * 1000;//h,m,s
                        }

                        if ((date.getTime() - d.getTime()) < ms) {
                            subs = s;
                            subs.id_account = sdx.id_account;
                            rm.message = s.status.name();
                            break;
                        }
                    }
                }
                if (subs != null) {
                    rm.status = ResponseMessage.Status.LOCKED;
                    subscription.setStatus(Subscription.Status.LOCKED);
                    processSubscriptionData(subs, subscription, isMsisdn);
                } else {
                    for (SubscriptionData s : sdx.subscriptions) {
                        if (ValidateSubscription.ValidateSubscriptionStatus.blacklist.equals(s.status)) {
                            subs = s;
                            subs.id_account = sdx.id_account;
                            break;
                        }
                    }
                    if (subs != null) {
                        rm.status = ResponseMessage.Status.BLACKLIST;
                        subscription.setStatus(Subscription.Status.BLACKLIST);
                        processSubscriptionData(subs, subscription, isMsisdn);
                    } else {
                        rm.status = ResponseMessage.Status.REMOVED;
                    }
                }
            }
        }
    }

    private void processSubscriptionData(SubscriptionData subs, Subscription subscription, boolean isMsisdn) throws UnsupportedEncodingException {

        Date d = npayResources.locateDate(subs.created + "000", TimeZone.getTimeZone("America/Mexico_City"));
        subscription.setSubscriptionDate(d);
        if (subscription.getSubscriptionRegistry() == null && isMsisdn) {
            subscription.setSubscriptionRegistry(new SubscriptionRegistry());
        } else if (subscription.getSubscriptionRegistry() == null
                || (!subs.id_subscription.equals(subscription.getSubscriptionRegistry().getExternalId()) && subscription.getSubscriptionRegistry().getExternalId() != null)) {
            subscription.setSubscriptionRegistry(new SubscriptionRegistry());
        }
        subscription.getSubscriptionRegistry().setSubscriptionDate(d);
        subscription.setExternalUserAccount(subs.id_account);
        subscription.setExternalCustomer(subs.id_customer);
        subscription.getSubscriptionRegistry().setExternalId(subs.id_subscription);

        if (!isMsisdn) {
            //No tiene ANI asi que vamos a setear el id_account
            subscription.setUserAccount(subs.id_account);
            subscription.getSubscriptionRegistry().setUserAccount(subs.id_account);
        }

        if (subscription.getSubscriptionRegistry().getAdnetworkTracking() == null) {
            PaymentHubMessage msg = new PaymentHubMessage();
            if (subs.tracking_id != null && subs.tracking_id.length() > 0) {
                String aux = URLDecoder.decode(subs.tracking_id, "UTF-8");
                aux = aux.replace("campaign", "npayCampaign");
                processParams(msg, aux);
            }
            if (subs.metadata != null && subs.metadata.length() > 0) {
                String aux = URLDecoder.decode(subs.metadata, "UTF-8");
                aux = aux.replace("itcampaign", "campaign");
                processParams(msg, aux);
            }
            if (!msg.getTrackingParams().isEmpty()) {
                subscription.getSubscriptionRegistry().setAdnetworkTracking(msg.getTrackingParams());
            }
        }
    }

    public ResponseMessage sendPin(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(this.userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);
        switch (aux.status) {
            case ACTIVE:
            case PENDING:
            case BLACKLIST:
            case LOCKED:
                rm.status = aux.status;
                rm.message = aux.message;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case REMOVED:
                ResponseMessage.Status result = npayResources.sendSubscriptionPin(
                        profilePropertiesService.getCommonProperty(subscription.getSop().getProvider().getId(), "npay.carrier"),
                        subscription);
                if (result != null) {
                    rm.status = result;
                }
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;

        try {
            this.userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        if (subscription.getSubscriptionRegistry().getExternalId() != null) {
            ResponseMessage.Status result = npayResources.removeSubscription(subscription);
            if (result != null) {
                rm.status = result;
            }
        }

        rm.data = mapper.writeValueAsString(subscription);

        return rm;
    }

    public ResponseMessage validatePin(Subscription subscription, String pin) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(this.userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);

        switch (aux.status) {
            case ACTIVE:
            case PENDING:
            case BLACKLIST:
            case LOCKED:
                rm.status = aux.status;
                rm.message = aux.message;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case REMOVED:
                ResponseMessage result = npayResources.validatePin(
                        profilePropertiesService.getCommonProperty(subscription.getSop().getProvider().getId(), "npay.carrier"),
                        subscription, pin);
                if (result != null) {
                    rm.status = result.status;
                    rm.message = result.message;
                }
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);

        return rm;
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        if (!isNumber(subscription.getUserAccount()) && subscription.getUserAccount().length() > 20) {
            // log.info("No se valida si el UserAccount: [" + subscription.getUserAccount() + "] es un numero");
        } else {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        }
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

}
