/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.billing.BillerAttempt;
import java.util.List;

/**
 *
 * @author javier
 */
public interface BillerAttemptService {

    public BillerAttempt findById(Long id) throws Exception;

    public void saveOrUpdate(BillerAttempt billerAttempt) throws Exception;

    public List<BillerAttempt> getNextAttempts(Provider p);

}
