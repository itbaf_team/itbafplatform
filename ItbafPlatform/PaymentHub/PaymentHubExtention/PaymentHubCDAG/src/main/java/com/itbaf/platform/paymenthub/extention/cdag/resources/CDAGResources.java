package com.itbaf.platform.paymenthub.extention.cdag.resources;

import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.cdag.model.subscription.Channel;
import com.itbaf.platform.paymenthub.extention.cdag.ws.subscription.AuthenticationErrorException;
import com.itbaf.platform.paymenthub.extention.cdag.ws.subscription.InsufficientPermissionsException;
import com.itbaf.platform.paymenthub.extention.cdag.ws.subscription.InvalidParameterException;
import com.itbaf.platform.paymenthub.extention.cdag.ws.subscription.ResultadoAccion;
import com.itbaf.platform.paymenthub.extention.cdag.ws.subscription.SecurityInfo;
import com.itbaf.platform.paymenthub.extention.cdag.ws.subscription.SessionExpiredException;
import com.itbaf.platform.paymenthub.extention.cdag.ws.subscription.StatusResponse;
import com.itbaf.platform.paymenthub.extention.cdag.ws.subscription.SuscProviderService;
import com.itbaf.platform.paymenthub.extention.cdag.ws.subscription.UsuarioSuscripcion;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import java.util.List;
import javax.xml.ws.Holder;

@lombok.extern.log4j.Log4j2
public class CDAGResources {

    private final RequestClient requestClient;
    private final PHProfilePropertiesService phProfilePropertiesService;
    private static final int RETRY_COUNT = 3;

    @Inject
    public CDAGResources(final RequestClient requestClient,
            final PHProfilePropertiesService phProfilePropertiesService) {
        this.requestClient = requestClient;
        this.phProfilePropertiesService = phProfilePropertiesService;
    }

    private SecurityInfo login(String user, String password, String externalUser, SuscProviderService service) throws Exception {
        try {
            StatusResponse response = service.getService().login(user, password, externalUser, null);

            //log.info("Got response with status code: " + response.getStatusCode());
            this.checkStatusResponse(response);
            if (response.getStatusCode() != 0) {
                return null;
            }
            SecurityInfo credentials = new SecurityInfo();

            credentials.setUser(user);
            credentials.setPassword(password);
            credentials.setExternalUser(externalUser);
            credentials.setSessionId(response.getSessionId());

            return credentials;
        } catch (Exception e) {
            log.error("Error login into WS: " + e.getMessage(), e);
            throw e;
        }
    }

    private ResultadoAccion getResultadoAccion(StatusResponse statusResponse, String serverTId) {
        ResultadoAccion result = new ResultadoAccion();
        result.setStatus(statusResponse);
        result.setServerTid(serverTId);
        return result;
    }

    private void checkStatusResponse(StatusResponse response) {
        if (response.getStatusCode() != 0) {
            int errorCode = response.getErrorCode();
            switch (errorCode) {
                case 100:
                    throw new AuthenticationErrorException(response);
                case 101:
                    throw new InsufficientPermissionsException(response);
                case 102:
                    throw new SessionExpiredException(response);
                case 104:
                    throw new InvalidParameterException(response);
                default:
                    throw new RuntimeException(String.format("Error invoking service. Response: %s", response));
            }
        }
    }

    private ResultadoAccion createSubscription(SOP sop, String msisdn, String clientTransactionId, Channel channel, String keyword) throws Exception {
        String user = sop.getIntegrationSettings().get("subscription.service.user");
        String password = sop.getIntegrationSettings().get("subscription.service.password");
        String externalUser = sop.getIntegrationSettings().get("subscription.service.externalUser");
        Integer serviceId = Integer.parseInt(sop.getIntegrationSettings().get("cdag_serviceId"));

        int retriesLeft = RETRY_COUNT;
        String serverTId = null;
        StatusResponse status = new StatusResponse();
        SuscProviderService service = new SuscProviderService(CDAGResources.class.getResource("/wsdl/" + sop.getProvider().getCountry().getCode().toLowerCase() + "/subscription/SuscProviderService.wsdl"));
        SecurityInfo credentials = this.login(user, password, externalUser, service);

        while (retriesLeft > 0) {
            try {
                Holder<String> serverTIdHolder = new Holder<>();
                Holder<StatusResponse> statusHolder = new Holder<>(status);
                service.getService().altaSuscripcion(serviceId, msisdn, clientTransactionId, channel.getValue(), keyword, credentials, null, statusHolder, serverTIdHolder);
                status = statusHolder.value;
                serverTId = serverTIdHolder.value;
                this.checkStatusResponse(status);

                return this.getResultadoAccion(status, serverTId);
            } catch (SessionExpiredException e) {
                try {
                    credentials = login(user, password, externalUser, service);
                } catch (Exception le) {
                    retriesLeft = 0;
                }
            } catch (Exception e) {
                retriesLeft = 0;
            }
        }

        return this.getResultadoAccion(status, serverTId);
    }

    public void unsubscribe(String msisdn, SOP sop) {

        String clientTransactionId = System.currentTimeMillis() + "." + msisdn;

        ResultadoAccion ra;
        try {
            ra = unsubscribe(msisdn, sop, clientTransactionId, "", "");
            log.info("unsubscribe. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. CDAG Response: [" + ra + "]");
        } catch (Exception ex) {
            log.error("Error unsubscribe. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. " + ex, ex);
        }

    }

    private ResultadoAccion unsubscribe(String msisdn, SOP sop, String clientTransactionId, String motive, String categories) throws Exception {
        String user = sop.getIntegrationSettings().get("subscription.service.user");
        String password = sop.getIntegrationSettings().get("subscription.service.password");
        String externalUser = sop.getIntegrationSettings().get("subscription.service.externalUser");
        Integer serviceId = Integer.parseInt(sop.getIntegrationSettings().get("cdag_serviceId"));
        Integer providerId = Integer.parseInt(sop.getIntegrationSettings().get("providerId"));
        String shortcode = sop.getIntegrationSettings().get("shortcode");
        String keyword = sop.getIntegrationSettings().get("keywordBaja");

        int retriesLeft = RETRY_COUNT;
        String serverTId = null;
        StatusResponse status = new StatusResponse();

        SuscProviderService service = new SuscProviderService(CDAGResources.class.getResource("/wsdl/" + sop.getProvider().getCountry().getCode().toLowerCase() + "/subscription/SuscProviderService.wsdl"));
        SecurityInfo credentials = this.login(user, password, externalUser, service);

        while (retriesLeft > 0) {
            try {
                Holder<String> serverTIdHolder = new Holder<>();
                Holder<StatusResponse> statusHolder = new Holder<>(status);

                service.getService().bajaSuscripcion(msisdn, clientTransactionId, serviceId, providerId, shortcode, keyword, motive, categories, credentials, null, statusHolder, serverTIdHolder);
                status = statusHolder.value;
                serverTId = serverTIdHolder.value;

                this.checkStatusResponse(status);

                return this.getResultadoAccion(status, serverTId);
            } catch (SessionExpiredException e) {
                try {
                    credentials = login(user, password, externalUser, service);
                } catch (Exception le) {
                    retriesLeft = 0;
                }
            } catch (Exception e) {
                retriesLeft = 0;
            }
        }

        return this.getResultadoAccion(status, serverTId);
    }

    public boolean subscriptionStatus(String msisdn, SOP sop) throws Exception {
        String user = sop.getIntegrationSettings().get("subscription.service.user");
        String password = sop.getIntegrationSettings().get("subscription.service.password");
        String externalUser = sop.getIntegrationSettings().get("subscription.service.externalUser");
        Integer serviceId = Integer.parseInt(sop.getIntegrationSettings().get("cdag_serviceId"));
        Integer providerId = Integer.parseInt(sop.getIntegrationSettings().get("providerId"));
        String shortcode = sop.getIntegrationSettings().get("shortcode");

        SuscProviderService service = new SuscProviderService(CDAGResources.class.getResource("/wsdl/" + sop.getProvider().getCountry().getCode().toLowerCase() + "/subscription/SuscProviderService.wsdl"));
        SecurityInfo credentials = this.login(user, password, externalUser, service);

        Holder<StatusResponse> status = new Holder();
        Holder<List<UsuarioSuscripcion>> result = new Holder();

        service.getService().consultaSuscripcionesUsuario(Long.parseLong(msisdn),
                providerId, shortcode, "", Boolean.FALSE, credentials, null, status, result);

        log.info("subscriptionStatus. CDAG Response: [" + status.value.getStatusCode() + "] - [" + status.value.getErrorCode() + "] - [" + status.value.getErrorDescription() + "]");
        boolean isActive = false;
        if (result.value != null) {
            for (UsuarioSuscripcion us : result.value) {
                if (us.getSuscripcion().getSuscripcionId() == serviceId) {
                    isActive = true;
                    break;
                }
            }
        }
        return isActive;
    }

    /**
     * Canal de alta del usuario a la suscripcion. SAT= 1 WEB= 2 SMS=3 USSD=6
     * IVR=7 SSO=11
     */
    public ResponseMessage checkAndBill(SOP sop, String msisdn, String tid, String channel) {
        return checkAndBill(sop, msisdn, tid, channel, 1);
    }

    public ResponseMessage checkAndBill(SOP sop, String msisdn, String tid, String channel, int q) {
        ResponseMessage status = null;
        String qs = "FUN=CHKANDBILL&VOL=3&TIME=3"
                + "&CSP=" + sop.getIntegrationSettings().get("providerId")
                + "&MSISDN=" + msisdn
                + "&USR=" + sop.getIntegrationSettings().get("sbp.user")
                + "&PWD=" + sop.getIntegrationSettings().get("sbp.password")
                + "&CLIENTTID=CDAG." + tid
                + "&KEYWORD=" + sop.getIntegrationSettings().get("keywordAlta")
                + "&SERVICEID=" + sop.getIntegrationSettings().get("cdag_serviceId")
                + "&CANAL=" + channel
                + "&SC=" + sop.getIntegrationSettings().get("keywordAlta")
                + "&SC=" + sop.getIntegrationSettings().get("keywordAlta")
                + "&SC=" + sop.getIntegrationSettings().get("keywordAlta");

        String url = phProfilePropertiesService.getCommonProperty(sop.getProvider().getId(), "cdag.service.url.checkandbill");

        String response = requestClient.requestPost(url, qs);

        log.info("CDAG. checkAndBill. Request: [" + url + "][" + qs + "]. Response: [" + response + "]");

        if (response != null) {
            String code = null;
            String[] aux = response.split(" ");
            for (String s : aux) {
                if (s.contains("Ret_code")) {
                    code = s.split("=")[1];
                    break;
                }
            }
            status = new ResponseMessage();
            status.message = response;
            if (code != null) {
                switch (code) {
                    case "0":
                        status.status = ResponseMessage.Status.OK;
                        break;
                    case "6"://Credito insuficiente
                    case "68"://Billing no permitido    
                        status.status = ResponseMessage.Status.ERROR_BILLING;
                        break;
                    case "49"://Blacklist
                    case "55"://Blacklist
                    case "58"://Usuario prepago no habilitado en la red
                        status.status = ResponseMessage.Status.BLACKLIST;
                        break;
                    case "201"://Cobro parcial
                    case "202"://Recobros    
                        status.status = ResponseMessage.Status.ACTIVE;
                        break;
                    case "1001"://Reintentar (Hasta 3 veces)
                        if (q < 3) {
                            q++;
                            try {
                                Thread.sleep(5000);
                            } catch (Exception ex) {
                            }
                            return checkAndBill(sop, msisdn, tid, channel, q);
                        }
                        break;
                    case "1002"://Error del sistema
                        status.status = ResponseMessage.Status.ERROR;
                        break;
                    case "1104"://Usuario ya suscripto
                        status.status = ResponseMessage.Status.ACTIVE;
                        break;
                    case "1014"://Esperando respuesta optin
                        status.status = ResponseMessage.Status.OK_NS;
                        break;
                }
            }
        }

        return status;
    }
}
