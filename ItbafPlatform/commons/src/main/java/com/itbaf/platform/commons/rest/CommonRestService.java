/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.rest;

import com.google.inject.Inject;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.CacheControl;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public abstract class CommonRestService {

    @Inject
    private InstrumentedObject instrumentedObject;

    protected final CacheControl getCacheControl() {
        CacheControl cc = new CacheControl();
        cc.setNoCache(true);
        cc.setNoStore(true);
        cc.setMaxAge(-1);
        cc.setMustRevalidate(true);

        return cc;
    }

    protected final boolean isHeavyUser(HttpServletRequest request, String key) {
       String ip = request.getHeader("X-Forwarded-For");
            if (ip == null || ip.length() < 1) {
                ip = request.getRemoteAddr();
            }
        key=key+"_"+ip;
       
        return instrumentedObject.getAndIncrementAtomicLong(key, 10) >= 10;
    }
}
