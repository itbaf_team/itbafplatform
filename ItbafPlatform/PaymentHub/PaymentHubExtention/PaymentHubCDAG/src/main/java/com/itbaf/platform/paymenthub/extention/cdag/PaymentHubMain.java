/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.cdag;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.resources.InstrumentedRunnable;
import com.itbaf.platform.paymenthub.commons.PaymentHubBasic;
import com.itbaf.platform.paymenthub.extention.cdag.consumer.RabbitMQMoProcessor;
import com.itbaf.platform.paymenthub.extention.cdag.consumer.RabbitMQPortingProcessor;
import com.itbaf.platform.paymenthub.extention.cdag.consumer.RabbitMQRecobrosProcessor;
import com.itbaf.platform.paymenthub.extention.cdag.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain extends PaymentHubBasic {

    private final PHInstance instance;
    private final RabbitMQMoProcessor moProcessor;
    private final RabbitMQPortingProcessor portingProcessor;
    private final RabbitMQRecobrosProcessor recobrosProcessor;
    private final ServiceProcessorHandler serviceProcessorHandler;
    private final PHProfilePropertiesService phProfilePropertiesService;

    @Inject
    public PaymentHubMain(final PHInstance instance,
            final RabbitMQMoProcessor moProcessor,
            final RabbitMQPortingProcessor portingProcessor,
            final RabbitMQRecobrosProcessor recobrosProcessorImpl,
            final ServiceProcessorHandler serviceProcessorHandler,
            final PHProfilePropertiesService phProfilePropertiesService) {
        this.instance = Validate.notNull(instance, "A PHInstance class must be provided");
        this.moProcessor = Validate.notNull(moProcessor, "A RabbitMQMoProcessor class must be provided");
        this.portingProcessor = Validate.notNull(portingProcessor, "A RabbitMQPortingProcessor class must be provided");
        this.recobrosProcessor = Validate.notNull(recobrosProcessorImpl, "A RabbitMQRecobrosProcessorImpl class must be provided");
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
        this.phProfilePropertiesService = Validate.notNull(phProfilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    @Override
    protected void startActions() {
        super.startActions();
        try {
            for (Provider p : providerSet) {
                try {
                    ChannelQuantityProvider cqp = instance.getConfigurations().get(p);
                    if (cqp.getBillingConsumerQuantity() > 0) {
                        rabbitMQConsumer.createChannel(p.getName() + ".recobros", cqp.getBillingConsumerQuantity(), recobrosProcessor);
                    }
                } catch (Exception ex) {
                    log.error("Error al crear consumer [recobros] para provider: [" + p + "]. " + ex, ex);
                }

                try {
                    ChannelQuantityProvider cqp = instance.getConfigurations().get(p);
                    if (cqp.getSmppConsumerQuantity() > 0) {
                        rabbitMQConsumer.createChannel(CommonFunction.ADAPTER_SMS_PORTING + p.getName(), 1, portingProcessor);
                    }
                } catch (Exception ex) {
                    log.error("Error al crear consumer [" + CommonFunction.ADAPTER_SMS_PORTING + "] para provider: [" + p + "]. " + ex, ex);
                }

                try {
                    ChannelQuantityProvider cqp = instance.getConfigurations().get(p);
                    if (cqp.getSmppConsumerQuantity() > 0) {
                        rabbitMQConsumer.createChannel(CommonFunction.ADAPTER_SMS_RCV + p.getName(), cqp.getSmppConsumerQuantity(), moProcessor);
                        String hour = phProfilePropertiesService.getCommonProperty(p.getId(), "cdag.renew.hour.start");
                        if (hour != null) {
                            try {
                                scheduledThreadPool.scheduleWithFixedDelay(new InstrumentedRunnable("renew.find", p.getName(), instrumentedObject) {
                                    @Override
                                    public boolean exec() {
                                        if (isStop) {
                                            return false;
                                        }
                                        Thread.currentThread().setName(CommonFunction.getTID("renew.find." + p.getName()));
                                        RLock lock = instrumentedObject.lockObject("CDAG_FIND_" + p.getName(), 60);
                                        long aux = instrumentedObject.getAtomicLong("CDAG_FIND_J_" + p.getName(), 50);
                                        if (aux == 0) {
                                            instrumentedObject.getAndIncrementAtomicLong("CDAG_FIND_J_" + p.getName(), 50);
                                            try {
                                                serviceProcessorHandler.findActiveRenew(p);
                                                instrumentedObject.unLockObject(lock);
                                                return true;
                                            } catch (Exception ex) {
                                            }
                                        }
                                        instrumentedObject.unLockObject(lock);
                                        return false;
                                    }
                                }, 1, 60, TimeUnit.MINUTES);
                            } catch (Exception ex) {
                            }
                        }
                    }
                } catch (Exception ex) {
                    log.error("Error al crear consumer [" + CommonFunction.ADAPTER_SMS_RCV + "] para provider: [" + p + "]. " + ex, ex);
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar cobro. " + ex, ex);
        }
    }
}
