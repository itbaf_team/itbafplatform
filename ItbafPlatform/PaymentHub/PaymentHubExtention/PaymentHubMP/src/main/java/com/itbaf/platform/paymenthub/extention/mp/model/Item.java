package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;
import java.math.BigDecimal;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Item {

    private String id;
    private String title;
    private String description;
    private String picture_url;
    private String category_id;
    private Integer quantity;
    private String currency_id;
    private BigDecimal unit_price = BigDecimal.ZERO;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("title", title)
                .add("description", description)
                .add("picture_url", picture_url)
                .add("category_id", category_id)
                .add("quantity", quantity)
                .add("currency_id", currency_id)
                .add("unit_price", unit_price)
                .omitNullValues().toString();
    }
}
