/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonDaemon;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

/**
 *
 * @author javier
 */
public class InstrumentedObject {

    public static String appName;
    public static String profile;
    public static String instance;
    public static String container;
    public static RedissonClient redis;
    public static ObjectMapper mapper;
    public static String logType;
    public static String redisConfigLocalPath;
    public static String redisConfigRemotePath;
    public static CommonDaemon commonDaemon;
    public static final Map<String, String> parameterMap = new HashMap();

    private final org.apache.logging.log4j.Logger log;

    @Inject
    public InstrumentedObject(/*@Named("app.name") String appName,
            @Named("guice.profile") String profile,
            @Named("guice.instance") String instance,
            @Named("guice.container") String container*/) {
        /* this.appName = appName;
        this.profile = profile;
        this.instance = instance;
        this.container = container;*/
        log = LogManager.getLogger(InstrumentedObject.class);
    }

    public List<RLock> lockSubscription(final String externalId, final String userAccount,
            final String externalUserAccount, final String externalCustomer, final Long sopId) {

        List<RLock> lst = new ArrayList();

        if (externalId != null) {
            lst.add(lockObject(externalId + "_" + sopId, 30));
        }
        if (userAccount != null && !userAccount.equals(externalId)) {
            lst.add(lockObject(userAccount + "_" + sopId, 30));
        }
        if (externalUserAccount != null && !externalUserAccount.equals(externalId) && !externalUserAccount.equals(userAccount)) {
            lst.add(lockObject(externalUserAccount + "_" + sopId, 30));
        }
        if (externalCustomer != null && !externalCustomer.equals(externalId) && !externalCustomer.equals(userAccount) && !externalCustomer.equals(externalUserAccount)) {
            lst.add(lockObject(externalCustomer + "_" + sopId, 30));
        }

        return lst;
    }

    public void unLockSubscription(List<RLock> lst) {
        for (RLock r : lst) {
            unLockObject(r);
        }
    }

    public RLock lockObject(String key, long leaseTimeSeconds) {
        try {
            RLock lock = redis.getFairLock(key);
            lock.lock(leaseTimeSeconds, TimeUnit.SECONDS);
            log.info("LOCK: [" + key + "]");
            return lock;
        } catch (Exception ex) {
            log.error("Error en LOCK: [" + key + "]. " + ex, ex);
        }
        return null;
    }

    public void unLockObject(RLock lock) {
        try {
            log.info("UNLOCK: [" + lock.getName() + "]");
            lock.unlock();
        } catch (Exception ex) {
        }
    }

    public long getAndIncrementAtomicLong(String name, long ttl, TimeUnit timeUnit) {
        RAtomicLong atomicLong = redis.getAtomicLong(name);
        Long val = atomicLong.getAndIncrement();
        if (val == 0) {
            atomicLong.expire(ttl, timeUnit);
        }

        return val;
    }

    public long getAndIncrementAtomicLong(String name, long ttlMinutes) {
        RAtomicLong atomicLong = redis.getAtomicLong(name);
        Long val = atomicLong.getAndIncrement();
        if (val == 0) {
            atomicLong.expire(ttlMinutes, TimeUnit.MINUTES);
        }

        return val;
    }

    public long decrementAndGetAtomicLong(String name) {
        RAtomicLong atomicLong = redis.getAtomicLong(name);
        return atomicLong.decrementAndGet();
    }

    public long getAtomicLong(String name, long ttlMinutes) {
        RAtomicLong atomicLong = redis.getAtomicLong(name);
        Long val = atomicLong.get();
        if (val == 0) {
            atomicLong.expire(ttlMinutes, TimeUnit.MINUTES);
        }

        return val;
    }

    public void resetAtomicLong(String name) {
        RAtomicLong atomicLong = redis.getAtomicLong(name);
        atomicLong.set(0);
    }

}
