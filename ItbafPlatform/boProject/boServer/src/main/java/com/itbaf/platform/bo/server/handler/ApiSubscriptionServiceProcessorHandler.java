/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.bo.server.handler;

import com.google.inject.Inject;
import com.itbaf.platform.bo.server.model.UserResponse;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageRequest;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionBillingService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ApiSubscriptionServiceProcessorHandler extends ServiceProcessorHandler {
    
    private final SubscriptionBillingService subscriptionBillingService;
    private final SubscriptionRegistryService subscriptionRegistryService;
    
    @Inject
    public ApiSubscriptionServiceProcessorHandler(final SubscriptionBillingService subscriptionBillingService,
            final SubscriptionRegistryService subscriptionRegistryService) {
        this.subscriptionBillingService = Validate.notNull(subscriptionBillingService, "A SubscriptionBillingService class must be provided");
        this.subscriptionRegistryService = Validate.notNull(subscriptionRegistryService, "A SubscriptionRegistryService class must be provided");
    }
    
    public PGMessageResponse getUserInformation(User user) {
        UserResponse ur = new UserResponse();
        ur.id = user.getId();
        ur.nickname = user.getCredentialUserProperty().getUsername().getNickname();
        ur.avatar = user.getCredentialUserProperty().getUsername().getAvatarURL();
        ur.permissions = user.getLimitPermissions();
        
        String url = genericPropertyService.getValueByKey("pg.server.url");
        
        ur.params.put("logoutURL", url + "/account/signout/slo");
        ur.params.put("profileURL", url + "/account/");
        
        PGMessageResponse rm = new PGMessageResponse();
        rm.meta.code = 200;
        rm.meta.status = Meta.Status.ok;
        rm.data.content = ur;
        return rm;
    }
    
    public PGMessageResponse getSubscriptions(User user, PGMessageRequest messageRequest) throws Exception {
     //   log.info("getSubscriptions. UserId: [" + user.getId() + "]. UserAccount: [" + messageRequest.userAccount + "]");
        messageRequest.userAccount = messageRequest.userAccount.trim().toLowerCase();
        if (!messageRequest.userAccount.contains("@") && messageRequest.searchProvider != null && messageRequest.searchProvider) {
            Country c = countryService.findByInternationalNumberPhone(messageRequest.userAccount);
            subscriptionService.getSubscriptionCountryFacadeRequest(messageRequest.userAccount, c.getCode());
        }
        
        List<Subscription> ss = subscriptionService.findSubscriptionsByUserAccount(messageRequest.userAccount);
        List<Subscription> aux = new ArrayList();
        if (ss != null && !ss.isEmpty()) {
            for (Subscription s : ss) {
                s.getSop().setIntegrationSettings(null);
                s.getSubscriptionRegistry().setSubscription(null);
                s.getSubscriptionRegistry().setSop(null);
                aux.add(s);
            }
        }
        
        PGMessageResponse rm = new PGMessageResponse();
        rm.meta.code = 200;
        rm.meta.status = Meta.Status.ok;
        rm.data.userAccount = messageRequest.userAccount;
        rm.data.content = aux;
        return rm;
    }
    
    public PGMessageResponse getActiveSubscriptions(User user, PGMessageRequest messageRequest) throws Exception {
       // log.info("getActiveSubscriptions. UserId: [" + user.getId() + "]. UserAccount: [" + messageRequest.userAccount + "]");
        
        messageRequest.userAccount = messageRequest.userAccount.trim().toLowerCase();
        if (!messageRequest.userAccount.contains("@") && messageRequest.searchProvider != null && messageRequest.searchProvider) {
            Country c = countryService.findByInternationalNumberPhone(messageRequest.userAccount);
            subscriptionService.getSubscriptionCountryFacadeRequest(messageRequest.userAccount, c.getCode());
        }
        
        List<Subscription> ss = subscriptionService.findSubscriptionsByUserAccount(messageRequest.userAccount);
        List<Subscription> aux = new ArrayList();
        if (ss != null && !ss.isEmpty()) {
            for (Subscription s : ss) {
                if (Subscription.Status.ACTIVE.equals(s.getStatus()) || Subscription.Status.PENDING.equals(s.getStatus())) {
                    s.getSop().setIntegrationSettings(null);
                    s.getSubscriptionRegistry().setSubscription(null);
                    s.getSubscriptionRegistry().setSop(null);
                    aux.add(s);
                }
            }
        }
        
        PGMessageResponse rm = new PGMessageResponse();
        rm.meta.code = 200;
        rm.meta.status = Meta.Status.ok;
        rm.data.userAccount = messageRequest.userAccount;
        rm.data.content = aux;
        return rm;
    }
    
    public PGMessageResponse deleteActiveSubscription(User user, PGMessageRequest messageRequest) throws Exception {
        
        SubscriptionRegistry sr = subscriptionRegistryService.findById(messageRequest.susbcriptionRegistryId);
        
        String txt = "No fue posible cancelar la suscripcion.";
        Integer processed = 1001;
        if (sr.getUserAccount().equals(messageRequest.userAccount)) {
            try {
                ResponseMessage rmx = subscriptionService.unsubscribeFacadeRequest(sr.getSubscription().getUserAccount(), sr.getSop(), "BO_" + user.getId());
                if (ResponseMessage.Status.OK.equals(rmx.status)) {
                    txt = "Suscripcion cancelada satisfactoriamente.";
                    processed = 1000;
                }
            } catch (Exception ex) {
                log.error("Error al cancelar suscripcion: [" + sr + "]. " + ex, ex);
            }
        }
        
        PGMessageResponse rm = getActiveSubscriptions(user, messageRequest);
        rm.data.message = txt;
        rm.data.processedCode = processed;
        return rm;
    }
    
    public PGMessageResponse searchBillingHistory(User user, PGMessageRequest messageRequest) {
        List<SubscriptionBilling> sbl = subscriptionBillingService.getSubscriptionBillingsByUserAccount(messageRequest.userAccount);
        Map<SubscriptionRegistry, List<SubscriptionBilling>> aux = new HashMap();
        
        if (sbl != null && !sbl.isEmpty()) {
            for (SubscriptionBilling sb : sbl) {
                sb.getSubscriptionRegistry().setSubscription(null);
                sb.setSop(null);
                sb.getSubscriptionRegistry().getSop().setIntegrationSettings(null);
                
                List<SubscriptionBilling> l = aux.get(sb.getSubscriptionRegistry());
                if (l == null) {
                    l = new ArrayList();
                    aux.put(sb.getSubscriptionRegistry(), l);
                }
                sb.setSubscriptionRegistry(null);
                l.add(sb);
            }
        }
        for (SubscriptionRegistry sr : aux.keySet()) {
            List<SubscriptionBilling> l = aux.get(sr);
            sr.setSubscriptionBillings(l);
        }
        
        PGMessageResponse rm = new PGMessageResponse();
        rm.meta.code = 200;
        rm.meta.status = Meta.Status.ok;
        rm.data.userAccount = messageRequest.userAccount;
        rm.data.content = (aux.isEmpty() ? null : aux.keySet());
        return rm;
    }
    
}
