/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.paymenthub.model.Operator;
import com.itbaf.platform.paymenthub.model.QOperator;
import com.querydsl.jpa.impl.JPAQuery;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAOperatorRepository extends AbstractJPARepositorySession<Operator, Long> {

    @Inject
    public JPAOperatorRepository(final Provider<EntityManager> entityManagerProvider) {
        super(Operator.class, entityManagerProvider);
    }

    public Operator findByName(String name) {
        if (name == null) {
            String error = "El nombre del Operator es nulo!!";
            log.error(error);

            throw new RuntimeException(error);
        }
        name = name.toUpperCase();
        QOperator operator = QOperator.operator;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Operator p = query.select(operator).from(operator).
                where(operator.name.eq(name)).fetchFirst();

        return p;
    }
}
