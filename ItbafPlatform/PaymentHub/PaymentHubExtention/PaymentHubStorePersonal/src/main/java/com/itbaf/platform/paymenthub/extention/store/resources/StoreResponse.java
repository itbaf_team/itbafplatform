package com.itbaf.platform.paymenthub.extention.store.resources;


public class StoreResponse {

    public String errorCode;
    public String errorMessage;
    public String ownerRightsPid;
    public String pid;
    public String type;
    public String title;
    public boolean isActive;
    public String status;

}
