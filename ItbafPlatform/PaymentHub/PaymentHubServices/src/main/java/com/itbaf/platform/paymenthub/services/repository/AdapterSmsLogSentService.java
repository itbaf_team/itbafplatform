/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.sms.AdapterSmsLogSent;

/**
 *
 * @author javier
 */
public interface AdapterSmsLogSentService {

    public void save(AdapterSmsLogSent adapterSmsLogSent) throws Exception;
}
