/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository.transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.pgp.model.wallet.WalletUser;
import com.itbaf.platform.pgp.model.wallet.properties.Credit;
import com.itbaf.platform.pgp.repository.jpa.model.wallet.JPAWalletUserRepository;
import com.itbaf.platform.pgp.services.repository.WalletUserService;
import java.util.Set;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
public class WalletUserServiceTransactional implements WalletUserService {

    private final JPAWalletUserRepository walletUserRepository;
    private final ObjectMapper mapper;

    @Inject
    public WalletUserServiceTransactional(final ObjectMapper mapper,
            final JPAWalletUserRepository walletUserRepository) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.walletUserRepository = Validate.notNull(walletUserRepository, "A JPAWalletUserRepository class must be provided");
    }

    @Override
    public void saveOrUpdate(WalletUser walletUser) throws Exception {

        try {
            walletUserRepository.merge(walletUser);
        } catch (Exception ex) {
            walletUserRepository.save(walletUser);
        }
    }

    @Override
    public WalletUser findById(Long id) {
        try {
            WalletUser wu = walletUserRepository.findById(id);
            if (wu != null) {
                Set<String> keys = wu.getCredits().keySet();
                for (String key : keys) {
                    Credit c = mapper.convertValue(wu.getCredits().get(key), Credit.class);
                    wu.getCredits().put(key, c);
                }
            }
            return wu;
        } catch (Exception ex) {
        }
        return null;
    }

}
