package com.itbaf.platform.paymenthub.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QServiceMatcher is a Querydsl query type for ServiceMatcher
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QServiceMatcher extends EntityPathBase<ServiceMatcher> {

    private static final long serialVersionUID = -1252025627L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QServiceMatcher serviceMatcher = new QServiceMatcher("serviceMatcher");

    public final StringPath genericName = createString("genericName");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    protected QService service;

    public QServiceMatcher(String variable) {
        this(ServiceMatcher.class, forVariable(variable), INITS);
    }

    public QServiceMatcher(Path<? extends ServiceMatcher> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QServiceMatcher(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QServiceMatcher(PathMetadata metadata, PathInits inits) {
        this(ServiceMatcher.class, metadata, inits);
    }

    public QServiceMatcher(Class<? extends ServiceMatcher> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.service = inits.isInitialized("service") ? new QService(forProperty("service")) : null;
    }

    public QService service() {
        if (service == null) {
            service = new QService(forProperty("service"));
        }
        return service;
    }

}

