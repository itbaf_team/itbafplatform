/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.co.movistar.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Subscription {

    @XmlType(name = "action")
    @XmlEnum
    public enum Action {
        ALTA, BAJA, BAJAATT
    }

    private String transactionId;//Adicionado por JF para procesar luego
    private String subscriptionId;
    private String media;
    private String msisdn;
    private Date datetime;
    private Action action;
    private String thread = Thread.currentThread().getName();

    @JacksonXmlProperty(localName = "subserviceId")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Long> subserviceIds;
    @JacksonXmlProperty(localName = "shortnumber")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Long> shortnumbers;
    private Date validityStart;

    @JacksonXmlProperty(localName = "serviceXBI")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Subscription> serviceXBIs;

    @JacksonXmlProperty(localName = "serviceGKP")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Subscription> serviceGKPs;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("transactionId", transactionId)
                .add("subscriptionId", subscriptionId)
                .add("msisdn", msisdn)
                .add("media", media)
                .add("action", action)
                .add("datetime", datetime)
                .add("subserviceIds", subserviceIds)
                .add("shortnumbers", shortnumbers)
                .add("validityStart", validityStart)
                .add("serviceXBIs", serviceXBIs)
                .add("serviceGKPs", serviceGKPs)
                .add("thread", thread)
                .omitNullValues().toString();
    }
}
