
package com.itbaf.platform.paymenthub.messaging.smpp.smppadapter;

import com.google.common.base.MoreObjects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="destinationAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="originationAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "destinationAddr",
    "originationAddr",
    "message"
})
@XmlRootElement(name = "RcvSmsRequest")
public class RcvSmsRequest {

    protected long id;
    @XmlElement(required = true)
    protected String destinationAddr;
    @XmlElement(required = true)
    protected String originationAddr;
    @XmlElement(required = true)
    protected String message;

    /**
     * Gets the value of the id property.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Gets the value of the destinationAddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationAddr() {
        return destinationAddr;
    }

    /**
     * Sets the value of the destinationAddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationAddr(String value) {
        this.destinationAddr = value;
    }

    /**
     * Gets the value of the originationAddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginationAddr() {
        return originationAddr;
    }

    /**
     * Sets the value of the originationAddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginationAddr(String value) {
        this.originationAddr = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("destinationAddr", destinationAddr)
                .add("originationAddr", originationAddr)
                .add("message", message)
                .omitNullValues().toString();
    }
}
