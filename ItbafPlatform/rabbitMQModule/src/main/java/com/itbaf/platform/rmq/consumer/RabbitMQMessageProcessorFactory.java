/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.rmq.consumer;

/**
 *
 * @author javier
 * @param <M>
 */
public interface RabbitMQMessageProcessorFactory <M extends MessageProcessor>{
 
    RabbitMQMessageProcessor create(M messageProcessor);
}
