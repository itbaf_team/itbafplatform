/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.model;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author javier
 */
public class SubscriptionData implements Serializable {

    private static final long serialVersionUID = 2019010911024L;

    public String object;
    public String id_subscription;
    public String id_service;
    public String id_customer;
    public String created;
    public String first_charge;
    public String last_charge;
    public String next_charge;
    public String canceled;
    public ValidateSubscription.ValidateSubscriptionStatus status;
    public String status_desc;
    public Long count;
    public Long active_subscriptions;
    public List<SubscriptionData> data;
    public String id_account;
    public String alias;
    public String type;
    public List<SubscriptionData> subscriptions;
    public String sent;
    public SubscriptionData subscription;
    public String message;
    public String failure_code;
    public String tracking_id;
    public String metadata;
    public String extras;

    @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("object", object)
                    .add("id_subscription", id_subscription)
                    .add("id_service", id_service)
                    .add("id_customer", id_customer)
                    .add("created", created)
                    .add("first_charge", first_charge)
                    .add("last_charge", last_charge)
                    .add("next_charge", next_charge)
                    .add("canceled", canceled)
                    .add("status", status)
                    .add("status_desc", status_desc)
                    .add("count", count)
                    .add("active_subscriptions", active_subscriptions)
                    .add("data", data)
                    .add("id_account", id_account)
                    .add("alias", alias)
                    .add("type", type)
                    .add("subscriptions", subscriptions)
                    .add("sent", sent)
                    .add("subscription", subscription)
                    .add("message", message)
                    .add("failure_code", failure_code)
                    .add("tracking_id", tracking_id)
                    .add("metadata", metadata)
                    .add("extras", extras)
                    .omitNullValues().toString();
        }
    
}
