/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.model;

/**
 *
 * @author JF
 */
@lombok.Getter
@lombok.Setter
public class ConsoleHttpRequest {

    private String requestMethod;
    private String requestUrl;
    private Long requestSize;
    private Integer status;
    private Long responseSize;
    private String userAgent;
    private String remoteIp;
    private String serverIp;
    private String referer;
    private boolean cacheLookup = false;
    private boolean cacheHit = false;
    private boolean cacheValidatedWithOriginServer = false;
    private Long cacheFillBytes;
    private String latency;
    ////
    private String method;
    private String url;
    private Integer responseStatusCode;

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
        this.method = requestMethod;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
        this.url = requestUrl;
    }

    public void setStatus(Integer status) {
        this.status = status;
        this.responseStatusCode = status;
    }
}
