package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPASmsSopRepository;
import com.itbaf.platform.paymenthub.services.repository.SmsSopService;
import org.apache.commons.lang3.Validate;

@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class SmsSopServiceTransactional implements SmsSopService {

    private final JPASmsSopRepository smsSopRepository;

    @Inject
    public SmsSopServiceTransactional(final JPASmsSopRepository smsSopRepository) {
        this.smsSopRepository = Validate.notNull(smsSopRepository, "A JPASmsSopRepository class must be provided");
    }

    @Override
    public String getSms(Long sop_id, String type) {
        String msmSopSen = null;
        try {
            msmSopSen =  smsSopRepository.getSms(sop_id,type);
        }catch (Exception ex ){
            log.info("No se pudo consultar el mensajes del sop: ["+ ex.getCause().getCause().getMessage() +"]");
        }
        return msmSopSen;
    }
}
