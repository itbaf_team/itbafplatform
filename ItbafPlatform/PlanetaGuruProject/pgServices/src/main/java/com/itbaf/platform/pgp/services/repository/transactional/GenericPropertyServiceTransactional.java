/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.pgp.model.GenericProperty;
import com.itbaf.platform.pgp.model.GenericProperty.Profile;
import com.itbaf.platform.pgp.repository.jpa.model.JPAGenericPropertyRepository;
import com.itbaf.platform.pgp.services.repository.GenericPropertyService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class GenericPropertyServiceTransactional implements GenericPropertyService {

    private final JPAGenericPropertyRepository genericPropertyRepository;
    private final Profile profile;

    @Inject
    public GenericPropertyServiceTransactional(@Named("guice.profile") String profile,
            final JPAGenericPropertyRepository genericPropertyRepository) {
        this.profile = Profile.getProfile(profile);
        this.genericPropertyRepository = Validate.notNull(genericPropertyRepository, "A JPAGenericPropertyRepository class must be provided");
    }

    @Override
    public String getValueByKey(String key) {
        return getValueByProfileKey(profile, key);
    }

    @Override
    public String getValueByProfileKey(String profile, String key) {
        return getValueByProfileKey(Profile.getProfile(profile), key);
    }

    @Override
    public String getValueByProfileKey(Profile profile, String key) {
        String value = null;
        Object aux = MainCache.memory5Hours().getIfPresent(profile + "_" + key);
        if (aux == null) {
            GenericProperty gp = genericPropertyRepository.getValueByProfileKey(profile, key);
            if (gp != null) {
                value = gp.getValue();
                MainCache.memory5Hours().put(profile + "_" + key, value);
            }
        } else {
            value = (String) aux;
        }

        return value;
    }

    @Override
    public void saveOrUpdate(GenericProperty gp) throws Exception {
        if (gp.getId() == null) {
            genericPropertyRepository.save(gp);
        } else {
            genericPropertyRepository.merge(gp);
        }
    }

    @Override
    public GenericProperty findById(Long id) {
        try {
            return  genericPropertyRepository.findById(id);
        } catch (Exception ex) {
            log.error("Error al obtener GenericProperty por ID: ["+id+"]. " + ex);
        }
        return null;
    }

    @Override
    public String getGeneralProperty(String key) {
         return getValueByProfileKey(profile, key);
    }

    @Override
    public String getCommonProperty(String key) {
        return getValueByProfileKey(Profile.ALL, key);
    }

    @Override
    public String getCommonProperty(Long providerId, String key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
