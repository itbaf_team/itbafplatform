/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.stats;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author JF
 */
public class StatsService {

    private final RabbitMQProducer rabbitMQProducer;

    @Inject
    public StatsService(final ObjectMapper mapper,
            final RabbitMQProducer rabbitMQProducer,
            final InstrumentedObject instrumentedObject) {
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
    }

    @Deprecated
    public Boolean sendToRabbit(ElasticData.ElasticType elasticType, String elasticId) throws Exception {
        if (elasticId != null) {
            String json = "{\"elasticData\":{\"elasticType\":\"" + elasticType + "\",\"elasticId\":\"" + elasticId + "\"},\"thread\":\"" + Thread.currentThread().getName() + "\",\"lastUpdateMillis\":" + System.currentTimeMillis() + "}";
            return rabbitMQProducer.messageSender(CommonFunction.PROCESS_DW_EVENTS_QUEUE + elasticType, json);
        }
        return null;
    }

    public Boolean sendToRabbitByID(ElasticData.ElasticType elasticType, String id) throws Exception {
        if (id != null) {
            String json = "{\"elasticData\":{\"elasticType\":\"" + elasticType + "\",\"id\":\"" + id + "\"},\"thread\":\"" + Thread.currentThread().getName() + "\",\"lastUpdateMillis\":" + System.currentTimeMillis() + "}";
            return rabbitMQProducer.messageSender(CommonFunction.PROCESS_DW_EVENTS_QUEUE + elasticType, json);
        }
        return null;
    }

    public Boolean sendToRabbitByFunctionType(ElasticData.ElasticType elasticType, String id, String functionType) throws Exception {
        if (id != null) {
            String json = "{\"elasticData\":{\"elasticType\":\"" + elasticType + "\",\"id\":\"" + id + "\",\"functionType\":\"" + functionType + "\"},\"thread\":\"" + Thread.currentThread().getName() + "\",\"lastUpdateMillis\":" + System.currentTimeMillis() + "}";
            return rabbitMQProducer.messageSender(CommonFunction.PROCESS_DW_EVENTS_QUEUE + elasticType, json);
        }
        return null;
    }

    public Boolean sendETLToRabbit(ElasticData.ElasticType elasticType, String json) throws Exception {
        if (json != null) {
            return rabbitMQProducer.messageSender(CommonFunction.PROCESS_DW_ETL_QUEUE + elasticType, json);
        }
        return null;
    }
}
