# ItbafPlatform

## Configuración de ambiente de trabajo

### Pre requisitos

1- Instalar:

- Java 11  
- Maven 3  
- Redis 5  
- Erland 21 
- Rabbitmq 3

2- Una vez instalado Rabbitmq, ingresar a la aplicación de cliente (por defecto http://localhost:15672) y crear un Virtual Host. Para esto, ir a la pestaña Admin, hacer click en Virtual Host en el lateral derecho y agregarlo con un nombre. 

3- Configurar la conexión del sistema a la base de datos mysql. Para esto se debe abrir el archivo [setting.conf](ItbafPlatform/commons/src/main/resources/sconfig/settings.properties) y establecer los valores correspondientes.

4- Configurar la conexión a rabbitmq. Para esto es necesario crear registros en la tabla PH_PROFILE_PROPERTIES de la base de datos paymentHub. Algunos Insert SQL de ejemplo:

```
INSERT INTO paymentHub.PH_PROFILE_PROPERTIES ( CREATED_DATE, UPDATE_DATE, PH_PROFILE_ID, PROVIDER_ID, `KEY`, VALUE, DESCRIPTION) VALUES ( {CREATED_DATE_NOW}, {UPDATE_DATE_NOW}, 1, 1, 'rabbitmq.password', 'guest', 'Parametros de conexion al RabbitMQ Prod');
INSERT INTO paymentHub.PH_PROFILE_PROPERTIES ( CREATED_DATE, UPDATE_DATE, PH_PROFILE_ID, PROVIDER_ID, `KEY`, VALUE, DESCRIPTION) VALUES ( {CREATED_DATE_NOW}, {UPDATE_DATE_NOW}, 1, 1, 'rabbitmq.port', '5672', 'Parametros de conexion al RabbitMQ Prod');
INSERT INTO paymentHub.PH_PROFILE_PROPERTIES ( CREATED_DATE, UPDATE_DATE, PH_PROFILE_ID, PROVIDER_ID, `KEY`, VALUE, DESCRIPTION) VALUES ( {CREATED_DATE_NOW}, {UPDATE_DATE_NOW}, 1, 1, 'rabbitmq.url', '127.0.0.1', 'Parametros de conexion al RabbitMQ Prod');
INSERT INTO paymentHub.PH_PROFILE_PROPERTIES ( CREATED_DATE, UPDATE_DATE, PH_PROFILE_ID, PROVIDER_ID, `KEY`, VALUE, DESCRIPTION) VALUES ( {CREATED_DATE_NOW}, {UPDATE_DATE_NOW}, 1, 1, 'rabbitmq.username', 'guest', 'Parametros de conexion al RabbitMQ Prod');
INSERT INTO paymentHub.PH_PROFILE_PROPERTIES ( CREATED_DATE, UPDATE_DATE, PH_PROFILE_ID, PROVIDER_ID, `KEY`, VALUE, DESCRIPTION) VALUES ( {CREATED_DATE_NOW}, {UPDATE_DATE_NOW}, 1, 1, 'rabbitmq.virtualHost', 'vHostPaymentHub', 'Parametros de conexion al RabbitMQ Prod');
```

Donde en la columna Value debe ir el valor configurado en el ambiente de trabajo de acuerdo a su key. Los mismos están configurados para perfil dev. 

5- Configurar los servicios de payment hub que se comuniquen con PH.SERVER. Estos se caracterizan por tener la key `internal.service.url`. Algunos ejemplos:

```
INSERT INTO paymentHub.PH_PROFILE_PROPERTIES ( CREATED_DATE, UPDATE_DATE, PH_PROFILE_ID, PROVIDER_ID, `KEY`, VALUE, DESCRIPTION) VALUES ({CREATED_DATE_NOW}, {UPDATE_DATE_NOW}, 1, 32, 'internal.service.url', 'http://localhost:9934/rest/inner/', 'URL interna al modulo PaymentHubStore Timwe');
INSERT INTO paymentHub.PH_PROFILE_PROPERTIES ( CREATED_DATE, UPDATE_DATE, PH_PROFILE_ID, PROVIDER_ID, `KEY`, VALUE, DESCRIPTION) VALUES ({CREATED_DATE_NOW}, {UPDATE_DATE_NOW}, 1, 34, 'internal.service.url', 'http://localhost:9935/rest/inner/', 'URL interna al modulo PaymentHubTiaxa Telcel Mexico');
INSERT INTO paymentHub.PH_PROFILE_PROPERTIES ( CREATED_DATE, UPDATE_DATE, PH_PROFILE_ID, PROVIDER_ID, `KEY`, VALUE, DESCRIPTION) VALUES ({CREATED_DATE_NOW}, {UPDATE_DATE_NOW}, 1, 2002, 'internal.service.url', 'http://localhost:9938/rest/inner/', 'URL interna al modulo PaymentHubStore Personal Argentina');
INSERT INTO paymentHub.PH_PROFILE_PROPERTIES ( CREATED_DATE, UPDATE_DATE, PH_PROFILE_ID, PROVIDER_ID, `KEY`, VALUE, DESCRIPTION) VALUES ({CREATED_DATE_NOW}, {UPDATE_DATE_NOW}, 1, 2003, 'internal.service.url', 'http://localhost:9928/rest/inner/', 'URL interna al modulo PaymentHubSMT');
```

El puerto donde corre cada servicio se define en el archivo `setting-dev.properties` de cada módulo. Por ejemplo, para Timwe, la url es http://localhost:9934/rest/inner/, dicho puerto tiene que estar configurado en [setting-dev.properties](ItbafPlatform/commons/src/main/resources/sconfig/settings-dev.properties)

### Compilar y empaquetar

Para compilar y empaquetar en un **jar**, se hace a través de maven. Desde el path [ItbafPlatform](ItbafPlatform) del proyecto, correr el siguiente comando:

```
mvn -B -e --also-make  --projects {PATH_MODULE} -s ./settings.xml -Dmaven.test.skip=true  clean compile install package
```

Donde en PATH_MODULE tiene que ir la ruta relativa al módulo. Por ejemplo, para Tiaxa:

```
mvn -B -e --also-make  --projects PaymentHub/PaymentHubExtention/PaymentHubTiaxa/ -s ./settings.xml -Dmaven.test.skip=true  clean compile install package
```

De esta forma se generará el **jar** en la carpeta [target](ItbafPlatform/PaymentHub/PaymentHubExtention/PaymentHubTiaxa/target)

### Correr servicio

En el caso que el servicio a levantar sea un módulo de proveedor (ejemplo Tiaxa, SMT, Spiralis) se requiere configurar una variable de entorno INSTANCE_ID con el identificador del registro de la tabla PH_INSTANCE de la base de datos paymentHub. 

Ejemplo para SMT

```
export INSTANCE_ID=9
```

Luego, tanto para un módulo proveedor como para cualquier otro, ir a la carpeta target del módulo y ejecutar el **jar**:

```
java -jar target/PaymentHubServer.jar 
```

Si se quiere hacer debug, usar las banderas necesarias:

```
java  -jar -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=*:5005 target/PaymentHubServer.jar
```

## IMPORTANTE

Para generar el **jar**, agrego las siguientes líneas en el archivo **pom.xml** del módulo, dentro del tag `plugins`:

```
            <plugin>
                <!-- Build an executable JAR -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>3.1.0</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <classpathPrefix>lib/</classpathPrefix>
                            <mainClass>com.itbaf.platform.paymenthub.extention.smt.PaymentHubInstance</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
```

__Nota__:
Estimo que en producción esto se debe hacer de forma más óptima, pero desde mi ambiente de trabajo no encontré cómo generar el jar desde línea de comando sin la configuración del **pom.xml**. Dicha modificación no se debe subir al repositorio.

## Documentación recomendada

- [PaymentHub Project](https://wiki.planeta.guru/doku.php?id=paymenthub)


