package org.csapi.schema.parlayx.syncsubscription.v1_0;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.extention.smt.soap.SoapAdapter;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * <p>
 * Java class for syncOrderRelationship complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="syncOrderRelationship">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userID" type="{http://www.csapi.org/schema/parlayx/syncsubscription/v1_0}UserID"/>
 *         &lt;element name="spID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="updateType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="creatTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="effectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="notifyAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rentResult" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="extensionInfo" type="{http://www.csapi.org/schema/parlayx/syncsubscription/v1_0}NamedParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "syncOrderRelationship", namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local", propOrder = {
    "userID",
    "spID",
    "productID",
    "serviceID",
    "updateType",
    "creatTime",
    "effectiveTime",
    "expireTime",
    "notifyAddress",
    "rentResult",
    "extensionInfo"
})
public class SyncOrderRelationship {

    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local", required = true)
    protected UserID userID;
    @XmlJavaTypeAdapter(SoapAdapter.class)
    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local", required = true)
    protected String spID;
    @XmlJavaTypeAdapter(SoapAdapter.class)
    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local", required = true)
    protected String productID;
    @XmlJavaTypeAdapter(SoapAdapter.class)
    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local", required = true)
    protected String serviceID;
    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local")
    protected int updateType;
    @XmlJavaTypeAdapter(SoapAdapter.class)
    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local", required = true)
    protected String creatTime;
    @XmlJavaTypeAdapter(SoapAdapter.class)
    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local")
    protected String effectiveTime;
    @XmlJavaTypeAdapter(SoapAdapter.class)
    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local")
    protected String expireTime;
    @XmlJavaTypeAdapter(SoapAdapter.class)
    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local")
    protected String notifyAddress;
    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local")
    protected Integer rentResult;
    @XmlElement(namespace = "http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local")
    protected NamedParameterList extensionInfo;
    @XmlTransient
    private Map<String, String> extensionInfoMap;
    @XmlTransient
    private String countryCode;
    @XmlTransient
    public String thread = Thread.currentThread().getName();

    public Map<String, String> getExtensionInfoMap() {
        if (extensionInfoMap == null) {
            extensionInfoMap = new HashMap();
            try {
                for (NamedParameter np : extensionInfo.getNamedParameters()) {
                    extensionInfoMap.put(np.key, np.value);
                }
            } catch (Exception ex) {
            }
        }
        return extensionInfoMap;
    }

    public String getFrequencyType() {
        String ft = getExtensionInfoMap().get("chargeMode");
        if (ft != null) {
            switch (ft) {
                case "0":
                    ft = "month";
                    break;
                case "1":
                    ft = "times";
                    break;
                case "4":
                    ft = "volume";
                    break;
                case "5":
                    ft = "time";
                    break;
                case "7":
                    ft = "Free";
                    break;
                case "10":
                    ft = "days";
                    break;
                case "13":
                    ft = "months";
                    break;
                case "18":
                    ft = "day";
                    break;
                case "19":
                    ft = "week";
                    break;
                case "20":
                    ft = "weeks";
                    break;
            }
        }

        return ft;
    }

    public String getChannel() {
        String channel = getExtensionInfoMap().get("channelID");

        if (channel != null) {
            switch (channel) {
                case "1":
                    channel = "WEB";
                    break;
                case "2":
                    channel = "SMS";
                    break;
                case "3":
                    channel = "USSD";
                    break;
                case "4":
                    channel = "IVR";
                    break;
                case "5":
                    channel = "CC_CRM";
                    break;
                case "6":
                    channel = "PDA";
                    break;
                case "7":
                    channel = "WAP";
                    break;
                case "10":
                    channel = "PROMOTION";
                    break;
                case "99":
                    channel = "OTHER";
                    break;
                case "128":
                    channel = "SYSTEM";
                    break;
                case "100":
                    channel = "SP_WEBSITE";
                    break;
                case "102":
                    channel = "SMT_CONSOLE";
                    break;
            }
        }

        return channel;
    }

    public String getAdProvider() {
        return getExtensionInfoMap().get("adProvider");
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * Gets the value of the userID property.
     *
     * @return possible object is {@link UserID }
     *
     */
    public UserID getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     *
     * @param value allowed object is {@link UserID }
     *
     */
    public void setUserID(UserID value) {
        this.userID = value;
    }

    /**
     * Gets the value of the spID property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getSpID() {
        return spID;
    }

    /**
     * Sets the value of the spID property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setSpID(String value) {
        this.spID = value;
    }

    /**
     * Gets the value of the productID property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getProductID() {
        return productID;
    }

    /**
     * Sets the value of the productID property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setProductID(String value) {
        this.productID = value;
    }

    /**
     * Gets the value of the serviceID property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getServiceID() {
        return serviceID;
    }

    /**
     * Sets the value of the serviceID property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setServiceID(String value) {
        this.serviceID = value;
    }

    /**
     * Gets the value of the updateType property.
     *
     */
    public int getUpdateType() {
        return updateType;
    }

    /**
     * Sets the value of the updateType property.
     *
     */
    public void setUpdateType(int value) {
        this.updateType = value;
    }

    /**
     * Gets the value of the creatTime property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCreatTime() {
        return creatTime;
    }

    /**
     * Sets the value of the creatTime property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCreatTime(String value) {
        this.creatTime = value;
    }

    /**
     * Gets the value of the effectiveTime property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getEffectiveTime() {
        return effectiveTime;
    }

    /**
     * Sets the value of the effectiveTime property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setEffectiveTime(String value) {
        this.effectiveTime = value;
    }

    /**
     * Gets the value of the expireTime property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getExpireTime() {
        return expireTime;
    }

    /**
     * Sets the value of the expireTime property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setExpireTime(String value) {
        this.expireTime = value;
    }

    /**
     * Gets the value of the notifyAddress property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getNotifyAddress() {
        return notifyAddress;
    }

    /**
     * Sets the value of the notifyAddress property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setNotifyAddress(String value) {
        this.notifyAddress = value;
    }

    /**
     * Gets the value of the rentResult property.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getRentResult() {
        return rentResult;
    }

    /**
     * Sets the value of the rentResult property.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setRentResult(Integer value) {
        this.rentResult = value;
    }

    /**
     * Gets the value of the extensionInfo property.
     *
     * @return possible object is {@link NamedParameterList }
     *
     */
    public NamedParameterList getExtensionInfo() {
        return extensionInfo;
    }

    /**
     * Sets the value of the extensionInfo property.
     *
     * @param value allowed object is {@link NamedParameterList }
     *
     */
    public void setExtensionInfo(NamedParameterList value) {
        this.extensionInfo = value;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userID", userID)
                .add("spID", spID)
                .add("productID", productID)
                .add("serviceID", serviceID)
                .add("updateType", updateType)
                .add("creatTime", creatTime)
                .add("effectiveTime", effectiveTime)
                .add("expireTime", expireTime)
                .add("notifyAddress", notifyAddress)
                .add("rentResult", rentResult)
                .add("extensionInfo", getExtensionInfoMap())
                .add("countryCode", countryCode)
                .add("thread", thread)
                .omitNullValues().toString();
    }
}
