
package com.itbaf.platform.paymenthub.extention.store.ws.subscription;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="viewName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="limit" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="offset" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="filters" type="{urn:SendatelSoaService}arrayOfFilter"/>
 *         &lt;element name="order" type="{urn:SendatelSoaService}arrayOfFieldOrder"/>
 *         &lt;element name="credentials" type="{urn:SendatelSoaService}securityInfo"/>
 *         &lt;element name="params" type="{urn:SendatelSoaService}arrayOfExtraParam"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "viewName",
    "limit",
    "offset",
    "filters",
    "order",
    "credentials",
    "params"
})
@XmlRootElement(name = "getList")
public class GetList {

    @XmlElement(required = true)
    protected String viewName;
    protected int limit;
    protected int offset;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfFilter filters;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfFieldOrder order;
    @XmlElement(required = true)
    protected SecurityInfo credentials;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfExtraParam params;

    /**
     * Gets the value of the viewName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViewName() {
        return viewName;
    }

    /**
     * Sets the value of the viewName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViewName(String value) {
        this.viewName = value;
    }

    /**
     * Gets the value of the limit property.
     * 
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Sets the value of the limit property.
     * 
     */
    public void setLimit(int value) {
        this.limit = value;
    }

    /**
     * Gets the value of the offset property.
     * 
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Sets the value of the offset property.
     * 
     */
    public void setOffset(int value) {
        this.offset = value;
    }

    /**
     * Gets the value of the filters property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFilter }
     *     
     */
    public ArrayOfFilter getFilters() {
        return filters;
    }

    /**
     * Sets the value of the filters property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFilter }
     *     
     */
    public void setFilters(ArrayOfFilter value) {
        this.filters = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfFieldOrder }
     *     
     */
    public ArrayOfFieldOrder getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfFieldOrder }
     *     
     */
    public void setOrder(ArrayOfFieldOrder value) {
        this.order = value;
    }

    /**
     * Gets the value of the credentials property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityInfo }
     *     
     */
    public SecurityInfo getCredentials() {
        return credentials;
    }

    /**
     * Sets the value of the credentials property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityInfo }
     *     
     */
    public void setCredentials(SecurityInfo value) {
        this.credentials = value;
    }

    /**
     * Gets the value of the params property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public ArrayOfExtraParam getParams() {
        return params;
    }

    /**
     * Sets the value of the params property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public void setParams(ArrayOfExtraParam value) {
        this.params = value;
    }

}
