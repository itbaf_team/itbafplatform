
package org.csapi.schema.parlayx.payment.amount_charging.v2_1.local;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.csapi.schema.parlayx.payment.v2_1.Parameters;


/**
 * <p>Java class for refundAmount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="refundAmount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chargeTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="endUserIdentifier" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *         &lt;element name="extraParams" type="{http://www.csapi.org/schema/parlayx/payment/v2_1}Parameters" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "refundAmount", propOrder = {
    "chargeTransactionID",
    "endUserIdentifier",
    "extraParams"
})
public class RefundAmount {

    @XmlElement(required = true)
    protected String chargeTransactionID;
    @XmlElement(required = true)
    @XmlSchemaType(name = "anyURI")
    protected String endUserIdentifier;
    protected Parameters extraParams;

    /**
     * Gets the value of the chargeTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeTransactionID() {
        return chargeTransactionID;
    }

    /**
     * Sets the value of the chargeTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeTransactionID(String value) {
        this.chargeTransactionID = value;
    }

    /**
     * Gets the value of the endUserIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndUserIdentifier() {
        return endUserIdentifier;
    }

    /**
     * Sets the value of the endUserIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndUserIdentifier(String value) {
        this.endUserIdentifier = value;
    }

    /**
     * Gets the value of the extraParams property.
     * 
     * @return
     *     possible object is
     *     {@link Parameters }
     *     
     */
    public Parameters getExtraParams() {
        return extraParams;
    }

    /**
     * Sets the value of the extraParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link Parameters }
     *     
     */
    public void setExtraParams(Parameters value) {
        this.extraParams = value;
    }

}
