/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.persistence.util.HibernateEquality;
import com.querydsl.core.annotations.Config;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author javier
 */
@Entity(name = "GENERIC_PROPERTIES")
@Table(name = "GENERIC_PROPERTIES")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class GenericProperty implements Serializable {

    public enum Profile {
        ALL, DEV, STAGING, PROD;

        public static Profile getProfile(String profile) {
            Profile p = ALL;
            switch (profile.toUpperCase()) {
                case "DEV":
                    p = DEV;
                    break;
                case "STAGING":
                    p = STAGING;
                    break;
                case "PROD":
                    p = PROD;
                    break;
            }
            return p;
        }
    }

    private static final long serialVersionUID = 20170926094201L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "PROFILE", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Profile profile;

    @Column(name = "KEY_P", nullable = false)
    private String key;

    @Column(name = "VALUE_P", nullable = false)
    private String value;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("profile", profile)
                .add("key", key)
                .add("value", value)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, profile, key, value);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<GenericProperty>()
                .over(GenericProperty::getId)
                .over(GenericProperty::getProfile)
                .over(GenericProperty::getKey)
                .over(GenericProperty::getValue)
                .asserts(this, obj);
    }
}
