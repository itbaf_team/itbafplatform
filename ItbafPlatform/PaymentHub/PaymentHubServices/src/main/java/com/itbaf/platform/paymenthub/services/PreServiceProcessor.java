/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services;

import com.google.inject.Inject;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.sync.ServiceProcessor;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.TariffService;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.redisson.api.RLock;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public abstract class PreServiceProcessor extends ServiceProcessor {

    @Inject
    protected PHInstance instance;
    @Inject
    protected SopService sopService;
    @Inject
    protected TariffService tariffService;
    @Inject
    protected SubscriptionManager subscriptionManager;

    @Override
    public final ChannelQuantityProvider getChannelQuantityProvider(Provider p) {
        return instance.getConfigurations().get(p);
    }

    protected Tariff updateMainTariff(SOP sop, BigDecimal fullAmount, BigDecimal netAmount) {
        Tariff t = null;
        RLock lock = instrumentedObject.lockObject("updateMainTariff_" + sop.getId() + "_" + fullAmount + "_" + netAmount, 30L);
        try {
            if (fullAmount == null || netAmount == null) {
                Tariff tm = sop.getMainTariff();
                BigDecimal aux = (tm.getNetAmount().multiply(BigDecimal.valueOf(100))).divide(tm.getFullAmount(), 2, RoundingMode.CEILING);
                if (fullAmount == null && netAmount != null && netAmount.compareTo(tm.getNetAmount()) > 0) {
                    log.fatal("El parametro fullAmount de MainTariff es NULL. Se actualizara con base en el existen. Hay que verificar si es correcto. [" + fullAmount + "][" + netAmount + "] - SOP: [" + sop + "]");
                    aux = aux.add(new BigDecimal(100));
                    fullAmount = aux.multiply(netAmount).divide(BigDecimal.valueOf(100), RoundingMode.HALF_EVEN);
                } else if (netAmount == null && fullAmount != null && fullAmount.compareTo(tm.getFullAmount()) > 0) {
                    log.fatal("El parametro netAmount de MainTariff es NULL. Se actualizara con base en el existen. Hay que verificar si es correcto. [" + fullAmount + "][" + netAmount + "] - SOP: [" + sop + "]");
                    netAmount = aux.multiply(fullAmount).divide(BigDecimal.valueOf(100), RoundingMode.HALF_EVEN);
                }
            }

            if (fullAmount != null && netAmount != null && fullAmount.compareTo(netAmount) > 0) {
                Tariff ta = sop.getMainTariff();
                Tariff tm = new Tariff();
                tm.setCurrency(ta.getCurrency());
                tm.setExternalId(ta.getExternalId());
                tm.setFrecuency(ta.getFrecuency());
                tm.setFrecuencyType(ta.getFrecuencyType());
                tm.setFullAmount(fullAmount);
                tm.setLocalAmount(ta.getLocalAmount());
                tm.setLocalCurrency(ta.getLocalCurrency());
                tm.setNetAmount(netAmount);
                tm.setPercentage(ta.getPercentage());
                tm.setTariffType(ta.getTariffType());
                tm.setStatus(Status.ENABLE);
                tm.setFromDate(new Date());
                Calendar c = Calendar.getInstance();
                c.add(Calendar.YEAR, 1);
                tm.setToDate(c.getTime());

                tariffService.saveOrUpdate(tm);
                t = tm;
                log.info("Creada nueva tarifa principal: [" + tm + "]");

                sop.getTariffs().add(t);
                sopService.saveOrUpdate(sop);
                sop.setTsTariff(null);
                sop.setMainTariff(null);

                ta.setToDate(tm.getFromDate());
                ta.setStatus(Status.DISABLE);
                tariffService.saveOrUpdate(ta);
                log.info("Actualizada antigua tarifa principal: [" + ta + "]");
            }
        } catch (Exception ex) {
            log.error("Error al actualizar la tarifa principal para fullAmount: [" + fullAmount + "] - netAmount: [" + netAmount + "] - SOP: [" + sop + "]. " + ex, ex);
        }
        instrumentedObject.unLockObject(lock);
        return t;
    }

    protected Tariff updateNetAmountChildrenTariff(SOP sop, BigDecimal netAmount, String nameId) {
        Tariff t = null;
        RLock lock = instrumentedObject.lockObject("updateNetAmountChildrenTariff" + sop.getId() + "_" + nameId + "_" + netAmount, 30L);
        try {
            Tariff tm = sop.getMainTariff();
            if (netAmount != null && tm.getNetAmount().compareTo(netAmount) > 0) {
                BigDecimal aux = (netAmount.multiply(BigDecimal.valueOf(100))).divide(tm.getNetAmount(), 2, RoundingMode.CEILING);
                // log.info("Porcentaje: [" + aux + "]. [" + tm.getNetAmount() + "][" + netAmount + "]");
                Tariff ta = sop.findTariffByPercentage(aux);

                t = new Tariff();
                t.setCurrency(tm.getCurrency());
                t.setExternalId(sop.getProvider().getName() + "." + nameId);
                if ("week".equals(sop.getIntegrationSettings().get("frequencyType"))) {
                    t.setFrecuency(7);
                } else if ("month".equals(sop.getIntegrationSettings().get("frequencyType"))) {
                    t.setFrecuency(30);
                } else {
                    t.setFrecuency(1);
                }
                t.setFrecuencyType("days");
                t.setLocalCurrency(tm.getLocalCurrency());
                t.setLocalAmount(aux.multiply(tm.getLocalAmount()).divide(BigDecimal.valueOf(100), RoundingMode.HALF_EVEN));
                t.setPercentage(aux);
                t.setStatus(Status.ENABLE);
                t.setTariffType(Tariff.TariffType.CHILDREN);
                t.setFromDate(new Date());
                Calendar c = Calendar.getInstance();
                c.add(Calendar.YEAR, 1);
                t.setToDate(c.getTime());
                t.setFullAmount(aux.multiply(tm.getFullAmount()).divide(BigDecimal.valueOf(100), RoundingMode.HALF_EVEN));
                t.setNetAmount(netAmount);

                tariffService.saveOrUpdate(t);
                log.info("Creada nueva tarifa hija: [" + t + "]");

                sop.getTariffs().add(t);
                sopService.saveOrUpdate(sop);
                if (ta != null) {
                    ta.setToDate(tm.getFromDate());
                    ta.setStatus(Status.DISABLE);
                    tariffService.saveOrUpdate(ta);
                    log.info("Actualizada antigua tarifa hija: [" + t + "]");
                }
                sop.setTsTariff(null);
            }
        } catch (Exception ex) {
            log.error("Error al actualizar la tarifa hija para netAmount: [" + netAmount + "] - SOP: [" + sop + "]. " + ex, ex);
            t = null;
        }
        instrumentedObject.unLockObject(lock);
        return t;
    }

    protected Tariff updateFullAmountChildrenTariff(SOP sop, BigDecimal fullAmount, String nameId) {
        Tariff t = null;
        RLock lock = instrumentedObject.lockObject("updateFullAmountChildrenTariff" + sop.getId() + "_" + nameId + "_" + fullAmount, 30L);
        try {
            Tariff tm = sop.getMainTariff();
            if (fullAmount != null && tm.getFullAmount().compareTo(fullAmount) > 0) {
                BigDecimal aux = (fullAmount.multiply(BigDecimal.valueOf(100))).divide(tm.getFullAmount(), 2, RoundingMode.CEILING);

                //  log.info("Porcentaje: [" + aux + "]. [" + tm.getFullAmount() + "][" + fullAmount + "]");
                Tariff ta = sop.findTariffByPercentage(aux);

                t = new Tariff();
                t.setCurrency(tm.getCurrency());
                t.setExternalId(sop.getProvider().getName() + "." + nameId);
                if ("week".equals(sop.getIntegrationSettings().get("frequencyType"))) {
                    t.setFrecuency(7);
                } else if ("month".equals(sop.getIntegrationSettings().get("frequencyType"))) {
                    t.setFrecuency(30);
                } else {
                    t.setFrecuency(1);
                }
                t.setFrecuencyType("days");
                t.setLocalCurrency(tm.getLocalCurrency());
                t.setLocalAmount(aux.multiply(tm.getLocalAmount()).divide(BigDecimal.valueOf(100), RoundingMode.HALF_EVEN));
                t.setPercentage(aux);
                t.setStatus(Status.ENABLE);
                t.setTariffType(Tariff.TariffType.CHILDREN);
                t.setFromDate(new Date());
                Calendar c = Calendar.getInstance();
                c.add(Calendar.YEAR, 1);
                t.setToDate(c.getTime());
                t.setFullAmount(fullAmount);
                t.setNetAmount(aux.multiply(tm.getNetAmount()).divide(BigDecimal.valueOf(100), RoundingMode.HALF_EVEN));
                tariffService.saveOrUpdate(t);
                log.info("Creada nueva tarifa hija: [" + t + "]");

                sop.getTariffs().add(t);
                sopService.saveOrUpdate(sop);

                if (ta != null) {
                    ta.setToDate(tm.getFromDate());
                    ta.setStatus(Status.DISABLE);
                    tariffService.saveOrUpdate(ta);
                    log.info("Actualizada antigua tarifa hija: [" + t + "]");
                }
                sop.setTsTariff(null);
            }
        } catch (Exception ex) {
            log.error("Error al actualizar la tarifa hija para fullAmount: [" + fullAmount + "] - SOP: [" + sop + "]. " + ex, ex);
            t = null;
        }
        instrumentedObject.unLockObject(lock);
        return t;
    }

    protected Tariff updateTariff(Tariff t, SOP sop, BigDecimal fullAmount, BigDecimal netAmount) throws Exception {
        log.info("Se actualizara con: [" + fullAmount + "][" + netAmount + "] la tariff: [" + t + "] ");
        t.setFullAmount(fullAmount);
        t.setNetAmount(netAmount);
        tariffService.saveOrUpdate(t);
        sopService.saveOrUpdate(sop);
        log.info("Actualizada tarifa: [" + t + "]");

        return t;
    }

    public abstract ResponseMessage getSubscriptionStatus(Subscription s) throws Exception;

    @Override
    public void syncSubscriptions(Provider provider) {
        log.info("Sincronizacion de suscripciones para provider: [" + provider.getName() + "]");

        List<Subscription> slist = subscriptionManager.getSyncSubscriptions(provider);
        if (slist == null) {
            log.info("No hay suscripciones a sincronizar para provider: [" + provider.getName() + "]");
            return;
        }

        log.info("Suscripciones a sincronizar encontradas: [" + slist.size() + "] - provider: [" + provider.getName() + "]");
        for (Subscription s : slist) {
            try {
                s.getSubscriptionRegistry().setSubscription(null);
                String json = mapper.writeValueAsString(s);
                rabbitMQProducer.messageSender("sync.subscription." + provider.getName(), json);
            } catch (Exception ex) {
                log.error("Error al procesar SYNC: [" + s + "]. " + ex, ex);
            }
        }
    }

    @Override
    public void syncSubscription(Subscription s) {
        try {
            List<RLock> lock = instrumentedObject.lockSubscription(s.getSubscriptionRegistry().getExternalId(),
                    s.getUserAccount(), s.getExternalUserAccount(), s.getExternalCustomer(), s.getSop().getId());
            s = this.subscriptionManager.getSubscription(s.getId());
            if (s != null && (Subscription.Status.TRIAL.equals(s.getStatus()) || Subscription.Status.ACTIVE.equals(s.getStatus()) || Subscription.Status.PENDING.equals(s.getStatus())) || Subscription.Status.RETENTION.equals(s.getStatus())) {
                try {
                    ResponseMessage rm = getSubscriptionStatus(s);
                    switch (rm.status) {
                        case TRIAL:
                            subscriptionManager.updateSubscriptionStatus(s, Subscription.Status.TRIAL);
                            break;
                        case ACTIVE:
                            subscriptionManager.updateSubscriptionStatus(s, Subscription.Status.ACTIVE);
                            break;
                        case PENDING:
                            subscriptionManager.updateSubscriptionStatus(s, Subscription.Status.PENDING);
                            break;
                        case BLACKLIST:
                            subscriptionManager.updateSubscriptionStatus(s, Subscription.Status.BLACKLIST);
                            break;
                        case REMOVED:
                            subscriptionManager.updateSubscriptionStatus(s, Subscription.Status.CANCELLED);
                            break;
                        case LOCKED:
                            s.getSubscriptionRegistry().setOriginUnsubscription("SYNC:LOCKED");
                            subscriptionManager.updateSubscriptionStatus(s, Subscription.Status.LOCKED);
                            break;
                        case ERROR:
                            break;
                        case RETENTION:
                            subscriptionManager.updateSubscriptionStatus(s, Subscription.Status.RETENTION);
                            break;
                    }
                } catch (Exception ex) {
                    log.error("Error al obtener el estado de la suscripcion para: [" + s + "]. " + ex, ex);
                }
            }
            instrumentedObject.unLockSubscription(lock);
        } catch (Exception ex) {
        }
    }
}
