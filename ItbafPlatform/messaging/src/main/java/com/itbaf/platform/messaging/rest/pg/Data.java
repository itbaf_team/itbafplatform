/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.messaging.rest.pg;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class Data {
    
    /**
     * 1000 Cancelacion de suscripcion OK 
     * 1001 No se realizo la cancelacion de la suscripcion
     */
    public Integer processedCode;
    public String userAccount;
    public String message;
    public Object object;
    public Object content;
    public String urlRedirect;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userAccount",userAccount)
                .add("message", message)
                .add("processedCode", processedCode)
                .add("object", object)
                .add("urlRedirect", urlRedirect)
                .add("content", content)
                .omitNullValues().toString();
    }
}
