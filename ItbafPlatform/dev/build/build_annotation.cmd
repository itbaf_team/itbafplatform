docker run -it --rm  -v %cd%/dev/.m2:/root/.m2 -v %cd%:/usr/src -w /usr/src maven:3-jdk-11 mvn -B -e --projects resources/itbaf-annotation -Dmaven.test.skip=true clean compile install
