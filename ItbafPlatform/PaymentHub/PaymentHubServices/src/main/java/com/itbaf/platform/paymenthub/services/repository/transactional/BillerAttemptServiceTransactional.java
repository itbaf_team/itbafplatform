/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.billing.BillerAttempt;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPABillerAttemptRepository;
import com.itbaf.platform.paymenthub.services.repository.BillerAttemptService;
import java.util.List;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class BillerAttemptServiceTransactional implements BillerAttemptService {

    private final JPABillerAttemptRepository billerAttemptRepository;

    @Inject
    public BillerAttemptServiceTransactional(final JPABillerAttemptRepository billerAttemptRepository) {
        this.billerAttemptRepository = Validate.notNull(billerAttemptRepository, "A JPABillerAttemptRepository class must be provided");
    }

    @Override
    public BillerAttempt findById(Long id) throws Exception {
        return billerAttemptRepository.findById(id);
    }

    @Override
    public void saveOrUpdate(BillerAttempt billerAttempt) throws Exception {

        try {
            billerAttemptRepository.merge(billerAttempt);
        } catch (Exception ex) {
            try {
                billerAttemptRepository.save(billerAttempt);
            } catch (Exception exx) {
                log.error("Error al guardar BillerAttempt: [" + billerAttempt + "]. " + exx, exx);
            }
        }
    }

    @Override
    public List<BillerAttempt> getNextAttempts(Provider p) {
        return billerAttemptRepository.getNextAttempts(p);
    }

}
