import { of as observableOf,  Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';


export class PGMessageRequest {
   description: string;
   userAccount: string;
   searchProvider: boolean;
   susbcriptionRegistryId: number;
   constructor() { 
   }
} 

export class PGMessageResponse {
   meta: object;
   data: object;
   constructor() { 
   }
} 


//@Injectable()
export class BoHttpClient {

    protected httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      withCredentials: true
    };

    constructor() {}


    /**
      * Handle Http operation that failed.
      * Let the app continue.
      * @param operation - name of the operation that failed
      * @param result - optional value to return as the observable result
      */
    protected handleError<T> (operation = 'operation', result?: T) {
       return (error: any): Observable<T> => {

         // TODO: send the error to remote logging infrastructure
         console.error(error); // log to console instead

         // TODO: better job of transforming error for user consumption
         console.log(`${operation} failed: ${error.message}`);
         
         window.location.href = "/account/";

         // Let the app keep running by returning an empty result.
         return observableOf(result as T);
       };
    }
}

