/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model;

/**
 *
 * @author javier
 */
public enum Status {
    ENABLE, DISABLE, CREATED, PENDING, ACTIVE, REJECTED, REMOVED, BLOCKED
}
