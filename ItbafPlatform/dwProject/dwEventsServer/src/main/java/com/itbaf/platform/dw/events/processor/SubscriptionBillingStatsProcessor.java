/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events.processor;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.stats.model.BaseResumenElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.commons.stats.model.StatsMessage;
import com.itbaf.platform.paymenthub.commons.stats.model.SubscriptionBillingElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.SubscriptionRegistryElastic;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class SubscriptionBillingStatsProcessor extends StatsProcessor {

    private static final ElasticData.ElasticType elasticType = ElasticData.ElasticType.subscription_billing;

    @Inject
    public SubscriptionBillingStatsProcessor() {
    }

    @Override
    public Boolean process(String msg) throws Exception {
        // log.info("Procesando DW SubscriptionBilling: [" + msg + "]");

        StatsMessage smx = mapper.readValue(msg, StatsMessage.class);
        String mm = "dw_" + smx.elasticData.elasticType + "_" + smx.elasticData.elasticId;

        RLock lock = instrumentedObject.lockObject(mm, 60L);
        try {
            String dm = clientCache.getTemporalData(mm);
            if (dm != null) {
                if (smx.lastUpdateMillis < Long.parseLong(dm)) {
                    log.info("Ignorando DW SubscriptionBilling: [" + msg + "]");
                    return true;
                }
            }
            StatsMessage sm = new StatsMessage();
            SubscriptionBilling sb = subscriptionBillingService.findById(Long.parseLong(smx.elasticData.elasticId));
            if (sb == null) {
                log.info("No existe el registro en la base. [" + msg + "]");
                Thread.sleep(7000);
                sb = subscriptionBillingService.findById(Long.parseLong(smx.elasticData.elasticId));
                if (sb == null) {
                    log.warn("No existe el registro en la base. [" + msg + "]");
                    Thread.sleep(1000);
                    return null;
                }
            }
            log.info("SubscriptionBillingProcessor SubscriptionBilling: [" + sb + "]");
            sm.thread = smx.thread;
            SubscriptionBillingElastic sbe = new SubscriptionBillingElastic();
            sbe.chargedDate = sb.getChargedDate();
            sbe.currency = sb.getCurrency();
            sbe.elasticId = "subscription_billing_" + sb.getId().toString();
            sbe.fullAmount = sb.getFullAmount();
            sbe.netAmount = sb.getNetAmount();
            sbe.id = sb.getId().toString();
            sbe.sop = sb.getSop();
            sbe.sop.setIntegrationSettings(null);
            sbe.tariff = sbe.sop.getMainTariff(sbe.chargedDate);
            sbe.sop.setTariffs(null);
            Country c = new Country();
            c.setId(sbe.sop.getProvider().getCountry().getId());
            c.setCode(sbe.sop.getProvider().getCountry().getCode());
            c.setCurrency(sbe.sop.getProvider().getCountry().getCurrency());
            c.setName(sbe.sop.getProvider().getCountry().getName());
            sbe.sop.getProvider().setCountry(c);
            sbe.subscriptionDate = sb.getSubscriptionRegistry().getSubscriptionDate();
            sbe.subscriptionId = sb.getSubscriptionRegistry().getSubscription().getId();
            sbe.subscriptionRegistryId = sb.getSubscriptionRegistry().getId();
            sbe.userAccount = sb.getSubscriptionRegistry().getUserAccount();

            sm.elasticData = sbe;

            //Enviamos el SUBSCRIPTION_BILLING actualizado al Rabbit
            String json = mapper.writeValueAsString(sm);
            if (statsService.sendETLToRabbit(ElasticData.ElasticType.subscription_billing, json) != null) {
                //Creamos un evento de subscription_registry
                if (Boolean.TRUE.equals(statsService.sendToRabbit(ElasticData.ElasticType.subscription_registry, sb.getSubscriptionRegistry().getId().toString()))) {
                    SubscriptionRegistryElastic sre = getSubscriptionRegistryElastic(sb.getSubscriptionRegistry());
                    BaseResumenElastic bre = new BaseResumenElastic();
                    bre.functionType = BaseResumenElastic.FunctionType.COBRO;
                    bre.elasticId = sre.elasticId;
                    bre.fecha = sbe.chargedDate;
                    bre.subscriptionId = sre.subscriptionId;
                    bre.subscriptionRegistryId = Long.parseLong(sre.id);
                    bre.subscriptionRegistryElastic = sre;
                    bre.subscriptionBillingElastic = sbe;
                    StatsMessage sx = new StatsMessage();
                    sx.thread = smx.thread;
                    sx.elasticData = bre;
                    json = mapper.writeValueAsString(sx);
                    if (Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.base_resumen, json))) {
                        clientCache.setTemporalData(mm, sm.lastUpdateMillis.toString(), 1L);
                        return true;
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar SubscriptionBilling [" + msg + "]. " + ex, ex);
        } finally {
            instrumentedObject.unLockObject(lock);
        }

        return null;
    }

    @Override
    public ElasticData.ElasticType getElasticType() {
        return elasticType;
    }

}
