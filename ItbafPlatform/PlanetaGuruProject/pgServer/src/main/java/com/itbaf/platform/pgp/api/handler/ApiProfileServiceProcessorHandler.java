/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.paymenthub.commons.stats.model.StatsMessage;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Language;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.pgp.api.model.Birthdate;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.model.Product;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Friend;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Username;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import io.jsonwebtoken.impl.DefaultClaims;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ApiProfileServiceProcessorHandler extends ServiceProcessorHandler {

    private final RabbitMQProducer rabbitMQProducer;

    @Inject
    public ApiProfileServiceProcessorHandler(final RabbitMQProducer rabbitMQProducer) {
        this.rabbitMQProducer = rabbitMQProducer;
    }

    /**
     * @param sridTransaction true -> Busca el SR ID de la ultima suscripcion
     */
    public UserProfile getUserProfileFromUser(User client, User user, DefaultClaims defaultClaims, boolean clientData, boolean onlyActive, Boolean sridTransaction) {
        while (user.getPrincipal() != null) {
            user = user.getPrincipal();
        }

        UserProfile up = new UserProfile();
        up.setAddress(user.getProfileUserProperty().getAddress());
        up.setAgeRange(user.getProfileUserProperty().getAgeRange());

        if (user.getProfileUserProperty().getBirthdate() != null) {
            up.setBirthdate(new Birthdate(user.getProfileUserProperty().getBirthdate()));
        }

        boolean pEmail = false;
        boolean pPhone = false;
        try {
            if (!user.getCredentialUserProperty().getUsername().getEmails().isEmpty()) {
                up.setEmails(new HashSet());
                up.getEmails().addAll(user.getCredentialUserProperty().getUsername().getEmails().values());
                if (user.getCredentialUserProperty().getUsername().getPrimaryProcessed() == null || !user.getCredentialUserProperty().getUsername().getPrimaryProcessed()) {
                    pEmail = true;
                    boolean control = false;
                    for (Email e : up.getEmails()) {
                        if (e.getConfirmed() == null) {
                            e.setConfirmed(false);
                        } else if (e.getPrimary()) {
                            control = true;
                            break;
                        }
                    }
                    if (!control) {
                        for (Email e : up.getEmails()) {
                            if (e.getConfirmed()) {
                                e.setPrimary(true);
                                userService.saveOrUpdate(user);
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        }

        try {
            if (user.getProfileUserProperty().getFirstName() == null) {
                user.getProfileUserProperty().setFirstName("User");
                userService.saveOrUpdate(user);
            }
        } catch (Exception ex) {
        }

        up.setFirstName(user.getProfileUserProperty().getFirstName());
        up.setGender(user.getProfileUserProperty().getGender());

        if (defaultClaims != null) {
            try {
                String cc = defaultClaims.get("country", String.class);
                if (cc != null) {
                    up.setLocalCountry(countryService.findByCode(cc));
                }
            } catch (Exception ex) {
            }
        }

        Country caux;
        if (user.getProfileUserProperty().getCountry() == null) {
            if (up.getLocalCountry() != null) {
                log.warn("Usuario sin Country. Se asignara LocalCountry [" + up.getLocalCountry().getCode() + "]. User: [" + user + "]");
                up.setCountry(up.getLocalCountry());
                caux = up.getLocalCountry();
                user.getProfileUserProperty().setCountry(caux);
            } else {
                log.warn("Usuario sin Country. Se asignara AR por default. User: [" + user + "]");
                caux = countryService.findByCode("AR");
                user.getProfileUserProperty().setCountry(caux);
            }
            try {
                userService.saveOrUpdate(user);
            } catch (Exception ex) {
                log.fatal("No fue posible actualizar el Country para el user: [" + user + "]. " + ex, ex);
            }
        } else {
            caux = countryService.findByCode(user.getProfileUserProperty().getCountry().getCode());
        }

        up.setCountry(caux);
        try {
            if (user.getProfileUserProperty().getLanguage() == null) {
                if (caux.getLanguages() != null && !caux.getLanguages().isEmpty()) {
                    if (caux.getLanguages().size() > 1) {
                        Set<Language> list = new TreeSet();
                        list.addAll(caux.getLanguages());
                        user.getProfileUserProperty().setLanguage(list.iterator().next().getCode().toLowerCase().trim()
                                + "-" + caux.getCode().toUpperCase().trim());
                    } else {
                        user.getProfileUserProperty().setLanguage(caux.getLanguages().iterator().next().getCode().toLowerCase().trim()
                                + "-" + caux.getCode().toUpperCase().trim());
                    }
                    if (user.getProfileUserProperty().getLanguage() != null && "gn-AR".equals(user.getProfileUserProperty().getLanguage())) {
                        user.getProfileUserProperty().setLanguage("es-AR");
                    }
                    userService.saveOrUpdate(user);
                }
            }
            if (user.getProfileUserProperty().getLanguage() != null
                    && (user.getProfileUserProperty().getLanguage().length() < 3 || user.getProfileUserProperty().getLanguage().length() > 5)) {
                if (caux.getLanguages() != null && !caux.getLanguages().isEmpty()) {
                    if (caux.getLanguages().size() > 1) {
                        Set<Language> list = new TreeSet();
                        list.addAll(caux.getLanguages());
                        user.getProfileUserProperty().setLanguage(list.iterator().next().getCode().toLowerCase().trim()
                                + "-" + caux.getCode().toUpperCase().trim());
                    } else {
                        user.getProfileUserProperty().setLanguage(caux.getLanguages().iterator().next().getCode().toLowerCase().trim() + "-" + caux.getCode().toUpperCase().trim());
                    }
                    userService.saveOrUpdate(user);
                }
            }
            if (user.getProfileUserProperty().getLanguage() != null && user.getProfileUserProperty().getLanguage().contains("_")) {
                user.getProfileUserProperty().setLanguage(user.getProfileUserProperty().getLanguage().replace("_", "-"));
                userService.saveOrUpdate(user);
            }
        } catch (Exception ex) {
        }

        /* try {
              if ("es_419".equals(user.getProfileUserProperty().getLanguage()) || "es-419".equals(user.getProfileUserProperty().getLanguage())) {
                user.getProfileUserProperty().setLanguage("es-AR");
                userService.saveOrUpdate(user);
            }
            else if (user.getProfileUserProperty().getLanguage() != null && user.getProfileUserProperty().getLanguage().length() > 5) {
                log.fatal("Nuevo LANGUAGE: [" + user.getProfileUserProperty().getLanguage() + "]. Actualizar Codigo Fuente.");
                user.getProfileUserProperty().setLanguage("es_AR");
                userService.saveOrUpdate(user);
            }
        } catch (Exception ex) {
        }*/
        up.setLanguage(user.getProfileUserProperty().getLanguage());
        up.setLastName(user.getProfileUserProperty().getLastName());
        up.setMiddleName(user.getProfileUserProperty().getMiddleName());
        up.setOccupation(user.getProfileUserProperty().getOccupation());

        try {
            if (!user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                up.setPhones(new HashSet());
                up.getPhones().addAll(user.getCredentialUserProperty().getUsername().getMsisdns().values());
                if (user.getCredentialUserProperty().getUsername().getPrimaryProcessed() == null || !user.getCredentialUserProperty().getUsername().getPrimaryProcessed()) {
                    pPhone = true;
                    boolean control = false;
                    for (Msisdn e : up.getPhones()) {
                        if (e.getConfirmed() == null) {
                            e.setConfirmed(false);
                        } else if (e.getPrimary()) {
                            control = true;
                            break;
                        }
                    }
                    if (!control) {
                        for (Msisdn e : up.getPhones()) {
                            if (e.getConfirmed()) {
                                e.setPrimary(true);
                                userService.saveOrUpdate(user);
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        }

        if (pPhone || pEmail) {
            try {
                user.getCredentialUserProperty().getUsername().setPrimaryProcessed(true);
                userService.saveOrUpdate(user);
            } catch (Exception ex) {
            }
        }

        up.setPhotoURL(user.getProfileUserProperty().getPhotoURL());
        up.setState(user.getProfileUserProperty().getState());
        up.setUserId(user.getId());
        up.setUserPhoto(user.getProfileUserProperty().isUserPhoto());
        up.setZip(user.getProfileUserProperty().getZip());
        up.setSiteURL(user.getProfileUserProperty().getSiteURL());

        if (user.getCredentialUserProperty().getUsername().getAvatarURL() == null) {
            try {
                user.getCredentialUserProperty().getUsername().setAvatarURL(Username.DEFAULT_AVATAR_URL);
                userService.saveOrUpdate(user);
            } catch (Exception ex) {
            }
        }
        up.setAvatarURL(user.getCredentialUserProperty().getUsername().getAvatarURL());

        if (user.getCredentialUserProperty().getUsername().getNickname() == null) {
            try {
                user.getCredentialUserProperty().getUsername().setNickname(up.getFirstName().replace(" ", "_") + "_" + up.getUserId());
                userService.saveOrUpdate(user);
            } catch (Exception ex) {
            }
        }
        up.setNickname(user.getCredentialUserProperty().getUsername().getNickname());
        up.setNicknameEdited(user.getCredentialUserProperty().getUsername().getNicknameEdited());

        try {
            if (user.getUsers() != null) {
                up.setUserIds(new ArrayList());
                for (User u : user.getUsers()) {
                    up.getUserIds().add(u.getId());
                }
            }
        } catch (Exception ex) {
        }

        if (clientData) {
            try {
                User aux = client;

                while (aux.getPrincipal() != null) {
                    aux = aux.getPrincipal();
                }

                up.setClientData(user.getExternalDataUserProperty().getObjects().get(aux.getId().toString()));
            } catch (Exception ex) {
            }
        }

        if (user.getProfileUserProperty().getFriends() != null && !user.getProfileUserProperty().getFriends().isEmpty()) {
            up.setFriends(new HashSet());
            for (Friend f : user.getProfileUserProperty().getFriends().values()) {
                try {
                    User uf = userService.findById(f.getUserId());
                    Friend fa = uf.getProfileUserProperty().getFriends().get(user.getId());
                    if (fa == null) {
                        Status ss = null;
                        switch (f.getStatus()) {
                            case ACTIVE:
                                ss = Status.ACTIVE;
                                break;
                            case PENDING:
                                ss = Status.CREATED;
                                break;
                            case CREATED:
                                ss = Status.PENDING;
                                break;
                        }
                        if (ss != null) {
                            fa = new Friend();
                            fa.setUserId(user.getId());
                            fa.setStatus(ss);
                            uf.getProfileUserProperty().getFriends().put(fa.getUserId(), fa);
                            userService.saveOrUpdate(uf);
                        }
                    }
                    if (onlyActive && Status.ACTIVE.equals(f.getStatus())) {
                        fa = new Friend();
                        fa.setUserId(uf.getId());
                        fa.setCreatedDate(null);
                        fa.setStatus(null);
                        if (uf.getCredentialUserProperty().getUsername().getNickname() == null) {
                            try {
                                uf.getCredentialUserProperty().getUsername().setNickname(uf.getProfileUserProperty().getFirstName() + "_" + uf.getId());
                                userService.saveOrUpdate(uf);
                            } catch (Exception ex) {
                            }
                        }
                        fa.setNickname(uf.getCredentialUserProperty().getUsername().getNickname());
                        fa.setAvatarURL(uf.getCredentialUserProperty().getUsername().getAvatarURL());
                        up.getFriends().add(fa);
                    } else if (Status.ACTIVE.equals(f.getStatus()) || Status.PENDING.equals(f.getStatus())
                            || Status.REJECTED.equals(f.getStatus()) || Status.CREATED.equals(f.getStatus())) {
                        fa = new Friend();
                        fa.setUserId(uf.getId());
                        fa.setCreatedDate(null);
                        fa.setStatus(f.getStatus());
                        if (uf.getCredentialUserProperty().getUsername().getNickname() == null) {
                            try {
                                uf.getCredentialUserProperty().getUsername().setNickname(uf.getProfileUserProperty().getFirstName() + "_" + uf.getId());
                                userService.saveOrUpdate(uf);
                            } catch (Exception ex) {
                            }
                        }
                        fa.setNickname(uf.getCredentialUserProperty().getUsername().getNickname());
                        fa.setAvatarURL(uf.getCredentialUserProperty().getUsername().getAvatarURL());
                        up.getFriends().add(fa);
                    }
                } catch (Exception ex) {
                }
            }
        }
        if (Boolean.TRUE.equals(sridTransaction)) {
            User aux = client;

            while (aux.getPrincipal() != null) {
                aux = aux.getPrincipal();
            }

            Map<Long, SOP> sops = new HashMap();
            for (Product p : aux.getProducts()) {
                for (SOP sop : p.getSops()) {
                    sops.put(sop.getId(), sop);
                }
            }

            List<String> userAccounts = new ArrayList();
            if (!user.getCredentialUserProperty().getUsername().getEmails().isEmpty()) {
                for (Email m : user.getCredentialUserProperty().getUsername().getEmails().values()) {
                    if (m.getConfirmed()) {
                        userAccounts.add(m.getEmail());
                    }
                }
            }
            if (!user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                for (Msisdn m : user.getCredentialUserProperty().getUsername().getMsisdns().values()) {
                    if (m.getConfirmed()) {
                        userAccounts.add(m.getMsisdn());
                    }
                }
            }

            Subscription subscription = subscriptionService.getLastSubscription(userAccounts, sops.values());
            if (subscription != null && (new Date().getTime() - subscription.getSubscriptionDate().getTime() < (1000 * 60 * 60 * 1))) {
                up.setSrIdTransaction("SR_" + subscription.getSubscriptionRegistry().getId());
            }
        }

        return up;
    }

    public UserProfile setUserData(User client, User user, DefaultClaims defaultClaims, Object data) throws Exception {

        while (client.getPrincipal() != null) {
            client = client.getPrincipal();
        }

        if (data != null) {
            user.getExternalDataUserProperty().getObjects().put(client.getId().toString(), data);
            userService.saveOrUpdate(user);
        }

        UserProfile up = new UserProfile();
        up.setUserId(user.getId());

        try {
            up.setClientData(user.getExternalDataUserProperty().getObjects().get(client.getId().toString()));
        } catch (Exception ex) {
        }

        return up;
    }

    public UserProfile getUserProfileFromUserFriend(User client, User user, String userid) {
        Friend f = user.getProfileUserProperty().getFriends().get(Long.parseLong(userid));
        if (f == null || !Status.ACTIVE.equals(f.getStatus())) {
            return null;
        }
        user = userService.findById(Long.parseLong(userid));

        UserProfile up = getUserProfileFromUser(client, user, null, true, true, false);
        setPublicProfile(up);

        return up;
    }

    private void setPublicProfile(UserProfile up) {
        up.setEmails(null);
        up.setPhones(null);
        up.setFirstName(null);
        up.setMiddleName(null);
        up.setLastName(null);
        up.setBirthdate(null);
        up.setUserPhoto(null);
        up.setPhotoURL(null);
    }

    public Set<UserProfile> getProfileList(User client, List<Long> userList) {

        Set<UserProfile> upl = new HashSet();

        try {
            List<User> users = userService.findByIds(userList);
            if (users != null) {
                users.stream().map((user) -> {
                    while (user.getPrincipal() != null) {
                        user = user.getPrincipal();
                    }
                    return user;
                }).filter((user) -> (user.getCredentialUserProperty().getClient() == null || user.getCredentialUserProperty().getClient().getClientSecret() == null)).map((user) -> getUserProfileFromUser(client, user, null, true, false, false)).filter((up) -> (up != null)).map((up) -> {
                    setPublicProfile(up);
                    return up;
                }).map((up) -> {
                    up.setFriends(null);
                    return up;
                }).forEachOrdered((up) -> {
                    upl.add(up);
                });
            }
        } catch (Exception ex) {
            log.error("Error al obtener el UserProfileList. " + ex, ex);
        }

        log.info("UserList size: [" + userList.size() + "][" + upl.size() + "]");
        return upl;
    }

    public UserProfile getUserData(User client, User user, DefaultClaims defaultClaims, Object clientData) {
        while (client.getPrincipal() != null) {
            client = client.getPrincipal();
        }

        UserProfile up = new UserProfile();
        up.setUserId(user.getId());
        up.setClientData(user.getExternalDataUserProperty().getObjects().get(client.getId().toString()));

        return up;
    }

    public void sendNotificationContent(StatsMessage messageRequest) throws Exception {
        String json = mapper.writeValueAsString(messageRequest);
        rabbitMQProducer.messageSender(CommonFunction.PROCESS_DW_EVENTS_QUEUE + messageRequest.elasticData.elasticType, json);
    }

    public String getNicknameStatus(User user, UserProfile up) {
        if (up.getNickname() != null && up.getNickname().length() > 5) {
            up.setNickname(up.getNickname().replace("'", "").replace(".", "").replace("\"", "").replace("_", ""));
            if (up.getNickname().length() > 5) {
                user = userService.findByNickname(up.getNickname());
                if (user == null) {
                    return "available";
                }
                return "not_available";
            }
        }
        return null;
    }

    public String saveOrUpdateProfile(String path, User user, UserProfile up) {
        switch (path.toLowerCase()) {
            case "name":
                if (up.getFirstName() != null) {
                    up.setFirstName(up.getFirstName().trim());
                }
                if (up.getMiddleName() != null) {
                    up.setMiddleName(up.getMiddleName().trim());
                }
                if (up.getLastName() != null) {
                    up.setLastName(up.getLastName().trim());
                }
                user.getProfileUserProperty().setFirstName(up.getFirstName());
                user.getProfileUserProperty().setMiddleName(up.getMiddleName());
                user.getProfileUserProperty().setLastName(up.getLastName());
                try {
                    userService.saveOrUpdate(user);
                } catch (Exception ex) {
                }
                break;
            case "nickname":
                if (up.getNickname() != null) {
                    up.setNickname(up.getNickname().replace("'", "").replace(".", "").replace("\"", "").replace("_", ""));
                    if (up.getNickname().length() > 5 && userService.findByNickname(up.getNickname()) == null) {
                        user.getCredentialUserProperty().getUsername().setNickname(up.getNickname());
                        user.getCredentialUserProperty().getUsername().setNicknameEdited(true);
                        try {
                            userService.saveOrUpdate(user);
                        } catch (Exception ex) {
                        }
                    } else {
                        return "nickname:not_available";
                    }
                }
                break;
            case "personal":
                if (up.getBirthdate() != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        Date d = sdf.parse(up.getBirthdate().year + "-" + up.getBirthdate().month + "-" + up.getBirthdate().day + " 12:00:00");

                        Calendar cs = Calendar.getInstance();
                        cs.add(Calendar.YEAR, -103);
                        Calendar ce = Calendar.getInstance();
                        cs.add(Calendar.YEAR, -3);
                        if (cs.getTime().before(d) && ce.getTime().after(d)) {
                            user.getProfileUserProperty().setBirthdate(d);
                        }
                    } catch (ParseException ex) {
                    }
                }

                if (up.getGender() != null) {
                    user.getProfileUserProperty().setGender(up.getGender());
                }

                if (up.getCountry() != null && up.getCountry().getCode() != null
                        && !up.getCountry().getCode().equalsIgnoreCase(user.getProfileUserProperty().getCountry().getCode())) {
                    Country c = countryService.findByCode(up.getCountry().getCode());
                    if (c != null) {
                        user.getProfileUserProperty().setCountry(c);
                    }
                }

                if (up.getLanguage() != null && up.getLanguage().contains("-")) {
                    try {
                        String aux[] = up.getLanguage().split("-");
                        Country c = countryService.findByCode(aux[1]);
                        for (Language l : c.getLanguages()) {
                            if (aux[0].equalsIgnoreCase(l.getCode())) {
                                user.getProfileUserProperty().setLanguage(up.getLanguage());
                                break;
                            }
                        }
                    } catch (Exception ex) {
                    }
                }

                try {
                    userService.saveOrUpdate(user);
                } catch (Exception ex) {
                }

                break;
        }
        return null;
    }
}
