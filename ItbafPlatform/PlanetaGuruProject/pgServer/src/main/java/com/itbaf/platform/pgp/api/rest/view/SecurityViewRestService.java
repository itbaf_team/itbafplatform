package com.itbaf.platform.pgp.api.rest.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgp.api.handler.AccountViewServiceProcessorHandler;
import com.itbaf.platform.pgp.api.handler.SecurityViewServiceProcessorHandler;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import io.jsonwebtoken.impl.DefaultClaims;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;

@Path("/security")
@Singleton
@lombok.extern.log4j.Log4j2
public class SecurityViewRestService extends CommonRestService {

    private final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler;
    private final SecurityViewServiceProcessorHandler securityViewServiceProcessorHandler;

    @Inject
    public SecurityViewRestService(final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler,
            final SecurityViewServiceProcessorHandler securityViewServiceProcessorHandler) {
        this.accountViewServiceProcessorHandler = Validate.notNull(accountViewServiceProcessorHandler, "An AccountViewServiceProcessorHandler class must be provided");
        this.securityViewServiceProcessorHandler = Validate.notNull(securityViewServiceProcessorHandler, "A SecurityViewServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public Response security(@Context HttpServletRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.security");
        } catch (Exception ex) {
        }
        log.info("security. [" + request.getQueryString() + "]");

        return getSecurityPage(request);
    }

    @GET
    @Path("/edit/{path}")
    @Produces(MediaType.TEXT_HTML)
    public Response securityEdit(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            @QueryParam("ru") String redirectUri) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.security.edit." + path);
        } catch (Exception ex) {
        }
        log.info("securityEdit. path: [" + path + "] - redirectUri: [" + redirectUri + "]");

        switch (path) {
            case "username":
                try {
                    User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
                    User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
                    String productId = request.getAttribute(CommonAttribute.PRODUCT_ID).toString();
                    if (client != null && user != null) {
                        String redUri = request.getAttribute(CommonAttribute.U_REDIRECT_URI).toString();
                        String html = securityViewServiceProcessorHandler.getUsernamePage(client, user, productId, redUri);
                        if (html != null) {
                            String prvdr = null;
                            try {
                                DefaultClaims dc = (DefaultClaims) request.getAttribute(CommonAttribute.U_DEFAULT_CLAIMS);
                                prvdr = dc.get("prvdr", String.class);
                            } catch (Exception ex) {
                            }
                            html = accountViewServiceProcessorHandler.i18n(user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage(), prvdr, html);
                            return Response.status(Response.Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
                        }
                    }
                } catch (Exception ex) {
                }
                return Response.status(Response.Status.UNAUTHORIZED).build();
            case "password":
                try {
                    User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
                    User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
                    String redirectUriClient = (String) request.getAttribute(CommonAttribute.U_REDIRECT_URI);
                    String productId = (String) request.getAttribute(CommonAttribute.PRODUCT_ID);
                    if (client != null && user != null && redirectUriClient != null && productId != null) {
                        String url = securityViewServiceProcessorHandler.getPasswordUrl(client, user, redirectUriClient, productId);
                        if (url != null) {
                            java.net.URI location = new java.net.URI(url);
                            return Response.status(302).location(location).build();
                        }
                    }
                } catch (Exception ex) {
                }
                return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return getSecurityPage(request);
    }

    private Response getSecurityPage(HttpServletRequest request) {
        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            String productId = request.getAttribute(CommonAttribute.PRODUCT_ID).toString();
            if (client != null && user != null) {
                String redirectUri = request.getAttribute(CommonAttribute.U_REDIRECT_URI).toString();
                String html = securityViewServiceProcessorHandler.getUserSecurityHtml(client, user, productId, redirectUri);
                if (html != null) {
                    String prvdr = null;
                    try {
                        DefaultClaims dc = (DefaultClaims) request.getAttribute(CommonAttribute.U_DEFAULT_CLAIMS);
                        prvdr = dc.get("prvdr", String.class);
                    } catch (Exception ex) {
                    }
                    html = accountViewServiceProcessorHandler.i18n(user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage(), prvdr, html);
                    return Response.status(Response.Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar peticion. " + ex, ex);
        }

        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @POST
    @Path("/edit/username/{path}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editUsername(@Context HttpServletRequest request,
            @PathParam("path") final String path, UserProfile up) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.security.edit.username." + path);
        } catch (Exception ex) {
        }
        log.info("editUsername. UserProfile: [" + up + "]");
        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            String productId = request.getAttribute(CommonAttribute.PRODUCT_ID).toString();
            if (client != null && user != null && user.getId().equals(up.getUserId()) && up.getResult() != null) {
                boolean rest = false;
                switch (path) {
                    case "primary":
                        rest = securityViewServiceProcessorHandler.makePrimaryUsername(user, (String) up.getResult());
                        break;
                    case "remove":
                        rest = securityViewServiceProcessorHandler.removeUsername(user, (String) up.getResult());
                        break;
                    case "email":
                        if (up.getClientData() == null) {
                            rest = securityViewServiceProcessorHandler.sendEmail(client, user, (String) up.getResult(), productId);
                        } else {
                            rest = securityViewServiceProcessorHandler.verifyCodeEmail(user, (String) up.getResult(), (String) up.getClientData());
                        }
                        break;
                    case "msisdn":
                        if (up.getClientData() != null) {
                            rest = securityViewServiceProcessorHandler.verifyCodeMsisdn(user, (String) up.getResult(), (String) up.getClientData());
                        }
                        break;
                }

                if (rest) {
                    rm.meta.code = 200;
                    rm.meta.status = Meta.Status.ok;
                    rm.data.object = rest;
                }
            } else {
                rm.data.message = "Solicitud invalida.";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_request;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al cancelar suscription. UserProfile: [" + up + "]" + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @PUT
    @Path("/edit/username/{path}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUsername(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            UserProfile up) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.security.edit.username.add." + path);
        } catch (Exception ex) {
        }
        log.info("addUsername. UserProfile: [" + up + "]");
        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            if (client != null && user != null && user.getId().equals(up.getUserId()) && up.getResult() != null) {
                boolean rest = false;
                switch (path) {
                    case "email":
                        rest = securityViewServiceProcessorHandler.addUsernameEmail(user, (String) up.getResult());
                        break;
                    case "msisdn":
                        if (up.getClientData() != null) {
                            rest = securityViewServiceProcessorHandler.addUsernameMsisdn(user, (String) up.getResult(), (String) up.getClientData());
                        }
                        break;
                }

                if (rest) {
                    rm.meta.code = 200;
                    rm.meta.status = Meta.Status.ok;
                    rm.data.object = rest;
                }
            } else {
                rm.data.message = "Solicitud invalida.";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_request;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al cancelar suscription. UserProfile: [" + up + "]" + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }
}
