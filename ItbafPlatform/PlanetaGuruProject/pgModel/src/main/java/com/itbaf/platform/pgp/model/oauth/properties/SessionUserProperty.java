/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.pgp.model.oauth.properties.session.SessionGP;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author javier
 */
@lombok.Setter
@lombok.Getter
public class SessionUserProperty extends UserProperty implements Serializable {

    private static final long serialVersionUID = 20181207172404L;

    //<UserId, SessionGP>
    private Map<String, SessionGP> sessions;

    public SessionUserProperty() {
        super(Topic.SESSION);
        sessions = new HashMap();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("sessions", sessions)
                .omitNullValues().toString();
    }
}
