/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.sms.AdapterSmsLogReceived;

/**
 *
 * @author javier
 */
public interface AdapterSmsLogReceivedService {

    public void saveOrUpdate(AdapterSmsLogReceived adapterSmsLogReceived) throws Exception;
}
