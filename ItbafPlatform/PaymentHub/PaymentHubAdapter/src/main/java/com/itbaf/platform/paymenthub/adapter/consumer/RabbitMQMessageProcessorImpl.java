/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adapter.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.adapter.processor.AdapterProcessor;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQMessageProcessorImpl implements RabbitMQMessageProcessor {

    private final ObjectMapper mapper;
    private final InstrumentedObject instrumentedObject;
    private final AdapterProcessor adProcessor;

    @AssistedInject
    public RabbitMQMessageProcessorImpl(final ObjectMapper mapper,
            final InstrumentedObject instrumentedObject,
            @Assisted final AdapterProcessor adProcessor) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.adProcessor = Validate.notNull(adProcessor, "An AdapterProcessor class must be provided");
    }

    @Override
    public Boolean processMessage(String message) {
        try {
            String tid = Thread.currentThread().getName() + ".sms.mt." + adProcessor.adapterType;
            Thread.currentThread().setName(tid);
        } catch (Exception ex) {
        }

        try {
            return adProcessor.process(message);
        } catch (Exception ex) {
            log.error("Error al procesar MT: [" + message + "]. " + ex, ex);
        }
        return null;
    }

}
