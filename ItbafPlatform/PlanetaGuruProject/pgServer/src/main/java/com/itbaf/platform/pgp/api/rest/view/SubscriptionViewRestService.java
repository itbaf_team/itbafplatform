package com.itbaf.platform.pgp.api.rest.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgp.api.handler.AccountViewServiceProcessorHandler;
import com.itbaf.platform.pgp.api.handler.SubscriptionViewServiceProcessorHandler;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import io.jsonwebtoken.impl.DefaultClaims;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;

@Path("/subscriptions")
@Singleton
@lombok.extern.log4j.Log4j2
public class SubscriptionViewRestService extends CommonRestService {

    private final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler;
    private final SubscriptionViewServiceProcessorHandler subscriptionViewServiceProcessorHandler;

    @Inject
    public SubscriptionViewRestService(final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler,
            final SubscriptionViewServiceProcessorHandler subscriptionViewServiceProcessorHandler) {
        this.accountViewServiceProcessorHandler = Validate.notNull(accountViewServiceProcessorHandler, "An AccountViewServiceProcessorHandler class must be provided");
        this.subscriptionViewServiceProcessorHandler = Validate.notNull(subscriptionViewServiceProcessorHandler, "A SubscriptionViewServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public Response subscriptions(@Context HttpServletRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.subscriptions");
        } catch (Exception ex) {
        }
        log.info("subscriptions. [" + request.getQueryString() + "]");

        return getSubscPage(request);
    }

    private Response getSubscPage(HttpServletRequest request) {
        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            String productId = request.getAttribute(CommonAttribute.PRODUCT_ID).toString();

            String qs = request.getParameter("origin");
            boolean isOrigin = (qs != null && "portal".equals(qs));//Viene del portal
            if (client != null && user != null) {
                String lg = user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage();
                String redirectUri = request.getAttribute(CommonAttribute.U_REDIRECT_URI).toString();
                
                String html = subscriptionViewServiceProcessorHandler.getUserSubscriptionHtml(client, user, lg, productId, redirectUri, isOrigin);
                if (html != null) {
                    String prvdr = null;
                    try {
                        DefaultClaims dc = (DefaultClaims) request.getAttribute(CommonAttribute.U_DEFAULT_CLAIMS);
                        prvdr = dc.get("prvdr", String.class);
                    } catch (Exception ex) {
                    }
                    html = accountViewServiceProcessorHandler.i18n(lg, prvdr, html);
                    return Response.status(Response.Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar peticion. " + ex, ex);
        }

        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @DELETE
    @Path("/cancel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSubscription(@Context HttpServletRequest request,
            UserProfile up) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.subscriptions.cancel");
        } catch (Exception ex) {
        }
        log.info("deleteSubscription. UserProfile: [" + up + "]");
        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            if (client != null && user != null && user.getId().equals(up.getUserId()) && up.getResult() != null) {
                Holder<String> urlRedirect = new Holder();
                boolean rest = subscriptionViewServiceProcessorHandler.cancelSubscription(user, Long.parseLong((String) up.getResult()), urlRedirect);
                log.info("Cancel subscription response" + rest + ", user id "+up.getUserId());
                if (rest) {
                    rm.meta.code = 200;
                    rm.meta.status = Meta.Status.ok;
                    rm.data.object = rest;
                    rm.data.urlRedirect = urlRedirect.value;
                    log.info("Code 200 user id "+up.getUserId());
                }
            } else {
                rm.data.message = "Solicitud invalida.";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_request;
                log.info("Code 400 user id "+up.getUserId());
            }
        } catch (Exception ex) {
            rm.data.message = "Error al cancelar suscription. UserProfile: [" + up + "]" + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            log.info("Code 500 user id "+up.getUserId());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

}
