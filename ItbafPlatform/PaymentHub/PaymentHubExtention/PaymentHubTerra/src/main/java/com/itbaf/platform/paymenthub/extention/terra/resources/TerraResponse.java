/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.terra.resources;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author JF
 */
public class TerraResponse implements Serializable {

    private static final long serialVersionUID = 20181207171801L;

    public TerraResponse[] RESPONSE;

    public Integer ID;
    public String TOKEN;
    public Integer EXPIRES;
    public String STATUS;
    public String MESSAGE;
    public Integer SHORTCODE;
    public String VERIFYCODE;

    public String SERVICE;
    public String SERVICENAME;
    public BigDecimal PRICE;
    public String FORMATPRICE;
    public String CURRENCY;
    public String ABBREVIATION;
}
