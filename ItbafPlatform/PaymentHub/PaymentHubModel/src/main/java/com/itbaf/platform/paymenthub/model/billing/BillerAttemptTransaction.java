/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.billing;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.BaseEntity;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author JF
 */
@Entity(name = "BILLER_ATTEMPT_TRANSACTION")
@Table(name = "BILLER_ATTEMPT_TRANSACTION", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(false)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class BillerAttemptTransaction extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 20180726173201L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "SUBSCRIPTION_REGISTRY_ID")
    private Long subscriptionRegistryId;

    @Column(name = "SOP_ID")
    private Long sopId;

    @Column(name = "TRANSACTION_ID")
    private String transactionId;

    @Type(type = "json")
    @Column(name = "EVENT", columnDefinition = "json")
    private Object event;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("subscriptionRegistryId", subscriptionRegistryId)
                .add("sopId", sopId)
                .add("transactionId", transactionId)
                .add("event", event)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BillerAttemptTransaction other = (BillerAttemptTransaction) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
