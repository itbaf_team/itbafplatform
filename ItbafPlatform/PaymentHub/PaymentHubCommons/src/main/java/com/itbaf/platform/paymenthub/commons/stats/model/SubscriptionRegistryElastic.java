/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.stats.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.AdnetworkNotifier;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author JF
 */
public class SubscriptionRegistryElastic extends ElasticData implements Serializable {

    private static final long serialVersionUID = 20181029094901L;

    public SubscriptionRegistryElastic() {
        super(ElasticData.ElasticType.subscription_registry);
    }
    public Long subscriptionId;
    public String userAccount;
    public SOP sop;
    public String channelIn;
    public String channelOut;
    public String externalUserAccount;
    public String externalCustomer;
    public String externalId;
    public Date subscriptionDate;
    public Date unsubscriptionDate;
    public String originSubscription;
    public String originUnsubscription;
    public Date syncDate;
    public Object adnetworkTracking;
    public Extra extra;
    public Date firstCharged;
    public Date lastCharged;
    public BigInteger minSubscriptionBillingId;
    public BigInteger charges = BigInteger.ZERO;
    public BigDecimal totalFullAmount = BigDecimal.ZERO;
    public BigDecimal totalNetAmount = BigDecimal.ZERO;
    public Tariff tariff;
    public AdnetworkNotifier adnetworkNotifier;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userAccount", userAccount)
                .add("channelIn", channelIn)
                .add("channelOut", channelOut)
                .omitNullValues().toString();
    }
}
