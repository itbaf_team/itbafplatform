/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.commons.resources;

import com.google.inject.Inject;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.pgp.model.oauth.User;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.redisson.api.RMapCache;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ClientCache {

    private final InstrumentedObject instrumentedObject;

    @Inject
    public ClientCache(final InstrumentedObject instrumentedObject) {
        this.instrumentedObject = instrumentedObject;
    }

    public void setTemporalData(final String key, final String data, long ttlMinutes) {

        if (key == null || data == null) {
            return;
        }

        RMapCache<String, String> map = instrumentedObject.redis.getMapCache("temporalDataCache");
        log.info("setTemporalData Cache: [" + key + "][" + data + "]");
        map.put(key, data, ttlMinutes, TimeUnit.MINUTES);
    }

    public String getTemporalData(final String key) {

        RMapCache<String, String> map = instrumentedObject.redis.getMapCache("temporalDataCache");
        String data = map.get(key);

        log.info("getTemporalData Cache: [" + key + "][" + data + "]");
        return data;
    }

    public void setClientParams(final User user, final String key, final Map<String, String> params) {

        if (user == null || key == null || params == null) {
            return;
        }

        RMapCache<String, Map<String, String>> map = instrumentedObject.redis.getMapCache("clientParamsMapCache");
        String mainKey = user.getId() + "_" + key;
        log.info("setClientParams Cache: [" + mainKey + "][" + Arrays.toString(params.entrySet().toArray()) + "]");
        map.put(mainKey, params, 2, TimeUnit.HOURS);
    }

    public Map<String, String> getClientParams(final User user, final String key) {
        return getClientParams(user.getId() + "_" + key);
    }

    public Map<String, String> getClientParams(final String key) {

        RMapCache<String, Map<String, String>> map = instrumentedObject.redis.getMapCache("clientParamsMapCache");
        Map<String, String> params = map.get(key);

        log.info("getClientParams Cache: [" + key + "][" + (params == null ? params : Arrays.toString(params.entrySet().toArray())) + "]");
        return params;
    }

    public InstrumentedObject getInstrumentedObject() {
        return instrumentedObject;
    }

}
