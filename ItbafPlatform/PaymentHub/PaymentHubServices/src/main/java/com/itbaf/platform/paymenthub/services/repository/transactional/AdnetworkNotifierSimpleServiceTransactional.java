/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.model.AdnetworkNotifierSimple;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPAAdnetworkNotifierSimpleRepository;
import com.itbaf.platform.paymenthub.services.repository.AdnetworkNotifierSimpleService;
import com.itbaf.platform.paymenthub.services.stats.StatsService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class AdnetworkNotifierSimpleServiceTransactional implements AdnetworkNotifierSimpleService {

    private final StatsService statsService;
    private final JPAAdnetworkNotifierSimpleRepository adnetworkNotifierSimpleRepository;

    @Inject
    public AdnetworkNotifierSimpleServiceTransactional(final StatsService statsService,
            final JPAAdnetworkNotifierSimpleRepository adnetworkNotifierSimpleRepository) {
        this.statsService = Validate.notNull(statsService, "A StatsService class must be provided");
        this.adnetworkNotifierSimpleRepository = Validate.notNull(adnetworkNotifierSimpleRepository, "A JPAAdnetworkNotifierSimpleRepository class must be provided");
    }

    @Override
    public AdnetworkNotifierSimple findById(String id) throws Exception {
        return adnetworkNotifierSimpleRepository.findById(id);
    }

    @Override
    public void saveOrUpdate(AdnetworkNotifierSimple adnetworkNotifier) throws Exception {

        try {
            adnetworkNotifierSimpleRepository.merge(adnetworkNotifier);
        } catch (Exception ex) {
            try {
                adnetworkNotifierSimpleRepository.save(adnetworkNotifier);
            } catch (Exception exx) {
                log.error("Error al guardar AdnetworkNotifierSimple: [" + adnetworkNotifier + "]. " + exx, exx);
            }
        }

        try {
            statsService.sendToRabbit(ElasticData.ElasticType.adnetwork_notifier_simple, adnetworkNotifier.getTrackingId());
        } catch (Exception ex) {
            log.error("NO fue posible enviar a stats-rabbit: [" + ElasticData.ElasticType.adnetwork_notifier_simple + "]. [" + adnetworkNotifier.getTrackingId() + "]. " + ex, ex);
        }
    }

}
