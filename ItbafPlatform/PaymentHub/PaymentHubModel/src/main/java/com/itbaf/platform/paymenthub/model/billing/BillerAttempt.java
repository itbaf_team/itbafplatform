/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.billing;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.BaseEntity;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author JF
 */
@Entity(name = "BILLER_ATTEMPT")
@Table(name = "BILLER_ATTEMPT", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class BillerAttempt extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 20180724142901L;

    @Id
    @Column(name = "SUBSCRIPTION_REGISTRY_ID", nullable = false, unique = true)
    private Long id;

    @OneToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_ATTEMPT")
    private Date lastAttempt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NEXT_ATTEMPT")
    private Date nextAttempt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_CHARGED")
    private Date lastCharged;

    @Column(name = "TARGET_AMOUNT")
    private BigDecimal targetAmount;//Cual es el valor a cobrar
    @Column(name = "TOTAL_AMOUNT")
    private BigDecimal totalAmount;//Cuanto se ha ido cobrando
    
    @Type(type = "json")
    @Column(name = "EXTRA", columnDefinition = "json")
    private Extra extra;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("sop", sop)
                .add("lastAttempt", lastAttempt)
                .add("nextAttempt", nextAttempt)
                .add("lastCharged", lastCharged)
                .add("targetAmount", targetAmount)
                .add("totalAmount", totalAmount)
                .add("extra", extra)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.id);
        hash = 61 * hash + Objects.hashCode(this.sop);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BillerAttempt other = (BillerAttempt) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.sop, other.sop)) {
            return false;
        }
        return true;
    }

}
