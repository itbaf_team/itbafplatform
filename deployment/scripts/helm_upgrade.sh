#!/bin/bash

set -e

#NAMESPACE=ph
#NAMESPACE=pgproject
NAMESPACE=bo
CHART=planetaguru/itbafplatform
GIT_SHA=c38265a9a804a4c3ce78e5519804912b2f5a96da

cd ../..

for RELEASE in $(cat ./deployment/scripts/releases_${NAMESPACE}.list);
do
    helm upgrade --install $RELEASE \
        $CHART \
        --set "image.tag=${GIT_SHA}" \
        -f ./deployment/config/${RELEASE}/chart-values-${RELEASE}-pr.yaml \
        --force \
        --namespace ${NAMESPACE} \
        --description ${GIT_SHA};
done
