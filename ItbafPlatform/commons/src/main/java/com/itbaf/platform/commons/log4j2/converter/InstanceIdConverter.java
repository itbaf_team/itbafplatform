/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.log4j2.converter;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.*;

/**
 *
 * @author javier
 */
@Plugin(name = "InstanceIdConverter", category = "Converter")
@ConverterKeys({"instanceId"})
public class InstanceIdConverter extends LogEventPatternConverter {

    private static final InstanceIdConverter instanceIdConverter = new InstanceIdConverter("instanceId", "instanceId");
    private String instanceId = "0.0";

    protected InstanceIdConverter(String name, String style) {
        super(name, style);
    }

    public static InstanceIdConverter newInstance(String[] options) {
        return instanceIdConverter;
    }

    @Override
    public void format(LogEvent event, StringBuilder toAppendTo) {
        toAppendTo.append(getInstanceId());
    }

    protected String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

}
