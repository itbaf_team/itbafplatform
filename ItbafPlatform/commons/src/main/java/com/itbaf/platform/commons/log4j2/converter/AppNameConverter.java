/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.log4j2.converter;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.pattern.*;

/**
 *
 * @author javier
 */
@Plugin(name = "AppNameConverter", category = "Converter")
@ConverterKeys({"appName"})
public class AppNameConverter extends LogEventPatternConverter {

    private static final AppNameConverter appNameConverter = new AppNameConverter("appName", "appName");
    private String appName = "-";

    protected AppNameConverter(String name, String style) {
        super(name, style);
    }

    public static AppNameConverter newInstance(String[] options) {
        return appNameConverter;
    }

    @Override
    public void format(LogEvent event, StringBuilder toAppendTo) {
        toAppendTo.append(getAppName());
    }

    protected String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

}
