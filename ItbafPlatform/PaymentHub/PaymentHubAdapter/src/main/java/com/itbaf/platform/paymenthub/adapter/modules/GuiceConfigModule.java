/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adapter.modules;

import com.google.inject.multibindings.Multibinder;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.modules.GuiceDefaultConfigModule;
import com.itbaf.platform.paymenthub.adapter.processor.AdapterProcessor;
import com.itbaf.platform.paymenthub.adapter.processor.MoAdapterProcessor;
import com.itbaf.platform.paymenthub.adapter.processor.MtAdapterProcessor;
import com.itbaf.platform.paymenthub.repository.module.GuiceRepositoryConfigModule;
import com.itbaf.platform.paymenthub.services.module.GuiceServiceConfigModule;
import com.itbaf.platform.rmq.module.GuiceRabbitMQConfigModule;
import java.util.Arrays;
import java.util.Map;

/**
 *
 * @author jordonez
 */
public class GuiceConfigModule extends GuiceDefaultConfigModule {

    public GuiceConfigModule(final Map<String, String> args, CommonDaemon commonDaemon) {
        super(args,commonDaemon);
    }

    @Override
    protected void configure() {
        super.configure();

        //Binds        
        final Multibinder<AdapterProcessor> multi = Multibinder.newSetBinder(binder(), AdapterProcessor.class);
        multi.addBinding().to(MtAdapterProcessor.class).asEagerSingleton();
        multi.addBinding().to(MoAdapterProcessor.class).asEagerSingleton();

        //Modulos de aplicaciones
        install(new GuiceRepositoryConfigModule());
        install(new GuiceServiceConfigModule());
        install(new GuiceConsumersConfigModule());
        install(new GuiceRabbitMQConfigModule());
        install(new GuiceRestServicesConfigModule());

        log.info("Configuracion iniciada. " + Arrays.toString(args.entrySet().toArray()));
    }

}
