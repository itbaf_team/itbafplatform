/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.messaging;

/**
 *
 * @author javier
 */
public enum MessageType {
    SUBSCRIPTION_PENDING,
    SUBSCRIPTION_ACTIVE,
    SUBSCRIPTION_REMOVED, //Unsubscription
    SUBSCRIPTION_CANCELLATION,
    SUBSCRIPTION_BLACKLIST,
    SUBSCRIPTION_BY_BILLING,
    SUBSCRIPTION_TRIAL,
    SUBSCRIPTION_RETENTION,
    BILLING_TOTAL,
    BILLING_PARTIAL,
    BILLING_PENDING,
    ONDEMAND_TOTAL
}
