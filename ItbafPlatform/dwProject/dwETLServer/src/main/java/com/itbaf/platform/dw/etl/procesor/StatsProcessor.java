/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.etl.procesor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.rmq.consumer.MessageProcessor;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public abstract class StatsProcessor implements MessageProcessor {

    @Inject
    protected SubscriptionManager subscriptionManager;
    @Inject
    protected InstrumentedObject instrumentedObject;
    @Inject
    protected ObjectMapper mapper;
    @Inject
    protected RequestClient requestClient;
    @Inject
    protected SubscriptionRegistryService subscriptionRegistryService;

    public abstract ElasticData.ElasticType getElasticType();

    public abstract Boolean process(String msg) throws Exception;

}
