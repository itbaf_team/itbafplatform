
package org.csapi.schema.parlayx.subscribeproduct.v1_0;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.csapi.schema.parlayx.subscribeproduct.v1_0 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.csapi.schema.parlayx.subscribeproduct.v1_0
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DialectInfo }
     * 
     */
    public DialectInfo createDialectInfo() {
        return new DialectInfo();
    }

    /**
     * Create an instance of {@link NamedParameterList }
     * 
     */
    public NamedParameterList createNamedParameterList() {
        return new NamedParameterList();
    }

    /**
     * Create an instance of {@link LangInfo }
     * 
     */
    public LangInfo createLangInfo() {
        return new LangInfo();
    }

    /**
     * Create an instance of {@link UserID }
     * 
     */
    public UserID createUserID() {
        return new UserID();
    }

    /**
     * Create an instance of {@link SubInfoType }
     * 
     */
    public SubInfoType createSubInfoType() {
        return new SubInfoType();
    }

    /**
     * Create an instance of {@link ConsumeHistoryInfoType }
     * 
     */
    public ConsumeHistoryInfoType createConsumeHistoryInfoType() {
        return new ConsumeHistoryInfoType();
    }

    /**
     * Create an instance of {@link NamedParameter }
     * 
     */
    public NamedParameter createNamedParameter() {
        return new NamedParameter();
    }

    /**
     * Create an instance of {@link SubScriptionInfoType }
     * 
     */
    public SubScriptionInfoType createSubScriptionInfoType() {
        return new SubScriptionInfoType();
    }

    /**
     * Create an instance of {@link ConsumeRefundHistoryInfo }
     * 
     */
    public ConsumeRefundHistoryInfo createConsumeRefundHistoryInfo() {
        return new ConsumeRefundHistoryInfo();
    }

}
