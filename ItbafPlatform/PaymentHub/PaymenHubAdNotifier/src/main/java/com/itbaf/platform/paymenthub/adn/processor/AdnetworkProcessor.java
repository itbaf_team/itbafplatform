/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adn.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.adn.resources.AdnetworkNotificationType;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage.NotificationType;
import com.itbaf.platform.paymenthub.model.AdnetworkNotifier;
import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.AdnetworkNotifierService;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.paymenthub.services.stats.StatsService;
import com.itbaf.platform.rmq.consumer.MessageProcessor;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public abstract class AdnetworkProcessor implements MessageProcessor {

    @Inject
    protected SubscriptionManager subscriptionManager;
    @Inject
    protected InstrumentedObject instrumentedObject;
    @Inject
    protected ObjectMapper mapper;
    @Inject
    protected AdnetworkNotifierService adnetworkNotifierService;
    @Inject
    protected PHProfilePropertiesService profilePropertiesService;
    @Inject
    protected RequestClient requestClient;
    @Inject
    protected SubscriptionRegistryService subscriptionRegistryService;
    @Inject
    protected StatsService statsService;

    public abstract NotificationType getNotificationType();

    public abstract Boolean process(String msg) throws Exception;

    protected Boolean process(Long srId, Subscription.Status status, SubscriptionBilling sb) {
        RLock lock = null;
        try {
            lock = instrumentedObject.lockObject("Adn-SR-" + srId, 60);
            SubscriptionRegistry sr = subscriptionManager.getSubscriptionRegistry(srId);
            if (sr == null) {
                throw new Exception("No existe un SubscriptionRegistry para: [" + srId + "]");
            }

            if (sr.getAdnetworkTracking() == null) {
                return true;
            }
            LinkedHashMap<String, Object> adn = null;
            ArrayList<String> adnTrackingString = null;
            String adnNotTypeString = null;
            String source = null;
            AdnetworkNotificationType type = null;
            try {
                adn = (LinkedHashMap<String, Object>) sr.getAdnetworkTracking();
                adnTrackingString = (ArrayList<String>) adn.get("adnTrackingId");
                source = (String) adn.get("source");
                type = mapper.convertValue(adn.get("adnNotType"), AdnetworkNotificationType.class);
            } catch (Exception ex) {
                if (adn != null) {
                    try {
                        adnNotTypeString = (String) adn.get("adnNotType");
                    } catch (Exception exx) {
                    }
                }
            }

            if (adnTrackingString == null || source == null || (type == null && adnNotTypeString == null)) {
                return true;
            }

            AdnetworkNotifier an = adnetworkNotifierService.findById(srId);
            if (an == null) {
                an = new AdnetworkNotifier();
                an.setId(srId);
            }

            if (an.getNotificationSend()) {
                //Notificacion ya enviada a la ADN
                return true;
            }

            if (type == null) {
                if (adnNotTypeString == null || adnNotTypeString.isEmpty()) {
                    return true;
                }
                try {
                    if (!adnNotTypeString.startsWith("{")) {
                        adnNotTypeString = "{\"subscription\":\"" + adnNotTypeString + "\"}";
                    }
                    type = mapper.readValue(adnNotTypeString, AdnetworkNotificationType.class);
                } catch (Exception ex) {
                    log.error("Error al procesar adnNotTypeString: [" + adnNotTypeString + "]. " + ex);
                    return null;
                }
            }

            if (System.currentTimeMillis() - sr.getSubscriptionDate().getTime() > type.ttl) {
                //Notificacion fuera de rango de tiempo
                return true;
            }
            
            if (Subscription.Status.ADN_LOCKED.equals(an.getStatus())) {
                //Notificacion para ADN locked
                return true;
            }

            if (Subscription.Status.RETENTION.equals(status)) {
                //Notificacion para RETENTION
                return true;
            }

            if (status != null) {
                //Notificacion de Suscripcion
                switch (status) {
                    case ACTIVE:
                        if (type.lastSubs != null) {
                            Long quantity = type.lastSubs * 24 * 60 * 60 * 1000L;
                            SubscriptionRegistry srr = subscriptionRegistryService.getLastRegisterBySubscription(sr.getSubscription().getId(), true);
                            if (srr.getId().equals(sr.getId()) || (sr.getSubscriptionDate().getTime() - srr.getSubscriptionDate().getTime() >= quantity)) {
                                an.setStatus(Subscription.Status.ACTIVE);
                            } else {
                                an.setStatus(Subscription.Status.ADN_LOCKED);//Fuera de tiempo...
                            }
                        } else {
                            an.setStatus(Subscription.Status.ACTIVE);
                        }
                        break;
                    case PENDING:
                        if (Subscription.Status.ACTIVE.equals(an.getStatus())) {
                            log.warn("Hey.. se pretende cambiar el status de [" + an.getStatus() + "] a [" + status + "] para SubscriptionRegistry: [" + sr + "]");
                        } else {
                            an.setStatus(status);
                        }
                        break;
                    case BLACKLIST:
                    case CANCELLED:
                    case LOCKED:
                    case REMOVED:
                        an.setStatus(status);
                        break;
                }
            } else if (!Subscription.Status.ADN_LOCKED.equals(an.getStatus())) {
                //Notificacion de Cobro
                if (type.lastSubs != null) {
                    Long quantity = type.lastSubs * 24 * 60 * 60 * 1000L;
                    SubscriptionRegistry srr = subscriptionRegistryService.getLastRegisterBySubscription(sr.getSubscription().getId(), true);
                    if (srr.getId().equals(sr.getId()) || (sr.getSubscriptionDate().getTime() - srr.getSubscriptionDate().getTime() >= quantity)) {
                        an.setStatus(Subscription.Status.ACTIVE);
                    } else {
                        an.setStatus(Subscription.Status.ADN_LOCKED);//Fuera de tiempo...
                    }
                }
                if (!Subscription.Status.ADN_LOCKED.equals(an.getStatus())) {
                    an.setStatus(Subscription.Status.ACTIVE);
                    if (type.billPerce != null) {
                        Tariff t = sr.getSop().getMainTariff();
                        if (t != null) {
                            BigDecimal fullAmount = t.getFullAmount().multiply(type.billPerce).divide(new BigDecimal(100));
                            if (sb.getFullAmount().compareTo(fullAmount) >= 0) {
                                an.setCharged(sb.getFullAmount());
                            }
                        } else {
                            log.fatal("No existe un valor de Tariff para: [" + sr.getSop() + "]");
                        }
                    } else {
                        an.setCharged(sb.getFullAmount());
                    }
                }
            }

            boolean sent = false;
            if (type.subscription != null) {
                switch (type.subscription.toLowerCase().trim()) {
                    case "pends":
                        sent = true;
                        break;
                    case "subs":
                        if (Subscription.Status.ACTIVE.equals(an.getStatus())) {
                            sent = true;
                        }
                        break;
                    case "subsbill":
                        if (Subscription.Status.ACTIVE.equals(an.getStatus()) && an.getCharged() != null && an.getCharged().compareTo(BigDecimal.ZERO) > 0) {
                            sent = true;
                        }
                        break;
                    default:
                        log.error("No existe un CASE para AdnetworkNotificationType: [" + type + "]. ");
                }
            } else {
                log.error("No existe un AdnetworkNotificationType para adnNotType: [" + adnNotTypeString + "]. ");
                return null;
            }

            if (sent) {
                String url = null;
                try {
                    url = profilePropertiesService.getCommonProperty("adnetwork." + source + ".url");
                    log.info("Armado de URL Adnetwork " + url);
                    int i = 0;
                    for (String id : adnTrackingString) {
                        url = url.replace("<ID" + i + ">", id);
                        i++;
                    }
                    log.info("URL definitiva Adnetwork " + url);
                    String result = requestClient.requestOKGet(url);
                    log.info("Resultado del requets OK Adnetwork" + result);
                    if (result != null) {
                        log.info("Adnetwork [" + url + "] response: [" + result + "]");

                        if (result.contains("GIF89a")){
                            log.info("Adnetwork tipo de Respuesta GIF " + result);
                            an.setResponse("{\"result\":{\"code\":201,\"message\":\"OK\"}}");
                        }else {
                            an.setResponse(result);
                        }

                        an.setNotificationSend(true);
                    } else {
                        log.info("Adnetwork-Return-NULL [" + url + "] response: [" + result + "]");
                        return null;
                    }
                } catch (Exception ex) {
                    log.error("Error al notificar a: [" + source + "][" + url + "]. " + ex.getCause().getCause(), ex.getCause().getCause());
                }
            }

            try {
                log.info("Antes de guardar la ad:["+an+"]");
                adnetworkNotifierService.saveOrUpdate(an);
                try {
                    statsService.sendToRabbit(ElasticData.ElasticType.subscription_registry, sr.getId().toString());
                } catch (Exception ex) {
                    log.error("NO fue posible enviar a stats-rabbit: [" + ElasticData.ElasticType.subscription_registry + "]. [" + sr + "]. " + ex, ex);
                }
                return true;
            } catch (Exception ex) {
                log.error("Error al guardar AdnetworkNotifier: [" + an + "]. " + ex);
            }
        } catch (Exception ex) {
            log.error("Error al sincronizar notificacion. " + ex, ex);
        } finally {
            instrumentedObject.unLockObject(lock);
        }

        return false;
    }
}
