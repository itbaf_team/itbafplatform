/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.smsc.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class Notification {

    public String countryCode;
    public CTR[] ctrs;
    public String thread = Thread.currentThread().getName();

    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("countryCode", countryCode)
                .add("ctrs", ctrs)
                .add("thread", thread)
                .omitNullValues().toString();
    }
}
