import { Component } from '@angular/core';
import { PermissionsService } from '../@core/data/permissions.service';
import { MENU_ITEMS } from './pages-menu';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {
    
    menu: NbMenuItem[]; 
    permissions: any;
    
    constructor(private permissionsService: PermissionsService) { 
        this.menu=[
          {
            title: 'Home',
            icon: 'nb-home',
            link: '/pages/home',
          }];
        this.permissionsService.setComponent(this);
        this.permissionsService.getPermissions().subscribe((p: any) => this.remoteProcess(p));
    }

    remoteProcess(permissions): any {
        this.permissions=permissions;

        this.menu=[
          {
            title: 'Home',
            icon: 'nb-home',
            link: '/pages/home',
          },
          {
            title: 'E-commerce',
            icon: 'nb-e-commerce',
            link: '/pages/ecommerce',
            home: true,
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
          },
          {
            title: 'OPERACIONES',
            group: true,
            hidden: (this.permissions?(this.permissions.api_operations?false:true):true),
          },
          {
            title: 'Suscripciones',
            icon: 'far fa-address-book',
            link: '/pages/subscriptions',
            hidden: (this.permissions?(this.permissions.api_operations_subscriptions?false:true):true),
            children: [
              {
                title: 'Buscar',
                link: '/pages/subscriptions/search',
                hidden: (this.permissions?(this.permissions.api_operations_subscriptions_search?false:true):true),
              },
            ],
          },
          {
            title: 'FEATURES',
            group: true,
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
          },
          {
            title: 'UI Features',
            icon: 'nb-keypad',
            link: '/pages/ui-features',
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
            children: [
              {
                title: 'Buttons',
                link: '/pages/ui-features/buttons',
              },
              {
                title: 'Grid',
                link: '/pages/ui-features/grid',
              },
              {
                title: 'Icons',
                link: '/pages/ui-features/icons',
              },
              {
                title: 'Modals',
                link: '/pages/ui-features/modals',
              },
              {
                title: 'Popovers',
                link: '/pages/ui-features/popovers',
              },
              {
                title: 'Typography',
                link: '/pages/ui-features/typography',
              },
              {
                title: 'Animated Searches',
                link: '/pages/ui-features/search-fields',
              },
              {
                title: 'Tabs',
                link: '/pages/ui-features/tabs',
              },
            ],
          },
          {
            title: 'Forms',
            icon: 'nb-compose',
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
            children: [
              {
                title: 'Form Inputs',
                link: '/pages/forms/inputs',
              },
              {
                title: 'Form Layouts',
                link: '/pages/forms/layouts',
              },
            ],
          },
          {
            title: 'Components',
            icon: 'nb-gear',
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
            children: [
              {
                title: 'Tree',
                link: '/pages/components/tree',
              }, {
                title: 'Notifications',
                link: '/pages/components/notifications',
              },
            ],
          },
          {
            title: 'Maps',
            icon: 'nb-location',
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
            children: [
              {
                title: 'Google Maps',
                link: '/pages/maps/gmaps',
              },
              {
                title: 'Leaflet Maps',
                link: '/pages/maps/leaflet',
              },
              {
                title: 'Bubble Maps',
                link: '/pages/maps/bubble',
              },
              {
                title: 'Search Maps',
                link: '/pages/maps/searchmap',
              },
            ],
          },
          {
            title: 'Charts',
            icon: 'nb-bar-chart',
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
            children: [
              {
                title: 'Echarts',
                link: '/pages/charts/echarts',
              },
              {
                title: 'Charts.js',
                link: '/pages/charts/chartjs',
              },
              {
                title: 'D3',
                link: '/pages/charts/d3',
              },
            ],
          },
          {
            title: 'Editors',
            icon: 'nb-title',
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
            children: [
              {
                title: 'TinyMCE',
                link: '/pages/editors/tinymce',
              },
              {
                title: 'CKEditor',
                link: '/pages/editors/ckeditor',
              },
            ],
          },
          {
            title: 'Tables',
            icon: 'nb-tables',
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
            children: [
              {
                title: 'Smart Table',
                link: '/pages/tables/smart-table',
              },
            ],
          },
          {
            title: 'Miscellaneous',
            icon: 'nb-shuffle',
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
            children: [
              {
                title: '404',
                link: '/pages/miscellaneous/404',
              },
            ],
          },
          {
            title: 'Auth',
            icon: 'nb-locked',
            hidden: (this.permissions?(this.permissions.user_dev?false:true):true),
            children: [
              {
                title: 'Login',
                link: '/auth/login',
              },
              {
                title: 'Register',
                link: '/auth/register',
              },
              {
                title: 'Request Password',
                link: '/auth/request-password',
              },
              {
                title: 'Reset Password',
                link: '/auth/reset-password',
              },
            ],
          },
        ];
        return this.menu;
    }
}
