package com.itbaf.platform.paymenthub.commons.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.itbaf.platform.commons.rest.ManagerRestService;
import com.itbaf.platform.services.service.TestService;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import java.util.List;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/manager")
@Singleton
@lombok.extern.log4j.Log4j2
public class ManagerRestServiceImpl extends ManagerRestService {

    private final TestService testService;

    @Inject
    public ManagerRestServiceImpl(@Named("app.name") String appName,
            @Named("guice.profile") String profile,
            @Named("guice.instance") String instance,
            final TestService testService) {
        super(appName, profile, instance);
        this.testService = testService;
    }

    @Override
    public Response testInfraPost() {

        log.debug("Infra... Test... POST...");
        ResponseMessage rm = new ResponseMessage();
        rm.code = Thread.currentThread().getName();
        try {
            List<Provider> pList = testService.getAll();
            pList.get(0).getCountry().getId().toString();
            rm.status = ResponseMessage.Status.OK;
            rm.data = appName;
            rm.message = "Provider quantity [" + pList.size() + "]";
            return Response.status(Response.Status.OK).header("Host", appName)
                    .entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.data = ex.getMessage();
            rm.message = "Error al verificar el estado de la conexion a la DB. ";
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .header("Host", appName)
                .entity(rm).type(MediaType.APPLICATION_JSON)
                .build();
    }
}
