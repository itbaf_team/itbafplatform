/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.bn.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class MPNotification extends BNNotification {
    
    public String countryCode;
    public String mpTopic;
    public String id;
    public String thread = Thread.currentThread().getName();

    public MPNotification() {
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("countryCode",countryCode)
                .add("mpTopic", mpTopic)
                .add("id", id)
                .add("thread", thread)
                .omitNullValues().toString();
    }
}
