package com.itbaf.platform.paymenthub.model.ph;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QChannelQuantityProvider is a Querydsl query type for ChannelQuantityProvider
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QChannelQuantityProvider extends BeanPath<ChannelQuantityProvider> {

    private static final long serialVersionUID = -99400015L;

    public static final QChannelQuantityProvider channelQuantityProvider = new QChannelQuantityProvider("channelQuantityProvider");

    public final NumberPath<Integer> billingConsumerQuantity = createNumber("billingConsumerQuantity", Integer.class);

    public final NumberPath<Integer> onDemandBillingConsumerQuantity = createNumber("onDemandBillingConsumerQuantity", Integer.class);

    public final NumberPath<Integer> smppConsumerQuantity = createNumber("smppConsumerQuantity", Integer.class);

    public final NumberPath<Integer> subscriptionConsumerQuantity = createNumber("subscriptionConsumerQuantity", Integer.class);

    public final BooleanPath syncConsumer = createBoolean("syncConsumer");

    public QChannelQuantityProvider(String variable) {
        super(ChannelQuantityProvider.class, forVariable(variable));
    }

    public QChannelQuantityProvider(Path<? extends ChannelQuantityProvider> path) {
        super(path.getType(), path.getMetadata());
    }

    public QChannelQuantityProvider(PathMetadata metadata) {
        super(ChannelQuantityProvider.class, metadata);
    }

}

