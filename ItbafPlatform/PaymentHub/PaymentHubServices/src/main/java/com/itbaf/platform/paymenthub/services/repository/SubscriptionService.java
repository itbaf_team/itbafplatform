/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author javier
 */
public interface SubscriptionService {

    public Subscription findByUserAccount(String userAccout, SOP sop);

    public List<Subscription> findByUserAccount(String userAccout, String serviceCode);

    public Subscription findByExternalId(String externalId, SOP sop);

    public Subscription findByExternalUserAccount(String externalUserAccount, SOP sop);

    public Subscription findByExternalCustomer(String externalCustomer, SOP sop);

    public List<Subscription> getSyncSubscriptions(Provider provider, Date time);

    public void saveOrUpdate(Subscription s) throws Exception;

    public void delete(Long id) throws Exception;

    public Subscription findByMSISDNAndCountry(String msisdn, Country country);

    public Subscription findById(Long id) throws Exception;

    public List<Subscription> findSubscriptionsByProvider(String providerName, Date start, Date end);

    public List<Subscription> findSubscriptionsByUserAccount(String userAccount);

    public List<Subscription> getByExternalUserAccount(String externalUserAccount, SOP sop);

    public List<Subscription> getByMSISDNAndCountry(String msisdn, Country country);

    public Subscription getSubscriptionStatusFacadeRequest(String userName, SOP sop) throws Exception;

    public void sendSMSFacadeRequest(String userName, SOP sop, String message) throws Exception;

    public Subscription getSubscriptionInformationFacadeRequest(String userName, String countryCode) throws Exception;

    public List<Subscription> getSubscriptionCountryFacadeRequest(String userName, String countryCode) throws Exception;

    public void getSubscriptionCountryFacadeRequest(Map<String, String> userNameCountryCode) throws Exception;

    public ResponseMessage unsubscribeFacadeRequest(String userName, SOP sop, String channel) throws Exception;

    public List<Subscription> getActiveSubscriptions(List<String> userAccounts, Collection<SOP> sops);

    public List<Subscription> getActiveSubscriptions(String userAccount, String providerName);

    public List<Subscription> getSubscriptionsWithoutSubscriptionRegistry(Provider provider);

    public List<BigInteger> getSubscriptionsRenew(SOP sop, Integer frecuency);

    public List<BigInteger> getSubscriptionsMidRenew(SOP sop, Integer frecuency);

    public List<BigInteger> getSubscriptionsMidRenewTwiceWeek(SOP sop);

    public Subscription getLastSubscription(List<String> userAccounts, Collection<SOP> sops);

    public List<Subscription> getRetentionSubscriptionsBeforeDays(String providerName, int days);

    public List<Subscription> getActived24hsAndWithoutBilling();
}
