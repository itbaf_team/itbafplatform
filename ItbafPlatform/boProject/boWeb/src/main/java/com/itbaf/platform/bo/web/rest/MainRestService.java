package com.itbaf.platform.bo.web.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("/")
@Singleton
@lombok.extern.log4j.Log4j2
public class MainRestService {

    private final String maxCacheControl;

    @Inject
    public MainRestService(@Named("http.response.header.cache.control") String maxCacheControl) {
        this.maxCacheControl = "max-age=" + maxCacheControl;
    }

    @GET
    @Path("/")
    public Response getPage1(@PathParam("path") String path, @Context HttpServletRequest request) throws Exception {
        log.info("getFile1. path: [" + path + "]");
        path = "index.html";
        return getPage2(path, request);
    }

    @GET
    @Path("/{path : .+}")
    public Response getPage2(@PathParam("path") String path, @Context HttpServletRequest request) throws Exception {

        log.info("getFile2. path: [" + path + "]");
        if (path.isEmpty()) {
            path = "index.html";
        }

        try {
            InputStream is = getClass().getResourceAsStream("/webapp/" + path);
            return Response.ok((Object) is)
                    .header("Cache-Control", maxCacheControl)
                    .build();
        } catch (Exception ex) {
            log.error("Error al obtener el archivo. path: [" + path + "]. " + ex, ex);
        }

        return Response.status(Response.Status.FORBIDDEN).build();
    }
}
