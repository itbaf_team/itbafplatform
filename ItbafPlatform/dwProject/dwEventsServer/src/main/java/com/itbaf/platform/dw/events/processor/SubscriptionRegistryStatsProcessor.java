/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events.processor;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.stats.model.BaseResumenElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.commons.stats.model.StatsMessage;
import com.itbaf.platform.paymenthub.commons.stats.model.SubscriptionRegistryElastic;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class SubscriptionRegistryStatsProcessor extends StatsProcessor {

    private static final ElasticData.ElasticType elasticType = ElasticData.ElasticType.subscription_registry;

    @Inject
    public SubscriptionRegistryStatsProcessor() {
    }

    @Override
    public Boolean process(String msg) throws Exception {
        // log.info("Procesando DW SubscriptionRegistry: [" + msg + "]");

        StatsMessage smx = mapper.readValue(msg, StatsMessage.class);
        String mm = "dw_" + smx.elasticData.elasticType + "_" + smx.elasticData.elasticId;

        RLock lock = instrumentedObject.lockObject(mm, 60L);
        try {
            String dm = clientCache.getTemporalData(mm);
            if (dm != null) {
                if (smx.lastUpdateMillis < Long.parseLong(dm)) {
                    log.info("Ignorando DW SubscriptionRegistry: [" + msg + "]");
                    return true;
                }
            }
            StatsMessage sm = new StatsMessage();
            SubscriptionRegistry sr = subscriptionRegistryService.findById(Long.parseLong(smx.elasticData.elasticId));
            if (sr == null) {
                // log.info("No existe el registro en la base. [" + msg + "]");
                Thread.sleep(7000);
                sr = subscriptionRegistryService.findById(Long.parseLong(smx.elasticData.elasticId));
                if (sr == null) {
                    log.warn("No existe el registro en la base. [" + msg + "]");
                    Thread.sleep(1000);
                    return null;
                }
            }

            SubscriptionRegistryElastic sre = getSubscriptionRegistryElastic(sr);
            sm.thread = smx.thread;
            sm.elasticData = sre;

            String sxAlta = null;
            String sxBaja = null;
            String sxSuscripcion = null;

            if (sre.firstCharged != null) {
                BaseResumenElastic bre = new BaseResumenElastic();
                bre.functionType = BaseResumenElastic.FunctionType.ALTA;
                bre.elasticId = sre.elasticId;
                bre.fecha = sre.firstCharged;
                bre.subscriptionId = sre.subscriptionId;
                bre.subscriptionRegistryId = Long.parseLong(sre.id);
                bre.subscriptionRegistryElastic = sre;
                StatsMessage sx = new StatsMessage();
                sx.thread = smx.thread;
                sx.elasticData = bre;
                sxAlta = mapper.writeValueAsString(sx);
                /////
                if (sre.unsubscriptionDate != null) {
                    bre.functionType = BaseResumenElastic.FunctionType.BAJA;
                    bre.fecha = sre.unsubscriptionDate;
                    sxBaja = mapper.writeValueAsString(sx);
                }
                /////
                bre.functionType = BaseResumenElastic.FunctionType.SUSCRIPCION;
                bre.fecha = sre.subscriptionDate;
                sxSuscripcion = mapper.writeValueAsString(sx);
                /////
            }

            String json = mapper.writeValueAsString(sm);
            //Enviamos el SUBSCRIPTION_REGISTRY actualizado al Rabbit
            if (Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.subscription_registry, json))) {
                if (sxSuscripcion != null && Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.base_resumen, sxSuscripcion))) {
                    if (sxAlta != null && Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.base_resumen, sxAlta))) {
                        if (sxBaja != null && Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.base_resumen, sxBaja))) {
                            clientCache.setTemporalData(mm, sm.lastUpdateMillis.toString(), 2L);
                            return true;
                        } else  if (sxBaja == null){
                            clientCache.setTemporalData(mm, sm.lastUpdateMillis.toString(), 2L);
                            return true;
                        }
                    } else  if (sxAlta == null){
                        clientCache.setTemporalData(mm, sm.lastUpdateMillis.toString(), 2L);
                        return true;
                    }
                } else if (sxSuscripcion == null) {
                    clientCache.setTemporalData(mm, sm.lastUpdateMillis.toString(), 2L);
                    return true;
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar SubscriptionRegistry [" + msg + "]. " + ex, ex);
        } finally {
            instrumentedObject.unLockObject(lock);
        }

        return null;
    }

    @Override
    public ElasticData.ElasticType getElasticType() {
        return elasticType;
    }

}
