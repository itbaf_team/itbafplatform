/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.repository.jpa.model.wallet;

import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.wallet.QWalletTransaction;
import com.itbaf.platform.pgp.model.wallet.WalletTransaction;
import com.querydsl.core.group.GroupBy;
import com.querydsl.core.types.Expression;
import com.querydsl.jpa.impl.JPAQuery;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAWalletTransactionRepository extends AbstractJPARepositorySession<WalletTransaction, Long> {

    @Inject
    public JPAWalletTransactionRepository(final Provider<EntityManager> entityManagerProvider) {
        super(WalletTransaction.class, entityManagerProvider);
    }

    public List<WalletTransaction> getWalletTransaction(User user, Date from, Date to) {
        if (user == null || user.getId() == null || from == null || to == null) {
            String error = "Alguno de los parametros en getWalletTransaction es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }
        QWalletTransaction wt = QWalletTransaction.walletTransaction;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<WalletTransaction> wtl = query.select(wt).from(wt).
                where(ObjectPredicates.combine(wt.user().eq(user), wt.chargedDateSearch.between(from, to)))
                .orderBy(wt.chargedDate.desc())
                .fetch();

        return wtl;
    }

    public List<WalletTransaction> getWalletTransaction(User user) {
        if (user == null || user.getId() == null) {
            String error = "Alguno de los parametros en getWalletTransaction es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }
        QWalletTransaction wt = QWalletTransaction.walletTransaction;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<WalletTransaction> wtl = query.select(wt).from(wt).
                where(wt.user().eq(user))
                .orderBy(wt.chargedDate.asc())
                .fetch();

        return wtl;
    }

    public void deleteByUserId(Long userId) {
        String sql = "DELETE FROM W_TRANSACTION WHERE USER_ID = :userId ";

        EntityManager em = getEntityManager();
        int i = em.createNativeQuery(sql)
                .setParameter("userId", userId)
                .executeUpdate();
        log.info("Delete WalletTransaction: [" + userId + "][" + i + "]");
    }

    public WalletTransaction findByTransactionId(String transactionId) {
        if (transactionId == null) {
            String error = "Alguno de los parametros en findByTransactionId es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }
        QWalletTransaction wt = QWalletTransaction.walletTransaction;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        WalletTransaction wtr = query.select(wt).from(wt).
                where(wt.tid.eq(transactionId))
                .fetchFirst();

        return wtr;
    }

    //<sop,TotalLocalAmount>
    public Map<Long, BigDecimal> getTotalLocalAmount(User user) {
        if (user == null || user.getId() == null) {
            String error = "Alguno de los parametros en getTotalLocalAmount es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }

        QWalletTransaction wt = QWalletTransaction.walletTransaction;
        final Expression<Long> keyColumn = wt.sop().id;
        final Expression<BigDecimal> valueColumn = wt.localAmount.sum();

        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Map<Long, BigDecimal> wtr = query.select(keyColumn, valueColumn).from(wt).
                where(wt.sop().isNotNull(), wt.user().id.eq(user.getId()), wt.type.eq(WalletTransaction.Type.ADD))
                .groupBy(keyColumn)
                .transform(GroupBy.groupBy(keyColumn).as(valueColumn));
        return wtr;
    }

    public WalletTransaction findLatsWithdraw(User user, Long sopId, Date from, Date to) {
        if (user == null || user.getId() == null || from == null || to == null) {
            String error = "Alguno de los parametros en getWalletTransaction es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }
        QWalletTransaction wt = QWalletTransaction.walletTransaction;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        WalletTransaction wtr = query.select(wt).from(wt).
                where(wt.sop().id.eq(sopId), wt.user().id.eq(user.getId()), wt.type.eq(WalletTransaction.Type.WITHDRAW), wt.chargedDateSearch.between(from, to))
                .orderBy(wt.chargedDate.desc())
                .fetchFirst();

        return wtr;

    }
}
