#!/bin/bash

CHART_REPO=planetaguru

if [[ $# -ne 1 ]] ; then echo "ERROR: se debe pasar como parametro el chart name."; exit 1; fi

CHART_NAME=$1

export GOOGLE_APPLICATION_CREDENTIALS=/home/jdelazerda/GCP/service-accounts/helm-gcs/key.json
CHART_VERSION=$(grep version ${CHART_NAME}/Chart.yaml | cut -d '"' -f2)

helm package ${CHART_NAME} --debug
helm gcs push ${CHART_NAME}-${CHART_VERSION}.tgz ${CHART_REPO} --debug

helm repo update
