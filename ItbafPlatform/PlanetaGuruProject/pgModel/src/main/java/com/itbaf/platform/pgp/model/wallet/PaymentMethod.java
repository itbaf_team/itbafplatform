/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.wallet;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.pgp.model.*;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.persistence.util.HibernateEquality;
import com.querydsl.core.annotations.Config;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

/**
 *
 * @author JF
 */
@Entity(name = "W_PAYMENT_METHOD")
@Table(name = "W_PAYMENT_METHOD")
@Cacheable(true)
@Cache(region = "oauth", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class PaymentMethod extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 20180110142001L;

    public enum PaymentMethodType {
        SUBSCRIPTION,
        ONDEMAND
    }

    @Id
    @Column(name = "SOP_ID", unique = true, nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE")
    private PaymentMethodType type;

    @Enumerated(EnumType.STRING)
    @Column(name = "FRECUENCY_TYPE")
    private FrecuencyType frecuencyType;

    @Column(name = "NAME")
    private String name;

    @OneToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;

    @Column(name = "LOGO_URL")
    private String logoUrl;

    @Column(name = "LANDING_URL")
    private String landingUrl;

    @Type(type = "json")
    @Column(name = "FACADE_PARAMS", columnDefinition = "json")
    private FacadeRequest facadeParams;

    @Transient
    private Country country;
    @Transient
    public com.itbaf.platform.messaging.rest.pg.Credit credit;
    @Transient
    public com.itbaf.platform.messaging.rest.pg.Credit virtualCredit;
    @Transient
    public String frecuencyI18n;
    @Transient
    public String description;
    @Transient
    public Product product;
    @Transient
    public String userAccountType;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("type", type)
                .add("frecuencyType", frecuencyType)
                .add("name", name)
                .add("sop", sop)
                .add("logoUrl", logoUrl)
                .add("landingUrl", landingUrl)
                .add("facadeParams", facadeParams)
                .add("country", country)
                .add("credit", credit)
                .add("virtualCredit", virtualCredit)
                .add("frecuencyI18n", frecuencyI18n)
                .add("description", description)
                .add("product", product)
                .add("userAccountType", userAccountType)
                .omitNullValues().toString();
    }
    
    
    @Override
    public int hashCode() {
        return Objects.hash(id, type, frecuencyType, name, logoUrl, landingUrl, facadeParams, sop);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<PaymentMethod>()
                .over(PaymentMethod::getId)
                .over(PaymentMethod::getType)
                .over(PaymentMethod::getFrecuencyType)
                .over(PaymentMethod::getLogoUrl)
                .over(PaymentMethod::getLandingUrl)
                .over(PaymentMethod::getFacadeParams)
                .over(PaymentMethod::getSop)
                .asserts(this, obj);
    }
}
