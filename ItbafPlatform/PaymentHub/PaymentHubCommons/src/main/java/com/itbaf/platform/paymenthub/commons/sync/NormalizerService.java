/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.sync;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.services.service.PropertyManagerService;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public abstract class NormalizerService {

    @Inject
    protected PropertyManagerService propertyManagerService;

    protected final String userAccountNormalizer(String userAccount, Provider provider) throws Exception {

        Integer msisdnLengthLocal = Integer.parseInt(propertyManagerService.getCommonProperty("msisdn."
                + provider.getCountry().getCode().toLowerCase() + ".length.local"));
        Integer msisdnLengthInternational = Integer.parseInt(propertyManagerService.getCommonProperty("msisdn."
                + provider.getCountry().getCode().toLowerCase() + ".length.international"));
        String msisdnPrefix = provider.getCountry().getPhoneCode().toString();

        try {
            userAccount = userAccount.trim().replace("+", "");
            userAccount = Long.parseLong(userAccount) + "";

            if (!isNumber(userAccount)) {
                throw new RuntimeException("NumberFormatException... ");
            }

            if (!userAccount.startsWith(msisdnPrefix)) {
                userAccount = msisdnPrefix + userAccount;
            }

            //Validacion para Argentina
            if (userAccount.length() > 10 && userAccount.length() < 14) {
                if (userAccount.startsWith("5415")) {
                    userAccount = "5411" + userAccount.substring(4, userAccount.length());
                } else if (userAccount.startsWith("54915")) {
                    userAccount = "54911" + userAccount.substring(5, userAccount.length());
                }
            }
            //Validacion para Claro Argentina.
            if (provider.getName().startsWith("ar.claro")) {
                msisdnLengthInternational++;
                msisdnPrefix = "549";
            } else if (provider.getName().startsWith("br.") && (userAccount.length() == 10 || userAccount.length() == 8)) {
                msisdnLengthLocal--;
                msisdnLengthInternational--;
            }

            if (userAccount.length() < msisdnLengthLocal) {
                throw new RuntimeException("");
            }

            if (userAccount.length() == msisdnLengthInternational && !userAccount.startsWith(msisdnPrefix)) {
                throw new RuntimeException("");
            }

            userAccount = userAccount.substring(Math.max(0, userAccount.length() - msisdnLengthLocal), userAccount.length());
            if (userAccount.length() == msisdnLengthLocal) {
                userAccount = msisdnPrefix + userAccount;
            } else {
                throw new RuntimeException("");
            }
        } catch (Exception ex) {
            if (!provider.getName().endsWith(".npay")) {
                log.warn("El formato del userAni: [" + userAccount + "] para el provider: [" + provider + "] no es valido.");
            }
            throw new Exception("El formato del userAni: [" + userAccount + "] para el provider: [" + provider + "] no es valido.", ex);
        }

        return userAccount;
    }

    protected boolean isNumber(String number) {
        char x[] = number.toCharArray();
        for (char c : x) {
            if (c < 48 || c > 57) {
                return false;
            }
        }
        return true;
    }
}
