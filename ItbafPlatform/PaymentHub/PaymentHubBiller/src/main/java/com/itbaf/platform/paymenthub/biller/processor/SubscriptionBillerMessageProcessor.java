/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.biller.processor;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.messaging.BillerMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.BillerAttempt;

/**
 *
 * @author javier
 * 
 * Crea un nuevo BillerAttempt y lo envia al modulo de cobro correspondiente
 */
@lombok.extern.log4j.Log4j2
public class SubscriptionBillerMessageProcessor extends BillerMessageProcessor {

    @Inject
    public SubscriptionBillerMessageProcessor() {
        super(BillerMessage.BillerType.SUBSCRIPTION);

    }

    @Override
    public Boolean process(String message, BillerMessage bm, SOP sop) {
        Boolean result = null;
        if (message == null || bm == null || sop == null || bm.getSubscription() == null
                || bm.getSubscription().getSubscriptionRegistry() == null || bm.getSubscription().getSubscriptionRegistry().getId() == null) {
            log.error("Alguno de los parametros es nulo. [" + message + "][" + bm + "][" + sop + "]");
            return null;
        }

        BillerAttempt ba = serviceProcessorHandler.createBillerAttempt(bm.getSubscription());

        if (ba != null) {
            result = serviceProcessorHandler.sendBillerProcess(ba);
        }

        return result;
    }

}
