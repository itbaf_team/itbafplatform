package com.itbaf.platform.paymenthub.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCountry is a Querydsl query type for Country
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCountry extends EntityPathBase<Country> {

    private static final long serialVersionUID = 1679175278L;

    public static final QCountry country = new QCountry("country");

    public final StringPath code = createString("code");

    public final StringPath currency = createString("currency");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final SetPath<Language, QLanguage> languages = this.<Language, QLanguage>createSet("languages", Language.class, QLanguage.class, PathInits.DIRECT2);

    public final StringPath name = createString("name");

    public final StringPath nativeName = createString("nativeName");

    public final BooleanPath nativeView = createBoolean("nativeView");

    public final NumberPath<Integer> phoneCode = createNumber("phoneCode", Integer.class);

    public final EnumPath<Status> status = createEnum("status", Status.class);

    public final StringPath symbolCurrency = createString("symbolCurrency");

    public QCountry(String variable) {
        super(Country.class, forVariable(variable));
    }

    public QCountry(Path<? extends Country> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCountry(PathMetadata metadata) {
        super(Country.class, metadata);
    }

}

