/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adn.processor;

import com.google.inject.Inject;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.paymenthub.adn.resources.AdnetworkNotificationType;
import com.itbaf.platform.paymenthub.commons.bn.model.BorderNotificationMessage;
import com.itbaf.platform.paymenthub.commons.resources.FacadeCache;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.AdnetworkNotifierSimple;
import com.itbaf.platform.paymenthub.services.repository.AdnetworkNotifierSimpleService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class SimpleAdnetworkProcessor extends AdnetworkProcessor {
    
    private final SopService sopService;
    private final FacadeCache facadeCache;
    private final AdnetworkNotifierSimpleService adnetworkNotifierSimpleService;
    private static final PaymentHubMessage.NotificationType notificationType = PaymentHubMessage.NotificationType.ADN_SIMPLE;
    
    @Inject
    public SimpleAdnetworkProcessor(final SopService sopService,
            final FacadeCache facadeCache,
            final AdnetworkNotifierSimpleService adnetworkNotifierSimpleService) {
        this.sopService = sopService;
        this.facadeCache = facadeCache;
        this.adnetworkNotifierSimpleService = adnetworkNotifierSimpleService;
    }
    
    @Override
    public PaymentHubMessage.NotificationType getNotificationType() {
        return notificationType;
    }
    
    @Override
    public Boolean process(String msg) throws Exception {
        BorderNotificationMessage notification = mapper.readValue(msg, BorderNotificationMessage.class);
        
        FacadeRequest fr = facadeCache.getFacadeRequest(null, notification.message, null, 0L);
        
        AdnetworkNotifierSimple adns = null;
        
        RLock lock = null;
        try {
            lock = instrumentedObject.lockObject("Adn-Simple-" + notification.message, 60);
            adns = adnetworkNotifierSimpleService.findById(notification.message);
            if (fr == null && adns == null) {
                log.warn("No existe un FacadeRequest para: [" + msg + "]");
                return true;
            }
            
            if (adns != null && adns.getNotificationSend()) {
                log.info("Mensaje ya notificado a la ADN. [" + msg + "]");
                return true;
            }
            
            if (adns == null && fr != null) {
                adns = new AdnetworkNotifierSimple();
                adns.setAdnetworkTracking(fr.adTracking);
                adns.setSubscriptionDate(fr.creationDate);
                adns.setNotificationSend(Boolean.FALSE);
                adns.setSop(sopService.findById(fr.sopId));
                if (adns.getSop() == null) {
                    log.error("No existe un SOP definido para: [" + fr.sopId + "]");
                    return null;
                }
                adns.setTrackingId(fr.transactionId);
                adnetworkNotifierSimpleService.saveOrUpdate(adns);
            }
            
            if (adns == null) {
                return true;
            }
            
            LinkedHashMap<String, Object> adn = null;
            ArrayList<String> adnTrackingString = null;
            String adnNotTypeString = null;
            String source = null;
            AdnetworkNotificationType type = null;
            try {
                adn = (LinkedHashMap<String, Object>) adns.getAdnetworkTracking();
                adnTrackingString = (ArrayList<String>) adn.get("adnTrackingId");
                source = (String) adn.get("source");
                type = mapper.convertValue(adn.get("adnNotType"), AdnetworkNotificationType.class);
            } catch (Exception ex) {
            }
            
            if (adnTrackingString == null || source == null || (type == null && adnNotTypeString == null)) {
                return true;
            }
            
            String url = null;
            try {
                url = profilePropertiesService.getCommonProperty("adnetwork." + source + ".url");
                int i = 0;
                for (String id : adnTrackingString) {
                    url = url.replace("<ID" + i + ">", id);
                    i++;
                }
                String result = requestClient.requestOKGet(url);
                if (result != null) {
                    log.info("Adnetwork [" + url + "] response: [" + result + "]");
                    adns.setResponse(result);
                    adns.setNotificationSend(true);
                    adnetworkNotifierSimpleService.saveOrUpdate(adns);
                    return true;
                } else {
                    return null;
                }
            } catch (Exception ex) {
                log.error("Error al notificar a: [" + source + "][" + url + "]. " + ex, ex);
            }
        } catch (Exception ex) {
            log.error("Error al procesar AdnetworkNotifierSimple: [" + notification.message + "]. " + ex, ex);
        } finally {
            instrumentedObject.unLockObject(lock);
        }
        
        return null;
    }
    
}
