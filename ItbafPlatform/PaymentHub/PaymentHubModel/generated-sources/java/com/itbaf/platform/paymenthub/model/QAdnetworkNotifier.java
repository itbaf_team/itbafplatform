package com.itbaf.platform.paymenthub.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAdnetworkNotifier is a Querydsl query type for AdnetworkNotifier
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAdnetworkNotifier extends EntityPathBase<AdnetworkNotifier> {

    private static final long serialVersionUID = -1815716791L;

    public static final QAdnetworkNotifier adnetworkNotifier = new QAdnetworkNotifier("adnetworkNotifier");

    public final NumberPath<java.math.BigDecimal> charged = createNumber("charged", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final BooleanPath notificationSend = createBoolean("notificationSend");

    public final StringPath response = createString("response");

    public final EnumPath<com.itbaf.platform.paymenthub.model.subscription.Subscription.Status> status = createEnum("status", com.itbaf.platform.paymenthub.model.subscription.Subscription.Status.class);

    public QAdnetworkNotifier(String variable) {
        super(AdnetworkNotifier.class, forVariable(variable));
    }

    public QAdnetworkNotifier(Path<? extends AdnetworkNotifier> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdnetworkNotifier(PathMetadata metadata) {
        super(AdnetworkNotifier.class, metadata);
    }

}

