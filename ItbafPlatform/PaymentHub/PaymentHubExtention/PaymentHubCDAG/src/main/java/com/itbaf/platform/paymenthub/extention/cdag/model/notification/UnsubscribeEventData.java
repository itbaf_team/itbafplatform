package com.itbaf.platform.paymenthub.extention.cdag.model.notification;

import com.google.common.base.MoreObjects;
import java.util.Date;

@lombok.Getter
@lombok.Setter
public class UnsubscribeEventData extends EventData {

    private String msisdn;
    private Date unsubscriptionDate;
    private String serviceId;
    private String keyword;
    private String shortcode;
    private ServiceType serviceType;
    private String pautaId;
    private String pautaStatus;

    public UnsubscribeEventData(String data) {
        super(data);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("msisdn", msisdn)
                .add("unsubscriptionDate", unsubscriptionDate)
                .add("serviceId", serviceId)
                .add("keyword", keyword)
                .add("shortcode", shortcode)
                .add("serviceType", serviceType)
                .add("pautaId", pautaId)
                .add("pautaStatus", pautaStatus)
                .omitNullValues().toString();
    }
    
    @Override
    protected void parseData() {
        String[] values = this.getValues();

        this.msisdn = values[0];
        try {
            this.unsubscriptionDate = new Date(Long.parseLong(values[1] + "000"));
        } catch (Exception ex) {
            this.unsubscriptionDate = new Date();
        }
        this.serviceId = values[2];
        this.keyword = values[3];
        try {
            this.shortcode = values[4];
        } catch (Exception ex) {
        }
        try {
            this.serviceType = ServiceType.getserviceType(values[5]);
        } catch (Exception ex) {
        }
        try {
            this.pautaId = values[6];
        } catch (Exception ex) {
        }
        try {
            this.pautaStatus = values[7];
        } catch (Exception ex) {
        }
    }
}
