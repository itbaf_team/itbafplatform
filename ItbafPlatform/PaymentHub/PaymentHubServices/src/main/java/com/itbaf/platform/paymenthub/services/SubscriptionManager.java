/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services;

import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import java.util.List;

/**
 *
 * @author javier
 */
public interface SubscriptionManager {

    public Provider getProviderByName(String name);

    public Subscription subscriptionProcess(Subscription s);

    public Subscription getSubscriptionByExternalId(String externalId, SOP sop);

    public Subscription getSubscriptionByUserAccount(String userAccount, SOP sop);

    public List<Subscription> getSubscriptionsByUserAccount(String userAccout, String serviceCode);

    public Subscription getSubscriptionStatusByService(String userAccout, String serviceCode, List<Subscription> slist);

    public Subscription getSubscriptionByExternalUserAccount(String externalUserAccount, SOP sop);

    public Subscription getSubscription(String externalId, String userAccount, String externalUserAccount, String externalCustomer, SOP sop);

    public SOP getOrCreateSOP(String serviceMatcher, String operator, String provider) throws Exception;

    public List<Subscription> getSyncSubscriptions(Provider provider);

    public void updateSubscriptionStatus(Subscription s, Subscription.Status status);

    public SOP getSOP(String service, String operator, String provider);

    public List<SOP> getSOPsByService(String serviceCode, Provider p);

    public Subscription getSubscriptionsByUserAccountAndCountry(String userAccount, String countryCode);

    public List<SOP> getSOPsByCountry(String countryCode);

    public SubscriptionRegistry getSubscriptionRegistry(Long SubscriptionRegistryId) throws Exception;

    public Subscription getSubscription(Long SubscriptionId) throws Exception;

    public Subscription getSubscriptionStatusFacadeRequest(String userName, SOP sop) throws Exception;

    public List<Subscription> getSubscriptionsWithoutSubscriptionRegistry(Provider provider);

    public SubscriptionRegistry getLastRegisterBySubscription(Long subscriptionId, boolean isPenult);
    
    public void updateMemoryCache();
}
