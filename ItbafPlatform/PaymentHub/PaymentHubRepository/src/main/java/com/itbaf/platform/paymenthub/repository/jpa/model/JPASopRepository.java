/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.QSOP;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import io.jsonwebtoken.lang.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.SQLException;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPASopRepository extends AbstractJPARepositorySession<SOP, Long> {

    @Inject
    public JPASopRepository(final javax.inject.Provider<EntityManager> entityManagerProvider) {
        super(SOP.class, entityManagerProvider);
    }

    public SOP findByNames(String serviceMatcher, String operator, String provider) {
        if (serviceMatcher == null || operator == null || provider == null) {
            String error = "Algunos de los atributos del SOP son nulos... serviceMatcher: [" + serviceMatcher + "] - operator: [" + operator + "] - provider: [" + provider + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSOP qSOP = QSOP.sOP;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        operator = operator.toUpperCase();
        provider = provider.toLowerCase();

        SOP p = query.select(qSOP).from(qSOP).
                where(ObjectPredicates.combine(qSOP.serviceMatcher().name.eq(serviceMatcher),
                        qSOP.operator().name.eq(operator),
                        qSOP.provider().name.eq(provider),
                        qSOP.status.eq(Status.ENABLE)))
                .fetchFirst();

        return p;
    }

    public SOP findBySettings(String provider, Map<String, String> settings) {
        if (provider == null || settings == null) {
            String error = "Algunos de los parametros son nulos... provider: [" + provider + "] - settings: [" + settings + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSOP qSOP = QSOP.sOP;
        JPAQuery<?> query = new JPAQuery(getEntityManager());
        provider = provider.toLowerCase();

        Predicate p = null;
        if (provider.length() == 0) {
            p = ObjectPredicates.combine(qSOP.provider().isNotNull());
        } else {
            p = ObjectPredicates.combine(qSOP.provider().name.eq(provider));
        }
        Set<String> keys = settings.keySet();
        for (String key : keys) {
            String val = settings.get(key);
            if (val == null) {
                p = ObjectPredicates.combine(p, qSOP.integrationSettings.containsKey(key), qSOP.status.eq(Status.ENABLE));
            } else {
                p = ObjectPredicates.combine(p, qSOP.integrationSettings.get(key).eq(val), qSOP.status.eq(Status.ENABLE));
            }
        }

        SOP sop = query.select(qSOP).from(qSOP).
                where(p)
                .fetchFirst();

        return sop;
    }


    



    public List<SOP> findAllBySettings(String provider, Map<String, String> settings) {
        if (provider == null || settings == null) {
            String error = "Algunos de los parametros son nulos... provider: [" + provider + "] - settings: [" + settings + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSOP qSOP = QSOP.sOP;
        JPAQuery<?> query = new JPAQuery(getEntityManager());
        provider = provider.toLowerCase();

        Predicate p = null;
        if (provider.length() == 0) {
            p = ObjectPredicates.combine(qSOP.provider().isNotNull());
        } else {
            p = ObjectPredicates.combine(qSOP.provider().name.eq(provider));
        }
        Set<String> keys = settings.keySet();
        for (String key : keys) {
            String val = settings.get(key);
            if (val == null) {
                p = ObjectPredicates.combine(p, qSOP.integrationSettings.containsKey(key), qSOP.status.eq(Status.ENABLE));
            } else {
                p = ObjectPredicates.combine(p, qSOP.integrationSettings.get(key).eq(val), qSOP.status.eq(Status.ENABLE));
            }
        }

        List<SOP> sop = query.select(qSOP).from(qSOP).
                where(p)
                .fetch();

        return sop;
    }

    public List<SOP> getMatchByProviderName(String provider) {
        if (provider == null) {
            String error = "Algunos de los parametros son nulos... provider: [" + provider + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        provider = provider.toLowerCase();

        QSOP qSOP = QSOP.sOP;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List< SOP> sops = query.select(qSOP).from(qSOP).
                where(ObjectPredicates.combine(qSOP.status.eq(Status.ENABLE),
                        qSOP.provider().name.startsWith(provider)))
                .fetch();

        return sops;
    }

    public List<SOP> getSOPsByService(String serviceCode, Provider provider) {
        if (provider == null || serviceCode == null) {
            String error = "Algunos de los parametros son nulos... serviceCode: [" + serviceCode + "] - provider: [" + provider + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSOP qSOP = QSOP.sOP;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List< SOP> sops = query.select(qSOP).from(qSOP).
                where(ObjectPredicates.combine(qSOP.status.eq(Status.ENABLE),
                        qSOP.provider().name.eq(provider.getName()), qSOP.serviceMatcher().service().code.eq(serviceCode.toUpperCase())))
                .fetch();

        return sops;
    }

    public List<SOP> getSOPsByProviderName(String providerName) {
        if (providerName == null) {
            String error = "Algunos de los parametros son nulos... providerName: [" + providerName + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSOP qSOP = QSOP.sOP;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List< SOP> sops = query.select(qSOP).from(qSOP).
                where(ObjectPredicates.combine(qSOP.status.eq(Status.ENABLE),
                        qSOP.provider().name.eq(providerName)))
                .fetch();

        return sops;
    }

    public SOP getSOPByTariff(String tariffExternalid) {
        if (tariffExternalid == null) {
            String error = "Algunos de los parametros son nulos... tariffExternalid: [" + tariffExternalid + "]";
            log.error(error);

            throw new RuntimeException(error);
        }

        QSOP qSOP = QSOP.sOP;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<SOP> sops = query.select(qSOP).from(qSOP).
                where(ObjectPredicates.combine(qSOP.status.eq(Status.ENABLE),
                        qSOP.tariffs.any().externalId.eq(tariffExternalid)))
                .fetch();

        if (!Collections.isEmpty(sops)) {
            return sops.get(0);
        }

        return null;
    }

    public List<SOP> getSOPsByCountry(String countryCode) {
        if (countryCode == null) {
            String error = "Algunos de los parametros son nulos... countryCode: [" + countryCode + "]";
            log.error(error);

            throw new RuntimeException(error);
        }

        QSOP qSOP = QSOP.sOP;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List< SOP> sops = query.select(qSOP).from(qSOP).
                where(ObjectPredicates.combine(qSOP.status.eq(Status.ENABLE),
                        qSOP.provider().country().code.eq(countryCode.toUpperCase())))
                .fetch();

        return sops;
    }
}
