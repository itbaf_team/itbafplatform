package com.itbaf.platform.pgp.api.rest.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.pgp.api.handler.AccountViewServiceProcessorHandler;
import com.itbaf.platform.pgp.api.handler.StoreViewServiceProcessorHandler;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import io.jsonwebtoken.impl.DefaultClaims;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;

@Path("/store")
@Singleton
@lombok.extern.log4j.Log4j2
public class StoreViewRestService extends CommonRestService {

    private final StoreViewServiceProcessorHandler serviceViewServiceProcessorHandler;
    private final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler;

    @Inject
    public StoreViewRestService(final StoreViewServiceProcessorHandler serviceViewServiceProcessorHandler,
            final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler) {
        this.serviceViewServiceProcessorHandler = Validate.notNull(serviceViewServiceProcessorHandler, "A ServiceViewServiceProcessorHandler class must be provided");
        this.accountViewServiceProcessorHandler = Validate.notNull(accountViewServiceProcessorHandler, "An AccountViewServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public Response store(@Context HttpServletRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.store");
        } catch (Exception ex) {
        }
        log.info("store. [" + request.getQueryString() + "]");

        return getServicePage(request);
    }

    private Response getServicePage(HttpServletRequest request) {
        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            DefaultClaims dc = (DefaultClaims) request.getAttribute(CommonAttribute.U_DEFAULT_CLAIMS);
            String productId = request.getAttribute(CommonAttribute.PRODUCT_ID).toString();
            if (client != null && user != null && dc != null) {
                String lg = user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage();
                String redirectUri = request.getAttribute(CommonAttribute.U_REDIRECT_URI).toString();
                String html = serviceViewServiceProcessorHandler.getServicesHtml(client, user, lg, dc, productId,redirectUri);
                if (html != null) {
                    String prvdr = null;
                    try {
                        prvdr = dc.get("prvdr", String.class);
                    } catch (Exception ex) {
                    }
                    html = accountViewServiceProcessorHandler.i18n(lg, prvdr, html);
                    return Response.status(Response.Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar peticion. " + ex, ex);
        }

        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

}
