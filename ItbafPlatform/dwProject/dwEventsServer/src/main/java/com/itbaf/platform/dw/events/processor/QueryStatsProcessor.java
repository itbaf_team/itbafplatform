/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events.processor;

import com.google.inject.Inject;
import com.itbaf.platform.dw.events.etl.model.EtlQuery;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class QueryStatsProcessor extends StatsProcessor {

    private final BasicDataBaseConnection dataBaseConnection;
    private static final ElasticData.ElasticType elasticType = ElasticData.ElasticType.query;

    @Inject
    public QueryStatsProcessor(final BasicDataBaseConnection dataBaseConnection) {
        this.dataBaseConnection = dataBaseConnection;
    }

    @Override
    public Boolean process(String msg) throws Exception {

        EtlQuery etl = mapper.readValue(msg, EtlQuery.class);

        log.info("Procesando etl_query: [" + etl + "]");
        try {
            dwUpdate(etl.id, etl.type, etl.query);
            return true;
        } catch (Exception ex) {
            log.error("No fue posible procesar EtlQuery: [" + etl + "]. " + ex, ex);
        }
        return null;
    }

    private void dwUpdate(Long etlId, String type, String sql) {
        Connection conn = null;
        //Ejecuta queries no-select de la tabla
        if (type.startsWith("db_exe_")) {
            switch (type) {
                case "db_exe_dw_schema":
                    try {
                        conn = dataBaseConnection.getDBDWConnection();
                        Statement stmt = conn.createStatement();
                        stmt.executeUpdate(sql);
                        try {
                            stmt.close();
                        } catch (Exception ex) {
                        }
                    } catch (Exception ex) {
                        log.error("Error al ejecutar query: [" + etlId + "][" + type + "][" + sql + "]. " + ex, ex);
                    }
                    dataBaseConnection.closeDWConnection(conn);
                    break;
                default:
                    log.error("No existe una ejecucion en DB para type: [" + type + "]");
            }
        } else {//Ejecuta queries select de la tabla
            ElasticData.ElasticType et = ElasticData.ElasticType.valueOf(type);
            switch (et) {
                case pgj_content:
                    boolean isv2 = !sql.contains("ORDER_LEGACY");
                    if (isv2) {
                        conn = dataBaseConnection.getDBJuegosV2Connection();
                    } else {
                        conn = dataBaseConnection.getDBJuegosConnection();
                    }

                    Map<Long, String> idds = new HashMap();
                    try {
                        Statement stmt = conn.createStatement();
                        ResultSet rs = stmt.executeQuery(sql);
                        while (rs.next()) {
                            idds.put(rs.getLong("ID"), rs.getString("FUNCTION_TYPE"));
                        }
                        try {
                            rs.close();
                        } catch (Exception ex) {
                        }
                        try {
                            stmt.close();
                        } catch (Exception ex) {
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    if (isv2) {
                        dataBaseConnection.closeJuegosV2Connection(conn);
                    } else {
                        dataBaseConnection.closeJuegosConnection(conn);
                    }

                    log.info("ETL_ID: [" + etlId + "] - Quantity: [" + idds.size() + "]");
                    for (Long id : idds.keySet()) {
                        try {
                            statsService.sendToRabbitByFunctionType(et, id.toString(), idds.get(id));
                        } catch (Exception ex) {
                            log.error("Error al enviar al rabbit el ID: [" + id + "] para el ETL_ID: [" + etlId + "]. " + ex, ex);
                        }
                    }
                    break;
                default:
                    conn = dataBaseConnection.getDBPlanetaGuruConnection();
                    List<Long> ids = new ArrayList();
                    try {
                        Statement stmt = conn.createStatement();
                        ResultSet rs = stmt.executeQuery(sql);

                        while (rs.next()) {
                            ids.add(rs.getLong("ID"));
                        }
                        try {
                            rs.close();
                        } catch (Exception ex) {
                        }
                        try {
                            stmt.close();
                        } catch (Exception ex) {
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    dataBaseConnection.closePlanetaGuruConnection(conn);

                    log.info("ETL_ID: [" + etlId + "] - Quantity: [" + ids.size() + "]");
                    for (Long id : ids) {
                        try {
                            if (ElasticData.ElasticType.oauth_user_subscription.equals(et)) {
                                statsService.sendToRabbitByID(et, id.toString());
                            } else {
                                statsService.sendToRabbit(et, id.toString());
                            }
                        } catch (Exception ex) {
                            log.error("Error al enviar al rabbit el ID: [" + id + "] para el ETL_ID: [" + etlId + "]. " + ex, ex);
                        }
                    }
            }
        }
    }

    @Override
    public ElasticData.ElasticType getElasticType() {
        return elasticType;
    }

}
