package com.itbaf.platform.persistence.util.predicates;

import java.util.Arrays;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;

public class ObjectPredicates {

    public static Predicate combine(Predicate... expressions) {
        if (expressions.length == 0) {
            return null;
        }
        Predicate[] validExpressions = Arrays.asList(expressions).stream().filter(e -> e != null).toArray(Predicate[]::new);
        return ExpressionUtils.allOf(validExpressions);
    }

    public static Predicate or(Predicate... expressions) {
        if (expressions.length == 0) {
            return null;
        }
        Predicate[] validExpressions = Arrays.asList(expressions).stream().filter(e -> e != null).toArray(Predicate[]::new);
        return ExpressionUtils.anyOf(validExpressions);
    }

}
