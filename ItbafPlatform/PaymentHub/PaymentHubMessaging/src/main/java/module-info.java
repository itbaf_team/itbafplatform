/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module com.itbaf.platform.paymenthub.messaging {
    exports com.itbaf.platform.paymenthub.messaging;
    exports com.itbaf.platform.paymenthub.messaging.smpp;
    exports com.itbaf.platform.paymenthub.messaging.smpp.adapter;
    exports com.itbaf.platform.paymenthub.messaging.smpp.smppadapter;
    requires com.itbaf.platform.messaging;
    requires com.google.common;
    requires checker.qual;
    requires j2objc.annotations;
    requires animal.sniffer.annotations;
    requires lombok;
    requires gson;
    requires itbaf.jaxb;
    requires java.xml;
}
