/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adapter.resources;

import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.messaging.smpp.adapter.AdapterSMSMessage;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.RcvSmsRequest;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.SendSmsRequest;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.SendSmsResponse;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPMessage;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class SmppAdapterResource {

    private final RequestClient requestClient;
    private final InstrumentedObject instrumentedObject;
    private final PHProfilePropertiesService profilePropertiesService;

    @Inject
    public SmppAdapterResource(final RequestClient requestClient,
            final InstrumentedObject instrumentedObject,
            final PHProfilePropertiesService profilePropertiesService) {
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    public void sendSmsService(AdapterSMSMessage am) throws Exception {
        String smppAdapterUrl = profilePropertiesService.getValueByProviderKey("itbaf.paymenthub.adapter", "smpp.adapter.url");
        
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss0000");
        am.sendSmsRequest.setId(Long.parseLong(sdf.format(new Date())) + instrumentedObject.getAndIncrementAtomicLong("ph_smpp_sent_sms", 2, TimeUnit.SECONDS));
        
        String soap = getSendSmsSoapString(am.sendSmsRequest);
        String response = requestClient.requestSOAP(smppAdapterUrl, soap);
        log.info("Request a smppAdapterUrl. Request: [" + soap + "]. Response: [" + response + "]");
        if (response != null) {
            MessageFactory factory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(response.getBytes(Charset.forName("UTF-8"))));
            JAXBContext jbc = JAXBContext.newInstance(SendSmsResponse.class);
            Unmarshaller um = jbc.createUnmarshaller();
            JAXBElement<SendSmsResponse> element = um.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument(), SendSmsResponse.class);
            am.sendSmsResponse = element.getValue();
        }

    }

    private String getSendSmsSoapString(SendSmsRequest sr) {
        String soap = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                + "<soap:Body><ns2:SendSmsRequest xmlns:ns2=\"http://ws.sms.qubit.tv/\">"
                + "<id>" + sr.getId() + "</id>"
                + "<destinationAddr>" + sr.getDestinationAddr() + "</destinationAddr>"
                + "<originationAddr>" + sr.getOriginationAddr() + "</originationAddr>"
                + "<deliveryReceipt>" + sr.getDeliveryReceipt() + "</deliveryReceipt>"
                + "<dcsUcs2>" + sr.isDcsUcs2() + "</dcsUcs2>"
                + "<message>" + sr.getMessage() + "</message>"
                + "</ns2:SendSmsRequest></soap:Body></soap:Envelope>";
        return soap;
    }
}
