/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.bo.server.filter;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.Invokable;
import com.google.inject.Inject;
import com.itbaf.platform.pgp.services.repository.UserService;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author JF
 */
public final class PermissionFilterFactory implements ResourceFilterFactory {

    @Inject
    private UserService userService;

    @Override
    public List<ResourceFilter> create(AbstractMethod abstractMethod) {

        //keep track of required permission names 
        final ImmutableSet.Builder<String> requiredPermissionBuilder = ImmutableSet.builder();

        //get the annotation from the resource method        
        OauthPermission permission = Invokable.from(abstractMethod.getMethod()).getAnnotation(OauthPermission.class);

        //if it's present, add its value to the set 
        if (permission != null) {
            requiredPermissionBuilder.add(permission.value());
        }

        //return the new validation filter for this resource method 
        return Collections.<ResourceFilter>singletonList(new PermissionResourceFilter(userService, requiredPermissionBuilder.build()));
    }

    private static final class PermissionResourceFilter implements ResourceFilter {

        private final UserService userService;
        private final ImmutableSet<String> requiredPermissions;

        private PermissionResourceFilter(UserService userService, ImmutableSet<String> requiredPermissions) {
            this.userService=userService;
            this.requiredPermissions = requiredPermissions;
        }

        @Override
        public ContainerRequestFilter getRequestFilter() {
            return new PermissionFilter(userService, requiredPermissions);
        }

        @Override
        public com.sun.jersey.spi.container.ContainerResponseFilter getResponseFilter() {
            return null;
        }

    }
}
