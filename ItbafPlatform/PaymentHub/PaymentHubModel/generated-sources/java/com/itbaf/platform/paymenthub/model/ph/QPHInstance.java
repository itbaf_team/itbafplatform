package com.itbaf.platform.paymenthub.model.ph;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPHInstance is a Querydsl query type for PHInstance
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPHInstance extends EntityPathBase<PHInstance> {

    private static final long serialVersionUID = -966601381L;

    public static final QPHInstance pHInstance = new QPHInstance("pHInstance");

    public final MapPath<com.itbaf.platform.paymenthub.model.Provider, ChannelQuantityProvider, QChannelQuantityProvider> configurations = this.<com.itbaf.platform.paymenthub.model.Provider, ChannelQuantityProvider, QChannelQuantityProvider>createMap("configurations", com.itbaf.platform.paymenthub.model.Provider.class, ChannelQuantityProvider.class, QChannelQuantityProvider.class);

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public QPHInstance(String variable) {
        super(PHInstance.class, forVariable(variable));
    }

    public QPHInstance(Path<? extends PHInstance> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPHInstance(PathMetadata metadata) {
        super(PHInstance.class, metadata);
    }

}

