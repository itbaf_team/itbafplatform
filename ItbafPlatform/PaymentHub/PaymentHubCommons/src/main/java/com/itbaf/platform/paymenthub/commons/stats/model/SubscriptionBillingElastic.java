/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.stats.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author JF
 */
public class SubscriptionBillingElastic extends ElasticData implements Serializable {

    private static final long serialVersionUID = 20181029094901L;

    public SubscriptionBillingElastic() {
        super(ElasticData.ElasticType.subscription_billing);
    }
    public Long subscriptionId;
    public Long subscriptionRegistryId;
    public String userAccount;
    public SOP sop;
    public Date subscriptionDate;
    public Date chargedDate;
    public String currency;
    public BigDecimal fullAmount;
    public BigDecimal netAmount;
    public Tariff tariff;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("subscriptionId", subscriptionId)
                .add("subscriptionRegistryId", subscriptionRegistryId)
                .omitNullValues().toString();
    }
}
