/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.AdnetworkNotifierSimple;

/**
 *
 * @author javier
 */
public interface AdnetworkNotifierSimpleService {

    public AdnetworkNotifierSimple findById(String id) throws Exception;

    public void saveOrUpdate(AdnetworkNotifierSimple adNotifier) throws Exception;

}
