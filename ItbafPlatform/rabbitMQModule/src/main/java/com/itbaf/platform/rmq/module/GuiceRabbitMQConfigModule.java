/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.rmq.module;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import com.itbaf.platform.rmq.producer.RabbitMQProducerImpl;
import com.rabbitmq.client.ConnectionFactory;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumerImpl;
import com.itbaf.platform.services.service.PropertyManagerService;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author jordonez
 */
public class GuiceRabbitMQConfigModule extends AbstractModule {

    private static final org.apache.logging.log4j.Logger LOG = LogManager.getLogger(GuiceRabbitMQConfigModule.class);

    public GuiceRabbitMQConfigModule() {

    }

    @Override
    protected void configure() {

        //Binds
        bind(RabbitMQProducer.class).to(RabbitMQProducerImpl.class).asEagerSingleton();
        bind(RabbitMQConsumer.class).to(RabbitMQConsumerImpl.class).asEagerSingleton();
    }

    @Provides
    ConnectionFactory connectionFactory(final PropertyManagerService propertyManager, 
            final @Named("rabbitmq.virtualHost") String virtualHost) {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(propertyManager.getGeneralProperty("rabbitmq.url"));
        connectionFactory.setPort(Integer.parseInt(propertyManager.getGeneralProperty("rabbitmq.port")));
        connectionFactory.setUsername(propertyManager.getGeneralProperty("rabbitmq.username"));
        connectionFactory.setPassword(propertyManager.getGeneralProperty("rabbitmq.password"));
        connectionFactory.setVirtualHost(virtualHost);
        connectionFactory.setAutomaticRecoveryEnabled(true);
        connectionFactory.setTopologyRecoveryEnabled(true);

        return connectionFactory;
    }

}
