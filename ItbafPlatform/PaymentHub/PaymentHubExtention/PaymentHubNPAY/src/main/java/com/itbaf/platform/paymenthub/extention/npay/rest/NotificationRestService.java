package com.itbaf.platform.paymenthub.extention.npay.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.commons.bn.model.NPAYNotification;
import com.itbaf.platform.paymenthub.extention.npay.handler.ServiceProcessorHandler;
import java.util.Arrays;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/notification")
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationRestService {

    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public NotificationRestService(ServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");

    }

    @POST
    @Path("/npay/subscription")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    // @Produces(MediaType.APPLICATION_JSON)
    public Response npaySubscriptionNotification(final MultivaluedMap<String, String> formParams) {

        log.info("Notificacion de NPAY. FormParams: [" + Arrays.toString(formParams.entrySet().toArray()) + "]");

        if (formParams.getFirst("id_app") == null || formParams.getFirst("id_service") == null
                || formParams.getFirst("id_customer") == null || formParams.getFirst("id_subscription") == null) {
            log.warn("Noitificacion con parametros nulos.... [" + Arrays.toString(formParams.entrySet().toArray()) + "]");
            return Response.status(Status.BAD_REQUEST).entity("ERROR").type(MediaType.TEXT_PLAIN).build();
        }

        NPAYNotification notification = new NPAYNotification(formParams.getFirst("id_event"),
                formParams.getFirst("ipn_url"), formParams.getFirst("ipn_type"),
                formParams.getFirst("verify_sign"), formParams.getFirst("id_app"),
                formParams.getFirst("id_customer"), formParams.getFirst("id_transaction"),
                formParams.getFirst("amount"), formParams.getFirst("currency"),
                formParams.getFirst("id_subscription"), formParams.getFirst("status"),
                formParams.getFirst("id_service"), formParams.getFirst("created"),
                formParams.getFirst("updated"), formParams.getFirst("tracking_id"),
                formParams.getFirst("meta"), formParams.getFirst("subscription_date"),
                formParams.getFirst("billing_date"), formParams.getFirst("channel"),
                formParams.getFirst("cancelation_date"), formParams.getFirst("tried_at"));

        serviceProcessorHandler.processNotification(notification);
        return Response.status(Status.OK).entity("OK").type(MediaType.TEXT_PLAIN).build();
    }

}
