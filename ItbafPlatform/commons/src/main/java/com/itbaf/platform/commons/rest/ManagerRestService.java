package com.itbaf.platform.commons.rest;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import javax.ws.rs.Consumes;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;

@Path("/manager")
public abstract class ManagerRestService {

    protected final String appName;
    public static CommonDaemon paymentHubDaemon;
    private final org.apache.logging.log4j.Logger log;

    @Inject
    public ManagerRestService(String appName, String profile, String instance) {
        this.appName = appName + "." + profile + "." + instance;
        this.log = LogManager.getLogger(ManagerRestService.class);
    }

    @POST
    @Path("/service/stop")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response stopService() {
        log.info("Iniciando proceso de detencion del servicio");
        String name = Thread.currentThread().getName();
        ResponseMessage rm = new ResponseMessage();
        rm.data = paymentHubDaemon.getData();
        rm.code = "stop";
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.currentThread().setName(name + ".service.stop");
                        try {
                            Thread.sleep(1000);
                        } catch (Exception ex) {
                        }
                        paymentHubDaemon.stop();
                        log.info("Finalizado proceso de detencion del servicio");
                    } catch (Exception ex) {
                        log.error("Error al detener el servicio. " + ex, ex);
                    }
                }
            }).start();

        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Hubo un error al intentar detener el servicio. " + ex;
            log.error(rm.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        rm.status = ResponseMessage.Status.OK;
        rm.message = "Iniciado proceso de detencion del servicio...";
        return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/service/restart")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response restartService() {
        log.info("Iniciando proceso de reinicio del servicio");
        String name = Thread.currentThread().getName();
        ResponseMessage rm = new ResponseMessage();
        rm.data = paymentHubDaemon.getData();
        rm.code = "restart";
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.currentThread().setName(name + ".service.restart");
                        try {
                            Thread.sleep(1000);
                        } catch (Exception ex) {
                        }
                        paymentHubDaemon.stop();
                        paymentHubDaemon.init(null);
                        paymentHubDaemon.start();
                        log.info("Finalizado proceso de reinicio del servicio");
                        paymentHubDaemon.stop();
                    } catch (Exception ex) {
                        log.error("Error al reiniciar el servicio. " + ex, ex);
                    }
                }
            }).start();

        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Hubo un error al intentar reiniciar el servicio. " + ex;
            log.error(rm.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        rm.status = ResponseMessage.Status.OK;
        rm.message = "Iniciado proceso de reinicio del servicio...";
        return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @OPTIONS
    @Path("/infra/test")
    @Produces(MediaType.APPLICATION_JSON)
    public Response testInfraOptions() {

        log.debug("Infra... Test... OPTIONS...");
        return Response.status(Response.Status.OK)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type")
                .header("Content-Length", "0")
                .header("Host", appName)
                .build();
    }

    @POST
    @Path("/infra/test")
    @Produces(MediaType.APPLICATION_JSON)
    public abstract Response testInfraPost();
}
