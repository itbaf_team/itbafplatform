package com.itbaf.platform.paymenthub.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSOP is a Querydsl query type for SOP
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSOP extends EntityPathBase<SOP> {

    private static final long serialVersionUID = 1491975276L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSOP sOP = new QSOP("sOP");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final MapPath<String, String, StringPath> integrationSettings = this.<String, String, StringPath>createMap("integrationSettings", String.class, String.class, StringPath.class);

    protected QOperator operator;

    protected QProvider provider;

    protected QServiceMatcher serviceMatcher;

    public final EnumPath<Status> status = createEnum("status", Status.class);

    public final SetPath<com.itbaf.platform.paymenthub.model.billing.Tariff, com.itbaf.platform.paymenthub.model.billing.QTariff> tariffs = this.<com.itbaf.platform.paymenthub.model.billing.Tariff, com.itbaf.platform.paymenthub.model.billing.QTariff>createSet("tariffs", com.itbaf.platform.paymenthub.model.billing.Tariff.class, com.itbaf.platform.paymenthub.model.billing.QTariff.class, PathInits.DIRECT2);

    public QSOP(String variable) {
        this(SOP.class, forVariable(variable), INITS);
    }

    public QSOP(Path<? extends SOP> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSOP(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSOP(PathMetadata metadata, PathInits inits) {
        this(SOP.class, metadata, inits);
    }

    public QSOP(Class<? extends SOP> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.operator = inits.isInitialized("operator") ? new QOperator(forProperty("operator")) : null;
        this.provider = inits.isInitialized("provider") ? new QProvider(forProperty("provider"), inits.get("provider")) : null;
        this.serviceMatcher = inits.isInitialized("serviceMatcher") ? new QServiceMatcher(forProperty("serviceMatcher"), inits.get("serviceMatcher")) : null;
    }

    public QOperator operator() {
        if (operator == null) {
            operator = new QOperator(forProperty("operator"));
        }
        return operator;
    }

    public QProvider provider() {
        if (provider == null) {
            provider = new QProvider(forProperty("provider"));
        }
        return provider;
    }

    public QServiceMatcher serviceMatcher() {
        if (serviceMatcher == null) {
            serviceMatcher = new QServiceMatcher(forProperty("serviceMatcher"));
        }
        return serviceMatcher;
    }

}

