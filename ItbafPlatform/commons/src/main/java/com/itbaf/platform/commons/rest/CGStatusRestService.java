package com.itbaf.platform.commons.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/")
@Singleton
@lombok.extern.log4j.Log4j2
public class CGStatusRestService{


    @Inject
    public CGStatusRestService() {

    }


    @GET
    @Path("/")
    public Response testInfraPost() {

        return Response.status(Response.Status.OK).build();
    }
}
