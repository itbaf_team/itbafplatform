/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository;

import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.wallet.WalletTransaction;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author javier
 */
public interface WalletTransactionService {

    public void saveOrUpdate(WalletTransaction walletTransaction) throws Exception;

    public List<WalletTransaction> getWalletTransaction(User user);
    
    public List<WalletTransaction> getWalletTransaction(User user, Date from, Date to);

    public WalletTransaction findById(Long id) throws Exception;

    public WalletTransaction findByTransactionId(String transactionId);

    /*
    Returns last withdraw transaction
     */
    public WalletTransaction getLastWithdraw(User user, Long sopId, Date from, Date to);

}
