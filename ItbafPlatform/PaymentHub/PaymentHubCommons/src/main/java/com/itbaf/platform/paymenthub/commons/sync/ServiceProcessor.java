/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.sync;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public abstract class ServiceProcessor extends NormalizerService implements RabbitMQMessageProcessor {

    @Inject
    protected ObjectMapper mapper;
    @Inject
    protected RequestClient requestClient;
    @Inject
    protected RabbitMQProducer rabbitMQProducer;
    @Inject
    protected InstrumentedObject instrumentedObject;

    /**
     * Envia al broker las Suscripciones a sincronizar
     */
    public abstract void syncSubscriptions(Provider provider);

    /**
     * Lee del broker una suscripcion a sincronizar
     */
    public abstract void syncSubscription(Subscription s);

    public abstract ChannelQuantityProvider getChannelQuantityProvider(Provider p);

    protected Date dateNormalizer(Date date, int hours) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.HOUR_OF_DAY, hours);
        return c.getTime();
    }

    public final Date locateDate(String format, String date, TimeZone timeZone) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(timeZone);
        Date d = sdf.parse(date);
        Calendar auxx = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        auxx.setTimeInMillis(d.getTime());
        return auxx.getTime();
    }

}
