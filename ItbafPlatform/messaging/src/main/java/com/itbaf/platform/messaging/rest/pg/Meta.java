/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.messaging.rest.pg;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class Meta {

    public enum Status {
        ok,
        invalid_request,
        invalid_user_token,
        invalid_grant,
        unauthorized_client,
        unsupported_grant_type,
        invalid_scope,
        error_description,
        error_uri,
        error_internal
    }

    public Integer code;
    public Status status;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("code", code)
                .add("status", status)
                .omitNullValues().toString();
    }
}
