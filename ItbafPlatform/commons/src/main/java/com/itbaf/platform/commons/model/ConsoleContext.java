/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.model;

/**
 *
 * @author JF
 */
@lombok.Getter
@lombok.Setter
public class ConsoleContext {

    private ConsoleHttpRequest httpRequest;
    private ConsoleReportLocation reportLocation = new ConsoleReportLocation();
}
