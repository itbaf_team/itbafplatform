/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
@Entity(name = "SERVICE_MATCHER")
@Table(name = "SERVICE_MATCHER", indexes = {
    @Index(unique = true, columnList = "NAME")}, schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class ServiceMatcher implements Serializable {

    private static final long serialVersionUID = 20170809155406L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "GENERIC_NAME", nullable = true)
    private String genericName;

    @ManyToOne(optional = false)
    @JoinColumn(name = "SERVICE_ID")
    private Service service;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("name", name)
                .add("genericName", genericName)
                .add("service", service != null ? service.getId() : null)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, genericName, service);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<ServiceMatcher>()
                .over(ServiceMatcher::getId)
                .over(ServiceMatcher::getName)
                .over(ServiceMatcher::getGenericName)
                .over(ServiceMatcher::getService)
                .asserts(this, obj);
    }
}
