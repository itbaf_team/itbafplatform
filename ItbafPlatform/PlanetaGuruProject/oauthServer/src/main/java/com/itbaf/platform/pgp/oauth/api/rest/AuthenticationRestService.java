package com.itbaf.platform.pgp.oauth.api.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage.GrantType;
import com.itbaf.platform.messaging.rest.oauth.ResponseAuthMessage;
import com.itbaf.platform.pgp.oauth.api.handler.AutenticationServiceProcessorHandler;
import com.itbaf.platform.pgp.oauth.api.message.ResponseBasicMessage;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/auth")
@Singleton
@lombok.extern.log4j.Log4j2
public class AuthenticationRestService extends CommonRestService {

    private final String profile;
    private final AutenticationServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public AuthenticationRestService(@Named("guice.profile") String profile,
            final AutenticationServiceProcessorHandler serviceProcessorHandler) {
        this.profile = profile;
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "An AutenticationServiceProcessorHandler class must be provided");
    }

    public Map<String, String> getUrlValues(String url) throws UnsupportedEncodingException {
        int i = url.indexOf("?");
        Map<String, String> paramsMap = new HashMap<>();
        if (i > -1) {
            String searchURL = url.substring(url.indexOf("?") + 1);
            String params[] = searchURL.split("&");

            for (String param : params) {
                int pos = param.indexOf("=");
                String key = param.substring(0, pos);
                String value = param.substring(pos+1);
                paramsMap.put(key, value);
            }
        }

        return paramsMap;
    }

    @POST
    @Path("/token")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getToken(@FormParam("grant_type") final GrantType grantType,
            @FormParam("client_id") final String clientId,
            @FormParam("client_secret") final String clientSecret,
            @FormParam("access_token") final String accessToken,
            @FormParam("code_verifier") final String codeVerifier,
            @FormParam("code") final String code) {

        RequestTokenMessage tm = new RequestTokenMessage();
        tm.grantType = grantType;
        tm.clientId = clientId;
        tm.clientSecret = clientSecret;
        tm.accessToken = accessToken;
        tm.codeVerifier = codeVerifier;
        tm.code = code;

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-auth.token");
        } catch (Exception ex) {
        }
        log.info("getToken. RequestTokenMessage: [" + tm + "]");

        ResponseAuthMessage rm = new ResponseAuthMessage();
        try {
            if (grantType != null) {
                switch (grantType) {
                    case client_credentials:
                        rm = serviceProcessorHandler.getClientToken(tm);
                        break;
                    case authorization_code:
                        rm = serviceProcessorHandler.getUserToken(tm);
                        break;
                    default:
                        rm.error = ResponseAuthMessage.Error.invalid_grant;
                }
            }
        } catch (Exception ex) {
            rm.message = "Error al obtener Token. " + ex;
            log.error(rm.message, ex);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (rm.error == null) {
            return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/token/{scope}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteToken(@PathParam("scope") final String scope,
            @FormParam("client_id") final String clientId,
            @FormParam("access_token") final String accessToken) {

        RequestTokenMessage tm = new RequestTokenMessage();
        tm.clientId = clientId;
        tm.accessToken = accessToken;
        tm.scope = RequestTokenMessage.Scope.getScope(scope);
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-auth.token." + scope);
        } catch (Exception ex) {
        }
        log.info("deleteToken. RequestTokenMessage: [" + tm + "]");

        ResponseAuthMessage rm = new ResponseAuthMessage();

        try {
            rm = serviceProcessorHandler.deleteUserToken(tm);
        } catch (Exception ex) {
            rm.message = "Error al obtener Token. " + ex;
            log.error(rm.message, ex);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (rm.error == null) {
            return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @GET
    @Path("/authorize/{path}")
    //@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response authorize(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            @QueryParam("response_type") final String responseType,
            @QueryParam("client_id") final String clientId,
            @QueryParam("redirect_uri") String redirectUri,
            @QueryParam("redirect_pg") final Boolean redirectPG,
            @QueryParam("state") final String state,
            @QueryParam("product_id") final String productId,
            @QueryParam("code_challenge") final String codeChallenge,
            @QueryParam("code_challenge_method") final String codeChallengeMethod,
            @QueryParam("verify_mail") final String verifyMail,
            @QueryParam("token") final String udiToken,
            @QueryParam("locale") String locale,
            @QueryParam("prvdr") String prvdr,
            @QueryParam("userID") String userId,
            @QueryParam("mdsp") String mdsp,
            @QueryParam("msfw") String msfw,
            @QueryParam("suscrod") String suscrod) {

        if (locale == null) {
            String ip = request.getHeader("X-Forwarded-For");
            if (ip == null || ip.length() < 1) {
                ip = request.getRemoteAddr();
            }
            locale = serviceProcessorHandler.getLanguage(ip);
        }
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-auth.authorize." + path);
        } catch (Exception ex) {
        }
        log.info("authorize [" + path + "]. responseType: [" + responseType + "] - clientId: [" + clientId + "] - state: [" + state + "] - productId: [" + productId + "] - verifyMail: [" + verifyMail + "] - udiToken: [" + udiToken + "] - codeChallenge: [" + codeChallenge + "] - codeChallengeMethod: [" + codeChallengeMethod + "] - locale: [" + locale + "] - prvdr: [" + prvdr + "] - msfw: [" + msfw + "] - userId: [" + userId + "] - mdsp: [" + mdsp + "] - redirectPG: [" + redirectPG + "] - redirectUri: [" + redirectUri + "]");
        ResponseAuthMessage rm = new ResponseAuthMessage();
        try {
            if (redirectUri != null) {
                Map<String, String> uriparams = getUrlValues(redirectUri);

                if (uriparams.containsKey("msfw")) {
                    uriparams.remove("msfw");
                    StringBuilder sb = new StringBuilder();
                    for (Map.Entry<String,String> entry : uriparams.entrySet()){
                        if (sb.length() > 0) {
                            sb.append("&");
                        }
                        sb.append(String.format("%s=%s",
                                entry.getKey(),
                                entry.getValue()
                        ));
                    }
                    int paramsStart = redirectUri.indexOf('?');
                    redirectUri = redirectUri.substring(0, paramsStart) + "?"+ sb.toString();
                    uriparams = getUrlValues(redirectUri);
                }

                if (uriparams.containsKey("t")){
                    String t_param = uriparams.get("t");
                    String encoded_t_param = URLEncoder.encode(t_param, "UTF-8");
                    uriparams.put("t", encoded_t_param);

                    StringBuilder sb = new StringBuilder();
                    for (Map.Entry<String,String> entry : uriparams.entrySet()){
                        if (sb.length() > 0) {
                            sb.append("&");
                        }

                        sb.append(String.format("%s=%s",
                                entry.getKey(),
                                entry.getValue()
                        ));
                    }
                    int paramsStart = redirectUri.indexOf('?');
                    redirectUri = redirectUri.substring(0, paramsStart) + "?"+ sb.toString();
                }
            }
        }catch (Exception ex){
            rm.message = "Error al corregir parametro t. " + ex;
            log.error(rm.message, ex);
        }
        try {
            rm = serviceProcessorHandler.getAuthorizeCode(request, responseType, clientId,
                    redirectUri, state, productId, verifyMail, codeChallenge,
                    codeChallengeMethod, redirectPG, prvdr, userId, mdsp, msfw, suscrod);
            // log.info("Fin getAuthorizeCode");
            log.info("oauth - authorize - rm.url ["+rm.url+"]");
            if (rm.url != null) {
                java.net.URI location = new java.net.URI(rm.url);
                return Response.status(302).location(location).build();
            }
        } catch (Exception ex) {
            rm.message = "Error al obtener AuthorizeCode. " + ex;
            log.error(rm.message, ex);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (rm.error == null) {
            java.net.URI location = null;
            try {
                switch (path) {
                    case "signin":
                        if (udiToken != null) {
                            location = new java.net.URI("../oauth/signin/login/" + rm.message + "?token=" + udiToken + "&locale=" + locale);
                        } else {
                            log.info("Preparando redirect UDI");
                            // Extraer parametro t del redirectUri porque llega como json y genera problema
                            // ejemplo redirectUri: [http://planeta.guru/?msfw=vn1/2y6qsShLXAZA2VcyE6ZrfHpubabXDjaqxn7nS08=&affn=AA230&t={\"source\":\"googleads\",\"medium\":\"cpc_source\",\"campaign\":\"int_games\"}&gclid=EAIaIQobChMIkYal6Jm87wIVtjO5Bh3XMQZlEAEYASAAEgLPLvD_BwE&locale=es_PE&prvdr=pepe]"



                            String url = request.getRequestURL().toString().replace(request.getRequestURI(), "") + "/oauth/signin/login/" + rm.message + "?locale=" + locale;
                            String protocol = request.getHeader("X-Forwarded-Proto");
                            if (protocol != null) {
                                protocol = protocol + "://";
                                if (!url.startsWith(protocol) && url.startsWith("http://")) {
                                    url = protocol + url.substring(7, url.length());
                                }
                            }
                            log.info("URL_REQUEST_FINAL: " + url);
                            url = URLEncoder.encode(url, "UTF-8");
                            if(msfw != null){
                                // Si tiene msfw se lo paso a udi
                                location = new java.net.URI(serviceProcessorHandler.getUdiURL(productId) + "/token?msfw="+URLEncoder.encode(msfw, "UTF-8")+"&callback=" + url);
                            }else{
                                location = new java.net.URI(serviceProcessorHandler.getUdiURL(productId) + "/token?callback=" + url);
                            }
                            log.info("Listo redirect UDI");
                        }
                        break;
                    case "signup":
                        if (udiToken != null) {
                            location = new java.net.URI("../oauth/signup/new/" + rm.message + "?token=" + udiToken + "&locale=" + locale);
                        } else {
                            String url = request.getRequestURL().toString().replace(request.getRequestURI(), "") + "/oauth/signup/new/" + rm.message + "?locale=" + locale;
                            String protocol = request.getHeader("X-Forwarded-Proto");
                            if (protocol != null) {
                                protocol = protocol + "://";
                                if (!url.startsWith(protocol) && url.startsWith("http://")) {
                                    url = protocol + url.substring(7, url.length());
                                }
                            }
                            url = URLEncoder.encode(url, "UTF-8");
                            location = new java.net.URI(serviceProcessorHandler.getUdiURL(productId) + "/token?callback=" + url);
                        }
                        break;
                    case "password":
                        location = new java.net.URI("../oauth/signin/resetPassword/" + rm.message + "?locale=" + locale);
                        break;
                }
                if (location != null) {
                    log.info("Creacion Us_PG cookie");
                    log.info("Redireccionando a ["+location+"]");
                    NewCookie cookie = new NewCookie("Us_PG", rm.message, "/",
                            null, "Us_PG_TK", 1 * 60 * 60, !"dev".equals(profile));
                    log.info("Lista Us_PG cookie");
                    return Response.status(302).location(location).cookie(cookie).build();
                }
            } catch (Exception ex) {
                log.error("Error al generar location. " + ex, ex);
            }
        }

        return Response.status(Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @GET
    @Path("/tyc")
    @Produces(MediaType.TEXT_HTML)
    public Response sendTyC(@Context HttpServletRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-auth.tyc");
        } catch (Exception ex) {
        }
        log.info("sendTyC.");
        String html = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis ultricies lacus sed turpis tincidunt id. Ut tellus elementum sagittis vitae et. Ullamcorper sit amet risus nullam eget. Proin sed libero enim sed faucibus turpis in eu mi. Sagittis orci a scelerisque purus semper eget duis at tellus. Lacus sed viverra tellus in hac habitasse platea. Porttitor massa id neque aliquam vestibulum morbi blandit. Ornare aenean euismod elementum nisi. Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae. Sagittis orci a scelerisque purus semper eget duis at. Dolor morbi non arcu risus quis varius. Congue quisque egestas diam in arcu. Vulputate sapien nec sagittis aliquam malesuada bibendum. Neque gravida in fermentum et sollicitudin."
                + "<br/>"
                + "A diam sollicitudin tempor id eu nisl nunc. Sed vulputate mi sit amet. Porttitor rhoncus dolor purus non enim. Urna condimentum mattis pellentesque id nibh tortor id aliquet lectus. Auctor neque vitae tempus quam. Tellus elementum sagittis vitae et leo duis ut diam quam. Pellentesque habitant morbi tristique senectus et netus. Ultricies leo integer malesuada nunc. Semper auctor neque vitae tempus quam pellentesque nec nam. Rhoncus mattis rhoncus urna neque viverra justo nec. Volutpat blandit aliquam etiam erat velit. Elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique. Tellus elementum sagittis vitae et leo duis ut diam quam. Suspendisse in est ante in nibh mauris cursus. Id neque aliquam vestibulum morbi. Eget nulla facilisi etiam dignissim diam quis enim lobortis."
                + "<br/>"
                + "Libero volutpat sed cras ornare arcu. Volutpat ac tincidunt vitae semper quis. Aliquam eleifend mi in nulla. Duis at consectetur lorem donec massa sapien faucibus et molestie. Tempus imperdiet nulla malesuada pellentesque elit eget gravida cum sociis. Arcu dictum varius duis at consectetur. Enim eu turpis egestas pretium. Non pulvinar neque laoreet suspendisse interdum consectetur. Consectetur libero id faucibus nisl tincidunt eget nullam non nisi. Non quam lacus suspendisse faucibus. Consequat ac felis donec et odio pellentesque diam volutpat. Nulla malesuada pellentesque elit eget gravida cum sociis natoque penatibus. Quisque id diam vel quam elementum pulvinar."
                + "<br/>"
                + "Morbi tristique senectus et netus et malesuada fames ac turpis. Turpis egestas maecenas pharetra convallis posuere morbi leo urna molestie. Egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam. Ac odio tempor orci dapibus ultrices in. Nisl suscipit adipiscing bibendum est ultricies integer quis. Tristique sollicitudin nibh sit amet. Ipsum dolor sit amet consectetur adipiscing. Sollicitudin aliquam ultrices sagittis orci a scelerisque. Sed adipiscing diam donec adipiscing tristique risus. Sed sed risus pretium quam vulputate dignissim. Neque volutpat ac tincidunt vitae semper quis lectus nulla at. Venenatis tellus in metus vulputate eu scelerisque."
                + "<br/>"
                + "Ornare quam viverra orci sagittis eu volutpat odio facilisis. Et ligula ullamcorper malesuada proin libero nunc consequat. Sed odio morbi quis commodo odio aenean sed adipiscing diam. Non curabitur gravida arcu ac. Mattis aliquam faucibus purus in massa tempor. Pellentesque id nibh tortor id aliquet lectus proin. Nunc eget lorem dolor sed viverra ipsum. Nibh ipsum consequat nisl vel pretium. Tincidunt eget nullam non nisi est. Enim diam vulputate ut pharetra sit amet aliquam id. Blandit turpis cursus in hac habitasse platea dictumst quisque sagittis. Justo donec enim diam vulputate ut. Enim neque volutpat ac tincidunt vitae. Elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi. Purus in massa tempor nec feugiat. Feugiat in fermentum posuere urna nec tincidunt praesent semper.";

        return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
    }

    @POST
    @Path("/country/ip")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCountryByIP(@Context HttpServletRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-auth.country.ip");
        } catch (Exception ex) {
        }

        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() < 1) {
            ip = request.getRemoteAddr();
        }
        log.info("getCountryByIP. [" + ip + "]");
        ResponseAuthMessage rm = new ResponseAuthMessage();
        try {
            String countryCode = null;
            if (ip != null && ip.length() > 1) {
                countryCode = serviceProcessorHandler.getCountryCodeByIP(ip);
            }
            if (countryCode == null) {
                log.warn("No fue posible obtener el countryCode para la IP: [" + ip + "]. Se usara 'AR' por default.");
                countryCode = "AR";
            }
            ResponseBasicMessage rb = new ResponseBasicMessage();
            rb.credentials = null;
            rb.country = countryCode;
            return Response.status(Status.OK).entity(rb).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        } catch (Exception ex) {
            rm.message = "Error al obtener el pais por IP: [" + ip + "]. " + ex;
            log.error(rm.message, ex);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }
    }
}
