/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.cdag.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.cdag.model.notification.BillingEventData;
import com.itbaf.platform.paymenthub.extention.cdag.model.notification.BillingEventDataStatus;
import com.itbaf.platform.paymenthub.extention.cdag.ws.notification.NotificarEvento;
import com.itbaf.platform.paymenthub.extention.cdag.ws.notification.Notification;
import com.itbaf.platform.paymenthub.extention.cdag.model.notification.SubscriptionEventData;
import com.itbaf.platform.paymenthub.extention.cdag.model.notification.UnsubscribeEventData;
import com.itbaf.platform.paymenthub.extention.cdag.model.recobro.InfotainmentChargeReportEntry;
import com.itbaf.platform.paymenthub.extention.cdag.resources.CDAGResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.messaging.smpp.adapter.AdapterSMSMessage;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.SendSmsRequest;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.sms.AdapterSmsLogReceived;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import io.jsonwebtoken.lang.Collections;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final CDAGResources cdagResources;
    private final Map<String, Provider> providerMap;
    private final SubscriptionService subscriptionService;
    private final PHProfilePropertiesService phProfilePropertiesService;

    @Inject
    public ServiceProcessorHandler(final Map<String, Provider> providerMap,
            final CDAGResources cdagResources,
            final SubscriptionService subscriptionService,
            final PHProfilePropertiesService phProfilePropertiesService) {
        this.cdagResources = Validate.notNull(cdagResources, "A CDAGResources class must be provided");
        this.providerMap = Validate.notNull(providerMap, "A Map<String, Provider> class must be provided");
        this.subscriptionService = Validate.notNull(subscriptionService, "A SubscriptionService class must be provided");
        this.phProfilePropertiesService = Validate.notNull(phProfilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    public void processNotification(String countryCode, final NotificarEvento notifications) {

        if (countryCode == null || countryCode.length() != 2) {
            log.error("El Codigo de pais no es valido. countryCode: [" + countryCode + "]");
            return;
        }

        Provider p = providerMap.get(countryCode.toUpperCase());
        if (p == null) {
            log.error("No hay configurado un provider para CountryCode: [" + countryCode + "]");
            return;
        }
        notifications.setCountryCode(countryCode);
        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notifications);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notifications);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notifications + "]. " + ex, ex);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {
        NotificarEvento notifications;
        try {
            notifications = mapper.readValue(message, NotificarEvento.class);
            Thread.currentThread().setName(notifications.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }

        Provider provider = providerMap.get(notifications.getCountryCode().toUpperCase());
        final String threadName = Thread.currentThread().getName();
        for (Notification notification : notifications.getNotifications()) {
            Thread.currentThread().setName(threadName + "-" + notification.getTipoEvento() + "-" + notification.getTid());
            try {
                if (notification.getTid() != null && !notification.getTid().toLowerCase().startsWith("cdag")) {
                    notification.setTid("CDAG." + notification.getTid());
                }
                Thread.currentThread().setName(threadName + "-" + notification.getTipoEvento() + "-" + notification.getTid());
                if (processNotification(notification, provider) == null) {
                    if (notifications.getNotifications().size() == 1) {
                        return null;
                    }
                    NotificarEvento aux = new NotificarEvento();
                    aux.setCountryCode(notifications.getCountryCode());
                    aux.getNotifications().add(notification);
                    String jsonResponse;
                    jsonResponse = mapper.writeValueAsString(aux);
                    if (jsonResponse != null) {
                        return rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + provider.getName(), jsonResponse);
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar notificacion: [" + notification + "]. " + ex, ex);
                return null;
            }
        }

        return true;
    }

    private Boolean processNotification(Notification notification, Provider provider) throws Exception {
        Boolean result = null;
        switch (notification.getTipoEvento()) {
            case 0: // Alta de servicio
                //log.info("Notificacion de alta. [" + notification.getDatoEvento() + "]");
                SubscriptionEventData sevent = new SubscriptionEventData(notification.getDatoEvento());
                result = processSubscriptionNotification(notification, sevent, provider);
                break;
            case 1: // Baja de servicio
                //log.info("Notificacion de baja. [" + notification.getDatoEvento() + "]");
                UnsubscribeEventData uevent = new UnsubscribeEventData(notification.getDatoEvento());
                result = processUnsubscriptionNotification(notification, uevent, provider);
                break;
            case 2: // Billing
                //log.info("Notificacion de cobro. [" + notification.getDatoEvento() + "]");
                BillingEventData bevent = new BillingEventData(notification.getDatoEvento());
                result = processBillingNotification(notification, bevent, provider);
                break;
            default:
                throw new IllegalStateException("updateType inesperado: [" + notification.getTipoEvento() + "]");
        }
        return result;
    }

    private Boolean processSubscriptionNotification(Notification notification, SubscriptionEventData event, Provider provider) {

        Map<String, String> settings = new HashMap();
        settings.put("cdag_serviceId", event.getServiceId());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        if (sop == null) {
            log.error("No existe un SOP configurado para cdag_serviceId: [" + event.getServiceId() + "]");
            return null;
        }

        Tariff t = sop.getMainTariff();
        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(event.getSubscriptionDate());
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(MessageType.SUBSCRIPTION_ACTIVE);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getTid());
            msg.setTransactionId(notification.getTid());
            msg.setChannelIn(null);
            msg.setUserAccount(event.getMsisdn());
            msg.setRealUserAccount(true);

            try {
                Extra e = new Extra();
                boolean xx = false;
                if (event.getKeyword() != null) {
                    e.stringData.put("keyword_in", event.getKeyword());
                    xx = true;
                }
                if (event.getPautaId() != null) {
                    e.stringData.put("pauta", event.getPautaId());
                    xx = true;
                }
                if (event.getPautaStatus() != null) {
                    e.stringData.put("pauta_status", event.getPautaStatus());
                    xx = true;
                }
                if (lastMTSent(sop, e)) {
                    xx = true;
                }
                if (xx) {
                    msg.setExtra(e);
                }
            } catch (Exception ex) {
            }

            String jsonResponse = mapper.writeValueAsString(msg);
            if (rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse)) {
                String subscriptionMessage = sop.getIntegrationSettings().get("sms.message.subscription");
                if (subscriptionMessage != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    Calendar c = Calendar.getInstance();
                    c.setTimeInMillis(t.getToDate().getTime());
                    c.add(Calendar.DAY_OF_YEAR, -1);
                    subscriptionMessage = subscriptionMessage.replace("<fecha>", sdf.format(c.getTime()))
                            .replace("<precio>", t.getFullAmount().toPlainString());

                    AdapterSMSMessage am = new AdapterSMSMessage();
                    am.sopId = sop.getId();
                    am.operatorName = sop.getOperator().getName();
                    am.providerName = sop.getProvider().getName();
                    am.serviceName = sop.getServiceMatcher().getName();
                    am.sendSmsRequest = new SendSmsRequest();
                    am.sendSmsRequest.setDcsUcs2(Boolean.TRUE);
                    am.sendSmsRequest.setDeliveryReceipt(0);
                    am.sendSmsRequest.setDestinationAddr(event.getMsisdn());
                    am.sendSmsRequest.setOriginationAddr(sop.getIntegrationSettings().get("shortcode"));
                    am.sendSmsRequest.setMessage(subscriptionMessage);

                    jsonResponse = mapper.writeValueAsString(am);
                    if (rabbitMQProducer.messageSender(CommonFunction.ADAPTER_SMS_SEND + am.providerName, jsonResponse)) {
                        Thread.sleep(500);
                        String renew = sop.getIntegrationSettings().get("sms.message.renew");
                        renew = renew.replace("<fecha>", sdf.format(c.getTime()))
                                .replace("<precio>", t.getFullAmount().toPlainString());
                        am.sendSmsRequest.setMessage(renew);
                        am.createdDate = new Date();
                        jsonResponse = mapper.writeValueAsString(am);
                        return rabbitMQProducer.messageSender(CommonFunction.ADAPTER_SMS_SEND + am.providerName, jsonResponse);
                    }
                }
                return true;
            }
        } catch (Exception ex) {
            log.error("Error al procesar CDAGNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    private boolean lastMTSent(SOP sop, Extra e) {
        if (sop.getIntegrationSettings().get("sms.message.subscription") != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            e.stringData.put("lastMTSent", sdf.format(new Date()));
            return true;
        }
        return false;
    }

    private Boolean processUnsubscriptionNotification(Notification notification, UnsubscribeEventData event, Provider provider) {

        Map<String, String> settings = new HashMap();
        settings.put("cdag_serviceId", event.getServiceId());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        if (sop == null) {
            log.error("No existe un SOP configurado para cdag_serviceId: [" + event.getServiceId() + "]");
            return null;
        }

        BigDecimal fullAmount = null;
        BigDecimal netAmount = null;
        Tariff t = sop.getMainTariff();
        if (t != null) {
            fullAmount = t.getFullAmount();
            netAmount = t.getNetAmount();
        }

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setToDate(event.getUnsubscriptionDate());
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t != null ? t.getId() : null);
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getTid());
            msg.setTransactionId(notification.getTid());
            msg.setChannelIn(null);
            msg.setUserAccount(event.getMsisdn());

            try {
                if (event.getKeyword() != null) {
                    Extra e = new Extra();
                    msg.setExtra(e);
                    e.stringData.put("keyword_out", event.getKeyword());
                }
            } catch (Exception ex) {
            }
            try {
                Extra e = new Extra();
                boolean xx = false;
                if (event.getKeyword() != null) {
                    e.stringData.put("keyword_out", event.getKeyword());
                    xx = true;
                }
                if (event.getPautaId() != null) {
                    e.stringData.put("pauta", event.getPautaId());
                    xx = true;
                }
                if (event.getPautaStatus() != null) {
                    e.stringData.put("pauta_status", event.getPautaStatus());
                    xx = true;
                }
                if (xx) {
                    msg.setExtra(e);
                }
            } catch (Exception ex) {
            }

            String jsonResponse = mapper.writeValueAsString(msg);
            if (rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse)) {
                String unsubscriptionMessage = sop.getIntegrationSettings().get("sms.message.unsubscription");
                if (unsubscriptionMessage != null) {
                    AdapterSMSMessage am = new AdapterSMSMessage();
                    am.sopId = sop.getId();
                    am.operatorName = sop.getOperator().getName();
                    am.providerName = sop.getProvider().getName();
                    am.serviceName = sop.getServiceMatcher().getName();
                    am.sendSmsRequest = new SendSmsRequest();
                    am.sendSmsRequest.setDcsUcs2(Boolean.TRUE);
                    am.sendSmsRequest.setDeliveryReceipt(0);
                    am.sendSmsRequest.setDestinationAddr(event.getMsisdn());
                    am.sendSmsRequest.setOriginationAddr(sop.getIntegrationSettings().get("shortcode"));
                    am.sendSmsRequest.setMessage(unsubscriptionMessage);

                    jsonResponse = mapper.writeValueAsString(am);
                    return rabbitMQProducer.messageSender(CommonFunction.ADAPTER_SMS_SEND + am.providerName, jsonResponse);
                }
                return true;
            }
        } catch (Exception ex) {
            log.error("Error al procesar CDAGNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private Boolean processBillingNotification(Notification notification, BillingEventData event, Provider provider) {
        Map<String, String> settings = new HashMap();
        settings.put("cdag_serviceId", event.getServiceId());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        int control = 0;
        if (sop == null) {
            settings = new HashMap();
            settings.put("cdag_renewal_serviceId", event.getServiceId());
            sop = sopService.findBySettings(provider.getName(), settings);
            control = 2;
        } else {
            control = 1;
        }

        if (sop == null) {
            log.error("No existe un SOP configurado para cdag_serviceId: [" + event.getServiceId() + "]");
            return null;
        }

        if (!BillingEventDataStatus.Ok.equals(event.getStatus())) {
            if (BillingEventDataStatus.InvalidNumber.equals(event.getStatus())) {
                try {
                    log.info("Billing Notification. MSISDN invalido en la telco. [" + notification + "]");
                    PaymentHubMessage msg = new PaymentHubMessage();
                    Boolean controls = null;
                    try {
                        msg.setCodeCountry(provider.getCountry().getCode());
                        msg.setCurrencyId(provider.getCountry().getCurrency());
                        msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
                        msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
                        msg.setToDate(new Date());
                        msg.setMessageType(MessageType.SUBSCRIPTION_BLACKLIST);
                        msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
                        msg.setExternalUserAccount(null);
                        msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
                        msg.setSopId(sop.getId());
                        msg.setExternalId(notification.getTid());
                        msg.setTransactionId(notification.getTid());
                        msg.setChannelIn(null);
                        msg.setUserAccount(event.getMsisdn());
                        msg.setRealUserAccount(true);

                        try {
                            Extra e = new Extra();
                            boolean xx = false;
                            if (event.getPautaId() != null) {
                                e.stringData.put("pauta", event.getPautaId());
                                xx = true;
                            }
                            if (event.getPautaStatus() != null) {
                                e.stringData.put("pauta_status", event.getPautaStatus());
                                xx = true;
                            }
                            if (xx) {
                                msg.setExtra(e);
                            }
                        } catch (Exception ex) {
                        }

                        String jsonResponse = mapper.writeValueAsString(msg);
                        if (jsonResponse != null) {
                            controls = rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
                        }
                    } catch (Exception ex) {
                        log.error("Error al procesar CDAGNotification: [" + notification + "]. " + ex, ex);
                        return controls;
                    }
                    cdagResources.unsubscribe(event.getMsisdn(), sop);
                    return controls;
                } catch (Exception ex) {
                    log.error("Error al cambiar el estado de la suscripcion para: [" + notification + "]", ex);
                    return null;
                }
            } else {
                log.info("Billing Notification. No procesada.");
            }
            return true;
        }

        BigDecimal fullAmount = event.getCharged();
        BigDecimal netAmount;

        if (fullAmount == null || fullAmount.compareTo(BigDecimal.ZERO) < 1) {
            // log.info("Billing Notification. No procesada. fullAmount: [" + fullAmount + "]");
            return null;
        }

        Tariff t = sop.getMainTariff();
        fullAmount = t.getFullAmount();
        netAmount = t.getNetAmount();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(event.getChargedDate());
            msg.setChargedDate(event.getChargedDate());
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);

            if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }

            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setSopId(sop.getId());

            msg.setTransactionId("CDAG." + (event.getChargedTid() == null ? notification.getTid() : event.getChargedTid()));
            if ("PY".equals(sop.getProvider().getCountry().getCode().toUpperCase())) {
                msg.setTransactionId(msg.getTransactionId() + "." + event.getTid() + "." + event.getMsisdn());
            }

            msg.setChannelIn(null);
            msg.setUserAccount(event.getMsisdn());
            if (control == 1) {
                //Es el primer cobro de la suscripcion
                msg.setRealUserAccount(true);
                msg.setExternalId(notification.getTid());
            }
            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_BILLING, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar CDAGNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;

        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        if (subscription.getSubscriptionRegistry() != null && subscription.getSubscriptionRegistry().getId() != null
                && subscription.getSubscriptionRegistry().getUnsubscriptionDate() != null
                && (Subscription.Status.PENDING.equals(subscription.getStatus()) || Subscription.Status.ACTIVE.equals(subscription.getStatus()))) {
            rm.status = ResponseMessage.Status.OK_NS;
            rm.message = "La suscripcion ya tiene un estado valido";
        } else {
            /*ResponseMessage aux = getSubscriptionStatus(subscription);
            switch (aux.status) {
                case ACTIVE:
                case PENDING:
                case BLACKLIST:
                case LOCKED:
                    rm.status = aux.status;
                    rm.message = aux.message;
                    break;
                case ERROR:
                    rm.message = aux.message;
                    break;
                case REMOVED:
                case OK_NS:*/
                    //CDAG PY, hay q ver si se sigue llamando al metodo "createSubscription"
                    rm.status = ResponseMessage.Status.OK_NS;
                    subscription.setStatus(Subscription.Status.PENDING);
                 /*   break;
            }*/
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        if (subscription.getUserAccount().startsWith("CDAG")) {
            rm.status = ResponseMessage.Status.OK_NS;
            rm.message = "A espera de notificacion del CDAG. De existir.";
            subscription.setStatus(Subscription.Status.REMOVED);
        } else {

            boolean result = cdagResources.subscriptionStatus(subscription.getUserAccount(), subscription.getSop());
            if (result) {
                subscription.setStatus(Subscription.Status.ACTIVE);
                rm.status = ResponseMessage.Status.ACTIVE;
            } else {
                if (Subscription.Status.ACTIVE.equals(subscription.getStatus())) {
                    subscription.setStatus(Subscription.Status.CANCELLED);
                } else {
                    subscription.setStatus(Subscription.Status.REMOVED);
                }
                rm.status = ResponseMessage.Status.REMOVED;
            }
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        return rm;
    }

    public ResponseMessage sendPin(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);
        switch (aux.status) {
            case ACTIVE:
            case PENDING:
            case BLACKLIST:
            case LOCKED:
                rm.status = aux.status;
                rm.message = aux.message;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case REMOVED:
            /*  ResponseMessage.Status result = npayResources.sendSubscriptionPin(
                        profilePropertiesService.getCommonProperty(subscription.getSop().getProvider().getId(), "npay.carrier"),
                        subscription);
                if (result != null) {
                    rm.status = result;
                }
                break;*/
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;

        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        cdagResources.unsubscribe(subscription.getUserAccount(), subscription.getSop());
        subscription.setStatus(Subscription.Status.REMOVED);
        rm.status = ResponseMessage.Status.OK;
        rm.data = mapper.writeValueAsString(subscription);

        return rm;
    }

    public ResponseMessage validatePin(Subscription subscription, String pin) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);

        switch (aux.status) {
            case ACTIVE:
            case PENDING:
            case BLACKLIST:
            case LOCKED:
                rm.status = aux.status;
                rm.message = aux.message;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case REMOVED:
                /*     ResponseMessage result = npayResources.validatePin(
                        profilePropertiesService.getCommonProperty(subscription.getSop().getProvider().getId(), "npay.carrier"),
                        subscription, pin);
                if (result != null) {
                    rm.status = result.status;
                    rm.message = result.message;
                }*/
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);

        return rm;
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        if (!isNumber(subscription.getUserAccount()) && subscription.getUserAccount().startsWith("CDAG")) {

        } else {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        }
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    public Boolean processRecobro(InfotainmentChargeReportEntry rv) {
        log.info("Procesando cobro: [" + rv + "]");

        SOP sop = sopService.getSOPByTariff(rv.SER_TM);
        if (sop == null) {
            log.error("No hay configurado un SOP para el SerCode: [" + rv.SER_TM + "]");
            return null;
        }

        try {
            if (rv.SER_TH != null && rv.SER_TH.isEmpty()) {
                rv.SER_TH = null;
            }
            rv.MSISDN = userAccountNormalizer(rv.MSISDN, sop.getProvider());
            if (rv.SER_TH != null && rv.DEUDA.compareTo(BigDecimal.ZERO) > 0) {
                return registerBilling(rv, MessageType.BILLING_PARTIAL, sop);
            } else if (rv.SER_TH != null && rv.DEUDA.compareTo(BigDecimal.ZERO) == 0) {
                return registerBilling(rv, MessageType.BILLING_PARTIAL, sop);
            } else if (rv.SER_TH == null && rv.DEUDA.compareTo(BigDecimal.ZERO) == 0) {
                return registerBilling(rv, MessageType.BILLING_TOTAL, sop);
            } else {
                log.fatal("Entrada de recobro de Infotainment inconsistente: [" + rv + "]");
                return false;
            }
        } catch (Exception ex) {
            log.error("Error al procesar recobro: [" + rv + "]. " + ex, ex);
        }

        return null;
    }

    private Boolean registerBilling(InfotainmentChargeReportEntry rv, MessageType messageType, SOP sop) {

        Tariff t = null;
        if (MessageType.BILLING_TOTAL.equals(messageType)) {
            for (Tariff ts : sop.getTariffs()) {
                if (ts.getExternalId().equals(rv.SER_TM.trim())) {
                    t = ts;
                    break;
                }
            }
        } else {
            for (Tariff ts : sop.getTariffs()) {
                if (ts.getExternalId().equals(rv.SER_TH.trim())) {
                    t = ts;
                    break;
                }
            }
        }

        if (t == null) {
            log.fatal("No fue posible obtener una tarifa del sop: [" + sop + "] para InfotainmentChargeReportEntry: [" + rv + "]");
            return null;
        }

        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();
        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(sop.getProvider().getCountry().getCode());
            msg.setCurrencyId(sop.getProvider().getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);

            String fecha = "20" + rv.COD_AUT.substring(0, 12);
            msg.setFromDate(locateDate("yyyyMMddHHmmss", fecha, TimeZone.getTimeZone("America/Buenos_Aires")));
            msg.setChargedDate(msg.getFromDate());

            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(messageType);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setSopId(sop.getId());
            msg.setTransactionId("CDAG." + rv.TID);
            if (MessageType.BILLING_PARTIAL.equals(messageType)) {
                msg.setTransactionId(msg.getTransactionId() + "." + rv.COD_AUT);
            }
            msg.setChannelIn(null);
            msg.setUserAccount(rv.MSISDN);
            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar InfotainmentChargeReportEntry: [" + rv + "]. " + ex, ex);
        }

        return null;
    }

    public Boolean smsSubscribe(SOP sop, AdapterSmsLogReceived ar) {
        Subscription s = subscriptionManager.getSubscriptionByUserAccount(ar.getMessage().rcvSmsRequest.getOriginationAddr(), sop);
        if (s != null && Subscription.Status.ACTIVE.equals(s.getStatus())) {
            return true;
        }
        cdagResources.checkAndBill(sop, ar.getMessage().rcvSmsRequest.getOriginationAddr(), ar.getTid(), "3");
        return true;
    }

    public void findActiveRenew(Provider p) {
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int startHour = Integer.parseInt(phProfilePropertiesService.getCommonProperty(p.getId(), "cdag.renew.hour.start"));
        int endHour = Integer.parseInt(phProfilePropertiesService.getCommonProperty(p.getId(), "cdag.renew.hour.end"));

        if (hour < startHour || hour >= endHour) {
            return;
        }
        Thread.currentThread().setName(Thread.currentThread().getName() + "." + hour);
        log.info("Busqueda de Activos para SMS de renovacion de: [" + p.getName() + "]");
        List<SOP> sops = sopService.getSOPsByProviderName(p.getName());
        if (Collections.isEmpty(sops)) {
            log.warn("No hay una lista de SOPs: [" + p.getName() + "][]");
            return;
        }
        List<BigInteger> subscriptions = new ArrayList();
        for (SOP sop : sops) {
            log.info("Buscando suscripciones activas para: [" + sop.getId() + "]");
            try {
                List<BigInteger> ss = subscriptionService.getSubscriptionsRenew(sop, sop.getMainTariff().getFrecuency());
                if (ss != null) {
                    subscriptions.addAll(ss);
                }
            } catch (Exception ex) {
            }
        }
        log.info("Activos para SMS de renovacion: [" + subscriptions.size() + "]");
        for (BigInteger bi : subscriptions) {
            hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            if (hour < startHour || hour >= endHour) {
                log.error("No fue posible terminar el envio de MTs");
                return;
            }
            Long l = bi.longValue();
            RLock lock = instrumentedObject.lockObject("S_MT_" + l, 120);
            long aux = instrumentedObject.getAtomicLong("S_MT_B_" + l, 700);
            if (aux == 0) {
                instrumentedObject.getAndIncrementAtomicLong("S_MT_B_" + l, 700);
                try {
                    Subscription s = subscriptionService.findById(l);
                    if (Subscription.Status.ACTIVE.equals(s.getStatus())) {
                        SOP sop = s.getSop();

                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                        Calendar c = Calendar.getInstance();
                        c.setTimeInMillis(sop.getMainTariff().getToDate().getTime());
                        c.add(Calendar.DAY_OF_YEAR, -1);
                        String renew = sop.getIntegrationSettings().get("sms.message.renew");
                        renew = renew.replace("<fecha>", sdf.format(c.getTime()))
                                .replace("<precio>", sop.getMainTariff().getFullAmount().toPlainString());

                        AdapterSMSMessage am = new AdapterSMSMessage();
                        am.sopId = sop.getId();
                        am.operatorName = sop.getOperator().getName();
                        am.providerName = sop.getProvider().getName();
                        am.serviceName = sop.getServiceMatcher().getName();
                        am.sendSmsRequest = new SendSmsRequest();
                        am.sendSmsRequest.setDcsUcs2(Boolean.TRUE);
                        am.sendSmsRequest.setDeliveryReceipt(0);
                        am.sendSmsRequest.setDestinationAddr(s.getUserAccount());
                        am.sendSmsRequest.setOriginationAddr(sop.getIntegrationSettings().get("shortcode"));
                        am.sendSmsRequest.setMessage(renew);

                        String jsonResponse = mapper.writeValueAsString(am);
                        if (rabbitMQProducer.messageSender(CommonFunction.ADAPTER_SMS_SEND + am.providerName, jsonResponse)) {
                            Extra e = s.getSubscriptionRegistry().getExtra();
                            if (e == null) {
                                e = new Extra();
                            }
                            lastMTSent(s.getSop(), e);
                            subscriptionManager.subscriptionProcess(s);
                        }
                    }
                } catch (Exception ex) {
                }
            }
            instrumentedObject.unLockObject(lock);
        }
    }

}
