/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.billing.OnDemandBilling;
import java.util.Date;
import java.util.List;

/**
 *
 * @author javier
 */
public interface OnDemandBillingService {

    public OnDemandBilling findById(String id) throws Exception;

    public void saveOrUpdate(OnDemandBilling ob) throws Exception;

    public List<OnDemandBilling> getByUserAccount(String userAccount);

    public List<OnDemandBilling> getByUserAccount(Date time, String userAccount);

    public List<OnDemandBilling> getByUserAccount(String msisdn, Country country);

    public List<OnDemandBilling> getByUserAccount(Date time, String msisdn, Country country);

}
