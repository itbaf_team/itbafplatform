/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.sc.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class Data {

    public String status;
    public SCSubscription subscription;
    public SCSubscription[] subscriptions;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("status", status)
                .add("subscription", subscription)
                .add("subscriptions", subscriptions)
                .omitNullValues().toString();
    }
}
