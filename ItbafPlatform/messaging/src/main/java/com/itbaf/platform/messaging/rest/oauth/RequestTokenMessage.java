/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.messaging.rest.oauth;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class RequestTokenMessage {

    public enum GrantType {
        /**
         * Token de autorizacion para acceso a funciones del cliente
         */
        client_credentials,
        /**
         * Token de autorizacion para acceso a funciones del usuario desde el
         * cliente
         */
        authorization_code
    }

    public enum Scope {
        SLO, // Single logout. Invalida la sesion actual
        SLOC, // Single logout client. Invalida todas las sesiones del cliente
        SSO; // Invalida todas las sesiones en PG

        public static Scope getScope(String scopes) {
            Scope scope = null;
            switch (scopes.toUpperCase()) {
                case "SLO":
                    scope = SLO;
                    break;
                case "SLOC":
                    scope = SLOC;
                    break;
                case "SSO":
                    scope = SSO;
                    break;
                default:
                    scope = SSO;
            }
            return scope;
        }
    }

    public GrantType grantType;
    public Scope scope;
    public String clientId;
    public String userId;
    public String clientSecret;
    public String accessToken;
    public String codeVerifier;
    public String codeChallenge;
    public String code;
    public String redirectUri;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("grantType", grantType)
                .add("scope", scope)
                .add("clientId", clientId)
                .add("userId", userId)
                .add("clientSecret", clientSecret)
                .add("accessToken", accessToken)
                .add("codeVerifier", codeVerifier)
                .add("codeChallenge", codeChallenge)
                .add("code", code)
                .omitNullValues().toString();
    }
}
