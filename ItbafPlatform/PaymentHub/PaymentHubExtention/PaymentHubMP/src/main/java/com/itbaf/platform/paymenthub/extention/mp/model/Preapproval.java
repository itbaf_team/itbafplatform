/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;
import java.util.Date;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Preapproval {

    public enum Status {
        pending, authorized, paused, cancelled
    }

    private String id;
    private Long payer_id;
    private String payer_email;
    private String back_url;
    private Long collector_id;
    private Long application_id;
    private Status status;
    private String reason;
    private String external_reference;
    private Date date_created;
    private Date last_modified;
    private AutoRecurring auto_recurring;
    private String init_point;
    private String sandbox_init_point;
    private String preapproval_plan_id;
    private String payment_method_id;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("payer_id", payer_id)
                .add("payer_email", payer_email)
                .add("back_url", back_url)
                .add("collector_id", collector_id)
                .add("application_id", application_id)
                .add("status", status)
                .add("reason", reason)
                .add("external_reference", external_reference)
                .add("date_created", date_created)
                .add("last_modified", last_modified)
                .add("auto_recurring", auto_recurring)
                .add("init_point", init_point)
                .add("sandbox_init_point", sandbox_init_point)
                .add("preapproval_plan_id", preapproval_plan_id)
                .add("payment_method_id", payment_method_id)
                .omitNullValues().toString();
    }

}
