/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.messaging.rest.pg;

import com.google.common.base.MoreObjects;
import java.math.BigDecimal;

/**
 *
 * @author JF
 */
public class Credit {

    public String symbol;
    public String currency;
    public String countryCode;
    public BigDecimal amount;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("symbol", symbol)
                .add("currency", currency)
                .add("countryCode", countryCode)
                .add("amount", amount)
                .omitNullValues().toString();
    }
}
