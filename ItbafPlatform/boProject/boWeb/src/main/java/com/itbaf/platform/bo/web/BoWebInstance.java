/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.bo.web;

import com.google.inject.Injector;
import com.itbaf.platform.bo.web.modules.GuiceConfigModule;
import com.itbaf.platform.commons.servlets.ServletServer;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.rest.ManagerRestService;

/**
 *
 * @author jordonez
 */
public class BoWebInstance extends CommonDaemon {

    private BoWebMain boWebMain;
    private ServletServer servletServer;
    private static BoWebInstance serverDaemon;

    public static void main(String args[]) throws Exception {
        BoWebInstance.getInstance().init(args);
        BoWebInstance.getInstance().start();
    }

    public synchronized static BoWebInstance getInstance() {
        if (serverDaemon == null) {
            serverDaemon = new BoWebInstance();
            ManagerRestService.paymentHubDaemon = serverDaemon;
        }
        return serverDaemon;
    }

    @Override
    public void start() throws Exception {
        servletServer = new ServletServer(new GuiceConfigModule(args, this));
        final Injector injector = servletServer.getInject();
        boWebMain = injector.getInstance(BoWebMain.class);
        boWebMain.start();
        servletServer.startServer();
    }

    @Override
    public void stop() throws Exception {
        try {
            servletServer.stopServer();
        } catch (Exception ex) {
        }
        try {
            Thread.sleep(100);
        } catch (Exception ex) {
        }
        try {
            boWebMain.stop();
        } catch (Exception ex) {
        }
    }

}
