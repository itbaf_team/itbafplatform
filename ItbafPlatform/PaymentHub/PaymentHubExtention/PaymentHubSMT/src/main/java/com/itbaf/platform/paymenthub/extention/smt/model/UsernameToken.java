package com.itbaf.platform.paymenthub.extention.smt.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import lombok.Getter;
import lombok.Setter;

import org.apache.commons.codec.binary.Base64;

@Setter
@Getter
@lombok.extern.log4j.Log4j2
public class UsernameToken {

    private String username;
    private String password;
    private String nonce;
    private String created;
    private String passwordDigest;
    private String randTransId;

    public UsernameToken(final String username, final String password) {
        this.username = username;
        this.password = password;
        this.nonce = createNonce();
        this.created = createCreated();
        this.passwordDigest = createPassDigest();
        this.randTransId = randTransId();
    }

    private String createNonce() {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    private String createCreated() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date today = new Date();

        return formatter.format(today);
    }

    private String randTransId() {
        Random rnd = new Random();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHmmssSSS");
        Date today = new Date();

        int rand = rnd.nextInt();
        if (rand < 0) {
            rand = rand * -1;
        }

        return formatter.format(today) + rand;
    }

    private String createPassDigest() {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException ex) {
            log.error("Error en createPassDigest. " + ex, ex);
        }
        String key = nonce + created + password;
        return Base64.encodeBase64String(md.digest(key.getBytes()));
    }
}
