/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.billing.OnDemandBilling;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPAOnDemandBillingRepository;
import com.itbaf.platform.paymenthub.services.repository.OnDemandBillingService;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class OnDemandBillingServiceTransactional implements OnDemandBillingService {

    private final JPAOnDemandBillingRepository onDemandBillingRepository;

    @Inject
    public OnDemandBillingServiceTransactional(final JPAOnDemandBillingRepository onDemandBillingRepository) {
        this.onDemandBillingRepository = Validate.notNull(onDemandBillingRepository, "A JPAOnDemandBillingRepository class must be provided");

    }

    @Override
    public void saveOrUpdate(OnDemandBilling ob) throws Exception {
        try {
            onDemandBillingRepository.merge(ob);
        } catch (Exception ex) {
            try {
                onDemandBillingRepository.save(ob);
            } catch (Exception exx) {
                log.error("Error al guardar OnDemandBilling: [" + ob + "]. " + exx, exx);
            }
        }
    }

    @Override
    public List<OnDemandBilling> getByUserAccount(String userAccount) {
        return onDemandBillingRepository.getByUserAccount(userAccount);
    }

    @Override
    public List<OnDemandBilling> getByUserAccount(Date time, String userAccount) {
        return onDemandBillingRepository.getByUserAccount(time, userAccount);
    }

    @Override
    public OnDemandBilling findById(String id) throws Exception {
        return onDemandBillingRepository.findById(id);
    }

    @Override
    public List<OnDemandBilling> getByUserAccount(String msisdn, Country country) {
        return onDemandBillingRepository.getByUserAccount(msisdn, country);
    }

    @Override
    public List<OnDemandBilling> getByUserAccount(Date time, String msisdn, Country country) {
        return onDemandBillingRepository.getByUserAccount(time, msisdn, country);
    }
}
