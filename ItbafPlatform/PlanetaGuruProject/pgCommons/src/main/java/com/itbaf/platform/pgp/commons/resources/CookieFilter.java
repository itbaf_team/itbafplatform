/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.commons.resources;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */

public class CookieFilter {

    /**
     * AccessToken
     */
    public String at;
    /**
     * UserToken
     */
    public String ut;
    /**
     * UserId
     */
    public Long ui;
    /**
     * ClientId
     */
    public Long ci;
    /**
     * redirect_uri
     */
    public String ru;
    /**
     * product_id
     */
    public String pi;
    /**
     * Password Token
     */
    public String pt;
    
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("at", at)
                .add("ut", ut)
                .add("ui", ui)
                .add("ci", ci)
                .add("ru", ru)
                .add("pi", pi)
                .add("pt", pt)
                .omitNullValues().toString();
    }
}
