
package com.itbaf.platform.paymenthub.extention.cdag.ws.subscription;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="status" type="{urn:SendatelSoaService}statusResponse"/>
 *         &lt;element name="serverTid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "status",
    "serverTid"
})
@XmlRootElement(name = "resultadoAccion", namespace = "urn:SendatelSuscripcionesService")
public class ResultadoAccion {

    @XmlElement(required = true)
    protected StatusResponse status;
    @XmlElement(required = true, nillable = true)
    protected String serverTid;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link StatusResponse }
     *     
     */
    public StatusResponse getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusResponse }
     *     
     */
    public void setStatus(StatusResponse value) {
        this.status = value;
    }

    /**
     * Gets the value of the serverTid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerTid() {
        return serverTid;
    }

    /**
     * Sets the value of the serverTid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerTid(String value) {
        this.serverTid = value;
    }

    @Override
    public String toString() {
    	return String.format("ResultadoAccion(status: %s, serverTId: %s)", this.status, this.serverTid);
    }
}
