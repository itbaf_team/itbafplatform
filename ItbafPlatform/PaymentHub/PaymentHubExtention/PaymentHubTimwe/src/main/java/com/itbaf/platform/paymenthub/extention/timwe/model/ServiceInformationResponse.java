/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.model;

import com.google.common.base.MoreObjects;
import java.io.Serializable;

/**
 *
 * @author javier
 */
@lombok.Setter
@lombok.Getter
public class ServiceInformationResponse implements Serializable {

    private static final long serialVersionUID = 20181207155455L;

    private Meta meta;
    private Data data;

    public class Meta implements Serializable {

        private static final long serialVersionUID = 20181207171701L;

        public String code;
        public String status;
        public String description;

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("code", code)
                    .add("status", status)
                    .add("description", description)
                    .omitNullValues().toString();
        }
    }

    public class Data implements Serializable {

        private static final long serialVersionUID = 20181207171702L;

        public String object;
        public String id_service;
        public HubDetail hub_details;
        public HubDetail smt_details;

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("object", object)
                    .add("id_service", id_service)
                    .add("hub_details", hub_details)
                    .add("smt_details", smt_details)
                    .omitNullValues().toString();
        }
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("meta", meta)
                .add("data", data)
                .omitNullValues().toString();
    }
}
