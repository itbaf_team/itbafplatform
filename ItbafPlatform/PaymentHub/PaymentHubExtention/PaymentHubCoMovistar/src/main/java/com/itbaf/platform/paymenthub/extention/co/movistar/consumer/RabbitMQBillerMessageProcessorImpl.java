/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.co.movistar.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.commons.messaging.BillerMessage;
import com.itbaf.platform.paymenthub.extention.co.movistar.model.DirectDebitEvent;
import com.itbaf.platform.paymenthub.extention.co.movistar.resources.XBIResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.model.billing.BillerAttempt;
import com.itbaf.platform.paymenthub.model.billing.BillerAttemptTransaction;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.redisson.api.RLock;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQBillerMessageProcessorImpl implements RabbitMQBillerMessageProcessor {

    @Inject
    private ObjectMapper mapper;
    @Inject
    private XBIResources xbiResources;
    @Inject
    private RabbitMQProducer rabbitMQProducer;
    @Inject
    private InstrumentedObject instrumentedObject;
    @Inject
    private SubscriptionRegistryService subscriptionRegistryService;

    @Override
    public Boolean processMessage(String message) {
        Boolean rest = null;

        BillerMessage bm = null;
        try {
            bm = mapper.readValue(message, BillerMessage.class);
        } catch (Exception ex) {
            log.error("No fue posible procesar el mensaje [" + message + "]. " + ex, ex);
        }

        if (bm == null) {
            return rest;
        }

        Thread.currentThread().setName(bm.getThread() + "." + bm.getBillerAttempt().getId());
        BillerAttempt ba = bm.getBillerAttempt();
        RLock lock = null;
        try {
            lock = instrumentedObject.lockObject(ba.getId() + "_attempt_" + ba.getSop().getId(), 10);
            if (instrumentedObject.getAndIncrementAtomicLong(ba.getId() + "_attempts_" + ba.getSop().getId(), 60) == 1l) {
                SubscriptionRegistry sr = subscriptionRegistryService.findById(ba.getId());
                if (sr.getId().equals(sr.getSubscription().getSubscriptionRegistry().getId()) && Subscription.Status.ACTIVE.equals(sr.getSubscription().getStatus())) {

                    if (ba.getExtra() == null || ba.getExtra() == null || ba.getExtra().stringData == null) {
                        ba.setExtra(new Extra());
                    }
                    String tariffExternalId = ba.getExtra().stringData.get("tariffExternalId");

                    Tariff t = null;
                    Calendar c = Calendar.getInstance();
                    if (tariffExternalId == null) {
                        t = ba.getSop().getMainTariff();
                        switch (ba.getSop().getIntegrationSettings().get("frequencyType")) {
                            case "days":
                                break;
                            case "week":
                                ba.getExtra().stringData.put("currentWeek", c.get(Calendar.WEEK_OF_YEAR) + "");
                                c.setTime(sr.getSubscriptionDate());
                                //Dia del alta (1 = Sunday, 2 = Monday, 3 = Tuesday, 4 = Wednesday, 5 = Thursday, 6 = Friday, 7 = Saturday)
                                ba.getExtra().stringData.put("subscriptionDay", c.get(Calendar.DAY_OF_WEEK) + "");
                                break;
                            case "month":
                                break;
                        }
                        ba.setTargetAmount(t.getFullAmount());
                        ba.setTotalAmount(BigDecimal.ZERO);
                    } else {
                        switch (ba.getSop().getIntegrationSettings().get("frequencyType")) {
                            case "days":
                                break;
                            case "week":
                                Integer currentWeek = Integer.parseInt(ba.getExtra().stringData.get("currentWeek"));
                                String subscriptionDay = ba.getExtra().stringData.get("subscriptionDay");
                                if ((currentWeek < c.get(Calendar.WEEK_OF_YEAR)) && subscriptionDay.equals(c.get(Calendar.DAY_OF_WEEK) + "")) {
                                    // Comienza un nuevo periodo de cobro
                                    ba.getExtra().stringData.put("currentWeek", c.get(Calendar.WEEK_OF_YEAR) + "");
                                    t = ba.getSop().getMainTariff();
                                    ba.setTargetAmount(t.getFullAmount());
                                    ba.setTotalAmount(BigDecimal.ZERO);
                                }
                                break;
                            case "month":
                                break;
                        }
                        if (t == null) {
                            for (Tariff tf : ba.getSop().getTariffs()) {
                                if (tariffExternalId.equals(tf.getExternalId())) {
                                    t = tf;
                                    break;
                                }
                            }
                        }
                    }

                    if (t == null) {
                        log.fatal("No hay una tarifa configurada para: [" + ba + "]");
                        return null;
                    }

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    Long tid = Long.parseLong(sdf.format(new Date())) * 10000;
                    bm.setTransactionId((tid + instrumentedObject.getAndIncrementAtomicLong("TID." + tid, 1)) + "");

                    DirectDebitEvent dde = xbiResources.directDebitEventReqNew(bm.getUserAccount(), ba.getSop(),
                            ba.getSop().getIntegrationSettings().get("id_servicio"), t.getExternalId(), bm.getTransactionId());

                    if (dde == null) {
                        log.error("No fue posible realizar el cobro para: [" + bm + "]");
                    }

                    BillerAttemptTransaction bat = new BillerAttemptTransaction();
                    try {
                        bat.setEvent(dde);
                        bat.setSopId(ba.getSop().getId());
                        bat.setSubscriptionRegistryId(sr.getId());
                        bat.setTransactionId(bm.getTransactionId());
                        String json = mapper.writeValueAsString(bat);
                        rabbitMQProducer.messageSender(CommonFunction.BILLER_TRANSACTION, json);
                    } catch (Exception ex) {
                        log.fatal("No fue posible enviar al Rabbit la transaccion de cobro: [" + bat + "]");
                    }

                    switch (dde.type) {
                        case directDebitEventRes:
                            rest = directDebitEventResProcess(bm, t, dde);
                            break;
                        case directDebitEventErr:
                            rest = directDebitEventErrProcess(bm, t, dde);
                            break;
                        default:
                            log.error("directDebitEventType desconocido: [" + dde.type + "]");
                            rest = false;
                    }

                    try {
                        instrumentedObject.decrementAndGetAtomicLong("BILLER_NEXT_ATTEPMTS_" + ba.getSop().getProvider().getName());
                    } catch (Exception ex) {
                    }
                } else {
                    rest = false;
                }
            } else {
                rest = false;
            }
        } catch (Exception ex) {
            log.error("Error al enviar BillerMessage. [" + bm + "]. " + ex, ex);
        }
        instrumentedObject.unLockObject(lock);

        return rest;
    }

    private Boolean directDebitEventResProcess(BillerMessage bm, Tariff t, DirectDebitEvent dde) {
        Boolean control = null;
        BillerAttempt ba = bm.getBillerAttempt();
        switch (ba.getSop().getIntegrationSettings().get("frequencyType")) {
            case "days":
                break;
            case "week":
                control = directDebitEventResWeekProcess(bm, t, dde);
                break;
            case "month":
                break;
        }

        return control;
    }

    private Boolean directDebitEventErrProcess(BillerMessage bm, Tariff t, DirectDebitEvent dde) {
        Boolean control = null;
        BillerAttempt ba = bm.getBillerAttempt();
        switch (ba.getSop().getIntegrationSettings().get("frequencyType")) {
            case "days":
                break;
            case "week":
                control = directDebitEventErrWeekProcess(bm, t, dde);
                break;
            case "month":
                break;
        }

        return control;
    }

    private Calendar getNextAttemptWeek(BillerAttempt ba, DirectDebitEvent dde) {
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(dde.createdDate.getTime());
        int today = now.get(Calendar.DAY_OF_WEEK);
        int subscriptionDay = Integer.parseInt(ba.getExtra().stringData.get("subscriptionDay"));
        int days = 7;
        if (today != subscriptionDay) {
            days = today - subscriptionDay;
            days = (7 - days) % 7;
        }
        now.add(Calendar.DAY_OF_YEAR, days);//Posiblemente haya q sumarle 1. Probar y vemosssssss
        return now;
    }

    private Boolean directDebitEventResWeekProcess(BillerMessage bm, Tariff t, DirectDebitEvent dde) {
        BillerAttempt ba = bm.getBillerAttempt();
        bm.setBillerType(BillerMessage.BillerType.BILLING);
        if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
            bm.setMessageType(MessageType.BILLING_TOTAL);
            bm.setFullAmount(t.getFullAmount());
            bm.setNetAmount(t.getNetAmount());
            ba.getExtra().stringData.put("tariffExternalId", t.getExternalId());
            ba.setTargetAmount(t.getFullAmount());
            ba.setTotalAmount(BigDecimal.ZERO);

            ba.setLastAttempt(dde.createdDate);
            ba.setLastCharged(dde.createdDate);

            Calendar c = getNextAttemptWeek(ba, dde);
            ba.setNextAttempt(c.getTime());
            ba.getExtra().stringData.put("currentWeek", c.get(Calendar.WEEK_OF_YEAR) + "");
        } else {
            bm.setMessageType(MessageType.BILLING_PARTIAL);
            bm.setFullAmount(t.getFullAmount());
            bm.setNetAmount(t.getNetAmount());

            ba.setTargetAmount(ba.getTargetAmount().subtract(t.getFullAmount()));
            ba.setTotalAmount(ba.getTotalAmount().add(t.getFullAmount()));
            ba.setLastAttempt(dde.createdDate);
            ba.setLastCharged(dde.createdDate);

            Tariff aux = null;
            for (Tariff ta : ba.getSop().getTsTariff()) {
                if (ta.getFullAmount().compareTo(ba.getTargetAmount()) < 1) {
                    aux = ta;
                    break;
                }
            }

            if (aux == null) {
                //Si no hay una tarifa para el valor restante
                //Reiniciamos el cobro a la tarifa madre y
                //Esperamos hasta el siguiente periodo de cobro
                //log.info("No existe un Tariff para el amount: [" + ba.getTargetAmount() + "] - [" + ba.getSop().getTariffs() + "]");
                aux = ba.getSop().getMainTariff();
                ba.setTargetAmount(aux.getFullAmount());
                ba.setTotalAmount(BigDecimal.ZERO);
                Calendar c = getNextAttemptWeek(ba, dde);
                ba.setNextAttempt(c.getTime());
                ba.getExtra().stringData.put("currentWeek", c.get(Calendar.WEEK_OF_YEAR) + "");
            } else {
                Calendar now = Calendar.getInstance();
                now.setTimeInMillis(dde.createdDate.getTime());
                now.add(Calendar.HOUR_OF_DAY, 6);
                ba.setNextAttempt(now.getTime());
            }
            ba.getExtra().stringData.put("tariffExternalId", aux.getExternalId());
        }

        try {
            String json = mapper.writeValueAsString(bm);
            rabbitMQProducer.messageSender(CommonFunction.BILLER_PROCESSED + ba.getSop().getProvider().getName(), json);
        } catch (Exception ex) {
            log.fatal("No fue posible enviar al Rabbit el BillerMessage: [" + bm + "]. " + ex, ex);
        }
        //Debe retornar TRUE para que no se realice otro cobro!!
        return true;
    }

    private Boolean directDebitEventErrWeekProcess(BillerMessage bm, Tariff t, DirectDebitEvent dde) {

        BillerAttempt ba = bm.getBillerAttempt();
        Calendar c = Calendar.getInstance();
        bm.setMessageType(MessageType.BILLING_PENDING);
        ba.setLastAttempt(dde.createdDate);
        ba.getExtra().stringData.put("tariffExternalId", t.getExternalId());
        switch (dde.errorCode) {
            case "1917"://El abonado no está aprovisionado
            case "1970"://Error al hacer consulta de autorización PVS
            case "3108"://Suscripción tiene una autorización pendiente
            case "3110"://Error desconocido
            case "1951"://Ocurrió un error en la plataforma de cobro. No fue posible efectuar el cobro
                c.add(Calendar.HOUR_OF_DAY, 6);
                ba.setNextAttempt(c.getTime());
                break;
            case "1955"://El tipo de usuario no es válido para este Subservicio
                log.warn("El tipo de usuario no es válido para este Subservicio: [" + bm + "]");
                // aca hay q pasar a la siguiente tarifa y si no hay mas, 
                // reset a la main e intentar en el siguiente periodo
                Tariff aux = null;
                boolean xx = false;
                for (Tariff ta : ba.getSop().getTsTariff()) {
                    if (xx) {
                        aux = t;
                        break;
                    }
                    if (ta.getId().equals(t.getId())) {
                        xx = true;
                    }
                }

                if (aux == null) {
                    for (Tariff ta : ba.getSop().getTsTariff()) {
                        if (ta.getFullAmount().compareTo(ba.getTargetAmount()) < 1) {
                            aux = ta;
                            break;
                        }
                    }
                }
                if (aux == null) {
                    aux = ba.getSop().getMainTariff();
                    ba.getExtra().stringData.put("tariffExternalId", aux.getExternalId());
                    ba.setTargetAmount(aux.getFullAmount());
                    ba.setTotalAmount(BigDecimal.ZERO);
                    c = getNextAttemptWeek(ba, dde);
                    ba.setNextAttempt(c.getTime());
                    ba.getExtra().stringData.put("currentWeek", c.get(Calendar.WEEK_OF_YEAR) + "");
                } else {
                    ba.getExtra().stringData.put("tariffExternalId", aux.getExternalId());
                    c.add(Calendar.HOUR_OF_DAY, 6);
                    ba.setNextAttempt(c.getTime());
                }
                break;
            case "1960"://Alcanzado el máximo de intentos de cobros diarios por usuario para este Subservicio
                c.add(Calendar.HOUR_OF_DAY, 12);
                ba.setNextAttempt(c.getTime());
                break;
            case "3101"://Suscripción en estado inactivo
            case "3105"://suscripción no activa para msisdn
                // dar de baja con cancelled
                bm.setMessageType(MessageType.SUBSCRIPTION_CANCELLATION);
                break;
            case "3104"://msisdn : no encontrado en lista blanca para suscripción
            case "3109"://Suscripción se encuentra bloqueada
                //Mandar a Blacklist
                bm.setMessageType(MessageType.SUBSCRIPTION_BLACKLIST);
                break;
            case "3106"://suscripción en periodo promocional, msisdn
            case "3107"://suscripción en periodo facturado    
                //Reintentar luego en un nuevo periodo de cobro
                aux = ba.getSop().getMainTariff();
                ba.getExtra().stringData.put("tariffExternalId", aux.getExternalId());
                ba.setTargetAmount(aux.getFullAmount());
                ba.setTotalAmount(BigDecimal.ZERO);
                c = getNextAttemptWeek(ba, dde);
                ba.setNextAttempt(c.getTime());
                ba.getExtra().stringData.put("currentWeek", c.get(Calendar.WEEK_OF_YEAR) + "");
                break;
            case "1952"://El saldo del abonado es insuficiente para el Subservicio requerido
                //pasar a la siguiente tarifa y mover en horas
                aux = null;
                for (Tariff ta : ba.getSop().getTsTariff()) {
                    if (ta.getFullAmount().compareTo(ba.getTargetAmount()) < 1) {
                        aux = ta;
                        break;
                    }
                }
                if (aux == null) {
                    aux = ba.getSop().getMainTariff();
                    ba.getExtra().stringData.put("tariffExternalId", aux.getExternalId());
                    ba.setTargetAmount(aux.getFullAmount());
                    ba.setTotalAmount(BigDecimal.ZERO);
                    c = getNextAttemptWeek(ba, dde);
                    ba.setNextAttempt(c.getTime());
                    ba.getExtra().stringData.put("currentWeek", c.get(Calendar.WEEK_OF_YEAR) + "");
                } else {
                    ba.getExtra().stringData.put("tariffExternalId", aux.getExternalId());
                    c.add(Calendar.HOUR_OF_DAY, 6);
                    ba.setNextAttempt(c.getTime());
                }

                break;
            case "1980"://Plan del Abonado no es compatible con la categoría del servicio
                //Preguntar, pero lo mas seguro es q haya q cancelar la suscripcion
                log.fatal("No se que hacer con el error de este cobro: [" + bm + "]. Codigo de error: [" + dde + "]");
                c.add(Calendar.DATE, 1);
                ba.setNextAttempt(c.getTime());
                break;
            default:
                log.fatal("No se ha podido realizar el cobro para: [" + bm + "]. Codigo de error: [" + dde + "]");
                c.add(Calendar.DATE, 1);
                ba.setNextAttempt(c.getTime());
        }

        try {
            String json = mapper.writeValueAsString(bm);
            rabbitMQProducer.messageSender(CommonFunction.BILLER_PROCESSED_ERROR + ba.getSop().getProvider().getName(), json);
        } catch (Exception ex) {
            log.fatal("No fue posible enviar al Rabbit el BillerMessage: [" + bm + "]. " + ex, ex);
        }

        //Debe retornar TRUE para que no se realice otro intento de cobro
        return true;
    }

}
