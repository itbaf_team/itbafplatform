/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.smpp.consumer;

import com.itbaf.platform.paymenthub.smpp.message.SMPPMessageProcessor;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessorFactory;

/**
 *
 * @author javier

 */
public interface RabbitMQSMPPMessageProcessorFactory extends RabbitMQMessageProcessorFactory <SMPPMessageProcessor>{
 
}
