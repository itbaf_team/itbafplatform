package com.itbaf.platform.commons.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.Validate;

/**
 * A {@link Properties} loader.
 *
 * @author Julian Gutierrez Oschmann
 *
 */
public class PropertiesLoader {

    private final String configPath;

    /**
     * Creates a new {@link PropertiesLoader}.
     *
     * @param configPath The properties file path.
     */
    public PropertiesLoader(final String configPath) {

        Validate.notNull(configPath, "A configuration path must be provided");

        this.configPath = configPath;
    }

    /**
     * Loads the properties file from the classpath.
     *
     * @param clazz A {@link Class} with same parent classloader.
     * @return The properties instance.
     */
    public Properties loadPropertiesFromClassPath(final Class<?> clazz) {
        final Properties props = new Properties();
        return fillProperties(props, clazz.getResourceAsStream(configPath));
    }

    /**
     * Loads the properties instance from a file.
     *
     * @return The properties instance.
     */
    public Properties loadProperties() {
        final Properties props = new Properties();
        try {
            return fillProperties(props, getClass().getResourceAsStream("/config/"+configPath));
        } catch (Exception e) {
            throw new ConfigurationException(String.format(
                    "Configuration file not found: %s", configPath), e);
        }
    }
    public Properties loadSProperties() {
        final Properties props = new Properties();
        try {
            return fillProperties(props, getClass().getResourceAsStream("/sconfig/"+configPath));
        } catch (Exception e) {
            throw new ConfigurationException(String.format(
                    "Configuration file not found: %s", configPath), e);
        }
    }

    private Properties fillProperties(final Properties props,
            final InputStream inputStream) {
        try {
            props.load(inputStream);
            inputStream.close();
            return props;
        } catch (final IOException e) {
            throw new ConfigurationException(String.format(
                    "Error loading configuration file not found: %s", configPath), e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (final IOException e) {
                    throw new ConfigurationException(String.format(
                            "Error loading configuration file not found: %s", configPath), e);
                }
            }
        }
    }
}
