/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Client;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Username;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author javier
 */

@lombok.Setter
@lombok.Getter
public class CredentialUserProperty extends UserProperty implements Serializable {

    private static final long serialVersionUID = 20181207172401L;

    private Username username = new Username();
    private Client client = new Client();
    private String password;

    public CredentialUserProperty() {
        super(Topic.CREDENTIAL);
    }

    public void setPassword(String password) throws Exception {
        if (password == null || password.length() < 7) {
            throw new Exception("El password debe tener al menos 8 caracteres...");
        } else if (password.startsWith("JF.")) {
            this.password = password;
            return;
        }

        this.password = "JF." + Jwts.builder()
                .setIssuer("oauth")
                .claim("thread", Thread.currentThread().getName())
                .claim("scope", "CredentialUserProperty")
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, (password + getClass().getCanonicalName()).getBytes("UTF-8"))
                .compact();
    }

    public boolean validatePassword(String password) {
        try {
            Jwts.parser().setSigningKey((password + getClass().getCanonicalName()).getBytes("UTF-8"))
                    .parse(this.password.substring(3));
            return true;
        } catch (Exception ex) {
        }
        return false;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("username", username)
                .add("password", password)
                .add("client", client)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(password, username, client);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CredentialUserProperty other = (CredentialUserProperty) obj;
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.client, other.client)) {
            return false;
        }
        return true;
    }

}
