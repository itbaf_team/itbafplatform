package com.itbaf.platform.paymenthub.model.subscription;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSubscriptionRegistry is a Querydsl query type for SubscriptionRegistry
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSubscriptionRegistry extends EntityPathBase<SubscriptionRegistry> {

    private static final long serialVersionUID = 468989379L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSubscriptionRegistry subscriptionRegistry = new QSubscriptionRegistry("subscriptionRegistry");

    public final com.itbaf.platform.paymenthub.model.QBaseEntity _super = new com.itbaf.platform.paymenthub.model.QBaseEntity(this);

    public final SimplePath<Object> adnetworkTracking = createSimple("adnetworkTracking", Object.class);

    public final StringPath channelIn = createString("channelIn");

    public final StringPath channelOut = createString("channelOut");

    public final StringPath externalId = createString("externalId");

    public final SimplePath<Extra> extra = createSimple("extra", Extra.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath originSubscription = createString("originSubscription");

    public final StringPath originUnsubscription = createString("originUnsubscription");

    protected com.itbaf.platform.paymenthub.model.QSOP sop;

    protected QSubscription subscription;

    public final DateTimePath<java.util.Date> subscriptionDate = createDateTime("subscriptionDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> syncDate = createDateTime("syncDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> unsubscriptionDate = createDateTime("unsubscriptionDate", java.util.Date.class);

    public final StringPath userAccount = createString("userAccount");

    public QSubscriptionRegistry(String variable) {
        this(SubscriptionRegistry.class, forVariable(variable), INITS);
    }

    public QSubscriptionRegistry(Path<? extends SubscriptionRegistry> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSubscriptionRegistry(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSubscriptionRegistry(PathMetadata metadata, PathInits inits) {
        this(SubscriptionRegistry.class, metadata, inits);
    }

    public QSubscriptionRegistry(Class<? extends SubscriptionRegistry> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sop = inits.isInitialized("sop") ? new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"), inits.get("sop")) : null;
        this.subscription = inits.isInitialized("subscription") ? new QSubscription(forProperty("subscription"), inits.get("subscription")) : null;
    }

    public com.itbaf.platform.paymenthub.model.QSOP sop() {
        if (sop == null) {
            sop = new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"));
        }
        return sop;
    }

    public QSubscription subscription() {
        if (subscription == null) {
            subscription = new QSubscription(forProperty("subscription"));
        }
        return subscription;
    }

}

