/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.messaging.rest;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
public class ResponseMessage {

    public enum Status {
        OK, // Operacion realizada satisfactoriamente
        OK_NS, // Operacion realizada satisfactoriamente. No suscribir
        PIN_ERROR, // El PIN es invalido o ha expirado
        PIN_NOT_MATCH,// pin no coincide
        ACTIVE,// Usuario esta suscrito
        PENDING,// Usuario con suscripcion pendiente 
        REMOVED,// Usuario no esta suscrito
        PORTING,// Usuario no pertenece a este proveedor
        BLACKLIST,// Usuario en blacklist  
        LOCKED, // No se puede suscribir por un tiempo. Limite maximo de suscripciones
        TRIAL, //Suscripcion en periodo de prueba
        ERROR, // Operacion finalizada con error 
        ERROR_403, // Operacion finalizada con error por parametros invalidos 
        ERROR_BILLING, // No se pudo realizar el cobro ,
        RETENTION, // usuario intenta suscribirse al servicio pero no cuenta con el saldo suficiente
        INTERNAL_ERROR, // error interno del sistema
        INSUFFICIENT_BALANCE //saldo insuficiente
    }

    public String thread = Thread.currentThread().getName();
    public Status status = Status.ERROR;
    public String code;
    public String message;
    public Object data;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("thread", thread)
                .add("code", code)
                .add("status", status)
                .add("message", message)
                .add("data", data)
                .omitNullValues().toString();
    }
}
