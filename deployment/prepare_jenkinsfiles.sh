#!/bin/bash

for MODULE in $(ls ./config | grep -v -E 'commons|tasks'); do
    source ./config/${MODULE}/deploy.conf
    cp -f Jenkinsfile.template ./config/${MODULE}/Jenkinsfile
    sed -i "s^%RELEASE_NAME%^${RELEASE_NAME}^g" ./config/${MODULE}/Jenkinsfile
    sed -i "s^%POM_PATH%^${POM_PATH}^g" ./config/${MODULE}/Jenkinsfile
    sed -i "s^%KUBE_NS%^${KUBE_NS}^g" ./config/${MODULE}/Jenkinsfile
    sed -i "s^%KUBE_CLUSTER_PR%^${KUBE_CLUSTER_PR}^g" ./config/${MODULE}/Jenkinsfile
    sed -i "s^%KUBE_CLUSTER_ST%^${KUBE_CLUSTER_ST}^g" ./config/${MODULE}/Jenkinsfile
done
