/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.biller.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import java.util.List;
import org.redisson.api.RLock;
import com.itbaf.platform.paymenthub.biller.processor.BillerMessageProcessor;
import com.itbaf.platform.paymenthub.commons.messaging.BillerMessage;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQBillerMessageProcessorImpl implements RabbitMQMessageProcessor {

    private final ObjectMapper mapper;
    private final SopService sopService;
    private final InstrumentedObject instrumentedObject;
    private final BillerMessageProcessor billerMessageProcessor;

    @AssistedInject
    public RabbitMQBillerMessageProcessorImpl(final ObjectMapper mapper,
            final SopService sopService,
            final InstrumentedObject instrumentedObject,
            @Assisted final BillerMessageProcessor billerMessageProcessor) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.sopService = Validate.notNull(sopService, "A SopService class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.billerMessageProcessor = Validate.notNull(billerMessageProcessor, "A BillerMessageProcessor class must be provided");
    }

    @Override
    public Boolean processMessage(String message) {

        BillerMessage bm = null;

        try {
            bm = mapper.readValue(message, BillerMessage.class);
        } catch (Exception ex) {
            log.error("No fue posible interpretar el mensaje: [" + message + "]. " + ex, ex);
            return null;
        }

        SOP sop = null;
        try {
            sop = sopService.findById(bm.getSopId());
        } catch (Exception ex) {
        }

        List<RLock> lock = null;
        try {
            lock = instrumentedObject.lockSubscription(bm.getExternalId(), bm.getUserAccount(),
                    bm.getExternalUserAccount(), bm.getExternalCustomer(), sop.getId());
        } catch (Exception ex) {
            log.error("Error al sincronizar billerMessage. " + ex, ex);
        }

        Boolean processed = null;

        try {
            processed = billerMessageProcessor.process(message, bm, sop);
        } catch (Exception ex) {
            log.error("Error procesando billerMessage: [" + bm + "]. " + ex, ex);
        }

        instrumentedObject.unLockSubscription(lock);
        return processed;
    }

}
