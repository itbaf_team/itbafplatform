/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.Notification;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
public class JPANotificationRepository extends AbstractJPARepositorySession<Notification, Long> {

    @Inject
    public JPANotificationRepository(final Provider<EntityManager> entityManagerProvider) {
        super(Notification.class, entityManagerProvider);
    }

}
