package com.itbaf.platform.paymenthub.extention.store.ws.subscription;


public class PersonalArProviderServiceException extends RuntimeException {

	private static final long serialVersionUID = 4701312863467920182L;

	private StatusResponse response;

	public PersonalArProviderServiceException(StatusResponse response) {
		this.response = response;
	}

	@Override
	public String getMessage() {
		return this.response.toString();
	}

	public StatusResponse getResponse() {
		return this.response;
	}
}
