/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.handler;

import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.bn.model.TimweNotification;
import com.itbaf.platform.paymenthub.extention.timwe.resources.TimweSMSRequest;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import javax.xml.ws.Holder;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class EntelPeServiceProcessorHandler extends ServiceProcessorHandler {

    @Inject
    public EntelPeServiceProcessorHandler() {
        super("pe.entel.timwe");
    }

    @Override
    protected Boolean processSubscriptionNotification(TimweNotification notification, SOP sop, Subscription s, boolean sendSMS) {
        Boolean rst = super.processSubscriptionNotification(notification, sop, s);

        if (rst != null && rst && sendSMS) {
            rst = sendSMS(notification, sop, "sms.message.renew");
        }

        return rst;
    }

    @Override
    protected Boolean processBillingNotification(TimweNotification notification, SOP sop, Subscription s) {
        Boolean rst = super.processBillingNotification(notification, sop, s);
        if (rst != null && rst) {
            return sendSMS(notification, sop, "sms.message.renew");
        }
        return rst;
    }

    @Override
    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResponseMessage sendSMS(Subscription subscription) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
