import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubscriptionsComponent } from './subscriptions.component';
import { SearchComponent } from './search/search.component';


const routes: Routes = [{
  path: '',
  component: SubscriptionsComponent,
  children: [{
    path: 'search',
    component: SearchComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubscriptionsRoutingModule { }
