/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.npay;

import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NPAY_QUEUE_NAME;
import com.itbaf.platform.paymenthub.commons.PaymentHubBasic;
import static com.itbaf.platform.commons.CommonFunction.BORDER_NOTIFICATION;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain extends PaymentHubBasic {

    @Inject
    public PaymentHubMain() {
    }

    @Override
    protected void startActions() {
        isBNProcessor = false;
        super.startActions();

        String name = BORDER_NOTIFICATION + NPAY_QUEUE_NAME;
        try {
            rabbitMQConsumer.createChannel(name, consumerQuantity, bnProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer BORDER_NOTIFICATION para provider: [" + name + "]. " + ex, ex);
        }
    }
}
