/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.connection;

import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import java.sql.Connection;
import java.sql.SQLException;
import org.hibernate.c3p0.internal.C3P0ConnectionProvider;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class BasicDBConnectionProvider extends C3P0ConnectionProvider {

    public static BasicDataBaseConnection dataBaseConnection;

    @Override
    public Connection getConnection() throws SQLException {
        if (dataBaseConnection != null) {
            return dataBaseConnection.getDBPaymentHubConnection();
        }
        return super.getConnection();
    }

    @Override
    public void closeConnection(Connection conn) throws SQLException {
        if (dataBaseConnection != null) {
            dataBaseConnection.closePaymentHubConnection(conn);
            return;
        }
        super.closeConnection(conn);
    }

}
