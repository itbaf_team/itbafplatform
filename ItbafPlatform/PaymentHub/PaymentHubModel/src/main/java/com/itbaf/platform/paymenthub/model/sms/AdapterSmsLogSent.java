/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.sms;

import com.itbaf.platform.paymenthub.model.*;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.messaging.smpp.adapter.AdapterSMSMessage;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.querydsl.core.annotations.Config;
import org.hibernate.annotations.Type;

/**
 *
 * @author javier
 */
@Entity(name = "ADAPTER_SMS_LOG_SENT")
@Table(name = "ADAPTER_SMS_LOG_SENT", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(false)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors = true)
public class AdapterSmsLogSent extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 2018112314482L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "TID", nullable = false, unique = true)
    private String tid;
    @Type(type = "json")
    @Column(name = "MESSAGE", columnDefinition = "json")
    private AdapterSMSMessage message;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("tid", tid)
                .add("message", message)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.id);
        hash = 31 * hash + Objects.hashCode(this.tid);
        hash = 31 * hash + Objects.hashCode(this.message);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AdapterSmsLogSent other = (AdapterSmsLogSent) obj;
        if (!Objects.equals(this.tid, other.tid)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.message, other.message)) {
            return false;
        }
        return true;
    }

}
