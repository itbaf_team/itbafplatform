/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Operator;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.ServiceMatcher;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPACountryRepository;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPAOperatorRepository;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPAProviderRepository;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPAServiceMatcherRepository;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPASopRepository;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import io.jsonwebtoken.lang.Collections;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class SopServiceTransactional implements SopService {

    private final JPASopRepository sopRepository;
    private final JPACountryRepository countryRepository;
    private final SubscriptionManager subscriptionManager;
    private final JPAOperatorRepository operatorRepository;
    private final JPAProviderRepository providerRepository;
    private final JPAServiceMatcherRepository serviceMatcherRepository;

    @Inject
    public SopServiceTransactional(final JPASopRepository sopRepository,
            final JPACountryRepository countryRepository,
            final SubscriptionManager subscriptionManager,
            final JPAOperatorRepository operatorRepository,
            final JPAProviderRepository providerRepository,
            final JPAServiceMatcherRepository serviceMatcherRepository) {
        this.sopRepository = Validate.notNull(sopRepository, "A JPASopRepository class must be provided");
        this.countryRepository = Validate.notNull(countryRepository, "A JPACountryRepository class must be provided");
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
        this.operatorRepository = Validate.notNull(operatorRepository, "A JPAOperatorRepository class must be provided");
        this.providerRepository = Validate.notNull(providerRepository, "A JPAProviderRepository class must be provided");
        this.serviceMatcherRepository = Validate.notNull(serviceMatcherRepository, "A JPAServiceMatcherRepository class must be provided");
    }

    @Override
    public SOP findById(Long id) {
        try {
            return sopRepository.findById(id);
        } catch (Exception ex) {
        }

        return null;
    }



    @Override
    public SOP findByNames(String serviceMatcher, String operator, String provider) {
        SOP sop;
        Object obj = MainCache.memory2Hours().getIfPresent(serviceMatcher + "_" + operator + "_" + provider);
        if (obj == null) {
            sop = sopRepository.findByNames(serviceMatcher, operator, provider);
            if (sop != null) {
                MainCache.memory2Hours().put(serviceMatcher + "_" + operator + "_" + provider, sop);
            }
        } else {
            sop = (SOP) obj;
        }
        return sop;
    }

    @Override
    public SOP findOrCreateByNames(String serviceMatcher, String operator, String provider) throws Exception {
        SOP sop;
        Object obj = MainCache.memory2Hours().getIfPresent(serviceMatcher + "_" + operator + "_" + provider);
        if (obj == null) {
            sop = sopRepository.findByNames(serviceMatcher, operator, provider);
            if (sop == null) {
                sop = new SOP(serviceMatcher, operator, provider);
                ServiceMatcher s = serviceMatcherRepository.findByName(sop.getServiceMatcher().getName());
                if (s != null) {
                    sop.setServiceMatcher(s);
                } else {
                    serviceMatcherRepository.save(sop.getServiceMatcher());
                }
                Operator o = operatorRepository.findByName(sop.getOperator().getName());
                if (o != null) {
                    sop.setOperator(o);
                } else {
                    operatorRepository.save(sop.getOperator());
                }
                Provider p = providerRepository.findByName(sop.getProvider().getName());
                if (p != null) {
                    sop.setProvider(p);
                } else {
                    Country c = sop.getProvider().getCountry();
                    if (c == null) {
                        c = new Country();
                        //Valor por default
                        c.setCode("XX");
                        sop.getProvider().setCountry(c);
                    }

                    c = countryRepository.findByCode(sop.getProvider().getCountry().getCode());
                    if (c == null) {
                        throw new RuntimeException("No existe Country para: [" + sop.getProvider().getCountry() + "]");
                    } else {
                        sop.getProvider().setCountry(c);
                    }
                    providerRepository.save(sop.getProvider());
                }
                sop.setStatus(Status.ENABLE);
                sopRepository.save(sop);
                MainCache.memory2Hours().put(serviceMatcher + "_" + operator + "_" + provider, sop);
            }
        } else {
            sop = (SOP) obj;
        }
        return sop;
    }

    @Override
    public SOP findBySettings(String key, String value) {
        if (key != null && value != null) {
            Map<String, String> settings = new HashMap();
            settings.put(key, value);
            return findBySettings("", settings);
        }
        return null;
    }

    @Override
    public SOP findBySettings(Map<String, String> settings) {
        if (settings != null) {
            return findBySettings("", settings);
        }
        return null;
    }

    @Override
    public SOP findBySettings(String provider, String key, String value) {
        if (provider != null && key != null && value != null) {
            Map<String, String> settings = new HashMap();
            settings.put(key, value);
            return findBySettings(provider, settings);
        }
        return null;
    }

    @Override
    public SOP findBySettings(String provider, Map<String, String> settings) {
        if (provider != null && settings != null) {
            try {
                Object aux = MainCache.memory1Hour().getIfPresent(provider + "_" + Arrays.toString(settings.entrySet().toArray()));
                SOP instance;
                if (aux == null) {
                    instance = sopRepository.findBySettings(provider, settings);
                    if (instance != null) {
                        MainCache.memory1Hour().put(provider + "_" + Arrays.toString(settings.entrySet().toArray()), instance);
                    }
                } else {
                    instance = (SOP) aux;
                }
                return instance;

            } catch (Exception ex) {
                log.error("Error al obtener SOP para Provider: [" + provider + "_" + Arrays.toString(settings.entrySet().toArray()) + "]. " + ex, ex);
            }
        }
        return null;
    }

    @Override
    public List<SOP> findAllBySettings(String provider, Map<String, String> settings) {
        if (provider != null && settings != null) {
            try {
                Object aux = MainCache.memory1Hour().getIfPresent(provider + "_all_" + Arrays.toString(settings.entrySet().toArray()));
                List<SOP> instance;
                if (aux == null) {
                    instance = sopRepository.findAllBySettings(provider, settings);
                    if (instance != null) {
                        MainCache.memory1Hour().put(provider + "_all_" + Arrays.toString(settings.entrySet().toArray()), instance);
                    }
                } else {
                    instance = (List<SOP>) aux;
                }
                return instance;

            } catch (Exception ex) {
                log.error("Error al obtener SOP para Provider: [" + provider + "_" + Arrays.toString(settings.entrySet().toArray()) + "]. " + ex, ex);
            }
        }
        return null;
    }

    @Override
    public SOP findWithTariffBySettings(String provider, String key, String value) {
        if (provider != null && key != null && value != null) {
            Map<String, String> settings = new HashMap();
            settings.put(key, value);
            return findWithTariffBySettings(provider, settings);
        }
        return null;
    }

    @Override
    public SOP findWithTariffBySettings(String provider, Map<String, String> settings) {
        if (provider != null && settings != null) {
            try {
                Object aux = MainCache.memory2Hours().getIfPresent(provider + "_" + Arrays.toString(settings.entrySet().toArray()) + "_wt");
                SOP instance;
                if (aux == null) {
                    instance = sopRepository.findBySettings(provider, settings);
                    instance.getTariffs().size();
                    MainCache.memory2Hours().put(provider + "_" + Arrays.toString(settings.entrySet().toArray()), instance);
                    MainCache.memory2Hours().put(provider + "_" + Arrays.toString(settings.entrySet().toArray()) + "_wt", instance);
                } else {
                    instance = (SOP) aux;
                }
                return instance;

            } catch (Exception ex) {
                log.error("Error al obtener SOP para Provider: [" + provider + "_" + Arrays.toString(settings.entrySet().toArray()) + "]. " + ex, ex);
            }

        }
        return null;
    }

    @Override
    public List<SOP> getMatchByProviderName(String name) {
        return sopRepository.getMatchByProviderName(name);
    }

    @Override
    public List<SOP> getSOPsByService(String serviceCode, Provider provider) {
        return sopRepository.getSOPsByService(serviceCode, provider);
    }

    @Override
    public List<SOP> getSOPsByProviderName(String providerName) {
        Object aux = MainCache.memory48Hours().getIfPresent("SOPs_providerName_" + providerName);
        List<SOP> sops;
        if (aux == null) {
            sops = sopRepository.getSOPsByProviderName(providerName);
            if (!Collections.isEmpty(sops)) {
                MainCache.memory48Hours().put("SOPs_providerName_" + providerName, sops);
            }
        } else {
            sops = (List<SOP>) aux;
        }

        return sops;
    }

    @Override
    public List<SOP> getSOPsByCountry(String countryCode) {
        return sopRepository.getSOPsByCountry(countryCode);
    }

    @Override
    public void saveOrUpdate(SOP sop) throws Exception {
        if (sop.getId() == null) {
            sopRepository.save(sop);
        } else {
            sopRepository.merge(sop);
        }
        subscriptionManager.updateMemoryCache();
    }


    @Override
    public SOP getSOPByTariff(String tariffExternalid) {

        Object aux = MainCache.memory2Hours().getIfPresent("SOP_tariffExternalid_" + tariffExternalid);
        SOP instance = null;
        if (aux == null) {
            instance = sopRepository.getSOPByTariff(tariffExternalid);
            if (instance != null) {
                MainCache.memory2Hours().put("SOP_tariffExternalid_" + tariffExternalid, instance);
            }
        } else {
            instance = (SOP) aux;
        }
        return instance;
    }
}
