/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.biller.processor;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.biller.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.commons.messaging.BillerMessage;
import com.itbaf.platform.paymenthub.commons.messaging.BillerMessage.BillerType;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.rmq.consumer.MessageProcessor;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
public abstract class BillerMessageProcessor implements MessageProcessor {

    @Inject
    protected ServiceProcessorHandler serviceProcessorHandler;

    private final BillerMessage.BillerType billerType;

    public BillerMessageProcessor(final BillerMessage.BillerType billerType) {
        this.billerType = Validate.notNull(billerType, "A BillerType class must be provided");
    }

    public final BillerType getBillerType() {
        return billerType;
    }

    /**
     * @see RabbitMQMessageProcessor
     * @return NULL. Por alguna razon no se proceso message. Se reencola........
     * ........TRUE.Mensaje procesado satisfactoriamente........................
     * ........FALSE. Mensaje procesado y descartable...........................
     */
    public abstract Boolean process(String message, BillerMessage bm, SOP sop);
}
