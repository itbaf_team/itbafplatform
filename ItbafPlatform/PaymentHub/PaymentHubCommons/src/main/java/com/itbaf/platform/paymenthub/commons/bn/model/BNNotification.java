/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.bn.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "topic")
@JsonSubTypes({
    @JsonSubTypes.Type(value = MDivulgaNotification.class, name = "MDivulgaNotification"),
    @JsonSubTypes.Type(value = MPNotification.class, name = "MPNotification"),
    @JsonSubTypes.Type(value = NPAYNotification.class, name = "NPAYNotification"),
    @JsonSubTypes.Type(value = SpiralisNotification.class, name = "SpiralisNotification"),
    @JsonSubTypes.Type(value = TimweNotification.class, name = "TimweNotification"),
    @JsonSubTypes.Type(value = WAUNotification.class, name = "WAUNotification")})
public abstract class BNNotification {

    /*public enum Topic {
        NPAY_NOTIFICATION,
        SPIRALIS_NOTIFICATION,
        WAU_NOTIFICATION,
        MP_NOTIFICATION,
        MD_NOTIFICATION,
        TIMWE_NOTIFICATION
    }*/
    // @Enumerated(EnumType.STRING)
    @JsonProperty
    private final String topic = this.getClass().getSimpleName();

    /*public BNNotification(Topic topic) {
        this.topic = topic;
    }*/
    public String getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("topic", topic)
                .omitNullValues().toString();
    }
}
