
package com.itbaf.platform.paymenthub.adapter.resources.smppadapter;

import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.RcvDRRequest;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.RcvDRResponse;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.RcvSmsRequest;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.RcvSmsResponse;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.SendSmsRequest;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.SendSmsResponse;
import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.blowbit.telcohub.integration.smppadapter package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.blowbit.telcohub.integration.smppadapter
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RcvSmsResponse }
     * 
     */
    public RcvSmsResponse createRcvSmsResponse() {
        return new RcvSmsResponse();
    }

    /**
     * Create an instance of {@link RcvSmsRequest }
     * 
     */
    public RcvSmsRequest createRcvSmsRequest() {
        return new RcvSmsRequest();
    }

    /**
     * Create an instance of {@link SendSmsResponse }
     * 
     */
    public SendSmsResponse createSendSmsResponse() {
        return new SendSmsResponse();
    }

    /**
     * Create an instance of {@link RcvDRRequest }
     * 
     */
    public RcvDRRequest createRcvDRRequest() {
        return new RcvDRRequest();
    }

    /**
     * Create an instance of {@link RcvDRResponse }
     * 
     */
    public RcvDRResponse createRcvDRResponse() {
        return new RcvDRResponse();
    }

    /**
     * Create an instance of {@link SendSmsRequest }
     * 
     */
    public SendSmsRequest createSendSmsRequest() {
        return new SendSmsRequest();
    }

}
