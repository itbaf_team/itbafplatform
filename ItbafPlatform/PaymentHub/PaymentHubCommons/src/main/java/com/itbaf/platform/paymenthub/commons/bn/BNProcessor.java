/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.bn;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;

/**
 *
 * @author JF
 */
public abstract class BNProcessor implements RabbitMQMessageProcessor {

    @Inject
    protected ObjectMapper mapper;
}
