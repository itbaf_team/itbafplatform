package com.itbaf.platform.pgp.test.services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.itbaf.platform.pgp.test.instance.GuiceInstance;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Friend;
import com.itbaf.platform.pgp.services.repository.UserService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author JF
 */
public class UserServiceTest {

    private UserService userService;

    public UserServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        userService = GuiceInstance.getInstance().getInstance(UserService.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    @Ignore
    public void getUserByID() {
        User user = userService.findById(104L);
        assertNotNull(user);
        // assertTrue("AR".equalsIgnoreCase(country.getCode()));
    }

    @Test
    @Ignore
    public void getUserByNickname() {
        User user = userService.findByNickname("user_104");
        assertNotNull(user);
    }

    @Test
    @Ignore
    public void getUserFriend() {
        User user = userService.findByNickname("user_104");
        assertNotNull(user);
        Friend f = new Friend();
        f.setStatus(Status.CREATED);
        f.setUserId(111L);
        user.getProfileUserProperty().getFriends().put(f.getUserId(), f);
        boolean error = true;
        try {
            userService.saveOrUpdate(user);
            error = false;
        } catch (Exception ex) {
        }
        assertFalse(error);
    }

    @Test
   @Ignore
    public void setUserProperties() {
        User user = new User();
        user.setId(6l);
        assertNotNull(user);

        user.getCredentialUserProperty().getUsername().setEmail("admin@planeta.guru", true);
        user.getProfileUserProperty().setFirstName("Claro VR");
        user.getProfileUserProperty().setLastName("PORTAL");

        boolean isSaved = false;
        try {
            //  user.getCredentialUserProperty().setPassword("Javier1234");
            user.getCredentialUserProperty().getClient().clientCreate();
            user.getCredentialUserProperty().getClient().setClientName("Claro VR");
            userService.saveOrUpdate(user);
            isSaved = true;
        } catch (Exception ex) {
        }

        assertTrue(isSaved);
    }
}
