/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth.filter;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jordonez
 */
@Singleton
@lombok.extern.log4j.Log4j2
public class AuthorizationRequestFilter implements Filter {

    private final String appName;
    private final String profile;
    private final String instance;
    private final String container;
    private final InstrumentedObject instrumentedObject;

    @Inject
    public AuthorizationRequestFilter(@Named("app.name") String appName,
            @Named("guice.profile") String profile,
            @Named("guice.instance") String instance,
            @Named("guice.container") String container,
            final InstrumentedObject instrumentedObject) {
        this.appName = appName;
        this.profile = profile;
        this.instance = instance;
        this.container = container;
        this.instrumentedObject = instrumentedObject;
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {
        Long time = System.currentTimeMillis();
        HttpServletRequest req = ((HttpServletRequest) request);
        String uri = req.getRequestURI();
        String ip = request.getRemoteAddr();
        String tid = CommonFunction.getTID();
        Thread.currentThread().setName(tid);
        String referer = "-";
        String queryString = null;
        try {
            try {
                referer = req.getHeader("referer");
            } catch (Exception ex) {
            }
            try {
                queryString = req.getQueryString();
            } catch (Exception ex) {
            }

            if (uri.contains("/infra/test")) {
                log.debug("Request. IP: [" + ip + "] "
                        + (referer == null ? "" : "- referer: [" + referer + "] ")
                        + "- URI: [" + uri + "] "
                        + (queryString == null ? "" : "- queryString: [" + queryString + "] "));
            } else {
                log.info("Request. IP: [" + ip + "] "
                        + (referer == null ? "" : "- referer: [" + referer + "] ")
                        + "- URI: [" + uri + "] "
                        + (queryString == null ? "" : "- queryString: [" + queryString + "] "));
            }
        } catch (Exception ex) {
            log.error("RequestFilter error. " + ex, ex);
        }

        filterChain.doFilter(request, response);

        HttpServletResponse resp = ((HttpServletResponse) response);
        if (uri.contains("/infra/test")) {
            log.debug("Response [" + resp.getStatus() + "]. IP: [" + ip + "] "
                    + (referer == null ? "" : "- referer: [" + referer + "] ")
                    + "- URI: [" + uri + "] "
                    + (queryString == null ? "" : "- queryString: [" + queryString + "] "));
        } else {
            log.info("Response [" + resp.getStatus() + "]. IP: [" + ip + "] "
                    + (referer == null ? "" : "- referer: [" + referer + "] ")
                    + "- URI: [" + uri + "] "
                    + (queryString == null ? "" : "- queryString: [" + queryString + "] "));
        }
    }

    @Override
    public void destroy() {
    }

}
