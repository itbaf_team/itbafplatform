/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.modules;


import com.google.inject.AbstractModule;

/**
 *
 * @author javier
 */
public class GuiceInstrumentedConfigModule extends AbstractModule {

    public static final String influxDataBase = "PaymentHub";

    @Override
    protected void configure() {
        requestInjection(this);
    }
  
}
