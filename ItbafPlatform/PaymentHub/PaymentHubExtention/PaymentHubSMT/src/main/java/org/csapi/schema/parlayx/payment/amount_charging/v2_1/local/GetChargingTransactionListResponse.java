
package org.csapi.schema.parlayx.payment.amount_charging.v2_1.local;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.csapi.schema.parlayx.payment.v2_1.ChargingTransactionsType;


/**
 * <p>Java class for getChargingTransactionListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getChargingTransactionListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listSize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="chargingTransactions" type="{http://www.csapi.org/schema/parlayx/payment/v2_1}ChargingTransactionsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getChargingTransactionListResponse", propOrder = {
    "resultCode",
    "listSize",
    "chargingTransactions"
})
public class GetChargingTransactionListResponse {

    protected String resultCode;
    protected Integer listSize;
    protected ChargingTransactionsType chargingTransactions;

    /**
     * Gets the value of the resultCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultCode() {
        return resultCode;
    }

    /**
     * Sets the value of the resultCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultCode(String value) {
        this.resultCode = value;
    }

    /**
     * Gets the value of the listSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getListSize() {
        return listSize;
    }

    /**
     * Sets the value of the listSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setListSize(Integer value) {
        this.listSize = value;
    }

    /**
     * Gets the value of the chargingTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link ChargingTransactionsType }
     *     
     */
    public ChargingTransactionsType getChargingTransactions() {
        return chargingTransactions;
    }

    /**
     * Sets the value of the chargingTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChargingTransactionsType }
     *     
     */
    public void setChargingTransactions(ChargingTransactionsType value) {
        this.chargingTransactions = value;
    }

}
