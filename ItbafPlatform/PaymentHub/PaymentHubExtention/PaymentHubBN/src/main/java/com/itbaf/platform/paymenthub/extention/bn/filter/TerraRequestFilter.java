/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.bn.filter;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author jordonez
 */
@Singleton
@lombok.extern.log4j.Log4j2
public class TerraRequestFilter implements Filter {

    @Inject
    public TerraRequestFilter() {
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = ((HttpServletRequest) request);
        String uri = req.getRequestURI();
        // if (uri.contains("/terra/")) {
        String xml = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String aux;
        while ((aux = br.readLine()) != null) {
            xml = xml + aux;
        }
        request.setAttribute("notification", xml);
        // }

        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

}
