/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.npay.model;

import com.google.common.base.MoreObjects;
import java.io.Serializable;

/**
 *
 * @author javier
 */
public class HubDetail implements Serializable {

    private static final long serialVersionUID = 2019010911001L;

    public String s_id;
    public String p_id;
    public String country;
    public String carrier;
    public String amount;
    public String subtotal;
    public String tax;
    public String interval;
    public String interval_count;
    public String interval_expire;
    public String currency;
    public String reference_code;
    public String double_opt_in;
    public String type;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("s_id", s_id)
                .add("p_id", p_id)
                .add("country", country)
                .add("carrier", carrier)
                .add("amount", amount)
                .add("subtotal", subtotal)
                .add("tax", tax)
                .add("interval", interval)
                .add("interval_count", interval_count)
                .add("interval_expire", interval_expire)
                .add("currency", currency)
                .add("reference_code", reference_code)
                .add("double_opt_in", double_opt_in)
                .add("type", type)
                .omitNullValues().toString();
    }
}
