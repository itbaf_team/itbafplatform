package com.itbaf.platform.paymenthub.extention.store.model.notification;

import com.google.common.base.MoreObjects;
import java.math.BigDecimal;
import java.util.Date;

@lombok.Getter
@lombok.Setter
public class BillingEventData extends EventData {

    private BillingEventDataStatus status;
    private String msisdn;
    private String shortcode;
    private String serviceId;
    private String serviceName;
    private BigDecimal charged;
    private String operationTimestamp;
    private Date chargedDate;
    private String tid;
    private String chargedTid;
    private String pautaId;
    private String pautaStatus;

    public BillingEventData(String data) {
        super(data);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("status", status)
                .add("msisdn", msisdn)
                .add("shortcode", shortcode)
                .add("serviceId", serviceId)
                .add("serviceName", serviceName)
                .add("charged", charged)
                .add("operationTimestamp", operationTimestamp)
                .add("chargedDate", chargedDate)
                .add("tid", tid)
                .add("chargedTid", chargedTid)
                .add("pautaId", pautaId)
                .add("pautaStatus", pautaStatus)
                .omitNullValues().toString();
    }

    /*public BillingEventData(InfotainmentChargeReportEntry entry, BigDecimal charged, Integer serviceId) {
		super();
		this.status=BillingEventDataStatus.Ok;
		this.phoneNumber=entry.msisdn;
		this.destinationNumber="";
		this.serviceId = serviceId;
		this.charged=charged.doubleValue();
		this.serviceName=entry.csp;
		// TODO: setear 'operationTimestamp' en base al campo entry.codAuth
		this.operationTimestamp = (int)(dateFromAuth(entry.codAuth).getTime() / 1000);
		this.processingTimestamp = (int)(Time.timestamp()/1000);
		this.transactionId = entry.codAuth;
	}

	
	@SuppressWarnings("deprecation")
	private Date dateFromAuth(String codAuth) {
		//16 06 13 00 11 30 93 7248
		
		int year = Integer.parseInt(codAuth.substring(0, 2)) + 2000;
		int month = Integer.parseInt(codAuth.substring(2,4));
		int day = Integer.parseInt(codAuth.substring(4,6));
		int hours = Integer.parseInt(codAuth.substring(6,8));
		int minutes = Integer.parseInt(codAuth.substring(8,10));
		int seconds = Integer.parseInt(codAuth.substring(10,12));
		
		return new Date(year - 1900,month-1,day,hours,minutes,seconds);
	}*/
    @Override
    protected void parseData() {
        String[] values = this.getValues();

        this.status = BillingEventDataStatus.fromCode(Integer.parseInt(values[0]));
        this.msisdn = values[1];
        this.shortcode = values[2];
        this.serviceId = values[3];
        this.serviceName = values[4];

        if (values[5] != null && !values[5].trim().isEmpty()) {
            this.charged = new BigDecimal(values[5].trim());
        } else {
            this.charged = BigDecimal.ZERO;
        }

        this.operationTimestamp = values[6];
        try {
            this.chargedDate = new Date(Long.parseLong(values[7] + "000"));
        } catch (Exception ex) {
            this.chargedDate = new Date();
        }
        this.tid = values[8];
        try {
            this.chargedTid = (values[9]);
        } catch (Exception ex) {
        }
        try {
            this.pautaId = (values[10]);
        } catch (Exception ex) {
        }
        try {
            this.pautaStatus = (values[11]);
        } catch (Exception ex) {
        }
    }
}
