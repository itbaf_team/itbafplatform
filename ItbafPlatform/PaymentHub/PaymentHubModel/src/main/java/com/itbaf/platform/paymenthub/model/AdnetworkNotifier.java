/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Cacheable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
@Entity(name = "ADNETWORK_NOTIFIER")
@Table(name = "ADNETWORK_NOTIFIER", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class AdnetworkNotifier implements Serializable {

    private static final long serialVersionUID = 20180118153801L;

    @Id
    @Column(name = "SUBSCRIPTION_REGISTRY_ID", nullable = false, unique = true)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "SUBSCRIPTION_STATUS")
    private Subscription.Status status;

    @Column(name = "CHARGED")
    private BigDecimal charged;

    @Column(name = "NOTIFICATION_SEND")
    private Boolean notificationSend;

    @Column(name = "ADN_RESPONSE")
    private String response;

    public AdnetworkNotifier() {
        status = Subscription.Status.PENDING;
        notificationSend = Boolean.FALSE;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("status", status)
                .add("charged", charged)
                .add("notificationSend", notificationSend)
                .add("response", response)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status, charged, notificationSend, response);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<AdnetworkNotifier>()
                .over(AdnetworkNotifier::getId)
                .over(AdnetworkNotifier::getStatus)
                .over(AdnetworkNotifier::getNotificationSend)
                .over(AdnetworkNotifier::getCharged)
                .over(AdnetworkNotifier::getResponse)
                .asserts(this, obj);
    }
}
