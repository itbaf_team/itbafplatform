import { Component } from '@angular/core';

@Component({
  selector: 'ngx-subscriptions',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class SubscriptionsComponent {
}
