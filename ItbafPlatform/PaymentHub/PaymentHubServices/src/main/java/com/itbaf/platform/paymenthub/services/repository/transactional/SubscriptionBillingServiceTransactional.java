/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPASubscriptionBillingRepository;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionBillingService;
import com.itbaf.platform.paymenthub.services.stats.StatsService;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class SubscriptionBillingServiceTransactional implements SubscriptionBillingService {

    private final StatsService statsService;
    private final JPASubscriptionBillingRepository subscriptionBillingRepository;

    @Inject
    public SubscriptionBillingServiceTransactional(final StatsService statsService,
            final JPASubscriptionBillingRepository subscriptionBillingRepository) {
        this.statsService = Validate.notNull(statsService, "A StatsService class must be provided");
        this.subscriptionBillingRepository = Validate.notNull(subscriptionBillingRepository, "A JPASubscriptionBillingRepository class must be provided");
    }

    @Override
    public SubscriptionBilling findById(Long id) throws Exception {
        return subscriptionBillingRepository.findById(id);
    }

    @Override
    public void save(SubscriptionBilling sb) throws Exception {
        subscriptionBillingRepository.save(sb);
        try {
            statsService.sendToRabbit(ElasticData.ElasticType.subscription_billing, sb.getId().toString());
        } catch (Exception ex) {
            log.error("No fue posible enviar a stats-rabbit: [" + ElasticData.ElasticType.subscription_billing + "]. [" + sb + "]. " + ex, ex);
        }
    }

    @Override
    public List<SubscriptionBilling> getSubscriptionBillingsByUserAccount(String userAccount) {
        return subscriptionBillingRepository.getByUserAccount(userAccount);
    }

    @Override
    public List<SubscriptionBilling> getSubscriptionBillingsByUserAccount(Date time, String userAccount) {
        return subscriptionBillingRepository.getByUserAccount(time, userAccount);
    }

    @Override
    public List<Object> getSubscriptionBillingResume(Long id) {
        return subscriptionBillingRepository.getResume(id);
    }
}
