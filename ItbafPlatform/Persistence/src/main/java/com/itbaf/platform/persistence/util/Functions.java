package com.itbaf.platform.persistence.util;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public final class Functions {
	private Functions() {}

	public static <T> Supplier<T> unit() {
		return () -> null;
	}

	public static <A,B> BiFunction<A,B,? super A> first() {
		return (x, __) -> x;
	}

	public static <A,B> BiFunction<A,B,? super B> second() {
		return (__, x) -> x;
	}

	public static <T> Supplier<T> constant(final T value) {
		return () -> value;
	}

	public static <T> Function<T,T> identity() {
		return (x) -> x;
	}
}
