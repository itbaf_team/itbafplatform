pipeline {
  agent any

  environment {
        HELM_RELEASE_NAME = "ph-spiralis" //chart nameOverride
        HELM_CHART_NAME = "itbafplatform" //chart fullnameOverride
        KUBE_NS = "ph"
        KUBE_CLUSTER_PR_NAME = "ph-cluster"
        KUBE_CLUSTER_PR_REGION = "us-central1"

        CONTAINER_REG_PROJECT = "itbaf-infra-pg-prod"
        CONTAINER_IMAGE_NAME = "gcr.io/" + "$CONTAINER_REG_PROJECT" + "/" + "$HELM_RELEASE_NAME"

        APP_POM_PATH = "ItbafPlatform/PaymentHub/PaymentHubExtention/PaymentHubSpiralis"

        BUILD_CONTAINER_NAME = BUILD_TAG.replace("%2F", "_") 

  }

  stages {
    stage('build environment') {
      steps {
        script {
          echo 'Job environment variables:'
          echo sh(returnStdout: true, script: 'env')
          currentBuild.displayName = "#$BUILD_NUMBER" + "_" + BRANCH_NAME.replace("%2F", "_") + "-" + "${GIT_COMMIT[0..7]}"
          echo 'Pull google/cloud-sdk Image Container:'
          sh '''docker pull google/cloud-sdk:latest'''
          echo "Build Container Name: $BUILD_CONTAINER_NAME"
          echo 'Run Environment Build Container:'
          sh '''docker run -id --rm \
                  --name ${BUILD_CONTAINER_NAME} \
                  -v $(pwd):/workspace \
                  -w /workspace \
                  google/cloud-sdk:latest cat'''
          echo 'Install Helm:'
          sh '''docker exec -i ${BUILD_CONTAINER_NAME} \
                  bash -c "curl -L https://git.io/get_helm.sh | bash -s -- --version v2.16.0"'''
        }
      }
    }

    stage('build') {
      steps {
        script {
        echo 'Run Cloud Build:'
        sh '''docker exec -i ${BUILD_CONTAINER_NAME} \
                bash -c "gcloud builds submit ./deployment/secrets/ \
                  --config=./deployment/cloudbuild.yaml \
                  --substitutions=_COMMIT_SHA=${GIT_COMMIT},_CONTAINER_IMAGE_NAME=${CONTAINER_IMAGE_NAME},_REPO_NAME=${GIT_URL},_BRANCH_NAME=${GIT_BRANCH},_POM_PATH=${APP_POM_PATH}"'''
        }
      }
    }

    stage('deploy:production') {
      environment {
        ENVIRONMENT_NAME = "pr"
        KUBE_CLUSTER_NAME = "$KUBE_CLUSTER_PR_NAME"
        KUBE_CLUSTER_REGION = "$KUBE_CLUSTER_PR_REGION"
      }
      when {
        branch 'master'
      }
      steps {
        script {
          changeContext()
          helmDeploy()
        }
      }
    }
  }

  post {
    always {
      script {
        echo 'Cleaning Up Build Environment:'
        sh '''docker rm -f ${BUILD_CONTAINER_NAME}'''
        cleanWs()
      }
    }
  }
}

def changeContext() {
  echo "Change Context: ${env.KUBE_CLUSTER_NAME}"
  sh '''docker exec -i ${BUILD_CONTAINER_NAME} \
          bash -c "gcloud container clusters \
            get-credentials ${KUBE_CLUSTER_NAME} \
            --region ${KUBE_CLUSTER_REGION}"'''
}

def helmDeploy() {
  script {
    try {
      echo 'Helm Upgrade / Install:'
      sh '''docker exec -i ${BUILD_CONTAINER_NAME} \
              bash ./deployment/scripts/deploy_helm.sh \
                --environment-name "${ENVIRONMENT_NAME}" \
                --helm-release-name "${HELM_RELEASE_NAME}" \
                --helm-chart-name "${HELM_CHART_NAME}" \
                --container-image "${CONTAINER_IMAGE_NAME}" \
                --kube-ns "${KUBE_NS}" \
                --git-commit "${GIT_COMMIT}"'''
      sh '''docker exec -i ${BUILD_CONTAINER_NAME} \
              bash -c "kubectl -n ${KUBE_NS} rollout status deployment ${HELM_RELEASE_NAME}"'''
    } catch(e) {
      echo 'ERROR Helm Upgrade / Install. Rollbacking:'
      sh '''docker exec -i ${BUILD_CONTAINER_NAME} \
              bash ./deployment/scripts/rollback_helm.sh \
              --helm-release-name "${HELM_RELEASE_NAME}"'''
      sh '''docker exec -i ${BUILD_CONTAINER_NAME} \
              bash -c "kubectl -n ${KUBE_NS} rollout status deployment ${HELM_RELEASE_NAME}"'''
      error "Failed ${env.ENVIRONMENT_NAME} deployment"
    }
  }
}
