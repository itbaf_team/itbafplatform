/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.bn.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class WAUNotification extends BNNotification {

    public enum Type {
        SubscriptionResult,
        SubscriptionEvent
    }

    public enum Events {
        renew,
        partial,
        optout,
        pendingcharge,
        cancelled,
        expired;
    }

    public String countryCode;
    public String telco;
    public Type type;
    public String request_id;
    public String result_code;
    public String subscription_id;
    public Events event;
    public String event_date;
    public String expire_date;
    public String channel_id;
    public String mobile_number;
    public String product_id;
    public String keyword;
    public String shortcode;
    public String status;
    public String tariff;
    public String fraction;
    public String reason_id;
    public String thread = Thread.currentThread().getName();

    public WAUNotification() {
    }

    public WAUNotification(Type type, String request_id, String result_code, String subscription_id,
            String event, String event_date, String expire_date, String channel_id,
            String mobile_number, String product_id, String keyword, String shortcode,
            String status, String tariff, String fraction, String reason_id) {
        this.type = type;
        this.request_id = request_id != null && !"null".equals(request_id.toLowerCase()) ? request_id : null;
        this.result_code = result_code != null && !"null".equals(result_code.toLowerCase()) ? result_code : null;
        this.subscription_id = subscription_id;
        if (event != null) {
            this.event = WAUNotification.Events.valueOf(event.toLowerCase());
        }
        this.event_date = event_date != null && !"null".equals(event_date.toLowerCase()) ? event_date : null;
        this.expire_date = expire_date != null && !"null".equals(expire_date.toLowerCase()) ? expire_date : null;
        this.channel_id = channel_id != null && !"null".equals(channel_id.toLowerCase()) ? channel_id : null;
        this.mobile_number = mobile_number;
        this.product_id = product_id;
        this.keyword = keyword != null && !"null".equals(keyword.toLowerCase()) ? keyword : null;
        this.shortcode = shortcode != null && !"null".equals(shortcode.toLowerCase()) ? shortcode : null;;
        this.status = status != null && !"null".equals(status.toLowerCase()) ? status : null;
        this.tariff = tariff != null && !"null".equals(tariff.toLowerCase()) ? tariff : null;
        this.fraction = fraction != null && !"null".equals(fraction.toLowerCase()) ? fraction : null;
        this.reason_id = reason_id != null && !"null".equals(reason_id.toLowerCase()) ? reason_id : null;
    }

    public String getChannel() {
        if (channel_id == null) {
            return null;
        }
        switch (channel_id) {
            case "1":
                return "WEB";
            case "2":
                return "SMS";
            case "3":
                return "WAP";
            case "4":
                return "IVR";
            case "5":
                return "USSD";
            case "6":
                return "SAT";
            case "7":
                return "SIM";
            case "10":
                return "MOVISTAR_STORE_10";
            case "11":
                return "MOVISTAR_STORE_11";
            case "12":
                return "SMSConsulting";
            case "50":
                return "MEDIA_MANAGER";
            case "71":
                return "CALL_CENTER";
            default:
                log.error("No existe un channel conocido para: [" + channel_id + "]");
                return "DESC:" + channel_id;
        }
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("countryCode", countryCode)
                .add("telco", telco)
                .add("type", type)
                .add("request_id", request_id)
                .add("result_code", result_code)
                .add("subscription_id", subscription_id)
                .add("event", event)
                .add("event_date", event_date)
                .add("expire_date", expire_date)
                .add("channel_id", channel_id)
                .add("mobile_number", mobile_number)
                .add("product_id", product_id)
                .add("keyword", keyword)
                .add("shortcode", shortcode)
                .add("status", status)
                .add("tariff", tariff)
                .add("fraction", fraction)
                .add("reason_id", reason_id)
                .add("thread", thread)
                .omitNullValues().toString();
    }
}
