import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-modal',
  template: `
    <div class="modal-header">
      <span>{{ modalHeader }}</span>
      <button class="close" aria-label="Close" (click)="closeModal()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      {{ modalContent }}
    </div>
    <div class="modal-footer">
      <button class="btn btn-md btn-outline-primary" (click)="closeModal()">Volver</button>
      <button class="btn btn-md btn-outline-danger" (click)="deleteSubscription()">Cancelar Suscr.</button>
    </div>
  `,
})
export class ModalComponent {

    modalHeader: string;
    modalContent = ``;
    modalComponent: any;
    modalSubscription: any;

    constructor(private activeModal: NgbActiveModal) { }

    deleteSubscription(){
       this.modalComponent.deleteSubscription(this.modalSubscription);  
       this.closeModal();        
    } 

    closeModal() {
      this.activeModal.close();
    }
}
