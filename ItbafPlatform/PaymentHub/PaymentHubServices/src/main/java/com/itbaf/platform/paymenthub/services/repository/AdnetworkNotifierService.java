/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.AdnetworkNotifier;

/**
 *
 * @author javier
 */
public interface AdnetworkNotifierService {

    public AdnetworkNotifier findById(Long id) throws Exception;

    public void saveOrUpdate(AdnetworkNotifier adNotifier) throws Exception;

}
