package com.itbaf.platform.paymenthub.extention.bn.modules.rest;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import static com.itbaf.platform.commons.CommonFunction.NPAY_QUEUE_NAME;
import com.itbaf.platform.paymenthub.commons.bn.model.BorderNotificationMessage;
import com.itbaf.platform.paymenthub.commons.bn.model.MPNotification;
import com.itbaf.platform.paymenthub.commons.bn.model.NPAYNotification;
import com.itbaf.platform.paymenthub.commons.bn.model.SpiralisNotification;
import com.itbaf.platform.paymenthub.commons.bn.model.WAUNotification;
import com.itbaf.platform.paymenthub.commons.bn.model.MDivulgaNotification;
import com.itbaf.platform.paymenthub.extention.bn.handler.ServiceProcessorHandler;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.paymenthub.commons.mdivulga.model.MDivulgaResponse;
import static com.restfb.util.UrlUtils.urlDecode;
import java.io.StringWriter;
import java.util.Map;

@Path("/notification")
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationRestService {

    private final XmlMapper xmlMapper;
    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public NotificationRestService(final XmlMapper xmlMapper,
            ServiceProcessorHandler serviceProcessorHandler) {
        this.xmlMapper = Validate.notNull(xmlMapper, "A XmlMapper class must be provided");
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
    }

    @POST
    @Path("/{country}/cdag/subscription")
    @Consumes("application/soap+xml")
    @Produces("application/soap+xml")
    public Response cdagSubscriptionNotification(final @PathParam("country") String countryCode, String soap) {

        soap = soap.replace("\n", "").replace("\r", "");
        log.info("Notificacion de CDAG. countryCode: " + countryCode + " - soap: " + soap);
        String soapResponse = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\"><soap:Body><ns2:NotificarEventoResponse xmlns:ns4=\"urn:SendatelSoaService\" xmlns:ns3=\"urn:SendatelSuscripcionesService\" xmlns:ns2=\"urn:CdagNotificationService\"/></soap:Body></soap:Envelope>";
        try {
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.message = soap;
            serviceProcessorHandler.processNotification(countryCode.toLowerCase() + ".personal.cdag", nm);
        } catch (Exception ex) {
            log.error("Error al interpretar SOAP. CountryCode [" + countryCode + "] - Subscription Notification: [" + soap + "]. " + ex, ex);

        }
        return Response.status(Status.OK).entity(soapResponse).type("application/soap+xml").build();
    }


    @POST
    @Path("/{country}/store/subscription")
    @Consumes("application/soap+xml")
    @Produces("application/soap+xml")
    public Response storeSubscriptionNotification(final @PathParam("country") String countryCode, String soap) {

        soap = soap.replace("\n", "").replace("\r", "");
        log.info("Notificacion de Store. countryCode: " + countryCode + " - soap: " + soap);
        String soapResponse = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\"><soap:Body><ns2:NotificarEventoResponse xmlns:ns4=\"urn:SendatelSoaService\" xmlns:ns3=\"urn:SendatelSuscripcionesService\" xmlns:ns2=\"urn:StoreNotificationService\"/></soap:Body></soap:Envelope>";
        try {
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.message = soap;
            serviceProcessorHandler.processNotification(countryCode.toLowerCase() + ".personal.store", nm);
        } catch (Exception ex) {
            log.error("Error al interpretar SOAP. CountryCode [" + countryCode + "] - Subscription Notification: [" + soap + "]. " + ex, ex);

        }
        return Response.status(Status.OK).entity(soapResponse).type("application/soap+xml").build();
    }
////////////////////////////////////////////////////////////////////////////////

    @POST
    @Path("/npay/subscription")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response npaySubscriptionNotification(final MultivaluedMap<String, String> formParams) {

        log.info("Notificacion de NPAY. FormParams: [" + Arrays.toString(formParams.entrySet().toArray()) + "]");

        if (formParams.getFirst("id_app") == null || formParams.getFirst("id_service") == null
                || formParams.getFirst("id_customer") == null || formParams.getFirst("id_subscription") == null) {
            log.warn("Noitificacion con parametros nulos.... [" + Arrays.toString(formParams.entrySet().toArray()) + "]");
            return Response.status(Status.BAD_REQUEST).entity("ERROR").type(MediaType.TEXT_PLAIN).build();
        }

        NPAYNotification notification = new NPAYNotification(formParams.getFirst("id_event"),
                formParams.getFirst("ipn_url"), formParams.getFirst("ipn_type"),
                formParams.getFirst("verify_sign"), formParams.getFirst("id_app"),
                formParams.getFirst("id_customer"), formParams.getFirst("id_transaction"),
                formParams.getFirst("amount"), formParams.getFirst("currency"),
                formParams.getFirst("id_subscription"), formParams.getFirst("status"),
                formParams.getFirst("id_service"), formParams.getFirst("created"),
                formParams.getFirst("updated"), formParams.getFirst("tracking_id"),
                formParams.getFirst("meta"), formParams.getFirst("subscription_date"),
                formParams.getFirst("billing_date"), formParams.getFirst("channel"),
                formParams.getFirst("cancelation_date"), formParams.getFirst("tried_at"));

        BorderNotificationMessage nm = new BorderNotificationMessage();
        nm.notification = notification;
        serviceProcessorHandler.processNotification(NPAY_QUEUE_NAME, nm);
        return Response.status(Status.OK).entity("OK").type(MediaType.TEXT_PLAIN).build();
    }
////////////////////////////////////////////////////////////////////////////////

    @GET
    @Path("/{country}/sc/subscription")
    // @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    // @Produces(MediaType.APPLICATION_JSON)
    public Response scSubscriptionNotificationGET(final @PathParam("country") String countryCode, final String json) {
        return scSubscriptionProcess(countryCode, json);
    }

    @POST
    @Path("/{country}/sc/subscription")
    // @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    // @Produces(MediaType.APPLICATION_JSON)
    public Response scSubscriptionNotificationPOST(final @PathParam("country") String countryCode, final String json) {
        return scSubscriptionProcess(countryCode, json);
    }

    private Response scSubscriptionProcess(final String countryCode, final String json) {

        log.info("Notificacion de SmartContents. countryCode: [" + countryCode + "] - Notification: [" + json + "]");
        try {
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.message = json;
            serviceProcessorHandler.processNotification(countryCode.toLowerCase() + ".vivo.smartcontents", nm);
        } catch (Exception ex) {
            log.error("Error al procesar notificacion: [" + json + "]. " + ex, ex);
        }

        return Response.status(Status.OK).entity("OK").type(MediaType.TEXT_PLAIN).build();
    }
////////////////////////////////////////////////////////////////////////////////

    /*   @POST
    @Path("/{country}/smsc/subscription")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response smscSubscriptionNotification(final @PathParam("country") String countryCode,
            @FormParam("ctr") final String ctr, String aux) {

        log.info("Notificacion de SMSC. countryCode: " + countryCode + " - CTR: " + ctr);
        try {
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.message = ctr;
            serviceProcessorHandler.processNotification(countryCode.toLowerCase() + ".movistar.smsconsulting", nm);
        } catch (Exception ex) {
            log.error("Error al interpretar CTR. CountryCode [" + countryCode + "] - Subscription Notification: [" + ctr + "]. " + ex, ex);
        }

        return Response.status(Status.OK).build();
    }*/
    @POST
    @Path("/{country}/smsc/subscription")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response smscSubscriptionNotification(final @PathParam("country") String countryCode,
            final String ctr) {

        log.info("Notificacion de SMSC. countryCode: " + countryCode + " - CTR: " + ctr);
        try {
            String aux = ctr;
            if (aux != null && aux.startsWith("ctr=")) {
                aux = aux.replace("ctr=", "");
                if (aux.startsWith("%")) {
                    aux = urlDecode(ctr);
                    log.info("Notificacion de SMSC. Decode. countryCode: " + countryCode + " - CTR: " + aux);
                }
            }

            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.message = aux;
            serviceProcessorHandler.processNotification(countryCode.toLowerCase() + ".movistar.smsconsulting", nm);
        } catch (Exception ex) {
            log.error("Error al interpretar CTR. CountryCode [" + countryCode + "] - Subscription Notification: [" + ctr + "]. " + ex, ex);
        }

        return Response.status(Status.OK).build();
    }

////////////////////////////////////////////////////////////////////////////////
    @POST
    @Path("/{country}/smt/subscription")
    @Produces(MediaType.TEXT_XML)
    public Response smtSubscriptionNotification(final @PathParam("country") String countryCode, String soap) {

        soap = soap.replace("\n", "").replace("\r", "");
        log.info("Notificacion de Suscripcion SMT. CountryCode [" + countryCode + "] - Notification: [" + soap + "]");

        try {
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.message = soap;
            serviceProcessorHandler.processNotification(countryCode + ".claro.smt", nm);
        } catch (Exception ex) {
            log.error("Error al interpretar SOAP. CountryCode [" + countryCode + "] - Subscription Notification: [" + soap + "]. " + ex, ex);
        }

        String soapResponse = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body>"
                + "<ns2:syncOrderRelationshipResponse xmlns:ns2=\"http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\">"
                + "<ns2:result><resultCode>00000000</resultCode><resultMessage>success</resultMessage></ns2:result>"
                + "</ns2:syncOrderRelationshipResponse></soap:Body></soap:Envelope>";
        return Response.status(Status.OK).entity(soapResponse).type(MediaType.TEXT_XML).build();
    }

    @POST
    @Path("/{country}/smt/sms")
    @Produces(MediaType.TEXT_XML)
    public Response smsNotification(final @PathParam("country") String countryCode, String soap) {

        soap = soap.replace("\n", "").replace("\r", "");
        String soapResponse = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:loc=\"http://www.csapi.org/schema/parlayx/sms/notification/v2_2/local\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<loc:notifySmsReceptionResponse/>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";
        try {
            if (soap.contains("notifySmsReception")) {
                log.info("Notificacion de SMS SMT. NotifySmsReception. CountryCode [" + countryCode + "] - Notification: [" + soap + "]");
                BorderNotificationMessage nm = new BorderNotificationMessage();
                nm.countryCode = countryCode;
                nm.message = soap;
                serviceProcessorHandler.processNotification(countryCode + ".claro.smt.sms", nm);
            } else if (soap.contains("notifySmsDeliveryReceipt")) {
                log.info("Notificacion de SMS SMT. NotifySmsDeliveryReceipt. CountryCode [" + countryCode + "] - Notification: [" + soap + "]");
                BorderNotificationMessage nm = new BorderNotificationMessage();
                nm.countryCode = countryCode;
                nm.message = soap;
                serviceProcessorHandler.processNotification(countryCode + ".claro.smt.sms", nm);
                soapResponse = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:loc=\"http://www.csapi.org/schema/parlayx/sms/notification/v2_2/local\">"
                        + "<soapenv:Header/>"
                        + "<soapenv:Body>"
                        + "<loc:notifySmsDeliveryReceiptResponse/>"
                        + "</soapenv:Body>"
                        + "</soapenv:Envelope>";
            } else {
                log.error("Metodo SOAP desconocido para: [" + countryCode + "] - SOAP: [" + soap + "].");
            }
        } catch (Exception ex) {
            log.error("Error al interpretar SOAP. CountryCode [" + countryCode + "] - SMS Notification: [" + soap + "]. " + ex, ex);
        }

        return Response.status(Status.OK).entity(soapResponse).type(MediaType.TEXT_XML).build();
    }

    @GET
    @Path("/{country}/smt/mdivulga/subscription")
    //@Produces(MediaType.TEXT_PLAIN)
    public Response mDivulgaSubscriptionNotificationGet(final @PathParam("country") String countryCode,
            final @Context UriInfo uriInfo) {

        log.info("Notificacion de m-Divulga con SMT. CountryCode [" + countryCode + "] - Notification: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]");

        MDivulgaNotification notification = new MDivulgaNotification();
        notification.setMsisdn(uriInfo.getQueryParameters().getFirst("msisdn"));
        notification.setName(uriInfo.getQueryParameters().getFirst("name"));
        notification.setProduct(uriInfo.getQueryParameters().getFirst("product"));
        notification.setTimestamp(uriInfo.getQueryParameters().getFirst("timestamp"));

        BorderNotificationMessage nm = new BorderNotificationMessage();
        nm.countryCode = countryCode;
        nm.notification = notification;
        serviceProcessorHandler.processNotification(countryCode + ".claro.smt.mdivulga", nm);
        return Response.status(Status.OK).build();
    }

    @GET
    @Path("/{country}/smt/mdivulga/subscription/status")
    @Produces(MediaType.TEXT_XML)
    public Response mDivulgaGetSubscriptionStatus(@Context HttpServletRequest request,
            final @PathParam("country") String countryCode,
            final @Context UriInfo uriInfo) throws Exception {

        log.info("SubscriptionStatus. m-Divulga. CountryCode [" + countryCode + "] - QueryString: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]");
        Map<String, Object> mapResponse = null;
        String basic = request.getHeader("Authorization");
        String msisdn = uriInfo.getQueryParameters().getFirst("msisdn");
        String servicecode = uriInfo.getQueryParameters().getFirst("servicecode");
        Integer status = null;
        String response = null;
        try {
            if (basic != null && msisdn != null && servicecode != null) {
                mapResponse = serviceProcessorHandler.mDivulgaGetSubscriptionStatus(countryCode, basic, msisdn, servicecode);
                status = (Integer) mapResponse.get("status");
                if (401 != status) {
                    response = (String) mapResponse.get("result");
                }

            }
        } catch (Exception ex) {
            log.error("Error al procesar SubscriptionStatus. basic: [" + basic + "] - . " + ex, ex);
            MDivulgaResponse xmlResponse = new MDivulgaResponse();
            xmlResponse.status = "500";
            xmlResponse.description = Status.INTERNAL_SERVER_ERROR.toString();
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(getXmlResponse(xmlResponse)).type(MediaType.TEXT_XML).build();
        }

        if (response == null) {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        return Response.status(status).entity(response).type(MediaType.TEXT_XML).build();
    }

    /**
     * Esto adiciona <?xml version='1.0' encoding='UTF-8'?> para no hardcodearlo
     * Sin este metodo, se puede hacer:
     * Response.status(Status.OK).entity(xmlResponse).type(MediaType.TEXT_XML).build()
     */
    private String getXmlResponse(MDivulgaResponse xmlResponse) throws Exception {
        StringWriter stringWriter = new StringWriter();
        javax.xml.stream.XMLOutputFactory xmlOutputFactory = javax.xml.stream.XMLOutputFactory.newFactory();
        javax.xml.stream.XMLStreamWriter sw = xmlOutputFactory.createXMLStreamWriter(stringWriter);

        sw.writeStartDocument();
        // sw.writeStartElement("root");
        xmlMapper.writeValue(sw, xmlResponse);
        // sw.writeEndElement();
        sw.writeEndDocument();
        return stringWriter.toString();
    }

////////////////////////////////////////////////////////////////////////////////
    @GET
    @Path("/{country}/spiralis/subscription")
    @Produces(MediaType.TEXT_PLAIN)
    public Response spiralisSubscriptionNotificationGet(final @PathParam("country") String countryCode,
            final @Context UriInfo uriInfo) {

        log.info("Notificacion de Spiralis. CountryCode [" + countryCode + "] - Notification: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]");

        SpiralisNotification notification = new SpiralisNotification(uriInfo.getQueryParameters().get("id").get(0),
                uriInfo.getQueryParameters().getFirst("msisdn"),
                uriInfo.getQueryParameters().getFirst("service"),
                uriInfo.getQueryParameters().getFirst("created"),
                uriInfo.getQueryParameters().getFirst("type"),
                uriInfo.getQueryParameters().getFirst("status"),
                uriInfo.getQueryParameters().getFirst("channelIn"),
                uriInfo.getQueryParameters().getFirst("channelOut"),
                uriInfo.getQueryParameters().getFirst("keywordIn"),
                uriInfo.getQueryParameters().getFirst("keywordOut"),
                uriInfo.getQueryParameters().getFirst("category"),
                uriInfo.getQueryParameters().getFirst("fee"),
                uriInfo.getQueryParameters().getFirst("adProvider"));

        BorderNotificationMessage nm = new BorderNotificationMessage();
        nm.countryCode = countryCode;
        nm.notification = notification;
        serviceProcessorHandler.processNotification(countryCode + ".claro.spiralis", nm);
        return Response.status(Status.OK).entity("OK").type(MediaType.TEXT_PLAIN).build();
    }
////////////////////////////////////////////////////////////////////////////////

    @POST
    @Path("{country}/terra/event")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_XML)
    public Response terraNotificationPost(final @PathParam("country") String countryCode,
            @Context HttpServletRequest request) {

        String xml = "";
        try {
            xml = (String) request.getAttribute("notification");
            log.info("Notificacion de Terra: [" + countryCode + "]. XML: [" + xml + "] - Notification: [" + xml + "]");
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.message = xml;
            serviceProcessorHandler.processNotification(countryCode + ".movistar.terra", nm);
        } catch (Exception ex) {
            log.error("No fue posible procesar el mensaje XML [" + xml + "]. " + ex, ex);
        }

        String response = "<Evento><Resultado>OK</Resultado><Descripcion>SUCCESSFUL</Descripcion></Evento>";
        return Response.status(Status.OK).entity(response).type(MediaType.APPLICATION_XML).build();
    }

    @GET
    @Path("{country}/terra/adn")
    public Response terraNotificationAdnPost(final @PathParam("country") String countryCode,
            final @Context UriInfo uriInfo,
            @Context HttpServletRequest request) {

        log.info("Notificacion de Terra ADN: [" + countryCode + "]. QueryParams: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]");

        String aff_click_id = uriInfo.getQueryParameters().getFirst("idclick");
        try {
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.message = aff_click_id;
            serviceProcessorHandler.processNotification(CommonFunction.PARTIAL_ADN_NOTIFICATION, nm);
        } catch (Exception ex) {
            log.error("No fue posible procesar el mensaje ADN: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]. " + ex, ex);
        }

        String response = "OK";
        return Response.status(Status.OK).entity(response).build();
    }
////////////////////////////////////////////////////////////////////////////////

    @GET
    @Path("/{country}/{telco}/wau/subscription")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public Response subscriptionNotification(final @PathParam("country") String countryCode,
            final @PathParam("telco") String telco, final @Context UriInfo uriInfo) {

        log.info("Notificacion de WAU. CountryCode: [" + countryCode + "] - telco: [" + telco + "] - Notification: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]");

        try {
            WAUNotification not = new WAUNotification(WAUNotification.Type.valueOf(uriInfo.getQueryParameters().getFirst("type")),
                    uriInfo.getQueryParameters().getFirst("request_id"), uriInfo.getQueryParameters().getFirst("result_code"),
                    uriInfo.getQueryParameters().getFirst("subscription_id"), uriInfo.getQueryParameters().getFirst("event"),
                    uriInfo.getQueryParameters().getFirst("event_date"), uriInfo.getQueryParameters().getFirst("expire_date"),
                    uriInfo.getQueryParameters().getFirst("channel_id"), uriInfo.getQueryParameters().getFirst("mobile_number"),
                    uriInfo.getQueryParameters().getFirst("product_id"), uriInfo.getQueryParameters().getFirst("keyword"),
                    uriInfo.getQueryParameters().getFirst("shorcode"), uriInfo.getQueryParameters().getFirst("status"),
                    uriInfo.getQueryParameters().getFirst("tariff"), uriInfo.getQueryParameters().getFirst("fraction"),
                    uriInfo.getQueryParameters().getFirst("reason_id"));
            not.countryCode = countryCode;
            not.telco = telco;
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.telco = telco;
            nm.notification = not;
            serviceProcessorHandler.processNotification(countryCode + "." + telco + ".wau", nm);
        } catch (Exception ex) {
            log.error("Error al interpretar CTR. CountryCode [" + countryCode + "] - Notification: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]. " + ex, ex);
        }

        return Response.status(Status.OK).entity("Ok").build();
    }
////////////////////////////////////////////////////////////////////////////////

    @POST
    @Path("/{country}/mp")
    @Produces(MediaType.APPLICATION_JSON)
    public Response mpSubscriptionNotification(final @PathParam("country") String countryCode,
            @QueryParam("topic") final String topic, @QueryParam("id") final String id) {

        log.info("Notificacion de MercadoPago. CountryCode: [" + countryCode + "] - Topic: [" + topic + "] - ID: [" + id + "]");
        try {
            MPNotification mp = new MPNotification();
            mp.mpTopic = topic;
            mp.id = id;
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.notification = mp;
            serviceProcessorHandler.processNotification(countryCode + ".mp", nm);
        } catch (Exception ex) {
            log.error("Error al procesar notificacion de MercadoPago. CountryCode: [" + countryCode + "] - Topic: [" + topic + "] - ID: [" + id + "]. " + ex, ex);
        }
        return Response.status(Status.OK).entity("OK").type(MediaType.APPLICATION_JSON).build();
    }

    ////////////////////////////////////////////////////////////////////////////////
    @POST
    @Path("/{country}/tiaxa/subscription")
    @Produces(MediaType.TEXT_XML)
    public Response tiaxaSubscriptionNotification(final @PathParam("country") String countryCode, String soap) {

        soap = soap.replace("\n", "").replace("\r", "");
        log.info("Notificacion de Suscripcion Tiaxa. CountryCode [" + countryCode + "] - Notification: [" + soap + "]");

        try {
            BorderNotificationMessage nm = new BorderNotificationMessage();
            nm.countryCode = countryCode;
            nm.message = soap;
            serviceProcessorHandler.processNotification(countryCode + ".claro.tiaxa", nm);
        } catch (Exception ex) {
            log.error("Error al interpretar SOAP. CountryCode [" + countryCode + "] - Subscription Notification: [" + soap + "]. " + ex, ex);
        }

        String soapResponse = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body>"
                + "<ns2:syncOrderRelationshipResponse xmlns:ns2=\"http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\">"
                + "<ns2:result><resultCode>00000000</resultCode><resultMessage>success</resultMessage></ns2:result>"
                + "</ns2:syncOrderRelationshipResponse></soap:Body></soap:Envelope>";
        return Response.status(Status.OK).entity(soapResponse).type(MediaType.TEXT_XML).build();
    }
}
