/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.tiaxa.resources;

/**
 *
 * @author javier
 */
public class TiaxaRequest {

    public String appId;
    public String transID;
    public String partnerID;
    public String version = "5.0";
    public UserID userID = new UserID();
    public SubInfoType subInfo = new SubInfoType();
    public Integer channelId;

    public class UserID {

        public String ID;
        public int userType = 0;
    }

    public class SubInfoType {

        public String productID;
    }
}
