/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.module;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.itbaf.platform.paymenthub.repository.jpa.JPAInitializer;
import com.itbaf.platform.persistence.connection.BasicDBConnectionConfig;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import static com.itbaf.platform.persistence.connection.BasicDataBaseConnection.PAYMENTHUB_PERSISTENT_UNIT_NAME_DOMAIN;
import com.itbaf.platform.persistence.connection.DBConnectionConfig;
import com.itbaf.platform.persistence.connection.DataBaseConnection;

/**
 *
 * @author jordonez
 */
public class GuiceRepositoryConfigModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new JpaPersistModule(PAYMENTHUB_PERSISTENT_UNIT_NAME_DOMAIN));
        bind(DataBaseConnection.class).to(BasicDataBaseConnection.class);
        bind(JPAInitializer.class).asEagerSingleton();
        requestInjection(this);
    }

    @Inject
    void initJPAInitializer(JPAInitializer jpaInitializer) {
        jpaInitializer.startService();
    }

    @Provides
    @Singleton
    BasicDBConnectionConfig basicDBConnectionConfig(@Named("app.name") String appName,
            @Named("guice.profile") String profile,
            @Named("guice.instance") String instance,
            @Named("paymenthub.datasource.url") String phdbUrl,
            @Named("paymenthub.datasource.username") String phdbUsername,
            @Named("paymenthub.datasource.password") String phdbPassword,
            @Named("paymenthub.datasource.driver") String phdbDriver) {

        BasicDBConnectionConfig config = new BasicDBConnectionConfig(appName + "." + profile + "." + instance);
        config.getConfig().put(PAYMENTHUB_PERSISTENT_UNIT_NAME_DOMAIN, new DBConnectionConfig(phdbUrl, phdbUsername, phdbPassword, phdbDriver));

        return config;
    }

}
