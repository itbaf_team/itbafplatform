package com.itbaf.platform.paymenthub.extention.store.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.extention.store.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.extention.store.ws.notification.NotificarEvento;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import org.apache.commons.lang3.Validate;

@Path("/notification")
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationRestService {

    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public NotificationRestService(ServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");

    }

    @POST
    @Path("/{country}/store/subscription")
    @Consumes("application/soap+xml")
    @Produces("application/soap+xml")
    public Response subscriptionNotification(final @PathParam("country") String countryCode, String soap) {

        soap = soap.replace("\n", "").replace("\r", "");
        log.info("Notificacion de Store. countryCode: " + countryCode + " - soap: " + soap);
        MessageFactory factory = null;
        String soapResponse = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\"><soap:Body><ns2:NotificarEventoResponse xmlns:ns4=\"urn:SendatelSoaService\" xmlns:ns3=\"urn:SendatelSuscripcionesService\" xmlns:ns2=\"urn:storeNotificationService\"/></soap:Body></soap:Envelope>";
        if (soap.contains("http://www.w3.org/2003/05/soap-envelope")) {
            try {
                factory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
            } catch (SOAPException ex) {
            }
        } else {
            try {
                factory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
            } catch (SOAPException ex) {
            }
        }
        try {
            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(soap.getBytes(Charset.forName("UTF-8"))));
            JAXBContext jbc = JAXBContext.newInstance(NotificarEvento.class);
            Unmarshaller um = jbc.createUnmarshaller();
            JAXBElement<NotificarEvento> element = um.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument(), NotificarEvento.class);
            NotificarEvento notifications = element.getValue();
            serviceProcessorHandler.processNotification(countryCode, notifications);
        } catch (Exception ex) {
            log.error("Error al interpretar SOAP. CountryCode [" + countryCode + "] - Subscription Notification: [" + soap + "]. " + ex, ex);

        }

        return Response.status(Status.OK).entity(soapResponse).type("application/soap+xml").build();
    }
}
