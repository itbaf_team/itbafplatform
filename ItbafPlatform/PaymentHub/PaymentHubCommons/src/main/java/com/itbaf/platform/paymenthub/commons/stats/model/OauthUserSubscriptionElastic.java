/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.stats.model;

import com.google.common.base.MoreObjects;
import java.io.Serializable;

/**
 *
 * @author JF
 */
public class OauthUserSubscriptionElastic extends ElasticData implements Serializable {

    private static final long serialVersionUID = 20190305095501L;

    public OauthUserSubscriptionElastic() {
        super(ElasticData.ElasticType.oauth_user_subscription);
    }
    public Long userId;
    public String userAccount;
    public Long subscriptionId;
    public Long SubscriptionRegistryId;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("userId", userId)
                .add("userAccount", userAccount)
                .add("subscriptionId", subscriptionId)
                .add("SubscriptionRegistryId", SubscriptionRegistryId)
                .omitNullValues().toString();
    }
}
