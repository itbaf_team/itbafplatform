/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adn.modules;

import com.google.inject.multibindings.Multibinder;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.modules.GuiceDefaultConfigModule;
import com.itbaf.platform.paymenthub.adn.processor.BillingAdnetworkProcessor;
import com.itbaf.platform.paymenthub.adn.processor.SubscriptionAdnetworkProcessor;
import com.itbaf.platform.paymenthub.adn.processor.SimpleAdnetworkProcessor;
import com.itbaf.platform.paymenthub.repository.module.GuiceRepositoryConfigModule;
import com.itbaf.platform.paymenthub.services.module.GuiceServiceConfigModule;
import com.itbaf.platform.rmq.module.GuiceRabbitMQConfigModule;
import java.util.Arrays;
import java.util.Map;
import com.itbaf.platform.paymenthub.adn.processor.AdnetworkProcessor;
import com.itbaf.platform.paymenthub.adn.processor.OnDemandBillingAdnetworkProcessor;

/**
 *
 * @author jordonez
 */
public class GuiceConfigModule extends GuiceDefaultConfigModule {

    public GuiceConfigModule(final Map<String, String> args, CommonDaemon commonDaemon) {
        super(args,commonDaemon);
    }

    @Override
    protected void configure() {
        super.configure();

        //Binds        
        final Multibinder<AdnetworkProcessor> multi = Multibinder.newSetBinder(binder(), AdnetworkProcessor.class);
        multi.addBinding().to(SubscriptionAdnetworkProcessor.class).asEagerSingleton();
        multi.addBinding().to(BillingAdnetworkProcessor.class).asEagerSingleton();
        multi.addBinding().to(OnDemandBillingAdnetworkProcessor.class).asEagerSingleton();
        multi.addBinding().to(SimpleAdnetworkProcessor.class).asEagerSingleton();

        //Modulos de aplicaciones
        install(new GuiceRepositoryConfigModule());
        install(new GuiceServiceConfigModule());
        install(new GuiceConsumersConfigModule());
        install(new GuiceRabbitMQConfigModule());
        install(new GuiceRestServicesConfigModule());

        log.info("Configuracion iniciada. " + Arrays.toString(args.entrySet().toArray()));
    }

}
