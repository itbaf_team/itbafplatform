
package org.csapi.schema.parlayx.subscribeproduct.v1_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsumeRefundHistoryInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsumeRefundHistoryInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="productID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productName" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}DialectInfo" maxOccurs="unbounded"/>
 *         &lt;element name="spID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="spName" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}DialectInfo" maxOccurs="unbounded"/>
 *         &lt;element name="appID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="appName" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}DialectInfo" maxOccurs="unbounded"/>
 *         &lt;element name="fee" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="chargedTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cycleStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cycleEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chargeType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="chargePercentage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="direction" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="channelID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="smtChargeTransactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refundFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refundTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refundTransactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refundOperatorID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refundCause" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extensionInfo" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}NamedParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsumeRefundHistoryInfo", propOrder = {
    "productID",
    "productName",
    "spID",
    "spName",
    "appID",
    "appName",
    "fee",
    "chargedTime",
    "cycleStartTime",
    "cycleEndTime",
    "chargeType",
    "chargePercentage",
    "eventType",
    "direction",
    "channelID",
    "smtChargeTransactionID",
    "refundFlag",
    "refundTime",
    "refundTransactionID",
    "refundOperatorID",
    "refundCause",
    "extensionInfo"
})
public class ConsumeRefundHistoryInfo {

    @XmlElement(required = true)
    protected String productID;
    @XmlElement(required = true)
    protected List<DialectInfo> productName;
    @XmlElement(required = true)
    protected String spID;
    @XmlElement(required = true)
    protected List<DialectInfo> spName;
    @XmlElement(required = true)
    protected String appID;
    @XmlElement(required = true)
    protected List<DialectInfo> appName;
    @XmlElement(required = true)
    protected String fee;
    protected String chargedTime;
    protected String cycleStartTime;
    protected String cycleEndTime;
    protected Integer chargeType;
    protected String chargePercentage;
    protected Integer eventType;
    protected Integer direction;
    protected Integer channelID;
    protected String smtChargeTransactionID;
    protected String refundFlag;
    protected String refundTime;
    protected String refundTransactionID;
    protected String refundOperatorID;
    protected String refundCause;
    protected NamedParameterList extensionInfo;

    /**
     * Gets the value of the productID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductID() {
        return productID;
    }

    /**
     * Sets the value of the productID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductID(String value) {
        this.productID = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DialectInfo }
     * 
     * 
     */
    public List<DialectInfo> getProductName() {
        if (productName == null) {
            productName = new ArrayList<DialectInfo>();
        }
        return this.productName;
    }

    /**
     * Gets the value of the spID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpID() {
        return spID;
    }

    /**
     * Sets the value of the spID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpID(String value) {
        this.spID = value;
    }

    /**
     * Gets the value of the spName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the spName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DialectInfo }
     * 
     * 
     */
    public List<DialectInfo> getSpName() {
        if (spName == null) {
            spName = new ArrayList<DialectInfo>();
        }
        return this.spName;
    }

    /**
     * Gets the value of the appID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppID() {
        return appID;
    }

    /**
     * Sets the value of the appID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppID(String value) {
        this.appID = value;
    }

    /**
     * Gets the value of the appName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DialectInfo }
     * 
     * 
     */
    public List<DialectInfo> getAppName() {
        if (appName == null) {
            appName = new ArrayList<DialectInfo>();
        }
        return this.appName;
    }

    /**
     * Gets the value of the fee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFee() {
        return fee;
    }

    /**
     * Sets the value of the fee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFee(String value) {
        this.fee = value;
    }

    /**
     * Gets the value of the chargedTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargedTime() {
        return chargedTime;
    }

    /**
     * Sets the value of the chargedTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargedTime(String value) {
        this.chargedTime = value;
    }

    /**
     * Gets the value of the cycleStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCycleStartTime() {
        return cycleStartTime;
    }

    /**
     * Sets the value of the cycleStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCycleStartTime(String value) {
        this.cycleStartTime = value;
    }

    /**
     * Gets the value of the cycleEndTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCycleEndTime() {
        return cycleEndTime;
    }

    /**
     * Sets the value of the cycleEndTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCycleEndTime(String value) {
        this.cycleEndTime = value;
    }

    /**
     * Gets the value of the chargeType property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChargeType() {
        return chargeType;
    }

    /**
     * Sets the value of the chargeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChargeType(Integer value) {
        this.chargeType = value;
    }

    /**
     * Gets the value of the chargePercentage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargePercentage() {
        return chargePercentage;
    }

    /**
     * Sets the value of the chargePercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargePercentage(String value) {
        this.chargePercentage = value;
    }

    /**
     * Gets the value of the eventType property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEventType(Integer value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the direction property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDirection() {
        return direction;
    }

    /**
     * Sets the value of the direction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDirection(Integer value) {
        this.direction = value;
    }

    /**
     * Gets the value of the channelID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChannelID() {
        return channelID;
    }

    /**
     * Sets the value of the channelID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChannelID(Integer value) {
        this.channelID = value;
    }

    /**
     * Gets the value of the smtChargeTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmtChargeTransactionID() {
        return smtChargeTransactionID;
    }

    /**
     * Sets the value of the smtChargeTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmtChargeTransactionID(String value) {
        this.smtChargeTransactionID = value;
    }

    /**
     * Gets the value of the refundFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundFlag() {
        return refundFlag;
    }

    /**
     * Sets the value of the refundFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundFlag(String value) {
        this.refundFlag = value;
    }

    /**
     * Gets the value of the refundTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundTime() {
        return refundTime;
    }

    /**
     * Sets the value of the refundTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundTime(String value) {
        this.refundTime = value;
    }

    /**
     * Gets the value of the refundTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundTransactionID() {
        return refundTransactionID;
    }

    /**
     * Sets the value of the refundTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundTransactionID(String value) {
        this.refundTransactionID = value;
    }

    /**
     * Gets the value of the refundOperatorID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundOperatorID() {
        return refundOperatorID;
    }

    /**
     * Sets the value of the refundOperatorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundOperatorID(String value) {
        this.refundOperatorID = value;
    }

    /**
     * Gets the value of the refundCause property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefundCause() {
        return refundCause;
    }

    /**
     * Sets the value of the refundCause property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefundCause(String value) {
        this.refundCause = value;
    }

    /**
     * Gets the value of the extensionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link NamedParameterList }
     *     
     */
    public NamedParameterList getExtensionInfo() {
        return extensionInfo;
    }

    /**
     * Sets the value of the extensionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NamedParameterList }
     *     
     */
    public void setExtensionInfo(NamedParameterList value) {
        this.extensionInfo = value;
    }

}
