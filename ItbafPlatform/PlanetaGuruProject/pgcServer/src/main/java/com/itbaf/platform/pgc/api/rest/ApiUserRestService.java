package com.itbaf.platform.pgc.api.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage.GrantType;
import com.itbaf.platform.messaging.rest.oauth.ResponseAuthMessage;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageRequest;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgc.api.handler.ApiUserServiceProcessorHandler;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;

@Path("/api/user")
@Singleton
@lombok.extern.log4j.Log4j2
public class ApiUserRestService extends CommonRestService {

    private final ApiUserServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public ApiUserRestService(ApiUserServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "An ApiUserServiceProcessorHandler class must be provided");
    }

    @POST
    @Path("/token")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response tokenClient(@Context HttpServletRequest request,
            @FormParam("grant_type") final GrantType grantType,
            @FormParam("client_id") final String clientId,
            @FormParam("code_verifier") final String codeVerifier,
            @FormParam("code") final String code) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.user.token");
        } catch (Exception ex) {
        }
        RequestTokenMessage tm = new RequestTokenMessage();
        tm.grantType = grantType;
        tm.clientId = clientId;
        tm.codeVerifier = codeVerifier;
        tm.code = code;

        log.info("Token. RequestTokenMessage: [" + tm + "]");

        ResponseAuthMessage rm = new ResponseAuthMessage();

        if (isHeavyUser(request, "token." + code)) {
            log.fatal("Request de un HeavyUser. [" + tm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        if (codeVerifier == null || codeVerifier.length() < 43) {
            log.warn("Request con codeVerifier invalido. [" + tm + "]");
            rm.error = ResponseAuthMessage.Error.invalid_request;
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        try {
            switch (grantType) {
                case authorization_code:
                    rm = serviceProcessorHandler.getUserToken(tm);
                    break;
                default:
                    rm.error = ResponseAuthMessage.Error.invalid_grant;
            }
        } catch (Exception ex) {
            rm = new ResponseAuthMessage();
            rm.message = "Error al obtener Token. " + ex;
            log.error(rm.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("OK").cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (rm.error == null) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/profile")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProfile(PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.user.profile");
        } catch (Exception ex) {
        }
        log.info("getProfile. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            PGMessageResponse rmx = serviceProcessorHandler.getProfile(messageRequest);

            if (rmx == null) {
                rm.data.message = "Error al procesar peticion de perfil. ";
                rm.meta.code = 500;
                rm.meta.status = Meta.Status.error_internal;
            } else {
                rm = rmx;
            }
        } catch (Exception ex) {
            rm = new PGMessageResponse();
            rm.data.message = "Error al obtener informacion del perfil. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/funds")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postFunds(PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.user.funds");
        } catch (Exception ex) {
        }
        log.info("postFunds. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            PGMessageResponse rmx = serviceProcessorHandler.getFunds(messageRequest);

            if (rmx == null) {
                rm.data.message = "Error al procesar peticion de obtencion de creditos. ";
                rm.meta.code = 500;
                rm.meta.status = Meta.Status.error_internal;
            } else {
                rm = rmx;
            }
        } catch (Exception ex) {
            rm = new PGMessageResponse();
            rm.data.message = "Error al obtener creditos. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postStatus(PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.user.status");
        } catch (Exception ex) {
        }
        log.info("postStatus. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            PGMessageResponse rmx = serviceProcessorHandler.getStatus(messageRequest);

            if (rmx == null) {
                rm.data.message = "Error al procesar peticion de obtencion de Status. ";
                rm.meta.code = 500;
                rm.meta.status = Meta.Status.error_internal;
            } else {
                rm = rmx;
            }
        } catch (Exception ex) {
            rm = new PGMessageResponse();
            rm.data.message = "Error al obtener status. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }
}
