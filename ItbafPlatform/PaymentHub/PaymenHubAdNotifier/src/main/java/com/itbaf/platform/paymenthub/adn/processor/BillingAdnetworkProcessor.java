/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adn.processor;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class BillingAdnetworkProcessor extends AdnetworkProcessor {

    private static final PaymentHubMessage.NotificationType notificationType = PaymentHubMessage.NotificationType.BILLING;

    @Inject
    public BillingAdnetworkProcessor() {

    }

    @Override
    public PaymentHubMessage.NotificationType getNotificationType() {
        return notificationType;
    }

    @Override
    public Boolean process(String msg) throws Exception {
        SubscriptionBilling sb = mapper.readValue(msg, SubscriptionBilling.class);
        return process(sb.getSubscriptionRegistry().getId(), null, sb);
    }

}
