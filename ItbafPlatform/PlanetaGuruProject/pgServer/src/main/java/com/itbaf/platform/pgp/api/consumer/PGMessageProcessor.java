/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.consumer;

import com.google.inject.Inject;
import com.itbaf.platform.messaging.queue.wallet.CurrencyRequest;
import com.itbaf.platform.pgp.api.handler.ApiWalletServiceProcessorHandler;
import com.itbaf.platform.pgp.model.wallet.WalletTransaction;
import com.itbaf.platform.rmq.consumer.MessageProcessor;
import java.math.BigDecimal;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PGMessageProcessor implements MessageProcessor {

    private final ApiWalletServiceProcessorHandler apiWalletServiceProcessorHandler;

    @Inject
    public PGMessageProcessor(final ApiWalletServiceProcessorHandler apiWalletServiceProcessorHandler) {
        this.apiWalletServiceProcessorHandler = Validate.notNull(apiWalletServiceProcessorHandler, "An ApiWalletServiceProcessorHandler class must be provided");
    }

    public Boolean billingProcess(CurrencyRequest cr) {
      //  log.info("BillingRequest Rabbit: [" + cr + "]");

        WalletTransaction wt = null;
        try {
            wt = apiWalletServiceProcessorHandler.walletTransactionCreate(cr, null, null, WalletTransaction.Type.ADD);
        } catch (Exception ex) {
            log.error("Error al crear WalletTransaction a: [" + cr + "]. " + ex, ex);
        }

        if (wt == null) {
          //  log.info("No se procesara la peticion: [" + cr + "]");
            return false;
        }

      //  log.info("Se ha creado WalletTransaction: [" + wt + "]");
        return apiWalletServiceProcessorHandler.processWalletTransaction(wt);
    }

    public Boolean subscriptionProcess(CurrencyRequest cr) {
      //  log.info("SubscriptionRequest Rabbit: [" + cr + "]");

        Boolean result = apiWalletServiceProcessorHandler.subscriptionProcess(cr);

        if (result != null && result && cr.localAmount != null && BigDecimal.ZERO.compareTo(cr.localAmount) != 0) {
            result = billingProcess(cr);
        }

        return result;
    }

    Boolean unsubscriptionProcess(CurrencyRequest cr) {
       return apiWalletServiceProcessorHandler.unsubscriptionProcess(cr);
    }



}
