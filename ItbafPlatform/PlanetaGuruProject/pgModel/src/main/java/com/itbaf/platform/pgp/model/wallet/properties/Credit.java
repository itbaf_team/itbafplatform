/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.wallet.properties;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class Credit implements Serializable {

    private static final long serialVersionUID = 20171226170202L;

    private Long clientId;
    private Long sopId;
    /**
     * <LocalCurrency, LocalAmount>
     */
    private Map<String, BigDecimal> values;

    public Credit() {
        values = new HashMap();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("clientId", clientId)
                .add("sopId", sopId)
                .add("values", values)
                .omitNullValues().toString();
    }
}
