/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.cdag.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.extention.cdag.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.extention.cdag.model.recobro.InfotainmentChargeReportEntry;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQRecobrosProcessor implements RabbitMQMessageProcessor {

    private final ObjectMapper mapper;
    private final ServiceProcessorHandler serviceProcessor;

    @Inject
    public RabbitMQRecobrosProcessor(final ObjectMapper mapper,
            final ServiceProcessorHandler serviceProcessor) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.serviceProcessor = Validate.notNull(serviceProcessor, "A ServiceProcessorHandler class must be provided");
    }

    @Override
    public Boolean processMessage(String message) {
        try {
            String tid = Thread.currentThread().getName() + ".rc";
            Thread.currentThread().setName(tid);
        } catch (Exception ex) {
        }

        try {
            InfotainmentChargeReportEntry rv = mapper.readValue(message, InfotainmentChargeReportEntry.class);
            return serviceProcessor.processRecobro(rv);
        } catch (Exception ex) {
            log.error("Error al procesar el Recobro: [" + message + "]. " + ex, ex);
        }
        return null;
    }

}
