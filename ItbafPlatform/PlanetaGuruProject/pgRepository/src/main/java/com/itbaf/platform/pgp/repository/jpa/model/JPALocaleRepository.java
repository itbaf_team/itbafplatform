/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.repository.jpa.model;

import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.pgp.model.Locale;
import com.itbaf.platform.pgp.model.QLocale;
import com.querydsl.jpa.impl.JPAQuery;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPALocaleRepository extends AbstractJPARepositorySession<Locale, Long> {

    @Inject
    public JPALocaleRepository(final Provider<EntityManager> entityManagerProvider) {
        super(Locale.class, entityManagerProvider);
    }

    public Locale getValueByKey(String languageCode, String countryCode, Long providerId, String key) {
        if (key == null || languageCode == null || countryCode == null || providerId == null) {
            String error = "Uno de los parametros es nulo!! languageCode: [" + languageCode + "] - countryCode: [" + countryCode + "] - providerId: [" + providerId + "] - key: [" + key + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        languageCode = languageCode.toLowerCase();
        countryCode = countryCode.toUpperCase();

        QLocale pp = QLocale.locale;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

          Locale pro = query.select(pp).from(pp).
                where(pp.languageCode.eq(languageCode),pp.countryCode.eq(countryCode),
                        pp.providerId.eq(providerId), pp.key.eq(key))
                .fetchFirst();      
         
        return pro;
    }

}
