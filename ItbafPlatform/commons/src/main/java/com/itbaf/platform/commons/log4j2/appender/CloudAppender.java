/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.log4j2.appender;

import com.itbaf.platform.commons.log4j2.appender.cloud.PatternConsoleCloudAppender;
import com.google.cloud.MonitoredResource;
import com.google.cloud.logging.LoggingOptions;
import com.google.cloud.logging.Payload;
import com.google.cloud.logging.Severity;
import com.google.cloud.logging.SourceLocation;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.log4j2.appender.cloud.JsonConsoleCloudAppender;
import com.itbaf.platform.commons.model.ConsoleContext;
import com.itbaf.platform.commons.model.ConsoleLogEntry;
import com.itbaf.platform.commons.model.ConsoleServiceContext;
import com.itbaf.platform.commons.model.FunnelLogMessage;
import com.itbaf.platform.commons.model.LogMessage;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.layout.PatternLayout;

/**
 *
 * @author JF
 */
@Plugin(name = "CloudAppender", category = "Core", printObject = false)
public abstract class CloudAppender extends AbstractAppender {
    
    protected static String providerId = "";
    private static String gcpProjectId;
    private MonitoredResource mr;
    private String appVersion = "na";
    
    protected CloudAppender(String name, Filter filter, Layout<? extends Serializable> layout, LoggingOptions options) {
        super(name, filter, layout);
        setProviderId();
    }
    
    protected CloudAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions, LoggingOptions options) {
        super(name, filter, layout, ignoreExceptions);
        setProviderId();
    }
    
    private static Severity severityFor(Level level) {
        if (level.intLevel() == Level.DEBUG.intLevel()) {
            return Severity.DEBUG;
        } else if (level.intLevel() == Level.INFO.intLevel()) {
            return Severity.INFO;
        } else if (level.intLevel() == Level.WARN.intLevel()) {
            return Severity.WARNING;
        } else if (level.intLevel() == Level.ERROR.intLevel()) {
            return Severity.ERROR;
        } else if (level.intLevel() == Level.FATAL.intLevel()) {
            return Severity.CRITICAL;
        } else {
            return Severity.DEFAULT;
        }
    }
    
    @PluginFactory
    public static CloudAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("target") String target,
            @PluginAttribute("jfAttribute") String jfAttribute) {
        if (name == null) {
            LOGGER.error("No name provided for CloudAppender");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        
        if ("pattern".equals(InstrumentedObject.logType)) {
            return new PatternConsoleCloudAppender(name, filter, layout, false, null);
        } else {
            return new JsonConsoleCloudAppender(name, filter, layout, false, null);
        }
    }
    
    private MonitoredResource getMonitoredResource() {
        if (mr == null) {
            try {
                com.google.cloud.MonitoredResource.Builder b = MonitoredResource.newBuilder("global")
                        .addLabel("project_id", gcpProjectId)
                        .addLabel("app_name", InstrumentedObject.appName)
                        .addLabel("app_version", appVersion)
                        .addLabel("container", InstrumentedObject.container)
                        .addLabel("instance", InstrumentedObject.instance)
                        .addLabel("profile", InstrumentedObject.profile);
                InstrumentedObject.parameterMap.keySet().forEach((key) -> {
                    String value = InstrumentedObject.parameterMap.get(key);
                    if (value != null) {
                        b.addLabel(key, value);
                    }
                });
                mr = b.build();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return mr;
    }
    
    protected final ConsoleLogEntry infoLogEntryFor(LogEvent event) {
        
        ConsoleLogEntry cle = new ConsoleLogEntry();
        final Level level = event.getLevel();
        
        if (event.getMessage() instanceof LogMessage) {
            LogMessage lm = (LogMessage) event.getMessage();
            cle.setHttpRequest(lm.httpRequest);
            try {
                String httpMessageType = (String) lm.parameters.get(CommonFunction.HTTP_MESSAGE_TYPE);
                if (httpMessageType != null) {
                    cle.getLabels().put(CommonFunction.HTTP_MESSAGE_TYPE, httpMessageType);
                    cle.getLabels().put(CommonFunction.HTTP_REQUEST_URL, (String) lm.parameters.get(CommonFunction.HTTP_REQUEST_URL));
                }
            } catch (Exception ex) {
            }
        } else if (event.getMessage() instanceof FunnelLogMessage) {
            FunnelLogMessage lm = (FunnelLogMessage) event.getMessage();
            cle.setContext(lm.parameters.get("context"));
            cle.setMessage("Event tracking");
        }
        cle.setLogName(InstrumentedObject.appName);
        cle.setPayload(Payload.StringPayload.of(event.getMessage().getFormattedMessage()));
        cle.setResource(mr);
        cle.setSeverity(severityFor(level));
        cle.setSourceLocation(SourceLocation.newBuilder()
                .setFile(event.getSource().getClassName())
                .setFunction(event.getSource().getMethodName())
                .setLine(Long.parseLong(String.valueOf(event.getSource().getLineNumber())))
                .build());
        Date d = new Date(event.getTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS'Z'");
        cle.setEventTime(sdf.format(d));
        cle.setTimestamp(event.getTimeMillis());
        cle.getLabels().put("log4j2_level_name", level.name());
        cle.getLabels().put("log4j2_level_value", String.valueOf(level.intLevel()));
        cle.getLabels().put("thread_name", event.getThreadName());
        cle.getLabels().put("thread_id", String.valueOf(event.getThreadId()));
        cle.getLabels().put("thread_priority", String.valueOf(event.getThreadPriority()));
        
        Throwable t = event.getThrown();
        if (t != null) {
            cle.getLabels().put("thrown_message", t.toString());
            StackTraceElement[] ste = t.getStackTrace();
            if (ste != null) {
                String stack = "[";
                for (StackTraceElement st : ste) {
                    stack = stack + st.toString() + ",";
                }
                stack = stack + "end]";
                cle.getLabels().put("thrown_stack_trace", stack);
            }
        }
        
        return cle;
    }
    
    protected final ConsoleLogEntry errorLogEntryFor(LogEvent event) {
        
        ConsoleLogEntry cle = new ConsoleLogEntry();
        final Level level = event.getLevel();
        cle.setSeverity(severityFor(level));
        Date d = new Date(event.getTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS'Z'");
        cle.setEventTime(sdf.format(d));
        cle.setResource(mr);
        cle.setTimestamp(event.getTimeMillis());
        cle.setServiceContext(new ConsoleServiceContext(providerId));
        cle.setMessage(event.getMessage().getFormattedMessage());
        
        Throwable t = event.getThrown();
        try {
            if (t != null && event.getThrownProxy() != null) {
                cle.setMessage(cle.getMessage() + "\n" + event.getThrownProxy().getExtendedStackTraceAsString());
            }
        } catch (Exception ex) {
        }
        
        ConsoleContext cc=new ConsoleContext();
        cle.setContext(cc);
        cc.getReportLocation().setFilePath(event.getSource().getClassName());
        cc.getReportLocation().setFunctionName(event.getSource().getMethodName());
        cc.getReportLocation().setLineNumber(event.getSource().getLineNumber());
        if (event.getMessage() instanceof LogMessage) {
            LogMessage lm = (LogMessage) event.getMessage();
            cc.setHttpRequest(lm.httpRequest);
            try {
                String httpMessageType = (String) lm.parameters.get(CommonFunction.HTTP_MESSAGE_TYPE);
                if (httpMessageType != null) {
                    cle.getLabels().put(CommonFunction.HTTP_MESSAGE_TYPE, httpMessageType);
                    cle.getLabels().put(CommonFunction.HTTP_REQUEST_URL, (String) lm.parameters.get(CommonFunction.HTTP_REQUEST_URL));
                }
            } catch (Exception ex) {
            }
        }
        cle.getLabels().put("log4j2_level_name", level.name());
        cle.getLabels().put("log4j2_level_value", String.valueOf(level.intLevel()));
        cle.getLabels().put("thread_name", event.getThreadName());
        cle.getLabels().put("thread_id", String.valueOf(event.getThreadId()));
        cle.getLabels().put("thread_priority", String.valueOf(event.getThreadPriority()));
        
        return cle;
    }
    
    private void setProviderId() {
        try {
            providerId = "[" + InstrumentedObject.container + "][" + InstrumentedObject.profile + "." + InstrumentedObject.instance + "]";
            gcpProjectId = System.getenv().get("GCP_PROJECTID");
            if (gcpProjectId == null) {
                gcpProjectId = providerId;
            }
            try {
                appVersion = getClass().getPackage().getImplementationVersion();
            } catch (Exception ex) {
            }
            getMonitoredResource();
        } catch (Exception ex) {
        }
    }
}
