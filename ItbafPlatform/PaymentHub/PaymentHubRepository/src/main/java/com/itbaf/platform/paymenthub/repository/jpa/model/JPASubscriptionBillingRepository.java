/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.billing.QSubscriptionBilling;
import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.querydsl.jpa.impl.JPAQuery;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPASubscriptionBillingRepository extends AbstractJPARepositorySession<SubscriptionBilling, Long> {

    @Inject
    public JPASubscriptionBillingRepository(final Provider<EntityManager> entityManagerProvider) {
        super(SubscriptionBilling.class, entityManagerProvider);
    }

    public List<Object> getResume(Long id) {

        String sql = "SELECT SB.SUBSCRIPTION_REGISTRY_ID, SUM(SB.FULL_AMOUNT), SUM(SB.NET_AMOUNT), COUNT(*), MIN(SB.CHARGED_DATE), MAX(SB.CHARGED_DATE), MIN(SB.ID) "
                + "FROM paymentHub.SUBSCRIPTION_BILLING SB "
                + "WHERE SB.SUBSCRIPTION_REGISTRY_ID = ? "
                + "GROUP BY SB.SUBSCRIPTION_REGISTRY_ID ";

        EntityManager em = getEntityManager();
        Query q = em.createNativeQuery(sql);
        q.setParameter(1, id);
        List<Object> result = (List<Object>) q.getResultList();

        return result;
    }

    public List<SubscriptionBilling> getByUserAccount(String userAccount) {
        if (userAccount == null) {
            String error = "Algunos de los atributos son nulos... userAccount: [" + userAccount + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscriptionBilling qSubscriptionBilling = QSubscriptionBilling.subscriptionBilling;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<SubscriptionBilling> list = query.select(qSubscriptionBilling).from(qSubscriptionBilling).
                where(qSubscriptionBilling.subscriptionRegistry().userAccount.eq(userAccount))
                .fetch();

        return list;
    }

    public List<SubscriptionBilling> getByUserAccount(Date time, String userAccount) {
        if (userAccount == null || time == null) {
            String error = "Algunos de los atributos son nulos... userAccount: [" + userAccount + "] - time: [" + time + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QSubscriptionBilling qSubscriptionBilling = QSubscriptionBilling.subscriptionBilling;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<SubscriptionBilling> list = query.select(qSubscriptionBilling).from(qSubscriptionBilling).
                where(ObjectPredicates.combine(qSubscriptionBilling.chargedDate.after(time),
                        qSubscriptionBilling.subscriptionRegistry().userAccount.eq(userAccount)))
                .fetch();

        return list;
    }

}
