
var host = window.location.protocol + "//" + window.location.host;
var canonical = $('link[rel=canonical]')[0].href;
var router = new Navigo(host, false);
!function () {
    router.on({'/account/subscriptions': subscriptionsInfo,
        '/pg/account/subscriptions': subscriptionsInfo
    }).resolve();
}();
function subscriptionsInfo(params, query) {
    var html = tmpl($("#subcriptionsModuleScript").html(), {"params": uparams});
    $("#subscriptions-table").html(html);
    var html = tmpl($("#unsubcriptionsModuleScript").html(), {"params": uparams});
    $("#unsubscriptions-table").html(html);
    $.each(uparams.subscriptions, function (index, value) {
        $("#viewId" + value.subscriptionRegistry.id).data("j", value);
    });

    var dialog = $("#dialogTrans").dialog({
        autoOpen: false,
        width: $(window).width() * 0.9,
        maxWidth: 550,
        modal: true,
        resizable: false,
        draggable: true,
        position: {my: "center", at: "center", of: window},
        buttons: [
            {
                text: tmpl($("#goBackButtonScript").html(), {"params": uparams}),
                class: "go-back-btn",
                click: function () {
                    $(this).dialog("close");
                }
            },
            {
                text: tmpl($("#confirmButtonScript").html(), {"params": uparams}).toUpperCase(),
                class: "confirm-btn",
                click: function (e) {
                    unsubc(e);
                }

            }],
        open: function (event, ui) {
            var mw = $(this).dialog("option", "maxWidth");
            var w = $(window).width() * 0.9;
            if (w > mw) {
                w = mw;
            }

            $(this).dialog("option", "width", w);
        },
        close: function (event, ui) {
            uparams.isOpen = false;
        }
    });
    var height = $(window).height();
    var Width = $(window).width();
    uparams.isOpen = false;
    // responsive width & height
    var resize = function () {
        if ($(window).width() !== Width && uparams.isOpen) {
            Width = $(window).width();
            dialog.dialog("close");
            dialog.dialog("open");
            uparams.isOpen = true;
        }
    };
    // resize on window resize
    $(window).on("resize", function () {
        resize();
    });
    // resize on orientation change
    if (window.addEventListener) {  // Add extra condition because IE8 doesn't support addEventListener (or orientationchange)
        window.addEventListener("orientationchange", function () {
            resize();
        });
    }

    if (uparams.autoCancel === 1) {
        console.log(uparams.autoCancel);
        setTimeout(function () {
            alert(tmpl($("#subscriptionCanceledScript").html(), {"value": "true"}));
        }, 1000);
    }
}

function viewDialog(e, id) {
    e.preventDefault();
    var value = $("#" + id).data("j");
    if (value.object) {
        $("#dialogTrans").html(value.object);
        $(".confirm-btn").css("display", "none");
        uparams.isOpen = true;
        $("#dialogTrans").dialog("open");
    } else {
        var html = tmpl($("#cancelSubscriptionModuleScript").html(), {"value": value});
        $("#dialogTrans").html(html);
        $(".confirm-btn").css("display", "inline-block");
        $(".confirm-btn").data("j", value);
        uparams.isOpen = true;
        $("#dialogTrans").dialog("open");
    }
}


function unsubc(e) {
    e.preventDefault();
    var value = $(".confirm-btn").data("j");
    var params = {};
    params.userId = uparams.userId;
    params.result = value.subscriptionRegistry.id + "";
    $.ajax({
        type: "DELETE",
        url: canonical + "/cancel",
        contentType: 'application/json; charset=UTF-8',
        data: JSON.stringify(params),
        success: function (data) {
            alert(tmpl($("#subscriptionCanceledScript").html(), {"value": value}));
            ajaxStop = false;
            if (data.data.urlRedirect) {
                window.location = data.data.urlRedirect;
            } else {
                location.reload();
            }
        },
        error: function (data) {
            console.log(data);
            alert(tmpl($("#subscriptionCanceledErrorScript").html(), {"value": value}));
            ajaxStop = false;
            location.reload();
        },
        dataType: "json"
    });

}


function normNum(number) {
    return number > 9 ? number : "0" + number;
}

function redirectSubscription(e, ru) {
    e.preventDefault();
    var url = ru;
    if (!ru) {
        url = canonical;
    }
    window.location = url;
}