
package org.csapi.schema.parlayx.payment.v2_1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChargingTransactionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChargingTransactionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nbTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sbTransactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hubTransactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chargingNBTransactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chargingHUBTransactionID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="countryID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aspID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aspName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="localServiceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="uniteServiceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uniteServiceDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contentDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="localProviderID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uniteProviderID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retailPrice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calculatedTax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="downloadFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="calculatedPromo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="downloadFeeWot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="percentPromo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billingSystem" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="sbResultCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sbResultDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hubResultCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hubResultDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aspRevenueShare" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="extraParams" type="{http://www.csapi.org/schema/parlayx/payment/v2_1}Parameters" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChargingTransactionType", propOrder = {
    "nbTransactionID",
    "sbTransactionID",
    "hubTransactionID",
    "chargingNBTransactionID",
    "chargingHUBTransactionID",
    "msisdn",
    "countryID",
    "aspID",
    "aspName",
    "timeStamp",
    "localServiceType",
    "transactionType",
    "uniteServiceType",
    "uniteServiceDescription",
    "contentId",
    "contentDescription",
    "localProviderID",
    "uniteProviderID",
    "retailPrice",
    "calculatedTax",
    "downloadFee",
    "calculatedPromo",
    "downloadFeeWot",
    "percentPromo",
    "currency",
    "billingSystem",
    "sbResultCode",
    "sbResultDescription",
    "hubResultCode",
    "hubResultDescription",
    "aspRevenueShare",
    "extraParams"
})
public class ChargingTransactionType {

    @XmlElement(required = true)
    protected String nbTransactionID;
    protected String sbTransactionID;
    protected String hubTransactionID;
    protected String chargingNBTransactionID;
    protected String chargingHUBTransactionID;
    @XmlElement(required = true)
    protected String msisdn;
    protected String countryID;
    @XmlElement(required = true)
    protected String aspID;
    @XmlElement(required = true)
    protected String aspName;
    @XmlElement(required = true)
    protected String timeStamp;
    protected String localServiceType;
    protected int transactionType;
    protected String uniteServiceType;
    protected String uniteServiceDescription;
    protected String contentId;
    protected String contentDescription;
    protected String localProviderID;
    protected String uniteProviderID;
    protected String retailPrice;
    protected String calculatedTax;
    protected String downloadFee;
    protected String calculatedPromo;
    protected String downloadFeeWot;
    protected String percentPromo;
    protected String currency;
    protected int billingSystem;
    protected String sbResultCode;
    protected String sbResultDescription;
    protected String hubResultCode;
    protected String hubResultDescription;
    protected BigDecimal aspRevenueShare;
    protected Parameters extraParams;

    /**
     * Gets the value of the nbTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNbTransactionID() {
        return nbTransactionID;
    }

    /**
     * Sets the value of the nbTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNbTransactionID(String value) {
        this.nbTransactionID = value;
    }

    /**
     * Gets the value of the sbTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSbTransactionID() {
        return sbTransactionID;
    }

    /**
     * Sets the value of the sbTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSbTransactionID(String value) {
        this.sbTransactionID = value;
    }

    /**
     * Gets the value of the hubTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHubTransactionID() {
        return hubTransactionID;
    }

    /**
     * Sets the value of the hubTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHubTransactionID(String value) {
        this.hubTransactionID = value;
    }

    /**
     * Gets the value of the chargingNBTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargingNBTransactionID() {
        return chargingNBTransactionID;
    }

    /**
     * Sets the value of the chargingNBTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargingNBTransactionID(String value) {
        this.chargingNBTransactionID = value;
    }

    /**
     * Gets the value of the chargingHUBTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargingHUBTransactionID() {
        return chargingHUBTransactionID;
    }

    /**
     * Sets the value of the chargingHUBTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargingHUBTransactionID(String value) {
        this.chargingHUBTransactionID = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the countryID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryID() {
        return countryID;
    }

    /**
     * Sets the value of the countryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryID(String value) {
        this.countryID = value;
    }

    /**
     * Gets the value of the aspID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAspID() {
        return aspID;
    }

    /**
     * Sets the value of the aspID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAspID(String value) {
        this.aspID = value;
    }

    /**
     * Gets the value of the aspName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAspName() {
        return aspName;
    }

    /**
     * Sets the value of the aspName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAspName(String value) {
        this.aspName = value;
    }

    /**
     * Gets the value of the timeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the value of the timeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeStamp(String value) {
        this.timeStamp = value;
    }

    /**
     * Gets the value of the localServiceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalServiceType() {
        return localServiceType;
    }

    /**
     * Sets the value of the localServiceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalServiceType(String value) {
        this.localServiceType = value;
    }

    /**
     * Gets the value of the transactionType property.
     * 
     */
    public int getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     * 
     */
    public void setTransactionType(int value) {
        this.transactionType = value;
    }

    /**
     * Gets the value of the uniteServiceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniteServiceType() {
        return uniteServiceType;
    }

    /**
     * Sets the value of the uniteServiceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniteServiceType(String value) {
        this.uniteServiceType = value;
    }

    /**
     * Gets the value of the uniteServiceDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniteServiceDescription() {
        return uniteServiceDescription;
    }

    /**
     * Sets the value of the uniteServiceDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniteServiceDescription(String value) {
        this.uniteServiceDescription = value;
    }

    /**
     * Gets the value of the contentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * Sets the value of the contentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentId(String value) {
        this.contentId = value;
    }

    /**
     * Gets the value of the contentDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentDescription() {
        return contentDescription;
    }

    /**
     * Sets the value of the contentDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentDescription(String value) {
        this.contentDescription = value;
    }

    /**
     * Gets the value of the localProviderID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalProviderID() {
        return localProviderID;
    }

    /**
     * Sets the value of the localProviderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalProviderID(String value) {
        this.localProviderID = value;
    }

    /**
     * Gets the value of the uniteProviderID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniteProviderID() {
        return uniteProviderID;
    }

    /**
     * Sets the value of the uniteProviderID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniteProviderID(String value) {
        this.uniteProviderID = value;
    }

    /**
     * Gets the value of the retailPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetailPrice() {
        return retailPrice;
    }

    /**
     * Sets the value of the retailPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetailPrice(String value) {
        this.retailPrice = value;
    }

    /**
     * Gets the value of the calculatedTax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculatedTax() {
        return calculatedTax;
    }

    /**
     * Sets the value of the calculatedTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculatedTax(String value) {
        this.calculatedTax = value;
    }

    /**
     * Gets the value of the downloadFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDownloadFee() {
        return downloadFee;
    }

    /**
     * Sets the value of the downloadFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDownloadFee(String value) {
        this.downloadFee = value;
    }

    /**
     * Gets the value of the calculatedPromo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculatedPromo() {
        return calculatedPromo;
    }

    /**
     * Sets the value of the calculatedPromo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculatedPromo(String value) {
        this.calculatedPromo = value;
    }

    /**
     * Gets the value of the downloadFeeWot property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDownloadFeeWot() {
        return downloadFeeWot;
    }

    /**
     * Sets the value of the downloadFeeWot property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDownloadFeeWot(String value) {
        this.downloadFeeWot = value;
    }

    /**
     * Gets the value of the percentPromo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPercentPromo() {
        return percentPromo;
    }

    /**
     * Sets the value of the percentPromo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPercentPromo(String value) {
        this.percentPromo = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the billingSystem property.
     * 
     */
    public int getBillingSystem() {
        return billingSystem;
    }

    /**
     * Sets the value of the billingSystem property.
     * 
     */
    public void setBillingSystem(int value) {
        this.billingSystem = value;
    }

    /**
     * Gets the value of the sbResultCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSbResultCode() {
        return sbResultCode;
    }

    /**
     * Sets the value of the sbResultCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSbResultCode(String value) {
        this.sbResultCode = value;
    }

    /**
     * Gets the value of the sbResultDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSbResultDescription() {
        return sbResultDescription;
    }

    /**
     * Sets the value of the sbResultDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSbResultDescription(String value) {
        this.sbResultDescription = value;
    }

    /**
     * Gets the value of the hubResultCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHubResultCode() {
        return hubResultCode;
    }

    /**
     * Sets the value of the hubResultCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHubResultCode(String value) {
        this.hubResultCode = value;
    }

    /**
     * Gets the value of the hubResultDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHubResultDescription() {
        return hubResultDescription;
    }

    /**
     * Sets the value of the hubResultDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHubResultDescription(String value) {
        this.hubResultDescription = value;
    }

    /**
     * Gets the value of the aspRevenueShare property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAspRevenueShare() {
        return aspRevenueShare;
    }

    /**
     * Sets the value of the aspRevenueShare property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAspRevenueShare(BigDecimal value) {
        this.aspRevenueShare = value;
    }

    /**
     * Gets the value of the extraParams property.
     * 
     * @return
     *     possible object is
     *     {@link Parameters }
     *     
     */
    public Parameters getExtraParams() {
        return extraParams;
    }

    /**
     * Sets the value of the extraParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link Parameters }
     *     
     */
    public void setExtraParams(Parameters value) {
        this.extraParams = value;
    }

}
