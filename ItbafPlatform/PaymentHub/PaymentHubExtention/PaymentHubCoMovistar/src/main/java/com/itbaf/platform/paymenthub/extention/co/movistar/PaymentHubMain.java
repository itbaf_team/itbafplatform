/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.co.movistar;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import static com.itbaf.platform.commons.CommonFunction.BILLER_PROCESS;
import com.itbaf.platform.commons.resources.InstrumentedRunnable;
import com.itbaf.platform.paymenthub.commons.PaymentHubBasic;
import com.itbaf.platform.paymenthub.extention.co.movistar.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.model.Provider;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;
import com.itbaf.platform.paymenthub.extention.co.movistar.consumer.RabbitMQBillerMessageProcessor;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain extends PaymentHubBasic {

    private final ServiceProcessorHandler serviceProcessorHandler;
    private final RabbitMQBillerMessageProcessor billerMessageProcessor;

    @Inject
    public PaymentHubMain(final ServiceProcessorHandler serviceProcessorHandler,
            final RabbitMQBillerMessageProcessor billerMessageProcessor) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
        this.billerMessageProcessor = Validate.notNull(billerMessageProcessor, "A BillerRabbitMQMessageProcessor class must be provided");
    }

    @Override
    protected void startActions() {
        super.startActions();
        Provider p = providerSet.iterator().next();
        try {
            String name = BILLER_PROCESS + p.getName();
            rabbitMQConsumer.createChannel(name, serviceProcessorHandler.getChannelQuantityProvider(p).getSubscriptionConsumerQuantity(), billerMessageProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer NOTIFICATION.RECEIVE para provider: [" + p + "]. " + ex, ex);
        }

        scheduledThreadPool.scheduleWithFixedDelay(new InstrumentedRunnable(".actions.T", p.getName(), instrumentedObject) {
            @Override
            public boolean exec() {
                if (isStop) {
                    return false;
                }
                Thread.currentThread().setName(CommonFunction.getTID(p.getName() + ".actions.T"));
                RLock lock = instrumentedObject.lockObject("CO_MOV", 40);
                long aux = instrumentedObject.getAtomicLong("CO_MOV_A", 1);
                if (aux == 0) {
                    instrumentedObject.getAndIncrementAtomicLong("CO_MOV_A", 1);
                    try {
                        serviceProcessorHandler.getActionRequest();
                        instrumentedObject.unLockObject(lock);
                        return true;
                    } catch (Exception ex) {
                    }
                }
                instrumentedObject.unLockObject(lock);
                return false;
            }
        }, 30, 30, TimeUnit.SECONDS);
    }

}
