/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.rmq.consumer;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQConsumerImpl implements RabbitMQConsumer {

    private final ConnectionFactory connectionFactory;
    private static Connection connection;
    private static final Map<String, ChannelEntity> channels = new HashMap();
    private static ScheduledExecutorService scheduledThreadPool;
    private static boolean controlStatus = true;
    private final RabbitMQProducer rabbitMQProducer;
    private final InstrumentedObject instrumentedObject;

    @Inject
    public RabbitMQConsumerImpl(final ConnectionFactory connectionFactory,
            final ScheduledExecutorService scheduledThreadPool,
            final RabbitMQProducer rabbitMQProducer,
            final InstrumentedObject instrumentedObject) {
        this.connectionFactory = Validate.notNull(connectionFactory, "A ConnectionFactory class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        controlStatus = true;
        createConnection();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.currentThread().setName(CommonFunction.getTID("RabbitStatus"));
                    if (!controlStatus) {
                        return;
                    }

                    if (connection == null || !connection.isOpen()) {
                        try {
                            connection.close();
                        } catch (Exception ex) {
                        }
                        log.warn("Conexion al RabbitMQ cerrada!!");
                        connection = connectionFactory.newConnection();
                    }

                    Set<String> keys = channels.keySet();
                    for (String key : keys) {
                        ChannelEntity ce = channels.get(key);
                        boolean control = false;
                        for (Channel c : ce.channelList) {
                            if (!c.isOpen()) {
                                log.warn("Channel al RabbitMQ cerrada!!");
                                control = true;
                                break;
                            }
                        }
                        if (control) {
                            log.info("Se procede a reiniciar todos los canales de: [" + key + "]");
                            for (Channel c : ce.channelList) {
                                try {
                                    c.close();
                                } catch (Exception ex) {
                                }
                            }
                            createChannel(ce.exchangeName, ce.consumersQuantity, ce.messageProcessor, false, ce.type);
                        }
                    }
                } catch (Exception ex) {
                    log.error("Error al validar status de consumidores al RabbitMQ. " + ex, ex);
                }
            }
        };

        if (RabbitMQConsumerImpl.scheduledThreadPool == null) {
            RabbitMQConsumerImpl.scheduledThreadPool = Validate.notNull(scheduledThreadPool, "A ScheduledExecutorService class must be provided");
            RabbitMQConsumerImpl.scheduledThreadPool.scheduleWithFixedDelay(run, 10, 120, TimeUnit.SECONDS);
        }
    }

    private void createConnection() {
        try {
            if (connection == null || !connection.isOpen()) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
                log.info("Conectando con rabbit: " + connectionFactory.getHost());
                connection = connectionFactory.newConnection();
            }
        } catch (Exception ex) {
            log.error("Error al crear una conexion hacia RabbitMQ. " + ex, ex);
        }
    }

    @Override
    public void createChannel(String exchangeName, Integer consumersQuantity, final RabbitMQMessageProcessor messageProcessor) {
        createChannel(exchangeName, consumersQuantity, messageProcessor, true, "topic");
    }

    @Override
    public void createFanoutChannel(String exchangeName, RabbitMQMessageProcessor messageProcessor) {
        if (!exchangeName.startsWith("fanout.")) {
            //Importante, deben comenzar asi para diferenciarlos en RabbitMQProducerImpl.class
            exchangeName = "fanout." + exchangeName;
        }
        createChannel(exchangeName, 1, messageProcessor, true, "fanout");
    }

    private void createChannel(final String exchangeName, Integer consumersQuantity, final RabbitMQMessageProcessor messageProcessor, boolean ignore, String type) {

        createConnection();
        if (channels.containsKey(exchangeName) && ignore) {
            log.error("Ya existe un consumidor para el channel con nombre: [" + exchangeName + "]");
            return;
        }
        ChannelEntity channelEntity = new ChannelEntity();
        channelEntity.consumersQuantity = consumersQuantity;
        channelEntity.exchangeName = exchangeName;
        channelEntity.messageProcessor = messageProcessor;
        channelEntity.type = type;
        channels.put(exchangeName, channelEntity);

        for (int i = 0; i < consumersQuantity; i++) {
            try {
                Channel channel = connection.createChannel();
                Consumer consumer = null;
                String queueName = null;
                switch (type) {
                    case "topic":
                        queueName = exchangeName;
                        channel.basicQos(5);
                        channel.queueDeclare(exchangeName, true, false, false, null);
                        channel.exchangeDeclare(exchangeName, type, true, false, null);
                        channel.queueBind(exchangeName, exchangeName, exchangeName);
                        consumer = new DefaultConsumer(channel) {
                            @Override
                            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                                    throws IOException {

                                String routing = envelope.getRoutingKey();
                                String message = new String(body, "UTF-8");
                                try {
                                    String tid = CommonFunction.getTID(routing);
                                    Thread.currentThread().setName(tid);
                                } catch (Exception ex) {
                                }

                                log.info("Mensaje del Rabbit: ["+ exchangeName +"] [" + message + "] ");
                                Boolean processed = null;
                                try {
                                    processed = messageProcessor.processMessage(message);
                                } catch (Exception ex) {
                                    log.fatal("Error al procesar mensaje: [" + message + "]. " + ex, ex);
                                }

                                long dt = envelope.getDeliveryTag();
                                if (processed == null) {
                                    //Por alguna razon no se proceso el msg. Se reencola
                                    try {
                                        if (instrumentedObject.getAtomicLong(message + "_rabbit", 45) > 3l) {
                                            log.fatal("Rabbit ACK. NULL. [" + dt + "] [" + routing + "] [" + message + "]");
                                            rabbitMQProducer.messageSender("error.process.notification." + routing, message);
                                            channel.basicNack(envelope.getDeliveryTag(), false, false);
                                        } else {
                                            log.error("Rabbit ACK. NULL. [" + instrumentedObject.getAndIncrementAtomicLong(message + "_rabbit", 45) + "] [" + dt + "] [" + routing + "] [" + message + "]");
                                            channel.basicReject(dt, true);
                                        }
                                    } catch (Exception ex) {
                                        log.fatal("Rabbit ACK. NULL. [" + dt + "] [" + message + "]. " + ex, ex);
                                        channel.basicReject(dt, true);
                                    }
                                } else if (processed) {
                                    //Mensaje procesado satisfactoriamente
                                    channel.basicAck(envelope.getDeliveryTag(), false);
                                } else {
                                    //channel.basicNack(envelope.getDeliveryTag(), false, false);//Mensaje procesado y descartable
                                    log.info("Rabbit ACK. False. [" + dt + "] [" + message + "]");
                                    channel.basicAck(envelope.getDeliveryTag(), false);//JF- Que responda OK
                                }
                            }
                        };
                        channel.basicConsume(exchangeName, false, consumer);
                        break;
                    case "fanout":
                        /* channel.queueDeclare(exchangeName, true, false, false, null);
                        channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT);
                        channel.queueBind(exchangeName, exchangeName, "");*/
                        channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT);
                        queueName = channel.queueDeclare().getQueue();
                        channel.queueBind(queueName, exchangeName, "");
                        consumer = new DefaultConsumer(channel) {
                            @Override
                            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                                String routing = envelope.getExchange();
                                String message = new String(body, "UTF-8");
                                try {
                                    String tid = CommonFunction.getTID(routing);
                                    Thread.currentThread().setName(tid);
                                } catch (Exception ex) {
                                }

                                log.info("Mensaje del Rabbit: ["+ exchangeName +"] [" + message + "] ");
                                try {
                                    messageProcessor.processMessage(message);
                                } catch (Exception ex) {
                                    log.fatal("Error al procesar mensaje: [" + message + "]. " + ex, ex);
                                }
                            }
                        };
                        channel.basicConsume(queueName, true, consumer);
                        queueName = queueName + "][" + exchangeName;
                        break;
                }
                channelEntity.channelList.add(channel);
                log.info("Consumer: [" + queueName + "] - Channel [" + channel.getChannelNumber() + "]");
            } catch (Exception ex) {
                log.error("Error al crear un channel. " + ex, ex);
            }
        }
    }

    @Override
    public void stopConsumers() {
        log.info("Deteniendo consumers RabbitMQ...");
        controlStatus = false;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
        }
        try {
            connection.close(1000);
        } catch (Exception ex) {
        }

        channels.clear();
        log.info("Detenidos consumers RabbitMQ...");
    }

    private class ChannelEntity {

        public String exchangeName;
        public String type;
        public int consumersQuantity;
        public RabbitMQMessageProcessor messageProcessor;
        public List<Channel> channelList = new ArrayList();

    }
}
