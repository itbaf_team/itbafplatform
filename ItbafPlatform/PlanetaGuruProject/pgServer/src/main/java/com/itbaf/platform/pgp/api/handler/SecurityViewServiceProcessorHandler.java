/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.handler;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.auth.UserRecord;
import com.google.inject.Inject;
import com.itbaf.platform.commons.EmailUtility;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.commons.resources.ClientCache;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.UsernameAttribute;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class SecurityViewServiceProcessorHandler extends ServiceProcessorHandler {

    private final ClientCache clientCache;
    private final EmailUtility emailUtility;
    private final PHProfilePropertiesService phprofilePropertiesService;
    private final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler;

    @Inject
    public SecurityViewServiceProcessorHandler(final ClientCache clientCache,
            final EmailUtility emailUtility,
            final PHProfilePropertiesService phprofilePropertiesService,
            final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler) {
        this.clientCache = Validate.notNull(clientCache, "A ClientCache class must be provided");
        this.emailUtility = Validate.notNull(emailUtility, "An EmailUtility class must be provided");
        this.phprofilePropertiesService = Validate.notNull(phprofilePropertiesService, "A PHProfilePropertiesService class must be provided");
        this.apiProfileServiceProcessorHandler = Validate.notNull(apiProfileServiceProcessorHandler, "An ApiProfileServiceProcessorHandler class must be provided");
    }

    public String getUserSecurityHtml(User client, User user, String productId, String redirectUri) throws Exception {
        String html = readResource("/tpl/html/account/security.html", client, productId);
        html = html.replace("<siteHeaderScript/>", readHeaderResource("/tpl/html/header.html", client, user, productId));
        html = html.replace("<siteFooterScript/>", readResource("/tpl/html/footer.html", client, productId));

        UserProfile up = apiProfileServiceProcessorHandler.getUserProfileFromUser(client, user, null, false, false, false);

        if (up.getPhones() != null && !up.getPhones().isEmpty()) {
            Set<Msisdn> aux = new HashSet();
            for (Msisdn m : up.getPhones()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setPhones(aux);
        }
        if (up.getEmails() != null && !up.getEmails().isEmpty()) {
            Set<Email> aux = new HashSet();
            for (Email m : up.getEmails()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setEmails(aux);
        }

        //Lista de productos del menu
        html = getMenuProducts(html, productId, redirectUri);

        html = html.replace("uparams = {}", "uparams = " + mapper.writeValueAsString(up));
        return html;
    }

    public String getPasswordUrl(User client, User user, String redirectUriClient, String productId) {
        try {
            while (client.getPrincipal() != null) {
                client = client.getPrincipal();
            }

            String oauthUrl = genericPropertyService.getValueByKey("oauth.url") + "/auth/authorize/password?response_type=code&client_id="
                    + client.getCredentialUserProperty().getClient().getClientId() + "&redirect_uri=" + URLEncoder.encode(redirectUriClient, "UTF-8")
                    + "&state=PG_" + System.currentTimeMillis() + "&product_id=" + productId + "&locale=" + user.getProfileUserProperty().getLanguage()
                    + "&redirect_pg=true";
            return oauthUrl;
        } catch (Exception ex) {
            log.error("Error al obtener URL para modificar contraseña. " + ex, ex);
        }
        return null;
    }

    public String getUsernamePage(User client, User user, String productId, String redirectUri) throws Exception {
        String html = readResource("/tpl/html/account/security.html", client, productId);
        html = html.replace("<siteHeaderScript/>", readHeaderResource("/tpl/html/header.html", client, user, productId));
        html = html.replace("<siteFooterScript/>", readResource("/tpl/html/footer.html", client, productId));

        html = html.replace("<captcha></captcha>", genericPropertyService.getValueByKey("google.recaptcha.key.public"));

        UserProfile up = apiProfileServiceProcessorHandler.getUserProfileFromUser(client, user, null, false, false, false);
        List<Msisdn> auxP = new ArrayList();
        if (up.getPhones() != null && !up.getPhones().isEmpty()) {
            for (Msisdn m : up.getPhones()) {
                if (m.getPrimary()) {
                    auxP.add(m);
                    break;
                }
            }
        }
        List<Email> auxE = new ArrayList();
        if (up.getEmails() != null && !up.getEmails().isEmpty()) {
            for (Email m : up.getEmails()) {
                if (m.getPrimary()) {
                    auxE.add(m);
                    break;
                }
            }
        }

        if (!auxE.isEmpty() && !auxP.isEmpty()) {
            try {
                auxP.get(0).setPrimary(false);
                userService.saveOrUpdate(user);
            } catch (Exception ex) {
            }
        }

        //Lista de productos del menu
        html = getMenuProducts(html, productId, redirectUri);

        //Lista de codigos de paises activos
        List<Country> activeCountries = countryService.getByStatus(Status.ENABLE);
        if (!activeCountries.isEmpty()) {
            up.setCodes(new String[activeCountries.size()]);
            int i = 0;
            for (Country c : activeCountries) {
                up.getCodes()[i] = c.getCode().toLowerCase();
                i++;
            }
        }

        html = html.replace("uparams = {}", "uparams = " + mapper.writeValueAsString(up));
        return html;
    }

    public boolean makePrimaryUsername(User user, String username) {
        Email e = user.getCredentialUserProperty().getUsername().getEmail(username);
        Msisdn m = user.getCredentialUserProperty().getUsername().getMsisdn(username);
        UsernameAttribute una = null;
        if (e != null && e.getConfirmed() && !e.getPrimary()) {
            una = e;
        } else if (m != null && m.getConfirmed() && !m.getPrimary()) {
            una = m;
        }

        if (una != null) {
            for (Email x : user.getCredentialUserProperty().getUsername().getEmails().values()) {
                x.setPrimary(false);
            }
            for (Msisdn x : user.getCredentialUserProperty().getUsername().getMsisdns().values()) {
                x.setPrimary(false);
            }

            try {
                una.setPrimary(Boolean.TRUE);
                userService.saveOrUpdate(user);
                return true;
            } catch (Exception ex) {
            }
        }

        return false;
    }

    public boolean removeUsername(User user, String username) {
        Email e = user.getCredentialUserProperty().getUsername().getEmail(username);
        Msisdn m = user.getCredentialUserProperty().getUsername().getMsisdn(username);
        UsernameAttribute una = null;
        if (e != null) {
            una = e;
            user.getCredentialUserProperty().getUsername().getEmails().remove(e.getEmail());
            user.getCredentialUserProperty().getUsername().getOldEmails().put(System.currentTimeMillis() + "|" + e.getEmail(), e);
        } else if (m != null) {
            una = m;
            user.getCredentialUserProperty().getUsername().getMsisdns().remove(m.getMsisdn());
            user.getCredentialUserProperty().getUsername().getOldMsisdns().put(System.currentTimeMillis() + "|" + m.getMsisdn(), m);
        }

        if (una != null) {
            try {
                userService.saveOrUpdate(user);
                return true;
            } catch (Exception ex) {
            }
        }

        return false;
    }

    public boolean addUsernameEmail(User user, String email) {
        email = filterCaractersToLowerCase(email);
        if (email == null || email.length() < 4 || !email.contains("@")) {
            log.fatal("Invalido Formato de Email: [" + email + "]");
            return false;
        }

        User userAux = userService.findByEmail(email);
        if (userAux != null) {
            log.warn("Ya existe un usuario con el email: [" + email + "]");
            return false;
        }

        user.getCredentialUserProperty().getUsername().setEmail(email, false);

        try {
            userService.saveOrUpdate(user);
            return true;
        } catch (Exception ex) {
        }

        return false;
    }

    public boolean addUsernameMsisdn(User user, String msisdn, String countryCode) {
        msisdn = msisdn.trim();
        countryCode = countryCode.trim().toUpperCase();

        if (msisdn != null && countryCode != null) {
            Country c = countryService.findByCode(countryCode);
            String localLength = phprofilePropertiesService.getCommonProperty("msisdn." + c.getCode().toLowerCase() + ".length.local");

            if (localLength == null) {
                log.error("No existe en DB la configuracion: [msisdn." + c.getCode().toLowerCase() + ".length.local]");
            } else {
                Integer msisdnLengthLocal = Integer.parseInt(localLength);
                msisdn = msisdn.replace(" ", "").replace("+", "");

                if (msisdn.startsWith(c.getPhoneCode().toString())) {
                    msisdn = msisdn.substring(c.getPhoneCode().toString().length(), msisdn.length());
                }

                msisdn = msisdn.substring(Math.max(0, msisdn.length() - msisdnLengthLocal), msisdn.length());

                if (msisdn.length() < msisdnLengthLocal) {
                   // log.info("Invalido Formato del MSISDN: [" + msisdn + "]");
                    return false;
                } else {
                    Holder<String> username = new Holder();
                    User userAux = userService.findByMsisdn(c.getPhoneCode().toString(), msisdn, username);
                    if (userAux != null) {
                        log.info("Ya existe un usuario con el MSISDN: [" + c.getPhoneCode().toString() + msisdn + "]");
                        return false;
                    }
                }

                try {
                    user.getCredentialUserProperty().getUsername().setMsisdn(c.getPhoneCode().toString() + msisdn, false, c, null);
                    userService.saveOrUpdate(user);
                    return true;
                } catch (Exception ex) {
                }
            }
        }

        return false;
    }

    public boolean sendEmail(User client, User user, String email, String productId) {
        Email e = user.getCredentialUserProperty().getUsername().getEmail(email);

        if (e == null || e.getConfirmed()) {
            return false;
        }

        return sendEmailVerifyCode(client, user, email, productId);
    }

    public boolean verifyCodeEmail(User user, String email, String ucode) {
        Email e = user.getCredentialUserProperty().getUsername().getEmail(email);
        ucode = ucode.trim().toUpperCase();
        if (e == null || e.getConfirmed() || ucode.length() != 6) {
            return false;
        }

        String code = clientCache.getTemporalData(email + "_ECODE_VERIFY");
        clientCache.getInstrumentedObject().getAndIncrementAtomicLong(email + "_e_attempts", 15);
        if (code != null && !"xx".equals(code) && ucode.equals(code)) {
            clientCache.setTemporalData(email + "_ECODE_VERIFY", "xx", 40);
            if (clientCache.getInstrumentedObject().getAtomicLong(email + "_e_attempts", 15) <= 5l) {
                e.setConfirmed(Boolean.TRUE);
                try {
                    userService.saveOrUpdate(user);
                    return true;
                } catch (Exception ex) {
                }
            }
        }

        return false;
    }

    /**
     * Codigo para validar propiedad de email
     */
    private boolean sendEmailVerifyCode(User client, User user, String email, String productId) {
        String code = clientCache.getTemporalData(email + "_ECODE_VERIFY");
        if (code == null || "xx".equals(code)) {
            code = generateDigitCode(6).toUpperCase();
           // log.info("ValidationCode: [" + code + "] - User: [" + user.getId() + "]");
            try {
                String html = readResource("/tpl/email/verifyEmailCode.html", client, productId);
                html = html.replace("<scode></scode>", code);
                String name = "User".equals(user.getProfileUserProperty().getFirstName()) ? "" : user.getProfileUserProperty().getFirstName();
                html = html.replace("<sname></sname>", name);
                html = localeService.i18n(user.getProfileUserProperty().getLanguage(), 1L, html);
                emailUtility.sendHTMLEmail("no-reply@planeta.guru", email,
                        localeService.i18n(user.getProfileUserProperty().getLanguage(), 1L, "<i18n>i18n.ix.px.email.verify.text.pvye</i18n>"), html);
                clientCache.setTemporalData(email + "_ECODE_VERIFY", code, 40);
                return true;
            } catch (Exception ex) {
                log.error("Error al enviar EMAIL. " + ex, ex);
            }
            return false;
        } else {
          //  log.info("ValidationCode: [" + code + "] - User: [" + user.getId() + "]");
            return true;
        }
    }

    public boolean verifyCodeMsisdn(User user, String msisdn, String tokenCode) {

        Msisdn m = user.getCredentialUserProperty().getUsername().getMsisdn(msisdn);

        if (m == null || m.getConfirmed() || tokenCode.length() == 0) {
            return false;
        }

        try {
            FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdTokenAsync(tokenCode).get();
            String uid = decodedToken.getUid();
            log.info("Google UserId: [" + uid + "]");
            UserRecord ur = FirebaseAuth.getInstance().getUserAsync(uid).get();
            String msisdnAux = ur.getPhoneNumber().replace("+", "");
            if (m.getMsisdn().equals(msisdnAux)) {
              //  log.info("Encontrado user en Google. [" + ur.getUid() + "]");
                m.setConfirmed(Boolean.TRUE);
                try {
                    userService.saveOrUpdate(user);
                    return true;
                } catch (Exception ex) {
                }
            } else {
                log.fatal("No coincide el MSISDN: [" + msisdnAux + "] con el MSISDN: [" + m.getMsisdn() + "]");
            }
        } catch (Exception ex) {
        }

        return false;
    }
}
