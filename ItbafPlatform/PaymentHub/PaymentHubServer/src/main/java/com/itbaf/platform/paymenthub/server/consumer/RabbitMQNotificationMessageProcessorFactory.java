/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.server.consumer;

import com.itbaf.platform.paymenthub.server.notification.NotificationProcessor;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessorFactory;

/**
 *
 * @author javier
 * @param <M>
 */
public interface RabbitMQNotificationMessageProcessorFactory extends RabbitMQMessageProcessorFactory <NotificationProcessor>{

}
