/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.bn.model;

import com.google.common.base.MoreObjects;
import java.util.Objects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class SpiralisNotification extends BNNotification {

    private String id;
    private String msisdn;
    private String service;
    private String created;
    private String type;
    private String status;
    private String channelIn;
    private String channelOut;
    private String keywordIn;
    private String keywordOut;
    private String category;
    private String fee;
    private String adProvider;
    private String originalMsisdn;
    private String countryCode;
    public String thread = Thread.currentThread().getName();

    public SpiralisNotification() {
    }

    public SpiralisNotification(String id, String msisdn, String service,
            String created, String type, String status, String channelIn,
            String channelOut, String keywordIn, String keywordOut,
            String category, String fee, String adProvider) {
        this.id = id;
        this.msisdn = msisdn;
        this.service = service;
        this.created = created;
        this.type = type;
        this.status = status;
        this.channelIn = channelIn;
        this.channelOut = channelOut;
        this.keywordIn = keywordIn;
        this.keywordOut = keywordOut;
        this.category = category;
        this.fee = fee;
        this.adProvider = adProvider;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("msisdn", msisdn)
                .add("originalMsisdn", originalMsisdn)
                .add("service", service)
                .add("created", created)
                .add("type", type)
                .add("status", status)
                .add("channelIn", channelIn)
                .add("channelOut", channelOut)
                .add("keywordIn", keywordIn)
                .add("keywordOut", keywordOut)
                .add("category", category)
                .add("fee", fee)
                .add("adProvider", adProvider)
                .add("countryCode", countryCode)
                .add("thread", thread)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, msisdn, service, created, type,
                status, channelIn, channelOut, keywordIn, keywordOut,
                category, fee, adProvider);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SpiralisNotification other = (SpiralisNotification) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.msisdn, other.msisdn)) {
            return false;
        }
        if (!Objects.equals(this.service, other.service)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.status, other.status)) {
            return false;
        }
        if (!Objects.equals(this.channelIn, other.channelIn)) {
            return false;
        }
        if (!Objects.equals(this.channelOut, other.channelOut)) {
            return false;
        }
        if (!Objects.equals(this.keywordIn, other.keywordIn)) {
            return false;
        }
        if (!Objects.equals(this.keywordOut, other.keywordOut)) {
            return false;
        }
        if (!Objects.equals(this.category, other.category)) {
            return false;
        }
        if (!Objects.equals(this.fee, other.fee)) {
            return false;
        }
        if (!Objects.equals(this.adProvider, other.adProvider)) {
            return false;
        }
        return true;
    }

}
