
package com.itbaf.platform.paymenthub.messaging.smpp.smppadapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="destinationAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="originationAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="deliveryReceipt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="receiptedMessageId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="messageState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "destinationAddr",
    "originationAddr",
    "deliveryReceipt",
    "receiptedMessageId",
    "messageState"
})
@XmlRootElement(name = "RcvDRRequest")
public class RcvDRRequest {

    protected long id;
    @XmlElement(required = true)
    protected String destinationAddr;
    @XmlElement(required = true)
    protected String originationAddr;
    @XmlElement(required = true)
    protected String deliveryReceipt;
    protected String receiptedMessageId;
    protected String messageState;

    /**
     * Gets the value of the id property.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Gets the value of the destinationAddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationAddr() {
        return destinationAddr;
    }

    /**
     * Sets the value of the destinationAddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationAddr(String value) {
        this.destinationAddr = value;
    }

    /**
     * Gets the value of the originationAddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginationAddr() {
        return originationAddr;
    }

    /**
     * Sets the value of the originationAddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginationAddr(String value) {
        this.originationAddr = value;
    }

    /**
     * Gets the value of the deliveryReceipt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryReceipt() {
        return deliveryReceipt;
    }

    /**
     * Sets the value of the deliveryReceipt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryReceipt(String value) {
        this.deliveryReceipt = value;
    }

    /**
     * Gets the value of the receiptedMessageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptedMessageId() {
        return receiptedMessageId;
    }

    /**
     * Sets the value of the receiptedMessageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptedMessageId(String value) {
        this.receiptedMessageId = value;
    }

    /**
     * Gets the value of the messageState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageState() {
        return messageState;
    }

    /**
     * Sets the value of the messageState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageState(String value) {
        this.messageState = value;
    }

}
