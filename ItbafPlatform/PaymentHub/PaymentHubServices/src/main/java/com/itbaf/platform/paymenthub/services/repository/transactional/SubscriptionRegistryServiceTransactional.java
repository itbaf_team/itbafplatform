/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPASubscriptionRegistryRepository;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.paymenthub.services.stats.StatsService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class SubscriptionRegistryServiceTransactional implements SubscriptionRegistryService {

    private final StatsService statsService;
    private final JPASubscriptionRegistryRepository subscriptionRegistryRepository;

    @Inject
    public SubscriptionRegistryServiceTransactional(final StatsService statsService,
            final JPASubscriptionRegistryRepository subscriptionRegistryRepository) {
        this.statsService = Validate.notNull(statsService, "A StatsService class must be provided");
        this.subscriptionRegistryRepository = Validate.notNull(subscriptionRegistryRepository, "A JPASubscriptionRegistryRepository class must be provided");
    }

    @Override
    public void saveOrUpdate(SubscriptionRegistry sr, boolean event) throws Exception {
        if (sr.getId() == null) {
            if (sr.getSubscription() != null) {
                if (sr.getExtra() == null) {
                    sr.setExtra(new Extra());
                }
                if (sr.getExtra().firstStatus == null) {
                    sr.getExtra().firstStatus = sr.getSubscription().getStatus();
                }
                if (sr.getExtra().statusChanged == null) {
                    sr.getExtra().statusChanged = new HashMap();
                }
                if (sr.getExtra().statusChanged.get(sr.getSubscription().getStatus()) == null) {
                    sr.getExtra().statusChanged.put(sr.getSubscription().getStatus(), new Date());
                }
            }
            subscriptionRegistryRepository.save(sr);
        } else {
            if (sr.getSubscription() != null && sr.getSubscription().getSubscriptionRegistry() != null
                    && sr.getId().equals(sr.getSubscription().getSubscriptionRegistry().getId())) {
                if (sr.getExtra() == null) {
                    sr.setExtra(new Extra());
                }
                if (sr.getExtra().statusChanged == null) {
                    sr.getExtra().statusChanged = new HashMap();
                }
                if (sr.getExtra().statusChanged.get(sr.getSubscription().getStatus()) == null) {
                    sr.getExtra().statusChanged.put(sr.getSubscription().getStatus(), new Date());
                }
            }
            subscriptionRegistryRepository.merge(sr);
        }
        try {
            if (event) {
                statsService.sendToRabbit(ElasticData.ElasticType.subscription_registry, sr.getId().toString());
            }
        } catch (Exception ex) {
            log.error("No fue posible enviar a stats-rabbit: [" + ElasticData.ElasticType.subscription_registry + "]. [" + sr + "]. " + ex, ex);
        }
    }

    @Override
    public void saveOrUpdate(SubscriptionRegistry sr) throws Exception {
        saveOrUpdate(sr, true);
    }

    @Override
    public SubscriptionRegistry findByExternalId(String externalId) {
        return subscriptionRegistryRepository.findByExternalId(externalId);
    }

    @Override
    public List<SubscriptionRegistry> getOrphanRegisters() {
        return subscriptionRegistryRepository.getOrphanRegisters();
    }

    @Override
    public SubscriptionRegistry findById(Long id) throws Exception {
        return subscriptionRegistryRepository.findById(id);
    }

    @Override
    public List<SubscriptionRegistry> getRegistersBySubscription(Long subscriptionId) {
        return subscriptionRegistryRepository.getRegistersBySubscription(subscriptionId);
    }

    @Override
    public SubscriptionRegistry getLastRegisterBySubscription(Long subscriptionId, boolean isPenult) {
        return subscriptionRegistryRepository.getLastRegisterBySubscription(subscriptionId, isPenult);
    }
    
}
