/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.co.movistar.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.paymenthub.extention.co.movistar.model.RequestResponse;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.extention.co.movistar.model.Subscription.Action;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.co.movistar.resources.PVSResources;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import java.util.Set;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final Provider provider;
    private final PVSResources resources;

    @Inject
    public ServiceProcessorHandler(final Set<Provider> providerSet,
            final PVSResources resources) {
        this.resources = Validate.notNull(resources, "A PVSResources class must be provided");
        this.provider = Validate.notNull(providerSet, "A Set<Provider> Provider class must be provided").iterator().next();
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws JsonProcessingException {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        try {
            Holder<String> msg = new Holder();
            rm.status = resources.vaspConsSubsReq(subscription.getUserAccount(), subscription.getSop(), msg);
            rm.message = msg.value;
        } catch (Exception ex) {
            rm.message = ("Error getSubscriptionStatus. [" + subscription.getUserAccount() + "]. " + ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.error(rm.message, ex);
        }
        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage sendPin(Subscription subscription) throws JsonProcessingException {
        ResponseMessage rm = new ResponseMessage();

        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        String channelIn = null;
        if (subscription.getFacadeRequest() != null) {
            channelIn = subscription.getFacadeRequest().channel;
        }
        try {
            Holder<String> msg = new Holder();
            rm.status = resources.vaspAddSubsReq(subscription.getUserAccount(), subscription.getSop(), channelIn, msg);
            rm.message = msg.value;
        } catch (Exception ex) {
            rm.message = ("Error sendPin. [" + subscription.getUserAccount() + "]. " + ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.error(rm.message, ex);
        }
        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage validatePin(Subscription subscription, String pin) throws JsonProcessingException {
        ResponseMessage rm = new ResponseMessage();

        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        try {
            Holder<String> msg = new Holder();
            rm.status = resources.vaspConfSubsReq(subscription.getUserAccount(), subscription.getSop(), pin, msg);
            rm.message = msg.value;
        } catch (Exception ex) {
            rm.message = ("Error validatePin. [" + subscription.getUserAccount() + "]. " + ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.error(rm.message, ex);
        }
        rm.data = mapper.writeValueAsString(subscription);

        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws JsonProcessingException {
        ResponseMessage rm = new ResponseMessage();

        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        try {
            Holder<String> msg = new Holder();
            rm.status = resources.vaspDelSubsReq(subscription.getUserAccount(), subscription.getSop(), msg);
            rm.message = msg.value;
        } catch (Exception ex) {
            rm.message = ("Error unsubscribe. [" + subscription.getUserAccount() + "]. " + ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.error(rm.message, ex);
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public void getActionRequest() {
        if (true) {
            return;
        }
        try {
            RequestResponse rr = resources.vaspGetActionsReq();
            if (rr != null) {
                try {
                    if (0L == (rr.getParams().getResultCode())) {
                        for (com.itbaf.platform.paymenthub.extention.co.movistar.model.Subscription s : rr.getParams().getSubscriptions()) {
                            String jsonResponse;
                            try {
                                s.setTransactionId(rr.getParams().getTransactionId());
                                jsonResponse = mapper.writeValueAsString(s);
                                if (jsonResponse != null) {
                                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + provider.getName(), jsonResponse);
                                }
                            } catch (Exception ex) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException ex1) {
                                }
                                try {
                                    jsonResponse = mapper.writeValueAsString(s);
                                    if (jsonResponse != null) {
                                        rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + provider.getName(), jsonResponse);
                                    }
                                } catch (Exception ex1) {
                                    log.error("No fue posible enviar al Rabbit: [" + s + "]. " + ex, ex);
                                }
                            }
                        }
                        resources.vaspConfActionsReq(rr.getParams().getTransactionId());
                    } else if (1150L == (rr.getParams().getResultCode())) {
                        log.info("No hay movimientos de Altas/Bajas");
                    } else {
                        log.warn("Error en el request del servidor: [" + rr.getParams().getDesc() + "]");
                    }
                } catch (Exception ex) {
                    log.error("Error al procesar RequestResponse [" + rr + "]. " + ex, ex);
                }
            }
        } catch (Exception ex) {
            log.error("Error al obtener ultimos movimientos. " + ex, ex);
        }
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    @Override
    public void syncSubscriptions(Provider provider) {
    }

    @Override
    public Boolean processMessage(String message) {
        Boolean control = null;

        try {
            com.itbaf.platform.paymenthub.extention.co.movistar.model.Subscription s = mapper.readValue(message, com.itbaf.platform.paymenthub.extention.co.movistar.model.Subscription.class);
            Thread.currentThread().setName(s.getThread() + "." + s.getSubscriptionId() + "." + s.getMsisdn());
            SOP sop = sopService.findWithTariffBySettings(provider.getName(), "id_suscripcion", s.getSubscriptionId().toString());
            if (sop == null) {
                throw new Exception("No existe un SOP para: provider: [" + provider + "] - key: [id_suscripcion] - value: [" + s.getSubscriptionId().toString() + "]");
            }

            Tariff t = sop.getMainTariff();

            PaymentHubMessage n = new PaymentHubMessage();
            n.setCodeCountry("CO");
            n.setTariffId(t.getId());
            n.setCurrencyId(t.getCurrency());
            n.setFrecuency(t.getFrecuency());
            n.setFrequencyType(t.getFrecuencyType());
            n.setFullAmount(t.getFullAmount());
            n.setNetAmount(t.getNetAmount());
            if (Action.ALTA.equals(s.getAction())) {
                n.setMessageType(MessageType.SUBSCRIPTION_ACTIVE);
                n.setTransactionId(s.getTransactionId() + "." + s.getSubscriptionId() + "." + s.getMsisdn());
                //Esto hay que ver q sea una fecha y hora superior a la existente en db... 
                //de ultima hacer un new Date();
                n.setFromDate(s.getDatetime());
                n.setChannelIn(s.getMedia());
                n.setBiller(true);
            } else if (Action.BAJA.equals(s.getAction()) || Action.BAJAATT.equals(s.getAction())) {
                n.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
                n.setToDate(s.getDatetime());
                n.setChannelOut(s.getMedia());
            } else {
                throw new Exception("No existe un Action conocido para: [" + s.getAction() + "]");
            }
            n.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            n.setSopId(sop.getId());
            n.setUserAccount(s.getMsisdn());
            n.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            String jsonResponse = mapper.writeValueAsString(n);
            if (jsonResponse != null) {
                control = rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar suscripcionMovistarCo: [" + message + "]. " + ex, ex);
        }
        return control;
    }
}
