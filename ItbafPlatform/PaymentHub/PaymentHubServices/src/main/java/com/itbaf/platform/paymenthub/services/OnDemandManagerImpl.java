/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.OnDemandBilling;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.services.repository.OnDemandBillingService;
import com.itbaf.platform.paymenthub.services.repository.OnDemandRegistryService;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class OnDemandManagerImpl implements OnDemandManager {

    private final ObjectMapper mapper;
    private final RequestClient requestClient;
    private final InstrumentedObject instrumentedObject;
    private final OnDemandBillingService onDemandBillingService;
    private final OnDemandRegistryService onDemandRegistryService;

    @Inject
    public OnDemandManagerImpl(final ObjectMapper mapper,
            final RequestClient requestClient,
            final InstrumentedObject instrumentedObject,
            final OnDemandBillingService onDemandBillingService,
            final OnDemandRegistryService onDemandRegistryService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.onDemandBillingService = Validate.notNull(onDemandBillingService, "An OnDemandBillingService class must be provided");
        this.onDemandRegistryService = Validate.notNull(onDemandRegistryService, "An OnDemandRegistryService class must be provided");
    }

    @Override
    public void ondemandRegistryProcess(OnDemandRegistry or) throws Exception {
        if (or.getStatusMessage() == null) {
            switch (or.getStatus()) {
                case CHARGED:
                    or.setStatusMessage("SUCCESS");
                    break;
                case OPTIN:
                    or.setStatusMessage("Esperando respuesta del usuario");
                    break;
                case PENDING:
                    or.setStatusMessage("Esperando respuesta del operador");
                    break;
                case ERROR:
                    or.setStatusMessage("Error al realizar el cobro");
                    break;
                case ERROR_CHARGE:
                    or.setStatusMessage("Error al realizar el cobro. Posible falta de credito.");
                    break;
            }
        }

        if (or.getSop() == null || or.getSop().getId() == null || or.getTransactionId() == null) {
            throw new Exception("No existe un SOP o un TransactionId en OnDemandRegistry. [" + or + "]");
        }
        if (or.getId() == null) {
            or.setId(or.getSop().getId() + ".TID." + or.getTransactionId());
        }

        onDemandRegistryService.saveOrUpdate(or);
        try {
            if (or.getTransactionTracking().urlNotify != null && or.getTransactionTracking().urlNotify.length() > 5) {
                ResponseMessage rm = new ResponseMessage();
                rm.status = ResponseMessage.Status.OK;
                OnDemandRegistry ora = new OnDemandRegistry();
                ora.setId(or.getId());
                ora.setStatus(or.getStatus());
                ora.setStatusMessage(or.getStatusMessage());
                ora.setTransactionTracking(or.getTransactionTracking());
                ora.setThread(null);
                rm.data = ora;

                String json = mapper.writeValueAsString(rm);
                requestClient.requestJSONPost(or.getTransactionTracking().urlNotify, json);
            }
        } catch (Exception ex) {
            log.error("Error al notificar a: [" + or.getTransactionTracking().urlNotify + "]. " + ex);
        }
        log.info("OnDemandRegistry actualizado. [" + or + "]");
    }

    @Override
    public void ondemandBillingProcess(OnDemandBilling ob) throws Exception {

        if (ob.getSop() == null || ob.getSop().getId() == null || ob.getTransactionId() == null) {
            throw new Exception("No existe un SOP o un TransactionId en OnDemandBilling. [" + ob + "]");
        }
        if (ob.getId() == null) {
            ob.setId(ob.getSop().getId() + ".TID." + ob.getTransactionId());
        }

        OnDemandRegistry or = onDemandRegistryService.findById(ob.getId());
        int intento = 0;
        if (or == null) {
            while (intento < 5) {
                log.warn("No existe un OnDemandRegistry para ID: [" + ob.getId() + "]");
                try {
                    Thread.sleep(7000);
                } catch (Exception ex) {
                }
                intento++;
                or = onDemandRegistryService.findById(ob.getId());
                if (or != null) {
                    intento = 100;
                }
            }
        }

        if (or == null) {
            log.error("No existe un OnDemandRegistry para ID: [" + ob.getId() + "]");
            RLock lock = instrumentedObject.lockObject(or.getUserAccount() + "_od_" + or.getSop().getId(), 30);
            try {
                or = new OnDemandRegistry();
                or.setId(ob.getId());
                or.setOndemandDate(ob.getChargedDate());
                or.setOriginOndemand("OnDemandRegistry");
                or.setSop(ob.getSop());
                or.setStatus(OnDemandRegistry.Status.CHARGED);
                or.setTransactionId(ob.getTransactionId());
                or.setTransactionTracking(ob.getTransactionTracking());
                or.setUserAccount(ob.getUserAccount());

                ondemandRegistryProcess(or);
            } catch (Exception ex) {
                log.error("Error al crear un OnDemandRegistry: [" + or + "] para OnDemandBilling: [" + ob + "]. " + ex, ex);
            }

            instrumentedObject.unLockObject(lock);
            or = onDemandRegistryService.findById(ob.getId());
            if (or == null) {
                log.fatal("No existe un OnDemandRegistry para ID: [" + ob.getId() + "]");
                return;
            }
        }

        onDemandBillingService.saveOrUpdate(ob);
        log.info("OnDemandBilling actualizada. [" + ob + "]");
    }

    @Override
    public OnDemandRegistry findOnDemandRegistryById(String id) {
        try {
            return onDemandRegistryService.findById(id);
        } catch (Exception ex) {
            log.error("Error al obtener OnDemandRegistry para ID: [" + id + "]. " + ex, ex);
        }
        return null;
    }

    @Override
    public OnDemandRegistry getAvailableByStatusOptin(String userAccout, List<SOP> sops) {
        return onDemandRegistryService.getAvailableByStatusOptin(userAccout, sops);
    }

    @Override
    public OnDemandRegistry getAvailableByStatusPending(String userAccout, List<SOP> sops) {
        return onDemandRegistryService.getAvailableByStatusPending(userAccout, sops);
    }

    @Override
    public OnDemandBilling findOnDemandBillingById(String id) {
        try {
            return onDemandBillingService.findById(id);
        } catch (Exception ex) {
            log.error("Error al obtener OnDemandBilling para ID: [" + id + "]. " + ex, ex);
        }
        return null;
    }
}
