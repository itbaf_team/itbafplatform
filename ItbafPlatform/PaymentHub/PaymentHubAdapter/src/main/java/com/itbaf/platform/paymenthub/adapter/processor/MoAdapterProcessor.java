/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adapter.processor;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.paymenthub.messaging.smpp.adapter.AdapterSMSMessage;
import com.itbaf.platform.paymenthub.model.sms.AdapterSmsLogReceived;
import com.itbaf.platform.paymenthub.services.repository.AdapterSmsLogReceivedService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class MoAdapterProcessor extends AdapterProcessor {

    private final AdapterSmsLogReceivedService adapterSmsLogReceivedtService;

    @Inject
    public MoAdapterProcessor(final AdapterSmsLogReceivedService adapterSmsLogReceivedtService) {
        super(AdapterType.MO_ADAPTER);
        this.adapterSmsLogReceivedtService = Validate.notNull(adapterSmsLogReceivedtService, "An AdapterSmsLogReceivedService class must be provided");
    }

    @Override
    public Boolean process(String msg) throws Exception {
        log.info("Procesando MO_ADAPTER [" + msg + "]");

        AdapterSMSMessage am = mapper.readValue(msg, AdapterSMSMessage.class);
        AdapterSmsLogReceived ar = new AdapterSmsLogReceived();
        ar.setMessage(am);
        ar.setTid("TID_" + am.rcvSmsRequest.getOriginationAddr() + "_" + am.rcvSmsRequest.getDestinationAddr() + "_" + am.rcvSmsRequest.getId() + "_" + am.createdDate.getTime());
        adapterSmsLogReceivedtService.saveOrUpdate(ar);
        
        return rabbitMQProducer.messageSender(CommonFunction.ADAPTER_SMS_RCV + am.providerName, mapper.writeValueAsString(ar));
    }

}
