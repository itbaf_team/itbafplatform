/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.co.movistar.model;

import com.google.common.base.MoreObjects;
import java.util.Date;

/**
 *
 * @author JF
 */
public class DirectDebitEvent {

    public enum Type {
        directDebitEventRes, directDebitEventErr
    }

    public enum UserPlatformId {
        PREPAGO_ALTAMIRA_11("11"),
        PREPAGO_DCO_12("12"),
        POSTPAGO_SCL_13("13"),
        CONTROL_ALTAMIRA_14("14"),
        CONTROL_DCO_15("15");

        private final String value;

        UserPlatformId(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static UserPlatformId fromValue(String v) {
            for (UserPlatformId c : UserPlatformId.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }
    }

    public String idVasp;
    public String subscriberId;
    public String serviceCode;
    public String subServiceCode;

    public Type type;
    public Long requestId;
    public String correlationId;
    public UserPlatformId userPlatformId;
    /**
     * 1911 El identificador del VASP no es válido..............................
     * 1912 La IP que origina el request no corresponde a este VASP.............
     * 1913 El identificador del request no es válido o está duplicado..........
     * 1914 El código de servicio no es válido para este VASP...................
     * 1915 El código de Subservicio no es válido para este VASP y servicio.....
     * 1916 El identificador del abonado no es válido. Debe estar en formato
     * MSISDN ..................................................................
     * 1917 El abonado no está aprovisionado....................................
     * 1955 El tipo de usuario no es válido para este Subservicio...............
     * 1960 Alcanzado el máximo de intentos de cobros diarios por usuario para
     * este Subservicio.........................................................
     * 1970 Error al hacer consulta de autorización PVS.........................
     * .........................................................................
     * 3100 PVS: SubscriptionId no encontrado para Subservicio XBI..............
     * 3101 PVS: Suscripción en estado inactivo.................................
     * 3102 PVS: Integrador en estado inactivo..................................
     * 3103 PVS: Subservicio XBI en estado inactivo.............................
     * 3104 PVS: msisdn : no encontrado en lista blanca para suscripción........
     * 3105 PVS: suscripción no activa para msisdn..............................
     * 3106 PVS: suscripción en periodo promocional, msisdn.....................
     * 3107 PVS: suscripción en periodo facturado...............................
     * 3108 PVS: Suscripción tiene una autorización pendiente...................
     * 3109 PVS: Suscripción se encuentra bloqueada.............................
     * 3110 PVS: Error desconocido..............................................
     * .........................................................................
     * 1951 Ocurrió un error en la plataforma de cobro. No fue posible efectuar
     * el cobro.................................................................
     * 1952 El saldo del abonado es insuficiente para el Subservicio requerido..
     * 1980 Plan del Abonado no es compatible con la categoría del servicio.....
     */
    public String errorCode;
    public String finalURL;
    public Date createdDate = new Date();

    public DirectDebitEvent() {
    }

    public DirectDebitEvent(String idVasp, String subscriberId, String serviceCode, String subServiceCode, String finalURL, final String event) {
        // directDebitEventRes&requestId&correlationId&userPlatformId
        // directDebitEventRes&273669&138646223&11   

        // directDebitEventErr&requestId&errorCode
        // directDebitEventErr&273669&1900   
        String[] aux = event.trim().split("&");
        switch (aux[0]) {
            case "directDebitEventRes":
                type = Type.directDebitEventRes;
                requestId = Long.parseLong(aux[1]);
                correlationId = aux[2];
                userPlatformId = UserPlatformId.fromValue(aux[3]);
                break;
            case "directDebitEventErr":
                type = Type.directDebitEventErr;
                requestId = Long.parseLong(aux[1]);
                errorCode = aux[2];
                break;
        }
        this.idVasp = idVasp.trim();
        this.subscriberId = subscriberId.trim();
        this.serviceCode = serviceCode.trim();
        this.subServiceCode = subServiceCode.trim();
        this.finalURL = finalURL.trim();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("type", type)
                .add("idVasp", idVasp)
                .add("subscriberId", subscriberId)
                .add("serviceCode", serviceCode)
                .add("subServiceCode", subServiceCode)
                .add("requestId", requestId)
                .add("correlationId", correlationId)
                .add("userPlatformId", userPlatformId)
                .add("errorCode", errorCode)
                .add("finalURL", finalURL)
                .add("createdDate", createdDate)
                .omitNullValues().toString();
    }

}
