
package org.csapi.schema.parlayx.subscribeproduct.v1_0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubScriptionInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubScriptionInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="productOrderkey" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="productID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productName" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}DialectInfo" maxOccurs="unbounded"/>
 *         &lt;element name="productDesc" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}DialectInfo" maxOccurs="unbounded"/>
 *         &lt;element name="serviceID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serviceName" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}DialectInfo" maxOccurs="unbounded"/>
 *         &lt;element name="isAutoExtend" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="channelID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="subOperTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unSubOperTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subApplyTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subscriptionState" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="extensionInfo" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}NamedParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubScriptionInfoType", propOrder = {
    "productOrderkey",
    "productID",
    "productName",
    "productDesc",
    "serviceID",
    "serviceName",
    "isAutoExtend",
    "channelID",
    "subOperTime",
    "unSubOperTime",
    "subApplyTime",
    "subExpireTime",
    "subscriptionState",
    "extensionInfo"
})
public class SubScriptionInfoType {

    protected long productOrderkey;
    @XmlElement(required = true)
    protected String productID;
    @XmlElement(required = true)
    protected List<DialectInfo> productName;
    @XmlElement(required = true)
    protected List<DialectInfo> productDesc;
    @XmlElement(required = true)
    protected String serviceID;
    @XmlElement(required = true)
    protected List<DialectInfo> serviceName;
    protected int isAutoExtend;
    protected Integer channelID;
    protected String subOperTime;
    protected String unSubOperTime;
    @XmlElement(required = true)
    protected String subApplyTime;
    @XmlElement(required = true)
    protected String subExpireTime;
    protected int subscriptionState;
    protected NamedParameterList extensionInfo;

    /**
     * Gets the value of the productOrderkey property.
     * 
     */
    public long getProductOrderkey() {
        return productOrderkey;
    }

    /**
     * Sets the value of the productOrderkey property.
     * 
     */
    public void setProductOrderkey(long value) {
        this.productOrderkey = value;
    }

    /**
     * Gets the value of the productID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductID() {
        return productID;
    }

    /**
     * Sets the value of the productID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductID(String value) {
        this.productID = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DialectInfo }
     * 
     * 
     */
    public List<DialectInfo> getProductName() {
        if (productName == null) {
            productName = new ArrayList<DialectInfo>();
        }
        return this.productName;
    }

    /**
     * Gets the value of the productDesc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productDesc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductDesc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DialectInfo }
     * 
     * 
     */
    public List<DialectInfo> getProductDesc() {
        if (productDesc == null) {
            productDesc = new ArrayList<DialectInfo>();
        }
        return this.productDesc;
    }

    /**
     * Gets the value of the serviceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceID() {
        return serviceID;
    }

    /**
     * Sets the value of the serviceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceID(String value) {
        this.serviceID = value;
    }

    /**
     * Gets the value of the serviceName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DialectInfo }
     * 
     * 
     */
    public List<DialectInfo> getServiceName() {
        if (serviceName == null) {
            serviceName = new ArrayList<DialectInfo>();
        }
        return this.serviceName;
    }

    /**
     * Gets the value of the isAutoExtend property.
     * 
     */
    public int getIsAutoExtend() {
        return isAutoExtend;
    }

    /**
     * Sets the value of the isAutoExtend property.
     * 
     */
    public void setIsAutoExtend(int value) {
        this.isAutoExtend = value;
    }

    /**
     * Gets the value of the channelID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChannelID() {
        return channelID;
    }

    /**
     * Sets the value of the channelID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChannelID(Integer value) {
        this.channelID = value;
    }

    /**
     * Gets the value of the subOperTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubOperTime() {
        return subOperTime;
    }

    /**
     * Sets the value of the subOperTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubOperTime(String value) {
        this.subOperTime = value;
    }

    /**
     * Gets the value of the unSubOperTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnSubOperTime() {
        return unSubOperTime;
    }

    /**
     * Sets the value of the unSubOperTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnSubOperTime(String value) {
        this.unSubOperTime = value;
    }

    /**
     * Gets the value of the subApplyTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubApplyTime() {
        return subApplyTime;
    }

    /**
     * Sets the value of the subApplyTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubApplyTime(String value) {
        this.subApplyTime = value;
    }

    /**
     * Gets the value of the subExpireTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubExpireTime() {
        return subExpireTime;
    }

    /**
     * Sets the value of the subExpireTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubExpireTime(String value) {
        this.subExpireTime = value;
    }

    /**
     * Gets the value of the subscriptionState property.
     * 
     */
    public int getSubscriptionState() {
        return subscriptionState;
    }

    /**
     * Sets the value of the subscriptionState property.
     * 
     */
    public void setSubscriptionState(int value) {
        this.subscriptionState = value;
    }

    /**
     * Gets the value of the extensionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link NamedParameterList }
     *     
     */
    public NamedParameterList getExtensionInfo() {
        return extensionInfo;
    }

    /**
     * Sets the value of the extensionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NamedParameterList }
     *     
     */
    public void setExtensionInfo(NamedParameterList value) {
        this.extensionInfo = value;
    }

}
