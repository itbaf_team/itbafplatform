
package com.itbaf.platform.paymenthub.extention.store.ws.subscription;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="viewName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="register" type="{urn:SendatelSoaService}arrayOfExtraParam"/>
 *         &lt;element name="credentials" type="{urn:SendatelSoaService}securityInfo"/>
 *         &lt;element name="params" type="{urn:SendatelSoaService}arrayOfExtraParam"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "viewName",
    "register",
    "credentials",
    "params"
})
@XmlRootElement(name = "insertRegister")
public class InsertRegister {

    @XmlElement(required = true)
    protected String viewName;
    @XmlElement(required = true)
    protected ArrayOfExtraParam register;
    @XmlElement(required = true)
    protected SecurityInfo credentials;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfExtraParam params;

    /**
     * Gets the value of the viewName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViewName() {
        return viewName;
    }

    /**
     * Sets the value of the viewName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViewName(String value) {
        this.viewName = value;
    }

    /**
     * Gets the value of the register property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public ArrayOfExtraParam getRegister() {
        return register;
    }

    /**
     * Sets the value of the register property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public void setRegister(ArrayOfExtraParam value) {
        this.register = value;
    }

    /**
     * Gets the value of the credentials property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityInfo }
     *     
     */
    public SecurityInfo getCredentials() {
        return credentials;
    }

    /**
     * Sets the value of the credentials property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityInfo }
     *     
     */
    public void setCredentials(SecurityInfo value) {
        this.credentials = value;
    }

    /**
     * Gets the value of the params property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public ArrayOfExtraParam getParams() {
        return params;
    }

    /**
     * Sets the value of the params property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public void setParams(ArrayOfExtraParam value) {
        this.params = value;
    }

}
