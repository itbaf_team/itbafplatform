/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.tigo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.base.MoreObjects;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author javier
 */
public class TigoNotification {

    public enum Type {
        subscription, unsubscription
    }

    public Type type;
    private String address;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    private Date receivedDateTime;
    private String serviceCode;
    private String groupCode;
    private String channel;
    private String contextData;
    private Boolean replyEvent;
    private Integer status;

    public TigoNotification() {
    }

    public TigoNotification(Integer status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getReceivedDateTime() {
        return receivedDateTime;
    }

    public void setReceivedDateTime(Date receivedDateTime) {
        this.receivedDateTime = receivedDateTime;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getContextData() {
        return contextData;
    }

    public void setContextData(String contextData) {
        this.contextData = contextData;
    }

    public Boolean getReplyEvent() {
        return replyEvent;
    }

    public void setReplyEvent(Boolean replyEvent) {
        this.replyEvent = replyEvent;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("type", type)
                .add("address", address)
                .add("receivedDateTime", receivedDateTime)
                .add("serviceCode", serviceCode)
                .add("groupCode", groupCode)
                .add("channel", channel)
                .add("contextData", contextData)
                .add("replyEvent", replyEvent)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, address, receivedDateTime, serviceCode, groupCode,
                channel, contextData, replyEvent);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TigoNotification other = (TigoNotification) obj;
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.serviceCode, other.serviceCode)) {
            return false;
        }
        if (!Objects.equals(this.groupCode, other.groupCode)) {
            return false;
        }
        if (!Objects.equals(this.channel, other.channel)) {
            return false;
        }
        if (!Objects.equals(this.contextData, other.contextData)) {
            return false;
        }
        if (!Objects.equals(this.receivedDateTime, other.receivedDateTime)) {
            return false;
        }
        if (!Objects.equals(this.replyEvent, other.replyEvent)) {
            return false;
        }
        return true;
    }

}
