/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class LegalServiceProcessorHandler extends ServiceProcessorHandler {

    @Inject
    public LegalServiceProcessorHandler() {
    }

    public String getLegalHtml(String i18n, String page) {
        String html = null;
        try {
            Object obj = MainCache.memory24Hours().getIfPresent("readResource_legal_" + i18n + "_" + page);
            if (obj == null) {
                html = readResource("/tpl/html/legal/legal.html");
                if (html != null) {
                    html = html.replace("</page>", page);
                    html = i18n(i18n, null, html);
                    if (html.isEmpty()) {
                        return null;
                    }
                    MainCache.memory24Hours().put("readResource_legal_" + i18n + "_" + page, html);
                }
            } else {
                html = (String) obj;
            }
        } catch (Exception ex) {
            html=null;
        }

        return html;
    }

}
