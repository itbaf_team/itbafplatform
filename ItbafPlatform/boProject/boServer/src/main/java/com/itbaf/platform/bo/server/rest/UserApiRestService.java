package com.itbaf.platform.bo.server.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.bo.server.handler.ApiUserServiceProcessorHandler;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageRequest;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;

@Path("/api/user")
@Singleton
@lombok.extern.log4j.Log4j2
public class UserApiRestService extends CommonRestService {

    private final ApiUserServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public UserApiRestService(final ApiUserServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "An ApiUserServiceProcessorHandler class must be provided");
    }

    @POST
    @Path("/")
    // @Permission("api-user-post")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postUser(@Context HttpServletRequest request, PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.post.user");
        } catch (Exception ex) {
        }
        log.info("postUser. PGMessageRequest: [" + messageRequest + "]");
        PGMessageResponse rm = new PGMessageResponse();
        try {
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);

            rm = serviceProcessorHandler.getUserInformation(user);
        } catch (Exception ex) {
            rm.data.message = "Error al obtener informacion del perfil. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

}
