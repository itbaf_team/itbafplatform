package com.itbaf.platform.paymenthub.model.ph;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPHProfileProperties is a Querydsl query type for PHProfileProperties
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPHProfileProperties extends EntityPathBase<PHProfileProperties> {

    private static final long serialVersionUID = -1617301066L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPHProfileProperties pHProfileProperties = new QPHProfileProperties("pHProfileProperties");

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath key = createString("key");

    protected QPHProfile phProfile;

    protected com.itbaf.platform.paymenthub.model.QProvider provider;

    public final StringPath value = createString("value");

    public QPHProfileProperties(String variable) {
        this(PHProfileProperties.class, forVariable(variable), INITS);
    }

    public QPHProfileProperties(Path<? extends PHProfileProperties> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPHProfileProperties(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPHProfileProperties(PathMetadata metadata, PathInits inits) {
        this(PHProfileProperties.class, metadata, inits);
    }

    public QPHProfileProperties(Class<? extends PHProfileProperties> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.phProfile = inits.isInitialized("phProfile") ? new QPHProfile(forProperty("phProfile")) : null;
        this.provider = inits.isInitialized("provider") ? new com.itbaf.platform.paymenthub.model.QProvider(forProperty("provider"), inits.get("provider")) : null;
    }

    public QPHProfile phProfile() {
        if (phProfile == null) {
            phProfile = new QPHProfile(forProperty("phProfile"));
        }
        return phProfile;
    }

    public com.itbaf.platform.paymenthub.model.QProvider provider() {
        if (provider == null) {
            provider = new com.itbaf.platform.paymenthub.model.QProvider(forProperty("provider"));
        }
        return provider;
    }

}

