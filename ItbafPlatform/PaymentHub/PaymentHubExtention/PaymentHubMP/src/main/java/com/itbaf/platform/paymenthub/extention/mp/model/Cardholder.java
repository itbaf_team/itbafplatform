package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Cardholder {

    private String name;
    private Identification identification;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("identification", identification)
                .omitNullValues().toString();
    }
}
