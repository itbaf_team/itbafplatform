
package org.csapi.schema.parlayx.payment.v2_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryConditionsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryConditionsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="msisdnQuery" type="{http://www.csapi.org/schema/parlayx/payment/v2_1}MSISDNQueryType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryConditionsType", propOrder = {
    "msisdnQuery"
})
public class QueryConditionsType {

    protected MSISDNQueryType msisdnQuery;

    /**
     * Gets the value of the msisdnQuery property.
     * 
     * @return
     *     possible object is
     *     {@link MSISDNQueryType }
     *     
     */
    public MSISDNQueryType getMsisdnQuery() {
        return msisdnQuery;
    }

    /**
     * Sets the value of the msisdnQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link MSISDNQueryType }
     *     
     */
    public void setMsisdnQuery(MSISDNQueryType value) {
        this.msisdnQuery = value;
    }

}
