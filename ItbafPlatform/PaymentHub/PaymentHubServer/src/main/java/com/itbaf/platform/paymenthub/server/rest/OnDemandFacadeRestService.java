package com.itbaf.platform.paymenthub.server.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.paymenthub.commons.resources.FacadeCache;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.sync.NormalizerService;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.OnDemandManager;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.ProviderService;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

@Path("/facade/od")
@Singleton
@lombok.extern.log4j.Log4j2
public class OnDemandFacadeRestService extends NormalizerService {

    private final SubscriptionManager subscriptionManager;
    private final OnDemandManager ondemandManager;
    private final RequestClient requestClient;
    private final PHProfilePropertiesService profilePropertiesService;
    private final ObjectMapper mapper;
    private final InstrumentedObject instrumentedObject;
    private final FacadeCache facadeCache;
    private final ProviderService providerService;
    private final ExecutorService cachedThreadPool;

    @Inject
    public OnDemandFacadeRestService(final PHProfilePropertiesService profilePropertiesService,
            final SubscriptionManager subscriptionManager,
            final OnDemandManager ondemandManager,
            final RequestClient requestClient, final FacadeCache facadeCache,
            final ObjectMapper mapper, final InstrumentedObject instrumentedObject,
            final ProviderService providerService, final ExecutorService cachedThreadPool) {
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
        this.ondemandManager = Validate.notNull(ondemandManager, "An OnDemandManager class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.facadeCache = Validate.notNull(facadeCache, "A FacadeCache class must be provided");
        this.providerService = Validate.notNull(providerService, "A ProviderService class must be provided");
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "A ExecutorService class must be provided");
    }

    private OnDemandRegistry getOnDemandRegistry(FacadeRequest request, ResponseMessage rm) {

        OnDemandRegistry or = null;
        SOP sop;
        try {
            sop = subscriptionManager.getSOP(request.service, request.operator, request.provider);
            if (sop == null) {
                log.error("No hay un SOP definido. Request: [" + request + "]");
                return or;
            }
        } catch (Exception ex) {
            log.error("Error al obtener el SOP. Request: [" + request + "]. " + ex, ex);
            return or;
        }

        Subscription aux = new Subscription();
        aux.setSop(sop);
        aux.setUserAccount(request.userAccount);

        if (aux.getSop().getProvider().getName().contains(".mp.") || aux.getSop().getProvider().getName().endsWith(".npay")) {
            String url = profilePropertiesService.getValueByProviderKey(aux.getSop().getProvider().getId(), "internal.service.url") + "subscription/normalizer";
            String response = null;
            try {
                response = requestClient.requestJSONPostFacade(url, mapper.writeValueAsString(aux));
                log.info(aux.getSop().getProvider().getName() + " response: [" + response + "]");
                ResponseMessage rsp = mapper.readValue(response, ResponseMessage.class);

                if (rsp.status == ResponseMessage.Status.ERROR || rsp.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = rsp.status;
                    rm.message = rsp.message;
                    return null;
                }
                aux = mapper.readValue((String) rsp.data, Subscription.class);
            } catch (Exception ex) {
                rm.message = "Error al procesar response. [" + response + "]. " + ex;
                log.error(rm.message, ex);
                rm.status = ResponseMessage.Status.ERROR;
                return null;
            }
        } else {
            try {
                aux.setUserAccount(userAccountNormalizer(aux.getUserAccount(), sop.getProvider()));
            } catch (Exception ex) {
                rm.status = ResponseMessage.Status.ERROR_403;
                rm.message = ex.getMessage();
                return null;
            }
        }

        or = new OnDemandRegistry();
        or.setUserAccount(aux.getUserAccount());
        or.setSop(sop);
        or.setTransactionTracking(request);
        or.setOndemandDate(new Date());
        or.setOriginOndemand("Facade");

        RLock lock = null;
        try {
            lock = instrumentedObject.lockObject(or.getUserAccount() + "_od_" + sop.getId(), 30);
        } catch (Exception ex) {
            log.error("Error al sincronizar facade. " + ex, ex);
        }

        or.setLock(lock);
        return or;
    }

    @PUT
    @Path("/charge")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putCharge(FacadeRequest request) {

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.od.c");
        } catch (Exception ex) {
        }

        log.info("putCharge. Facade request: [" + request + "]");
        ResponseMessage rm = new ResponseMessage();

        if (request == null || request.contentName == null || request.userAccount == null
                || request.amount == null || request.currency == null || request.contentUrl == null) {
            rm.status = ResponseMessage.Status.ERROR;
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
        try {
            OnDemandRegistry or = getOnDemandRegistry(request, rm);
            if (or == null) {
                rm.message = "Error al crear OnDemandRegistry. " + (rm.message == null ? "" : rm.message);
                if (rm.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = ResponseMessage.Status.ERROR;
                    return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            RLock lock = (RLock) or.getLock();
            or.setLock(null);
            String url = profilePropertiesService.getValueByProviderKey(or.getSop().getProvider().getId(), "internal.service.url") + "charge";
            String response;
            ResponseMessage rsp;
            try {
                response = requestClient.requestJSONPostFacade(url, mapper.writeValueAsString(or));
                log.info(or.getSop().getProvider().getName() + " response: [" + response + "]");
                rsp = mapper.readValue(response, ResponseMessage.class);
                or = mapper.readValue((String) rsp.data, OnDemandRegistry.class);
            } catch (Exception ex) {
                rm.message = "Error al interpretar el JSON. " + ex;
                log.error(rm.message, ex);
                rm.status = ResponseMessage.Status.ERROR;
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            if (ResponseMessage.Status.ERROR.equals(rsp.status)) {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = rsp.message;
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            } else if (ResponseMessage.Status.ERROR_403.equals(rsp.status)) {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = rsp.message;
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
            } else {
                try {
                    ondemandManager.ondemandRegistryProcess(or);
                } catch (Exception ex) {
                    log.error("Error al procesar OnDemandRegistry: [" + or + "]. " + ex, ex);
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "Error al procesar OnDemandRegistry: [" + or + "]. " + ex;
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                rm.status = ResponseMessage.Status.OK;

                OnDemandRegistry ora = new OnDemandRegistry();
                ora.setId(or.getId());
                ora.setStatus(or.getStatus());
                ora.setStatusMessage(or.getStatusMessage());
                ora.setTransactionTracking(or.getTransactionTracking());
                ora.setThread(null);
                rm.data = ora;
            }
            rm.message = rsp.message;

            instrumentedObject.unLockObject(lock);
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            rm.message = "Error al procesar request: [" + request + "]. " + ex;
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @POST
    @Path("/status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response status(FacadeRequest request) {

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.od.status");
        } catch (Exception ex) {
        }

        log.info("Status. Facade request: [" + request + "]");
        ResponseMessage rm = new ResponseMessage();

        if (request == null || request.transactionId == null) {
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
        try {
            OnDemandRegistry or = ondemandManager.findOnDemandRegistryById(request.transactionId);
            if (or == null) {
                rm.message = "Error al obtener suscripcion. " + request.transactionId;
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            rm.status = ResponseMessage.Status.OK;
            OnDemandRegistry aux = new OnDemandRegistry();
            aux.setStatus(or.getStatus());
            aux.setStatusMessage(or.getStatusMessage());
            aux.setTransactionTracking(or.getTransactionTracking());
            aux.setThread(null);
            rm.data = aux;

            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            rm.message = "Error al procesar request: [" + request + "]. " + ex;
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

}
