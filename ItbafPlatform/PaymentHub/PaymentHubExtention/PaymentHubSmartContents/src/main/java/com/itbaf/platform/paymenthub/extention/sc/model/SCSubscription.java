/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.sc.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class SCSubscription {

    public String id;
    public String sid;
    public String user_id;
    public String expire_at;
    public String billed_at;
    public Integer hits;
    public String protocol;
    public String channel_in;
    public String provider;
    public String channel_out;
    public String campaign_id;
    public String product_id;
    public String plan_id;
    /**
     * 0 Unsubscribed...........................................................
     * 1 Subscription Active....................................................
     * 2 Subscription Inactive (Pending Billing)................................
     * 3 Subscription Freezing. (For analisis)..................................
     */
    public Integer status;
    public String created_at;
    public String updated_at;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("sid", sid)
                .add("user_id", user_id)
                .add("expire_at", expire_at)
                .add("billed_at", billed_at)
                .add("hits", hits)
                .add("protocol", protocol)
                .add("channel_in", channel_in)
                .add("provider", provider)
                .add("channel_out", channel_out)
                .add("campaign_id", campaign_id)
                .add("product_id", product_id)
                .add("plan_id", plan_id)
                .add("status", status)
                .add("created_at", created_at)
                .add("updated_at", updated_at)
                .omitNullValues().toString();
    }
}
