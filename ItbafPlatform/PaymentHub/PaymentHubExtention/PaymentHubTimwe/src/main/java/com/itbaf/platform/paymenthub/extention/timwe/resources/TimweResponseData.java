/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.resources;

/**
 *
 * @author JF
 */
public class TimweResponseData {

    public String transactionUUID;
    public String subSubStatusDate;
    public String lastRenewalOkDate;
    public String externalTxId;
    public String renewalPricepointId;
    public String subSubStatus;
    public String subscriptionStatus;
    public String subscriptionError;
    public String subStatusDate;
    public String lastRenewalNotOkDate;
    public String transactionId;
    public String optinPricepointId;
    public String subscriptionResult;

}
