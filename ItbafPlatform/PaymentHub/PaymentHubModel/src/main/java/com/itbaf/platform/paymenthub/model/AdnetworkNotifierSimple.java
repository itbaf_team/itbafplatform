/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.google.common.base.MoreObjects;
import java.io.Serializable;
import javax.persistence.Cacheable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;
import java.util.Date;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Type;

/**
 *
 * @author javier
 */
@Entity(name = "ADNETWORK_NOTIFIER_SIMPLE")
@Table(name = "ADNETWORK_NOTIFIER_SIMPLE", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors = true)
public class AdnetworkNotifierSimple extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 20180118153801L;

    @Id
    @Column(name = "TRANSACTION_ID", nullable = false, unique = true)
    private String trackingId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SUBSCRIPTION_DATE")
    private Date subscriptionDate;

    @OneToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;

    @Type(type = "json")
    @Column(name = "ADNETWORK_TRACKING", columnDefinition = "json")
    private Object adnetworkTracking;

    @Column(name = "NOTIFICATION_SEND")
    private Boolean notificationSend;

    @Column(name = "ADN_RESPONSE")
    private String response;

    public AdnetworkNotifierSimple() {
        notificationSend = Boolean.FALSE;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("trackingId", trackingId)
                .add("subscriptionDate", subscriptionDate)
                .add("sop", sop)
                .add("adnetworkTracking", adnetworkTracking)
                .add("notificationSend", notificationSend)
                .add("response", response)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.trackingId);
        hash = 17 * hash + Objects.hashCode(this.sop);
        hash = 17 * hash + Objects.hashCode(this.adnetworkTracking);
        hash = 17 * hash + Objects.hashCode(this.notificationSend);
        hash = 17 * hash + Objects.hashCode(this.response);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AdnetworkNotifierSimple other = (AdnetworkNotifierSimple) obj;
        if (!Objects.equals(this.trackingId, other.trackingId)) {
            return false;
        }
        if (!Objects.equals(this.response, other.response)) {
            return false;
        }
        if (!Objects.equals(this.sop, other.sop)) {
            return false;
        }
        if (!Objects.equals(this.adnetworkTracking, other.adnetworkTracking)) {
            return false;
        }
        if (!Objects.equals(this.notificationSend, other.notificationSend)) {
            return false;
        }
        return true;
    }

}
