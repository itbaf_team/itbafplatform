/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import java.util.List;

/**
 *
 * @author javier
 */
public interface OnDemandRegistryService {

    public OnDemandRegistry findById(String id) throws Exception;

    public void saveOrUpdate(OnDemandRegistry or) throws Exception;

    public OnDemandRegistry findByTransactionId(Long sopId, String transactionId);

    public OnDemandRegistry getAvailableByStatusPending(String userAccout, List<SOP> sops);

    public OnDemandRegistry getAvailableByStatusOptin(String userAccout, List<SOP> sops);
}
