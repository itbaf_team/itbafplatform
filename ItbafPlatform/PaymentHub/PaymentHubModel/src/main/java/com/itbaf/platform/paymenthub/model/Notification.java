/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
@Entity(name = "NOTIFICATION")
@Table(name = "NOTIFICATION", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(false)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Enumerated(EnumType.STRING)
    @Column(name = "NOTIFICATION_TYPE")
    private MessageType notificationType;
    @ManyToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;
    @Column(name = "INTERNAL_ID")
    private String internalId;
    @Column(name = "EXTERNAL_ID")
    private String externalId;
    @Column(name = "TRANSACTION_ID")
    private String transactionId;
    @Column(name = "USER_ACCOUNT")
    private String userAccount;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "NOTIFICATION_DATE")
    private Date notificationDate;
    @Column(name = "ORIGINAL_NOTIFICATION")
    private String originalNotification;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("notificationType", notificationType)
                .add("sop", sop)
                .add("internalId", internalId)
                .add("externalId", externalId)
                .add("transactionId", transactionId)
                .add("userAccount", userAccount)
                .add("notificationDate", notificationDate)
                .add("originalNotification", originalNotification)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, notificationType, sop, internalId, externalId, transactionId, userAccount, notificationDate, originalNotification);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<Notification>()
                .over(Notification::getId)
                .over(Notification::getNotificationType)
                .over(Notification::getSop)
                .over(Notification::getInternalId)
                .over(Notification::getExternalId)
                .over(Notification::getTransactionId)
                .over(Notification::getUserAccount)
                .over(Notification::getNotificationDate)
                .over(Notification::getOriginalNotification)
                .asserts(this, obj);
    }
}
