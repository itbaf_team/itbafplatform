/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.bn.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class NPAYNotification extends BNNotification {

    private String id_event;
    private String ipn_url;
    private String ipn_type;
    private String verify_sign;
    private String id_app;
    private String id_customer;
    private String id_transaction;
    private String amount;
    private String currency;
    private String id_subscription;
    private String status;
    private String id_service;
    private String created;
    private String updated;
    private String tracking_id;
    private String meta;
    private String subscription_date;
    private String billing_date;
    private String channel;
    private String cancelation_date;
    private String tried_at;
    public String thread = Thread.currentThread().getName();

    public NPAYNotification() {
    }

    public NPAYNotification(String id_event, String ipn_url, String ipn_type, String verify_sign, String id_app,
            String id_customer, String id_transaction, String amount, String currency, String id_subscription,
            String status, String id_service, String created, String updated, String tracking_id, String meta,
            String subscription_date, String billing_date, String channel, String cancelation_date, String tried_at) {
        this.id_event = id_event;
        this.ipn_url = ipn_url;
        this.ipn_type = ipn_type;
        this.verify_sign = verify_sign;
        this.id_app = id_app;
        this.id_customer = id_customer;
        this.id_transaction = id_transaction;
        this.amount = amount;
        this.currency = currency;
        this.id_subscription = id_subscription;
        this.status = status;
        this.id_service = id_service;
        this.created = created;
        this.updated = updated;
        this.tracking_id = tracking_id;
        this.meta = meta;
        this.subscription_date = subscription_date;
        this.billing_date = billing_date;
        this.channel = channel;
        this.cancelation_date = cancelation_date;
        this.tried_at = tried_at;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id_event", id_event)
                .add("ipn_url", ipn_url)
                .add("ipn_type", ipn_type)
                .add("verify_sign", verify_sign)
                .add("id_app", id_app)
                .add("id_customer", id_customer)
                .add("id_transaction", id_transaction)
                .add("amount", amount)
                .add("currency", currency)
                .add("id_subscription", id_subscription)
                .add("status", status)
                .add("id_service", id_service)
                .add("created", created)
                .add("updated", updated)
                .add("subscription_date", subscription_date)
                .add("billing_date", billing_date)
                .add("channel", channel)
                .add("cancelation_date", cancelation_date)
                .add("tried_at", tried_at)
                .add("tracking_id", tracking_id)
                .add("meta", meta)
                .add("thread", thread)
                .omitNullValues().toString();
    }

}
