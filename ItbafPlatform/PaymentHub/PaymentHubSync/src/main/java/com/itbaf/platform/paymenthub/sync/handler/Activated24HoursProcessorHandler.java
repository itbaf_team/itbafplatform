package com.itbaf.platform.paymenthub.sync.handler;

import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.Validate;

import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;

@lombok.extern.log4j.Log4j2
public class Activated24HoursProcessorHandler {


    private final ObjectMapper mapper;
    private final RabbitMQProducer rabbitMQProducer;
    private final SubscriptionService subscriptionService;

    @Inject
    public Activated24HoursProcessorHandler(final ObjectMapper mapper,
                                            final RabbitMQProducer rabbitMQProducer,
                                            final SubscriptionService subscriptionService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.subscriptionService = Validate.notNull(subscriptionService, "A SubscriptionService class must be provided");
    }

    public boolean process() {
        try{
            List<Subscription> lst = subscriptionService.getActived24hsAndWithoutBilling();

            log.info("Suscriptions activated more than 24 hours ago without billing; List<Subscription>: [" + (lst == null ? "0" : lst.size()) + (lst == null || lst.size() == 0 ? "" : " - First: "  + lst.get(0)) + "]");

            if (lst == null) {
                return true;
            }

            for (Subscription subscription : lst) {
              //  log.info("Se procesará baja de suscripción con estado de retention: [" + subscription.getId() + "]");
                this.processActived24HoursWithoutBilling(subscription);
            }
            return true;
        }  catch (Exception ex) {
            log.error("Error al procesar SyncActivated24HoursWithoutBilling. " + ex, ex);
        }
        return false;
    }

    private Boolean processActived24HoursWithoutBilling(Subscription subscription) {
        SOP sop = subscription.getSop();
        if (sop == null) {
            log.error("No existe un SOP configurado para la suscripción: [" + subscription.getId()  + "]");
            return null;
        }

        try {
            Provider provider = subscription.getSop().getProvider();
            Tariff t = sop.getMainTariff();

            PaymentHubMessage msg = new PaymentHubMessage();

            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrecuency(t.getFrecuency());
            msg.setFrequencyType(t.getFrecuencyType());
            msg.setFromDate(new Date());
            msg.setFullAmount(t.getFullAmount());
            msg.setNetAmount(t.getNetAmount());
            msg.setMessageType(MessageType.SUBSCRIPTION_PENDING);
            msg.setOrigin(PaymentHubMessage.OriginMessage.SYNC_CHECK);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(subscription.getSubscriptionRegistry().getExternalId());
            msg.setUserAccount(subscription.getUserAccount());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
            return true;
        } catch (Exception ex) {
            log.error("Error al procesar SyncActivated24HoursWithoutBilling: [" + subscription + "]. " + ex, ex);
        }
        return null;
    }

}
