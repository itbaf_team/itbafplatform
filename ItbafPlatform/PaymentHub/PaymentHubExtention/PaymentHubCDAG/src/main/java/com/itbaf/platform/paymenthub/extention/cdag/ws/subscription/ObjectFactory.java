
package com.itbaf.platform.paymenthub.extention.cdag.ws.subscription;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.blowbit.telcohub.integration.external.personal.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.blowbit.telcohub.integration.external.personal.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultaSuscripcionesUsuarioResponse }
     * 
     */
    public ConsultaSuscripcionesUsuarioResponse createConsultaSuscripcionesUsuarioResponse() {
        return new ConsultaSuscripcionesUsuarioResponse();
    }

    /**
     * Create an instance of {@link StatusResponse }
     * 
     */
    public StatusResponse createStatusResponse() {
        return new StatusResponse();
    }

    /**
     * Create an instance of {@link UsuarioSuscripcion }
     * 
     */
    public UsuarioSuscripcion createUsuarioSuscripcion() {
        return new UsuarioSuscripcion();
    }

    /**
     * Create an instance of {@link BajaSuscripcion }
     * 
     */
    public BajaSuscripcion createBajaSuscripcion() {
        return new BajaSuscripcion();
    }

    /**
     * Create an instance of {@link SecurityInfo }
     * 
     */
    public SecurityInfo createSecurityInfo() {
        return new SecurityInfo();
    }

    /**
     * Create an instance of {@link ArrayOfExtraParam }
     * 
     */
    public ArrayOfExtraParam createArrayOfExtraParam() {
        return new ArrayOfExtraParam();
    }

    /**
     * Create an instance of {@link ListarSuscripcionesResponse }
     * 
     */
    public ListarSuscripcionesResponse createListarSuscripcionesResponse() {
        return new ListarSuscripcionesResponse();
    }

    /**
     * Create an instance of {@link Suscripcion }
     * 
     */
    public Suscripcion createSuscripcion() {
        return new Suscripcion();
    }

    /**
     * Create an instance of {@link ConsultaSuscripcionesUsuario }
     * 
     */
    public ConsultaSuscripcionesUsuario createConsultaSuscripcionesUsuario() {
        return new ConsultaSuscripcionesUsuario();
    }

    /**
     * Create an instance of {@link ListarSuscripciones }
     * 
     */
    public ListarSuscripciones createListarSuscripciones() {
        return new ListarSuscripciones();
    }

    /**
     * Create an instance of {@link AltaSuscripcion }
     * 
     */
    public AltaSuscripcion createAltaSuscripcion() {
        return new AltaSuscripcion();
    }

    /**
     * Create an instance of {@link ResultadoAccion }
     * 
     */
    public ResultadoAccion createResultadoAccion() {
        return new ResultadoAccion();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link GetPossibleValues }
     * 
     */
    public GetPossibleValues createGetPossibleValues() {
        return new GetPossibleValues();
    }

    /**
     * Create an instance of {@link GetPossibleValuesResponse }
     * 
     */
    public GetPossibleValuesResponse createGetPossibleValuesResponse() {
        return new GetPossibleValuesResponse();
    }

    /**
     * Create an instance of {@link InsertRegister }
     * 
     */
    public InsertRegister createInsertRegister() {
        return new InsertRegister();
    }

    /**
     * Create an instance of {@link GetList }
     * 
     */
    public GetList createGetList() {
        return new GetList();
    }

    /**
     * Create an instance of {@link ArrayOfFilter }
     * 
     */
    public ArrayOfFilter createArrayOfFilter() {
        return new ArrayOfFilter();
    }

    /**
     * Create an instance of {@link ArrayOfFieldOrder }
     * 
     */
    public ArrayOfFieldOrder createArrayOfFieldOrder() {
        return new ArrayOfFieldOrder();
    }

    /**
     * Create an instance of {@link GetListResponse }
     * 
     */
    public GetListResponse createGetListResponse() {
        return new GetListResponse();
    }

    /**
     * Create an instance of {@link ListView }
     * 
     */
    public ListView createListView() {
        return new ListView();
    }

    /**
     * Create an instance of {@link InsertRegisterResponse }
     * 
     */
    public InsertRegisterResponse createInsertRegisterResponse() {
        return new InsertRegisterResponse();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link ArrayOfFieldList }
     * 
     */
    public ArrayOfFieldList createArrayOfFieldList() {
        return new ArrayOfFieldList();
    }

    /**
     * Create an instance of {@link Filter }
     * 
     */
    public Filter createFilter() {
        return new Filter();
    }

    /**
     * Create an instance of {@link MatrixOfFieldList }
     * 
     */
    public MatrixOfFieldList createMatrixOfFieldList() {
        return new MatrixOfFieldList();
    }

    /**
     * Create an instance of {@link ExtraParam }
     * 
     */
    public ExtraParam createExtraParam() {
        return new ExtraParam();
    }

    /**
     * Create an instance of {@link FieldOrder }
     * 
     */
    public FieldOrder createFieldOrder() {
        return new FieldOrder();
    }

    /**
     * Create an instance of {@link FieldList }
     * 
     */
    public FieldList createFieldList() {
        return new FieldList();
    }

    /**
     * Create an instance of {@link NotificarEventoResponse }
     * 
     */
    public NotificarEventoResponse createNotificarEventoResponse() {
        return new NotificarEventoResponse();
    }

    /**
     * Create an instance of {@link NotificarEvento }
     * 
     */
    public NotificarEvento createNotificarEvento() {
        return new NotificarEvento();
    }

    /**
     * Create an instance of {@link Notification }
     * 
     */
    public Notification createNotification() {
        return new Notification();
    }

}
