/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.etl.consumer;

import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.dw.etl.procesor.StatsProcessor;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQMessageProcessorImpl implements RabbitMQMessageProcessor {

    private final StatsProcessor statsProcessor;

    @AssistedInject
    public RabbitMQMessageProcessorImpl(@Assisted final StatsProcessor statsProcessor) {
        this.statsProcessor = Validate.notNull(statsProcessor, "A StatsProcessor class must be provided");
    }

    @Override
    public Boolean processMessage(String message) {
        try {
            String tid = Thread.currentThread().getName() + ".dw." + statsProcessor.getElasticType().name();
            Thread.currentThread().setName(tid);
        } catch (Exception ex) {
        }
        
        if (message == null || message.isEmpty()) {
            return false;
        }
        
        try {
            return statsProcessor.process(message);
        } catch (Exception ex) {
            log.error("Error al procesar StatsMessage: [" + message + "]. " + ex, ex);
        }
        return null;
    }

}
