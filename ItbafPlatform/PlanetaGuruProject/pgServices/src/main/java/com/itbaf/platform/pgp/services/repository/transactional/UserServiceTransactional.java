/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository.transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.pgp.commons.resources.ClientCache;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Client;
import com.itbaf.platform.pgp.repository.jpa.model.oauth.JPAUserRepository;
import com.itbaf.platform.pgp.services.repository.GenericPropertyService;
import com.itbaf.platform.pgp.services.repository.UserService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.impl.DefaultClaims;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class UserServiceTransactional implements UserService {
    
    private final ObjectMapper mapper;
    private final ClientCache clientCache;
    private final JPAUserRepository userRepository;
    private final InstrumentedObject instrumentedObject;
    private final GenericPropertyService genericPropertyService;
    
    @Inject
    public UserServiceTransactional(final ObjectMapper mapper,
            final ClientCache clientCache,
            final JPAUserRepository userRepository,
            final InstrumentedObject instrumentedObject,
            final GenericPropertyService genericPropertyService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.clientCache = Validate.notNull(clientCache, "A ClientCache class must be provided");
        this.userRepository = Validate.notNull(userRepository, "A JPAUserRepository class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.genericPropertyService = Validate.notNull(genericPropertyService, "A GenericPropertyService class must be provided");
    }
    
    @Override
    public void saveOrUpdate(User user) throws Exception {
        String classs = "";
        String method = "";
        int line = 0;
        try {
            StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
            StackTraceElement e = stacktrace[10];
            classs = e.getClassName();
            method = e.getMethodName();
            line = e.getLineNumber();
        } catch (Exception ex) {
            
        }
        
        String key = "saveOrUpdate_service_"
                + Arrays.toString(user.getCredentialUserProperty().getUsername().getMsisdns().keySet().toArray()) + "_"
                + Arrays.toString(user.getCredentialUserProperty().getUsername().getEmails().keySet().toArray());
        RLock lock = instrumentedObject.lockObject(key, 15);
        log.info("UserServiceTransactional. saveOrUpdate: [" + classs + "][" + method + "][" + line + "][" + user + "]");
        if (user.getId() == null) {
            String id = clientCache.getTemporalData(key);
            if (id != null) {
                User u = findById(Long.parseLong(id));
                if (u != null) {
                    user.setId(u.getId());
                    user.setLimitPermissions(u.getLimitPermissions());
                    user.setPermissions(u.getPermissions());
                    user.setPrincipal(u.getPrincipal());
                    user.setProducts(u.getProducts());
                    user.setRole(u.getRole());
                    user.setStatus(u.getStatus());
                    user.setUserProperties(u.getUserProperties());
                    user.setUsers(u.getUsers());
                } else {
                    userRepository.save(user);
                    clientCache.setTemporalData(key, user.getId().toString(), 2);
                }
            } else {
                userRepository.save(user);
                clientCache.setTemporalData(key, user.getId().toString(), 2);
            }
        } else {
            userRepository.merge(user);
        }
        instrumentedObject.unLockObject(lock);
    }
    
    @Override
    public List<User> getAll() {
        try {
            return userRepository.getAll();
        } catch (Exception ex) {
            log.error("Error al obtener User List. " + ex, ex);
        }
        return new ArrayList();
    }
    
    @Override
    public User findByclientId(String clientId) {
        User user = null;
        Object o = MainCache.memory24Hours().getIfPresent("F_CLIENT_ID_" + clientId);
        if (o == null) {
            try {
                user = userRepository.findByclientId(clientId);
                if (user != null) {
                    MainCache.memory24Hours().put("F_CLIENT_ID_" + clientId, user);
                }
            } catch (Exception ex) {
                log.error("Error al obtener USER para clientId: [" + clientId + "]. " + ex, ex);
            }
        } else {
            user = (User) o;
        }
        return user;
    }
    
    @Override
    public User findById(Long id) {
        try {
            return userRepository.findById(id);
        } catch (Exception ex) {
            log.error("Error al obtener USER para ID: [" + id + "]. " + ex, ex);
        }
        return null;
    }
    
    @Override
    public List<User> findByIds(List<Long> ids) {
        try {
            return userRepository.findByIds(ids);
        } catch (Exception ex) {
            log.error("Error al obtener USERs para IDs: [" + ids + "]. " + ex, ex);
        }
        return null;
    }
    
    @Override
    public User findByEmail(String email) {
        return userRepository.findByUsername("email", email);
    }
    
    @Override
    public User findByMsisdn(String msisdn) {
        return userRepository.findByUsername("msisdn", msisdn);
    }
    
    @Override
    public User findByMsisdn(String prefix, String msisdn, Holder<String> username) {
        return userRepository.findByMsisdn(prefix, msisdn, username);
    }
    
    @Override
    public User authenticateUser(User client, String userToken, DefaultClaims defaultClaims) {
        try {
            DefaultClaims dc = Client.validateToken(genericPropertyService.getValueByKey("user.sign.secret"), userToken);
            String userId = dc.getIssuer();
            /*UserSSOPersonal up = mapper.convertValue(dc.get("ar.p.tk", LinkedHashMap.class), UserSSOPersonal.class);

            if (up != null && !ssoPersonalServices.tokenValidate(up.token)) {
                log.info("Token invalido para Personal: [" + up + "]");
                return null;
            }*/
            
            User user = findById(Long.parseLong(userId));
            //  log.info("Autenticando User ID: [" + user.getId() + "] - FirstName: [" + user.getProfileUserProperty().getFirstName() + "]");
            if (Status.ENABLE.equals(user.getStatus())) {
                String inner = dc.get("inner", String.class);
                if (Client.validateToken(user.getId() + "_" + user.getCredentialUserProperty().getPassword() + "_" + client.getId(), inner).getIssuer().equals(userId)) {
                    if (user.getSessionUserProperty().getSessions().get(client.getId().toString()) != null
                            && user.getSessionUserProperty().getSessions().get(client.getId().toString()).isActive()
                            && user.getSessionUserProperty().getSessions().get(client.getId().toString()).getCookieTokens().get(userToken) != null) {
                        if (defaultClaims != null) {
                            defaultClaims.putAll(dc);
                        }
                        return user;
                    }
                } else {
                    log.warn("UserToken [" + userToken + "] no coincide con el password... User: [" + userId + "]");
                }
            }
        } catch (ExpiredJwtException ex) {
            log.warn("Invalido Token: [" + userToken + "]. " + ex);
        } catch (Exception ex) {
            log.error("Error al autenticar User con token: [" + userToken + "]. " + ex);
        }
        
        return null;
    }
    
    @Override
    public List<User> getAllUserClients() {
        List<User> lst;
        Object obj = MainCache.memory24Hours().getIfPresent("getAllUserClients_list");
        if (obj == null) {
            lst = userRepository.getAllUserClients();
            if (lst != null && !lst.isEmpty()) {
                MainCache.memory24Hours().put("getAllUserClients_list", lst);
            }
        } else {
            lst = (List<User>) obj;
        }
        
        return lst;
        
    }
    
    @Override
    public User findByNickname(String nickname) {
        return userRepository.findByNickname(nickname);
    }
}
