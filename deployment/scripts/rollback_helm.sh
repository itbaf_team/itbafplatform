#!/bin/sh
set -e

# Parse command-line options

# Option strings
SHORT=r:
LONG=helm-release-name:

# read the options
OPTS=$(getopt --options $SHORT --long $LONG --name "$0" -- "$@")

if [ $? != 0 ] ; then echo "Failed to parse options...exiting." >&2 ; exit 1 ; fi

eval set -- "$OPTS"

# extract options and their arguments into variables.
while true ; do
  case "$1" in
    -r | --helm-release-name )
      HELM_RELEASE_NAME="$2"
      echo "HELM_RELEASE_NAME: $2"
      shift 2
      ;;
    -- )
      shift ;
      break
      ;;
  esac
done

#helm init --service-account tiller --client-only || true
helm rollback --force --debug ${HELM_RELEASE_NAME} 0
