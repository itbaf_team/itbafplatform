/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.smsc.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.resources.FacadeCache;
import com.itbaf.platform.paymenthub.extention.smsc.model.ActivosProductoResponse;
import com.itbaf.platform.paymenthub.extention.smsc.model.CTR;
import com.itbaf.platform.paymenthub.extention.smsc.model.Notification;
import com.itbaf.platform.paymenthub.extention.smsc.model.ValueResponse;
import com.itbaf.platform.paymenthub.extention.smsc.resources.SMSCResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import io.jsonwebtoken.lang.Collections;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final SMSCResources smscResources;
    private final Map<String, Provider> providerMap;
    private final FacadeCache facadeCache;

    @Inject
    public ServiceProcessorHandler(final FacadeCache facadeCache,
            final SMSCResources smscResources,
            final Map<String, Provider> providerMap) {
        this.facadeCache = Validate.notNull(facadeCache, "A FacadeCache class must be provided");
        this.smscResources = Validate.notNull(smscResources, "A SMSCResources class must be provided");
        this.providerMap = Validate.notNull(providerMap, "A Map<String, Provider> class must be provided");
    }

    public void processNotification(String countryCode, final String ctr) throws Exception {

        Provider p = providerMap.get(countryCode.toUpperCase());
        if (p == null) {
            log.error("No hay configurado un provider para CountryCode: [" + countryCode + "]");
            return;
        }

        JavaType type = mapper.getTypeFactory().constructArrayType(CTR.class);
        CTR[] ctrs = mapper.readValue(ctr, type);

        Notification notification = new Notification();
        notification.countryCode = countryCode;
        notification.ctrs = ctrs;

        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notification);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notification);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {
        Notification notification;
        try {
            notification = mapper.readValue(message, Notification.class);
            Thread.currentThread().setName(notification.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }
        Provider provider = providerMap.get(notification.countryCode.toUpperCase());
        final String threadName = Thread.currentThread().getName();
        for (CTR ctr : notification.ctrs) {
            try {
                Thread.currentThread().setName(threadName + "-" + ctr.getIdEvento() + "-" + ctr.getIdCTR());
                if (processNotification(ctr, provider) == null) {
                    if (notification.ctrs.length == 1) {
                        return null;
                    }
                    Notification aux = new Notification();
                    aux.countryCode = notification.countryCode;
                    aux.ctrs = new CTR[1];
                    aux.ctrs[0] = ctr;
                    String jsonResponse;
                    jsonResponse = mapper.writeValueAsString(aux);
                    if (jsonResponse != null) {
                        rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + provider.getName(), jsonResponse);
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar CTR: [" + ctr + "]. " + ex, ex);
                return null;
            }
        }

        return true;
    }

    private Boolean processNotification(CTR notification, Provider provider) throws Exception {
        Boolean response = null;
        notification.setMsisdn(this.userAccountNormalizer(notification.getMsisdn(), provider));

        switch (notification.getIdEvento()) {
            case 1: // Alta
                //  log.info("Notificacion de alta. [" + notification.getIdEvento() + "]");
                response = processSubscriptionNotification(notification, provider, MessageType.SUBSCRIPTION_ACTIVE);
                if (response != null) {
                    response = processBillingNotification(notification, provider);
                }
                break;
            case 2: // Baja SMS
            case 3: // Baja CRM
            case 4: // Baja por proceso o reciclaje de linea
            case 11: // Baja Migracion
            case 12: // Baja Autogestion
                //  log.info("Notificacion de baja. [" + notification.toString() + "]");
                response = processUnsubscriptionNotification(notification, provider);
                break;
            case 5: // Enviado
            case 6: // No enviado
            case 7: // Recibido
                //  log.info("Notification no procesada. [" + notification.getIdCTR() + "]");
                return false;
            case 10: // No cobrado
                //  log.info("Notification sin cobro. No procesada. [" + notification.getIdCTR() + "]");
                // response = processSubscriptionNotification(notification, provider,MessageType.SUBSCRIPTION_PENDING);
                return false;
            case 8: // Cobrado
            case 9: // Cobrado con avance
                //   log.info("Notificacion de cobro. [" + notification.getIdEvento() + "]");
                response = processBillingNotification(notification, provider);
                break;
            default:
                log.fatal("IdEvento de la notificacion no tenido en cuenta. IdEvento: [" + notification.getIdEvento() + "]");
                break;
        }
        return response;
    }

    private String getOrigen(Integer idOrigen, Integer idEvento) {
        String or = null;
        if (idOrigen != null) {
            switch (idOrigen) {
                case 0:
                    switch (idEvento) {
                        case 2:
                            or = "SMS";
                            break;
                        case 3:
                            or = "CRM";
                            break;
                        case 4:
                            or = "PROCESO/RECICLAJE";
                            break;
                        case 11:
                            or = "MIGRACION";
                            break;
                        case 12:
                            or = "AUTOGESTION";
                            break;
                        case 13:
                            or = "TPR";
                            break;
                    }
                    break;
                case 1:
                    or = "Desconocido";
                    break;
                case 2:
                    or = "SMS";
                    break;
                case 3:
                    or = "WAP";
                    break;
                case 4:
                    or = "WEB";
                    break;
                case 5:
                    or = "IVR";
                    break;
                case 6:
                    or = "SIM";
                    break;
                case 7:
                    or = "AUTOGESTION";
                    break;
                case 8:
                    or = "SATPUSH";
                    break;
                case 9:
                    or = "WAP.Semidirecta";
                    break;
                case 10:
                    or = "WAP.Deck";
                    break;
                case 11:
                    or = "WAPNT_2.0";
                    break;
                case 12:
                    or = "WAPNT_2.0_ONPORTAL";
                    break;
                case 13:
                    or = "TPR";
                    break;
                case 18:
                    or = "SMSC_ADWORDS";
                    break;
            }
        } else {
            switch (idEvento) {
                case 2:
                    or = "SMS";
                    break;
                case 3:
                    or = "CRM";
                    break;
                case 4:
                    or = "PROCESO/RECICLAJE";
                    break;
                case 11:
                    or = "MIGRACION";
                    break;
                case 12:
                    or = "AUTOGESTION";
                    break;
                case 13:
                    or = "TPR";
                    break;
            }
        }
        if (or == null) {
            or = "DESC:" + idEvento + (idOrigen == null ? "" : "_" + idOrigen);
        }
        return or;
    }

    private Boolean processSubscriptionNotification(CTR notification, Provider provider, MessageType messageType) {

        Map<String, String> settings = new HashMap();
        settings.put("smsc_serviceId." + notification.getIdServicio(), notification.getIdServicio().toString());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        if (sop == null) {
            log.error("No existe un SOP configurado para smsc_serviceId: [" + notification.getIdServicio() + "]");
            return null;
        }

        //JsonNode jn = null;
        String adTracking = null;
        if (notification.getXInfo() != null && notification.getXInfo().contains("CID:")) {
            try {
                String aux[] = notification.getXInfo().split("\\|");
                for (String c : aux) {
                    if (c.startsWith("CID:")) {
                        adTracking = aux[1].replace("CID:", "");
                        break;
                    }
                }
            } catch (Exception ex) {
                log.warn("No fue posible parsear xInfo para: [" + notification + "]. " + ex);
            }
        }

        Tariff t = sop.getMainTariff();
        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(locateDate("yyyy-MM-dd HH:mm:ss", notification.getFecha(), TimeZone.getTimeZone("America/Buenos_Aires")));
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(messageType);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getIdSuscripcion());
            msg.setTransactionId(notification.getIdCTR());
            msg.setChannelIn(getOrigen(notification.getIdOrigen(), notification.getIdEvento()));
            msg.setUserAccount(notification.getMsisdn());
            Extra extra = new Extra();
            extra.serviceId = notification.getIdServicio().toString();
            msg.setExtra(extra);

            if (adTracking != null) {
                try {
                    FacadeRequest fr = facadeCache.getFacadeRequest(null, adTracking, null, 0L);
                    if (fr != null) {
                        //  log.info("Encontrado un FacadeRequest: [" + fr + "]");
                        LinkedHashMap<String, Object> adn = (LinkedHashMap<String, Object>) fr.adTracking;
                        for (String s : adn.keySet()) {
                            msg.getTrackingParams().put(s, adn.get(s));
                        }
                    }
                } catch (Exception ex) {
                    log.error("Error al procesar adTracking. " + ex, ex);
                }
            }
            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar SMSCNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    private Boolean processUnsubscriptionNotification(CTR notification, Provider provider) {

        Map<String, String> settings = new HashMap();
        settings.put("smsc_serviceId." + notification.getIdServicio(), notification.getIdServicio().toString());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        if (sop == null) {
            log.error("No existe un SOP configurado para smsc_serviceId: [" + notification.getIdServicio() + "]");
            return null;
        }

        Tariff t = sop.getMainTariff();
        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setToDate(locateDate("yyyy-MM-dd HH:mm:ss", notification.getFecha(), TimeZone.getTimeZone("America/Buenos_Aires")));
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getIdCTR());
            msg.setTransactionId(notification.getIdSuscripcion());
            msg.setChannelOut(getOrigen(notification.getIdOrigen(), notification.getIdEvento()));
            msg.setUserAccount(notification.getMsisdn());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar CDAGNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private Boolean processBillingNotification(CTR notification, Provider provider) {
        Map<String, String> settings = new HashMap();
        settings.put("smsc_serviceId." + notification.getIdServicio(), notification.getIdServicio().toString());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        if (sop == null) {
            log.error("No existe un SOP configurado para smsc_serviceId: [" + notification.getIdServicio() + "]");
            return null;
        }

        BigDecimal fullAmount;
        BigDecimal netAmount = notification.getPrecio();

        if (netAmount == null || netAmount.compareTo(BigDecimal.ZERO) < 1) {
            // log.info("Billing Notification. No procesada. netAmount: [" + netAmount + "]");
            return false;
        }
        netAmount = netAmount.divide(BigDecimal.valueOf(100));
        Tariff t = sop.findTariffByNetAmount(netAmount);
        if (t == null) {
            //Verificamos si es una tarifa principal nueva
            t = updateMainTariff(sop, null, netAmount);
            if (t == null) {
                //Creamos o actualizamos la tarifa hija
                t = updateNetAmountChildrenTariff(sop, netAmount, notification.getIdServicio().toString());
            }
        }

        if (t == null) {
            log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
            return null;
        }

        fullAmount = t.getFullAmount();
        netAmount = t.getNetAmount();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(locateDate("yyyy-MM-dd HH:mm:ss", notification.getFecha(), TimeZone.getTimeZone("America/Buenos_Aires")));
            msg.setChargedDate(msg.getFromDate());
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setSopId(sop.getId());
            msg.setTransactionId(notification.getIdCTR());
            msg.setChannelIn(null);
            msg.setUserAccount(notification.getMsisdn());
            msg.setExternalId(notification.getIdSuscripcion());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar CDAGNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {
        throw new Exception("No implementado");
    }

    @Override
    public void syncSubscriptions(final Provider provider) {
        try {
            new Thread(() -> {
                try {
                    Thread.currentThread().setName(CommonFunction.getTID("sync.full.T." + provider.getName()));
                    RLock lock = instrumentedObject.lockObject("SMSC_SYNC_" + provider.getName(), 60);
                    long aux = instrumentedObject.getAtomicLong("SMSC_SYNC_A_" + provider.getName(), 1400);

                    if (aux == 0) {
                        instrumentedObject.getAndIncrementAtomicLong("SMSC_SYNC_A_" + provider.getName(), 1400);
                        instrumentedObject.getAndIncrementAtomicLong("SMSC_SYNC_A_" + provider.getName(), 1400);
                        try {
                            syncSMSCSubscriptions(provider);
                        } catch (Exception ex) {
                            log.error("Error al sincronizar la DB local con la DB de SMSConsulting. " + ex, ex);
                        }
                    }
                    instrumentedObject.unLockObject(lock);
                } catch (Exception ex) {
                }
            }).start();
        } catch (Exception ex) {
        }

        super.syncSubscriptions(provider);
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        try {
            String result = smscResources.subscriptionStatus(subscription.getUserAccount(), subscription.getSop());
            if (result != null && result.length() > 2) {
                subscription.setStatus(Subscription.Status.ACTIVE);
                rm.status = ResponseMessage.Status.ACTIVE;

                if (subscription.getSubscriptionRegistry() == null || subscription.getSubscriptionRegistry().getUnsubscriptionDate() != null) {
                    subscription.setSubscriptionRegistry(new SubscriptionRegistry());
                    subscription.getSubscriptionRegistry().setUserAccount(subscription.getUserAccount());
                    subscription.getSubscriptionRegistry().setSop(subscription.getSop());
                    subscription.setSubscriptionDate(new Date());
                    subscription.getSubscriptionRegistry().setSubscriptionDate(subscription.getSubscriptionDate());
                    subscription.getSubscriptionRegistry().setOriginSubscription("FACADE:STATUS");
                }

                if (subscription.getSubscriptionRegistry().getExtra() == null) {
                    subscription.getSubscriptionRegistry().setExtra(new Extra());
                }
                subscription.getSubscriptionRegistry().getExtra().serviceId = result;
            } else {
                if ("1".equals(result)) {
                    subscription.setStatus(Subscription.Status.PORTING);
                    rm.status = ResponseMessage.Status.PORTING;
                } else {
                    if (Subscription.Status.ACTIVE.equals(subscription.getStatus())) {
                        subscription.setStatus(Subscription.Status.CANCELLED);
                    } else {
                        subscription.setStatus(Subscription.Status.REMOVED);
                    }
                    rm.status = ResponseMessage.Status.REMOVED;
                }
            }
        } catch (Exception ex) {
            log.error("Error subscriptionStatus. msisdn: [" + subscription.getUserAccount() + "] - SOP: [" + subscription.getSop() + "]. " + ex, ex);
            rm.message = ex.getMessage();
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        return rm;
    }

    public ResponseMessage sendPin(Subscription subscription) throws Exception {
        throw new Exception("No implementado");
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        throw new Exception("No implementado");
    }

    public ResponseMessage validatePin(Subscription subscription, String pin) throws Exception {
        throw new Exception("No implementado");
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    private void syncSMSCSubscriptions(Provider p) {
        log.info("Sincronizacion de suscripciones con SMSConsulting para provider: [" + p.getName() + "]");

        List<SOP> sops = sopService.getMatchByProviderName(p.getName());

        if (Collections.isEmpty(sops)) {
            log.error("No hay SOPs consigurados para provider: [" + p.getName() + "]");
            return;
        }
        for (SOP sop : sops) {
            try {
                ActivosProductoResponse ap = smscResources.subscriptionSync(sop);
                if (ap != null) {
                    log.info("Suscripciones encontradas para el SOP: [" + sop.getId() + "] --> [" + ap.totalActivos + "]");
                    if (ap.listado == null || ap.listado.isEmpty()) {
                        return;
                    }
                    for (ValueResponse vr : ap.listado) {
                        try {
                            vr.msisdn = userAccountNormalizer(vr.msisdn, p);
                            Subscription s = subscriptionManager.getSubscriptionByUserAccount(vr.msisdn, sop);
                            if (s == null || !Subscription.Status.ACTIVE.equals(s.getStatus())) {
                                subscriptionManager.getSubscriptionStatusFacadeRequest(vr.msisdn, sop);
                            }
                        } catch (Exception ex) {
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("Error al sincronizar con SMSC el SOP: [" + sop + "]. " + ex, ex);
            }
        }
    }

}
