/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.etl.procesor;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.commons.stats.model.StatsMessage;
import com.itbaf.platform.dw.etl.repository.BasicDataBaseClient;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PGJContentStatsProcessor extends StatsProcessor {

    private static final ElasticData.ElasticType elasticType = ElasticData.ElasticType.pgj_content;
    private final BasicDataBaseClient baseClient;

    @Inject
    public PGJContentStatsProcessor(final BasicDataBaseClient baseClient) {
        this.baseClient = baseClient;
    }

    @Override
    public Boolean process(String msg) throws Exception {
        StatsMessage sm = mapper.readValue(msg, StatsMessage.class);
        return baseClient.saveOrUpdatePGJContent(sm);
    }

    @Override
    public ElasticData.ElasticType getElasticType() {
        return elasticType;
    }

}
