/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.model.Notification;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPANotificationRepository;
import com.itbaf.platform.paymenthub.services.repository.NotificationService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import org.apache.commons.lang3.Validate;


/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationServiceTransactional implements NotificationService {

    private final JPANotificationRepository notificationRepository;
    private final SopService sopService;

    @Inject
    public NotificationServiceTransactional(final JPANotificationRepository notificationRepository,
            final SopService sopService) {
        this.notificationRepository = Validate.notNull(notificationRepository, "A JPANotificationRepository class must be provided");
        this.sopService = Validate.notNull(sopService, "A SopService class must be provided");

    }

    @Override
    public Notification saveNotification(Notification notification) {
        try {
            SOP sop;
            if (notification.getSop().getId() == null) {
                sop = sopService.findOrCreateByNames(notification.getSop().getServiceMatcher().getName(),
                        notification.getSop().getOperator().getName(), notification.getSop().getProvider().getName());
            } else {
                sop = sopService.findById(notification.getSop().getId());
            }
            if (sop != null) {
                notification.setSop(sop);
            } else {
                log.error("Hubo un error al procesar el SOP");
            }
            notificationRepository.save(notification);
        } catch (Exception ex) {
            log.error("Error al registrar la notificacion: [" + notification + "]. " + ex, ex);
        }
        return notification;
    }

}
