/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.billing.BillerAttempt;
import com.itbaf.platform.paymenthub.model.billing.QBillerAttempt;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.QSubscription;
import com.itbaf.platform.paymenthub.model.subscription.QSubscriptionRegistry;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.querydsl.jpa.impl.JPAQuery;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPABillerAttemptRepository extends AbstractJPARepositorySession<BillerAttempt, Long> {

    @Inject
    public JPABillerAttemptRepository(final Provider<EntityManager> entityManagerProvider) {
        super(BillerAttempt.class, entityManagerProvider);
    }

    public List<BillerAttempt> getNextAttempts(com.itbaf.platform.paymenthub.model.Provider p) {
        QBillerAttempt billerAttempt = QBillerAttempt.billerAttempt;
        QSubscriptionRegistry subscriptionRegistry = QSubscriptionRegistry.subscriptionRegistry;
        QSubscription subscription = QSubscription.subscription;

        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<BillerAttempt> sla = query.select(billerAttempt).from(billerAttempt)
                .innerJoin(subscriptionRegistry).on(subscriptionRegistry.id.eq(billerAttempt.id))
                .innerJoin(subscription).on(subscription.subscriptionRegistry().id.eq(subscriptionRegistry.id))
                .where(ObjectPredicates.combine(billerAttempt.sop().provider().id.eq(p.getId()),
                        billerAttempt.lastAttempt.before(new Date())),
                        subscription.status.eq(Subscription.Status.ACTIVE))
                .orderBy(billerAttempt.nextAttempt.asc())
                .limit(1000)
                .fetch();
        return sla;
    }

}
