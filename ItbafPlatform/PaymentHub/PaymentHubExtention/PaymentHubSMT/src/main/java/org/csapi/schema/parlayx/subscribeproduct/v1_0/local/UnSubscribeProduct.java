
package org.csapi.schema.parlayx.subscribeproduct.v1_0.local;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.NamedParameterList;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.SubInfoType;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.UserID;


/**
 * <p>Java class for unSubscribeProduct complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="unSubscribeProduct">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userID" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}UserID"/>
 *         &lt;element name="subInfo" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}SubInfoType"/>
 *         &lt;element name="channelID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="extensionInfo" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}NamedParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "unSubscribeProduct", propOrder = {
    "version",
    "userID",
    "subInfo",
    "channelID",
    "extensionInfo"
})
public class UnSubscribeProduct {

    @XmlElement(required = true)
    protected String version;
    @XmlElement(required = true)
    protected UserID userID;
    @XmlElement(required = true)
    protected SubInfoType subInfo;
    protected int channelID;
    protected NamedParameterList extensionInfo;

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link UserID }
     *     
     */
    public UserID getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserID }
     *     
     */
    public void setUserID(UserID value) {
        this.userID = value;
    }

    /**
     * Gets the value of the subInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SubInfoType }
     *     
     */
    public SubInfoType getSubInfo() {
        return subInfo;
    }

    /**
     * Sets the value of the subInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubInfoType }
     *     
     */
    public void setSubInfo(SubInfoType value) {
        this.subInfo = value;
    }

    /**
     * Gets the value of the channelID property.
     * 
     */
    public int getChannelID() {
        return channelID;
    }

    /**
     * Sets the value of the channelID property.
     * 
     */
    public void setChannelID(int value) {
        this.channelID = value;
    }

    /**
     * Gets the value of the extensionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link NamedParameterList }
     *     
     */
    public NamedParameterList getExtensionInfo() {
        return extensionInfo;
    }

    /**
     * Sets the value of the extensionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NamedParameterList }
     *     
     */
    public void setExtensionInfo(NamedParameterList value) {
        this.extensionInfo = value;
    }

}
