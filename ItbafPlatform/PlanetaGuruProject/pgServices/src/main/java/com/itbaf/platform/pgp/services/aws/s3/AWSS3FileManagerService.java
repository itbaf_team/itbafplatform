package com.itbaf.platform.pgp.services.aws.s3;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import com.itbaf.platform.pgp.services.aws.s3.model.FileProperties;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@lombok.extern.log4j.Log4j2
public class AWSS3FileManagerService extends ServiceProcessorHandler implements AWSFileManagerService {

    private final static String AWS_BUCKET_NAME = "aws.s3.pg.assets.bucket.name";
    private final static String AWS_ACCESS_KEY = "aws.s3.pg.assets.access.key.id";
    private final static String AWS_SECRET_ACCESS = "aws.s3.pg.assets.access.key.secret";
    private static final Cache<String, AmazonS3> awsS3Cache = CacheBuilder.newBuilder()
            .expireAfterWrite(1, TimeUnit.HOURS)
            .build();

    private AmazonS3 getAmazonS3Client() {
        return getAmazonS3Client(0);
    }

    private AmazonS3 getAmazonS3Client(int cont) {
        AmazonS3 s3 = null;
        try {
            awsS3Cache.cleanUp();
        } catch (Exception ex) {
        }
        try {
            if (cont > 2) {
                awsS3Cache.invalidateAll();
            }

            if (awsS3Cache.asMap().isEmpty()) {
              //  log.info("Creando una conexion a AWS S3...");
                s3 = AmazonS3ClientBuilder.standard()
                        .withRegion(Regions.SA_EAST_1)
                        .withCredentials(new AWSStaticCredentialsProvider(
                                new BasicAWSCredentials(genericPropertyService.getCommonProperty(AWS_ACCESS_KEY),
                                        genericPropertyService.getCommonProperty(AWS_SECRET_ACCESS))))
                        .build();
            } else {
                synchronized (awsS3Cache) {
                    try {
                        String key = awsS3Cache.asMap().keySet().iterator().next();
                        s3 = awsS3Cache.getIfPresent(key);
                        awsS3Cache.invalidate(key);
                        if (s3 == null) {
                            awsS3Cache.invalidateAll();
                        }
                    } catch (Exception ex) {
                    }
                }
            }
            s3.getRegion();
        } catch (Exception ex) {
            log.error("Error al obtener una session S3 en AWS. " + ex, ex);
            awsS3Cache.invalidateAll();
            s3 = null;
        }

        if (s3 == null && cont < 8) {
            cont++;
            s3 = getAmazonS3Client(cont);
        }

        return s3;
    }

    private void putAmazonS3Client(AmazonS3 s3) {
        try {
            s3.getRegion();
            awsS3Cache.put(UUID.randomUUID() + "-" + System.currentTimeMillis(), s3);
        } catch (Exception ex) {
        }

    }

    @Override
    public boolean uploadFile(String path, FileProperties fileProperties, InputStream inputStream) {

        AmazonS3 s3 = getAmazonS3Client();
        try {
            String bucketName = genericPropertyService.getCommonProperty(AWS_BUCKET_NAME);
            String key = path + fileProperties.name;
            ObjectMetadata om = new ObjectMetadata();
            om.setContentType(fileProperties.type);
            om.setContentLength(Long.parseLong(fileProperties.size));
            om.setCacheControl("max-age=2592000");

            s3.putObject(new PutObjectRequest(bucketName, key, inputStream, om));
            putAmazonS3Client(s3);
            return true;
        } catch (Exception ex) {
            log.error("Error al subir archivo: [" + path + "][" + fileProperties + "]. " + ex, ex);
        }

        return false;
    }

}
