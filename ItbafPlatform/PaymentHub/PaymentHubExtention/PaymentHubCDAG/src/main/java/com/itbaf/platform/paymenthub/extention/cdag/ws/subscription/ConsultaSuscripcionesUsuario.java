
package com.itbaf.platform.paymenthub.extention.cdag.ws.subscription;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="proveedorId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroCorto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="categorias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="incluirBajas" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="credentials" type="{urn:SendatelSoaService}securityInfo"/>
 *         &lt;element name="params" type="{urn:SendatelSoaService}arrayOfExtraParam"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "msisdn",
    "proveedorId",
    "numeroCorto",
    "categorias",
    "incluirBajas",
    "credentials",
    "params"
})
@XmlRootElement(name = "ConsultaSuscripcionesUsuario", namespace = "urn:SendatelSuscripcionesService")
public class ConsultaSuscripcionesUsuario {

    protected long msisdn;
    protected int proveedorId;
    protected String numeroCorto;
    protected String categorias;
    protected Boolean incluirBajas;
    @XmlElement(required = true)
    protected SecurityInfo credentials;
    @XmlElement(required = true)
    protected ArrayOfExtraParam params;

    /**
     * Gets the value of the msisdn property.
     * 
     */
    public long getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     */
    public void setMsisdn(long value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the proveedorId property.
     * 
     */
    public int getProveedorId() {
        return proveedorId;
    }

    /**
     * Sets the value of the proveedorId property.
     * 
     */
    public void setProveedorId(int value) {
        this.proveedorId = value;
    }

    /**
     * Gets the value of the numeroCorto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCorto() {
        return numeroCorto;
    }

    /**
     * Sets the value of the numeroCorto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCorto(String value) {
        this.numeroCorto = value;
    }

    /**
     * Gets the value of the categorias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategorias() {
        return categorias;
    }

    /**
     * Sets the value of the categorias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategorias(String value) {
        this.categorias = value;
    }

    /**
     * Gets the value of the incluirBajas property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncluirBajas() {
        return incluirBajas;
    }

    /**
     * Sets the value of the incluirBajas property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncluirBajas(Boolean value) {
        this.incluirBajas = value;
    }

    /**
     * Gets the value of the credentials property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityInfo }
     *     
     */
    public SecurityInfo getCredentials() {
        return credentials;
    }

    /**
     * Sets the value of the credentials property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityInfo }
     *     
     */
    public void setCredentials(SecurityInfo value) {
        this.credentials = value;
    }

    /**
     * Gets the value of the params property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public ArrayOfExtraParam getParams() {
        return params;
    }

    /**
     * Sets the value of the params property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public void setParams(ArrayOfExtraParam value) {
        this.params = value;
    }

}
