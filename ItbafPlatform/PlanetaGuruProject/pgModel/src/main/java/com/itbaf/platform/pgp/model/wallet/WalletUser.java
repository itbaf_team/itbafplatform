/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.wallet;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.pgp.model.BaseEntity;
import com.itbaf.platform.pgp.model.wallet.properties.Credit;
import com.querydsl.core.annotations.Config;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

/**
 *
 * @author JF
 */
@Entity(name = "W_USER")
@Table(name = "W_USER")
@Cacheable(true)
@Cache(region = "oauth", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class WalletUser extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 20171226170201L;

    @Id
    @Column(name = "USER_ID", unique = true, nullable = false)
    private Long id;

    @Type(type = "json")
    @Column(name = "CREDITS", columnDefinition = "json")
    /**
     * <CLIENT_clientId,Credit>
     * <SOP_sopId,Credit>
     */
    private Map<String, Credit> credits;

    public WalletUser() {
        credits = new HashMap();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("credits", credits)
                .omitNullValues().toString();
    }
}
