/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Language;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import com.itbaf.platform.pgp.services.aws.s3.AWSFileManagerService;
import com.itbaf.platform.pgp.services.aws.s3.model.FileProperties;
import com.sun.jersey.core.header.FormDataContentDisposition;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ProfileViewServiceProcessorHandler extends ServiceProcessorHandler {

    private final AWSFileManagerService awsFileManagerService;
    private final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler;

    @Inject
    public ProfileViewServiceProcessorHandler(final AWSFileManagerService awsFileManagerService,
            final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler) {
        this.awsFileManagerService = Validate.notNull(awsFileManagerService, "An AWSFileManagerService class must be provided");
        this.apiProfileServiceProcessorHandler = Validate.notNull(apiProfileServiceProcessorHandler, "An ApiProfileServiceProcessorHandler class must be provided");
    }

    public String getUserProfileHtml(User client, User user, String productId, String redirectUri) throws Exception {
        String html = readResource("/tpl/html/account/profile.html", client, productId);
        html = html.replace("<siteHeaderScript/>", readHeaderResource("/tpl/html/header.html", client, user, productId));
        html = html.replace("<siteFooterScript/>", readResource("/tpl/html/footer.html", client, productId));

        UserProfile up = apiProfileServiceProcessorHandler.getUserProfileFromUser(client, user, null, false, false, false);
        if (up.getPhones() != null && !up.getPhones().isEmpty()) {
            Set<Msisdn> aux = new HashSet();
            for (Msisdn m : up.getPhones()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setPhones(aux);
        }
        if (up.getEmails() != null && !up.getEmails().isEmpty()) {
            Set<Email> aux = new HashSet();
            for (Email m : up.getEmails()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setEmails(aux);
        }

        //Lista de productos del menu
        html = getMenuProducts(html, productId, redirectUri);

        html = html.replace("<years/>", getYears());

        List<Country> countries = countryService.getByStatus(null);
        String countryList = "";
        String languageList = "";
        Set<LanguageAux> languages = new TreeSet();

        for (Country c : countries) {
            countryList = countryList + "<option value=\"" + c.getCode() + "\">" + (c.getName().length() == c.getNativeName().length() && !c.getNativeView() ? c.getNativeName() : c.getName() + " (" + c.getNativeName() + ")") + "</option>";
            try {
                for (Language l : c.getLanguages()) {
                    LanguageAux la = new LanguageAux();
                    la.order = l.getName() + "_" + c.getCode().toUpperCase();
                    la.value = l.getCode().toLowerCase() + "-" + c.getCode().toUpperCase();
                    la.code = "<option value=\"" + la.value + "\">" + l.getName() + " (" + (c.getName().length() == c.getNativeName().length() && !c.getNativeView() ? c.getNativeName() : c.getName()) + ")</option>";
                    languages.add(la);
                }
            } catch (Exception ex) {
            }
        }

        for (LanguageAux la : languages) {
            languageList = languageList + la.code;
        }

        html = html.replace("<languageList/>", languageList);
        html = html.replace("<countryList></countryList>", countryList);

        html = html.replace("uparams = {}", "uparams = " + mapper.writeValueAsString(up));
        return html;
    }

    private class LanguageAux implements Comparable<Object> {

        public String order;
        public String value;
        public String code;

        @Override
        public int compareTo(Object o) {
            LanguageAux aux = (LanguageAux) o;
            return order.compareTo(aux.order);
        }
    }

    public String saveOrUpdateProfile(String path, User user, UserProfile up) {
        switch (path.toLowerCase()) {
            case "name":
                if (up.getFirstName() != null) {
                    up.setFirstName(up.getFirstName().trim());
                }
                if (up.getMiddleName() != null) {
                    up.setMiddleName(up.getMiddleName().trim());
                }
                if (up.getLastName() != null) {
                    up.setLastName(up.getLastName().trim());
                }
                user.getProfileUserProperty().setFirstName(up.getFirstName());
                user.getProfileUserProperty().setMiddleName(up.getMiddleName());
                user.getProfileUserProperty().setLastName(up.getLastName());
                try {
                    userService.saveOrUpdate(user);
                } catch (Exception ex) {
                }
                break;
            case "nickname":
                if (up.getNickname() != null && !user.getCredentialUserProperty().getUsername().getNicknameEdited()) {
                    up.setNickname(up.getNickname().replace("'", "").replace(".", "").replace("\"", "").replace("_", ""));
                    if (up.getNickname().length() > 5 && userService.findByNickname(up.getNickname()) == null) {
                        user.getCredentialUserProperty().getUsername().setNickname(up.getNickname());
                        user.getCredentialUserProperty().getUsername().setNicknameEdited(true);
                        try {
                            userService.saveOrUpdate(user);
                        } catch (Exception ex) {
                        }
                    } else {
                        return "nickname:not_available";
                    }
                }
                break;
            case "personal":
                if (up.getBirthdate() != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        Date d = sdf.parse(up.getBirthdate().year + "-" + up.getBirthdate().month + "-" + up.getBirthdate().day + " 12:00:00");

                        Calendar cs = Calendar.getInstance();
                        cs.add(Calendar.YEAR, -103);
                        Calendar ce = Calendar.getInstance();
                        cs.add(Calendar.YEAR, -3);
                        if (cs.getTime().before(d) && ce.getTime().after(d)) {
                            user.getProfileUserProperty().setBirthdate(d);
                        }
                    } catch (ParseException ex) {
                    }
                }

                if (up.getGender() != null) {
                    user.getProfileUserProperty().setGender(up.getGender());
                }

                if (up.getCountry() != null && up.getCountry().getCode() != null
                        && !up.getCountry().getCode().equalsIgnoreCase(user.getProfileUserProperty().getCountry().getCode())) {
                    Country c = countryService.findByCode(up.getCountry().getCode());
                    if (c != null) {
                        user.getProfileUserProperty().setCountry(c);
                    }
                }

                if (up.getLanguage() != null && up.getLanguage().contains("-")) {
                    try {
                        String aux[] = up.getLanguage().split("-");
                        Country c = countryService.findByCode(aux[1]);
                        for (Language l : c.getLanguages()) {
                            if (aux[0].equalsIgnoreCase(l.getCode())) {
                                user.getProfileUserProperty().setLanguage(up.getLanguage());
                                break;
                            }
                        }
                    } catch (Exception ex) {
                    }
                }

                try {
                    userService.saveOrUpdate(user);
                } catch (Exception ex) {
                }

                break;
        }
        return null;
    }

    public String saveImagen(String path, User user, InputStream inputStream, FormDataContentDisposition fileDetail, FileProperties fp) {
        String aux[] = fileDetail.getFileName().split("\\.");
        String uploadedFileLocation = user.getId() + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "_" + generateCode(15);
        if (aux.length == 0) {
            uploadedFileLocation = uploadedFileLocation + fileDetail.getFileName();
        } else {
            uploadedFileLocation = uploadedFileLocation + "." + aux[aux.length - 1];
        }
        fp.name = uploadedFileLocation;

        String pathAux = "users/pg/" + path + "/";

        if (awsFileManagerService.uploadFile(pathAux, fp, inputStream)) {
            String urlS3 = genericPropertyService.getCommonProperty("pg.assets.aws.s3") + pathAux + fp.name;

            switch (path.toLowerCase()) {
                case "picture":
                    user.getProfileUserProperty().setPhotoURL(urlS3);
                    user.getProfileUserProperty().setUserPhoto(true);
                    break;
                case "avatar":
                    user.getCredentialUserProperty().getUsername().setAvatarURL(urlS3);
                    break;
                default:
                    return null;
            }
            try {
                userService.saveOrUpdate(user);
                return fp.name;
            } catch (Exception ex) {
            }

        }

        return null;
    }

    public String getNicknameStatus(User user, UserProfile up) {
        if (up.getNickname() != null && !user.getCredentialUserProperty().getUsername().getNicknameEdited() && up.getNickname().length() > 5) {
            up.setNickname(up.getNickname().replace("'", "").replace(".", "").replace("\"", "").replace("_", ""));
            if (up.getNickname().length() > 5) {
                user = userService.findByNickname(up.getNickname());
                if (user == null) {
                    return "available";
                }
                return "not_available";
            }
        }
        return null;
    }


}
