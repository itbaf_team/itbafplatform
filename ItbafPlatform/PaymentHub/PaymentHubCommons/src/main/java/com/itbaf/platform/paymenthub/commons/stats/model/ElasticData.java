/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.stats.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.base.MoreObjects;
import java.io.Serializable;

/**
 *
 * @author JF
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "elasticType")
@JsonSubTypes({
    @JsonSubTypes.Type(value = AdnetworkNotifierSimpleElastic.class, name = "adnetwork_notifier_simple"),
    @JsonSubTypes.Type(value = SubscriptionRegistryElastic.class, name = "subscription_registry"),
    @JsonSubTypes.Type(value = SubscriptionBillingElastic.class, name = "subscription_billing"),
    @JsonSubTypes.Type(value = BaseResumenElastic.class, name = "base_resumen"),
    @JsonSubTypes.Type(value = PGJContentElastic.class, name = "pgj_content"),
    @JsonSubTypes.Type(value = PGJContentElastic.class, name = "query"),
    @JsonSubTypes.Type(value = OauthUserSubscriptionElastic.class, name = "oauth_user_subscription")
})
public abstract class ElasticData implements Serializable {

    private static final long serialVersionUID = 20181029094801L;

    /**
     * [Elasticsearch exception [type=invalid_index_name_exception,
     * reason=Invalid index name [SUBSCRIPTION_BILLING], must be lowercase]
     */
    public enum ElasticType {
        subscription_registry,
        subscription_billing,
        ondemand_registry,
        ondemand_billing,
        adnetwork_notifier_simple,
        base_resumen,
        pgj_content,
        query,
        oauth_user_subscription
    }
    @Deprecated
    public String elasticId;
    public String id;
    public final ElasticType elasticType;

    public ElasticData(ElasticType elasticType) {
        this.elasticType = elasticType;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("elasticType", elasticType)
                .add("elasticId", elasticId)
                .add("id", id)
                .omitNullValues().toString();
    }
}
