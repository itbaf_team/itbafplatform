/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.smt.handler;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.bn.BNProcessor;
import com.itbaf.platform.paymenthub.commons.bn.model.BorderNotificationMessage;
import com.itbaf.platform.paymenthub.commons.bn.model.MDivulgaNotification;
import com.itbaf.platform.paymenthub.extention.smt.soap.NotificationSoapService;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class MDivulgaProcessorHandler extends BNProcessor {

    @Inject
    private ServiceProcessorHandler serviceProcessorHandler;

    @Override
    public Boolean processMessage(String message) {
        try {
            BorderNotificationMessage bm = mapper.readValue(message, BorderNotificationMessage.class);
            return serviceProcessorHandler.mDivulgaNotificationProcess(bm.countryCode, (MDivulgaNotification) bm.notification);
        } catch (Exception ex) {
            log.error("No fue posible interpretar la notificacion: [" + message + "]. " + ex, ex);
        }

        return null;
    }

}
