/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.smt.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.paymenthub.extention.smt.resources.SMTResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.mdivulga.model.MDivulgaResponse;
import com.itbaf.platform.paymenthub.commons.mdivulga.model.BodyResponse;
import com.itbaf.platform.paymenthub.commons.bn.model.MDivulgaNotification;
import com.itbaf.platform.paymenthub.commons.resources.FacadeCache;
import com.itbaf.platform.paymenthub.extention.smt.PaymentHubMain;
import com.itbaf.platform.paymenthub.extention.smt.resources.SMSMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import io.jsonwebtoken.lang.Collections;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;
import org.csapi.schema.parlayx.sms.notification.v2_2.local.NotifySmsDeliveryReceipt;
import org.csapi.schema.parlayx.sms.notification.v2_2.local.NotifySmsReception;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.SubScriptionInfoType;
import org.csapi.schema.parlayx.syncsubscription.v1_0.SyncOrderRelationship;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final FacadeCache facadeCache;
    private final SMTResources smtResources;
    private final Map<String, Provider> providerMap;
    private final SubscriptionService subscriptionService;
    private final OnDemandProcessorHandler onDemandProcessorHandler;
    private final PHProfilePropertiesService profilePropertiesService;

    @Inject
    public ServiceProcessorHandler(final FacadeCache facadeCache,
            final SMTResources smtResources,
            final Map<String, Provider> providerMap,
            final SubscriptionService subscriptionService,
            final OnDemandProcessorHandler onDemandProcessorHandler,
            final PHProfilePropertiesService profilePropertiesService) {
        this.facadeCache = Validate.notNull(facadeCache, "A FacadeCache class must be provided");
        this.smtResources = Validate.notNull(smtResources, "A SMTResources class must be provided");
        this.providerMap = Validate.notNull(providerMap, "A Map<String, Provider> class must be provided");
        this.subscriptionService = Validate.notNull(subscriptionService, "A SubscriptionService class must be provided");
        this.onDemandProcessorHandler = Validate.notNull(onDemandProcessorHandler, "An OnDemandProcessorHandler class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    public void processNotification(String countryCode, SyncOrderRelationship notification) {

        if (countryCode == null || countryCode.length() != 2) {
            log.error("El Codigo de pais no es valido. CountryCode: [" + countryCode + "]");
            return;
        }

        Provider p = providerMap.get(countryCode.toUpperCase());
        if (p == null) {
            log.error("No hay configurado un provider para CountryCode: [" + countryCode + "]");
            return;
        }

        notification.setCountryCode(countryCode);

        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notification);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notification);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {
        SyncOrderRelationship notification;
        try {
            notification = mapper.readValue(message, SyncOrderRelationship.class);
            Thread.currentThread().setName(notification.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }

        Provider provider = providerMap.get(notification.getCountryCode().toUpperCase());
        if (provider == null) {
            log.error("No hay configurado un provider para CountryCode: [" + notification.getCountryCode() + "]");
            return null;
        }
        final String threadName = Thread.currentThread().getName() + "-" + provider.getName() + ".n";
        Thread.currentThread().setName(threadName);

        PaymentHubMessage msg = null;
        switch (notification.getUpdateType()) {
            case 1: // add -> subscribe
                //  log.info("Notificacion de alta. MSISDN: [" + notification.getUserID().getID() + "] - ProductID: [" + notification.getProductID() + "] - ServiceID: [" + notification.getServiceID() + "]");
                msg = processSubscriptionNotification(notification, provider, true);
                msg = processBillingNotification(notification, msg, provider);
                break;
            case 2: // delete -> unsubscribe
                //  log.info("Notificacion de baja. MSISDN: [" + notification.getUserID().getID() + "] - ProductID: [" + notification.getProductID() + "] - ServiceID: [" + notification.getServiceID() + "]");
                msg = processUnsubscriptionNotification(notification, provider);
                break;
            case 3: // update -> billing
                //  log.info("Notificacion de renovacion. MSISDN: [" + notification.getUserID().getID() + "] - ProductID: [" + notification.getProductID() + "] - ServiceID: [" + notification.getServiceID() + "]");
                msg = processSubscriptionNotification(notification, provider, false);
                msg = processBillingNotification(notification, msg, provider);
                break;
            default:
                log.error("updateType inesperado: [" + notification.getUpdateType() + "]");
        }

        return msg == null ? null : true;
    }

    private PaymentHubMessage processSubscriptionNotification(SyncOrderRelationship notification, Provider provider, boolean isSubscription) {

        Map<String, String> settings = new HashMap();
        settings.put("productID", notification.getProductID());
        settings.put("serviceID", notification.getServiceID());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        if (sop == null) {
            log.error("No existe un SOP configurado para ProductID: [" + notification.getProductID() + "] - ServiceID: [" + notification.getServiceID() + "]");
            return null;
        }

        if (isSubscription) {
            String message = sop.getIntegrationSettings().get("message.subscription");
            if (message != null) {
                try {
                    String shortCode = sop.getIntegrationSettings().get("shortcode").trim();
                    smtResources.sendSms(shortCode, notification.getUserID().getID(), message, sop);
                } catch (Exception ex) {
                    log.error("Error al enviar SMS. [" + notification.getUserID().getID() + "]. " + ex, ex);
                }
            }
        }

        Tariff t = sop.getMainTariff();
        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(notification.getFrequencyType());
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(sdf.parse(notification.getEffectiveTime()));
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(MessageType.SUBSCRIPTION_ACTIVE);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getExtensionInfoMap().get("orderKey"));
            msg.setTransactionId(notification.getExtensionInfoMap().get("TraceUniqueID"));
            msg.setChannelIn(notification.getChannel());
            msg.setUserAccount(notification.getUserID().getID());

            String adTracking = null;
            try {
                adTracking = notification.getAdProvider();
                if (adTracking != null) {
                    Extra extra = new Extra();
                    extra.stringData.put("AdProvider", adTracking);
                    msg.setExtra(extra);
                    try {
                        FacadeRequest fr = facadeCache.getFacadeRequest(null, adTracking, null, 0L);
                        if (fr != null) {
                            log.info("Encontrado un FacadeRequest: [" + fr + "]");
                            LinkedHashMap<String, Object> adn = (LinkedHashMap<String, Object>) fr.adTracking;
                            msg.getTrackingParams().putAll(adn);
                        }
                    } catch (Exception ex) {
                        log.error("Error al procesar adTracking. " + ex, ex);
                    }
                }
            } catch (Exception ex) {
            }

            try {
                facadeCache.setTemporalData(sop.getProvider().getName() + "_" + adTracking, msg.getUserAccount(), 120);
            } catch (Exception ex) {
            }

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }

            return msg;
        } catch (Exception ex) {
            log.error("Error al procesar SMTNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    private PaymentHubMessage processBillingNotification(SyncOrderRelationship notification, PaymentHubMessage msg, Provider provider) {
        if (msg == null) {
            return msg;
        }

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String fee = notification.getExtensionInfoMap().get("fee");
        if (fee == null) {
            // log.info("No se procesara la notificacion de cobro con valor CERO");
            return msg;
        }

        // log.info("Procesando un cobro: [" + msg.getUserAccount() + "] - Amount: [" + fee + "] - SOP: [" + msg.getSopId() + "]");
        Map<String, String> settings = new HashMap();
        settings.put("productID", notification.getProductID());
        settings.put("serviceID", notification.getServiceID());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        if (sop == null) {
            log.error("No existe un SOP configurado para ProductID: [" + notification.getProductID() + "] - ServiceID: [" + notification.getServiceID() + "]");
            return null;
        }

        try {
            BigDecimal netAmount = new BigDecimal(fee);
            netAmount = netAmount.divide(new BigDecimal(profilePropertiesService.getCommonProperty(provider.getId(), "smt.amount.divisor")));

            Tariff t = sop.findTariffByNetAmount(netAmount);
            if (t == null) {
                //Verificamos si es una tarifa principal nueva
                t = updateMainTariff(sop, null, netAmount);
                if (t == null) {
                    //Creamos o actualizamos la tarifa hija
                    t = updateNetAmountChildrenTariff(sop, netAmount, notification.getServiceID());
                }
            }

            if (t == null) {
                log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
                return null;
            }

            if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }

            msg.setFullAmount(t.getFullAmount());
            msg.setNetAmount(t.getNetAmount());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setChargedDate(sdf.parse(notification.getCreatTime()));
            msg.setTariffId(t.getId());
            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
            }
            return msg;
        } catch (Exception ex) {
            log.error("Error al procesar notificacion de cobro SMTNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private PaymentHubMessage processUnsubscriptionNotification(SyncOrderRelationship notification, Provider provider) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Map<String, String> settings = new HashMap();
        settings.put("productID", notification.getProductID());
        settings.put("serviceID", notification.getServiceID());
        SOP sop = sopService.findBySettings(provider.getName(), settings);
        if (sop == null) {
            log.error("No existe un SOP configurado para ProductID: [" + notification.getProductID() + "] - ServiceID: [" + notification.getServiceID() + "]");
            return null;
        }

        Tariff t = sop.getMainTariff();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(notification.getFrequencyType());
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(sdf.parse(notification.getEffectiveTime()));
            msg.setToDate(sdf.parse(notification.getCreatTime()));
            msg.setFullAmount(t.getFullAmount());
            msg.setNetAmount(t.getNetAmount());
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getExtensionInfoMap().get("orderKey"));
            msg.setTransactionId(notification.getExtensionInfoMap().get("TraceUniqueID"));
            msg.setChannelOut(notification.getChannel());
            msg.setUserAccount(notification.getUserID().getID());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
            return msg;
        } catch (Exception ex) {
            log.error("Error al procesar SMTNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        SOP sop = subscription.getSop();
        String mode = sop.getIntegrationSettings().get("mode");

        //Alta efectiva se realizara en un sitio externo
        if ("WAP".equals(subscription.getSubscriptionRegistry().getChannelIn()) && "EXTERNAL".equals(mode)) {
            if (!Subscription.Status.ACTIVE.equals(subscription.getStatus())) {
                subscription.setStatus(Subscription.Status.PENDING);
            }
            rm.status = ResponseMessage.Status.OK_NS;
            rm.message = subscription.getSubscriptionRegistry().getChannelIn() + "_" + mode;
        } else {
            ResponseMessage aux = getSubscriptionStatus(subscription);
            switch (aux.status) {
                case ACTIVE:
                case PENDING:
                case BLACKLIST:
                    rm.status = aux.status;
                    rm.message = aux.message;
                    break;
                case ERROR:
                    rm.message = aux.message;
                    break;
                case REMOVED:
                    try {
                        int smtResponse = smtResources.subscribeProduct(subscription.getUserAccount(), sop, subscription.getSubscriptionRegistry().getChannelIn());

                        switch (smtResponse) {
                            case 0:
                                rm.status = ResponseMessage.Status.OK;
                                rm.message = "[" + smtResponse + "] Transaccion exitosa";
                                break;
                            case 7863:
                                rm.status = ResponseMessage.Status.OK_NS;
                                rm.message = "[" + smtResponse + "] En espera de confirmacion de doble optin por API";
                                break;
                            case 7865:
                                rm.status = ResponseMessage.Status.ERROR;
                                rm.message = "[" + smtResponse + "] Peticion enviada anteriormente y no se puede mandar otra hasta que el usuario responda";
                                break;
                            case 7201:
                                rm.status = ResponseMessage.Status.ACTIVE;
                                break;
                            case 7330:
                                rm.status = ResponseMessage.Status.RETENTION;
                                rm.message = "[" + smtResponse + "] Saldo insuficiente";
                                break;
                            case 7306:
                            case 7363:
                            case 7629:
                            case 7657:
                                rm.status = ResponseMessage.Status.BLACKLIST;
                                break;
                            default:
                                rm.status = ResponseMessage.Status.ERROR;
                                rm.message = "[" + smtResponse + "] Codigo de error en SMT.";
                        }
                    } catch (Exception ex) {
                        log.error("Error en SMT al intentar suscribir [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
                        rm.status = ResponseMessage.Status.ERROR;
                        rm.message = "Error en SMT. " + ex;
                    }
                    break;
            }
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {

        String msisdn = null;
        Provider provider = subscription.getSop().getProvider();
        ResponseMessage rm = new ResponseMessage();
        try {
            msisdn = userAccountNormalizer(subscription.getUserAccount(), provider);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        if (msisdn != null && subscription.getUserAccount() != null && subscription.getUserAccount().length() > 2) {

            Holder<Integer> resultCode = new Holder();
            Holder<List<SubScriptionInfoType>> subScriptionInfos = new Holder();

            try {
                String aux = profilePropertiesService.getCommonProperty(provider.getId(), "smt.subscription.list");
                if ("1".equals(aux)) {
                    smtResources.getSubscriptionList(msisdn, subscription.getSop(), resultCode, subScriptionInfos);
                }
            } catch (Exception ex) {
                log.error("Error al obtener la lista de suscripciones. ANI: [" + msisdn + "] - SOP: [" + subscription.getSop() + "]. " + ex, ex);
            }

            if (resultCode.value == null) {
                switch (subscription.getStatus()) {
                    case ACTIVE:
                        rm.status = ResponseMessage.Status.ACTIVE;
                        break;
                    case BLACKLIST:
                        rm.status = ResponseMessage.Status.BLACKLIST;
                        break;
                    default:
                        rm.status = ResponseMessage.Status.REMOVED;
                        break;
                }
                rm.message = "No es posible evaluar el estado del ani en el Operador. Valor en DB.";
            } else {
                log.fatal("Hey JF, aca hay que hacer la validacion de todo... XD");
            }
        } else {
            rm.message = "No es posible evaluar el estado del ani";
            try {
                rm.data = mapper.writeValueAsString(subscription);
            } catch (JsonProcessingException ex1) {
            }
            return rm;
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        SOP sop = subscription.getSop();
        Provider provider = sop.getProvider();
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        try {
            int smtResponse = smtResources.unsubscribeProduct(subscription.getUserAccount(), sop, subscription.getSubscriptionRegistry().getChannelOut());

            switch (smtResponse) {
                case 0:
                    rm.status = ResponseMessage.Status.OK;
                    rm.message = "[" + smtResponse + "] Transaccion exitosa";
                    break;
                case 7219:
                    rm.status = ResponseMessage.Status.OK;
                    rm.message = "[" + smtResponse + "] No se puede cancelar el servicio ya que el usuario no esta suscrito";
                    break;
                default:
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "[" + smtResponse + "] Codigo de error en SMT.";
            }
        } catch (Exception ex) {
            log.error("Error en SMT al realizar la baja [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Error en SMT. " + ex;
        }

        rm.data = mapper.writeValueAsString(subscription);

        return rm;
    }

    public ResponseMessage sendPin(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);
        switch (aux.status) {

            case PENDING:
            case BLACKLIST:
                rm.status = aux.status;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case ACTIVE:
            case REMOVED:
                SOP sop = subscription.getSop();
                try {
                    Holder<String> resultCode = new Holder();
                    Holder<String> resultDesc = new Holder();
                    smtResources.sendPin(subscription.getUserAccount(), sop, resultCode, resultDesc);
                    rm.message = "[" + resultCode.value + "][" + resultDesc.value + "]";
                    switch (Integer.parseInt(resultCode.value)) {
                        case 0:
                            rm.status = ResponseMessage.Status.OK_NS;
                            break;
                        case 12:
                            rm.status = ResponseMessage.Status.OK;
                            subscription.setStatus(Subscription.Status.PENDING);
                            break;
                        case 6:
                            rm.status = ResponseMessage.Status.ACTIVE;
                            subscription.setStatus(Subscription.Status.ACTIVE);
                            break;
                        case 7:
                        case 8:
                            rm.status = ResponseMessage.Status.PIN_ERROR;
                            break;
                        case 9:
                        case 10:
                        case 11:
                            rm.status = ResponseMessage.Status.BLACKLIST;
                            subscription.setStatus(Subscription.Status.BLACKLIST);
                            break;
                        default:
                            rm.status = ResponseMessage.Status.ERROR;
                    }
                } catch (Exception ex) {
                    log.error("Error en SMT al enviar PIN [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "Error en SMT. " + ex;
                }
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage validatePin(Subscription subscription, String pin) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);

        switch (aux.status) {
            case PENDING:
            case BLACKLIST:
                rm.status = aux.status;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case ACTIVE:
            case REMOVED:
                SOP sop = subscription.getSop();
                try {
                    Holder<String> resultCode = new Holder();
                    Holder<String> resultDesc = new Holder();
                    Holder<String> transactionID = new Holder();
                    smtResources.validatePin(subscription.getUserAccount(), sop, pin, resultCode, resultDesc, transactionID);
                    rm.message = "[" + resultCode.value + "][" + resultDesc.value + "]";
                    switch (Integer.parseInt(resultCode.value)) {
                        case 0:
                        case 12:
                            rm.status = ResponseMessage.Status.OK;
                            subscription.setStatus(Subscription.Status.PENDING);
                            break;
                        case 6:
                            rm.status = ResponseMessage.Status.ACTIVE;
                            subscription.setStatus(Subscription.Status.ACTIVE);
                            break;
                        case 7:
                        case 8:
                            rm.status = ResponseMessage.Status.PIN_ERROR;
                            break;
                        case 9:
                        case 10:
                        case 11:
                            rm.status = ResponseMessage.Status.BLACKLIST;
                            subscription.setStatus(Subscription.Status.BLACKLIST);
                            break;
                        default:
                            rm.status = ResponseMessage.Status.ERROR;
                    }
                } catch (Exception ex) {
                    log.error("Error en SMT al validar PIN [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "Error en SMT. " + ex;
                }
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);

        return rm;
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    public ResponseMessage sendSMS(Subscription subscription, String message) throws Exception {
        SOP sop = subscription.getSop();
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        try {
            //Hay un shortcode diferente al de suscripcion para enviar SMS
            String shortCode = sop.getIntegrationSettings().get("shortcode.messaging");
            if (shortCode == null) {
                //Hay un shortcode de suscripcion para enviar SMS
                shortCode = sop.getIntegrationSettings().get("shortcode");
            }
            shortCode = shortCode.trim();
            smtResources.sendSms(shortCode, subscription.getUserAccount(), message, sop);
        } catch (Exception ex) {
            log.error("Error al enviar SMS. [" + subscription.getUserAccount() + "]. " + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            try {
                rm.data = mapper.writeValueAsString(subscription);
            } catch (JsonProcessingException ex1) {
            }
            rm.message = "Error al enviar SMS. " + ex;
            return rm;
        }

        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    public void processNotifySmsReception(String countryCode, NotifySmsReception notification) {
        if (countryCode == null || countryCode.length() != 2) {
            log.error("El Codigo de pais no es valido. CountryCode: [" + countryCode + "]");
            return;
        }
        Provider provider = providerMap.get(countryCode.toUpperCase());
        if (provider == null) {
            log.error("No hay configurado un provider para CountryCode: [" + countryCode + "]");
            return;
        }

        try {
            SMSMessage smsMessage = new SMSMessage();
            smsMessage.from = notification.getMessage().getSenderAddress();
            smsMessage.to = notification.getMessage().getSmsServiceActivationNumber();
            smsMessage.message = notification.getMessage().getMessage();
            smsMessage.date = notification.getMessage().getDateTime().toGregorianCalendar().getTime();
            smsMessage.provider = provider;

            String json = mapper.writeValueAsString(smsMessage);
            rabbitMQProducer.messageSender(PaymentHubMain.QUEUE_SMT_SMS_MO + "." + provider.getName(), json);
        } catch (Exception ex) {
            log.error("Error al enviar notificacion SMT al Rabbit. CountryCode: [" + countryCode + "] - Notification: [" + notification + "]. " + ex, ex);
        }
    }

    public Boolean processNotifySmsReception(SMSMessage notification, Provider provider) {

        try {
            Thread.currentThread().setName(notification.thread + ".smt.sms-" + provider.getName());
        } catch (Exception ex) {
        }

        try {
            if (notification.message == null) {
                // log.info("Mensaje NULL. sea signa valor por default: [indice].");
                notification.message = "indice";
            }
            String smsText = notification.message.trim().toLowerCase()
                    .replace(" ", ".")
                    .replace("á", "a")
                    .replace("é", "e")
                    .replace("í", "i")
                    .replace("ó", "o")
                    .replace("ú", "u")
                    .replace("ü", "u");

            String shortcode = notification.to.trim().toLowerCase().replace("tel:", "");
            String msisdn = notification.from.trim().toLowerCase().replace("tel:", "").replace("+", "");

            Map<String, String> settings = new HashMap();
            settings.put("message." + smsText, null);
            settings.put("shortcode", shortcode);

            List<SOP> sops = sopService.findAllBySettings(provider.getName(), settings);

            if (!Collections.isEmpty(sops)) {
                switch (smsText) {
                    case "indice":
                        smtResources.sendSms(shortcode, msisdn, sops.get(0).getIntegrationSettings().get("message.indice"), sops.get(0));
                        return true;
                }
                String value = sops.get(0).getIntegrationSettings().get("message." + smsText);
                if (value != null && value.startsWith("ON_DEMAND")) {
                    msisdn = userAccountNormalizer(msisdn, sops.get(0).getProvider());
                    RLock lock = instrumentedObject.lockObject(msisdn + "_od_" + sops.get(0), 30);
                    Boolean control = null;
                    try {
                        onDemandProcessorHandler.chargeAccount(shortcode, msisdn, smsText, value, sops);
                        control = true;
                    } catch (Exception ex) {
                        log.error("Error al procesar SMS On Demand [" + notification + "]. " + ex, ex);
                    }
                    instrumentedObject.unLockObject(lock);
                    return control;
                }
            }
            settings = new HashMap();
            settings.put("sms.message.default", null);
            settings.put("shortcode", shortcode);

            SOP sop = sopService.findBySettings(provider.getName(), settings);
            if (sop == null) {
                throw new Exception("No hay un SOP configurado para [" + Arrays.toString(settings.entrySet().toArray()) + "]");
            }

            smtResources.sendSms(shortcode, msisdn, sop.getIntegrationSettings().get("sms.message.default"), sop);
            return true;
        } catch (Exception ex) {
            log.error("Error al procesar NotifySmsReception: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    public void processNotifySmsDeliveryReceipt(String countryCode, NotifySmsDeliveryReceipt notification) {
        log.warn("Not supported yet.");
    }

    public Boolean mDivulgaNotificationProcess(String countryCode, MDivulgaNotification notification) {
        Provider p = providerMap.get(countryCode.toUpperCase());
        if (p == null) {
            log.error("No hay configurado un provider para CountryCode: [" + countryCode + "]");
            return null;
        }

        Map<String, String> settings = new HashMap();
        settings.put("productID", notification.getProduct());
        SOP sop = sopService.findBySettings(p.getName(), settings);
        if (sop == null) {
            log.error("No existe un SOP configurado para ProductID: [" + notification.getProduct() + "] - Provider: [" + p + "]");
            return null;
        }

        Tariff t = sop.getMainTariff();
        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(p.getCountry().getCode());
            msg.setCurrencyId(p.getCountry().getCurrency());
            msg.setFromDate(new Date(Long.parseLong(notification.getTimestamp())));
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(MessageType.SUBSCRIPTION_PENDING);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setChannelIn("IVR");
            msg.setUserAccount(this.userAccountNormalizer(notification.getMsisdn(), p));
            try {
                Extra extra = new Extra();
                if (notification.getName() != null) {
                    extra.stringData.put("name", notification.getName());
                }
                if (notification.getTimestamp() != null) {
                    extra.stringData.put("timestamp", notification.getTimestamp());
                }
                msg.setExtra(extra);
            } catch (Exception ex) {
            }
            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(p.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }

            return true;
        } catch (Exception ex) {
            log.error("Error al procesar SMTNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    public boolean mDivulgaIsAuthorized(String basic) {
        if (basic == null || basic.isEmpty()) {
            return false;
        }

        String user = profilePropertiesService.getCommonProperty("internal.service.mdivulga.username");
        String password = profilePropertiesService.getCommonProperty("internal.service.mdivulga.password");
        String dec = user + ":" + password;

        basic = new String(Base64.getDecoder().decode(basic.replace("Basic ", "")));
        return dec.equals(basic);
    }

    public MDivulgaResponse mDivulgaGetSubscriptionStatus(MDivulgaNotification sStatus) {
        MDivulgaResponse response = new MDivulgaResponse();

        if (sStatus.getMsisdn() == null || sStatus.getMsisdn().isEmpty() || sStatus.getServicecode() == null || sStatus.getServicecode().isEmpty()) {
            response.status = "400";
            response.description = "Bad request. Invalid parameters";
            return response;
        }

        SOP sop = sopService.findBySettings("md_servicecode", sStatus.getServicecode());
        if (sop == null) {
            log.warn("No existe un SOP configurado para: [" + sStatus.getServicecode() + "]");
            response.status = "400";
            response.description = "Bad request. Invalid servicecode parameter. ";
            return response;
        }

        List<Subscription> subscriptions = subscriptionService.findSubscriptionsByUserAccount(sStatus.getMsisdn());
        response.body = new BodyResponse();
        if (Collections.isEmpty(subscriptions)) {
            response.body.statusSubscription = "0";
        } else {
            Date unsubscriptionDate = null;
            Subscription.Status status = null;
            Subscription.Status statusOp = null;
            for (Subscription s : subscriptions) {
                if (Subscription.Status.ACTIVE.equals(s.getStatus()) || Subscription.Status.PENDING.equals(s.getStatus())) {
                    status = s.getStatus();
                } else {
                    statusOp = s.getStatus();
                }
                if (s.getUnsubscriptionDate() != null) {
                    if (unsubscriptionDate == null || (s.getUnsubscriptionDate().getTime() > unsubscriptionDate.getTime())) {
                        unsubscriptionDate = s.getUnsubscriptionDate();
                    }
                }
            }

            if (status != null) {
                response.body.statusSubscription = "2"; // Activo
            } else if (unsubscriptionDate != null && (Calendar.getInstance().getTime().getTime() - unsubscriptionDate.getTime() < 2592000000l)) {
                response.body.statusSubscription = "1"; //Blacklist
            } else if (statusOp != null && !Subscription.Status.REMOVED.equals(statusOp) && !Subscription.Status.CANCELLED.equals(statusOp)) {
                response.body.statusSubscription = "1"; //Blacklist
            } else {
                response.body.statusSubscription = "0"; // Sin suscripcion
            }
        }

        response.status = "0";
        response.description = "Successful";

        return response;
    }

}
