package com.itbaf.platform.pgp.services.aws.s3;

import com.itbaf.platform.pgp.services.aws.s3.model.FileProperties;
import java.io.InputStream;

public interface AWSFileManagerService {

    public boolean uploadFile(String path, FileProperties fileProperties, InputStream inputStream);

}
