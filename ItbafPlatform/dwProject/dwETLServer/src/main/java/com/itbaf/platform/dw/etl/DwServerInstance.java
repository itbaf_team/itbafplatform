/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.etl;

import com.google.inject.Injector;
import com.itbaf.platform.paymenthub.commons.rest.ManagerRestServiceImpl;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.servlets.ServletServer;
import com.itbaf.platform.dw.etl.modules.GuiceConfigModule;

/**
 *
 * @author jordonez
 */
public class DwServerInstance extends CommonDaemon {

    private DwServerMain dwServerMain;
    private ServletServer servletServer;
    private static DwServerInstance serverDaemon;

    public static void main(String args[]) throws Exception {
        DwServerInstance.getInstance().init(args);
        DwServerInstance.getInstance().start();
    }

    public synchronized static DwServerInstance getInstance() {
        if (serverDaemon == null) {
            serverDaemon = new DwServerInstance();
            ManagerRestServiceImpl.paymentHubDaemon = serverDaemon;
        }
        return serverDaemon;
    }

    @Override
    public void start() throws Exception {
        servletServer = new ServletServer(new GuiceConfigModule(args, this));
        final Injector injector = servletServer.getInject();
        dwServerMain = injector.getInstance(DwServerMain.class);
        dwServerMain.start();
        servletServer.startServer();
    }

    @Override
    public void stop() throws Exception {
        try {
            servletServer.stopServer();
        } catch (Exception ex) {
        }
        try {
            Thread.sleep(100);
        } catch (Exception ex) {
        }
        try {
            dwServerMain.stop();
        } catch (Exception ex) {
        }
    }

}
