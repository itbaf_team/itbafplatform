/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.EmailUtility;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage;
import com.itbaf.platform.messaging.rest.oauth.ResponseAuthMessage;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.ProviderService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.pgp.commons.resources.ClientCache;
import com.itbaf.platform.pgp.model.Product;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Client;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.OauthProvider;
import com.itbaf.platform.pgp.model.oauth.properties.session.SessionGP;
import com.itbaf.platform.pgp.model.oauth.properties.session.Token;
import com.itbaf.platform.pgp.oauth.api.message.RequestBasicMessage;
import com.itbaf.platform.pgp.oauth.api.message.ResponseBasicMessage;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import com.itbaf.platform.pgp.services.personal.sso.SSOPersonalServices;
import com.itbaf.platform.pgp.services.personal.sso.model.UserSSOPersonal;
import com.itbaf.platform.pgp.commons.api.message.udi.UdiMessage;
import com.itbaf.platform.pgp.oauth.filter.BasicAuthenticationRequestFilter;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.lang.Collections;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class AutenticationServiceProcessorHandler extends ServiceProcessorHandler {

    private final SopService sopService;
    private final ClientCache clientCache;
    private final EmailUtility emailUtility;
    private final ProviderService providerService;
    private final SSOPersonalServices ssoPersonalServices;
    private final PHProfilePropertiesService phprofilePropertiesService;
    private final SubscriptionRegistryService subscriptionRegistryService;

    @Inject
    public AutenticationServiceProcessorHandler(final SopService sopService,
            final ClientCache clientCache,
            final EmailUtility emailUtility,
            final ProviderService providerService,
            final SSOPersonalServices ssoPersonalServices,
            final PHProfilePropertiesService phprofilePropertiesService,
            final SubscriptionRegistryService subscriptionRegistryService) {
        this.sopService = Validate.notNull(sopService, "A SopService class must be provided");
        this.clientCache = Validate.notNull(clientCache, "A ClientCache class must be provided");
        this.emailUtility = Validate.notNull(emailUtility, "An EmailUtility class must be provided");
        this.providerService = Validate.notNull(providerService, "A ProviderService class must be provided");
        this.ssoPersonalServices = Validate.notNull(ssoPersonalServices, "A SSOPersonalServices class must be provided");
        this.phprofilePropertiesService = Validate.notNull(phprofilePropertiesService, "A PHProfilePropertiesService class must be provided");
        this.subscriptionRegistryService = Validate.notNull(subscriptionRegistryService, "A SubscriptionRegistryService class must be provided");
    }

    String generateAuthenticationSessionCode(Map<String, String> prop, User client, User user,
            ResponseBasicMessage rm, String tid, String cookie, HttpServletRequest httpServletRequest, String userAccountProvider) {

        log.info("generateAuthenticationSessionCode. prop [" + prop + "], client: ["+client+"], user: ["+user+"]");

        //syncSubscriptionsRun(user);
        String ccode = client.getId() + "_" + user.getId() + "_" + generateCode(15);
        rm.redirectUrl = prop.get("redirectUri");


        // Extraer parametro adProvider del redirectUri porque llega como json y genera problema
        // ejemplo http://planeta.guru/?msisdn=968724600&adProvider={"source":"gomobile","medium":"pgj_claro_pe","campaign":"claropeent","adnTrackingId":["","pe-claro-juegos"],"adnNotType":{"subscription":"Pends"}}&locale=es_PE&prvdr=pesp&state=login&code=1_41225_PUZbV3hUHFYxYlF

        try {
            if (rm.redirectUrl != null) {
                Map<String, String> uriparams = getUrlValues(rm.redirectUrl);

                if (uriparams.containsKey("adProvider") ) {
                    String ad_param = uriparams.get("adProvider");
                    String ad_ecoded = URLEncoder.encode(ad_param, "UTF-8");
                    uriparams.put("adProvider", ad_ecoded);
                }
                if (uriparams.containsKey("t") ) {
                    String t_param = uriparams.get("t");
                    String t_encoded = URLEncoder.encode(t_param, "UTF-8");
                    uriparams.put("t", t_encoded);
                }
                if (uriparams.containsKey("adProvider") || uriparams.containsKey("t")){
                    StringBuilder sb = new StringBuilder();
                    for (Map.Entry<String,String> entry : uriparams.entrySet()){
                        if (sb.length() > 0) {
                            sb.append("&");
                        }

                        sb.append(String.format("%s=%s",
                                entry.getKey(),
                                entry.getValue()
                        ));
                    }
                    int paramsStart = rm.redirectUrl.indexOf('?');
                    rm.redirectUrl = rm.redirectUrl.substring(0, paramsStart) + "?"+ sb.toString();
                    log.info("generateAuthenticationSessionCode. redirectUrl modificado [" + rm.redirectUrl + "]");
                }

            }
        }catch (Exception ex){
            log.error("Error al corregir parametro adProvider. " + ex, ex);
        }

        if (rm.redirectUrl.contains("?")) {
            rm.redirectUrl = rm.redirectUrl + "&state=" + prop.get("state") + "&code=" + ccode;
        } else {
            rm.redirectUrl = rm.redirectUrl + "?state=" + prop.get("state") + "&code=" + ccode;
        }

        String cc = "";
        String codeChallengeMethod = prop.get("codeChallengeMethod");
        String codeChallenge = prop.get("codeChallenge");
        if (codeChallengeMethod != null && "S256".equals(codeChallengeMethod) && codeChallenge != null) {
            clientCache.setTemporalData(client.getId() + "_" + ccode + "_codeChallenge", codeChallenge, 15);
            cc = "_cc";
        }
        clientCache.setTemporalData(client.getId() + "_" + ccode + cc, user.getId().toString(), 15);
        try {
            String userAgent = httpServletRequest.getHeader("User-Agent");
            String ip = httpServletRequest.getHeader("X-Forwarded-For");
            if (ip == null || ip.length() < 1) {
                ip = httpServletRequest.getRemoteAddr();
            }
            clientCache.setTemporalData(client.getId() + "_ua_" + ccode, userAgent, 15);
            clientCache.setTemporalData(client.getId() + "_ip_" + ccode, ip, 15);

            try {
                String countryCode = clientCache.getTemporalData(tid);
                if (countryCode != null) {
                    clientCache.setTemporalData(ccode + "_country", countryCode, 15);
                }
                if (user.getProfileUserProperty().getCountry() == null) {
                    Country c = null;
                    if (countryCode != null) {
                        c = countryService.findByCode(countryCode);
                    } else if (ip != null) {
                        c = getCountryByIP(ip);
                    }
                    if (c != null) {
                        countryCode = c.getCode();
                        clientCache.setTemporalData(ccode + "_country", countryCode, 15);
                        user.getProfileUserProperty().setCountry(c);
                        userService.saveOrUpdate(user);
                    }
                }
            } catch (Exception ex) {
            }
        } catch (Exception ex) {
        }
        if (cookie != null) {
            clientCache.setTemporalData(client.getId() + "_" + ccode + "_COOKIE", cookie, 15);
        }

        return ccode;
    }


    public Map<String, String> getUrlValues(String url) throws UnsupportedEncodingException {
        int i = url.indexOf("?");
        Map<String, String> paramsMap = new HashMap<>();
        if (i > -1) {
            String searchURL = url.substring(url.indexOf("?") + 1);
            String params[] = searchURL.split("&");

            for (String param : params) {
                int pos = param.indexOf("=");
                String key = param.substring(0, pos);
                String value = param.substring(pos+1);
                paramsMap.put(key, value);
            }
        }

        return paramsMap;
    }

    /**
     * Obtiene el token de autorizacion para acceso a funciones dal cliente
     *
     * @param tm
     * @return
     */
    public ResponseAuthMessage getClientToken(RequestTokenMessage tm) {
        ResponseAuthMessage rm = new ResponseAuthMessage();

        if (tm.clientId == null || tm.clientSecret == null) {
            log.warn("Alguno de los parametros es nulo. clientId: [" + tm.clientId + "] - clientSecret: [" + tm.clientSecret + "]");
            rm.error = ResponseAuthMessage.Error.invalid_request;
            return rm;
        }
        rm.error = ResponseAuthMessage.Error.invalid_client;

        User client = userService.findByclientId(tm.clientId);
        if (client != null && tm.clientSecret.equals(client.getCredentialUserProperty().getClient().getClientSecret())) {
            try {
                rm.access_token = Client.getNewToken(genericPropertyService.getValueByKey("client.sign.secret"), client, null, 30);
                rm.token_type = ResponseAuthMessage.Token.Bearer;
                rm.expires_in = 30 * 24 * 60 * 60L;
                rm.error = null;
            } catch (Exception ex) {
                rm.message = "Error al obtener token Client. " + ex;
                log.error(rm.message, ex);
            }
        }
        return rm;
    }

    /**
     * Obtiene el token de autorizacion para acceso a funciones dal usuario
     * desde el cliente
     *
     * @param tm
     * @return
     */
    public ResponseAuthMessage getUserToken(RequestTokenMessage tm) {
        ResponseAuthMessage rm = new ResponseAuthMessage();

        if (tm.accessToken == null || tm.code == null) {
            log.warn("Alguno de los parametros es nulo. accessToken: [" + tm.accessToken + "] - code: [" + tm.code + "]");
            rm.error = ResponseAuthMessage.Error.invalid_request;
            return rm;
        }

        rm.error = ResponseAuthMessage.Error.invalid_client;
        tm.clientId = null;
        try {
            tm.clientId = Client.validateToken(genericPropertyService.getValueByKey("client.sign.secret"), tm.accessToken).getIssuer();
        } catch (Exception ex) {
            log.warn("Error al validar accessToken: [" + tm.accessToken + "]. " + ex);
        }

        if (tm.clientId == null) {
            return rm;
        }

        User client = userService.findById(Long.parseLong(tm.clientId));
        if (client == null) {
            return rm;
        }
        String cc = "";
        if (tm.codeVerifier != null) {
            String codeChallenge = clientCache.getTemporalData(client.getId() + "_" + tm.code + "_codeChallenge");
            if (codeChallenge == null) {
                rm.error = ResponseAuthMessage.Error.invalid_request;
                return rm;
            }
            String codeChallengeVerifier = this.getSHA256Hash(tm.codeVerifier);
            log.info("codeChallenge-login: [" + codeChallenge + "] - codeVerifier: [" + tm.codeVerifier + "] - codeChallenge-JF: [" + codeChallengeVerifier + "]");
            if (!codeChallenge.equals(codeChallengeVerifier)) {
                rm.error = ResponseAuthMessage.Error.invalid_request;
                return rm;
            }
            cc = "_cc";
        }

        String userId = clientCache.getTemporalData(client.getId() + "_" + tm.code + cc);
        if (userId == null || "xx".equals(userId)) {
            rm.error = ResponseAuthMessage.Error.invalid_request;
            return rm;
        }
        clientCache.setTemporalData(client.getId() + "_" + tm.code + cc, "xx", 15);

        User user = userService.findById(Long.parseLong(userId));
        if (user == null) {
            return rm;
        }

        String cookie = clientCache.getTemporalData(client.getId() + "_" + tm.code + "_COOKIE");
        if (cookie == null) {
            return rm;
        }

        try {
            String inner = Client.getNewToken(user.getId() + "_" + user.getCredentialUserProperty().getPassword() + "_" + client.getId(), user, null, -1);

            Map<String, Object> params = new HashMap();
            params.put("inner", inner);

            String pToken = clientCache.getTemporalData(tm.code + "_ar.p.token");

            if (pToken != null) {
                String pProvider = clientCache.getTemporalData(tm.code + "_ar.p.provider");
                UserSSOPersonal up = new UserSSOPersonal();
                up.token = pToken;
                up.provider = new Provider();
                up.provider.setName(pProvider);
                params.put("ar.p.tk", up);
            }

            String countryCode = clientCache.getTemporalData(tm.code + "_country");
            if (countryCode != null) {
                params.put("country", countryCode);
            }
            rm.access_token = Client.getNewToken(genericPropertyService.getValueByKey("user.sign.secret"), user, params, 1);
            rm.token_type = ResponseAuthMessage.Token.Bearer;
            rm.expires_in = 1 * 24 * 60 * 60L;// dia hora minuto segundo
            rm.error = null;

            SessionGP sgp = user.getSessionUserProperty().getSessions().get(client.getId().toString());
            if (sgp == null) {
                sgp = new SessionGP();
                sgp.setClientId(client.getId());
                user.getSessionUserProperty().getSessions().put(client.getId().toString(), sgp);
            }
            sgp.setActive(true);
            Token userToken = new Token();
            userToken.setToken(rm.access_token);
            String ua = clientCache.getTemporalData(client.getId() + "_ua_" + tm.code);
            String ip = clientCache.getTemporalData(client.getId() + "_ip_" + tm.code);
            userToken.setUserAgent(ua);
            userToken.setIp(ip);
            Token cookieToken = sgp.getUserTokens().get(cookie);
            if (cookieToken != null) {
                //cookieToken ahora tiene un token de usuario anterior.
                //Buscamos un posible cookieToken.
                String key = cookieToken.getToken();
                cookieToken = sgp.getCookieTokens().get(key);
                if (cookieToken != null) {
                    //Removemos el anterior userToken
                    sgp.getCookieTokens().remove(key);
                }
            }

            if (cookieToken == null) {
                //No existe un cookieToken anterior, crearemos uno...
                cookieToken = new Token();
                cookieToken.setToken(cookie);
                cookieToken.setUserAgent(ua);
            }

            sgp.getCookieTokens().put(userToken.getToken(), cookieToken);
            sgp.getUserTokens().put(cookieToken.getToken(), userToken);
            userService.saveOrUpdate(user);
        } catch (Exception ex) {
            rm.access_token = null;
            rm.token_type = null;
            rm.expires_in = null;
            rm.message = "Error al obtener token User. " + ex;
            log.error(rm.message, ex);
        }

        return rm;
    }

    /**
     * Obtiene un codigo de autorizacion token TID para iniciar el login de
     * usuarios
     *
     * @param httpServletRequest
     * @param responseType
     * @param clientId
     * @param redirectUri
     * @param state
     * @param productId
     * @param verifyMail
     * @param codeChallenge
     * @param codeChallengeMethod
     * @param redirectPG -> Fuerza a utilizar la redirectUri al validar el CODE
     * @param prvdr provider
     * @param userId parametro inseguro desde Tiaxa
     * @param mdsp parametro inseguro desde Tiaxa
     * @param msfw parametro inseguro desde Timwe
     * @param suscrod parametro de Timwe Entel-Peru altas TRIAL
     * @return
     */
    public ResponseAuthMessage getAuthorizeCode(HttpServletRequest httpServletRequest, String responseType, String clientId, String redirectUri,
            String state, String productId, String verifyMail, String codeChallenge, String codeChallengeMethod,
            Boolean redirectPG, String prvdr, String userId, String mdsp, String msfw, String suscrod) {
        ResponseAuthMessage rm = new ResponseAuthMessage();

        log.info("Oauth - getAuthorizeCode. responseType: ["+responseType+"], clientId: ["+clientId+"], redirectUri:["+redirectUri+"] \n" +
                " state:["+state+"], productId: ["+productId+"], verifyMail: ["+verifyMail+"], codeChallenge: ["+codeChallenge+"], codeChallengeMethod: ["+codeChallengeMethod+"] \n" +
                " redirectPG: ["+redirectPG+"], prvdr: ["+prvdr+"], userId: ["+userId+"], mdsp: ["+mdsp+"], msfw: ["+msfw+"], suscrod: ["+suscrod+"]");
        if (!"code".equals(responseType) || clientId == null || redirectUri == null || state == null) {
            log.error("Alguno de los parametros es nulo o erroneo.");
            rm.error = ResponseAuthMessage.Error.invalid_request;
            rm.url = BasicAuthenticationRequestFilter.REDIRECT_URL_404;
            return rm;
        }
        rm.error = ResponseAuthMessage.Error.invalid_client;
        User client = null;
        try {
            client = userService.findByclientId(clientId);
        } catch (Exception ex) {
            log.error("Error al obtener el client. clientId: [" + clientId + "]");
        }
        if (client != null) {
            Product p = null;
            if (client.getProducts() != null && !client.getProducts().isEmpty()) {
                for (Product aux : client.getProducts()) {
                    if (aux.getId().toString().equals(productId)) {
                        p = aux;
                    }
                }
            }

            //Para cliente con varios client_id, se busca en la cuenta principal
            if (p == null && client.getPrincipal() != null) {
                if (client.getPrincipal().getProducts() != null && !client.getPrincipal().getProducts().isEmpty()) {
                    for (Product aux : client.getPrincipal().getProducts()) {
                        if (aux.getId().toString().equals(productId)) {
                            p = aux;
                        }
                    }
                }
            }
            if (p != null) {
                try {
                    Map<String, String> data = new HashMap();
                    data.put("responseType", responseType);
                    data.put("clientId", clientId);
                    data.put("redirectUri", redirectUri);
                    if (redirectPG != null) {
                        data.put("redirectPG", redirectPG + "");
                    }
                    if (prvdr != null) {
                        data.put("prvdr_name", prvdr);
                        prvdr = phprofilePropertiesService.getCommonProperty(prvdr);
                        if (prvdr != null) {
                            data.put("prvdr", prvdr);
                        }
                    }
                    data.put("state", state);
                    data.put("userId", client.getId().toString());
                    data.put("productId", productId);
                    data.put("verifyMail", verifyMail == null ? "vcode" : verifyMail);
                    if (codeChallengeMethod != null && "S256".equals(codeChallengeMethod) && codeChallenge != null) {
                        data.put("codeChallenge", codeChallenge);
                        data.put("codeChallengeMethod", codeChallengeMethod);
                    }

                    Map<String, Object> params = new HashMap();
                    params.put("data", data);
                    rm.error = null;
                    rm.message = Client.getNewToken(genericPropertyService.getValueByKey("user.sign.secret"), client, params, 15);

                    if (userId != null && mdsp != null && prvdr != null) {
                        //Esto es algo horrible q hubo que hacer para Tiaxa
                        //Solamente acceden directamente usuarios activos...
                        RLock lock = instrumentedObject.lockObject("user_lock_sou_" + userId, 12);
                        try {
                            Provider provider = providerService.findById(Long.parseLong(prvdr));
                            Map<String, String> settings = new HashMap();

                            if(provider.getName().endsWith(".personal.store")) {
                                settings.put("store_serviceId", mdsp);
                                SOP sop = sopService.findBySettings(provider.getName(), settings);
                                Subscription s = null;
                                //store
                                String providerToken = sop.getIntegrationSettings().get("providerToken");
                                if (providerToken == null || providerToken.equals("")) {
                                    log.info("No existe un providerToken configurado");
                                }
                                DefaultClaims claim = null;
                                try {
                                    claim = CommonFunction.validateToken(providerToken, userId);
                                } catch (ExpiredJwtException ex) {
                                    log.info("Token expirado", ex);
                                    claim = (DefaultClaims) ex.getClaims();
                                } catch (SignatureException ex) {
                                    log.error("El providerToken '" + providerToken + "' no es una firma valida", ex);
                                } catch (Exception ex) {
                                    log.error("Error al validar firma", ex);
                                }

                                if (claim != null) {
                                    String msisdn = (String) claim.get("sub");

                                    try {
                                        SubscriptionRegistry sr = subscriptionRegistryService.findByExternalId(msisdn);
                                        if (sr != null) {
                                            s = sr.getSubscription();
                                        }
                                    } catch (Exception ex) {
                                        log.error("Error al obtener suscripcion... " + ex.getMessage());
                                    }
                                    if (s == null) {
                                        s = subscriptionService.findByUserAccount(msisdn, sop);
                                        if (s == null) {
                                            s = subscriptionService.findByExternalUserAccount(msisdn, sop);
                                        }
                                    }

                                    if (s != null) {
                                        log.info("Login por MSISDN: [" + msisdn + "] - productID: [" + mdsp + "] - Subscription: [" + s.getId() + "][" + s.getStatus() + "]");
                                        if (Subscription.Status.ACTIVE.equals(s.getStatus()) || Subscription.Status.PENDING.equals(s.getStatus())) {
                                            User u = userService.findByMsisdn(s.getUserAccount());
                                            if (u == null) {
                                                u = new User();
                                                u.getProfileUserProperty().setCountry(provider.getCountry());
                                                u.getCredentialUserProperty().getUsername().setMsisdn(s.getUserAccount(), true, provider.getCountry(), provider);
                                                userService.saveOrUpdate(u);
                                            }
                                            ResponseBasicMessage rmx = new ResponseBasicMessage();
                                            generateAuthenticationSessionCode(data, client, u, rmx, rm.message, "null", httpServletRequest, null);
                                            rm.url = rmx.redirectUrl;
                                        }
                                    }
                                }
                            } else if(provider.getName().endsWith(".claro.smt")){
                                //smt
                                settings.put("productID", mdsp);
                                SOP sop = sopService.findBySettings(provider.getName(), settings);
                                Subscription s = null;

                                try {
                                    SubscriptionRegistry sr = subscriptionRegistryService.findByExternalId(userId);
                                    if (sr != null) {
                                        s = sr.getSubscription();
                                    }
                                } catch (Exception ex) {
                                    log.error("Error al obtener suscripcion... " + ex.getMessage());
                                }
                                if (s == null) {//Remover cuando se apruebe el ATP... y se actualice la DB
                                    s = subscriptionService.findByUserAccount(userId, sop);
                                    if (s == null) {//Remover cuando se apruebe el ATP... y se actualice la DB
                                        s = subscriptionService.findByExternalUserAccount(userId, sop);
                                    }
                                }

                                if (s != null) {
                                    log.info("Login por UserId: [" + userId + "] - productID: [" + mdsp + "] - Subscription: [" + s.getId() + "][" + s.getStatus() + "]");
                                    if (Subscription.Status.ACTIVE.equals(s.getStatus()) || Subscription.Status.PENDING.equals(s.getStatus())) {
                                        User u = userService.findByMsisdn(s.getUserAccount());
                                        if (u == null) {
                                            u = new User();
                                            u.getProfileUserProperty().setCountry(provider.getCountry());
                                            u.getCredentialUserProperty().getUsername().setMsisdn(s.getUserAccount(), true, provider.getCountry(), provider);
                                            userService.saveOrUpdate(u);
                                        }
                                        ResponseBasicMessage rmx = new ResponseBasicMessage();
                                        generateAuthenticationSessionCode(data, client, u, rmx, rm.message, "null", httpServletRequest, null);
                                        rm.url = rmx.redirectUrl;
                                    } else {
                                        rm.url = sop.getIntegrationSettings().get("unsubscription.redirect.url");
                                    }
                                } else {
                                    rm.url = sop.getIntegrationSettings().get("unsubscription.redirect.url");
                                }
                            } else {
                                //tiaxa
                                settings.put("productID", mdsp);
                                SOP sop = sopService.findBySettings(provider.getName(), settings);
                                Subscription s = null;

                                try {
                                    SubscriptionRegistry sr = subscriptionRegistryService.findByExternalId(userId);
                                    if (sr != null) {
                                        s = sr.getSubscription();
                                    }
                                } catch (Exception ex) {
                                    log.error("Error al obtener suscripcion... " + ex.getMessage());
                                }
                                if (s == null) {//Remover cuando se apruebe el ATP... y se actualice la DB
                                    s = subscriptionService.findByUserAccount(userId, sop);
                                    if (s == null) {//Remover cuando se apruebe el ATP... y se actualice la DB
                                        s = subscriptionService.findByExternalUserAccount(userId, sop);
                                    }
                                }

                                if (s != null) {
                                    log.info("Login por UserId: [" + userId + "] - productID: [" + mdsp + "] - Subscription: [" + s.getId() + "][" + s.getStatus() + "]");
                                    if (Subscription.Status.ACTIVE.equals(s.getStatus())) {
                                        User u = userService.findByMsisdn(s.getUserAccount());
                                        if (u == null) {
                                            u = new User();
                                            u.getProfileUserProperty().setCountry(provider.getCountry());
                                            u.getCredentialUserProperty().getUsername().setMsisdn(s.getUserAccount(), true, provider.getCountry(), provider);
                                            userService.saveOrUpdate(u);
                                        }
                                        ResponseBasicMessage rmx = new ResponseBasicMessage();
                                        generateAuthenticationSessionCode(data, client, u, rmx, rm.message, "null", httpServletRequest, null);
                                        rm.url = rmx.redirectUrl;
                                    }
                                } else {
                                    rm.url = sop.getIntegrationSettings().get("unsubscription.redirect.url");
                                }
                            }
                        } catch (Exception ex) {
                        }
                        instrumentedObject.unLockObject(lock);
                    } else if ((suscrod != null || msfw != null) && prvdr != null) {
                        //Lo mismo horrible de Tiaxa pero con Timwe
                        RLock lock = null;
                        try {
                            Provider provider = providerService.findById(Long.parseLong(prvdr));
                            List<SOP> sops = sopService.getSOPsByProviderName(provider.getName());
                            if (sops != null && !sops.isEmpty()) {
                                String presharedKey = sops.get(0).getIntegrationSettings().get("preshared-key-oauth");
                                String[] paramx;
                                if (suscrod == null) {
                                    paramx = getTimweDecyptParams(msfw, presharedKey);
                                } else {
                                    paramx = getTimweDecyptParams(suscrod, presharedKey);
                                }

                                if (paramx != null && paramx.length >= 3 && suscrod == null) {
                                    lock = instrumentedObject.lockObject("user_lock_sou_" + paramx[0], 12);
                                    Map<String, String> settings = new HashMap();
                                    settings.put("product_id", paramx[1]);
                                    SOP sop = sopService.findBySettings(provider.getName(), settings);
                                    Subscription s = subscriptionService.getSubscriptionStatusFacadeRequest(paramx[0], sop);
                                    if (s != null) {
                                        log.info("Login por MSISDN: [" + paramx[0] + "] - productID: [" + paramx[1] + "] - Subscription: [" + s.getId() + "][" + s.getStatus() + "]");
                                        if (s.getUnsubscriptionDate() != null) {
                                            User u = userService.findByMsisdn(s.getUserAccount());
                                            log.info("oauth - getAuthorizeCode - Login por MSISDN - User: [" + u + "]");
                                            if (u == null) {
                                                u = new User();
                                                u.getProfileUserProperty().setCountry(provider.getCountry());
                                                u.getCredentialUserProperty().getUsername().setMsisdn(s.getUserAccount(), true, provider.getCountry(), provider);
                                                userService.saveOrUpdate(u);
                                            }
                                            ResponseBasicMessage rmx = new ResponseBasicMessage();
                                            generateAuthenticationSessionCode(data, client, u, rmx, rm.message, "null", httpServletRequest, null);
                                            rm.url = rmx.redirectUrl;
                                        } else {
                                            // Sin suscripcion activa.. redirecciona a unsubscription.redirect.url
                                            rm.url = sop.getIntegrationSettings().get("unsubscription.redirect.url");
                                        }
                                    } else {
                                        log.info("No existe la suscripcion: [" + paramx[0] + "] - productID: [" + paramx[1] + "]");
                                        rm.url = sop.getIntegrationSettings().get("unsubscription.redirect.url");
                                    }
                                } else if (paramx != null && paramx.length >= 2 && suscrod != null) {
                                    lock = instrumentedObject.lockObject("user_lock_sou_" + paramx[0], 12);

                                    List<Subscription> sl = subscriptionService.getSubscriptionCountryFacadeRequest(paramx[0], sops.get(0).getProvider().getCountry().getCode());
                                    if (sl != null && !sl.isEmpty()) {
                                        log.info("Login por MSISDN: [" + paramx[0] + "] - Type: [" + paramx[1] + "] - SubscriptionList: [" + sl.size() + "]");
                                        Subscription s = null;
                                        for (Subscription ss : sl) {
                                            if (ss.getUnsubscriptionDate() != null && s == null) {
                                                for (SOP sop : sops) {
                                                    if (sop.getId().equals(ss.getSop().getId())) {
                                                        s = ss;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (s != null) {
                                                break;
                                            }
                                        }

                                        if (s != null) {
                                            log.info("Login por MSISDN: [" + paramx[0] + "] - Type: [" + paramx[1] + "] - Subscription: [" + s.getId() + "][" + s.getStatus() + "]");
                                            User u = userService.findByMsisdn(s.getUserAccount());
                                            if (u == null) {
                                                u = new User();
                                                u.getProfileUserProperty().setCountry(provider.getCountry());
                                                u.getCredentialUserProperty().getUsername().setMsisdn(s.getUserAccount(), true, provider.getCountry(), provider);
                                                userService.saveOrUpdate(u);
                                            }
                                            ResponseBasicMessage rmx = new ResponseBasicMessage();
                                            generateAuthenticationSessionCode(data, client, u, rmx, rm.message, "null", httpServletRequest, null);
                                            rm.url = rmx.redirectUrl;
                                        } else {
                                            // Sin suscripcion.. redirecciona a unsubscription.redirect.url
                                            rm.url = phprofilePropertiesService.getCommonProperty(provider.getId(), "unsubscription.redirect.url");
                                        }
                                    } else {
                                        log.info("No existe la suscripcion: [" + paramx[0] + "] - Type: [" + paramx[1] + "]");
                                        rm.url = phprofilePropertiesService.getCommonProperty(provider.getId(), "unsubscription.redirect.url");
                                    }
                                } else {
                                    log.warn("Datos invalidos al desencriptar: msfw: [" + msfw + "][" + suscrod + "] - preshared-key: [" + presharedKey + "]");
                                    rm.url = phprofilePropertiesService.getCommonProperty(provider.getId(), "unsubscription.redirect.url");
                                }
                            } else {
                                log.warn("No existe sop para el provider especificado: [" + prvdr + "]");
                                rm.url = phprofilePropertiesService.getCommonProperty(provider.getId(), "unsubscription.redirect.url");
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        instrumentedObject.unLockObject(lock);
                    }
                } catch (Exception ex) {
                    rm.message = "Error al obtener token. " + ex;
                    log.error(rm.message, ex);
                }
            }
        }
        log.info("oauth - getAuthorizeCode - rm.url ["+rm.url+"]");
        return rm;
    }

    private String[] getTimweDecyptParams(String enrypti, String presharedKey) throws Exception {
        try {
            SecretKeySpec keySpecification = new SecretKeySpec(presharedKey.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, keySpecification);
            byte[] desencryptedBytes = java.util.Base64.getDecoder().decode(enrypti);
            byte[] encryptedBytes = cipher.doFinal(desencryptedBytes);
            return new String(encryptedBytes).split("#");
        } catch (Exception ex) {
        }
        return null;
    }

    public Map<String, String> validateTID(String tid) throws Exception {
        return Client.validateTokenData(genericPropertyService.getValueByKey("user.sign.secret"), tid);
    }

    public DefaultClaims validateToken(String tid) throws Exception {
        return Client.validateToken(genericPropertyService.getValueByKey("user.sign.secret"), tid);
    }

    public boolean googleRecaptchaValidation(String gResponse) {
        String url = "https://www.google.com/recaptcha/api/siteverify";
        String qs = "secret=" + genericPropertyService.getValueByKey("google.recaptcha.key.secret") + "&response=" + gResponse;
        String response = requestClient.requestPostGoogle(url, qs);

        // log.info("Google response: [" + response + "]");
        return response.contains("\"success\": true");
    }

    public ResponseBasicMessage validateUserCookieTokenArPersonal(Map<String, String> prop, String tid, String cookieToken,
            HttpServletRequest request, User udiUser, Holder<Provider> prov, HttpServletRequest httpServletRequest) throws Exception {
        log.info("Login por cookie de Personal: [" + cookieToken + "]");

        ResponseBasicMessage rm = new ResponseBasicMessage();
        User cliente = userService.findByclientId(prop.get("clientId"));
        if (cliente == null) {
            log.error("No existe el clientId: [" + prop.get("clientId") + "]");
            return rm;
        }

        // log.info("PERSONAL-TOKEN: [" + cookieToken + "]");
        if (prov.value == null) {
            prov.value = providerService.findByName("ar.personal.cdag");
        }
        UserSSOPersonal userPersonal = ssoPersonalServices.getUserSSOPersonal(cookieToken, prov.value);
        if (userPersonal != null && userPersonal.msisdn != null) {
            userPersonal.msisdn = userAccountNormalizer(userPersonal.msisdn, prov.value);

            if (udiUser != null) {
                if (udiUser.getCredentialUserProperty().getUsername().getMsisdn(userPersonal.msisdn) == null) {
                    log.warn("El msisdn de userPersonal: [" + userPersonal + "], es diferente del existente en udiUser: [" + udiUser + "]");
                    User udiUserAux = getUser(userPersonal.msisdn, prov.value.getCountry().getCode(), tid, prov.value.getName(), prov);
                    if ((udiUser.getId() == null && udiUserAux.getId() == null) || (udiUser.getId() != null && udiUserAux.getId() == null)) {
                        udiUser.getCredentialUserProperty().getUsername().getMsisdns().putAll(udiUserAux.getCredentialUserProperty().getUsername().getMsisdns());
                    } else if (udiUser.getId() == null && udiUserAux.getId() != null) {
                        udiUserAux.getCredentialUserProperty().getUsername().getMsisdns().putAll(udiUser.getCredentialUserProperty().getUsername().getMsisdns());
                        udiUser = udiUserAux;
                    } else {
                        udiUser = funUsers(Arrays.asList(udiUser, udiUserAux));
                    }
                }
            } else {
                udiUser = getUser(userPersonal.msisdn, prov.value.getCountry().getCode(), tid, prov.value.getName(), prov);
            }

            if (userPersonal.email != null) {
                User udiUserAux = userService.findByEmail(userPersonal.email);
                if (udiUserAux != null) {
                    udiUser = funUsers(Arrays.asList(udiUserAux, udiUser));
                } else {
                    udiUser.getCredentialUserProperty().getUsername().setEmail(userPersonal.email, false);
                }
            }

            if (userPersonal.lastName != null && udiUser.getProfileUserProperty().getLastName() == null) {
                udiUser.getProfileUserProperty().setLastName(userPersonal.lastName);
            }

            if (userPersonal.id != null) {
                OauthProvider op = udiUser.getProfileUserProperty().getProviders().get(userPersonal.id);
                if (op == null) {
                    op = new OauthProvider();
                    udiUser.getProfileUserProperty().getProviders().put(userPersonal.id, op);
                }
                op.setUserPersonal(userPersonal);
            }

            if (userPersonal.msisdn != null) {
                try {
                    udiUser.getCredentialUserProperty().getUsername().getMsisdn(userPersonal.msisdn).setConfirmed(Boolean.TRUE);
                } catch (Exception ex) {
                    log.error("Error al confirmar MSISDN: [" + userPersonal.msisdn + "] en User: [" + udiUser + "]. " + ex, ex);
                }
            }

            // log.info("Login por Personal Token. udiUser: [" + udiUser + "]");
            userService.saveOrUpdate(udiUser);

            try {
                String ua = httpServletRequest.getHeader("User-Agent");
                rm.uct = Client.getNewToken(udiUser.getCredentialUserProperty().getPassword() + "_" + udiUser.getId() + "_" + ua, udiUser, null, 365);
                Map<String, Object> params = new HashMap();
                params.put("inner", rm.uct);
                params.put("userAccountProvider", prov.value.getName());
                try {
                    UserSSOPersonal up = new UserSSOPersonal();
                    up.token = cookieToken;
                    up.provider = new Provider();
                    up.provider.setName(prov.value.getName());
                    params.put("ar.p.tk", up);
                } catch (Exception ex) {
                }
                rm.uct = Client.getNewToken(genericPropertyService.getValueByKey("user.sign.secret"), udiUser, params, 365);
                String ccode = generateAuthenticationSessionCode(prop, cliente, udiUser, rm, tid, rm.uct, httpServletRequest, prov.value.getName());

                clientCache.setTemporalData(ccode + "_ar.p.token", cookieToken, 15);
                clientCache.setTemporalData(ccode + "_ar.p.provider", userPersonal.provider.getName(), 15);
            } catch (Exception ex) {
                log.error("Error al generar Cookie/token. " + ex, ex);
            }

        }
        return rm;
    }

    public ResponseBasicMessage validateUserCookieToken(Map<String, String> prop, String tid, String cookieToken, HttpServletRequest httpServletRequest, User udiUser) throws Exception {
        log.info("Login por cookie: [" + cookieToken + "]");
        ResponseBasicMessage rm = new ResponseBasicMessage();
        User cliente = userService.findByclientId(prop.get("clientId"));
        if (cliente == null) {
            log.error("No existe el clientId: [" + prop.get("clientId") + "]");
            return rm;
        }

        DefaultClaims cd;
        try {
            cd = Client.validateToken(genericPropertyService.getValueByKey("user.sign.secret"), cookieToken);
        } catch (Exception ex) {
            return rm;
        }
        if (cd == null) {
            return rm;
        }

        User user = userService.findById(Long.parseLong(cd.getIssuer()));
        if (user == null) {
            log.error("No existe el userId: [" + cd.getIssuer() + "]");
            return rm;
        }
        try {
            String ua = httpServletRequest.getHeader("User-Agent");
            cd = Client.validateToken(user.getCredentialUserProperty().getPassword() + "_" + user.getId() + "_" + ua, cd.get("inner", String.class));
            if (cd == null) {
                return rm;
            }
        } catch (Exception ex) {
            return rm;
        }

        boolean hasSession = false;
        if (!user.getSessionUserProperty().getSessions().isEmpty()
                && user.getSessionUserProperty().getSessions().get(cliente.getId().toString()) != null
                && user.getSessionUserProperty().getSessions().get(cliente.getId().toString()).isActive()) {
            if (user.getSessionUserProperty().getSessions().get(cliente.getId().toString()).getUserTokens().get(cookieToken) != null) {
                hasSession = true;
            }
        }
        //Busqueda de la sesion en otros client asociados CrossSession.
        if (!hasSession) {
            User clientAux = cliente;
            while (clientAux.getPrincipal() != null) {
                clientAux = clientAux.getPrincipal();
            }

            if (!user.getSessionUserProperty().getSessions().isEmpty()
                    && user.getSessionUserProperty().getSessions().get(clientAux.getId().toString()) != null
                    && user.getSessionUserProperty().getSessions().get(clientAux.getId().toString()).isActive()) {
                if (user.getSessionUserProperty().getSessions().get(clientAux.getId().toString()).getUserTokens().get(cookieToken) != null) {
                    hasSession = true;
                }
            }

            if (!hasSession && !clientAux.getUsers().isEmpty()) {
                for (User caux : clientAux.getUsers()) {
                    if (!user.getSessionUserProperty().getSessions().isEmpty()
                            && user.getSessionUserProperty().getSessions().get(caux.getId().toString()) != null
                            && user.getSessionUserProperty().getSessions().get(caux.getId().toString()).isActive()) {
                        if (user.getSessionUserProperty().getSessions().get(caux.getId().toString()).getUserTokens().get(cookieToken) != null) {
                            hasSession = true;
                            break;
                        }
                    }
                }
            }
        }

        if (hasSession) {
            generateAuthenticationSessionCode(prop, cliente, user, rm, tid, cookieToken, httpServletRequest, null);
            try {
                while (user.getPrincipal() != null) {
                    user = user.getPrincipal();
                }
                if (udiUser != null) {
                    if (udiUser.getId() == null) {
                        Set<String> keys = udiUser.getCredentialUserProperty().getUsername().getMsisdns().keySet();
                        for (String key : keys) {
                            user.getCredentialUserProperty().getUsername().setMsisdn(udiUser.getCredentialUserProperty().getUsername().getMsisdns().get(key));
                        }
                    } else if (!udiUser.getId().equals(user.getId())) {
                        User aux = funUsers(Arrays.asList(udiUser, user));
                        if (aux != null) {
                            user = aux;
                        }
                    }
                    userService.saveOrUpdate(user);
                }
            } catch (Exception ex) {
                log.error("Error al integrar UdiMessage con User por cookie. " + ex, ex);
            }
        }

        return rm;
    }

    public User getUserFromUdiMessage(String udiToken, String aniFound, String tid, Map<String, String> prop, Holder<Provider> prov, Holder<String> redirect) throws Exception {
        UdiMessage um = getUdiMessageFromToken(udiToken);
        User user = null;
        log.info("Adentro del getUserFromUdiMessage con aniFound:"+aniFound);
        if (um != null && um.phone != null && um.phone.ani != null && "true".equals(aniFound)) {
            String msisdn = um.phone.ani.trim().replace("+", "");

            /////////////////Validacion de suscripcion para Tiaxa/////////////////
            //Todo lo q sea de Claro MX y tenga HE pasara por aca
            try {
                if (um.phone.carrier != null && "mx.claro.tiaxa".equals(um.phone.carrier.replace("_", "."))) {
                    Product pd = productService.findbyId(Long.parseLong(prop.get("productId")));
                    redirect.value = pd.getProductSettings().get("oauth.sso.mx.tiaxa");
                    if ("true".equals(pd.getProductSettings().get("mx.claro.tiaxa.is.he"))) {
                        List<Subscription> ls = subscriptionService.getActiveSubscriptions(msisdn, "mx.claro.tiaxa");
                        if (ls != null && !ls.isEmpty()) {
                            boolean ct = false;
                            for (Subscription s : ls) {
                                if (!ct) {
                                    for (SOP sop : pd.getSops()) {
                                        if (sop.getId().equals(s.getSop().getId())) {
                                            ct = true;
                                            break;
                                        }
                                    }
                                } else {
                                    break;
                                }
                            }

                            if (!ct) {
                                return null;
                            }
                            redirect.value = null;
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                }

                if (um.phone.carrier != null && "ec.claro.tiaxa".equals(um.phone.carrier.replace("_", "."))) {
                    Product pd = productService.findbyId(Long.parseLong(prop.get("productId")));
                    redirect.value = pd.getProductSettings().get("oauth.sso.ec.tiaxa");
                    if ("true".equals(pd.getProductSettings().get("ec.claro.tiaxa.is.he"))) {
                        List<Subscription> ls = subscriptionService.getActiveSubscriptions(msisdn, "ec.claro.tiaxa");
                        if (ls != null && !ls.isEmpty()) {
                            boolean ct = false;
                            for (Subscription s : ls) {
                                if (!ct) {
                                    for (SOP sop : pd.getSops()) {
                                        if (sop.getId().equals(s.getSop().getId())) {
                                            ct = true;
                                            break;
                                        }
                                    }
                                } else {
                                    break;
                                }
                            }

                            if (!ct) {
                                return null;
                            }
                            redirect.value = null;
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                }

                log.info("Antes del if de la linea 957. Ani :"+aniFound);
                if (um.phone.carrier != null && "do.claro.tiaxa".equals(um.phone.carrier.replace("_", "."))) {
                    log.info("Despues  del if de la linea 957. Ani :"+aniFound);
                    Product pd = productService.findbyId(Long.parseLong(prop.get("productId")));
                    redirect.value = pd.getProductSettings().get("oauth.sso.do.tiaxa");
                    if ("true".equals(pd.getProductSettings().get("do.claro.tiaxa.is.he"))) {
                        log.info("Url retornada "+redirect.value+" Ani:"+aniFound);
                        List<Subscription> ls = subscriptionService.getActiveSubscriptions(msisdn, "do.claro.tiaxa");
                        if (ls != null && !ls.isEmpty()) {
                            boolean ct = false;
                            for (Subscription s : ls) {
                                if (!ct) {
                                    for (SOP sop : pd.getSops()) {
                                        if (sop.getId().equals(s.getSop().getId())) {
                                            ct = true;
                                            break;
                                        }
                                    }
                                } else {
                                    break;
                                }
                            }

                            if (!ct) {
                                return null;
                            }
                            redirect.value = null;
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                }
            } catch (Exception ex) {
            }
            //////////////////////////////////////////////////////////////////////

            user = getUser(msisdn, um.geo.country.iso_code, tid, um.phone.carrier, prov);
        } else if (um != null && um.geo != null) {
            try {
                String countryCode = um.geo.country.iso_code;
                Country c = countryService.findByCode(countryCode);
                clientCache.setTemporalData(tid, c.getCode(), 60);
                /*if (user.getProfileUserProperty().getCountry() == null) {
                    user.getProfileUserProperty().setCountry(c);
                }*/
            } catch (Exception ex) {
            }
        }
        log.info("User para Udi: [" + um + "] - User: [" + user + "]");
        return user;
    }

    private User getUser(String msisdn, String countryCode, String tid, String carrier, Holder<Provider> prov) throws Exception {
        User user = null;
        Country c = null;
        RLock lock = instrumentedObject.lockObject("user_lock_sou_" + msisdn, 12);
        try {
            try {
                c = countryService.findByCode(countryCode);
                clientCache.setTemporalData(tid, c.getCode(), 60);
            } catch (Exception ex) {
            }
            if (c != null && !msisdn.startsWith(c.getPhoneCode().toString())) {
                msisdn = c.getPhoneCode() + msisdn;
            }
            if (msisdn.length() > 10) {
                if (msisdn.startsWith("5415")) {
                    msisdn = "5411" + msisdn.substring(4, msisdn.length());
                } else if (msisdn.startsWith("54915")) {
                    msisdn = "54911" + msisdn.substring(5, msisdn.length());
                }
            }
            user = userService.findByMsisdn(msisdn);
            if (user == null) {
                Country aux = countryService.findByInternationalNumberPhone(msisdn);
                if (aux == null) {
                    log.warn("No fue posible obtener el country a partir del MSISDN: [" + msisdn + "]");
                } else {
                    if (c == null || !aux.getId().equals(c.getId())) {
                        log.warn("El pais de UDI [" + c + "] y obtenido por findByInternationalNumberPhone [" + aux + "]");
                        c = aux;
                    }
                }
                String localLength = phprofilePropertiesService.getCommonProperty("msisdn." + c.getCode().toLowerCase() + ".length.local");
                if (localLength == null) {
                    log.error("No existe en DB la configuracion: [msisdn." + c.getCode().toLowerCase() + ".length.local]");
                } else {
                    Integer msisdnLengthLocal = Integer.parseInt(localLength);
                    String ani = msisdn;
                    if (ani.startsWith(c.getPhoneCode().toString())) {
                        ani = ani.substring(c.getPhoneCode().toString().length(), ani.length());
                    }
                    ani = ani.substring(Math.max(0, ani.length() - msisdnLengthLocal), ani.length());

                    if (ani.length() < msisdnLengthLocal) {
                        log.warn("Invalido Formato del MSISDN: [" + ani + "]");
                    } else {
                        Holder<String> username = new Holder();
                        user = userService.findByMsisdn(c.getPhoneCode().toString(), ani, username);
                        if (user != null) {
                            msisdn = username.value;
                            if (prov.value == null) {
                                prov.value = providerService.findOrCreateByName(carrier.replace("_", "."));
                                user.getCredentialUserProperty().getUsername().getMsisdn(msisdn).setProvider(prov.value);
                                userService.saveOrUpdate(user);
                            }
                        } else {
                            user = userService.findByMsisdn(ani);
                            if (user != null) {
                                Msisdn m = new Msisdn(msisdn, Boolean.TRUE, c, user.getCredentialUserProperty().getUsername().getMsisdn(ani).getProvider());
                                if (m.getProvider() == null && carrier != null) {
                                    m.setProvider(providerService.findOrCreateByName(carrier.replace("_", ".")));
                                }
                                user.getCredentialUserProperty().getUsername().getMsisdns().remove(ani);
                                user.getCredentialUserProperty().getUsername().setMsisdn(m);
                                prov.value = m.getProvider();
                                if (prov.value == null) {
                                    prov.value = providerService.findOrCreateByName(carrier.replace("_", "."));
                                    m.setProvider(prov.value);
                                }
                                userService.saveOrUpdate(user);
                            }
                        }
                    }
                }
            } else {
                prov.value = user.getCredentialUserProperty().getUsername().getMsisdn(msisdn).getProvider();
                if (prov.value == null) {
                    prov.value = providerService.findOrCreateByName(carrier.replace("_", "."));
                    user.getCredentialUserProperty().getUsername().getMsisdn(msisdn).setProvider(prov.value);
                    userService.saveOrUpdate(user);
                }
            }

            if (user == null) {
                Subscription s = subscriptionService.findByMSISDNAndCountry(msisdn, c);
                if (s == null) {
                    s = subscriptionService.getSubscriptionInformationFacadeRequest(msisdn, c.getCode());
                }

                if (s != null && s.getSubscriptionRegistry() != null) {
                    // log.info("Suscripcion encontrada en PH. [" + s.getId() + "]");
                    Msisdn msis = new Msisdn(s.getUserAccount(), Boolean.TRUE, c, s.getSubscriptionRegistry().getSop().getProvider());
                    user = new User();
                    user.getCredentialUserProperty().getUsername().setMsisdn(msis);
                    prov.value = msis.getProvider();
                    user.getProfileUserProperty().setCountry(c);
                }
            }
        } catch (Exception ex) {
            log.error("Error al obtener User de UdiMessage msisdn: [" + msisdn + "]");
        }

        if (user == null) {
            Provider p = null;
            try {
                if (carrier != null) {
                    p = providerService.findOrCreateByName(carrier.replace("_", "."));
                }
            } catch (Exception ex) {
            }
            msisdn = userAccountNormalizer(msisdn, p);
            Msisdn msis = new Msisdn(msisdn, Boolean.TRUE, c, p);
            user = new User();
            user.getCredentialUserProperty().getUsername().setMsisdn(msis);
        }
        if (user.getProfileUserProperty().getCountry() == null) {
            user.getProfileUserProperty().setCountry(c);
        }

        user.lock = lock;
        return user;
    }

    public ResponseBasicMessage startUdiSession(Map<String, String> prop, String tid, User udiUser,
            HttpServletRequest httpServletRequest, Holder<Provider> prov) throws Exception {
        ResponseBasicMessage rm = new ResponseBasicMessage();

        User cliente = userService.findByclientId(prop.get("clientId"));
        if (cliente == null) {
            log.error("No existe el clientId: [" + prop.get("clientId") + "]");
            return rm;
        }
        if (udiUser == null) {
            return rm;
        }
        if (udiUser.getId() == null) {
            userService.saveOrUpdate(udiUser);
            instrumentedObject.unLockObject(udiUser.lock);
        }
        String provr = null;
        if (prov.value != null) {
            provr = prov.value.getName();
        }
        generateAuthenticationSessionCode(prop, cliente, udiUser, rm, tid, "UDI_SESSION", httpServletRequest, provr);
        return rm;
    }

    private UdiMessage getUdiMessageFromToken(String udiToken) {
        if (udiToken == null) {
            return null;
        }
        try {
            String url = getUdiURL() + "/ani/" + udiToken;
            String response = requestClient.requestJsonGet(url);
            log.info("UdiMessage [" + udiToken + "]. Respuesta de UDI: [" + response + "]");
            UdiMessage um = mapper.readValue(response, UdiMessage.class);
            return um;
        } catch (Exception ex) {
            log.error("Error al obtener Message de UDI con token: [" + udiToken + "]");
        }
        return null;
    }

    /**
     * Obtiene informacion basica del usuario con base en el MSISDN o el EMAIL
     *
     * @param requestBasicMessage
     * @param prop
     * @param sendCode
     * @param httpServletRequest
     * @return
     */
    public ResponseBasicMessage getUserInformation(RequestBasicMessage requestBasicMessage,
            Map<String, String> prop, boolean sendCode, HttpServletRequest httpServletRequest) {
        ResponseBasicMessage rm = new ResponseBasicMessage();

        RLock lock = instrumentedObject.lockObject("user_lock_sou_" + requestBasicMessage.username, 10);
        try {
            rm.ifExistsResult = 0;
            if (requestBasicMessage.location != null) {
                rm.location = requestBasicMessage.location.toUpperCase();
            } else if (requestBasicMessage.locale != null) {
                try {
                    if (requestBasicMessage.locale.contains("-")) {
                        rm.location = requestBasicMessage.locale.split("-")[1].toUpperCase();
                    } else if (requestBasicMessage.locale.contains("_")) {
                        rm.location = requestBasicMessage.locale.split("_")[1].toUpperCase();
                    }
                } catch (Exception ex) {
                }
            }

            rm.username = requestBasicMessage.username;
            rm.usernameType = requestBasicMessage.usernameType;
            rm.tid = requestBasicMessage.tid;

            //No se puede cambiar el cliente
            final User cliente = userService.findByclientId(prop.get("clientId"));
            if (cliente == null) {
                log.error("No existe el clientId: [" + prop.get("clientId") + "]");
                return rm;
            }
            rm.locale = requestBasicMessage.locale;

            switch (requestBasicMessage.usernameType) {
                case EMAIL:
                    rm.username = filterCaractersToLowerCase(rm.username);
                    User user = userService.findByEmail(rm.username);
                    if (user != null) {
                        rm.ifExistsResult = 1;
                        if (user.getCredentialUserProperty().getUsername().getMsisdns() != null
                                && !user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                            rm.credentials.hasPhone = 1;
                        }
                        if (user.getCredentialUserProperty().getPassword() != null) {
                            rm.credentials.hasPassword = 1;
                        }
                        if (sendCode) {
                            sendEmailValidationCode(cliente, user, rm, prop);
                        }

                        try {
                            if (!user.getCredentialUserProperty().getUsername().getEmail(rm.username).getConfirmed()) {
                                rm.tid = requestBasicMessage.tid;
                                rm = createVerifyCodePage(rm);
                                if (rm.redirectUrl != null) {
                                    rm.redirectUrl = "/oauth" + rm.redirectUrl;
                                }
                                return rm;
                            }
                        } catch (Exception ex) {
                            log.error("Error al enviar PIN de validacion. " + ex, ex);
                        }
                    } else if (rm.username != null && rm.username.contains("@") && !"1".equals(requestBasicMessage.registry)) {
                        user = new User();
                        user.getCredentialUserProperty().getUsername().setEmail(rm.username, false);
                        String countryCode = clientCache.getTemporalData(requestBasicMessage.tid);
                        if (countryCode != null) {
                            Country c = countryService.findByCode(countryCode);
                            if (c != null) {
                                user.getProfileUserProperty().setCountry(c);
                            }
                        }
                        try {
                            userService.saveOrUpdate(user);
                            rm.ifExistsResult = 1;
                            rm.credentials.hasPassword = 0;
                            rm.tid = requestBasicMessage.tid;
                            rm = createVerifyCodePage(rm);
                            if (rm.redirectUrl != null) {
                                rm.redirectUrl = "/oauth" + rm.redirectUrl;
                            }
                            return rm;
                        } catch (Exception ex) {
                        }
                    }
                    break;
                case PHONE:
                    Country c = countryService.findByCode(rm.location);

                    String localLength = phprofilePropertiesService.getCommonProperty("msisdn."
                            + c.getCode().toLowerCase() + ".length.local");

                    if (localLength == null) {
                        log.warn("No existe en DB la configuracion: [msisdn." + c.getCode().toLowerCase() + ".length.local]");
                        rm.credentials.error = 5;
                        break;
                    }

                    Integer msisdnLengthLocal = Integer.parseInt(localLength);
                    rm.username = rm.username.replace(" ", "").replace("+", "");
                    if (!rm.username.startsWith(c.getPhoneCode().toString())) {
                        rm.username = c.getPhoneCode() + rm.username;
                    }
                    if (rm.username.length() > 10) {
                        if (rm.username.startsWith("5415")) {
                            rm.username = "5411" + rm.username.substring(4, rm.username.length());
                        } else if (rm.username.startsWith("54915")) {
                            rm.username = "54911" + rm.username.substring(5, rm.username.length());
                        }
                    }

                    if (rm.username.startsWith(c.getPhoneCode().toString())) {
                        rm.username = rm.username.substring(c.getPhoneCode().toString().length(), rm.username.length());
                    }

                    rm.username = rm.username.substring(Math.max(0, rm.username.length() - msisdnLengthLocal), rm.username.length());

                    if (rm.username.length() < msisdnLengthLocal) {
                        rm.credentials.error = 4;
                        break;
                    }

                    Holder<String> username = new Holder();
                    //Buscamos el MSISDN en Oauth
                    user = userService.findByMsisdn(c.getPhoneCode().toString(), rm.username, username);

                    if (user == null) {
                        //Buscamos el MSISDN en PaymentHubServer
                        try {
                            Subscription s = null;
                            try {
                                s = subscriptionService.getSubscriptionInformationFacadeRequest(rm.username, rm.location);
                            } catch (Exception ex) {
                                log.warn("No se pudo obtener la Suscripcion en PaymentHub. [" + rm.username + "][" + rm.location + "]. " + ex, ex);
                            }
                            if (s != null && s.getSubscriptionRegistry() != null) {
                                // log.info("Suscripcion en PaymentHub encontrada: [" + (s.getId() == null ? s : s.getId()) + "]");

                                user = new User();
                                user.getProfileUserProperty().setFirstName("User");
                                user.getCredentialUserProperty().getUsername()
                                        .setMsisdn(s.getUserAccount(), false,
                                                s.getSubscriptionRegistry().getSop().getProvider().getCountry(),
                                                s.getSubscriptionRegistry().getSop().getProvider());
                                user.getProfileUserProperty().setCountry(s.getSubscriptionRegistry().getSop().getProvider().getCountry());
                                userService.saveOrUpdate(user);
                                username.value = s.getUserAccount();
                                // log.info("Creado user: [" + user + "]");
                            }
                        } catch (Exception ex) {
                            log.error("Error al obtener Suscripcion en PaymentHub. [" + rm.username + "][" + rm.location + "]. " + ex, ex);
                        }
                    }

                    if (user != null) {
                        rm.username = username.value;
                        rm.ifExistsResult = 1;
                        rm.credentials.hasPhone = 1;
                        try {
                            if (user.getCredentialUserProperty().getUsername().getMsisdn(username.value).getCountry() == null
                                    && user.getCredentialUserProperty().getUsername().getMsisdn(username.value).getProvider() != null
                                    && user.getCredentialUserProperty().getUsername().getMsisdn(username.value).getProvider().getCountry() != null) {
                                user.getCredentialUserProperty().getUsername().getMsisdn(username.value).setCountry(user.getCredentialUserProperty().getUsername().getMsisdn(username.value).getCountry());
                                userService.saveOrUpdate(user);
                            }
                        } catch (Exception ex) {
                            log.error("Error al actualizar Country en User: [" + user + "]. " + ex, ex);
                        }

                        //Algo horrible para claro Peru 
                        //Solo funciona con ANIs en la DB
                        if (51 == c.getPhoneCode()) {
                            try {
                                Subscription s = subscriptionService.findByMSISDNAndCountry(rm.username, c);
                                if (s != null && "pe.claro.spiralis".equals(s.getSop().getProvider().getName())) {
                                    if (Subscription.Status.ACTIVE.equals(s.getStatus()) || Subscription.Status.PENDING.equals(s.getStatus())) {
                                        generateAuthenticationSessionCode(prop, cliente, user, rm, null, "null", httpServletRequest, null);
                                    } else {
                                        String productId = prop.get("productId");
                                        Product p = null;
                                        if (!Collections.isEmpty(cliente.getProducts())) {
                                            for (Product aux : cliente.getProducts()) {
                                                if (aux.getId().toString().equals(productId)) {
                                                    p = aux;
                                                }
                                            }
                                        }
                                        rm.redirectUrl = p.getProductSettings().get("oauth.redirect.pe.claro.spiralis");
                                    }
                                    rm.ifExistsResult = 1;
                                    return rm;
                                }
                            } catch (Exception ex) {
                                log.warn("No fue posible validar al usuario. [" + rm.username + "][" + rm.location + "]. " + ex, ex);
                            }
                        }

                        if (user.getCredentialUserProperty().getPassword() != null) {
                            rm.credentials.hasPassword = 1;
                        }

                        Msisdn mm = user.getCredentialUserProperty().getUsername().getMsisdn(rm.username);

                        String productId = prop.get("productId");
                        Product p = null;
                        if (!Collections.isEmpty(cliente.getProducts())) {
                            for (Product aux : cliente.getProducts()) {
                                if (aux.getId().toString().equals(productId)) {
                                    p = aux;
                                }
                            }
                        }

                        String prvdr = prop.get("prvdr_name");
                        String providerName = null;

                        if (p != null && mm != null && mm.getProvider() != null) {
                            providerName = mm.getProvider().getName();
                        }

                        if (p == null) {
                            User client = cliente.getPrincipal();
                            while (client.getPrincipal() != null) {
                                client = client.getPrincipal();
                            }
                            if (!Collections.isEmpty(client.getProducts())) {
                                for (Product aux : client.getProducts()) {
                                    if (aux.getId().toString().equals(productId)) {
                                        p = aux;
                                        break;
                                    }
                                }
                            }
                            if (mm != null && mm.getProvider() != null) {
                                providerName = mm.getProvider().getName();
                            }
                        }

                        if (rm.credentials.hasPassword == 0 && p != null && prvdr != null) {
                            providerName = p.getProductSettings().get("oauth." + prvdr + ".pin");
                            if (providerName != null && "true".equals(p.getProductSettings().get("oauth." + providerName + ".pin"))) {
                                //Envio de PIN por SMS del proveedor
                                rm.credentials.hasPin = 1;
                            }
                        } else if (rm.credentials.hasPassword == 0 && p != null && providerName != null
                                && "true".equals(p.getProductSettings().get("oauth." + providerName + ".pin"))) {
                            //Envio de PIN por SMS del proveedor
                            rm.credentials.hasPin = 1;
                        }
                    } else {
                        rm.username = c.getPhoneCode() + rm.username;
                        String productId = prop.get("productId");
                        Product p = null;
                        User client = cliente;
                        while (client.getPrincipal() != null) {
                            client = client.getPrincipal();
                        }
                        if (!Collections.isEmpty(client.getProducts())) {
                            for (Product aux : client.getProducts()) {
                                if (aux.getId().toString().equals(productId)) {
                                    p = aux;
                                    break;
                                }
                            }
                        }
                        String prvdr = prop.get("prvdr_name");
                        String providerName = null;
                        if (rm.credentials.hasPassword == 0 && p != null && prvdr != null) {
                            providerName = p.getProductSettings().get("oauth." + prvdr + ".pin");
                            if (providerName != null && "true".equals(p.getProductSettings().get("oauth." + providerName + ".pin"))) {
                                //Envio de PIN por SMS del proveedor
                                rm.credentials.hasPin = 1;
                            }
                        }
                    }
                    break;
            }
            return rm;
        } finally {
            instrumentedObject.unLockObject(lock);
        }
    }

    /**
     * Codigo de validacion para recuperar password
     */
    private void sendEmailValidationCode(User cliente, User user, ResponseBasicMessage rm, Map<String, String> prop) {
        String code = clientCache.getTemporalData(user.getId() + "_ECODE");
        if (code == null || "xx".equals(code)) {
            code = generateDigitCode(6).toUpperCase();
            // log.info("ValidationCode: [" + code + "] - User: [" + user.getId() + "]");
            try {
                String html = readResource("/tpl/email/validationCode.html", prop);
                html = html.replace("<scode></scode>", code);
                html = localeService.i18n(rm.locale, prop.get("prvdr"), html);
                emailUtility.sendHTMLEmail("no-reply@planeta.guru", rm.username,
                        localeService.i18n(rm.locale, prop.get("prvdr"), "<i18n>i18n.ix.px.email.validation.code.text.ypgsc</i18n>: " + code), html);
                rm.credentials.hasPin = 1;
                clientCache.setTemporalData(user.getId() + "_ECODE", code, 10);
                clientCache.setTemporalData(user.getId() + "_" + code, code, 15);
            } catch (Exception ex) {
                log.error("Error al enviar EMAIL. " + ex, ex);
            }
        } else {
            //  log.info("ValidationCode: [" + code + "] - User: [" + user.getId() + "]");
            rm.credentials.hasPin = 1;
        }
    }

    public ResponseBasicMessage createVerifyCodePage(ResponseBasicMessage rm) {
        //Validamos si el EMAIL ya esta registrado en DB como User
        rm.username = rm.username.toLowerCase();
        User user = userService.findByEmail(rm.username);
        if (user == null) {
            log.error("Error al obtener User con email: [" + rm.username + "]");
            return rm;
        }

        if (user.getCredentialUserProperty().getUsername().getEmail(rm.username).getConfirmed()) {
            rm.redirectUrl = "/signin/login/" + rm.tid + "?locale=" + rm.locale;
            return rm;
        }

        Map<String, Object> params = new HashMap();
        params.put("username", rm.username);
        params.put("tid", rm.tid);
        String verifyMail = null;
        try {
            Map<String, String> pp = Client.validateTokenData(genericPropertyService.getValueByKey("user.sign.secret"), rm.tid);
            verifyMail = pp.get("verifyMail");
        } catch (Exception ex) {
        }
        if (verifyMail == null) {
            verifyMail = "vcode";
        }
        params.put("verifyMail", verifyMail);

        try {
            String token = Client.getNewToken(genericPropertyService.getValueByKey("user.sign.secret"), user, params, 365);
            rm.redirectUrl = "/signin/secure/verify/" + token + "?locale=" + rm.locale;
        } catch (UnsupportedEncodingException ex) {
            log.error("Error al crear token de pagina de validacion de Email. " + ex, ex);
        }

        return rm;
    }

    /**
     * Busca el TID en la cookie, por si se pierde...
     *
     * @param request
     */
    public String getTID(HttpServletRequest request, String tid) {
        if (tid == null) {
            Cookie[] cooks = request.getCookies();
            if (cooks != null) {
                for (Cookie c : cooks) {
                    try {
                        if ("Us_PG_TK".equals(c.getName())) {
                            // cookie de oauth con TID
                            // log.info("TID: [" + tid + "]");
                            tid = c.getValue();
                        }
                    } catch (Exception ex) {
                        log.error("Error al validar cookie. " + ex, ex);
                    }
                }
            }
        }
        return tid;
    }

    public String ClaroVrValidation(Map<String, String> prop) throws Exception {
        //Validamos si es Tiaxa - ClaroVR en WiFi
        log.info("Oauth - ClaroVrValidation Prop: ["+prop+"]");
        Product pd = productService.findbyId(Long.parseLong(prop.get("productId")));

        // (1) Siempre toma mexico porque el product setting tiene tanto mx como ec, pero primero se pregunta por mx
        /*
        if ("true".equals(pd.getProductSettings().get("mx.claro.tiaxa.is.he"))) {
            return pd.getProductSettings().get("oauth.sso.mx.tiaxa");
        }
        if ("true".equals(pd.getProductSettings().get("ec.claro.tiaxa.is.he"))) {
            return pd.getProductSettings().get("oauth.sso.ec.tiaxa");
        }
        */

        // Para evitar (1) buscar el provider segun el prvdr_name que viene en prop
        String prvdr_name = prop.get("prvdr_name");
        if (prvdr_name != null) {
            String prvdr = phprofilePropertiesService.getCommonProperty(prvdr_name);
            Provider pr = providerService.findById(Long.parseLong(prvdr));
            if (pr != null) {
                try {
                    log.info("encuentra el is he para "+prop.get("prvdr_name")+": "+ "true".equals(pd.getProductSettings().get(pr.getName() + ".is.he")));
                    if ("true".equals(pd.getProductSettings().get(pr.getName() + ".is.he"))) {
                        String name = pr.getName();
                        String[] parts = name.split("\\.");
                        String shortName = parts[0] + '.' + parts[2];
                        log.info("log del provider name"+pr.getName());
                        log.info("log del para ver si el provider es = a claro ar smt "+pr.getName().equals("ar.claro.smt"));
                        if (pr.getName().equals("ar.claro.smt")) {
                            //shortName += prop.get("mdsp").equals("MDSP2000033320") ? ".semanal" : ".mensual";
                            log.info("shortname "+ prop.get("redirectUri").split("mdsp=")[1].split("&")[0]);
                            shortName += prop.get("redirectUri").split("mdsp=")[1].split("&")[0].equals("MDSP2000033320") ? ".semanal" : ".mensual";
                        }
                        log.info("shortname ar.claro.smt "+ shortName);
                        return pd.getProductSettings().get("oauth.sso." + shortName);
                    }
                } catch (Exception ex) {
                    log.error("Error al obtener oauth.sso" + ex, ex);
                }
            }
        } else {
            log.info("oauth - ClaroVrValidation prop no tine prvdr_name ["+prop+"]");
        }
        if ("true".equals(pd.getProductSettings().get("mx.claro.tiaxa.is.he"))) {
            return pd.getProductSettings().get("oauth.sso.mx.tiaxa");
        }
        if ("true".equals(pd.getProductSettings().get("ec.claro.tiaxa.is.he"))) {
            return pd.getProductSettings().get("oauth.sso.ec.tiaxa");
        }
        if ("true".equals(pd.getProductSettings().get("do.claro.tiaxa.is.he"))) {
            return pd.getProductSettings().get("oauth.sso.do.tiaxa");
        }

        return null;
    }
}
