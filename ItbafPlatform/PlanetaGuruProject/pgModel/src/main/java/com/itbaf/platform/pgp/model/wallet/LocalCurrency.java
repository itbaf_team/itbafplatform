/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.wallet;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.persistence.util.HibernateEquality;
import com.querydsl.core.annotations.Config;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 *
 * @author javier
 */
@Entity(name = "W_LOCAL_CURRENCY")
@Table(name = "W_LOCAL_CURRENCY")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class LocalCurrency implements Serializable {

    private static final long serialVersionUID = 20170904152702L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "CODE")
    private String code;
    @Column(name = "DESCRIPTION")
    private String description;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("name", name)
                .add("code", code)
                .add("description", description)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, code, description);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<LocalCurrency>()
                .over(LocalCurrency::getId)
                .over(LocalCurrency::getName)
                .over(LocalCurrency::getCode)
                .over(LocalCurrency::getDescription)
                .asserts(this, obj);
    }
}
