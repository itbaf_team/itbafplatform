/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adapter;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import com.itbaf.platform.paymenthub.adapter.consumer.RabbitMQAdnetworkMessageProcessorFactory;
import com.itbaf.platform.paymenthub.adapter.processor.AdapterProcessor;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.rmq.services.RabbitMQMemoryProcessorImpl;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain {

    private final PHInstance instance;
    private final ExecutorService cachedThreadPool;
    private final RabbitMQConsumer rabbitMQConsumer;
    private final BasicDataBaseConnection dataBaseConnection;
    private final RabbitMQMemoryProcessorImpl memoryProcessor;
    private final ScheduledExecutorService scheduledThreadPool;
    private final Map<AdapterProcessor.AdapterType, AdapterProcessor> adapterProcessorMap;
    private final RabbitMQAdnetworkMessageProcessorFactory rabbitMQMessageProcessorFactory;
    
    @Inject
    public PaymentHubMain(final PHInstance instance,
            final ExecutorService cachedThreadPool,
            final RabbitMQConsumer rabbitMQConsumer,
            final Set<AdapterProcessor> adapterProcessorSet,
            final BasicDataBaseConnection dataBaseConnection,
            final RabbitMQMemoryProcessorImpl memoryProcessor,
            final ScheduledExecutorService scheduledThreadPool,
            final RabbitMQAdnetworkMessageProcessorFactory rabbitMQMessageProcessorFactory) {
        this.instance = Validate.notNull(instance, "A PHInstance class must be provided");
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "An ExecutorService class must be provided");
        this.rabbitMQConsumer = Validate.notNull(rabbitMQConsumer, "A RabbitMQConsumer class must be provided");
        this.memoryProcessor = Validate.notNull(memoryProcessor, "A RabbitMQMemoryProcessorImpl class must be provided");
        this.dataBaseConnection = Validate.notNull(dataBaseConnection, "A BasicDataBaseConnection class must be provided");
        this.scheduledThreadPool = Validate.notNull(scheduledThreadPool, "A ScheduledExecutorService class must be provided");
        this.rabbitMQMessageProcessorFactory = Validate.notNull(rabbitMQMessageProcessorFactory, "A RabbitMQMessageProcessorFactory class must be provided");

        adapterProcessorMap = new HashMap();
        if (adapterProcessorSet != null) {
            log.info("adapterProcessorSet size: " + adapterProcessorSet.size());
            for (AdapterProcessor nr : adapterProcessorSet) {
                log.info("JF- AdapterProcessor item: [" + nr.adapterType + "] - [" + nr.getClass().getSimpleName() + "]");
                adapterProcessorMap.put(nr.adapterType, nr);
            }
        }
    }

    public void start() {
        log.info("Initializing app... ");
        try {
            rabbitMQConsumer.createFanoutChannel(CHACHE_MEMORY_UPDATE, memoryProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer '" + CHACHE_MEMORY_UPDATE + "'. " + ex, ex);
        }

        Set<Provider> providers = instance.getConfigurations().keySet();
        for (Provider p : providers) {
            ChannelQuantityProvider cqp = instance.getConfigurations().get(p);
            createConsumer(CommonFunction.ADAPTER_SMS_SEND + p.getName(),
                    cqp.getSmppConsumerQuantity(),
                    adapterProcessorMap.get(AdapterProcessor.AdapterType.MT_ADAPTER));
            createConsumer(CommonFunction.ADAPTER_SMS_RCV+"in."+ p.getName(),
                    cqp.getSubscriptionConsumerQuantity(),
                    adapterProcessorMap.get(AdapterProcessor.AdapterType.MO_ADAPTER));
        }
        log.info("Initializing app OK. ");
    }

    private RabbitMQConsumer createConsumer(String exchangeName, Integer consumerQuantity, AdapterProcessor notificationProcessor) {
        rabbitMQConsumer.createChannel(exchangeName, consumerQuantity, createRabbitMQMessageProcessor(notificationProcessor));
        return rabbitMQConsumer;
    }

    private RabbitMQMessageProcessor createRabbitMQMessageProcessor(AdapterProcessor messageProcessor) {
        return rabbitMQMessageProcessorFactory.create(messageProcessor);
    }

    public void stop() {
        rabbitMQConsumer.stopConsumers();
        stopThreads();
        dataBaseConnection.shutdownConnectionsPool();
        log.info("App destroyed. ");
    }

    public void stopThreads() {
        log.info("Finalizando Threads en 5 segundos... ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }
        try {
            log.info("Finalizando scheduledThreadPool ");
            scheduledThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            log.info("Esperando Threads por 2 segundos... ");
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        try {
            scheduledThreadPool.shutdownNow();
            log.info("Finalizado scheduledThreadPool ");
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdownNow();
        } catch (Exception ex) {
        }
    }

}
