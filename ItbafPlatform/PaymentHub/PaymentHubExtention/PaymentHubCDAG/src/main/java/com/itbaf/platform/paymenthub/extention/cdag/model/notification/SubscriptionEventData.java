package com.itbaf.platform.paymenthub.extention.cdag.model.notification;

import com.google.common.base.MoreObjects;
import java.util.Date;

@lombok.Getter
@lombok.Setter
@lombok.extern.log4j.Log4j2
public class SubscriptionEventData extends EventData {

    private String msisdn;
    private Date subscriptionDate;
    private String serviceId;
    private String keyword;
    private String shortcode;
    private ServiceType serviceType;
    private String pautaId;
    private String pautaStatus;

    public SubscriptionEventData(String data) {
        super(data);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("msisdn", msisdn)
                .add("subscriptionDate", subscriptionDate)
                .add("serviceId", serviceId)
                .add("keyword", keyword)
                .add("shortcode", shortcode)
                .add("serviceType", serviceType)
                .add("pautaId", pautaId)
                .add("pautaStatus", pautaStatus)
                .omitNullValues().toString();
    }

    @Override
    protected void parseData() {
        String[] values = this.getValues();

        this.msisdn = values[0];
        try {
            this.subscriptionDate = new Date(Long.parseLong(values[1] + "000"));
        } catch (Exception ex) {
            this.subscriptionDate = new Date();
        }
        this.serviceId = values[2];
        this.keyword = values[3];
        try {
            this.shortcode = values[4];
        } catch (Exception ex) {
        }
        try {
            this.serviceType = ServiceType.getserviceType(values[5]);
        } catch (Exception ex) {
        }

        try {
            this.pautaId = values[6];
        } catch (Exception ex) {
        }
        try {
            this.pautaStatus = values[7];
        } catch (Exception ex) {
        }
    }
}
