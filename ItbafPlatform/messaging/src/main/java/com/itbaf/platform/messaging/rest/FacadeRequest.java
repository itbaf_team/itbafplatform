/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.messaging.rest;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author javier
 */
public class FacadeRequest implements Serializable {

    public enum ContentType {
        TEST, IMAGE, GAME, THEME, VIDEO, VIDEOSTREAM, APP,
        PROMO, BUNDLE, SONG, RINGTONE, KARAOKE, AUDIOSTREAM
    }

    private static final long serialVersionUID = 20170823135401L;

    public Long sopId;
    public String userAccount;//ANI, EMAIL, ID de la suscripcion (externalId)
    public String service;
    public String operator;
    public String provider;
    public String serviceCode;//Code del servicio principal
    public String urlNotify; //Status service. Response asincronico. Cobros OK OnDemand
    public Object adTracking;
    public Object ammoParams;//Parametros AMMO RUG
    public Map<String, String> dataString;
    public String transactionId;
    public String contentUrl; //URL hacia la pagina de contenido o de redireccion
    public Date creationDate = new Date();

    /**
     * ***************SUBSCRIPTION****************
     */
    public String internalId; //ID unico del usuario del facade
    public String externalId; //ID unico de la suscripcion del lado del proveedor
    public String accountId; //ID unico del userAccount en el proveedor
    public String customerId; //ID unico del cliente (userAccounts) en el proveedor
    public String channel;
    public String pin;
    public String message;
    public String country;
    public String action;
    public String persist;
    /**
     * *******************************************
     */

    /**
     * *****************ON-DEMAND******************
     */
    public String currency;
    public BigDecimal amount;
    public String contentId; //ID del lado del cliente a este producto. (20 Caracteres Max SMT)
    public String contentName; // Descripcion del contenido. 
    public ContentType contentType;
    public String failureUrl;
    public String pendingUrl;
    public String pictureUrl; //Una imagen del producto-Juego
    public String contentDescription;//Una descripcion del producto-Juego

    /**
     * *******************************************
     */
    /**
     *
     */
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("creationDate", creationDate)
                .add("sopId", sopId)
                .add("userAccount", userAccount)
                .add("service", service)
                .add("operator", operator)
                .add("provider", provider)
                .add("serviceCode", serviceCode)
                .add("internalId", internalId)
                .add("externalId", externalId)
                .add("accountId", accountId)
                .add("customerId", customerId)
                .add("channel", channel)
                .add("pin", pin)
                .add("adTracking", adTracking)
                .add("message", message)
                .add("country", country)
                .add("action", action)
                .add("persist", persist)
                .add("urlNotify", urlNotify)
                .add("transactionId", transactionId)
                .add("amount", amount)
                .add("contentId", contentId)
                .add("contentDescription", contentName)
                .add("contentType", contentType)
                .add("dataString", dataString)
                .add("ammoParams", ammoParams)
                .add("contentUrl", contentUrl)
                .add("currency", currency)
                .add("pendingUrl", pendingUrl)
                .add("failureUrl", failureUrl)
                .add("pictureUrl", pictureUrl)
                .add("contentDescription", contentDescription)
                .omitNullValues().toString();
    }

}
