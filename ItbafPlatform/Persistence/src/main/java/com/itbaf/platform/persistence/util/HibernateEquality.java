package com.itbaf.platform.persistence.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import org.hibernate.Hibernate;

public final class HibernateEquality<T> {
	
	private List<Function<T,Object>> properties = new ArrayList<>();

	public HibernateEquality() {
		super();
	}
	
	public HibernateEquality(Function<T, Object> property) {
		this();
		this.over(property);
	}
	
	public HibernateEquality<T> over(Function<T, Object> property) {
		properties.add(property);
		return this;
	}

	public <R> HibernateEquality<T> over(Function<T, R> property, Function<R,Object> nested) {
		properties.add((T t) -> {
			if(t == null) return singleton();
			R result = property.apply(t);
			if(result == null) return singleton();
			return nested.apply(result);
		});
		return this;
	}

	protected Object singleton() {
		return new Object();
	}
	
	public <R,R1> HibernateEquality<T> over(Function<T,R> property, Function<R,R1> nested, Function<R1,Object> nested1) {
		properties.add((T t) -> {
			if(t == null) return singleton();
			R result = property.apply(t);
			if(result == null) return singleton();
			R1 result1 = nested.apply(result);
			if(result1 == null) return singleton();
			return nested1.apply(result1);
		});
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public boolean asserts(T lhs, Object rhs) {
		if(lhs == rhs) return true;
		if(lhs == null) return false;
		if(rhs == null) return false;
		if(Hibernate.getClass(lhs) != Hibernate.getClass(rhs)) return false;
		return properties.stream().allMatch(p -> Objects.equals(p.apply(lhs), p.apply((T)rhs)));	
	}

}
