/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.server.notification;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.messaging.queue.wallet.CurrencyRequest;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.OnDemandBilling;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.services.OnDemandManager;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.TariffService;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class OnDemandBillingNotificationProcessor implements NotificationProcessor {

    private final ObjectMapper mapper;
    private final TariffService tariffService;
    private final OnDemandManager onDemandManager;
    private final RabbitMQProducer rabbitMQProducer;
    private final InstrumentedObject instrumentedObject;
    private final PHProfilePropertiesService profilePropertiesService;
    private static final PaymentHubMessage.NotificationType notificationType = PaymentHubMessage.NotificationType.ONDEMAND_BILLING;

    @Inject
    public OnDemandBillingNotificationProcessor(final ObjectMapper mapper,
            final TariffService tariffService,
            final OnDemandManager onDemandManager,
            final RabbitMQProducer rabbitMQProducer,
            final InstrumentedObject instrumentedObject,
            final PHProfilePropertiesService profilePropertiesService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.tariffService = Validate.notNull(tariffService, "A TariffService class must be provided");
        this.onDemandManager = Validate.notNull(onDemandManager, "An OnDemandManager class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    @Override
    public PaymentHubMessage.NotificationType getNotificationType() {
        return notificationType;
    }

    @Override
    public Boolean process(String smsg, PaymentHubMessage msg, SOP sop) {
        Boolean control = null;
        RLock lock = null;
        try {
            lock = instrumentedObject.lockObject(msg.getUserAccount() + "_od_" + msg.getSopId(), 30);
        } catch (Exception ex) {
            log.error("Error al sincronizar notificacion. " + ex, ex);
        }

       // log.info("Procesando notificacion de Billing On Demand.");
        try {
            OnDemandBilling ob = onDemandManager.findOnDemandBillingById(msg.getInternalId());

            if (ob != null) {
                log.error("Ya existe un OnDemandBilling para ID: [" + msg.getInternalId() + "] - OnDemandBilling: [" + ob + "] - PaymentHubMessage: [" + msg + "]");
                return false;
            }

            ob = new OnDemandBilling();
            ob.setChargedDate(msg.getFromDate());
            ob.setCurrency(msg.getCurrencyId());
            ob.setFullAmount(msg.getFullAmount());
            ob.setId(msg.getInternalId());
            ob.setNetAmount(msg.getNetAmount());
            ob.setSop(sop);
            ob.setTransactionId(msg.getTransactionId());
            ob.setTransactionTracking(msg.getTransactionTracking());
            ob.setUserAccount(msg.getUserAccount());

            onDemandManager.ondemandBillingProcess(ob);
           // log.info("Notificacion de On Demand billing guardada: [" + ob + "]");

            try {
                String jsonResponse = mapper.writeValueAsString(ob);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(profilePropertiesService.getCommonProperty("rabbitmq.paymenthub.adn.billings.od"), jsonResponse);
                }
            } catch (Exception ex) {
                log.error("Error al enviar mensaje de billing On Demand ADN al rabbit. " + ex, ex);
            }

            try {
                CurrencyRequest cr = new CurrencyRequest();
                cr.currency = ob.getCurrency();
                cr.amount = ob.getFullAmount();
                cr.countryId = sop.getProvider().getCountry().getId();
                cr.date = ob.getChargedDate();
                cr.sopId = sop.getId();
                cr.transactionId = "PH_OB_" + ob.getId();
                cr.userAccount = ob.getUserAccount();
                if (msg.getTransactionTracking() != null) {
                    cr.tracking = msg.getTransactionTracking().adTracking;
                }
                if (msg.getTransactionTracking() != null) {
                    cr.description = msg.getTransactionTracking().contentName;
                }
                if (msg.getTariffId() != null) {
                    Tariff t = tariffService.findById(msg.getTariffId());
                    if (t != null) {
                        cr.localAmount = t.getLocalAmount();
                        cr.localCurrency = t.getLocalCurrency();
                        cr.tariffId = msg.getTariffId();
                        if (cr.currency == null) {
                            cr.currency = t.getCurrency();
                        }
                    }
                } else {
                    //aca hay q hacer magia para SOPs sin tarifas por On demand
                    //Quiza pasar desde el msg ya esos valores localAmount - localCurrency
                }

                String jsonRequest = mapper.writeValueAsString(cr);
                if (jsonRequest != null) {
                    rabbitMQProducer.messageSender(profilePropertiesService.getCommonProperty("rabbitmq.pg.wallet.funds"), jsonRequest);
                }
            } catch (Exception ex) {
                log.error("Error al enviar mensaje de billing OnDemand Wallet al rabbit. " + ex, ex);
            }
            control = true;
        } catch (Exception ex) {
            log.error("Error al procesar notificacion de cobro On Demand: [" + msg + "]. " + ex, ex);
        }
        instrumentedObject.unLockObject(lock);
        return control;
    }

}
