/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.messaging.rest.pg;

import com.google.common.base.MoreObjects;
import java.util.List;

/**
 *
 * @author JF
 */
public class PGMessageRequest {

    public String clientId;
    public String userToken;
    /**
     * ID unico del lado del cliente para un usuario... Un ID de promo... ID de
     * un juego... Se puede repetir entre usuarios ya q puede ser el mismo ID de
     * un juego comprado.. De esta forma se garantiza no adicionar o eliminar
     * creditos para la misma tracsaccion
     */
    public String transactionId;
    public Credit credit;
    public Credit virtualCredit;
    public Object clientData;
    public String description;
    public List<Long> userList;
    ////////////////////////////////////////
    public Long susbcriptionRegistryId;
    public String userAccount;
    public Boolean searchProvider;
    // Se busca SR.ID de ultima suscripcion realizada en la ultima hora
    public Boolean srIdTransaction;
    ////////////////////////////////////////
    public String countryCode;
    public String service;
    public String operator;
    public String provider;
    ////////////////////////////////////////

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("susbcriptionRegistryId", susbcriptionRegistryId)
                .add("userAccount", userAccount)
                .add("clientId", clientId)
                .add("userToken", userToken)
                .add("transactionId", transactionId)
                .add("credit", credit)
                .add("virtualCredit", virtualCredit)
                .add("clientData", clientData)
                .add("description", description)
                .add("searchProvider", searchProvider)
                .add("countryCode", countryCode)
                .add("service", service)
                .add("operator", operator)
                .add("provider", provider)
                .add("userList", userList)
                .add("srIdTransaction", srIdTransaction)
                .omitNullValues().toString();
    }
}
