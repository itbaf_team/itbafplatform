/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model.ph;

import com.itbaf.platform.paymenthub.model.ph.PHProfileProperties;
import com.itbaf.platform.paymenthub.model.ph.QPHProfileProperties;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.querydsl.jpa.impl.JPAQuery;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
 
/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAPHProfilePropertiesRepository extends AbstractJPARepositorySession<PHProfileProperties, Long> {

    @Inject
    public JPAPHProfilePropertiesRepository(final Provider<EntityManager> entityManagerProvider) {
        super(PHProfileProperties.class, entityManagerProvider);
    }

    public List<PHProfileProperties> findByProfileAndProvider(String profile, Long providerId) {
        if (profile == null || providerId == null) {
            String error = "Uno de los parametros es nulo!! profile: [" + profile + "] - provider: [" + providerId + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        profile = profile.toLowerCase();
        QPHProfileProperties pp = QPHProfileProperties.pHProfileProperties;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<PHProfileProperties> pro = query.select(pp).from(pp).
                where(ObjectPredicates.combine(pp.phProfile().name.eq(profile),
                        pp.provider().id.eq(providerId))).fetch();

        if (pro == null) {
            return new ArrayList();
        }

        return pro;
    }

    public PHProfileProperties getValueByProfileProviderKey(String profile, Long providerId, String key) {
        if (profile == null || providerId == null || key == null) {
            String error = "Uno de los parametros es nulo!! profile: [" + profile + "] - provider: [" + providerId + "] - key: [" + key + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        profile = profile.toLowerCase();
        QPHProfileProperties pp = QPHProfileProperties.pHProfileProperties;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        PHProfileProperties pro = query.select(pp).from(pp).
                where(ObjectPredicates.combine(pp.phProfile().name.eq(profile),
                        pp.provider().id.eq(providerId),
                        pp.key.eq(key))).fetchFirst();

        return pro;
    }

    public PHProfileProperties getValueByProfileProviderKey(String profile, String provider, String key) {
        if (profile == null || provider == null || key == null) {
            String error = "Uno de los parametros es nulo!! profile: [" + profile + "] - provider: [" + provider + "] - key: [" + key + "]";
            log.error(error);
            throw new RuntimeException(error);
        }
        profile = profile.toLowerCase();
        provider = provider.toLowerCase();
        QPHProfileProperties pp = QPHProfileProperties.pHProfileProperties;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        PHProfileProperties pro = query.select(pp).from(pp).
                where(ObjectPredicates.combine(pp.phProfile().name.eq(profile),
                        pp.provider().name.eq(provider),
                        pp.key.eq(key))).fetchFirst();

        return pro;
    }

}
