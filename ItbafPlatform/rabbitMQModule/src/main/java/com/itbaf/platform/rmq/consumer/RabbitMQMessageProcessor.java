/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.rmq.consumer;

/**
 *
 * @author javier
 */
public interface RabbitMQMessageProcessor {

    /**
     * @return NULL. Por alguna razon no se proceso message. Se reencola........
     * TRUE.Mensaje procesado satisfactoriamente................................
     * FALSE. Mensaje procesado y descartable...................................
     */
    public Boolean processMessage(String message);
}
