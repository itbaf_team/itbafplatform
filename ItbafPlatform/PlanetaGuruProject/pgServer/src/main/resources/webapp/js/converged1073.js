var router = new Navigo(window.location.protocol + "//" + window.location.host, false);

!function () {
    router.on({'/account/': accountInfo,
    '/pg/account/': accountInfo
    }).resolve();
}();


function accountInfo(params, query) {
    

    html = tmpl($("#profileInfoScript").html(), {"params": uparams});
    $("#profileInfo-module").html(html);

    html = tmpl($("#balanceModuleScript").html(), {"params": uparams});
    $("#balance-module").html(html);

    html = tmpl($("#totalAvailableModuleScript").html(), {"params": uparams});
    $("#totalAvalilable-module").html(html);

    html = tmpl($("#transactionsModuleScript").html(), {"params": uparams});
    $("#transactions-module").html(html);
}

