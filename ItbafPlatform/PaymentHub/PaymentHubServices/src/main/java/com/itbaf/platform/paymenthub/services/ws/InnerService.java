/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.ws;

import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import javax.ws.rs.core.Response;

/**
 *
 * @author javier
 */
public interface InnerService {

    public Response getSubscriptionStatus(Subscription subscription);

    public Response subscribe(Subscription subscription);

    public Response userAccountNormalizer(Subscription subscription);

    public Response unsubscribe(Subscription subscription);

    public Response sendPin(Subscription subscription);

    public Response validatePin(Subscription subscription, String pin);

    public Response sendSMS(Subscription subscription);
    
    public Response charge(OnDemandRegistry or);

}
