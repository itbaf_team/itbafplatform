/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.smsc.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */

public class ValueResponse {

    public String msisdn;
    public String idServicio;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("msisdn", msisdn)
                .add("idServicio", idServicio)
                .omitNullValues().toString();
    }
}
