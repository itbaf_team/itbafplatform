#!/bin/bash

_ARTIFACTORY_USR=itbafplatform_user
_ARTIFACTORY_PWD=4ve52xcBNMqdb34VxYoNkNPj
_BRANCH_NAME=java10_migration
_REPO_NAME=git@bitbucket.org:itbaf_team/itbafplatform.git

gcloud builds submit --substitutions \
        _BRANCH_NAME=${_BRANCH_NAME},_REPO_NAME=${_REPO_NAME},_ARTIFACTORY_USR=${_ARTIFACTORY_USR},_ARTIFACTORY_PWD=${_ARTIFACTORY_PWD} \
        --config ./cloudbuild-all.yaml \
        ./secrets/
