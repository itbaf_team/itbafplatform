
package com.itbaf.platform.paymenthub.extention.cdag.ws.subscription;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="suscripcionId" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="fechaDesde" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaHasta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="proveedorId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="credentials" type="{urn:SendatelSoaService}securityInfo"/>
 *         &lt;element name="params" type="{urn:SendatelSoaService}arrayOfExtraParam"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "suscripcionId",
    "fechaDesde",
    "fechaHasta",
    "proveedorId",
    "credentials",
    "params"
})
@XmlRootElement(name = "ListarSuscripciones", namespace = "urn:SendatelSuscripcionesService")
public class ListarSuscripciones {

    @XmlElement(type = Integer.class)
    protected List<Integer> suscripcionId;
    @XmlElement(required = true)
    protected String fechaDesde;
    @XmlElement(required = true)
    protected String fechaHasta;
    protected int proveedorId;
    @XmlElement(required = true)
    protected SecurityInfo credentials;
    @XmlElement(required = true)
    protected ArrayOfExtraParam params;

    /**
     * Gets the value of the suscripcionId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the suscripcionId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSuscripcionId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getSuscripcionId() {
        if (suscripcionId == null) {
            suscripcionId = new ArrayList<Integer>();
        }
        return this.suscripcionId;
    }

    /**
     * Gets the value of the fechaDesde property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaDesde() {
        return fechaDesde;
    }

    /**
     * Sets the value of the fechaDesde property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaDesde(String value) {
        this.fechaDesde = value;
    }

    /**
     * Gets the value of the fechaHasta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaHasta() {
        return fechaHasta;
    }

    /**
     * Sets the value of the fechaHasta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaHasta(String value) {
        this.fechaHasta = value;
    }

    /**
     * Gets the value of the proveedorId property.
     * 
     */
    public int getProveedorId() {
        return proveedorId;
    }

    /**
     * Sets the value of the proveedorId property.
     * 
     */
    public void setProveedorId(int value) {
        this.proveedorId = value;
    }

    /**
     * Gets the value of the credentials property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityInfo }
     *     
     */
    public SecurityInfo getCredentials() {
        return credentials;
    }

    /**
     * Sets the value of the credentials property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityInfo }
     *     
     */
    public void setCredentials(SecurityInfo value) {
        this.credentials = value;
    }

    /**
     * Gets the value of the params property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public ArrayOfExtraParam getParams() {
        return params;
    }

    /**
     * Sets the value of the params property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public void setParams(ArrayOfExtraParam value) {
        this.params = value;
    }

}
