/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.QProvider;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.querydsl.jpa.impl.JPAQuery;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAProviderRepository extends AbstractJPARepositorySession<Provider, Long> {

    @Inject
    public JPAProviderRepository(final javax.inject.Provider<EntityManager> entityManagerProvider) {
        super(Provider.class, entityManagerProvider);
    }

    public Provider findByName(String name) {
        if (name == null) {
            String error = "El nombre del proveedor es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }
        name = name.toLowerCase();
        QProvider provider = QProvider.provider;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Provider p = query.select(provider).from(provider).
                where(provider.name.eq(name)).fetchFirst();

        return p;
    }

}
