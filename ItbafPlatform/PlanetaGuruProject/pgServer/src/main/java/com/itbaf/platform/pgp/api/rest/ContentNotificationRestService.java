package com.itbaf.platform.pgp.api.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageRequest;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.paymenthub.commons.stats.model.StatsMessage;
import com.itbaf.platform.pgp.api.handler.ApiProfileServiceProcessorHandler;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import io.jsonwebtoken.impl.DefaultClaims;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;

@Path("/api/content")
@Singleton
@lombok.extern.log4j.Log4j2
public class ContentNotificationRestService extends CommonRestService {

    private final ApiProfileServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public ContentNotificationRestService(ApiProfileServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "An ApiProfileServiceProcessorHandler class must be provided");
    }

    @POST
    @Path("/notification")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response notification(@Context HttpServletRequest request,
            StatsMessage messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.content.notification");
        } catch (Exception ex) {
        }
        log.info("notification. StatsMessage: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();
        try {
            serviceProcessorHandler.sendNotificationContent(messageRequest);
        } catch (Exception ex) {
        }

        rm.meta.status = Meta.Status.ok;
        rm.meta.code = 200;
        return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/friend/{userid}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFriendProfile(@Context HttpServletRequest request,
            @PathParam("userid") final String userid,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.profile.friend." + userid);
        } catch (Exception ex) {
        }
        log.info("getFriendProfile. PGMessageRequest: [" + messageRequest + "] - userid: [" + userid + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            DefaultClaims defaultClaims = new DefaultClaims();
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = serviceProcessorHandler.authenticateUser(client, messageRequest.userToken, defaultClaims);

            if (user == null || userid == null || userid.length() < 1) {
                rm.data.message = "Token de usuario invalido. Forzar el inicio de sesion de usuario. ";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_user_token;
            } else {
                UserProfile up = serviceProcessorHandler.getUserProfileFromUserFriend(client, user, userid);
                if (up == null) {
                    rm.meta.code = 400;
                    rm.meta.status = Meta.Status.invalid_request;
                } else {
                    rm.meta.code = 200;
                    rm.meta.status = Meta.Status.ok;
                    rm.data.object = up;
                }
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener informacion del perfil. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/list")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProfileList(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.profile.list");
        } catch (Exception ex) {
        }
        log.info("getProfileList. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            DefaultClaims defaultClaims = new DefaultClaims();
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);

            if (messageRequest.userList == null || messageRequest.userList.isEmpty()) {
                rm.data.message = "Lista de usuarios inválida. ";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_request;
            } else {
                Set<UserProfile> upl = serviceProcessorHandler.getProfileList(client, messageRequest.userList);
                if (upl == null || upl.isEmpty()) {
                    rm.meta.code = 400;
                    rm.meta.status = Meta.Status.invalid_request;
                } else {
                    rm.meta.code = 200;
                    rm.meta.status = Meta.Status.ok;
                    rm.data.object = upl;
                }
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener informacion del perfil. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/data")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getData(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.profile.get.data");
        } catch (Exception ex) {
        }
        log.info("getData. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            DefaultClaims defaultClaims = new DefaultClaims();
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = serviceProcessorHandler.authenticateUser(client, messageRequest.userToken, defaultClaims);

            if (user == null) {
                rm.data.message = "Token de usuario invalido. Forzar el inicio de sesion de usuario. ";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_user_token;
            } else {
                UserProfile up = serviceProcessorHandler.getUserData(client, user, defaultClaims, messageRequest.clientData);
                rm.meta.code = 200;
                rm.meta.status = Meta.Status.ok;
                rm.data.object = up;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener informacion del perfil. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @PUT
    @Path("/data")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setData(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.profile.set.data");
        } catch (Exception ex) {
        }
        log.info("setData. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            DefaultClaims defaultClaims = new DefaultClaims();
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = serviceProcessorHandler.authenticateUser(client, messageRequest.userToken, defaultClaims);

            if (user == null) {
                rm.data.message = "Token de usuario invalido. Forzar el inicio de sesion de usuario. ";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_user_token;
            } else {
                UserProfile up = serviceProcessorHandler.setUserData(client, user, defaultClaims, messageRequest.clientData);
                rm.meta.code = 200;
                rm.meta.status = Meta.Status.ok;
                rm.data.object = up;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener informacion del perfil. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

}
