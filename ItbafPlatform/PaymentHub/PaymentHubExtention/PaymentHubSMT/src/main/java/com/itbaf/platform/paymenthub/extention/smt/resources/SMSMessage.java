/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.smt.resources;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import java.util.Date;

/**
 *
 * @author JF
 */
public class SMSMessage {

    public String from;
    public String to;
    public String message;
    public Date date;
    public Provider provider;
    public SOP sop;
    public String thread = Thread.currentThread().getName();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("from", from)
                .add("to", to)
                .add("date", date)
                .add("message", message)
                .add("provider", provider)
                .add("sop", sop)
                .add("thread", thread)
                .omitNullValues().toString();
    }
}
