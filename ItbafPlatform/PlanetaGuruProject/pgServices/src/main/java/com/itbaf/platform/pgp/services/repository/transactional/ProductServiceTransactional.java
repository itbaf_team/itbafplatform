/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.pgp.model.Product;
import com.itbaf.platform.pgp.repository.jpa.model.JPAProductRepository;
import com.itbaf.platform.pgp.services.repository.ProductService;
import java.util.List;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class ProductServiceTransactional implements ProductService {

    private final JPAProductRepository productRepository;

    @Inject
    public ProductServiceTransactional(final JPAProductRepository productRepository) {
        this.productRepository = Validate.notNull(productRepository, "A JPAProductRepository class must be provided");

    }

    @Override
    public Product findbyId(Long id) throws Exception {
        return productRepository.findById(id);
    }

    @Override
    public List<Product> getAll() throws Exception {

        Object obj = MainCache.memory48Hours().getIfPresent("all_products");

        if (obj == null) {
            List<Product> products = productRepository.getAll();
            if (products != null && !products.isEmpty()) {
                MainCache.memory48Hours().put("all_products", products);
                return products;
            }
        }

        return (List<Product>) obj;

    }

}
