/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.personal.sso;

import com.itbaf.platform.pgp.services.personal.sso.model.UserSSOPersonal;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.paymenthub.model.Provider;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class SSOPersonalServices {

    private final ObjectMapper mapper;
    private final RequestClient requestClient;
    private final String API_SSO_PERSONAL_URL = "https://sso.personal.com.ar";

    @Inject
    public SSOPersonalServices(final ObjectMapper mapper,
            final RequestClient requestClient) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
    }

    public boolean tokenValidate(String cookieToken) {

        String response = null;
        String url = null;
        try {
            url = API_SSO_PERSONAL_URL + "/openam/identity/isTokenValid?tokenid=" + cookieToken;
            response = requestClient.requestSimpleGet(url);

            log.info("tokenValidate. URL: [" + url + "]. PERSONAL SSO response: [" + response + "]");
            return response.contains("boolean=true");
        } catch (Exception ex) {
            log.error("Error tokenValidate. URL: [" + url + "]. PERSONAL SSO response: [" + response + "]");
        }

        return false;
    }

    public UserSSOPersonal getUserSSOPersonal(String cookieToken, Provider provider) {

        String response = null;
        String url = null;
        try {
            url = API_SSO_PERSONAL_URL
                    + "/openam/identity/attributes?&attributenames=DatosLineaNroLinea&attributenames=DatosLineaNroContrato&attributenames=DatosPagoApellidoResPago&attributenames=DatosPagoEmail&subjectid=" + cookieToken;
            response = requestClient.requestSimpleGet(url);

            log.info("getUserSSOPersonal. URL: [" + url + "]. PERSONAL SSO response: [" + response + "]");
            return UserSSOPersonal.getUserAttributeCDAG(response, provider);
        } catch (Exception ex) {
            log.error("Error getUserSSOPersonal. URL: [" + url + "]. PERSONAL SSO response: [" + response + "]");
        }

        return null;
    }

}
