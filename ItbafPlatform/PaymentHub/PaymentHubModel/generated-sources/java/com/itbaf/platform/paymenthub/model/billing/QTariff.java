package com.itbaf.platform.paymenthub.model.billing;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QTariff is a Querydsl query type for Tariff
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTariff extends EntityPathBase<Tariff> {

    private static final long serialVersionUID = 1542435359L;

    public static final QTariff tariff = new QTariff("tariff");

    public final StringPath currency = createString("currency");

    public final StringPath externalId = createString("externalId");

    public final NumberPath<Integer> frecuency = createNumber("frecuency", Integer.class);

    public final StringPath frecuencyType = createString("frecuencyType");

    public final NumberPath<java.math.BigDecimal> fullAmount = createNumber("fullAmount", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> localAmount = createNumber("localAmount", java.math.BigDecimal.class);

    public final StringPath localCurrency = createString("localCurrency");

    public final NumberPath<java.math.BigDecimal> netAmount = createNumber("netAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> percentage = createNumber("percentage", java.math.BigDecimal.class);

    public final EnumPath<com.itbaf.platform.paymenthub.model.Status> status = createEnum("status", com.itbaf.platform.paymenthub.model.Status.class);

    public final EnumPath<Tariff.TariffType> tariffType = createEnum("tariffType", Tariff.TariffType.class);

    public QTariff(String variable) {
        super(Tariff.class, forVariable(variable));
    }

    public QTariff(Path<? extends Tariff> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTariff(PathMetadata metadata) {
        super(Tariff.class, metadata);
    }

}

