/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.test.instance;

import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.modules.GuiceDefaultConfigModule;
import com.itbaf.platform.pgp.repository.module.GuiceRepositoryConfigModule;
import com.itbaf.platform.pgp.services.module.PGGuiceServiceConfigModule;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumerImpl;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import com.itbaf.platform.rmq.producer.RabbitMQProducerImpl;
import java.util.Arrays;
import java.util.Map;

/**
 *
 * @author jordonez
 */
public class GuiceConfigModule extends GuiceDefaultConfigModule {
    

    public GuiceConfigModule(final Map<String, String> args, CommonDaemon commonDaemon) {
        super(args,commonDaemon);
    }

    @Override
    protected void configure() {
        super.configure();

        //Binds
        //Modulos de aplicaciones
        install(new GuiceRepositoryConfigModule());
        install(new PGGuiceServiceConfigModule());
        bind(RabbitMQProducer.class).to(RabbitMQProducerImpl.class).asEagerSingleton();
        bind(RabbitMQConsumer.class).to(RabbitMQConsumerImpl.class).asEagerSingleton();
        //install(new GuiceRestServicesConfigModule());

        log.info("Configuracion iniciada. " + Arrays.toString(args.entrySet().toArray()));
    }

}
