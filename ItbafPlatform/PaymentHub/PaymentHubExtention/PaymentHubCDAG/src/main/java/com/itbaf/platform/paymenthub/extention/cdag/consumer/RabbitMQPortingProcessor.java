/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.cdag.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.extention.cdag.resources.CDAGResources;
import com.itbaf.platform.paymenthub.messaging.smpp.adapter.AdapterSMSMessage;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import io.jsonwebtoken.lang.Collections;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQPortingProcessor implements RabbitMQMessageProcessor {

    private final ObjectMapper mapper;
    private final CDAGResources cdagResources;
    private final SubscriptionService subscriptionService;
    private final SubscriptionManager subscriptionManager;

    @Inject
    public RabbitMQPortingProcessor(final ObjectMapper mapper,
            final CDAGResources cdagResources,
            final SubscriptionService subscriptionService,
            final SubscriptionManager subscriptionManager) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.cdagResources = Validate.notNull(cdagResources, "A CDAGResources class must be provided");
        this.subscriptionService = Validate.notNull(subscriptionService, "A SubscriptionService class must be provided");
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
    }

    @Override
    public Boolean processMessage(String message) {
        log.info("Procesando Porting");
        try {
            String tid = Thread.currentThread().getName() + ".mo";
            Thread.currentThread().setName(tid);
        } catch (Exception ex) {
        }
        try {
            AdapterSMSMessage am = mapper.readValue(message, AdapterSMSMessage.class);
            List<Subscription> ls = subscriptionService.getActiveSubscriptions(am.sendSmsRequest.getDestinationAddr(), am.providerName);
            if (!Collections.isEmpty(ls)) {
                for (Subscription s : ls) {
                    unsubscription(s, am);
                }
            }
            return true;
        } catch (Exception ex) {
            log.error("Error al procesar Porting Message: [" + message + "]. " + ex, ex);
        }
        return null;
    }

    private void unsubscription(Subscription subscription, AdapterSMSMessage am) throws Exception {

        cdagResources.unsubscribe(subscription.getUserAccount(), subscription.getSop());
        subscription.setStatus(Subscription.Status.PORTING);
        subscription.setUnsubscriptionDate(new Date());
        subscription.getSubscriptionRegistry().setUnsubscriptionDate(subscription.getUnsubscriptionDate());
        subscription.getSubscriptionRegistry().setOriginUnsubscription("SMS:MT:ERROR_" + am.sendSmsResponse.getResultCode());
        subscriptionManager.subscriptionProcess(subscription);
    }
}
