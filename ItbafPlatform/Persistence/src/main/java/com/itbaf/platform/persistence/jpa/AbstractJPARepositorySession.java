/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.persistence.jpa;

import java.util.List;
import javax.inject.Provider;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.annotations.Where;

/**
 *
 * @author JF
 * @param <T>
 * @param <K>
 */
public abstract class AbstractJPARepositorySession<T, K> {

    private final Provider<EntityManager> entityManagerProvider;
    private final Class<T> entityClass;

    public AbstractJPARepositorySession(Class<T> entityClass,
            final Provider<EntityManager> entityManagerProvider) {
        this.entityClass = entityClass;
        this.entityManagerProvider = entityManagerProvider;
    }

    protected EntityManager getEntityManager() {
        return entityManagerProvider.get();
    }

    public T findById(K id) throws Exception {

        T o = null;
        try {
            o = getEntityManager().find(entityClass, id);
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return o;
    }

    public List<T> getAll() throws Exception {
        List<T> l = null;
        String where = "";
        try {
            String tableName = (entityClass.getAnnotation(Entity.class)).name();
            if (null != entityClass.getAnnotation(Where.class)) {
                where = " WHERE " + (entityClass.getAnnotation(Where.class)).clause();
            }
            Query query = getEntityManager().createQuery("SELECT t FROM " + tableName + " t " + where, entityClass);
            l = query.getResultList();
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return l;
    }

    public List<T> getAll(int offset, int max) throws Exception {
        List<T> l = null;
        String where = "";
        try {
            String tableName = (entityClass.getAnnotation(Entity.class)).name();
            if (null != entityClass.getAnnotation(Where.class)) {
                where = " WHERE " + (entityClass.getAnnotation(Where.class)).clause();
            }
            Query query = getEntityManager().createQuery("SELECT t FROM " + tableName + " t " + where, entityClass);
            if (offset >= 0) {
                query.setFirstResult(offset);
            }
            if (max > 0) {
                query.setMaxResults(max);
            }
            l = query.getResultList();
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return l;
    }

    public void save(T value) throws Exception {
        getEntityManager().persist(value);
    }

    public void refresh(T value) throws Exception {
        getEntityManager().refresh(value);
    }

    public T merge(T value) throws Exception {
        T merged = (T) getEntityManager().merge(value);
        return merged;
    }

    public void delete(final K id) throws Exception {
        final T instance = findById(id);
        if (instance != null) {
            getEntityManager().remove(instance);
        }
    }

    public void remove(final T entity) throws Exception {
        getEntityManager().remove(entity);
    }

}
