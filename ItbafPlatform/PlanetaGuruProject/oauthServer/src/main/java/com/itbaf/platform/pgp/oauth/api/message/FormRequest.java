/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth.api.message;

import com.google.common.base.MoreObjects;
import java.util.Date;

/**
 *
 * @author JF
 */
public class FormRequest {

    public String firstname;
    public String lastname;
    public String email;
    public String password;
    public String rpassword;
    public String countryCode;
    public Date birthdate;
    public String gender;
    public String phoneCountryCode;
    public String phoneNumber;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("firstname", firstname)
                .add("lastname", lastname)
                .add("email", email)
                .add("password", password)
                .add("rpassword", rpassword)
                .add("countryCode", countryCode)
                .add("birthdate", birthdate)
                .add("gender", gender)
                .add("phoneCountryCode", phoneCountryCode)
                .add("phoneNumber", phoneNumber)
                .omitNullValues().toString();
    }
}
