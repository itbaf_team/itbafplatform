/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model.ph;

import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
public class JPAPHInstanceRepository extends AbstractJPARepositorySession<PHInstance, Long> {

    @Inject
    public JPAPHInstanceRepository(final Provider<EntityManager> entityManagerProvider) {
        super(PHInstance.class, entityManagerProvider);
    }

}
