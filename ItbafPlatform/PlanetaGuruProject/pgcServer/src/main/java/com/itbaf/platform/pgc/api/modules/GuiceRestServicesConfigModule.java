package com.itbaf.platform.pgc.api.modules;

import com.google.inject.Singleton;
import com.itbaf.platform.commons.filter.RequestFilter;
import com.itbaf.platform.pgc.api.rest.ApiAccountRestService;
import com.itbaf.platform.pgc.api.rest.ApiUserRestService;
import com.itbaf.platform.pgp.commons.rest.ManagerRestServiceImpl;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import java.util.HashMap;

public class GuiceRestServicesConfigModule extends JerseyServletModule {

    @Override
    protected void configureServlets() {
        super.configureServlets();

        HashMap<String, String> options = new HashMap();
        options.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

        //Filters
        filter("/*").through(RequestFilter.class);

        //Servlets
        serve("/rest/*").with(GuiceContainer.class, options);

        bind(ManagerRestServiceImpl.class).in(Singleton.class);
        bind(ApiUserRestService.class).in(Singleton.class);
        bind(ApiAccountRestService.class).in(Singleton.class);
    }

}
