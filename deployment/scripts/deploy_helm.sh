#!/bin/sh
set -e

# Parse command-line options

# Option strings
SHORT=e:r:c:i:n:g:
LONG=environment-name:,helm-release-name:,helm-chart-name:,container-image:,kube-ns:,git-commit:

# read the options
OPTS=$(getopt --options $SHORT --long $LONG --name "$0" -- "$@")

if [ $? != 0 ] ; then echo "Failed to parse options...exiting." >&2 ; exit 1 ; fi

eval set -- "$OPTS"

# extract options and their arguments into variables.
while true ; do
  case "$1" in
    -e | --environment-name )
      ENVIRONMENT_NAME="$2"
      echo "ENVIRONMENT_NAME: $2"
      shift 2
      ;;
    -r | --helm-release-name )
      HELM_RELEASE_NAME="$2"
      echo "HELM_RELEASE_NAME: $2"
      shift 2
      ;;
    -c | --helm-chart-name )
      HELM_CHART_NAME="$2"
      echo "HELM_CHART_NAME: $2"
      shift 2
      ;;
    -i | --container-image )
      CONTAINER_IMAGE="$2"
      echo "CONTAINER_IMAGE: $2"
      shift 2
      ;;
    -n | --kube-ns )
      KUBE_NS="$2"
      echo "KUBE_NS: $2"
      shift 2
      ;;
    -g | --git-commit )
      GIT_COMMIT="$2"
      echo "GIT_COMMIT: $2"
      shift 2
      ;;
    -- )
      shift ;
      break
      ;;
  esac
done

#helm init --service-account tiller --client-only || true
helm upgrade --install ${HELM_RELEASE_NAME} ./deployment/charts/${HELM_CHART_NAME} \
             --set image.tag=${GIT_COMMIT} \
             --set image.repository=${CONTAINER_IMAGE} \
             -f ./deployment/config/${HELM_RELEASE_NAME}/chart-values-${HELM_RELEASE_NAME}-$(echo $ENVIRONMENT_NAME | awk '{print tolower($0)}').yaml \
             --force \
             --namespace ${KUBE_NS} \
             --description "${GIT_COMMIT}"
