/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties.attributes;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class Email extends UsernameAttribute implements Serializable {

    private static final long serialVersionUID = 20181207172503L;

    private String email;

    public Email() {
        date = new Date();
        primary = false;
        confirmed=false;
    }

    public Email(String email, Boolean confirmed) {
        date = new Date();
        this.email = email;
        this.confirmed = confirmed;
        this.primary = false;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("email", email)
                .add("confirmed", confirmed)
                .add("primary", primary)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, confirmed, primary);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Email other = (Email) obj;
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.confirmed, other.confirmed)) {
            return false;
        }
        if (!Objects.equals(this.primary, other.primary)) {
            return false;
        }
        return true;
    }

}
