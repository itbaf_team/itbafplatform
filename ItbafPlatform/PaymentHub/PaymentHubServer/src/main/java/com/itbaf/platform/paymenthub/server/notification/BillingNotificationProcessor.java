/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.server.notification;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.messaging.queue.wallet.CurrencyRequest;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.Subscription.Status;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionBillingService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.paymenthub.services.repository.TariffService;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import java.math.BigDecimal;
import java.sql.SQLIntegrityConstraintViolationException;
import org.apache.commons.lang3.Validate;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class BillingNotificationProcessor implements NotificationProcessor {

    private final ObjectMapper mapper;
    private final SopService sopService;
    private final TariffService tariffService;
    private final RabbitMQProducer rabbitMQProducer;
    private final SubscriptionManager subscriptionManager;
    private final PHProfilePropertiesService profilePropertiesService;
    private final SubscriptionBillingService subscriptionBillingService;
    private final SubscriptionRegistryService subscriptionRegistryService;
    private final SubscriptionNotificationProcessor subscriptionNotificationProcessor;
    private static final PaymentHubMessage.NotificationType notificationType = PaymentHubMessage.NotificationType.BILLING;

    @Inject
    public BillingNotificationProcessor(final ObjectMapper mapper,
            final SopService sopService,
            final TariffService tariffService,
            final RabbitMQProducer rabbitMQProducer,
            final SubscriptionManager subscriptionManager,
            final PHProfilePropertiesService profilePropertiesService,
            final SubscriptionBillingService subscriptionBillingService,
            final SubscriptionRegistryService subscriptionRegistryService,
            final SubscriptionNotificationProcessor subscriptionNotificationProcessor) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.sopService = Validate.notNull(sopService, "A SopService class must be provided");
        this.tariffService = Validate.notNull(tariffService, "A TariffService class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
        this.subscriptionBillingService = Validate.notNull(subscriptionBillingService, "A SubscriptionBillingService class must be provided");
        this.subscriptionRegistryService = Validate.notNull(subscriptionRegistryService, "A SubscriptionRegistryService class must be provided");
        this.subscriptionNotificationProcessor = Validate.notNull(subscriptionNotificationProcessor, "A SubscriptionNotificationProcessor class must be provided");
    }

    @Override
    public PaymentHubMessage.NotificationType getNotificationType() {
        return notificationType;
    }

    @Override
    public Boolean process(String smsg, PaymentHubMessage msg, SOP sop) {
        // log.info("Procesando notificacion de Billing.");

        try {
            Subscription s = subscriptionManager.getSubscription(msg.getExternalId(),
                    msg.getUserAccount(), msg.getExternalUserAccount(), msg.getExternalCustomer(), sop);

            if (s == null) {
                log.warn("No existe una Suscripcion para [" + msg.getUserAccount() + "]["
                        + msg.getExternalId() + "] - SOP: [" + sop + "]");

                msg.setMessageType(MessageType.SUBSCRIPTION_BY_BILLING);
                subscriptionNotificationProcessor.process(smsg, msg, sop);

                s = subscriptionManager.getSubscription(msg.getExternalId(),
                        msg.getUserAccount(), msg.getExternalUserAccount(), msg.getExternalCustomer(), sop);
            } else {
                if (!Status.ACTIVE.equals(s.getStatus())) {
                    log.warn("Notificacion de cobro para una suscripcion con status: [" + s.getStatus() + "] [" + s + "]");
                    try {
                        if (Status.PENDING.equals(s.getStatus()) || Status.TRIAL.equals(s.getStatus()) || Status.CLEANED_PENDING.equals(s.getStatus()) || Status.CLEANED_RETENTION.equals(s.getStatus())){
                            s.setStatus(Status.ACTIVE);
                            s.setUnsubscriptionDate(null);
                            s.getSubscriptionRegistry().setSyncDate(null);
                            s.getSubscriptionRegistry().setChannelOut(null);
                            s.getSubscriptionRegistry().setOriginUnsubscription(null);
                            s.getSubscriptionRegistry().setUnsubscriptionDate(null);
                            subscriptionManager.subscriptionProcess(s);
                        } else if ((Status.LOCKED.equals(s.getStatus()))
                                || (Status.CANCELLED.equals(s.getStatus()) && s.getUnsubscriptionDate() != null
                                && msg.getChargedDate().getTime() - s.getUnsubscriptionDate().getTime() > 144000000)) {
                            s.setStatus(Status.PENDING);
                            s.setUnsubscriptionDate(null);
                            s.getSubscriptionRegistry().setOriginSubscription(s.getSubscriptionRegistry().getOriginSubscription() + ";NOTIFICATION:BILLING:PENDING");
                            s.getSubscriptionRegistry().setUnsubscriptionDate(null);
                            s.getSubscriptionRegistry().setOriginUnsubscription(null);
                            s.getSubscriptionRegistry().setSyncDate(null);
                            subscriptionManager.subscriptionProcess(s);
                        }
                    } catch (Exception ex) {
                    }
                }
            }
            if (s != null && s.getSubscriptionRegistry() != null) {
                //  log.info("Subscription: [" + s.getId() + "]");
                SubscriptionBilling sb = new SubscriptionBilling();
                sb.setChargedDate(msg.getChargedDate() == null ? (msg.getToDate() == null ? msg.getFromDate() : msg.getToDate()) : msg.getChargedDate());
                sb.setNetAmount(msg.getNetAmount());
                sb.setSubscriptionRegistry(s.getSubscriptionRegistry());
                sb.setCurrency(msg.getCurrencyId());
                sb.setFullAmount(msg.getFullAmount());
                sb.setTransactionId(msg.getTransactionId());
                sb.setSop(sop);

                subscriptionBillingService.save(sb);
                // log.info("Notificacion de billing guardada: [" + sb + "]");
                try {
                    if (s.getSubscriptionRegistry().getExtra() == null) {
                        s.getSubscriptionRegistry().setExtra(new Extra());
                    }
                    s.getSubscriptionRegistry().getExtra().stringData.put("lastCharged", sb.getChargedDate().getTime() + "");
                    if (msg.getExtra() != null) {
                        Extra extra = mapper.convertValue(msg.getExtra(), Extra.class);
                        if (s.getSubscriptionRegistry().getExtra().serviceId == null) {
                            s.getSubscriptionRegistry().getExtra().serviceId = extra.serviceId;
                        }
                        if (s.getSubscriptionRegistry().getExtra().keyword == null) {
                            s.getSubscriptionRegistry().getExtra().keyword = extra.keyword;
                        }
                        if (s.getSubscriptionRegistry().getExtra().stringData == null) {
                            s.getSubscriptionRegistry().getExtra().stringData = extra.stringData;
                        } else {
                            s.getSubscriptionRegistry().getExtra().stringData.putAll(extra.stringData);
                        }
                    }

                    subscriptionRegistryService.saveOrUpdate(s.getSubscriptionRegistry());
                } catch (Exception ex) {
                    log.error("Error al actualizar SubscriptionRegistry con cobro. [" + s.getSubscriptionRegistry() + "]");
                }

                try {
                    String jsonResponse = mapper.writeValueAsString(sb);
                    if (jsonResponse != null) {
                        rabbitMQProducer.messageSender(profilePropertiesService.getCommonProperty("rabbitmq.paymenthub.adn.billings"), jsonResponse);
                    }
                } catch (Exception ex) {
                    log.error("Error al enviar mensaje de billing ADN al rabbit. " + ex, ex);
                }

                try {
                    CurrencyRequest cr = new CurrencyRequest();
                    cr.currency = sb.getCurrency();
                    cr.amount = sb.getFullAmount();
                    cr.countryId = sb.getSop().getProvider().getCountry().getId();
                    cr.date = sb.getChargedDate();
                    cr.sopId = sb.getSop().getId();
                    cr.transactionId = "PH_SB_" + sb.getId();
                    cr.userAccount = sb.getSubscriptionRegistry().getUserAccount();
                    cr.tracking = s.getSubscriptionRegistry().getAdnetworkTracking();
                    cr.tariffId = msg.getTariffId();

                    Tariff t = tariffService.findById(msg.getTariffId());
                    if (t != null) {
                        cr.localAmount = t.getLocalAmount();
                        cr.localCurrency = t.getLocalCurrency();
                        if (cr.currency == null) {
                            cr.currency = t.getCurrency();
                        }
                    }

                    String jsonRequest = mapper.writeValueAsString(cr);
                    if (jsonRequest != null) {
                        rabbitMQProducer.messageSender(profilePropertiesService.getCommonProperty("rabbitmq.pg.wallet.funds"), jsonRequest);
                    }
                } catch (Exception ex) {
                    log.error("Error al enviar mensaje de billing Wallet al rabbit. " + ex, ex);
                }

                try {
                    boolean control = false;
                    if (msg.getFrequencyType() != null && sop.getIntegrationSettings().get("frequencyType") == null) {
                        sop.getIntegrationSettings().put("frequencyType", msg.getFrequencyType().toLowerCase());
                        control = true;
                    }
                    if (msg.getCurrencyId() != null && sop.getIntegrationSettings().get("currency") == null) {
                        sop.getIntegrationSettings().put("currency", msg.getCurrencyId().toUpperCase());
                        control = true;
                    }
                    if (sop.getIntegrationSettings().get("fullAmount") == null) {
                        sop.getIntegrationSettings().put("fullAmount", msg.getFullAmount().toPlainString());
                        control = true;
                    } else {
                        BigDecimal aux = new BigDecimal(sop.getIntegrationSettings().get("fullAmount"));
                        if (msg.getFullAmount().compareTo(aux) == 1) {
                            sop.getIntegrationSettings().put("fullAmount", msg.getFullAmount().toPlainString());
                            control = true;
                        }
                    }

                    if (control) {
                        sopService.saveOrUpdate(sop);
                    }
                } catch (Exception ex) {
                    log.error("Error al actualizar SOP_INTEGRATION_SETTINGS. " + ex, ex);
                }
                return true;
            } else {
                log.fatal("Error al procesar notificacion de cobro. No existe una suscripcion valida para: [" + msg + "] - Subs: [" + s + "]");
                return null;
            }
        } catch (ConstraintViolationException | SQLIntegrityConstraintViolationException ex) {
            log.warn("Registro duplicado. [" + msg + "]. " + ex);
            return false;
        } catch (Exception ex) {
            boolean control = true;
            try {
                if (ex.getCause() instanceof ConstraintViolationException) {
                    control = false;
                    log.warn("Registro duplicado. [" + msg + "]. " + ex.getCause().getCause());
                }
            } catch (Exception exx) {
            }

            if (control) {
                log.error("Error al procesar notificacion de cobro: [" + msg + "]. " + ex, ex);
            } else {
                return false;
            }
        }
        return null;
    }

}
