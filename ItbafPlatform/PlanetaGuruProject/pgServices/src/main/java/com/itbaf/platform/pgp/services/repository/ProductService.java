/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository;

import com.itbaf.platform.pgp.model.Product;
import java.util.List;

/**
 *
 * @author javier
 */
public interface ProductService {

    public Product findbyId(Long id) throws Exception;

    public List<Product> getAll() throws Exception;
}
