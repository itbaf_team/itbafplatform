package com.itbaf.platform.paymenthub.extention.cdag.ws.subscription;



public class SessionExpiredException extends PersonalArProviderServiceException {

	private static final long serialVersionUID = -3999861270741544698L;

	public SessionExpiredException(StatusResponse response) {
		super(response);
	}
}
