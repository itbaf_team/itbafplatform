/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.billing;

import com.itbaf.platform.paymenthub.model.subscription.*;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.querydsl.core.annotations.Config;
/**
 *
 * @author javier
 */
@Entity(name = "SUBSCRIPTION_BILLING")
@Table(name = "SUBSCRIPTION_BILLING", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(false)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class SubscriptionBilling implements Serializable {

    private static final long serialVersionUID = 20170904124801L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @ManyToOne(optional = false)
    @JoinColumn(name = "SUBSCRIPTION_REGISTRY_ID")
    private SubscriptionRegistry subscriptionRegistry;
    @ManyToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;
    @Column(name = "TRANSACTION_ID")
    private String transactionId;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CHARGED_DATE")
    private Date chargedDate;
    @Column(name = "CURRENCY")
    private String currency;
    @Column(name = "FULL_AMOUNT")
    private BigDecimal fullAmount;
    @Column(name = "NET_AMOUNT")
    private BigDecimal netAmount;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("sop", sop)
                .add("subscriptionRegistry", subscriptionRegistry)
                .add("transactionId", transactionId)
                .add("chargedDate", chargedDate)
                .add("currency", currency)
                .add("fullAmount", fullAmount)
                .add("netAmount", netAmount)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, subscriptionRegistry, transactionId, chargedDate, currency, fullAmount, netAmount, sop);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<SubscriptionBilling>()
                .over(SubscriptionBilling::getId)
                .over(SubscriptionBilling::getSubscriptionRegistry)
                .over(SubscriptionBilling::getTransactionId)
                .over(SubscriptionBilling::getChargedDate)
                .over(SubscriptionBilling::getCurrency)
                .over(SubscriptionBilling::getFullAmount)
                .over(SubscriptionBilling::getNetAmount)
                .over(SubscriptionBilling::getSop)
                .asserts(this, obj);
    }
}
