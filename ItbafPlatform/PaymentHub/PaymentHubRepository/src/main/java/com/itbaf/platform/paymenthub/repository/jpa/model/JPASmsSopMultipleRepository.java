package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.QSmsSopMultiple;
import com.itbaf.platform.paymenthub.model.SmsSopMultiple;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.querydsl.jpa.impl.JPAQuery;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

@lombok.extern.log4j.Log4j2
public class JPASmsSopMultipleRepository  extends AbstractJPARepositorySession<SmsSopMultiple, Long> {

    @Inject
    public JPASmsSopMultipleRepository(final Provider<EntityManager> entityManagerProvider) {
        super(SmsSopMultiple.class, entityManagerProvider);
    }

    public boolean isMultipleMessage(Long sop) {
        Boolean isMultiple = Boolean.FALSE;
        SmsSopMultiple sm = null;

        log.info("Antes de ejecutar getSingleResult: SOP NUM " + sop);

        if (sop != null) {

            QSmsSopMultiple multipleSms = QSmsSopMultiple.smsSopMultiple;
            JPAQuery<SmsSopMultiple> query = new JPAQuery<>(getEntityManager());
            log.info("Antes de ejecutar query:" + query);

            sm = query.select(multipleSms).from(multipleSms)
                    .where(ObjectPredicates.combine(multipleSms.sop().id.eq(sop)))
                    .fetchFirst();

            isMultiple = sm.getMultipleSms();
        }

        log.info("Resultado consulta isMultipleMessage con querydsl : "+sm);
        return isMultiple;
    }
}