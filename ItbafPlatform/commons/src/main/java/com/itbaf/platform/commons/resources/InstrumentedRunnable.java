/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.resources;

import java.text.SimpleDateFormat;

/**
 *
 * @author javier
 */
public abstract class InstrumentedRunnable implements Runnable {

    private final String name;
    private final String providerName;
    private final InstrumentedObject instrumentedObject;

    public InstrumentedRunnable(final String name, final String providerName,
            final InstrumentedObject instrumentedObject) {
        this.name = name;
        if (providerName == null) {
            this.providerName = instrumentedObject.appName;
        } else {
            this.providerName = providerName;
        }
        this.instrumentedObject = instrumentedObject;
    }

    @Override
    public final void run() {
        Long time = System.currentTimeMillis();
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            boolean result = exec();
            //Descomentar si se quire revivir el proyecto de instrumentacion
        /*    if (result && instrumentedObject.influxDB != null) {
                Point point = Point
                        .measurement("paymenthub_runnable_process")
                        .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                        .tag("appName", instrumentedObject.appName)
                        .tag("instance", instrumentedObject.profile + "." + instrumentedObject.instance)
                        .tag("container", instrumentedObject.container)
                        .tag("date", sdf.format(new Date()))
                        .tag("name", name)
                        .tag("provider", providerName)
                        .addField("thread", Thread.currentThread().getName())
                        .addField("value", System.currentTimeMillis() - time)
                        .build();
                instrumentedObject.influxDB.write(GuiceInstrumentedConfigModule.influxDataBase, "autogen", point);
            }*/
        } catch (Exception ex) {
        }
    }

    public abstract boolean exec();

}
