package com.itbaf.platform.paymenthub.services.repository;

public interface SmsSopService {

    public String getSms(Long sop_id, String type);
}
