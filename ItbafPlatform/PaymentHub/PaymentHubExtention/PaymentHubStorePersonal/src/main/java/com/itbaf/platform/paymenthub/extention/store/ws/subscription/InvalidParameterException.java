package com.itbaf.platform.paymenthub.extention.store.ws.subscription;

public class InvalidParameterException extends PersonalArProviderServiceException {

    private static final long serialVersionUID = 105717548442149250L;

    public InvalidParameterException(StatusResponse response) {
        super(response);
    }
}
