/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.itbaf.platform.paymenthub.extention.smt.model.UsernameToken;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

/**
 *
 * @author JF
 */
public class TokenResourcesTest {

    public TokenResourcesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    @Ignore
    public void tokenInformation() {
        UsernameToken token = new UsernameToken("PA00002976", "Itb4f2");
        System.out.println("token.getUsername(): " + token.getUsername());
        System.out.println("token.getPasswordDigest(): " + token.getPasswordDigest());
        System.out.println("token.getNonce(): " + token.getNonce());
        System.out.println("token.getCreated(): " + token.getCreated());
        System.out.println("token.getRandTransId(): " + token.getRandTransId());

        System.out.println("------SEND-PIN------");
        String soap = "curl -v -k -X POST \\\n"
                + "http://smt.telcel.com/portalone/services/SendPIN \\\n"
                + "-H 'Cache-Control: no-cache' \\\n"
                + "-H 'Content-Type: text/xml' \\\n"
                + "-d '<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:loc=\"http://www.csapi.org/schema/parlayx/sendpin/v1_0/local\" xmlns:ser=\"http://www.csapi.org/wsdl/parlayx/sendpin/v1_0/service\">"
                + "<soapenv:Header>"
                + "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
                + "<wsse:Username>" + token.getUsername() + "</wsse:Username>"
                + "<wsse:Password Type=\"...#PasswordDigest\">" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce>" + token.getNonce() + "</wsse:Nonce>"
                + "<wsu:Created>" + token.getCreated() + "</wsu:Created>"
                + "</wsse:UsernameToken>"
                + "</wsse:Security>"
                + "<tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com.cn/schema/common/v2_1\">"
                + "<tns:AppId>pgjapp</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId>"
                + "<tns:OA>tel:525539292878</tns:OA>"
                + "<tns:FA>tel:525539292878</tns:FA>"
                + "</tns:RequestSOAPHeader>"
                + "</soapenv:Header>"
                + "<soapenv:Body>"
                + "<loc:sendPIN>"
                + "<loc:msisdn>525539292878</loc:msisdn>"
                + "<loc:productID>MDSP2000028721</loc:productID>"
                + "</loc:sendPIN>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>'";
        System.out.println(soap);
        System.out.println("-------------");

        System.out.println("------VALIDATE-PIN------");
        soap = "curl -v -k -X POST \\\n"
                + "https://hub.americamovil.com:443/asg/services/SendPINService \\\n"
                + "-H 'Cache-Control: no-cache' \\\n"
                + "-H 'Content-Type: text/xml' \\\n"
                + "-d '<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:loc=\"http://www.csapi.org/schema/parlayx/sendpin/v1_0/local\" xmlns:ser=\"http://www.csapi.org/wsdl/parlayx/sendpin/v1_0/service\">"
                + "<soapenv:Header>"
                + "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken wsu:Id=\"UsernameToken-9\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
                + "<wsse:Username>" + token.getUsername() + "</wsse:Username>"
                + "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + token.getNonce() + "</wsse:Nonce>"
                + "<wsu:Created>" + token.getCreated() + "</wsu:Created>"
                + "</wsse:UsernameToken>"
                + "</wsse:Security>"
                + "<tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\">"
                + "<AppId>pgjapp</AppId>"
                + "<TransId>" + token.getRandTransId() + "</TransId>"
                + "<tns:token />"
                + "<tns:OA>tel:525539292878</tns:OA>"
                + "<tns:FA>tel:525539292878</tns:FA>"
                + "</tns:RequestSOAPHeader>"
                + "</soapenv:Header>"
                + "<soapenv:Body>"
                + "<loc:confirmPIN>"
                + "<loc:pin>1234</loc:pin>"
                + "<loc:msisdn>525539292878</loc:msisdn>"
                + "<loc:productID>MDSP2000028721</loc:productID>"
                + "</loc:confirmPIN>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>'";
        System.out.println(soap);
        System.out.println("-------------");

        System.out.println("------BAJA------");
        soap = "curl -v -k -X POST \\\n"
                + "https://hub.americamovil.com:443/asg/services/SubscribeProductService \\\n"
                + "-H 'Cache-Control: no-cache' \\\n"
                + "-H 'Content-Type: text/xml' \\\n"
                + "-d '<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken><wsse:Username>" + token.getUsername() + "</wsse:Username>"
                + "<wsse:Password>" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce>" + token.getNonce() + "</wsse:Nonce>"
                + "<wsse:Created>" + token.getCreated() + "</wsse:Created>"
                + "</wsse:UsernameToken></wsse:Security><tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\"><tns:AppId>app_tt</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId>"
                + "<tns:token/></tns:RequestSOAPHeader></SOAP-ENV:Header><soap:Body>"
                + "<ns2:unSubscribeProduct xmlns:ns2=\"http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\"><ns2:version>5.0</ns2:version>"
                + "<ns2:userID><ID>525591955584</ID><type>0</type></ns2:userID><ns2:subInfo>"
                + "<productID>MDSP2000028721</productID></ns2:subInfo><ns2:channelID>1</ns2:channelID></ns2:unSubscribeProduct></soap:Body></soap:Envelope>'";
        System.out.println(soap);
        System.out.println("-------------");
        System.out.println("------SMS------");
        soap = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken><wsse:Username>" + token.getUsername() + "</wsse:Username>"
                + "<wsse:Password>" + token.getPasswordDigest() + "</wsse:Password><wsse:Nonce>" + token.getNonce() + "</wsse:Nonce>"
                + "<wsse:Created>" + token.getCreated() + "</wsse:Created>"
                + "</wsse:UsernameToken></wsse:Security><tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\"><tns:AppId>app_tt</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId><tns:token/></tns:RequestSOAPHeader></SOAP-ENV:Header>"
                + "<soap:Body><ns2:sendSms xmlns:ns2=\"http://www.csapi.org/schema/parlayx/sms/send/v2_2/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\">"
                + "<ns2:addresses>525535066527</ns2:addresses><ns2:senderName>5299910</ns2:senderName>"
                + "<ns2:message>Test</ns2:message><ns2:esm_class>11</ns2:esm_class>"
                + "<ns2:data_coding>11</ns2:data_coding></ns2:sendSms></soap:Body></soap:Envelope>";
        System.out.println(soap);
        System.out.println("-------------");
    }
}
