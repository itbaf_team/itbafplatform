/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository;

import com.itbaf.platform.pgp.model.GenericProperty;
import com.itbaf.platform.pgp.model.GenericProperty.Profile;
import com.itbaf.platform.services.service.PropertyManagerService;

/**
 *
 * @author javier
 */
public interface GenericPropertyService extends PropertyManagerService {

    public void saveOrUpdate(GenericProperty gp) throws Exception;

    public GenericProperty findById(Long id);

    public String getValueByKey(String key);

    public String getValueByProfileKey(String profile, String key);

    public String getValueByProfileKey(Profile profile, String key);
}
