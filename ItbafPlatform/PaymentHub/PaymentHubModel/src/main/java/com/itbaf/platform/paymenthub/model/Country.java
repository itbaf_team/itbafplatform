/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
@Entity(name = "COUNTRY")
@Table(name = "COUNTRY", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class Country implements Serializable {

    private static final long serialVersionUID = 20170809155401L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "NATIVE_NAME")
    private String nativeName;

    @Column(name = "CODE", nullable = false, unique = true)
    private String code = "XX";

    @Column(name = "CURRENCY", nullable = false, unique = true)
    private String currency;

    @Column(name = "SYMBOL_CURRENCY")
    private String symbolCurrency;

    @Column(name = "PHONE_CODE", nullable = false)
    private Integer phoneCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private Status status;
    
    @Column(name = "NATIVE_VIEW")
    private Boolean nativeView;

    @Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(catalog = "paymentHub", name = "LANGUAGE_COUNTRY", joinColumns = {
        @JoinColumn(name = "COUNTRY_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "LANGUAGE_CODE_2", nullable = false, updatable = false)})
    private Set<Language> languages;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("status", status)
                .add("name", name)
                .add("nativeName", nativeName)
                .add("code", code)
                .add("currency", currency)
                .add("symbolCurrency", symbolCurrency)
                .add("phoneCode", phoneCode)
                .add("languages", languages)
                .add("nativeView", nativeView)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, code, currency, phoneCode, status, languages,nativeView);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<Country>()
                .over(Country::getId)
                .over(Country::getName)
                .over(Country::getCode)
                .over(Country::getCurrency)
                .over(Country::getPhoneCode)
                .over(Country::getStatus)
                .over(Country::getLanguages)
                .over(Country::getNativeView)
                .asserts(this, obj);
    }
    
    public static Country basicClone(Country country) {
        if (country != null && country.getId() != null && country.getSymbolCurrency() != null) {
            Country aux = new Country();
            aux.setCode(country.getCode());
            aux.setId(country.getId());
            aux.setName(country.getName());
            country = aux;
        }
       return country;
    }
}
