/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.co.movistar.modules;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.modules.GuiceDefaultConfigModule;
import com.itbaf.platform.paymenthub.commons.bn.BNProcessor;
import com.itbaf.platform.paymenthub.commons.sync.RabbitMQSyncMessageProcessorImpl;
import com.itbaf.platform.paymenthub.commons.sync.ServiceProcessor;
import com.itbaf.platform.paymenthub.extention.co.movistar.consumer.RabbitMQBillerMessageProcessorImpl;
import com.itbaf.platform.paymenthub.extention.co.movistar.handler.BNProcessorHandler;
import com.itbaf.platform.paymenthub.extention.co.movistar.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.repository.module.GuiceRepositoryConfigModule;
import com.itbaf.platform.paymenthub.services.module.GuiceServiceConfigModule;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import com.itbaf.platform.rmq.module.GuiceRabbitMQConfigModule;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Map;
import com.itbaf.platform.paymenthub.extention.co.movistar.consumer.RabbitMQBillerMessageProcessor;

/**
 *
 * @author jordonez
 */
public class GuiceConfigModule extends GuiceDefaultConfigModule {

    public GuiceConfigModule(final Map<String, String> args, CommonDaemon commonDaemon) {
        super(args,commonDaemon);
    }

    @Override
    protected void configure() {
        super.configure();

        //Binds
        bind(BNProcessor.class).to(BNProcessorHandler.class);
        bind(ServiceProcessor.class).to(ServiceProcessorHandler.class);
        bind(RabbitMQMessageProcessor.class).to(RabbitMQSyncMessageProcessorImpl.class);
        bind(RabbitMQBillerMessageProcessor.class).to(RabbitMQBillerMessageProcessorImpl.class);

        //Modulos de aplicaciones
        install(new GuiceRepositoryConfigModule());
        install(new GuiceServiceConfigModule());
        install(new GuiceRabbitMQConfigModule());
        install(new GuiceRestServicesConfigModule());

        log.info("Configuracion iniciada. " + Arrays.toString(args.entrySet().toArray()));
    }

    @Provides
    @Singleton
    XmlMapper xmlMapper() {
        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        XmlMapper xmlMapper = new XmlMapper(module);
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        xmlMapper.setDateFormat(new SimpleDateFormat("yyyyMMddHHmmss"));
        return xmlMapper;
    }
}
