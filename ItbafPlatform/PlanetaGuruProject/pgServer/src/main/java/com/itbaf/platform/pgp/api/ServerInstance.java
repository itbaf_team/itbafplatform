/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api;

import com.google.inject.Injector;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.rest.ManagerRestService;
import com.itbaf.platform.commons.servlets.ServletServer;
import com.itbaf.platform.pgp.api.modules.GuiceConfigModule;

/**
 * 
 * @author javier
 */
public class ServerInstance extends CommonDaemon {

    private ServerMain daemonMain;
    private ServletServer servletServer;
    private static ServerInstance serverDaemon;

    public static void main(String args[]) throws Exception {
        ServerInstance.getInstance().init(args);
        ServerInstance.getInstance().start();
    } 

    public synchronized static ServerInstance getInstance() {
        if (serverDaemon == null) {
            serverDaemon = new ServerInstance();
            ManagerRestService.paymentHubDaemon = serverDaemon;
        }
        return serverDaemon;
    }

    @Override
    public void start() throws Exception {
        servletServer = new ServletServer(new GuiceConfigModule(args, this));
        final Injector injector = servletServer.getInject();
        daemonMain = injector.getInstance(ServerMain.class);
        daemonMain.start();
        servletServer.startGzipServer("/account/*");
    }

    @Override
    public void stop() throws Exception {
        try {
            servletServer.stopServer();
        } catch (Exception ex) {
        }
        try {
            Thread.sleep(100);
        } catch (Exception ex) {
        }
        try {
            daemonMain.stop();
        } catch (Exception ex) {
        }
    }

}
