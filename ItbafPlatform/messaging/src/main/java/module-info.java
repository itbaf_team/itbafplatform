/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module com.itbaf.platform.messaging {
    exports com.itbaf.platform.messaging;
    exports com.itbaf.platform.messaging.queue.wallet;
    exports com.itbaf.platform.messaging.rest;
    exports com.itbaf.platform.messaging.rest.oauth;
    exports com.itbaf.platform.messaging.rest.pg;
    requires com.google.common;
    requires checker.qual;
    requires j2objc.annotations;
    requires animal.sniffer.annotations;
    requires lombok;
    requires gson;
} 
