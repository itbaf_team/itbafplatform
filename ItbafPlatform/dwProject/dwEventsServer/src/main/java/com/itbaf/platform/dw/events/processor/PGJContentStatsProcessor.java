/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events.processor;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.commons.stats.model.PGJContentElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.StatsMessage;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.services.repository.UserService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PGJContentStatsProcessor extends StatsProcessor {

    private static final ElasticData.ElasticType elasticType = ElasticData.ElasticType.pgj_content;

    private final UserService userService;
    private final BasicDataBaseConnection dataBaseConnection;
    private final static String ORDER_QUERY = "SELECT RS.*, CT.id CONTENT_TYPE_ID, CT.slug CONTENT_TYPE_NAME "
            + "FROM content C, content_type CT,( "
            + "	SELECT O.id ORDER_ID, O.date ORDER_DATE ,C.id CONTENT_ID, C.slug CONTENT_NAME, T.slug DEVELOPER_NAME, O.user_id, O.client_id "
            + "	FROM `order` O, content C, content_tag CT, tag T, tag_type TT, tag_tag_type TTT "
            + "	WHERE O.content_id = C.id AND C.id=CT.content_id AND T.id=CT.tag_id AND T.id=TTT.tag_id AND TT.id=TTT.tag_type_id "
            + "	AND TT.id=3 "
            + "	AND O.id=?) RS "
            + "WHERE C.id=RS.CONTENT_ID AND C.content_type_id=CT.id ";

    private final static String DOWNLOAD_QUERY = "SELECT RS.*, CT.id CONTENT_TYPE_ID, CT.slug CONTENT_TYPE_NAME "
            + "FROM content C, content_type CT,( "
            + "	SELECT dh.id DOWNLOAD_ID, dh.date DOWNLOAD_DATE ,C.id CONTENT_ID, C.slug CONTENT_NAME, T.slug DEVELOPER_NAME, dh.user_id, dh.client_id "
            + "	FROM download_history dh, content C, content_tag CT, tag T, tag_type TT, tag_tag_type TTT "
            + "	WHERE dh.content_id = C.id AND C.id=CT.content_id AND T.id=CT.tag_id AND T.id=TTT.tag_id AND TT.id=TTT.tag_type_id "
            + "	AND TT.id=3  "
            + "	AND dh.id=?) RS "
            + "WHERE C.id=RS.CONTENT_ID AND C.content_type_id=CT.id ";

    private final static String ORDER_LEGACY_QUERY = "SELECT DH.id DOWNLOAD_LEGACY_ID, DH.game_id GAME_ID,G.slug GAME_NAME, "
            + "DH.user_id USER_LEGACY_ID,D.id DEVELOPER_LEGACY_ID, D.slug DEVELOPER_LEGACY_NAME, DH.datetime FECHA  "
            + "FROM downloads_history DH, games G, developers D "
            + "WHERE DH.game_id=G.id AND G.developer_id=D.id "
            + "AND DH.paid = 1 "
            + "AND DH.id=? ";

    @Inject
    public PGJContentStatsProcessor(final UserService userService,
            final BasicDataBaseConnection dataBaseConnection) {
        this.userService = userService;
        this.dataBaseConnection = dataBaseConnection;
    }

    @Override
    public Boolean process(String msg) throws Exception {
        // log.info("Procesando DW SubscriptionRegistry: [" + msg + "]");
        StatsMessage smx = mapper.readValue(msg, StatsMessage.class);

        PGJContentElastic oex = (PGJContentElastic) smx.elasticData;
        String mm = smx.elasticData.elasticType + "_" + smx.elasticData.id + "_" + oex.functionType;
        RLock lock = instrumentedObject.lockObject(mm, 60L);
        try {
            String dm = clientCache.getTemporalData(mm);
            if (dm != null) {
                if (smx.lastUpdateMillis < Long.parseLong(dm)) {
                    // log.info("Ignorando DW SubscriptionRegistry: [" + msg + "]");
                    return true;
                }
            }

            StatsMessage sm = new StatsMessage();
            PGJContentElastic oe = new PGJContentElastic();
            sm.thread = smx.thread;
            sm.elasticData = oe;

            Connection conn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            boolean isv2 = true;
            switch (oex.functionType) {
                case ORDER:
                    try {
                        conn = dataBaseConnection.getDBJuegosV2Connection();
                        ps = conn.prepareStatement(ORDER_QUERY);
                        ps.setLong(1, Long.parseLong(oex.id));
                        rs = ps.executeQuery();
                        while (rs.next()) {
                            oe.orderId = rs.getLong("ORDER_ID");
                            oe.fecha = new Date(rs.getTimestamp("ORDER_DATE").getTime());
                            oe.contentId = rs.getLong("CONTENT_ID");
                            oe.contentName = rs.getString("CONTENT_NAME");
                            oe.developers.add(rs.getString("DEVELOPER_NAME"));
                            oe.userId = rs.getLong("user_id");
                            oe.clientId = rs.getString("client_id");
                            oe.contentTypeId = rs.getLong("CONTENT_TYPE_ID");
                            oe.contentTypeName = rs.getString("CONTENT_TYPE_NAME");
                        }
                    } catch (Exception ex) {
                        log.error("Error al procesar SQL: [" + ORDER_QUERY + "]. " + ex, ex);
                    }
                    break;
                case DOWNLOAD:
                    try {
                        conn = dataBaseConnection.getDBJuegosV2Connection();
                        ps = conn.prepareStatement(DOWNLOAD_QUERY);
                        ps.setLong(1, Long.parseLong(oex.id));
                        rs = ps.executeQuery();
                        while (rs.next()) {
                            oe.downloadId = rs.getLong("DOWNLOAD_ID");
                            oe.fecha = new Date(rs.getTimestamp("DOWNLOAD_DATE").getTime());
                            oe.contentId = rs.getLong("CONTENT_ID");
                            oe.contentName = rs.getString("CONTENT_NAME");
                            oe.developers.add(rs.getString("DEVELOPER_NAME"));
                            oe.userId = rs.getLong("user_id");
                            oe.clientId = rs.getString("client_id");
                            oe.contentTypeId = rs.getLong("CONTENT_TYPE_ID");
                            oe.contentTypeName = rs.getString("CONTENT_TYPE_NAME");
                        }
                    } catch (Exception ex) {
                        log.error("Error al procesar SQL: [" + DOWNLOAD_QUERY + "]. " + ex, ex);
                    }
                    break;
                case ORDER_LEGACY:
                    try {
                        conn = dataBaseConnection.getDBJuegosConnection();
                        isv2 = false;
                        ps = conn.prepareStatement(ORDER_LEGACY_QUERY);
                        ps.setLong(1, Long.parseLong(oex.id));
                        rs = ps.executeQuery();
                        while (rs.next()) {
                            oe.orderLegacyId = rs.getLong("DOWNLOAD_LEGACY_ID");
                            oe.fecha = new Date(rs.getTimestamp("FECHA").getTime());
                            oe.gameLegacyId = rs.getLong("GAME_ID");
                            oe.gameLegacyName = rs.getString("GAME_NAME");
                            oe.userLegacyId = rs.getLong("USER_LEGACY_ID");
                            oe.developerLegacyId = rs.getLong("DEVELOPER_LEGACY_ID");
                            oe.developerLegacyName = rs.getString("DEVELOPER_LEGACY_NAME");
                        }
                    } catch (Exception ex) {
                        log.error("Error al procesar SQL: [" + ORDER_LEGACY_QUERY + "]. " + ex, ex);
                    }
                    break;

            }
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                ps.close();
            } catch (Exception ex) {
            }

            if (oe.fecha == null) {
                log.warn("MSG con datos incompletos en DB. [" + msg + "]");
                return true;
            }

            if (isv2) {
                dataBaseConnection.closeJuegosV2Connection(conn);
                if (oe.userId == null) {
                    log.warn("UserId null. [" + msg + "]");
                    return null;
                } else {
                    User user = userService.findById(oe.userId);
                    if (user == null) {
                        log.warn("User null. [" + msg + "]");
                        return null;
                    } else if (user.getExternalDataUserProperty() != null
                            && user.getExternalDataUserProperty().getObjects() != null
                            && user.getExternalDataUserProperty().getObjects().get("uTest") != null) {
                        //Es un usuario de pruebas...
                        return true;
                    }
                }
                if (!"unknown".equals(oe.clientId) && oe.clientId != null) {
                    try {
                        User client = userService.findByclientId(oe.clientId);
                        if (client != null) {
                            oe.clientId = client.getId().toString();
                            oe.clientName = client.getProfileUserProperty().getFirstName() + " " + client.getProfileUserProperty().getLastName();
                        }
                    } catch (Exception ex) {
                    }
                } else {
                    oe.clientName = oe.clientId;
                }
            } else {
                dataBaseConnection.closeJuegosConnection(conn);
            }
            oe.functionType = oex.functionType;
            oe.id = oex.id;

            //Enviamos el PGJ_CONTENT actualizado al Rabbit
            String json = mapper.writeValueAsString(sm);
            if (Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.pgj_content, json))) {
                if (oe.userId != null) {
                    //Enviamos al Rabbit para actualizar Oauth_User_Subscription
                    return statsService.sendToRabbitByID(ElasticData.ElasticType.oauth_user_subscription, oe.userId.toString());
                } else {
                    return true;
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar pgj_content: [" + msg + "]. " + ex, ex);
        } finally {
            instrumentedObject.unLockObject(lock);
        }

        return null;
    }

    @Override
    public ElasticData.ElasticType getElasticType() {
        return elasticType;
    }

}
