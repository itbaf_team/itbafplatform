/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.repository.module;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.itbaf.platform.persistence.connection.BasicDBConnectionConfig;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.persistence.connection.DBConnectionConfig;
import com.itbaf.platform.persistence.connection.DataBaseConnection;
import com.itbaf.platform.pgp.repository.jpa.JPAInitializer;

/**
 *
 * @author jordonez
 */
public class GuiceRepositoryConfigModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new JpaPersistModule(BasicDataBaseConnection.PGB_PERSISTENT_UNIT_NAME_DOMAIN));
        bind(DataBaseConnection.class).to(BasicDataBaseConnection.class);
        bind(JPAInitializer.class).asEagerSingleton();
        requestInjection(this);
    }

    @Inject
    void initJPAInitializer(JPAInitializer jpaInitializer) {

        jpaInitializer.startService();
    }

    @Provides
    @Singleton
    BasicDBConnectionConfig basicDBConnectionConfig(@Named("app.name") String appName,
            @Named("guice.profile") String profile,
            @Named("guice.instance") String instance,
            @Named("pgschema.datasource.url") String pgdbUrl,
            @Named("pgschema.datasource.username") String pgdbUsername,
            @Named("pgschema.datasource.password") String pgdbPassword,
            @Named("pgschema.datasource.driver") String pgdbDriver,
            @Named("dwschema.datasource.url") String dwdbUrl,
            @Named("dwschema.datasource.username") String dwdbUsername,
            @Named("dwschema.datasource.password") String dwdbPassword,
            @Named("dwschema.datasource.driver") String dwdbDriver,
            @Named("dwschema.datasource.stats") String isdwStats,
            @Named("juegos.datasource.url") String juegosdbUrl,
            @Named("juegos.datasource.username") String juegosdbUsername,
            @Named("juegos.datasource.password") String juegosdbPassword,
            @Named("juegos.datasource.driver") String juegosdbDriver,
            @Named("juegos.datasource.stats") String isjuegosStats,
            @Named("juegosv2.datasource.url") String juegosv2dbUrl,
            @Named("juegosv2.datasource.username") String juegosv2dbUsername,
            @Named("juegosv2.datasource.password") String juegosv2dbPassword,
            @Named("juegosv2.datasource.driver") String juegosv2dbDriver,
            @Named("juegosv2.datasource.stats") String isjuegosv2Stats) {

        BasicDBConnectionConfig config = new BasicDBConnectionConfig(appName + "." + profile + "." + instance);
        config.getConfig().put(BasicDataBaseConnection.PGB_PERSISTENT_UNIT_NAME_DOMAIN, new DBConnectionConfig(pgdbUrl, pgdbUsername, pgdbPassword, pgdbDriver));

        if ("true".equals(isdwStats)) {
            config.getConfig().put(BasicDataBaseConnection.DW_PERSISTENT_UNIT_NAME_DOMAIN, new DBConnectionConfig(dwdbUrl, dwdbUsername, dwdbPassword, dwdbDriver));
        }

        if ("true".equals(isjuegosStats)) {
            config.getConfig().put(BasicDataBaseConnection.JUEGOS_PERSISTENT_UNIT_NAME_DOMAIN, new DBConnectionConfig(juegosdbUrl, juegosdbUsername, juegosdbPassword, juegosdbDriver));
        }

        if ("true".equals(isjuegosv2Stats)) {
            config.getConfig().put(BasicDataBaseConnection.JUEGOS_V2_PERSISTENT_UNIT_NAME_DOMAIN, new DBConnectionConfig(juegosv2dbUrl, juegosv2dbUsername, juegosv2dbPassword, juegosv2dbDriver));
        }

        return config;
    }
}
