/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth;

import com.google.inject.Injector;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.rest.ManagerRestService;
import com.itbaf.platform.commons.servlets.ServletServer;
import com.itbaf.platform.pgp.oauth.modules.GuiceConfigModule;

/**
 *
 * @author javier
 */
public class OauthInstance extends CommonDaemon {

    private OauthMain daemonMain;
    private ServletServer servletServer;
    private static OauthInstance serverDaemon;

    public static void main(String args[]) throws Exception {
        OauthInstance.getInstance().init(args);
        OauthInstance.getInstance().start();
    }

    public synchronized static OauthInstance getInstance() {
        if (serverDaemon == null) {
            serverDaemon = new OauthInstance();
            ManagerRestService.paymentHubDaemon = serverDaemon;
        }
        return serverDaemon;
    }

    @Override
    public void start() throws Exception {
        servletServer = new ServletServer(new GuiceConfigModule(args, this));
        final Injector injector = servletServer.getInject();
        daemonMain = injector.getInstance(OauthMain.class);
        daemonMain.start();//Debe ejecutarse antes del servletServer
        servletServer.startGzipServer("/oauth/signin/*","/oauth/signup/*");
    }

    @Override
    public void stop() throws Exception {
        try {
            servletServer.stopServer();
        } catch (Exception ex) {
        }
        try {
            Thread.sleep(100);
        } catch (Exception ex) {
        }
        try {
            daemonMain.stop();
        } catch (Exception ex) {
        }
    }

}
