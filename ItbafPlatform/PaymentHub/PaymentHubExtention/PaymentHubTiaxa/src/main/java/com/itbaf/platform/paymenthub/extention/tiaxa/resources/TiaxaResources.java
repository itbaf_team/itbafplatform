package com.itbaf.platform.paymenthub.extention.tiaxa.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import org.apache.commons.lang3.Validate;
import org.codehaus.jettison.json.JSONObject;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Holder;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@lombok.extern.log4j.Log4j2
public class TiaxaResources {

    private final ObjectMapper mapper;
    private final RequestClient requestClient;
    private final PHProfilePropertiesService profilePropertiesService;

    private static final String EXTERNAL_SERVICE_URL_TOKEN = "https://portal.shop/index.php/rest/all/V1/integration/admin/token";
    private static final String ENDPOINT = "https://services.portal.shop/";
    private static final String PATH_UNSUBSCRIBE = "unSubscribeProduct";
    private static final String PATH_SUBSCRIBE = "subscribeProduct";
    private static final String EXTERNAL_SERVICE_USERNAME = "external.service.username";
    private static final String EXTERNAL_SERVICE_PASSWORD = "external.service.password";

    private final Cache<String, String> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(45, TimeUnit.MINUTES)
            .build();

    @Inject
    public TiaxaResources(final ObjectMapper mapper,
            final RequestClient requestClient,
            final PHProfilePropertiesService profilePropertiesService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    private String getToken(SOP sop) throws UnsupportedEncodingException {
        String x = sop.getIntegrationSettings().get("user") + ":" + sop.getIntegrationSettings().get("password");
        return new String(java.util.Base64.getEncoder().encode(x.getBytes("UTF-8")));
    }

    public TiaxaResponse unSubscribeUser(String userId, SOP sop) throws Exception {

        String token = getToken(sop);

        if (token != null) {
            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Basic " + token);

            TiaxaRequest tr = new TiaxaRequest();
            tr.appId = sop.getIntegrationSettings().get("appId");
            tr.channelId = Integer.parseInt(sop.getIntegrationSettings().get("channelUnsubscribeId"));
            tr.partnerID = sop.getIntegrationSettings().get("partnerID");
            tr.subInfo.productID = sop.getIntegrationSettings().get("productID");
            tr.transID = "SMT" + new Date().getTime() + "";
            tr.userID.ID = userId;

            String endpoint = sop.getIntegrationSettings().get("endpoint");
            String pathUnsubscribe = sop.getIntegrationSettings().get("pathUnsubscribe");

            if (endpoint == null || endpoint.isEmpty()) {
                endpoint = ENDPOINT;
            }

            if (pathUnsubscribe == null || pathUnsubscribe.isEmpty()) {
                pathUnsubscribe = PATH_UNSUBSCRIBE;
            }

            String json = mapper.writeValueAsString(tr);
            String response = requestClient.requestJSONPost(endpoint + pathUnsubscribe, json, headers);

            if (response != null) {
                return mapper.readValue(response, TiaxaResponse.class);
            }
        }

        return null;
    }

    public TiaxaResponse subscribeUser(String userId, SOP sop) throws Exception {

        String token = getToken(sop);

        if (token != null) {
            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Basic " + token);

            TiaxaRequest tr = new TiaxaRequest();
            tr.appId = sop.getIntegrationSettings().get("appId");
            tr.channelId = Integer.parseInt(sop.getIntegrationSettings().get("channelSubscribeId"));
            tr.partnerID = sop.getIntegrationSettings().get("partnerID");
            tr.subInfo.productID = sop.getIntegrationSettings().get("productID");
            tr.transID = "SMT" + new Date().getTime() + "";
            tr.userID.ID = userId;

            String endpoint = sop.getIntegrationSettings().get("endpoint");
            String pathSubscribe = sop.getIntegrationSettings().get("pathSubscribe");

            if (endpoint == null || endpoint.isEmpty()) {
                endpoint = ENDPOINT;
            }

            if (pathSubscribe == null || pathSubscribe.isEmpty()) {
                pathSubscribe = PATH_SUBSCRIBE;
            }

            String json = mapper.writeValueAsString(tr);
            String response = requestClient.requestJSONPost(endpoint + pathSubscribe, json, headers);

            if (response != null) {
                return mapper.readValue(response, TiaxaResponse.class);
            }
        }

        return null;
    }
    
    public JSONObject sendPin(String msisdn, SOP sop, String transactionID) throws Exception {

        String url = profilePropertiesService.getCommonProperty("tiaxa.service.url.pin")+"sendPin";
        String username = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), EXTERNAL_SERVICE_USERNAME);
        String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), EXTERNAL_SERVICE_PASSWORD);
        String json = buildJSONStringForSendPin(msisdn, sop, transactionID);
        
        String result = requestClient.requestJSONPost(url, json, username, password);
        log.info("TIAXA SMT SENDPIN Response. MSISDN: [" + msisdn + "] - JSON ["+ json +"] - RESULT: [" + result + "]");

        JSONObject respuestaJson = new JSONObject(result);
        log.info("TIAXA SMT SENDPIN Response. MSISDN: [" + msisdn + "] - " +
                "JSON ["+ json +"] - RESULT: [" + result + "] - jsonresponse: ["+respuestaJson.toString()+"]" +
                "resultCode: ["+respuestaJson.getString("resultCode") +"] - " +
                "resultDesc: ["+ respuestaJson.getString("resultDesc") +"]");

        return respuestaJson;
    }

    public JSONObject validatePin(String msisdn, SOP sop, String pin, String transactionID) throws Exception {
        String url = profilePropertiesService.getCommonProperty("tiaxa.service.url.pin")+"confirmPin";
        String username = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), EXTERNAL_SERVICE_USERNAME);
        String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), EXTERNAL_SERVICE_PASSWORD);
        String json = buildJSONStringForValidatePin(msisdn, sop, pin, transactionID);
        
        String result = requestClient.requestJSONPost(url, json, username, password);
        log.info("TIAXA SMT CHECKPIN Response. MSISDN: [" + msisdn + "] - JSON ["+ json +"] - RESULT: [" + result + "]");
        JSONObject respuestaJson = new JSONObject(result);

        return respuestaJson;
    }
    
    private String buildJSONStringForSendPin(String msisdn, SOP sop, String transactionID) throws Exception {

        String json = "{\"appId\": \" "+sop.getIntegrationSettings().get("appId")+"\", "
                + "\"transID\":\"" + transactionID +"\", "
                + "\"msisdn\":\""+msisdn+"\", "
                + "\"productID\":\""+sop.getIntegrationSettings().get("productID")+"\"}";
        return json;
    }   
    
    private String buildJSONStringForValidatePin(String msisdn, SOP sop, String pin, String transactionID) throws Exception {

        String json = "{\"appId\": \" "+sop.getIntegrationSettings().get("appId")+"\", "
                + "\"transID\":\"" + transactionID +"\", "
                + "\"pin\":\""+pin+"\", "
                + "\"msisdn\":\""+msisdn+"\", "
                + "\"productID\":\""+sop.getIntegrationSettings().get("productID")+"\"}";
        return json;
    }   

}
