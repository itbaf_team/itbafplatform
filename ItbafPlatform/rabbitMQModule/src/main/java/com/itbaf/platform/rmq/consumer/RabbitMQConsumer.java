/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.rmq.consumer;

/**
 *
 * @author javier
 */
public interface RabbitMQConsumer {

    public void createChannel(String exchangeName, Integer consumersQuantity, RabbitMQMessageProcessor messageProcessor);

    public void createFanoutChannel(String exchangeName, RabbitMQMessageProcessor messageProcessor);

    public void stopConsumers();
}
