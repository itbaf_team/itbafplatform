/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.sc.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.sc.model.SCResponse;
import com.itbaf.platform.paymenthub.extention.sc.model.Notification;
import com.itbaf.platform.paymenthub.extention.sc.model.SCSubscription;
import com.itbaf.platform.paymenthub.extention.sc.resources.SCResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final List<Provider> providers;
    private final Random rand = new Random();
    private final SCResources scResources;

    @Inject
    public ServiceProcessorHandler(final Set<Provider> providers,
            final SCResources scResources) {
        this.providers = new ArrayList();
        this.scResources = Validate.notNull(scResources, "A SCResources class must be provided");
        this.providers.addAll(Validate.notNull(providers, "A Set<Provider> class must be provided"));
    }

    public void processNotification(final Notification notification) {

        Provider p;
        final SOP sop = sopService.findBySettings("sc_productId", notification.getProduct_id());
        if (sop == null) {
            log.error("No existe un SOP configurado para el product_id: [" + notification.getProduct_id() + "] - Notificacion: [" + notification + "]");
            p = providers.get(rand.nextInt(providers.size()));
        } else {
            p = sop.getProvider();
        }

        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notification);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notification);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
            }
        }

    }

    @Override
    public Boolean processMessage(String message) {

        Notification notification;
        try {
            notification = mapper.readValue(message, Notification.class);
            Thread.currentThread().setName(notification.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }

        final SOP sop = sopService.findBySettings("sc_productId", notification.getProduct_id());

        if (sop == null) {
            log.error("No existe un SOP configurado para el product_id: [" + notification.getProduct_id() + "] - Notificacion: [" + notification + "]");
            return null;
        }
        final String threadName = Thread.currentThread().getName() + "-" + sop.getProvider().getName() + ".n";
        Thread.currentThread().setName(threadName);

        String subscriptionId = notification.getSubscription_id();
        Boolean msg = null;
        switch (notification.getAction()) {
            case "subscribe":
            case "add":
            case "active":
                // log.info("Notificacion de alta. subscriptionId: [" + subscriptionId + "] - msisdn: [" + notification.getSid() + "]");
                processSubscriptionNotification(notification, sop);
                msg = processBillingNotification(notification, sop);
                break;
            case "deactivate":
                // log.info("Notificacion de renovacion pendiente. subscriptionId: [" + subscriptionId + "] - msisdn: [" + notification.getSid() + "]. Ignorada");
                return true;
            case "renewal":
                // log.info("Notificacion de renovacion. subscriptionId: [" + subscriptionId + "] - msisdn: [" + notification.getSid() + "]");
                msg = processBillingNotification(notification, sop);
                break;
            case "unsubscribe":
                // log.info("Notificacion de baja. subscriptionId: [" + subscriptionId + "] - msisdn: [" + notification.getSid() + "]");
                return processUnsubscriptionNotification(notification, sop);
            case "purchase":
                // log.info("Notificacion Ondemand. subscriptionId: [" + subscriptionId + "] - msisdn: [" + notification.getSid() + "]. Ignorada");
                return true;
            default:
                String aux = "Notificacion desconocida. subscriptionId: [" + subscriptionId + "] - msisdn: [" + notification.getSid() + "]";
                log.error(aux);
                throw new IllegalArgumentException(aux);
        }
        return msg == null ? null : true;
    }

    private Boolean processSubscriptionNotification(Notification notification, SOP sop) {

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(sop.getProvider().getCountry().getCode());
            msg.setFromDate(locateDate("yyyy-MM-dd HH:mm:ss", notification.getCreated_at(), TimeZone.getTimeZone("UTC")));
            msg.setMessageType(MessageType.SUBSCRIPTION_ACTIVE);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(notification.getUser_id());
            msg.setTariffId(sop.getMainTariff().getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setTransactionId(notification.getId());
            msg.setChannelIn(notification.getChannel());
            if (notification.getSid() == null) {
                msg.setUserAccount(notification.getUser_id());
            } else {
                msg.setUserAccount(notification.getSid().replace("+", ""));
                msg.setRealUserAccount(true);
            }
            Extra extra = new Extra();
            extra.stringData = new HashMap();
            if (notification.getCampaign_id() != null) {
                extra.stringData.put("campaign_id", notification.getCampaign_id());
            }
            if (notification.getChannel_source() != null) {
                extra.stringData.put("channel_source", notification.getChannel_source());
            }
            if (notification.getPlan_id() != null) {
                extra.stringData.put("plan_id", notification.getPlan_id());
            }
            msg.setExtra(extra);

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar WauNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    private Boolean processUnsubscriptionNotification(Notification notification, SOP sop) {

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            if (notification.getCreated_at() != null) {
                msg.setFromDate(locateDate("yyyy-MM-dd HH:mm:ss", notification.getCreated_at(), TimeZone.getTimeZone("UTC")));
            }
            msg.setCodeCountry(sop.getProvider().getCountry().getCode());
            msg.setToDate(locateDate("yyyy-MM-dd HH:mm:ss", notification.getUpdated_at(), TimeZone.getTimeZone("UTC")));
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setTariffId(sop.getMainTariff().getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getSubscription_id());
            msg.setExternalUserAccount(notification.getUser_id());
            msg.setTransactionId(notification.getId());
            msg.setChannelOut(notification.getChannel());
            if (notification.getSid() == null) {
                msg.setUserAccount(notification.getUser_id());
            } else {
                msg.setUserAccount(notification.getSid().replace("+", ""));
                msg.setRealUserAccount(true);
            }
            Extra extra = new Extra();
            extra.stringData = new HashMap();
            if (notification.getCampaign_id() != null) {
                extra.stringData.put("campaign_id", notification.getCampaign_id());
            }
            if (notification.getChannel_source() != null) {
                extra.stringData.put("channel_source", notification.getChannel_source());
            }
            if (notification.getPlan_id() != null) {
                extra.stringData.put("plan_id", notification.getPlan_id());
            }
            msg.setExtra(extra);

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar WauNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private Boolean processBillingNotification(Notification notification, SOP sop) {

        if (notification.getPrice() == null || notification.getPrice().compareTo(BigDecimal.ZERO) < 1) {
            log.info("No se procesa la notificacion de cobros: [" + notification + "]");
            return true;
        }

        //Hay que procesarlo segun la notificacion FULL O NET amount...
        Tariff t = sop.findTariffByNetAmount(notification.getPrice());
        if (t == null) {
            //Verificamos si es una tarifa principal nueva
            t = updateMainTariff(sop, null, notification.getPrice());
            if (t == null) {
                //Creamos o actualizamos la tarifa hija
                t = updateNetAmountChildrenTariff(sop, notification.getPrice(), notification.getProduct_id());
            }
        }

        if (t == null) {
            log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
            return null;
        }

        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(sop.getProvider().getCountry().getCode());
            msg.setCurrencyId(sop.getProvider().getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(locateDate("yyyy-MM-dd HH:mm:ss", notification.getCreated_at(), TimeZone.getTimeZone("UTC")));
            msg.setTransactionId(notification.getProduct_id() + notification.getSubscription_id() + "_" + (notification.getId() == null ? msg.getFromDate().getTime() : notification.getId()));
            msg.setChargedDate(msg.getFromDate());
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getSubscription_id());
            msg.setExternalUserAccount(notification.getUser_id());
            msg.setChannelIn(notification.getChannel());
            if (notification.getSid() == null) {
                msg.setUserAccount(notification.getUser_id());
            } else {
                msg.setUserAccount(notification.getSid().replace("+", ""));
                msg.setRealUserAccount(true);
            }
            Extra extra = new Extra();
            extra.stringData = new HashMap();
            if (notification.getCampaign_id() != null) {
                extra.stringData.put("campaign_id", notification.getCampaign_id());
            }
            if (notification.getChannel_source() != null) {
                extra.stringData.put("channel_source", notification.getChannel_source());
            }
            if (notification.getPlan_id() != null) {
                extra.stringData.put("plan_id", notification.getPlan_id());
            }
            msg.setExtra(extra);

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar WauNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        subscription.setStatus(Subscription.Status.PENDING);
        rm.status = ResponseMessage.Status.OK_NS;
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {
        String msisdn = null;

        try {
            msisdn = userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider());
        } catch (Exception ex) {
        }

        ResponseMessage rm = new ResponseMessage();

        //  Por MSISDN
        if (msisdn != null && subscription.getUserAccount().length() > 5) {
            SCResponse sr = scResources.checkStatusByMSISDN(msisdn, subscription.getSop());
            responseProcess(sr, rm, subscription);
        } else if (subscription.getSubscriptionRegistry().getExternalId() != null) {
            SCResponse sr = scResources.checkStatusBySubsriptionId(subscription.getSubscriptionRegistry().getExternalId(), subscription.getSop());
            responseProcess(sr, rm, subscription);
        } else {
            rm.message = "No es posible evaluar el estado de la suscripcion.";
            log.fatal(rm.message + " [" + subscription + "]");
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        return rm;
    }

    private void responseProcess(SCResponse sr, ResponseMessage rm, Subscription subscription) {
        if (sr != null) {
            rm.message = sr.message;
            switch (sr.error) {
                case 0:
                case 1004:
                case 1007:
                case 1009:
                case 1011:
                    SCSubscription ssc = null;
                    if (sr.data.subscriptions != null && sr.data.subscriptions.length > 0 && 1009 != sr.error) {
                        //Subscripcion activa
                        for (SCSubscription s : sr.data.subscriptions) {
                            if (1L == s.status || (Subscription.Status.ACTIVE.equals(subscription.getStatus()) && 2L == s.status)) {
                                subscription.setStatus(Subscription.Status.ACTIVE);
                                rm.status = ResponseMessage.Status.ACTIVE;
                                ssc = s;
                                break;
                            }
                        }
                        if (ssc == null) {
                            //Suscripcion pendiente
                            for (SCSubscription s : sr.data.subscriptions) {
                                if (2L == s.status || 3L == s.status) {
                                    subscription.setStatus(Subscription.Status.PENDING);
                                    rm.status = ResponseMessage.Status.PENDING;
                                    ssc = s;
                                    break;
                                }
                            }
                        }
                    } else if (sr.data.subscription != null) {
                        subscription.setStatus(Subscription.Status.PENDING);
                        rm.status = ResponseMessage.Status.PENDING;
                        ssc = sr.data.subscription;
                    }

                    if (ssc == null) {
                        rm.status = ResponseMessage.Status.REMOVED;
                        subscription.setStatus(Subscription.Status.REMOVED);
                        if (subscription.getSubscriptionRegistry() != null && subscription.getSubscriptionRegistry().getExternalId() != null) {
                            //Suscripcion cancelada
                            if (sr.data.subscriptions != null && sr.data.subscriptions.length > 0) {
                                for (SCSubscription s : sr.data.subscriptions) {
                                    if (subscription.getSubscriptionRegistry().getExternalId().equals(s.id)) {
                                        subscription.getSubscriptionRegistry().setChannelIn(s.channel_in);
                                        subscription.getSubscriptionRegistry().setChannelOut(s.channel_out);
                                        if (s.sid != null) {
                                            subscription.setUserAccount(s.sid.replace("+", ""));
                                            subscription.getSubscriptionRegistry().setUserAccount(subscription.getUserAccount());
                                        }
                                        try {
                                            subscription.setSubscriptionDate(locateDate("yyyy-MM-dd HH:mm:ss", s.created_at, TimeZone.getTimeZone("UTC")));
                                            subscription.getSubscriptionRegistry().setSubscriptionDate(subscription.getSubscriptionDate());
                                        } catch (Exception ex) {
                                        }

                                        if (ssc.campaign_id != null) {
                                            subscription.getSubscriptionRegistry().getExtra().stringData.put("campaign_id", ssc.campaign_id);
                                        }
                                        if (ssc.protocol != null) {
                                            subscription.getSubscriptionRegistry().getExtra().stringData.put("protocol", ssc.protocol);
                                        }
                                        if (ssc.plan_id != null) {
                                            subscription.getSubscriptionRegistry().getExtra().stringData.put("plan_id", ssc.plan_id);
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        if (subscription.getSubscriptionRegistry() == null) {
                            subscription.setSubscriptionRegistry(new SubscriptionRegistry());
                            subscription.getSubscriptionRegistry().setSop(subscription.getSop());
                        }
                        subscription.getSubscriptionRegistry().setId(null);
                        if (ssc.id != null && !ssc.id.equals(subscription.getSubscriptionRegistry().getExternalId())) {
                            subscription.getSubscriptionRegistry().setAdnetworkTracking(null);
                            subscription.getSubscriptionRegistry().setExtra(new Extra());
                        }
                        if (ssc.sid != null) {
                            subscription.setUserAccount(ssc.sid.replace("+", ""));
                            subscription.getSubscriptionRegistry().setUserAccount(subscription.getUserAccount());
                        }
                        subscription.setExternalUserAccount(ssc.user_id);
                        try {
                            subscription.setSubscriptionDate(locateDate("yyyy-MM-dd HH:mm:ss", ssc.created_at, TimeZone.getTimeZone("UTC")));
                            subscription.getSubscriptionRegistry().setSubscriptionDate(subscription.getSubscriptionDate());
                        } catch (Exception ex) {
                        }
                        subscription.getSubscriptionRegistry().setChannelIn(ssc.channel_in);
                        subscription.getSubscriptionRegistry().setExternalId(ssc.id);

                        if (ssc.campaign_id != null) {
                            subscription.getSubscriptionRegistry().getExtra().stringData.put("campaign_id", ssc.campaign_id);
                        }
                        if (ssc.protocol != null) {
                            subscription.getSubscriptionRegistry().getExtra().stringData.put("protocol", ssc.protocol);
                        }
                        if (ssc.plan_id != null) {
                            subscription.getSubscriptionRegistry().getExtra().stringData.put("plan_id", ssc.plan_id);
                        }
                    }
                    break;
                case 1008:
                    subscription.setStatus(Subscription.Status.REMOVED);
                    rm.status = ResponseMessage.Status.REMOVED;
                    break;
                case 1005:
                    subscription.setStatus(Subscription.Status.BLACKLIST);
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    break;
                case 1010:
                    subscription.setStatus(Subscription.Status.LOCKED);
                    rm.status = ResponseMessage.Status.LOCKED;
                    break;
                default:
                    rm.status = ResponseMessage.Status.ERROR;
            }
        } else {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "No fue posible evaluar el estado de la suscripcion";
        }
    }

    public ResponseMessage sendPin(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;

        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        try {
            String channel = "web";
            if (subscription.getObject() != null) {
                channel = (String) subscription.getObject();
                channel = channel.trim().toLowerCase();
            }

            SCResponse response = scResources.sendPin(subscription.getUserAccount(), subscription.getSop(), channel);
            if (response != null) {
                rm.message = response.message;
                switch (response.error) {
                    case 0:
                        rm.status = ResponseMessage.Status.OK;
                        break;
                    case 1004:
                        rm.status = ResponseMessage.Status.ACTIVE;
                        subscription.setStatus(Subscription.Status.ACTIVE);
                        rm = getSubscriptionStatus(subscription);
                        break;
                    case 1005:
                        rm.status = ResponseMessage.Status.BLACKLIST;
                        subscription.setStatus(Subscription.Status.BLACKLIST);
                        break;
                    case 1010:
                        rm.status = ResponseMessage.Status.LOCKED;
                        subscription.setStatus(Subscription.Status.LOCKED);
                        break;
                    default:
                        rm.status = ResponseMessage.Status.ERROR;
                }
            } else {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "No fue posible realizar el envio de PIN";
            }
        } catch (Exception ex) {
            log.error("Error al enviar PIN. [" + subscription + "]. " + ex, ex);
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        try {
            SCResponse response = scResources.unsubscribe(subscription.getUserAccount(), subscription.getSop());
            if (response != null) {
                rm.message = response.message;
                switch (response.error) {
                    case 0:
                    case 1009:
                        rm.status = ResponseMessage.Status.OK;
                        try {
                            subscription.setUnsubscriptionDate(locateDate("yyyy-MM-dd HH:mm:ss", response.data.subscriptions[0].updated_at, TimeZone.getTimeZone("UTC")));
                        } catch (Exception ex) {
                            subscription.setUnsubscriptionDate(new Date());
                        }
                        subscription.getSubscriptionRegistry().setUnsubscriptionDate(subscription.getSubscriptionDate());
                        try {
                            subscription.getSubscriptionRegistry().setChannelOut(response.data.subscriptions[0].channel_out);
                        } catch (Exception ex) {
                        }
                        subscription.setStatus(Subscription.Status.REMOVED);
                        break;
                    default:
                        rm.status = ResponseMessage.Status.ERROR;
                }
            } else {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "No fue posible realizar el envio de PIN";
            }
        } catch (Exception ex) {
            log.error("Error al enviar PIN. [" + subscription + "]. " + ex, ex);
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage validatePin(Subscription subscription, String pin) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        try {
            SCResponse response = scResources.validatePin(subscription.getUserAccount(), subscription.getSop(), pin);
            if (response != null) {
                rm.message = response.message;
                switch (response.error) {
                    case 0:
                        responseProcess(response, rm, subscription);
                        subscription.setStatus(Subscription.Status.PENDING);
                        rm.status = ResponseMessage.Status.OK;
                        break;
                    case 1003:
                        subscription.setStatus(Subscription.Status.PENDING);
                        rm.status = ResponseMessage.Status.PIN_ERROR;
                        if (rm.message != null) {
                            rm.message = rm.message.replace("validator", "pin");
                        } else {
                            rm.message = "Invalid pin, new code was sent";
                        }
                        break;
                    case 1004:
                        rm.status = ResponseMessage.Status.ACTIVE;
                        subscription.setStatus(Subscription.Status.ACTIVE);
                        break;
                    case 1005:
                        rm.status = ResponseMessage.Status.BLACKLIST;
                        subscription.setStatus(Subscription.Status.BLACKLIST);
                        break;
                    case 1010:
                        rm.status = ResponseMessage.Status.LOCKED;
                        subscription.setStatus(Subscription.Status.LOCKED);
                        break;
                    default:
                        rm.status = ResponseMessage.Status.ERROR;
                }
            } else {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "No fue posible realizar la validacion del PIN: [" + pin + "]";
            }
        } catch (Exception ex) {
            log.error("Error al validar PIN. [" + subscription + "]. " + ex, ex);
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        if (!isNumber(subscription.getUserAccount()) && subscription.getUserAccount().length() > 20) {
            log.info("No se valida si el UserAccount: [" + subscription.getUserAccount() + "] es un numero");
        } else {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        }
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

}
