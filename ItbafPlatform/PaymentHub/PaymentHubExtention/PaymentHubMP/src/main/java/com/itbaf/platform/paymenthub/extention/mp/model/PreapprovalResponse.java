/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
@lombok.Getter
@lombok.Setter
public class PreapprovalResponse {

    private String status;
    private Preapproval response;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("status", status)
                .add("response", response)
                .omitNullValues().toString();
    }
}
