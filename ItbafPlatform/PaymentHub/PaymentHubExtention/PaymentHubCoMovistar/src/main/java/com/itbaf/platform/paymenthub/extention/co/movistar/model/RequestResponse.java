/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.co.movistar.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
@JacksonXmlRootElement(localName = "requestResponse")
public class RequestResponse {

    @JacksonXmlElementWrapper(localName = "params")
    private Params params;


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("params", params)
                .omitNullValues().toString();
    }


    /* @XmlType(name = "resultCode")
    @XmlEnum
    public enum ResultCode {

        @XmlEnumValue("0")
        Success("0"),
        @XmlEnumValue("1100")
        IPdeOrigenInvalido("1100"),
        @XmlEnumValue("1101")
        FormatVaspIdIncorrecto("1101"),
        @XmlEnumValue("1102")
        FormatoSubscriptionIdIncorrecto("1102"),
        @XmlEnumValue("1103")
        FormatoMsisdnIncorrecto("1103"),
        @XmlEnumValue("1104")
        IntegradorDesconocido("1104"),
        @XmlEnumValue("1105")
        AbonadoNoAprovisionado("1105"),
        @XmlEnumValue("1106")
        SuscripcionDesconocida("1106"),
        @XmlEnumValue("1107")
        AbonadoYaEstaSuscrito("1107"),
        @XmlEnumValue("1108")
        SuscripcionSinShortNumberPrincipal("1108"),
        @XmlEnumValue("1128")
        TipoDeSuscriptorInvalido("1128"),
        @XmlEnumValue("1200")
        AbonadoEnListaNegra("1200"),
        @XmlEnumValue("1999")
        ErrorInterno("1999"),
        @XmlEnumValue("Year")
        YEAR("Year");
        private final String value;

        ResultCode(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static ResultCode fromValue(String v) {
            for (ResultCode c : ResultCode.values()) {
                if (c.value.equals(v)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(v);
        }

    }*/
}
