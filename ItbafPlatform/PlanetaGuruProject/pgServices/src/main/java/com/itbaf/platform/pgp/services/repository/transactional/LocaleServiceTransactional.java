/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.pgp.model.Locale;
import com.itbaf.platform.pgp.repository.jpa.model.JPALocaleRepository;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.pgp.services.repository.LocaleService;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class LocaleServiceTransactional implements LocaleService {

    private final JPALocaleRepository localeRepository;
    private final SubscriptionManager subscriptionManager;

    @Inject
    public LocaleServiceTransactional(final JPALocaleRepository localeRepository,
            final SubscriptionManager subscriptionManager) {
        this.localeRepository = Validate.notNull(localeRepository, "A JPALocaleRepository class must be provided");
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
    }

    @Override
    public void saveOrUpdate(Locale locale) throws Exception {
        if (locale.getId() == null) {
            localeRepository.save(locale);
        } else {
            localeRepository.merge(locale);
        }
        subscriptionManager.updateMemoryCache();
    }

    /**
     * Sin pais, ni idioma ni proveedor. Language: 'es', Country: 'XX',
     * Provider: '1';
     */
    @Override
    public String getValueByKey(String key) {
        return getValueByKey("es", "XX", 1L, key);
    }

    /**
     * Sin pais ni proveedor. Country: 'XX', Provider: '1';
     */
    @Override
    public String getValueByKey(String languageCode, String key) {
        return getValueByKey(languageCode, "XX", 1L, key);
    }

    /**
     * Sin proveedor. Provider: '1';
     */
    @Override
    public String getValueByKey(String languageCode, String countryCode, String key) {
        return getValueByKey(languageCode, countryCode, 1L, key);
    }

    @Override
    public String getValueByKey(String languageCode, String countryCode, Long providerId, String key) {
        String value = null;
        Object aux = MainCache.memory12Hours().getIfPresent(languageCode + "." + countryCode + "." + providerId + "." + key);
        if (aux == null) {
            Locale lp = localeRepository.getValueByKey(languageCode, countryCode, providerId, key);
            if (lp != null) {
                value = lp.getValue();
                MainCache.memory12Hours().put(languageCode + "." + countryCode + "." + providerId + "." + key, value);
            }
        } else {
            value = (String) aux;
        }
        return value;
    }

    @Override
    public String i18n(String i18n, String providerId, String html) {
        if (providerId != null) {
            return i18n(i18n, Long.parseLong(providerId), html);
        }
        return i18n(i18n, 1L, html);
    }

    @Override
    public String i18n(String i18n, Long providerId, String html) {

        i18n = i18n.toLowerCase();
        String[] i18nx = i18n.split("-");
        if (i18nx.length == 1) {
            String aux = i18nx[0];
            i18nx = new String[2];
            i18nx[0] = aux;
            i18nx[1] = "XX";
        }
        while (html.contains("<i18n>")) {
            String tag = html.substring(html.indexOf("<i18n>"), html.indexOf("</i18n>") + 7);
            String key = tag.replace("<i18n>i18n.ix.px.", "").replace("</i18n>", "").trim();
            String aux;
            if (providerId == null) {
                aux = getValueByKey(i18nx[0], i18nx[1], key);
            } else {
                aux = getValueByKey(i18nx[0], i18nx[1], providerId, key);
            }

            if (aux == null) {
                aux = getValueByKey(i18nx[0], key);
                if (aux == null) {
                    aux = getValueByKey(key);
                    if (aux == null && providerId != null) {
                        aux = getValueByKey("en", "us", providerId, key);
                    } else if (aux == null) {
                        aux = getValueByKey("en", "us", key);
                    }
                    if (aux == null) {
                        aux = getValueByKey("en", key);
                        if (aux == null) {
                            aux = getValueByKey("es", key);
                            if (aux == null) {
                                aux = "";
                            }
                        }
                    }
                }
            }

            html = html.replace(tag, aux);
        }

        return html;
    }

}
