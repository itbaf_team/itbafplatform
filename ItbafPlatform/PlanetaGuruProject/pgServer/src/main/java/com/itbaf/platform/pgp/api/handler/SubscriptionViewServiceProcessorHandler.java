/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.Service;
import com.itbaf.platform.paymenthub.model.ServiceMatcher;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import static com.itbaf.platform.pgp.api.handler.ApiWalletServiceProcessorHandler.TYC_URL;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.model.Product;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import io.jsonwebtoken.lang.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class SubscriptionViewServiceProcessorHandler extends ServiceProcessorHandler {

    private final SubscriptionRegistryService subscriptionRegistryService;
    private final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler;

    @Inject
    public SubscriptionViewServiceProcessorHandler(final SubscriptionRegistryService subscriptionRegistryService,
            final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler) {
        this.subscriptionRegistryService = Validate.notNull(subscriptionRegistryService, "A SubscriptionRegistryService class must be provided");
        this.apiProfileServiceProcessorHandler = Validate.notNull(apiProfileServiceProcessorHandler, "An ApiProfileServiceProcessorHandler class must be provided");
    }

    /**
     * Metodo utilizado solamente por la vista
     *
     * @param isOrigin El Request viene del portal. Hara la baja automatica de
     * servicios para el producto
     */
    public String getUserSubscriptionHtml(User client, User user, String i18n, String productId, String redirectUri, boolean isOrigin) throws Exception {
        String html = readResource("/tpl/html/account/subscription.html", client, productId);
        html = html.replace("<siteHeaderScript/>", readHeaderResource("/tpl/html/header.html", client, user, productId));
        html = html.replace("<siteFooterScript/>", readResource("/tpl/html/footer.html", client, productId));

        UserProfile up = apiProfileServiceProcessorHandler.getUserProfileFromUser(client, user, null, false, false, false);

        if (up.getPhones() != null && !up.getPhones().isEmpty()) {
            Set<Msisdn> aux = new HashSet();
            for (Msisdn m : up.getPhones()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setPhones(aux);
        }
        if (up.getEmails() != null && !up.getEmails().isEmpty()) {
            Set<Email> aux = new HashSet();
            for (Email m : up.getEmails()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setEmails(aux);
        }

        //Suscripciones Activas
        try {
            List<Subscription> subscriptions = new ArrayList();
            List<Subscription> unsubscriptions = new ArrayList();
            Holder<Integer> autoCancel = new Holder();
            autoCancel.value = 0;
            Holder<String> urlRedirect = new Holder();
            getSubscriptions(user, subscriptions, unsubscriptions, false, isOrigin, productId, autoCancel, urlRedirect);

            if (urlRedirect.value != null) {
                //Esto hay q internacionalizarlo. 
                html = "<html>"
                        + "<head>"
                        + "<title>Planeta Guru</title>"
                        + "<link rel=\"shortcut icon\" href=\"https://cdn.planeta.guru/favicon.ico\" type=\"image/x-icon\">"
                        + "<meta http-equiv=\"refresh\" content=\"5; url=" + urlRedirect.value + "\">"
                        + "</head>"
                        + "<body>"
                        + "<a href=\"" + urlRedirect.value + "\">Haga clic aqui para continuar</a>"
                        + "<script>"
                        + "alert(\"Se ha cancelado la suscripcion.\");"
                        + "window.location = \"" + urlRedirect.value + "\""
                        + "</script>"
                        + "</body>"
                        + "</html>";
                return html;
            }

            up.setSubscriptions(subscriptions.isEmpty() ? null : subscriptions);
            up.setUnsubscriptions(unsubscriptions.isEmpty() ? null : unsubscriptions);
            up.setAutoCancel(autoCancel.value);
        } catch (Exception ex) {
        }

        //Lista de productos del menu
        html = getMenuProducts(html, productId, redirectUri);

        html = html.replace("uparams = {}", "uparams = " + mapper.writeValueAsString(up));
        return html;
    }

    //Metodo que solo deberia ser invocado por la API y no por la Vista
    public List<Subscription> getActiveSubscriptions(User client, User user) {

        syncSubscriptionsRun(user);
        while (client.getPrincipal() != null) {
            client = client.getPrincipal();
        }

        Map<Long, SOP> sops = new HashMap();
        for (Product p : client.getProducts()) {
            for (SOP sop : p.getSops()) {
                sops.put(sop.getId(), sop);
            }
        }

        List<String> userAccounts = new ArrayList();
        if (!user.getCredentialUserProperty().getUsername().getEmails().isEmpty()) {
            for (Email m : user.getCredentialUserProperty().getUsername().getEmails().values()) {
                if (m.getConfirmed()) {
                    userAccounts.add(m.getEmail());
                }
            }
        }
        if (!user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
            for (Msisdn m : user.getCredentialUserProperty().getUsername().getMsisdns().values()) {
                if (m.getConfirmed()) {
                    userAccounts.add(m.getMsisdn());
                }
            }
        }

        List<Subscription> subscriptions = subscriptionService.getActiveSubscriptions(userAccounts, sops.values());

        if (!Collections.isEmpty(subscriptions)) {
            List<Subscription> subscriptionsAux = new ArrayList();

            for (Subscription s : subscriptions) {
                if (Subscription.Status.ACTIVE.equals(s.getStatus())) {
                    Subscription aux = new Subscription();
                    aux.setSubscriptionRegistry(new SubscriptionRegistry());
                    aux.setUserAccount(s.getUserAccount());
                    aux.setStatus(s.getStatus());
                    SOP sop = new SOP();
                    sop.setId(s.getSubscriptionRegistry().getSop().getId());
                    sop.setProvider(s.getSubscriptionRegistry().getSop().getProvider());
                    sop.setServiceMatcher(s.getSubscriptionRegistry().getSop().getServiceMatcher());
                    try {
                        sop.setTyc(s.getSubscriptionRegistry().getSop().getIntegrationSettings().get("tyc"));
                    } catch (Exception ex) {
                    }
                    if (sop.getTyc() == null) {
                        sop.setTyc(TYC_URL);
                    }
                    aux.setCharged(s.isCharged());
                    aux.getSubscriptionRegistry().setSop(sop);
                    aux.getSubscriptionRegistry().setId(s.getSubscriptionRegistry().getId());
                    aux.setSubscriptionDate(s.getSubscriptionDate());
                    subscriptionsAux.add(aux);
                }
            }
            return subscriptionsAux;
        }

        return new ArrayList();
    }

    //Metodo que solo deberia ser invocado por la API y no por la Vista
    public Subscription.Status hasActiveSubscriptions(User client, User user) {

        syncSubscriptionsRun(user);
        while (client.getPrincipal() != null) {
            client = client.getPrincipal();
        }

        Map<Long, SOP> sops = new HashMap();
        for (Product p : client.getProducts()) {
            for (SOP sop : p.getSops()) {
                sops.put(sop.getId(), sop);
            }
        }

        List<String> userAccounts = new ArrayList();
        if (!user.getCredentialUserProperty().getUsername().getEmails().isEmpty()) {
            for (Email m : user.getCredentialUserProperty().getUsername().getEmails().values()) {
                if (m.getConfirmed()) {
                    userAccounts.add(m.getEmail());
                }
            }
        }
        if (!user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
            for (Msisdn m : user.getCredentialUserProperty().getUsername().getMsisdns().values()) {
                if (m.getConfirmed()) {
                    userAccounts.add(m.getMsisdn());
                }
            }
        }

        List<Subscription> subscriptions = subscriptionService.getActiveSubscriptions(userAccounts, sops.values());
        if (subscriptions != null && !subscriptions.isEmpty()) {
            for (Subscription s : subscriptions) {
                if (Subscription.Status.ACTIVE.equals(s.getStatus())) {
                    return Subscription.Status.ACTIVE;
                }
            }
        }

        return Subscription.Status.REMOVED;
    }

    /**
     * Obtiene el estado actual de las suscripciones
     *
     * @param isActive Solamente procesa las suscripciones ACTIVAS
     * @param isOrigin El Request viene del portal. Hara la baja automatica de
     * servicios para el producto
     */
    private void getSubscriptions(User user, List<Subscription> subscriptions, List<Subscription> unsubscriptions,
            boolean isActive, boolean isOrigin, String productId, Holder<Integer> autoCancel, Holder<String> urlRedirect) {
        List<Subscription> subscriptionsAux = new ArrayList();

        try {
            if (!user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                List<Msisdn> msisdns = new ArrayList();
                msisdns.addAll(user.getCredentialUserProperty().getUsername().getMsisdns().values());
                for (Msisdn m : msisdns) {
                    if (m.getCountry() != null && m.getConfirmed()) {
                        try {
                            List<Subscription> sl = subscriptionService.getByMSISDNAndCountry(m.getMsisdn(), m.getCountry());
                            if (sl != null && !sl.isEmpty()) {
                                subscriptionsAux.addAll(sl);
                                String msisdn = sl.get(0).getUserAccount();
                                try {
                                    if (msisdn != null && !m.getMsisdn().equals(msisdn)) {
                                        Msisdn maux = user.getCredentialUserProperty().getUsername().getMsisdns().remove(m.getMsisdn());
                                        maux.setMsisdn(msisdn);
                                        user.getCredentialUserProperty().getUsername().getMsisdns().put(msisdn, maux);
                                        userService.saveOrUpdate(user);
                                    }
                                } catch (Exception ex) {
                                }
                                try {
                                    Provider p = sl.get(0).getSubscriptionRegistry().getSop().getProvider();
                                    Provider auxp = user.getCredentialUserProperty().getUsername().getMsisdns().get(msisdn).getProvider();
                                    if (p != null && auxp != null && !p.getName().equals(auxp.getName())) {
                                        user.getCredentialUserProperty().getUsername().getMsisdns().get(msisdn).setProvider(p);
                                        userService.saveOrUpdate(user);
                                    } else if (p != null && auxp == null) {
                                        user.getCredentialUserProperty().getUsername().getMsisdns().get(msisdn).setProvider(p);
                                        userService.saveOrUpdate(user);
                                    }
                                } catch (Exception ex) {
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al obtener suscripciones por Msisdns. [" + m + "]. " + ex, ex);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar suscripciones por Msisdns. [" + user + "]. " + ex, ex);
        }

        try {
            if (!user.getCredentialUserProperty().getUsername().getEmails().isEmpty()) {
                for (Email e : user.getCredentialUserProperty().getUsername().getEmails().values()) {
                    if (!e.getConfirmed()) {
                        continue;
                    }
                    List<Subscription> sl = subscriptionService.findSubscriptionsByUserAccount(e.getEmail());
                    if (sl != null && !sl.isEmpty()) {
                        subscriptionsAux.addAll(sl);
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar suscripciones por Emails. [" + user + "]. " + ex, ex);
        }

        Map<Long, SOP> sopMap = new HashMap();
        try {
            if (isOrigin) {
                Product product = productService.findbyId(Long.parseLong(productId));
                for (SOP sop : product.getSops()) {
                    if ("true".equals(sop.getIntegrationSettings().get("unsubscription.one.click"))) {
                        sopMap.put(sop.getId(), sop);
                    }
                }
            }
        } catch (Exception ex) {
        }

        for (Subscription s : subscriptionsAux) {
            try {
                if (s.getSubscriptionRegistry() != null && s.getSubscriptionRegistry().getId() != null) {
                    if (isActive) {
                        if (Subscription.Status.ACTIVE.equals(s.getStatus())) {
                            SOP sop = new SOP();
                            sop.setId(s.getSubscriptionRegistry().getSop().getId());
                            sop.setProvider(s.getSubscriptionRegistry().getSop().getProvider());
                            sop.setServiceMatcher(s.getSubscriptionRegistry().getSop().getServiceMatcher());
                            s.getSubscriptionRegistry().setSop(sop);
                            subscriptions.add(s);
                        }
                    } else {
                        try {
                            //Bajas automaticas
                            if (isOrigin & sopMap.containsKey(s.getSubscriptionRegistry().getSop().getId())
                                    & (Subscription.Status.PENDING.equals(s.getStatus())
                                    || Subscription.Status.ACTIVE.equals(s.getStatus())
                                    || Subscription.Status.TRIAL.equals(s.getStatus()))) {
                                ResponseMessage rm = subscriptionService.unsubscribeFacadeRequest(s.getUserAccount(), s.getSubscriptionRegistry().getSop(), "AUTO_PROFILE_" + user.getId());
                                if (ResponseMessage.Status.OK.equals(rm.status)) {
                                    autoCancel.value = 1;
                                    SubscriptionRegistry sr = subscriptionRegistryService.findById(s.getSubscriptionRegistry().getId());
                                    s.setStatus(sr.getSubscription().getStatus());
                                    s.getSubscriptionRegistry().setUnsubscriptionDate(sr.getUnsubscriptionDate());

                                    //Cerramos la sesion!
                                    if ("true".equals(s.getSubscriptionRegistry().getSop().getIntegrationSettings().get("unsubscribe.close.session"))) {
                                        try {
                                            user.getSessionUserProperty().getSessions().clear();
                                            userService.saveOrUpdate(user);
                                        } catch (Exception ex) {
                                        }
                                    }
                                    if (urlRedirect.value == null) {
                                        urlRedirect.value = s.getSubscriptionRegistry().getSop().getIntegrationSettings().get("unsubscription.redirect.url");
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al cancelar la suscripcion de: [" + s + "]. " + ex, ex);
                        }

                        String frec = getFrecuency(s.getSubscriptionRegistry().getSop().getIntegrationSettings().get("frequencyType"));
                        String service = s.getSubscriptionRegistry().getSop().getServiceMatcher().getGenericName()
                                + (frec == null || frec.length() < 2 ? "" : " (" + frec + ")");
                        String gn = s.getSubscriptionRegistry().getSop().getProvider().getGenericName();
                        String unsubscriptionMessageView = s.getSubscriptionRegistry().getSop().getIntegrationSettings().get("unsubscriptionMessageView");
                        s.getSubscriptionRegistry().setSop(new SOP());
                        s.getSubscriptionRegistry().getSop().setServiceMatcher(new ServiceMatcher());
                        s.getSubscriptionRegistry().getSop().getServiceMatcher().setService(new Service());
                        s.getSubscriptionRegistry().getSop().getServiceMatcher().getService().setName(service);
                        s.getSubscriptionRegistry().getSop().setProvider(new Provider());
                        s.getSubscriptionRegistry().getSop().getProvider().setGenericName(gn);

                        if (Subscription.Status.PENDING.equals(s.getStatus())
                                || Subscription.Status.ACTIVE.equals(s.getStatus())
                                || Subscription.Status.TRIAL.equals(s.getStatus())) {
                            s.setObject(unsubscriptionMessageView);
                            subscriptions.add(s);
                        } else {
                            if (Subscription.Status.REMOVED.equals(s.getStatus())) {
                                s.setStatus(Subscription.Status.CANCELLED);
                            }
                            unsubscriptions.add(s);
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar suscripcion: [" + s + "]. " + ex, ex);
            }
        }
    }

    public boolean cancelSubscription(User user, Long aLong, Holder<String> urlRedirect) {
        try {
            SubscriptionRegistry sr = subscriptionRegistryService.findById(aLong);
            Email email = user.getCredentialUserProperty().getUsername().getEmail(sr.getSubscription().getUserAccount());
            Msisdn msisdn = user.getCredentialUserProperty().getUsername().getMsisdn(sr.getSubscription().getUserAccount());

            if (email == null && msisdn == null) {
                return false;
            }
            if (email != null && !email.getConfirmed()) {
                return false;
            }
            if (msisdn != null && !msisdn.getConfirmed()) {
                return false;
            }

            if (Subscription.Status.ACTIVE.equals(sr.getSubscription().getStatus())
                    || Subscription.Status.PENDING.equals(sr.getSubscription().getStatus())
                    || Subscription.Status.TRIAL.equals(sr.getSubscription().getStatus())) {

                ResponseMessage rm = subscriptionService.unsubscribeFacadeRequest(sr.getSubscription().getUserAccount(), sr.getSop(), "PROFILE_" + user.getId());
                if (ResponseMessage.Status.OK.equals(rm.status)) {
                    urlRedirect.value = sr.getSop().getIntegrationSettings().get("unsubscription.redirect.url");
                    return true;
                }

            }
        } catch (Exception ex) {
        }
        return false;
    }

}
