/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.api.model.WalletTransactionView;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.wallet.WalletTransaction;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class TransactionViewServiceProcessorHandler extends ServiceProcessorHandler {

    private final ApiWalletServiceProcessorHandler apiWalletServiceProcessorHandler;
    private final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler;

    @Inject
    public TransactionViewServiceProcessorHandler(final ApiWalletServiceProcessorHandler apiWalletServiceProcessorHandler,
            final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler) {
        this.apiWalletServiceProcessorHandler = Validate.notNull(apiWalletServiceProcessorHandler, "An ApiWalletServiceProcessorHandler class must be provided");
        this.apiProfileServiceProcessorHandler = Validate.notNull(apiProfileServiceProcessorHandler, "An ApiProfileServiceProcessorHandler class must be provided");
    }

    public String getUserTransactionHtml(User client, User user, String i18n, String productId, String prvdr, String redirectUri) throws Exception {
        String html = readResource("/tpl/html/account/transaction.html", client, productId);
        html = html.replace("<siteHeaderScript/>", readHeaderResource("/tpl/html/header.html", client, user, productId));
        html = html.replace("<siteFooterScript/>", readResource("/tpl/html/footer.html", client, productId));

        UserProfile up = apiProfileServiceProcessorHandler.getUserProfileFromUser(client, user, null, false, false, false);

        if (up.getPhones() != null && !up.getPhones().isEmpty()) {
            Set<Msisdn> aux = new HashSet();
            for (Msisdn m : up.getPhones()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setPhones(aux);
        }
        if (up.getEmails() != null && !up.getEmails().isEmpty()) {
            Set<Email> aux = new HashSet();
            for (Email m : up.getEmails()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setEmails(aux);
        }

        //Lista de productos del menu
        html = getMenuProducts(html, productId, redirectUri);

        //Transacciones realizadas
        try {
            Calendar c = Calendar.getInstance();
            c.add(Calendar.MONTH, -3);
            List<WalletTransaction> wtl = apiWalletServiceProcessorHandler.getWalletTransaction(user, c.getTime(), new Date());
            processWalletTransaction(up, wtl, i18n, prvdr);
        } catch (Exception ex) {
        }

        html = html.replace("uparams = {}", "uparams = " + mapper.writeValueAsString(up));
        return html;
    }

    public UserProfile getOrders(User user, String fromString, String i18n, String prvdr) {
        UserProfile up = new UserProfile();
        //Transacciones realizadas
        try {
            Calendar c = Calendar.getInstance();

            switch (fromString) {
                case "3m":
                    c.add(Calendar.MONTH, -3);
                    break;
                case "6m":
                    c.add(Calendar.MONTH, -6);
                    break;
                case "1y":
                    c.add(Calendar.YEAR, -1);
                    break;
                case "all":
                    c.set(2000, 0, 1);
                    break;
                default:
                    c.add(Calendar.MONTH, -1);
            }
            List<WalletTransaction> wtl = apiWalletServiceProcessorHandler.getWalletTransaction(user, c.getTime(), new Date());
            processWalletTransaction(up, wtl, i18n, prvdr);
            return up;
        } catch (Exception ex) {
        }

        return null;
    }

    private void processWalletTransaction(UserProfile up, List<WalletTransaction> wtl, String i18n, String prvdr) {
        if (wtl != null && !wtl.isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            up.setTransactions(new ArrayList());
            for (WalletTransaction wt : wtl) {
                WalletTransactionView wtv = new WalletTransactionView();
                wtv.localCurrency = wt.getLocalCurrency();
                wtv.date = sdf.format(wt.getChargedDate());
                wtv.localAmount = wt.getLocalAmount();
                wtv.description = wt.getDescription();
                wtv.amount = wt.getAmount();
                wtv.currency = wt.getCurrency();
                if (wt.getSop() != null) {
                    try {
                        wtv.symbol = wt.getSop().getProvider().getCountry().getSymbolCurrency();
                    } catch (Exception ex) {
                    }
                }

                String d = (wt.getChargedDate().getTime() + "").substring(0, 7);
                switch (wt.getType()) {
                    case ADD:
                        if (wt.getTid().startsWith("PH_SB_")) {
                            wtv.type = WalletTransactionView.Type.SUBSCRIPTION;
                            wtv.tid = "S" + wt.getId() + "T" + d;
                            if (wtv.description == null && wt.getSop() != null) {
                                String frec = getFrecuency(wt.getSop().getIntegrationSettings().get("frequencyType"));
                                wtv.description = WalletTransactionView.Type.getI8Nfrom(wtv.type) + ": "
                                        + wt.getSop().getServiceMatcher().getService().getName() + "."
                                        + (frec == null || frec.length() < 2 ? " (" + wt.getSop().getProvider().getGenericName() + ")" : " (" + wt.getSop().getProvider().getGenericName() + " - " + frec + ")");
                                wtv.description = i18n(i18n, prvdr, wtv.description);
                            }
                        } else if (wt.getTid().startsWith("PH_OB_")) {
                            wtv.type = WalletTransactionView.Type.ONDEMAND;
                            wtv.tid = "O" + wt.getId() + "T" + d;
                        } else {
                            wtv.type = WalletTransactionView.Type.BONUS_REFUNDS;
                            wtv.tid = "R" + wt.getId() + "T" + d;
                        }
                        break;
                    case WITHDRAW:
                        wtv.type = WalletTransactionView.Type.PURCHASE;
                        wtv.tid = "P" + wt.getId() + "T" + d;
                        break;
                }

                wtv.typeI8N = WalletTransactionView.Type.getI8Nfrom(wtv.type);
                wtv.typeI8N = i18n(i18n, prvdr, wtv.typeI8N);

                up.getTransactions().add(wtv);
            }
        }
    }

}
