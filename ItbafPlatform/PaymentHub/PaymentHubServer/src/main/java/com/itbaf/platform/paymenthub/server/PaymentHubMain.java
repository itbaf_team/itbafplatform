/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.server;

import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING_OD;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.paymenthub.server.consumer.RabbitMQNotificationMessageProcessorFactory;
import com.itbaf.platform.paymenthub.server.notification.NotificationProcessor;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import com.itbaf.platform.rmq.services.RabbitMQMemoryProcessorImpl;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain {

    private final PHInstance instance;
    private final ExecutorService cachedThreadPool;
    private final RabbitMQConsumer rabbitMQConsumer;
    private final BasicDataBaseConnection dataBaseConnection;
    private final RabbitMQMemoryProcessorImpl memoryProcessor;
    private final ScheduledExecutorService scheduledThreadPool;
    private final RabbitMQNotificationMessageProcessorFactory rabbitMQMessageProcessorFactory;
    private final Map<PaymentHubMessage.NotificationType, NotificationProcessor> notificationProcessorMap;

    @Inject
    public PaymentHubMain(final PHInstance instance,
            final ExecutorService cachedThreadPool,
            final RabbitMQConsumer rabbitMQConsumer,
            final BasicDataBaseConnection dataBaseConnection,
            final RabbitMQMemoryProcessorImpl memoryProcessor,
            final ScheduledExecutorService scheduledThreadPool,
            final Set<NotificationProcessor> notificationProcessorSet,
            final RabbitMQNotificationMessageProcessorFactory rabbitMQMessageProcessorFactory) {
        this.instance = Validate.notNull(instance, "A PHInstance class must be provided");
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "An ExecutorService class must be provided");
        this.rabbitMQConsumer = Validate.notNull(rabbitMQConsumer, "A RabbitMQConsumer class must be provided");
        this.memoryProcessor = Validate.notNull(memoryProcessor, "A RabbitMQMemoryProcessorImpl class must be provided");
        this.dataBaseConnection = Validate.notNull(dataBaseConnection, "A BasicDataBaseConnection class must be provided");
        this.scheduledThreadPool = Validate.notNull(scheduledThreadPool, "A ScheduledExecutorService class must be provided");
        this.rabbitMQMessageProcessorFactory = Validate.notNull(rabbitMQMessageProcessorFactory, "A RabbitMQMessageProcessorFactory class must be provided");

        notificationProcessorMap = new HashMap();
        if (notificationProcessorSet != null) {
            log.info("notificationProcessorSet size: " + notificationProcessorSet.size());
            for (NotificationProcessor nr : notificationProcessorSet) {
                log.info("JF- NotificationProcessor item: [" + nr.getNotificationType() + "] - [" + nr.getClass().getSimpleName() + "]");
                notificationProcessorMap.put(nr.getNotificationType(), nr);
            }
        }
    }

    public void start() {
        log.info("Initializing app... PaymentHubServer");
        try {
            rabbitMQConsumer.createFanoutChannel(CHACHE_MEMORY_UPDATE, memoryProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer '" + CHACHE_MEMORY_UPDATE + "'. " + ex, ex);
        }
        Set<Provider> providers = instance.getConfigurations().keySet();
        for (Provider p : providers) {
            ChannelQuantityProvider cqp = instance.getConfigurations().get(p);

            if (cqp.getSubscriptionConsumerQuantity() > 0) {
                createConsumer(p.getName() + NOTIFICATION_SUBSCRIPTION, cqp.getSubscriptionConsumerQuantity(),
                        notificationProcessorMap.get(PaymentHubMessage.NotificationType.SUBSCRIPTION));
            }
            if (cqp.getBillingConsumerQuantity() > 0) {
                createConsumer(p.getName() + NOTIFICATION_BILLING, cqp.getBillingConsumerQuantity(),
                        notificationProcessorMap.get(PaymentHubMessage.NotificationType.BILLING));
            }
            if (cqp.getBillingConsumerQuantity() > 0) {
                createConsumer(p.getName() + NOTIFICATION_BILLING_OD, cqp.getOnDemandBillingConsumerQuantity(),
                        notificationProcessorMap.get(PaymentHubMessage.NotificationType.ONDEMAND_BILLING));
            }
        }
        log.info("Initializing app OK. PaymentHubServer");
    }

    private RabbitMQConsumer createConsumer(String exchangeName, Integer consumerQuantity, NotificationProcessor notificationProcessor) {
        rabbitMQConsumer.createChannel(exchangeName, consumerQuantity, createRabbitMQMessageProcessor(notificationProcessor));
        return rabbitMQConsumer;
    }

    private RabbitMQMessageProcessor createRabbitMQMessageProcessor(NotificationProcessor messageProcessor) {
        return rabbitMQMessageProcessorFactory.create(messageProcessor);
    }

    public void stop() {
        rabbitMQConsumer.stopConsumers();
        stopThreads();
        dataBaseConnection.shutdownConnectionsPool();
        log.info("App destroyed. PaymentHubServer");
    }

    public void stopThreads() {
        log.info("Finalizando Threads en 5 segundos... ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }
        try {
            log.info("Finalizando scheduledThreadPool ");
            scheduledThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            log.info("Esperando Threads por 2 segundos... ");
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        try {
            scheduledThreadPool.shutdownNow();
            log.info("Finalizado scheduledThreadPool ");
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdownNow();
        } catch (Exception ex) {
        }
    }

}
