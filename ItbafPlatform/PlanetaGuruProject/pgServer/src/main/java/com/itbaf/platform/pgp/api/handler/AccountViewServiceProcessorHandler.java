/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Language;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.api.model.WalletTransactionView;
import com.itbaf.platform.pgp.commons.resources.CookieFilter;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.wallet.WalletTransaction;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import com.itbaf.platform.pgp.services.repository.ProductService;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class AccountViewServiceProcessorHandler extends ServiceProcessorHandler {

    private final ApiWalletServiceProcessorHandler apiWalletServiceProcessorHandler;
    private final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler;

    @Inject
    public AccountViewServiceProcessorHandler(final ProductService productService,
            final ApiWalletServiceProcessorHandler apiWalletServiceProcessorHandler,
            final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler) {
        this.productService = Validate.notNull(productService, "A ProductService class must be provided");
        this.apiWalletServiceProcessorHandler = Validate.notNull(apiWalletServiceProcessorHandler, "An ApiWalletServiceProcessorHandler class must be provided");
        this.apiProfileServiceProcessorHandler = Validate.notNull(apiProfileServiceProcessorHandler, "An ApiProfileServiceProcessorHandler class must be provided");
    }

    @Override
    public String i18n(String i18n, String providerId, String html) {
        String result = super.i18n(i18n, providerId, html);
        try {
            String url = this.genericPropertyService.getValueByKey("pg.server.url");
            result = result.replace("<canonical/>", url);
        } catch (Exception ex) {
        }
        return result;
    }

    /**
     * Obtiene el HTML de la pagina principal Account
     *
     * @param client
     * @param user
     * @param productId
     * @param redirectUri
     * @return
     */
    public String getUserAccountHtml(User client, User user, String productId, String redirectUri) throws Exception {
        try {
            if (user.getProfileUserProperty().getLanguage() == null && user.getProfileUserProperty().getCountry() != null) {
                Country c = countryService.findById(user.getProfileUserProperty().getCountry().getId());
                if (c.getLanguages().size() > 1) {
                    Set<Language> list = new TreeSet();
                    list.addAll(c.getLanguages());
                    user.getProfileUserProperty().setLanguage(list.iterator().next().getCode().toLowerCase().trim()
                            + "-" + c.getCode().toUpperCase().trim());
                } else {
                    user.getProfileUserProperty().setLanguage(c.getLanguages().iterator().next().getCode()
                            + "-" + c.getCode().toUpperCase().trim());
                }
                userService.saveOrUpdate(user);
            }
        } catch (Exception ex) {
        }

        String html = readResource("/tpl/html/account/account.html", client, productId);
        html = html.replace("<siteHeaderScript/>", readHeaderResource("/tpl/html/header.html", client, user, productId));
        html = html.replace("<siteFooterScript/>", readResource("/tpl/html/footer.html", client, productId));

        //Perfil de usuario
        UserProfile up = apiProfileServiceProcessorHandler.getUserProfileFromUser(client, user, null, false, false, false);
        if (up.getPhones() != null && !up.getPhones().isEmpty()) {
            Set<Msisdn> aux = new HashSet();
            for (Msisdn m : up.getPhones()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setPhones(aux);
        }
        if (up.getEmails() != null && !up.getEmails().isEmpty()) {
            Set<Email> aux = new HashSet();
            for (Email m : up.getEmails()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setEmails(aux);
        }

        //Lista de productos del menu
        html = getMenuProducts(html, productId, redirectUri);

        //Creditos por producto
        try {
            List<User> clients = userService.getAllUserClients();
            for (User u : clients) {
                try {
                    Map<String, BigDecimal> credits = apiWalletServiceProcessorHandler.getWalletFunds(u, user);
                    if (credits != null && !credits.isEmpty()) {
                        if (up.getCreditsByProduct() == null) {
                            up.setCreditsByProduct(new HashMap());
                        }
                        up.getCreditsByProduct().put(u.getCredentialUserProperty().getClient().getClientName(), credits);
                    }
                } catch (Exception ex) {
                }
            }
        } catch (Exception ex) {
        }

        //Creditos total
        try {
            up.setCredits(apiWalletServiceProcessorHandler.getWalletFunds(user));
        } catch (Exception ex) {
        }

        //Transacciones realizadas
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar c = Calendar.getInstance();
            c.add(Calendar.MONTH, - 1);
            List<WalletTransaction> wtl = apiWalletServiceProcessorHandler.getWalletTransaction(user, c.getTime(), new Date());
            if (wtl != null && !wtl.isEmpty()) {
                up.setTransactions(new ArrayList());
                for (WalletTransaction wt : wtl) {
                    WalletTransactionView wtv = new WalletTransactionView();
                    wtv.localCurrency = wt.getLocalCurrency();
                    wtv.date = sdf.format(wt.getChargedDate());
                    wtv.localAmount = wt.getLocalAmount();
                    String d = (wt.getChargedDate().getTime() + "").substring(0, 7);
                    switch (wt.getType()) {
                        case ADD:
                            if (wt.getTid().startsWith("PH_SB_")) {
                                wtv.type = WalletTransactionView.Type.SUBSCRIPTION;
                                wtv.tid = "S" + wt.getId() + "T" + d;
                            } else if (wt.getTid().startsWith("PH_OB_")) {
                                wtv.type = WalletTransactionView.Type.ONDEMAND;
                                wtv.tid = "O" + wt.getId() + "T" + d;
                            } else {
                                wtv.type = WalletTransactionView.Type.BONUS_REFUNDS;
                                wtv.tid = "R" + wt.getId() + "T" + d;
                            }
                            break;
                        case WITHDRAW:
                            wtv.type = WalletTransactionView.Type.PURCHASE;
                            wtv.tid = "P" + wt.getId() + "T" + d;
                            break;
                    }

                    wtv.typeI8N = WalletTransactionView.Type.getI8Nfrom(wtv.type);
                    up.getTransactions().add(wtv);
                }
            }
        } catch (Exception ex) {
        }

        html = html.replace("uparams = {}", "uparams = " + mapper.writeValueAsString(up));
        return html;
    }

    public void signOutUserAccount(String path, User client, User user, CookieFilter cf) {
        RequestTokenMessage tm = new RequestTokenMessage();
        tm.clientId = client.getCredentialUserProperty().getClient().getClientId();
        tm.accessToken = cf.at;
        tm.scope = RequestTokenMessage.Scope.getScope(path);
        super.deleteUserToken(tm);
    }

}
