
package org.csapi.schema.parlayx.payment.amount_charging.v2_1.local;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.csapi.schema.parlayx.payment.v2_1.Parameters;
import org.csapi.schema.parlayx.payment.v2_1.QueryConditionsType;


/**
 * <p>Java class for getChargingTransactionList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getChargingTransactionList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="queryType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="queryConditions" type="{http://www.csapi.org/schema/parlayx/payment/v2_1}QueryConditionsType"/>
 *         &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="count" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="extraParams" type="{http://www.csapi.org/schema/parlayx/payment/v2_1}Parameters" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getChargingTransactionList", propOrder = {
    "queryType",
    "queryConditions",
    "start",
    "count",
    "extraParams"
})
public class GetChargingTransactionList {

    protected int queryType;
    @XmlElement(required = true)
    protected QueryConditionsType queryConditions;
    protected Integer start;
    protected Integer count;
    protected Parameters extraParams;

    /**
     * Gets the value of the queryType property.
     * 
     */
    public int getQueryType() {
        return queryType;
    }

    /**
     * Sets the value of the queryType property.
     * 
     */
    public void setQueryType(int value) {
        this.queryType = value;
    }

    /**
     * Gets the value of the queryConditions property.
     * 
     * @return
     *     possible object is
     *     {@link QueryConditionsType }
     *     
     */
    public QueryConditionsType getQueryConditions() {
        return queryConditions;
    }

    /**
     * Sets the value of the queryConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryConditionsType }
     *     
     */
    public void setQueryConditions(QueryConditionsType value) {
        this.queryConditions = value;
    }

    /**
     * Gets the value of the start property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStart() {
        return start;
    }

    /**
     * Sets the value of the start property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStart(Integer value) {
        this.start = value;
    }

    /**
     * Gets the value of the count property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCount(Integer value) {
        this.count = value;
    }

    /**
     * Gets the value of the extraParams property.
     * 
     * @return
     *     possible object is
     *     {@link Parameters }
     *     
     */
    public Parameters getExtraParams() {
        return extraParams;
    }

    /**
     * Sets the value of the extraParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link Parameters }
     *     
     */
    public void setExtraParams(Parameters value) {
        this.extraParams = value;
    }

}
