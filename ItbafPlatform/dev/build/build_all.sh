#!/bin/bash
#Lista de argumentos de linea de comandos
#Referencia: https://maven.apache.org/ref/3.1.0/maven-embedder/cli.html

#-B -> Ejecutar en modo batch no interactivo
#-e -> Mostrar errores
#--also-make -> Si se especifica una lista de proyectos compilar todos los proyectos
#--projects -> list de poyectos
#-s -> Archivo de configuración de usuario a utilizar
#-D -> define propiedades del sistema

#original
#docker run -it --rm  -v "$(pwd)"/dev/.m2:/root/.m2 -v "$(pwd)":/usr/src  -w /usr/src maven:3-jdk-11 mvn -B -e --also-make  --projects PaymentHub/PaymentHubServer/ -s ./settings.xml -Dmaven.test.skip=true  clean compile install package

docker run -it --rm  -v "$(pwd)"/dev/.m2:/root/.m2 -v "$(pwd)":/usr/src -w /usr/src maven:3-jdk-11 mvn -T 1C -B -e --also-make --projects PaymentHub/PaymentHubServer/ -s ./settings.xml -Dmaven.test.skip=true  clean compile install package
