
package org.csapi.schema.parlayx.subscribeproduct.v1_0.local;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.ConsumeRefundHistoryInfo;


/**
 * <p>Java class for getConsumeRefundHistoryListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getConsumeRefundHistoryListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="recordNum" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="consumeRefundHistoryInfos" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}ConsumeRefundHistoryInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getConsumeRefundHistoryListResponse", propOrder = {
    "resultCode",
    "recordNum",
    "consumeRefundHistoryInfos"
})
public class GetConsumeRefundHistoryListResponse {

    protected int resultCode;
    protected int recordNum;
    protected List<ConsumeRefundHistoryInfo> consumeRefundHistoryInfos;

    /**
     * Gets the value of the resultCode property.
     * 
     */
    public int getResultCode() {
        return resultCode;
    }

    /**
     * Sets the value of the resultCode property.
     * 
     */
    public void setResultCode(int value) {
        this.resultCode = value;
    }

    /**
     * Gets the value of the recordNum property.
     * 
     */
    public int getRecordNum() {
        return recordNum;
    }

    /**
     * Sets the value of the recordNum property.
     * 
     */
    public void setRecordNum(int value) {
        this.recordNum = value;
    }

    /**
     * Gets the value of the consumeRefundHistoryInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the consumeRefundHistoryInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsumeRefundHistoryInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsumeRefundHistoryInfo }
     * 
     * 
     */
    public List<ConsumeRefundHistoryInfo> getConsumeRefundHistoryInfos() {
        if (consumeRefundHistoryInfos == null) {
            consumeRefundHistoryInfos = new ArrayList<ConsumeRefundHistoryInfo>();
        }
        return this.consumeRefundHistoryInfos;
    }

}
