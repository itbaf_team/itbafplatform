/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.billing.Tariff;

/**
 *
 * @author javier
 */
public interface TariffService {

    public void saveOrUpdate(Tariff tariff) throws Exception;

    public Tariff findById(Long tariffId) throws Exception;
}
