
package com.itbaf.platform.paymenthub.extention.cdag.ws.subscription;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for usuarioSuscripcion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="usuarioSuscripcion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="suscripcion" type="{urn:SendatelSuscripcionesService}suscripcion"/>
 *         &lt;element name="fechaAlta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaBaja" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="palabraClave" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="canalAlta" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "usuarioSuscripcion", namespace = "urn:SendatelSuscripcionesService", propOrder = {
    "suscripcion",
    "fechaAlta",
    "fechaBaja",
    "palabraClave",
    "canalAlta",
    "estado",
    "descargasRestantes"
})
public class UsuarioSuscripcion {

    @XmlElement(required = true)
    protected Suscripcion suscripcion;
    @XmlElement(required = true)
    protected String fechaAlta;
    @XmlElement(required = true)
    protected String fechaBaja;
    @XmlElement(required = true)
    protected String palabraClave;
    protected Integer canalAlta;
    protected int estado;
    protected int descargasRestantes;

    /**
     * Gets the value of the suscripcion property.
     * 
     * @return
     *     possible object is
     *     {@link Suscripcion }
     *     
     */
    public Suscripcion getSuscripcion() {
        return suscripcion;
    }

    /**
     * Sets the value of the suscripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Suscripcion }
     *     
     */
    public void setSuscripcion(Suscripcion value) {
        this.suscripcion = value;
    }

    /**
     * Gets the value of the fechaAlta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaAlta() {
        return fechaAlta;
    }

    /**
     * Sets the value of the fechaAlta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaAlta(String value) {
        this.fechaAlta = value;
    }

    /**
     * Gets the value of the fechaBaja property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaBaja() {
        return fechaBaja;
    }

    /**
     * Sets the value of the fechaBaja property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaBaja(String value) {
        this.fechaBaja = value;
    }

    /**
     * Gets the value of the palabraClave property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPalabraClave() {
        return palabraClave;
    }

    /**
     * Sets the value of the palabraClave property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPalabraClave(String value) {
        this.palabraClave = value;
    }

    /**
     * Gets the value of the canalAlta property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCanalAlta() {
        return canalAlta;
    }

    /**
     * Sets the value of the canalAlta property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCanalAlta(Integer value) {
        this.canalAlta = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     */
    public int getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     */
    public void setEstado(int value) {
        this.estado = value;
    }

    public int getDescargasRestantes() {
        return descargasRestantes;
    }

    public void setDescargasRestantes(int descargasRestantes) {
        this.descargasRestantes = descargasRestantes;
    }

    
}
