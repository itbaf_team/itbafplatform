package com.itbaf.platform.pgp.oauth.api.rest.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.oauth.api.handler.AutenticationServiceProcessorHandler;
import com.itbaf.platform.pgp.oauth.api.handler.ViewAutenticationServiceProcessorHandler;
import com.itbaf.platform.pgp.oauth.api.message.FormRequest;
import com.itbaf.platform.pgp.oauth.api.message.RequestBasicMessage;
import com.itbaf.platform.pgp.oauth.api.message.ResponseBasicMessage;
import java.net.URI;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;

@Path("/signup")
@Singleton
@lombok.extern.log4j.Log4j2
public class SignUpViewAuthenticationRestService {

    private final String profile;
    private final AutenticationServiceProcessorHandler autenticationServiceProcessorHandler;
    private final ViewAutenticationServiceProcessorHandler viewAutenticationServiceProcessorHandler;

    @Inject
    public SignUpViewAuthenticationRestService(@Named("guice.profile") String profile,
            final AutenticationServiceProcessorHandler autenticationServiceProcessorHandler,
            final ViewAutenticationServiceProcessorHandler viewAutenticationServiceProcessorHandler) {
        this.profile = profile;
        this.autenticationServiceProcessorHandler = Validate.notNull(autenticationServiceProcessorHandler, "An AutenticationServiceProcessorHandler class must be provided");
        this.viewAutenticationServiceProcessorHandler = Validate.notNull(viewAutenticationServiceProcessorHandler, "A ViewAutenticationServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/new/{tid}")
    @Produces(MediaType.TEXT_HTML)
    public Response newAccountGet(@Context HttpServletRequest request,
            @PathParam("tid") String tid,
            @QueryParam("token") String udiToken,
            @QueryParam("aniFound") String aniFound,
            @QueryParam("locale") String locale) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-signup.new");
        } catch (Exception ex) {
        }
        log.info("newAccountGet. TID: [" + tid + "] - locale: [" + locale + "] - udiToken: [" + udiToken + "] - aniFound: [" + aniFound + "]");
        if (locale == null) {
            locale = "es";
        }

        tid = autenticationServiceProcessorHandler.getTID(request, tid);
        Map<String, String> prop = null;
        if (tid != null) {
            try {
                prop = autenticationServiceProcessorHandler.validateTID(tid);
            } catch (Exception ex) {
                log.warn("TID Invalido: [" + tid + "]" + ex, ex);
            }
        }
        if (prop != null) {
            Holder<Provider> prov = new Holder();
            User udiUser = null;
            try {
                String urlRedirect = null;
                if ("true".equals(aniFound)) {
                    Holder<String> ur = new Holder();
                    udiUser = autenticationServiceProcessorHandler.getUserFromUdiMessage(udiToken, aniFound, tid, prop, prov, ur);
                    urlRedirect = ur.value;
                } else {
                    urlRedirect = autenticationServiceProcessorHandler.ClaroVrValidation(prop);
                }
                if (urlRedirect != null) {
                    return Response.status(302).location(new URI(urlRedirect)).build();
                }
            } catch (Exception ex) {
            }

            Cookie[] cooks = request.getCookies();
            if (cooks != null) {
                log.info("Cookies: [" + Arrays.toString(cooks) + "]");
                for (Cookie c : cooks) {
                    try {
                        if ("iPlanetDirectoryPro".equals(c.getName())) {
                            // login con cookie de SSO Personal Argentina
                            ResponseBasicMessage rm = autenticationServiceProcessorHandler.validateUserCookieTokenArPersonal(prop, tid, c.getValue(), request, udiUser, prov, request);
                            if (rm != null && rm.redirectUrl != null) {
                                log.info("Login por Cookie - CDAG Argentina");
                                if (rm.uct != null) {
                                    NewCookie cookie = new NewCookie("Ux_PG", rm.uct, "/",
                                            null, "UID_PG_TK", 365 * 24 * 60 * 60, !"dev".equals(profile));
                                    return Response.status(302).location(new URI(rm.redirectUrl)).cookie(cookie).build();
                                }
                                return Response.status(302).location(new URI(rm.redirectUrl)).build();
                            }
                        } else if ("Ux_PG".equals(c.getName())) {
                            // login con cookie de oauth ITBAF
                            ResponseBasicMessage rm = autenticationServiceProcessorHandler.validateUserCookieToken(prop, tid, c.getValue(), request, udiUser);
                            if (rm != null && rm.redirectUrl != null) {
                                log.info("Login por Cookie - ITBAF");
                                return Response.status(302).location(new URI(rm.redirectUrl)).build();
                            }
                        }
                    } catch (Exception ex) {
                        log.error("Error al validar cookie. " + ex, ex);
                    }
                }
            }

            if (udiUser != null) {
                try {
                    ResponseBasicMessage rm = autenticationServiceProcessorHandler.startUdiSession(prop, tid, udiUser, request, prov);
                    if (rm.redirectUrl != null) {
                        log.info("Login por udiUser");
                        return Response.status(302).location(new URI(rm.redirectUrl)).build();
                    } else if (rm.html != null) {
                        return Response.status(Status.OK).entity(rm.html).type(MediaType.TEXT_HTML).build();
                    }
                } catch (Exception ex) {
                    log.error("Error al procesar inicio de sesion para udiUser: [" + udiUser + "]. " + ex, ex);
                }
            }
            // Aca arma el html del login
            String html = viewAutenticationServiceProcessorHandler.getNewAccountHtml(prop, tid);
            html = viewAutenticationServiceProcessorHandler.i18n(locale, prop.get("prvdr"), html);
            return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
        }
        return Response.status(Status.UNAUTHORIZED).build();
    }

    @POST
    @Path("/new/{path}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response newAccountPost(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            @FormParam("tid") final String tid,
            @FormParam("g-recaptcha-response") final String gResponse,
            @FormParam("form_firstname") final String form_firstname,
            @FormParam("form_lastname") final String form_lastname,
            @FormParam("form_email") final String form_email,
            @FormParam("form_password") final String form_password,
            @FormParam("form_rpassword") final String form_rpassword,
            @FormParam("form_countryCode") final String form_countryCode,
            @FormParam("form_birthdate") final String form_birthdate,
            @FormParam("form_gender") final String form_gender,
            @FormParam("form_phoneNumber") final String form_phoneNumber,
            @FormParam("form_phoneCountryCode") final String form_phoneCountryCode,
            @FormParam("locale") String locale,
            @FormParam("email") final String email,
            @FormParam("FirstName") final String firstname,
            @FormParam("LastName") final String lastname,
            @FormParam("passwd") final String password,
            @FormParam("rpasswd") final String rpassword,
            @FormParam("Country") final String countryCode,
            @FormParam("BirthMonth") final String birthMonth,
            @FormParam("BirthDay") final String birthDay,
            @FormParam("BirthYear") final String birthYear,
            @FormParam("Gender") final String gender,
            @FormParam("PhoneNumber") final String phoneNumber) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-signup.new." + path);
        } catch (Exception ex) {
        }
        RequestBasicMessage requestBasicMessage = new RequestBasicMessage();
        requestBasicMessage.gResponse = gResponse;
        requestBasicMessage.tid = tid;
        requestBasicMessage.form = new FormRequest();
        try {
            requestBasicMessage.form.firstname = URLDecoder.decode(form_firstname == null ? firstname : form_firstname, "UTF-8");
            requestBasicMessage.form.lastname = URLDecoder.decode(form_lastname == null ? lastname : form_lastname, "UTF-8");
            requestBasicMessage.form.email = URLDecoder.decode(form_email == null ? email : form_email, "UTF-8");
            requestBasicMessage.form.password = URLDecoder.decode(form_password == null ? password : form_password, "UTF-8");
            requestBasicMessage.form.rpassword = URLDecoder.decode(form_rpassword == null ? rpassword : form_rpassword, "UTF-8");
        } catch (Exception ex) {
        }
        requestBasicMessage.form.countryCode = form_countryCode == null ? countryCode : form_countryCode;
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if (form_birthdate != null) {
                requestBasicMessage.form.birthdate = sdf.parse(form_birthdate);
            } else if (birthMonth != null && birthDay != null && birthYear != null) {
                requestBasicMessage.form.birthdate = sdf.parse(birthYear + "-" + birthMonth + "-" + birthDay);
            } else {
                Calendar c = Calendar.getInstance();
                c.add(Calendar.YEAR, -10);
                requestBasicMessage.form.birthdate = c.getTime();
            }
        } catch (Exception ex) {
            log.error(ex.getMessage() + ". form_birthdate: [" + form_birthdate + "]");
            Calendar c = Calendar.getInstance();
            c.add(Calendar.YEAR, -10);
            requestBasicMessage.form.birthdate = c.getTime();
        }
        requestBasicMessage.form.gender = form_gender == null ? gender : form_gender;
        requestBasicMessage.form.phoneNumber = form_phoneNumber == null ? phoneNumber : form_phoneNumber;
        requestBasicMessage.form.phoneCountryCode = form_phoneCountryCode;
        if (locale == null) {
            locale = "es";
        }
        requestBasicMessage.locale = locale;

        log.info("newAccountPost. RequestBasicMessage: [" + requestBasicMessage + "]");
        requestBasicMessage.tid = autenticationServiceProcessorHandler.getTID(request, requestBasicMessage.tid);
        Map<String, String> prop = null;
        try {
            prop = autenticationServiceProcessorHandler.validateTID(tid);
        } catch (Exception ex) {
            log.warn("TID Invalido: [" + requestBasicMessage + "]" + ex, ex);
        }
        if (prop != null) {
            try {
                if (!"dev".equals(profile) && !autenticationServiceProcessorHandler.googleRecaptchaValidation(gResponse)) {
                    String url = "/signup/new/" + tid + "?locale=" + locale;
                    return Response.status(302).location(new URI(url)).build();
                }

                ResponseBasicMessage rm = null;
                switch (path) {
                    case "create":
                        rm = viewAutenticationServiceProcessorHandler.createNewAccount(prop, requestBasicMessage);
                        break;
                }

                rm.tid = tid;
                rm.locale = requestBasicMessage.locale;
                if (rm.credentials.error != null && rm.credentials.error == 11) {

                    rm = autenticationServiceProcessorHandler.createVerifyCodePage(rm);
                    if (rm.redirectUrl != null) {
                        return Response.status(302).location(new URI(rm.redirectUrl)).build();
                    }
                }

                String url = "/signup/new/" + tid + "?locale=" + locale;
                return Response.status(302).location(new URI(url)).build();
            } catch (Exception ex) {
                log.error("Error al procesar informacion de usuario. " + ex, ex);
            }
        }
        return Response.status(Status.UNAUTHORIZED).build();
    }

}
