/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.personal.sso.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Provider;

/**
 *
 * @author JF
 */
public class UserSSOPersonal {

    public String id;
    public String msisdn;
    public String lastName;
    public String email;
    public Provider provider;
    public String token;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("msisdn", msisdn)
                .add("lastName", lastName)
                .add("email", email)
                .add("token", token)
                .add("provider", provider)
                .omitNullValues().toString();
    }

    public static UserSSOPersonal getUserAttributeCDAG(String data, Provider provider) {
        UserSSOPersonal user = null;
        if (data == null) {
            return user;
        }
        user = new UserSSOPersonal();
        String[] aux = data.replace("\r", "").replace("\n", "").split("userdetails.attribute.name=");
        for (String item : aux) {
            if (item.contains("DatosLineaNroLinea")) {
                item = item.replace("DatosLineaNroLineauserdetails.attribute.value=", "");
                if (item.length() > 9) {
                    user.msisdn = item;
                }
            } else if (item.contains("DatosPagoEmail")) {
                item = item.replace("DatosPagoEmailuserdetails.attribute.value=", "");
                if (item.length() > 4 && item.contains("@")) {
                    user.email = item;
                }
            } else if (item.contains("DatosPagoApellidoResPago") && !item.contains("DATOS NO APORTADOS")) {
                item = item.replace("DatosPagoApellidoResPagouserdetails.attribute.value=", "");
                if (item.length() > 2) {
                    user.lastName = item;
                }
            } else if (item.contains("DatosLineaNroContrato")) {
                item = item.replace("DatosLineaNroContratouserdetails.attribute.value=", "");
                if (item.length() > 2) {
                    user.id = item;
                }
            }
        }
        
        user.provider = provider;
        if (user.msisdn == null) {
            user = null;
        }
        
        return user;
    }
}
