package com.itbaf.platform.paymenthub.extention.mp.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.mp.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.extention.mp.handler.OnDemandProcessorHandler;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.ws.InnerService;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/inner")
@Singleton
@lombok.extern.log4j.Log4j2
public class InnerRestService implements InnerService {

    private final ObjectMapper mapper;
    private final ServiceProcessorHandler serviceProcessorHandler;
    private final OnDemandProcessorHandler onDemandProcessorHandler;

    @Inject
    public InnerRestService(final ObjectMapper mapper,
            final ServiceProcessorHandler serviceProcessorHandler,
            final OnDemandProcessorHandler onDemandProcessorHandler) {
        this.mapper = Validate.notNull(mapper, "A ObjectMapper class must be provided");
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
        this.onDemandProcessorHandler = Validate.notNull(onDemandProcessorHandler, "An OnDemandProcessorHandler class must be provided");
    }

    @POST
    @Path("/unsubscribe")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response unsubscribe(Subscription subscription) {
        //log.info("Unsubscribe [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }

        ResponseMessage rm = new ResponseMessage();
        try {
            rm = serviceProcessorHandler.unsubscribe(subscription);
            return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al cancelar suscripcion. [" + subscription + "]. " + ex, ex);
            rm.message = "Error al cancelar suscripcion. " + ex.getMessage();
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscribe")
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscribe(Subscription subscription) {
        // log.info("Subscribe [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        try {
            rm = serviceProcessorHandler.subscribe(subscription);
            return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al suscribir. [" + subscription + "]. " + ex, ex);
            rm.message = "Error al suscribir. " + ex.getMessage();
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscribe/pin/send")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendPin(Subscription subscription) {
        // log.info("SendPin [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        rm.message = "Metodo no implementado en MercadoPago para el envio de PIN a usuarios.";

        //Validar status en la telco antes de dar de alta
        return Response.status(Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscribe/pin/validate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validatePin(Subscription subscription, @QueryParam("pin") final String pin) {
        // log.info("ValidatePin [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        rm.message = "Metodo no implementado en MercadoPago para la validacion de PIN a usuarios.";

        //Validar status en la telco antes de dar de alta
        return Response.status(Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscription/status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptionStatus(Subscription subscription) {
        //  log.info("SubscriptionStatus [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm = serviceProcessorHandler.getSubscriptionStatus(subscription);
        } catch (Exception ex) {
            rm.message = "Error al procesar status." + ex.getMessage();
            log.error(rm.message, ex);
        }
        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscription/normalizer")
    @Produces(MediaType.APPLICATION_JSON)
    public Response userAccountNormalizer(Subscription subscription) {
        //  log.info("userAccountNormalizer [" + subscription + "]");
        try {
            Thread.currentThread().setName(subscription.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm = serviceProcessorHandler.userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.message = "Error al procesar userAccountNormalizer. " + ex.getMessage();
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR_403;
        }
        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @Override
    @POST
    @Path("/subscription/send/sms")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendSMS(Subscription subscription) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Response charge(OnDemandRegistry or) {
        log.info("Charge [" + or + "]");
        try {
            Thread.currentThread().setName(or.getThread());
        } catch (Exception ex) {
        }
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            rm = onDemandProcessorHandler.charge(or);
        } catch (Exception ex) {
            rm.message = "Error al procesar charge." + ex.getMessage();
            log.error(rm.message, ex);
        }
        return Response.status(Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }
}
