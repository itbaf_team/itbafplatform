/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPATariffRepository;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.TariffService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class TariffServiceTransactional implements TariffService {

    private final JPATariffRepository tariffRepository;
    private final SubscriptionManager subscriptionManager;

    @Inject
    public TariffServiceTransactional(final JPATariffRepository tariffRepository,
            final SubscriptionManager subscriptionManager) {
        this.tariffRepository = Validate.notNull(tariffRepository, "A JPATariffRepository class must be provided");
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
    }

    @Override
    public void saveOrUpdate(Tariff tariff) throws Exception {
        if (tariff.getId() == null) {
            tariffRepository.save(tariff);
        } else {
            tariffRepository.merge(tariff);
        }
        subscriptionManager.updateMemoryCache();
    }

    @Override
    public Tariff findById(Long tariffId) throws Exception {
        if (tariffId == null) {
            return null;
        }

        Object obj = MainCache.memory24Hours().getIfPresent("Tariff_" + tariffId);
        if (obj == null) {
            Tariff t = tariffRepository.findById(tariffId);
            if (t != null) {
                MainCache.memory24Hours().put("Tariff_" + tariffId, t);
                return t;
            }
        }

        return (Tariff) obj;
    }

}
