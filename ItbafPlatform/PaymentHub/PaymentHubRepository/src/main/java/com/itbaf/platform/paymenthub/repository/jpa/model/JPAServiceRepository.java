/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.QService;
import com.itbaf.platform.paymenthub.model.Service;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.querydsl.jpa.impl.JPAQuery;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAServiceRepository extends AbstractJPARepositorySession<Service, Long> {

    @Inject
    public JPAServiceRepository(final Provider<EntityManager> entityManagerProvider) {
        super(Service.class, entityManagerProvider);
    }

    public Service findByName(String name) {
        if (name == null) {
            String error = "El nombre del Service es nulo!!";
            log.error(error);

            throw new RuntimeException(error);
        }

        QService service = QService.service;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        Service s = query.select(service).from(service).
                where(service.name.equalsIgnoreCase(name)).fetchFirst();

        return s;
    }
}
