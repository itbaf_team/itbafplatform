/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.bo.server.model;

import com.google.common.base.MoreObjects;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author JF
 */
public class UserResponse {

    public Long id;
    public String nickname;
    public String avatar;
    public Map<String, String> permissions;
    public Map<String, Object> params = new HashMap();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("nickname", nickname)
                .add("avatar", avatar)
                .add("permissions", permissions)
                .add("params", params)
                .omitNullValues().toString();
    }
}
