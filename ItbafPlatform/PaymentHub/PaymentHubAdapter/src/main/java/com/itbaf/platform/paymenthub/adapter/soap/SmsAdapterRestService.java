package com.itbaf.platform.paymenthub.adapter.soap;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.adapter.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.RcvSmsRequest;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPMessage;
import org.apache.commons.lang3.Validate;

@Path("/sms")
@Singleton
@lombok.extern.log4j.Log4j2
public class SmsAdapterRestService {

    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public SmsAdapterRestService(ServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");

    }

    @POST
    @Path("/{country}/cdag/mo")
    @Consumes("text/xml")
    @Produces("text/xml")
    public Response subscriptionNotification(final @PathParam("country") String countryCode, String soap) {

        log.info("MO de CDAG. countryCode: " + countryCode + " - soap: [" + soap + "]");

        String soapResponse;
        try {
            MessageFactory factory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(soap.getBytes(Charset.forName("UTF-8"))));
            JAXBContext jbc = JAXBContext.newInstance(RcvSmsRequest.class);
            Unmarshaller um = jbc.createUnmarshaller();
            JAXBElement<RcvSmsRequest> element = um.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument(), RcvSmsRequest.class);
            RcvSmsRequest rcvSmsRequest = element.getValue();
            serviceProcessorHandler.processMO(countryCode + ".personal.cdag", rcvSmsRequest);
            soapResponse = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ns2:RcvSmsResponse xmlns:ns2=\"http://ws.sms.qubit.tv/\"><id>" + rcvSmsRequest.getId() + "</id><resultCode>0</resultCode><resultMessage>OK</resultMessage></ns2:RcvSmsResponse></soap:Body></soap:Envelope>";
            return Response.status(Status.OK).entity(soapResponse).type("text/xml").build();
        } catch (Exception ex) {
            log.error("Error al interpretar SOAP. CountryCode [" + countryCode + "] - Subscription Notification: [" + soap + "]. " + ex, ex);
            soapResponse = "<?xml version = '1.0' encoding = 'UTF-8'?><SOAP-ENV:Envelope xmlns:SOAP-ENV = \"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi = \"http://www.w3.org/1999/XMLSchema-instance\" xmlns:xsd = \"http://www.w3.org/1999/XMLSchema\"><SOAP-ENV:Body><SOAP-ENV:Fault><faultcode xsi:type = \"xsd:string\">1000</faultcode><faultstring xsi:type = \"xsd:string\">Error al procesar peticion. [" + ex + "]</faultstring></SOAP-ENV:Fault></SOAP-ENV:Body></SOAP-ENV:Envelope>";

        }

        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(soapResponse).type("text/xml").build();
    }
}
