package com.itbaf.platform.pgp.test.services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.fasterxml.jackson.databind.ObjectMapper;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import com.itbaf.platform.commons.cache.CacheMessage;
import com.itbaf.platform.paymenthub.services.SubscriptionManagerImpl;
import com.itbaf.platform.pgp.test.instance.GuiceInstance;
import com.itbaf.platform.rmq.producer.RabbitMQProducerImpl;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author JF
 */
public class RabbitMQProducerServiceTest {

    private RabbitMQProducerImpl producer;
    private ObjectMapper mapper;

    public RabbitMQProducerServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        mapper = GuiceInstance.getInstance().getInstance(ObjectMapper.class);
        producer = new RabbitMQProducerImpl(getConnectionFactory());
    }
    
    private ConnectionFactory getConnectionFactory() {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("localhost");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("vHostPaymentHub");

        return connectionFactory;
    }

    @After
    public void tearDown() {
    }

    @Test
    @Ignore
    public void Sendmessage() {
        CacheMessage cm = new CacheMessage();
        cm.type = CacheMessage.CacheMessageType.UPDATE;
        try {
            //   assertNotNull(user);
            assertTrue(Boolean.TRUE.equals(producer.messageSender(CHACHE_MEMORY_UPDATE, mapper.writeValueAsString(cm))));
        } catch (Exception ex) {
            System.out.println("No fue posible enviar al Rabbit el mensaje: [" + cm + "]. " + ex);
            ex.printStackTrace();
        }
    }

}
