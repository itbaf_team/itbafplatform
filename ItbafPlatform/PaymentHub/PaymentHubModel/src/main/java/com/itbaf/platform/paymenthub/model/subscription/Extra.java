/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.subscription;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author JF
 */
public class Extra implements Serializable {

    private static final long serialVersionUID = 20180504155413L;
    public String serviceId;
    public String keyword;
    public Subscription.Status firstStatus;
    public Map<Date, Subscription.Status> status = new HashMap();
    public Map<Subscription.Status, Date> statusChanged = new HashMap();
    public Map<String, String> stringData = new HashMap();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("serviceId", serviceId)
                .add("keyword", keyword)
                .add("firstStatus", firstStatus)
                .add("status", status)
                .add("statusChanged", statusChanged)
                .add("stringData", stringData)
                .omitNullValues().toString();
    }
}
