/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons;

import sun.misc.Signal;
import sun.misc.SignalHandler;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class DebugSignalHandler implements SignalHandler {

    private final CommonDaemon commonDaemon;

    public static void listenTo(String name, CommonDaemon commonDaemon) {

        try {
            Signal signal = new Signal(name);
            Signal.handle(signal, new DebugSignalHandler(commonDaemon));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public DebugSignalHandler(CommonDaemon commonDaemon) {
        this.commonDaemon = commonDaemon;
    }

    @Override
    public void handle(Signal signal) {
        log.info("Signal: " + signal);
        try {
            commonDaemon.stop();
            log.info("Servicio finalizado con exito.");
        } catch (Exception ex) {
            log.error("Error al detener el servicio. " + ex, ex);
        }
         System.exit(1);
    }
 
}
