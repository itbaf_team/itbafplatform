/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.persistence.cache;

import com.itbaf.platform.commons.resources.InstrumentedObject;
import java.util.Map;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.CacheException;
import org.redisson.hibernate.RedissonRegionFactory;

/*
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class CustomRedisRegionFactory extends RedissonRegionFactory {

    private static final long serialVersionUID = 20170810093001L;
    public static InstrumentedObject instrumentedObject;

    @Override
    protected void prepareForUse(SessionFactoryOptions settings, @SuppressWarnings("rawtypes") Map properties) throws CacheException {
        properties.put("hibernate.cache.redisson.config", InstrumentedObject.redisConfigRemotePath);
        super.prepareForUse(settings, properties);
        log.info("create CustomRedisRegionFactory instance. ");
    }
}
