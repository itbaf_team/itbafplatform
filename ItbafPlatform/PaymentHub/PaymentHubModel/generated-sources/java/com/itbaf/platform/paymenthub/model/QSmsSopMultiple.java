package com.itbaf.platform.paymenthub.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSmsSopMultiple is a Querydsl query type for SmsSopMultiple
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSmsSopMultiple extends EntityPathBase<SmsSopMultiple> {

    private static final long serialVersionUID = -1329823021L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSmsSopMultiple smsSopMultiple = new QSmsSopMultiple("smsSopMultiple");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final BooleanPath multipleSms = createBoolean("multipleSms");

    protected QSOP sop;

    public QSmsSopMultiple(String variable) {
        this(SmsSopMultiple.class, forVariable(variable), INITS);
    }

    public QSmsSopMultiple(Path<? extends SmsSopMultiple> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSmsSopMultiple(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSmsSopMultiple(PathMetadata metadata, PathInits inits) {
        this(SmsSopMultiple.class, metadata, inits);
    }

    public QSmsSopMultiple(Class<? extends SmsSopMultiple> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sop = inits.isInitialized("sop") ? new QSOP(forProperty("sop"), inits.get("sop")) : null;
    }

    public QSOP sop() {
        if (sop == null) {
            sop = new QSOP(forProperty("sop"));
        }
        return sop;
    }

}

