package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Source {

    private String id;
    private String name;
    private Type type;

    public enum Type {
        collector,
        operator,
        admin,
        bpp
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("type", type)
                .omitNullValues().toString();
    }

}
