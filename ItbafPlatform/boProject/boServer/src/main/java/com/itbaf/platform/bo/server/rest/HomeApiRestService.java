package com.itbaf.platform.bo.server.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.bo.server.handler.ApiHomeServiceProcessorHandler;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;
import javax.ws.rs.GET;
import com.itbaf.platform.bo.server.filter.OauthPermission;

@Path("/api/home")
@Singleton
@lombok.extern.log4j.Log4j2
public class HomeApiRestService extends CommonRestService {

    private final SubscriptionManager subscriptionManager;
    private final ApiHomeServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public HomeApiRestService(final SubscriptionManager subscriptionManager,
            final ApiHomeServiceProcessorHandler serviceProcessorHandler) {
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "An ApiHomeServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/token")
    @OauthPermission("api-home-get-token")
    // @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Response getToken(@Context HttpServletRequest request) {
        System.out.println("algo");
        return Response.status(Response.Status.OK).entity("OKA").type(MediaType.TEXT_HTML).cacheControl(getCacheControl()).build();
    }

    @GET
    @Path("/tokens")
    // @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Response getTokens(@Context HttpServletRequest request) {
        System.out.println("algos");
        return Response.status(Response.Status.OK).entity("OKAs").type(MediaType.TEXT_HTML).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/token")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getToken(@Context HttpServletRequest request,
            @FormParam("grant_type") final RequestTokenMessage.GrantType grantType,
            @FormParam("access_token") final String accessToken,
            @FormParam("code_verifier") final String codeVerifier) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.account.token");
        } catch (Exception ex) {
        }
        RequestTokenMessage tm = new RequestTokenMessage();
        tm.grantType = grantType;
        tm.accessToken = accessToken;
        tm.codeVerifier = codeVerifier;

        log.info("getToken. RequestTokenMessage: [" + tm + "]");

        PGMessageResponse rm = new PGMessageResponse();
        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = serviceProcessorHandler.authenticateUser(client, tm.accessToken, null);
            if (user == null) {
                rm.data.message = "Token de usuario invalido. Forzar el inicio de sesion de usuario. ";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_user_token;
            } else {
                /*   switch (grantType) {
                   case authorization_code:
                        rm = serviceProcessorHandler.getUserAccountToken(tm, client, user);
                        break;
                    default:
                        rm.data.message = "Hay un error en los parametros. ";
                        rm.meta.code = 400;
                        rm.meta.status = Meta.Status.invalid_grant;
                }*/
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener informacion del perfil. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

}
