/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.messaging.queue.wallet.CurrencyRequest;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQMessageProcessorImpl implements RabbitMQMessageProcessor {

    private final ObjectMapper mapper;
    private final PGMessageProcessor pgMessageProcessor;

    @Inject
    public RabbitMQMessageProcessorImpl(final ObjectMapper mapper,
            final PGMessageProcessor pgMessageProcessor) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.pgMessageProcessor = Validate.notNull(pgMessageProcessor, "A PGMessageProcessor class must be provided");

    }

    @Override
    public Boolean processMessage(String message) {

        CurrencyRequest cr = null;
        try {
            cr = mapper.readValue(message, CurrencyRequest.class);
            if (cr.transactionId.startsWith("PH_S_")) {
                return pgMessageProcessor.subscriptionProcess(cr);
            } else if (cr.transactionId.startsWith("PH_UNS_")) {
                return pgMessageProcessor.unsubscriptionProcess(cr);
            }
            return pgMessageProcessor.billingProcess(cr);
        } catch (Exception ex) {
            log.error("Error procesando mensaje [" + message + "]. " + ex, ex);
        }
        return null;
    }

}
