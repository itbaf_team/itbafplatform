/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adapter.modules;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.itbaf.platform.paymenthub.adapter.consumer.RabbitMQAdnetworkMessageProcessorFactory;
import com.itbaf.platform.paymenthub.adapter.consumer.RabbitMQMessageProcessorImpl;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;

/**
 *
 * @author javier
 */
public class GuiceConsumersConfigModule extends AbstractModule {

    @Override
    public void configure() {

        //Install
        install(new FactoryModuleBuilder()
                .implement(RabbitMQMessageProcessor.class, RabbitMQMessageProcessorImpl.class)
                .build(RabbitMQAdnetworkMessageProcessorFactory.class));
    }

}
