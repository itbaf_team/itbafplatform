
import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { PermissionsService } from './permissions.service';

import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { PGMessageRequest, PGMessageResponse, BoHttpClient } from '../utils/bo-http-client';



let counter = 0;

@Injectable()
export class UserService extends BoHttpClient {

    private users = {
      nick: { name: 'Nick Jones', picture: 'assets/images/nick.png' },
      eva: { name: 'Eva Moor', picture: 'assets/images/eva.png' },
      jack: { name: 'Jack Williams', picture: 'assets/images/jack.png' },
      lee: { name: 'Lee Wong', picture: 'assets/images/lee.png' },
      alan: { name: 'Alan Thompson', picture: 'assets/images/alan.png' },
      kate: { name: 'Kate Martinez', picture: 'assets/images/kate.png' },
    };

    private userArray: any[];

    constructor(private http: HttpClient, private permissionsService: PermissionsService) {
        super();
    }

    getUsers(): Observable<any> {
      return observableOf(this.users);
    }

    getUserArray(): Observable<any[]> {
      return observableOf(this.userArray);
    }

    getUser(): Observable<PGMessageResponse> {
      let pgMessage  = new PGMessageRequest();
      pgMessage.description="getUserInformation";
      return this.http.post<PGMessageResponse>(environment.apiUrl + "/bo/server/rest/api/user", pgMessage, this.httpOptions)
          .pipe(tap((response: PGMessageResponse) => this.userResponseProcess(response)),
            catchError(this.handleError<PGMessageResponse>('getUserInformation'))
          );
    }
    
    userResponseProcess(response){
        this.permissionsService.setPermissions(response.data.content.permissions);
    }
}
