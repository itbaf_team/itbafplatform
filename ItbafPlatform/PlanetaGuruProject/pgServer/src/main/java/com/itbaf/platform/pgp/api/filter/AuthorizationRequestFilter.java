/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.filter;

import com.google.common.net.HttpHeaders;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Client;
import com.itbaf.platform.pgp.services.repository.GenericPropertyService;
import com.itbaf.platform.pgp.services.repository.UserService;
import java.io.IOException;
import javax.annotation.Nullable;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itbaf.platform.pgp.api.handler.ApiAccountServiceProcessorHandler;

/**
 *
 * @author jordonez
 */
@Singleton
@lombok.extern.log4j.Log4j2
public class AuthorizationRequestFilter implements Filter {

    private final String appName;
    private final UserService userService;
    private final GenericPropertyService genericPropertyService;
    private final ApiAccountServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public AuthorizationRequestFilter(@Named("app.name") String appName,
            final UserService userService,
            final GenericPropertyService genericPropertyService,
            final ApiAccountServiceProcessorHandler serviceProcessorHandler) {
        this.appName = appName;
        this.userService = userService;
        this.genericPropertyService = genericPropertyService;
        this.serviceProcessorHandler = serviceProcessorHandler;
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {

        @Nullable
        String token = extractAccessTokenFromHeader(request);
        if (token == null) {
            setReAuthenticate401((HttpServletResponse) response);
            return;
        }

        @Nullable
        User client = authenticate(token);
        if (client == null) {
            setReAuthenticate401((HttpServletResponse) response);
            return;
        }

        if (!authorize(client)) {
            setForbidden403((HttpServletResponse) response);
            return;
        }

        @Nullable
        String tokenUser = extractAccessUserTokenFromHeader(request);
        
        if (tokenUser != null) {
            User user = authenticateUser(tokenUser, client);
            if (user == null) {
                setReAuthenticate401((HttpServletResponse) response);
                return;
            }
            request.setAttribute(CommonAttribute.U_PRINCIPAL, user);
        }

        request.setAttribute(CommonAttribute.PRINCIPAL, client);
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

    /**
     * Extracts the token from the Authorization header, removing the Bearer
     * prefix
     *
     * @param request The incoming request
     * @return the token or null if not present
     */
    private @Nullable
    String extractAccessTokenFromHeader(ServletRequest request) {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        @Nullable
        String authorizationHeader = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")) {
            String[] tokenSplit = authorizationHeader.split("Bearer\\s+");
            if (tokenSplit.length != 2) {
                log.warn("Incoming token in Authorization header is not a Bearer token");
                return null;
            }
            return tokenSplit[1];
        }
        return null;
    }

    private @Nullable
    String extractAccessUserTokenFromHeader(ServletRequest request) {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        @Nullable
        String authorizationHeader = httpRequest.getHeader("x-user-token");

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")) {
            String[] tokenSplit = authorizationHeader.split("Bearer\\s+");
            if (tokenSplit.length != 2) {
                log.warn("Incoming token in Authorization header is not a Bearer user token");
                return null;
            }
            return tokenSplit[1];
        }
        return null;
    }

    private void setReAuthenticate401(HttpServletResponse response) throws IOException {
        String msg = String.format("Bearer realm=\"%s\"", appName);
        response.setHeader(HttpHeaders.WWW_AUTHENTICATE, msg);
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }

    private void setForbidden403(HttpServletResponse response) throws IOException {
        String msg = String.format("Bearer realm=\"%s\"", appName);
        response.setHeader(HttpHeaders.WWW_AUTHENTICATE, msg);
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
    }

    /**
     * Evalua si el token es valido y retorna el User (Client) activo
     */
    private User authenticate(String token) {
        try {
            String clientId = Client.validateToken(genericPropertyService.getValueByKey("client.sign.secret"), token).getIssuer();
            User client = userService.findById(Long.parseLong(clientId));
            if (Status.ENABLE.equals(client.getStatus().ENABLE)) {
                return client;
            }
        } catch (Exception ex) {
            log.error("Error al autenticar Client con token: [" + token + "]. " + ex);
        }

        return null;
    }

    /**
     * Evalua si el token es valido y retorna el User activo
     */
    private User authenticateUser(String token, User client) {
        try {
            User user = serviceProcessorHandler.authenticateUser(client, token, null);
            return user;
        } catch (Exception ex) {
            log.error("Error al autenticar Client con token: [" + token + "]. " + ex);
        }

        return null;
    }

    /**
     * Evalua si tiene los permisos para ejecutar este request
     */
    private boolean authorize(User client) {
        return true;
    }

}
