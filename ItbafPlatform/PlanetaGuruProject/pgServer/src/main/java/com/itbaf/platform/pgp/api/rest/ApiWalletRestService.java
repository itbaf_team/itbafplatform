package com.itbaf.platform.pgp.api.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageRequest;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.pgp.api.handler.ApiWalletServiceProcessorHandler;
import com.itbaf.platform.pgp.api.handler.SubscriptionViewServiceProcessorHandler;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.wallet.PaymentMethod;
import io.jsonwebtoken.impl.DefaultClaims;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

@Path("/api/wallet")
@Singleton
@lombok.extern.log4j.Log4j2
public class ApiWalletRestService extends CommonRestService {

    private final InstrumentedObject instrumentedObject;
    private final ApiWalletServiceProcessorHandler apiWalletServiceProcessorHandler;
    private final SubscriptionViewServiceProcessorHandler subscriptionViewServiceProcessorHandler;

    @Inject
    public ApiWalletRestService(final InstrumentedObject instrumentedObject,
            final ApiWalletServiceProcessorHandler apiWalletServiceProcessorHandler,
            final SubscriptionViewServiceProcessorHandler subscriptionViewServiceProcessorHandler) {
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.apiWalletServiceProcessorHandler = Validate.notNull(apiWalletServiceProcessorHandler, "An ApiWalletServiceProcessorHandler class must be provided");
        this.subscriptionViewServiceProcessorHandler = Validate.notNull(subscriptionViewServiceProcessorHandler, "A SubscriptionViewServiceProcessorHandler class must be provided");
    }

    @POST
    @Path("/funds")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postFunds(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.wallet.funds.post");
        } catch (Exception ex) {
        }
        log.info("postFunds. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();
        RLock lock = null;
        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = apiWalletServiceProcessorHandler.authenticateUser(client, messageRequest.userToken, null);

            if (user == null) {
                rm.data.message = "Token de usuario invalido. Forzar el inicio de sesion de usuario. ";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_user_token;
            } else {
                lock = instrumentedObject.lockObject("wallet_" + user.getId(), 60);
                Map<String, BigDecimal> credits = apiWalletServiceProcessorHandler.getWalletFunds(client, user);
                rm.meta.code = 200;
                rm.meta.status = Meta.Status.ok;
                rm.data.object = credits;
                instrumentedObject.unLockObject(lock);
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener los creditos. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            instrumentedObject.unLockObject(lock);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @PUT
    @Path("/funds")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putFunds(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.wallet.funds.put");
        } catch (Exception ex) {
        }
        log.info("putFunds. PGMessageRequest: [" + messageRequest + "]");
        RLock lock = null;
        PGMessageResponse rm = new PGMessageResponse();
        if (messageRequest.transactionId == null || messageRequest.credit == null
                || messageRequest.credit.currency == null || messageRequest.description == null
                || messageRequest.credit.amount == null || messageRequest.credit.amount.signum() != 1) {
            rm.meta.code = 400;
            rm.meta.status = Meta.Status.invalid_request;
            rm.data.object = "Alguno de los parametros no es valido.";
        } else {
            try {
                User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
                User user = apiWalletServiceProcessorHandler.authenticateUser(client, messageRequest.userToken, null);

                if (user == null) {
                    rm.data.message = "Token de usuario invalido. Forzar el inicio de sesion de usuario. ";
                    rm.meta.code = 400;
                    rm.meta.status = Meta.Status.invalid_user_token;
                } else {
                    lock = instrumentedObject.lockObject("wallet_" + user.getId(), 60);
                    Boolean result = apiWalletServiceProcessorHandler.putWalletFunds(client, user, messageRequest);
                    if (result) {
                        rm.meta.code = 200;
                        rm.meta.status = Meta.Status.ok;
                        rm.data.object = result;
                    } else {
                        rm.meta.code = 400;
                        rm.meta.status = Meta.Status.invalid_request;
                        rm.data.object = result;
                    }
                    instrumentedObject.unLockObject(lock);
                }
            } catch (Exception ex) {
                rm.data.message = "Error al agregar creditos. " + ex;
                rm.meta.code = 500;
                rm.meta.status = Meta.Status.error_internal;
                log.error(rm.data.message, ex);
                instrumentedObject.unLockObject(lock);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
            }
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @PUT
    @Path("/funds/user")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putUserFunds(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.wallet.funds.user.put");
        } catch (Exception ex) {
        }
        log.info("putFunds.User. PGMessageRequest: [" + messageRequest + "]");
        RLock lock = null;
        PGMessageResponse rm = new PGMessageResponse();
        if (messageRequest.transactionId == null || messageRequest.credit == null
                || messageRequest.credit.currency == null || messageRequest.description == null
                || messageRequest.credit.amount == null || messageRequest.credit.amount.signum() != 1 
                || messageRequest.userToken == null) {
            rm.meta.code = 400;
            rm.meta.status = Meta.Status.invalid_request;
            rm.data.object = "Alguno de los parametros no es valido.";
        } else {
            try {
                User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
                User user = apiWalletServiceProcessorHandler.getUserById(messageRequest.userToken);

                if (user == null) {
                    rm.data.message = "Token de usuario invalido. Forzar el inicio de sesion de usuario. ";
                    rm.meta.code = 400;
                    rm.meta.status = Meta.Status.invalid_user_token;
                } else {
                    lock = instrumentedObject.lockObject("wallet_" + user.getId(), 60);
                    Boolean result = apiWalletServiceProcessorHandler.putWalletFunds(client, user, messageRequest);
                    if (result) {
                        rm.meta.code = 200;
                        rm.meta.status = Meta.Status.ok;
                        rm.data.object = result;
                    } else {
                        rm.meta.code = 400;
                        rm.meta.status = Meta.Status.invalid_request;
                        rm.data.object = result;
                    }
                    instrumentedObject.unLockObject(lock);
                }
            } catch (Exception ex) {
                rm.data.message = "Error al agregar creditos. " + ex;
                rm.meta.code = 500;
                rm.meta.status = Meta.Status.error_internal;
                log.error(rm.data.message, ex);
                instrumentedObject.unLockObject(lock);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
            }
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @DELETE
    @Path("/funds")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteFunds(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.wallet.funds.delete");
        } catch (Exception ex) {
        }
        log.info("deleteFunds. PGMessageRequest: [" + messageRequest + "]");
        RLock lock = null;
        PGMessageResponse rm = new PGMessageResponse();
        if (messageRequest.transactionId == null || messageRequest.credit == null
                || messageRequest.credit.currency == null || messageRequest.description == null
                || messageRequest.credit.amount == null || messageRequest.credit.amount.compareTo(BigDecimal.ZERO) == -1) {
            rm.meta.code = 400;
            rm.meta.status = Meta.Status.invalid_request;
            rm.data.object = "Alguno de los parametros no es valido.";
        } else {
            try {
                User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
                User user = apiWalletServiceProcessorHandler.authenticateUser(client, messageRequest.userToken, null);

                if (user == null) {
                    rm.data.message = "Token de usuario invalido. Forzar el inicio de sesion de usuario. ";
                    rm.meta.code = 400;
                    rm.meta.status = Meta.Status.invalid_user_token;
                } else {
                    lock = instrumentedObject.lockObject("wallet_" + user.getId(), 60);
                    Boolean result = apiWalletServiceProcessorHandler.removeWalletFunds(client, user, messageRequest);
                    if (result) {
                        rm.meta.code = 200;
                        rm.meta.status = Meta.Status.ok;
                        rm.data.object = result;
                    } else {
                        rm.meta.code = 400;
                        rm.meta.status = Meta.Status.invalid_request;
                        rm.data.object = result;
                    }
                    instrumentedObject.unLockObject(lock);
                }
            } catch (Exception ex) {
                rm.data.message = "Error al eliminar creditos. " + ex;
                rm.meta.code = 500;
                rm.meta.status = Meta.Status.error_internal;
                log.error(rm.data.message, ex);
                instrumentedObject.unLockObject(lock);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
            }
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/payment/methods")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postMethods(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.wallet.payment.methods");
        } catch (Exception ex) {
        }
        log.info("postMethods. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            DefaultClaims defaultClaims = new DefaultClaims();
            User user = apiWalletServiceProcessorHandler.authenticateUser(client, messageRequest.userToken, defaultClaims);

            if (user == null) {
                rm.data.message = "Token de usuario invalido. Forzar el inicio de sesion de usuario. ";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_user_token;
            } else {
                Set<PaymentMethod> pl = apiWalletServiceProcessorHandler.getPaymentList(client, user, defaultClaims, messageRequest);
                rm.meta.code = 200;
                rm.meta.status = Meta.Status.ok;
                rm.data.object = pl;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener los metodos de pago. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/payment/methods/outer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postMethodsOuter(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.wallet.payment.methods.outer");
        } catch (Exception ex) {
        }
        log.info("postMethodsOuter. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            Set<PaymentMethod> pl = apiWalletServiceProcessorHandler.getOuterPaymentList(client, messageRequest);
            rm.meta.code = 200;
            rm.meta.status = Meta.Status.ok;
            rm.data.object = pl;
        } catch (Exception ex) {
            rm.data.message = "Error al obtener los metodos de pago sin registro. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/payment/subscriptions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postSubscriptions(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.wallet.payment.subscriptions");
        } catch (Exception ex) {
        }
        log.info("postSubscriptions. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            DefaultClaims defaultClaims = new DefaultClaims();
            User user = apiWalletServiceProcessorHandler.authenticateUser(client, messageRequest.userToken, defaultClaims);

            if (user == null) {
                rm.data.message = "Token de usuario invalido. Forzar el inicio de sesion de usuario. ";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_user_token;
            } else {
                List<Subscription> sl = subscriptionViewServiceProcessorHandler.getActiveSubscriptions(client, user);
                rm.meta.code = 200;
                rm.meta.status = Meta.Status.ok;
                rm.data.object = sl;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener suscripciones activas. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/payment/subscriptions/status")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postSubscriptionsStatus(@Context HttpServletRequest request,
            PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.wallet.payment.subscriptions.status");
        } catch (Exception ex) {
        }
        log.info("postSubscriptionsStatus. PGMessageRequest: [" + messageRequest + "]");

        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            DefaultClaims defaultClaims = new DefaultClaims();
            User user = apiWalletServiceProcessorHandler.authenticateUser(client, messageRequest.userToken, defaultClaims);
            if (user == null) {
                rm.data.message = "Invalid token. Start user login. ";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_user_token;
            } else {
                rm.data.object = subscriptionViewServiceProcessorHandler.hasActiveSubscriptions(client, user);
                rm.meta.code = 200;
                rm.meta.status = Meta.Status.ok;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener suscripciones activas. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }
}
