import { NgModule } from '@angular/core';

import { ThemeModule } from '../../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToasterModule } from 'angular2-toaster';

import { SearchComponent } from './search.component';
import { ButtonSearchComponent } from './button-search/button-search.component';
import { TableSearchComponent } from './table-search/table-search.component';
import { SubscriptionService } from '../../../@core/data/subscription.service';
import { ModalComponent } from './table-search/modal/modal.component';

const components = [
  SearchComponent,
  ButtonSearchComponent,
  TableSearchComponent,
  ModalComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    Ng2SmartTableModule,
    ToasterModule.forRoot(),
  ],
  exports: [
    ...components,
  ],
  declarations: [
    ...components,
  ],
  entryComponents: [
    ModalComponent,
  ],
  providers: [SubscriptionService,],
})
export class SearchModule { }
