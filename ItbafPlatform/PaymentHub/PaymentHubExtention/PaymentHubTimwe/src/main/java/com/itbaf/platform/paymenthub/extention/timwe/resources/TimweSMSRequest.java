/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.resources;

/**
 *
 * @author JF
 */
public class TimweSMSRequest {

    public enum Context {
        STATELESS, SUBSCRIPTION, RENEW, VOTING
    }

    public enum Priority {
        LOW, NORMAL, HIGH
    }

    public Long productId;
    public Long pricepointId;
    public String mcc;
    public String mnc;
    public String text;
    public String msisdn;
    public String largeAccount;
    public Priority priority;
    public Context context;

}
