/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events.modules;

import com.google.inject.multibindings.Multibinder;
import com.itbaf.platform.dw.events.processor.StatsProcessor;
import com.itbaf.platform.dw.events.processor.SubscriptionBillingStatsProcessor;
import com.itbaf.platform.dw.events.processor.SubscriptionRegistryStatsProcessor;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.modules.GuiceDefaultConfigModule;
import com.itbaf.platform.dw.events.processor.AdnetworkNotifierSimpleStatsProcessor;
import com.itbaf.platform.dw.events.processor.OauthUserSubscriptionStatsProcessor;
import com.itbaf.platform.dw.events.processor.PGJContentStatsProcessor;
import com.itbaf.platform.dw.events.processor.QueryStatsProcessor;
import com.itbaf.platform.pgp.repository.module.GuiceRepositoryConfigModule;
import com.itbaf.platform.pgp.services.module.PGGuiceServiceConfigModule;
import com.itbaf.platform.rmq.module.GuiceRabbitMQConfigModule;
import java.util.Arrays;
import java.util.Map;

/**
 *
 * @author jordonez
 */
public class GuiceConfigModule extends GuiceDefaultConfigModule {

    public GuiceConfigModule(final Map<String, String> args, CommonDaemon commonDaemon) {
        super(args, commonDaemon);
    }

    @Override
    protected void configure() {
        super.configure();

        //Binds        
        final Multibinder<StatsProcessor> multi = Multibinder.newSetBinder(binder(), StatsProcessor.class);
        multi.addBinding().to(SubscriptionRegistryStatsProcessor.class).asEagerSingleton();
        multi.addBinding().to(SubscriptionBillingStatsProcessor.class).asEagerSingleton();
        multi.addBinding().to(AdnetworkNotifierSimpleStatsProcessor.class).asEagerSingleton();
        multi.addBinding().to(PGJContentStatsProcessor.class).asEagerSingleton();
        multi.addBinding().to(QueryStatsProcessor.class).asEagerSingleton();
        multi.addBinding().to(OauthUserSubscriptionStatsProcessor.class).asEagerSingleton();

        //Modulos de aplicaciones
        install(new GuiceRepositoryConfigModule());
        install(new PGGuiceServiceConfigModule());
        install(new GuiceConsumersConfigModule());
        install(new GuiceRabbitMQConfigModule());
        install(new GuiceRestServicesConfigModule());

        log.info("Configuracion iniciada. " + Arrays.toString(args.entrySet().toArray()));
    }

}
