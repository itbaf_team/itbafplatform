/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.log4j2.appender.cloud;

import com.google.cloud.logging.LoggingOptions;
import com.itbaf.platform.commons.log4j2.appender.CloudAppender;
import java.io.Serializable;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;

/**
 *
 * @author JF
 */
public class PatternConsoleCloudAppender extends CloudAppender {

    public PatternConsoleCloudAppender(String name, Filter filter, Layout<? extends Serializable> layout, LoggingOptions options) {
        super(name, filter, layout, options);
    }

    public PatternConsoleCloudAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions, LoggingOptions options) {
        super(name, filter, layout, ignoreExceptions, options);
    }

    @Override
    public void append(LogEvent event) {
        try {
            System.out.print(providerId + new String(getLayout().toByteArray(event)));
        } catch (Exception ex) {
            System.out.print(providerId + event);
        }
    }

}
