/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.filter;

import com.itbaf.platform.commons.model.LogMessage;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;
import java.io.ByteArrayInputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author JF
 */
public class ClientRequestFilter extends ClientFilter {

    private static final Logger log = LogManager.getLogger(ClientRequestFilter.class);

    @Override
    public ClientResponse handle(ClientRequest cr) {

        String url = cr.getURI().toString();
        String entity = "";
        Object ent = cr.getEntity();
        if (ent != null) {
            entity = " - Entity: [" + ent + "]";
        }

        log.info(new LogMessage("Request a: [" + url + "]" + entity, cr, null, null));

        Long time = System.currentTimeMillis();
        ClientResponse resp = getNext().handle(cr);
        String result = resp.getEntity(String.class);
        time = System.currentTimeMillis() - time;
        if (result == null) {
            result = "";
        }
        
        if (!(resp.getStatus() + "").startsWith("2")) {
            log.warn(new LogMessage("Request [" + url + "]" + entity + ". Codigo: [" + resp.getStatus() + "] - Response: [" + result + "]", cr, resp, time));
        } else {
            log.info(new LogMessage("Request [" + url + "]" + entity + ". Codigo: [" + resp.getStatus() + "] - Response: [" + result + "]", cr, resp, time));
        }

        resp.setEntityInputStream(new ByteArrayInputStream(result.getBytes()));
        return resp;
    }

}
