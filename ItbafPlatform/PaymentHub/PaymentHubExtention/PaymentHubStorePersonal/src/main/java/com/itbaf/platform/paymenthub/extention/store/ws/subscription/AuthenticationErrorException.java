package com.itbaf.platform.paymenthub.extention.store.ws.subscription;

public class AuthenticationErrorException extends PersonalArProviderServiceException {

    private static final long serialVersionUID = 8266354689895193753L;

    public AuthenticationErrorException(StatusResponse response) {
        super(response);
    }
}
