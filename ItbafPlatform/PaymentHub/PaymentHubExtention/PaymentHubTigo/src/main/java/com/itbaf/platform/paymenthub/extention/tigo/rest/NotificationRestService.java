package com.itbaf.platform.paymenthub.extention.tigo.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.extention.tigo.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.extention.tigo.model.TigoNotification;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;


@Path("/notification")
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationRestService {

    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public NotificationRestService(ServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");

    }

    @POST
    @Path("/{country}/tigo/{type}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response tigoSubscriptionNotification(final @PathParam("country") String countryCode,
            final @PathParam("type") TigoNotification.Type type,
            final TigoNotification notification) {

        notification.type = type;
        log.info("Notificacion de Tigo. CountryCode [" + countryCode + "] - Type: [" + type + "] -Notification: [" + notification + "]");
        serviceProcessorHandler.processNotification(countryCode, notification);
        return Response.status(Status.OK).entity(new TigoNotification(0)).type(MediaType.APPLICATION_JSON).build();
    }

}
