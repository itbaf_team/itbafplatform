package com.itbaf.platform.paymenthub.extention.cdag.ws.subscription;

public class InsufficientPermissionsException extends PersonalArProviderServiceException {

    private static final long serialVersionUID = -1653750512265603794L;

    public InsufficientPermissionsException(StatusResponse response) {
        super(response);
    }
}
