/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties.session;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class Token implements Serializable {

    private static final long serialVersionUID = 20181207172702L;

    private String token;
    private String userAgent;
    private String ip;
    private Date createdDate;//Fecha creacion del token

    public Token() {
        createdDate = new Date();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("token", token)
                .add("createdDate", createdDate)
                .add("ip", ip)
                .add("userAgent", userAgent)
                .omitNullValues().toString();
    }
}
