package com.itbaf.platform.paymenthub.extention.tiaxa.model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

/**
 *
 * @author www.javadb.com
 */
public class HeaderHandlerResolver implements HandlerResolver {

    private final String username;
    private final String password;

    public HeaderHandlerResolver(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public List<Handler> getHandlerChain(PortInfo portInfo) {

        List<Handler> handlerChain = new ArrayList<Handler>();
        HeaderHandler hh = new HeaderHandler(new UsernameToken(username, password));
        handlerChain.add(hh);

        return handlerChain;
    }
}
