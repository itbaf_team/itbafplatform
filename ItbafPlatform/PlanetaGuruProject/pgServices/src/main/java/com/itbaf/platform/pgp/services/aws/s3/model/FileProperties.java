/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.aws.s3.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class FileProperties {

    public String name;
    public String size;
    public String type;
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("size", size)
                .add("type", type)
                .omitNullValues().toString();
    }
}
