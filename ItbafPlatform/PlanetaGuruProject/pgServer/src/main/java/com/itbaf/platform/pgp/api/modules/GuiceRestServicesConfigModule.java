package com.itbaf.platform.pgp.api.modules;

import com.google.inject.Singleton;
import com.itbaf.platform.commons.filter.RequestFilter;
import com.itbaf.platform.pgp.api.filter.AuthorizationRequestFilter;
import com.itbaf.platform.pgp.api.rest.ApiAccountRestService;
import com.itbaf.platform.pgp.api.rest.ApiProfileRestService;
import com.itbaf.platform.pgp.api.rest.ApiWalletRestService;
import com.itbaf.platform.pgp.api.rest.ApiTransactionRestService;
import com.itbaf.platform.pgp.api.rest.ContentNotificationRestService;
import com.itbaf.platform.pgp.api.rest.view.AccountViewRestService;
import com.itbaf.platform.pgp.api.rest.view.FriendViewRestService;
import com.itbaf.platform.pgp.api.rest.view.ProfileViewRestService;
import com.itbaf.platform.pgp.api.rest.view.SecurityViewRestService;
import com.itbaf.platform.pgp.api.rest.view.StoreViewRestService;
import com.itbaf.platform.pgp.api.rest.view.SessionViewRestService;
import com.itbaf.platform.pgp.api.rest.view.SubscriptionViewRestService;
import com.itbaf.platform.pgp.api.rest.view.TransactionViewRestService;
import com.itbaf.platform.pgp.commons.rest.ManagerRestServiceImpl;
import com.itbaf.platform.pgp.services.filter.ViewRequestFilter;
import com.itbaf.platform.pgp.services.rest.FileRestService;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import java.util.HashMap;

public class GuiceRestServicesConfigModule extends JerseyServletModule {

    @Override
    protected void configureServlets() {
        super.configureServlets();

        HashMap<String, String> options = new HashMap();
        options.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

        //Filters
        filter("/*").through(RequestFilter.class);
        filter("/rest/api/*").through(AuthorizationRequestFilter.class);
        filter("/account/*").through(ViewRequestFilter.class);

        //Servlets
        serve("/rest/*").with(GuiceContainer.class, options);
        serve("/file/*").with(GuiceContainer.class, options);
        serve("/account/*").with(GuiceContainer.class, options);
        //serve("/account/profile/*").with(GuiceContainer.class, options);

        bind(FileRestService.class).in(Singleton.class);
        bind(ManagerRestServiceImpl.class).in(Singleton.class);
        bind(ApiProfileRestService.class).in(Singleton.class);
        bind(ApiWalletRestService.class).in(Singleton.class);
        bind(ApiAccountRestService.class).in(Singleton.class);
        bind(ApiTransactionRestService.class).in(Singleton.class);
        bind(AccountViewRestService.class).in(Singleton.class);
        bind(ProfileViewRestService.class).in(Singleton.class);
        bind(SecurityViewRestService.class).in(Singleton.class);
        bind(TransactionViewRestService.class).in(Singleton.class);
        bind(SubscriptionViewRestService.class).in(Singleton.class);
        bind(StoreViewRestService.class).in(Singleton.class);
        bind(SessionViewRestService.class).in(Singleton.class);
        bind(FriendViewRestService.class).in(Singleton.class);
        bind(ContentNotificationRestService.class).in(Singleton.class);
    }

}
