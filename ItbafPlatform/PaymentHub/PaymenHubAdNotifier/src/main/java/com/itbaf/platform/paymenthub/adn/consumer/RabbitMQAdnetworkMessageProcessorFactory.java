/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adn.consumer;

import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessorFactory;
import com.itbaf.platform.paymenthub.adn.processor.AdnetworkProcessor;

/**
 *
 * @author javier
 * @param <M>
 */
public interface RabbitMQAdnetworkMessageProcessorFactory extends RabbitMQMessageProcessorFactory <AdnetworkProcessor>{

}
