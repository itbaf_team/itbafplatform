/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.wallet;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.BaseEntity;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.persistence.util.HibernateEquality;
import com.itbaf.platform.pgp.model.oauth.User;
import com.querydsl.core.annotations.Config;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author JF
 */
@Entity(name = "W_TRANSACTION")
@Table(name = "W_TRANSACTION")
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class WalletTransaction extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 20171226115501L;

    public enum Type {
        ADD, WITHDRAW
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "TYPE", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Type type;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CHARGED_DATE")
    private Date chargedDate;
    @Temporal(TemporalType.DATE)
    @Column(name = "CHARGED_DATE_SEARCH")
    private Date chargedDateSearch;
    @Column(name = "TRANSACTION_ID", nullable = false)
    private String tid;
    @OneToOne(optional = false)
    @JoinColumn(name = "USER_ID")
    private User user;
    @OneToOne(optional = true)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;
    @OneToOne(optional = true)
    @JoinColumn(name = "CLIENT_ID")
    private User client;
    @OneToOne(optional = true)
    @JoinColumn(name = "COUNTRY_ID")
    private Country country;
    @Column(name = "USER_ACCOUNT", nullable = true)
    private String userAccount;
    @Column(name = "CURRENCY", nullable = true)
    private String currency;
    @Column(name = "AMOUNT", nullable = true)
    private BigDecimal amount;
    @Column(name = "LOCAL_CURRENCY", nullable = false)
    private String localCurrency;
    @Column(name = "LOCAL_AMOUNT", nullable = false)
    private BigDecimal localAmount;
    @org.hibernate.annotations.Type(type = "json")
    @Column(name = "TRACKING", columnDefinition = "json")
    private Object tracking;
    @Column(name = "DESCRIPTION", nullable = true)
    private String description;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("type", type)
                .add("chargedDate", chargedDate)
                .add("chargedDateSearch", chargedDateSearch)
                .add("tid", tid)
                .add("user", user)
                .add("sop", sop)
                .add("client", client)
                .add("country", country)
                .add("userAccount", userAccount)
                .add("currency", currency)
                .add("amount", amount)
                .add("localCurrency", localCurrency)
                .add("localAmount", localAmount)
                .add("tracking", tracking)
                .add("description", description)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, tid, user, sop, client, country, userAccount, currency, amount, localCurrency, localAmount, tracking, description, chargedDate);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<WalletTransaction>()
                .over(WalletTransaction::getId)
                .over(WalletTransaction::getType)
                .over(WalletTransaction::getTid)
                .over(WalletTransaction::getUser)
                .over(WalletTransaction::getSop)
                .over(WalletTransaction::getClient)
                .over(WalletTransaction::getCountry)
                .over(WalletTransaction::getUserAccount)
                .over(WalletTransaction::getCurrency)
                .over(WalletTransaction::getAmount)
                .over(WalletTransaction::getLocalCurrency)
                .over(WalletTransaction::getLocalAmount)
                .over(WalletTransaction::getTracking)
                .over(WalletTransaction::getDescription)
                .over(WalletTransaction::getChargedDate)
                .asserts(this, obj);
    }

}
