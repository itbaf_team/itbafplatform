
package com.itbaf.platform.paymenthub.extention.store.ws.notification;

import com.google.common.base.MoreObjects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * <p>Java class for notification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="notification">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoEvento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="datoEvento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "notification", propOrder = {
    "customer",
    "contentId",
    "pricingModel",
    "transactionType",
    "billingCode",
    "success",
    "transactionDate",
    "transactionTime",
    "price",
    "revenueShare",
    "distributorRevenue"
})
public class Notification {

    @XmlElement(required = true)
    protected String customer;
    @XmlElement(required = true)
    protected String contentId;
    @XmlElement(required = true)
    protected String pricingModel;
    @XmlElement(required = true)
    protected String transactionType;
    @XmlElement(required = true)
    protected String billingCode;
    @XmlElement(required = true)
    protected int success;
    @XmlElement(required = true)
    protected String transactionDate;
    @XmlElement(required = true)
    protected String transactionTime;
    protected BigDecimal price;
    protected String revenueShare;
    protected String distributorRevenue;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("customer", customer)
                .add("contentId", contentId)
                .add("pricingModel", pricingModel)
                .add("transactionType", transactionType)
                .add("billingCode", billingCode)
                .add("success", success)
                .add("transactionDate", transactionDate)
                .add("transactionTime", transactionTime)
                .add("price", price)
                .add("revenueShare", revenueShare)
                .add("distributorRevenue", distributorRevenue)
                .omitNullValues().toString();
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getPricingModel() {
        return pricingModel;
    }

    public void setPricingModel(String pricingModel) {
        this.pricingModel = pricingModel;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getRevenueShare() {
        return revenueShare;
    }

    public void setRevenueShare(String revenueShare) {
        this.revenueShare = revenueShare;
    }

    public String getDistributorRevenue() {
        return distributorRevenue;
    }

    public void setDistributorRevenue(String distributorRevenue) {
        this.distributorRevenue = distributorRevenue;
    }

    public Date getTransactionDateAndTime(){
        Date chargedDate;
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd,HH:mm");
            chargedDate = formatter.parse(this.transactionDate + "," +this.transactionTime );
        } catch (Exception ex) {
            chargedDate = new Date();
        }
        return chargedDate;
    }

    public String getTransactionId(){
        return "store." + this.getCustomer()   + "_" + this.getContentId()  + "_" + this.getPricingModel() +
                "_" + this.getTransactionType() + "_" + this.getTransactionDateAndTime().getTime();
    }

}
