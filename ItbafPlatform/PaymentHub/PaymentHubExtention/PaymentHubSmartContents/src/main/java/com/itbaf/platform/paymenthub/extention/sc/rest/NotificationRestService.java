package com.itbaf.platform.paymenthub.extention.sc.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.extention.sc.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.extention.sc.model.Notification;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/notification")
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationRestService {

    private final ObjectMapper mapper;
    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public NotificationRestService(final ObjectMapper mapper,
            final ServiceProcessorHandler serviceProcessorHandler) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/{country}/sc/subscription")
    // @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    // @Produces(MediaType.APPLICATION_JSON)
    public Response subscriptionNotificationGET(final @PathParam("country") String countryCode, final String json) {
        return subscriptionProcess(countryCode, json);
    }

    @POST
    @Path("/{country}/sc/subscription")
    // @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    // @Produces(MediaType.APPLICATION_JSON)
    public Response subscriptionNotificationPOST(final @PathParam("country") String countryCode, final String json) {
        return subscriptionProcess(countryCode, json);
    }

    private Response subscriptionProcess(final String countryCode, final String json) {

        log.info("SmartContents. countryCode: [" + countryCode + "] - Notification: [" + json + "]");
        try {
            Notification notification = mapper.readValue(json, Notification.class);
            serviceProcessorHandler.processNotification(notification);
        } catch (Exception ex) {
            log.error("Error al procesar notificacion: [" + json + "]. " + ex, ex);
        }

        return Response.status(Status.OK).entity("OK").type(MediaType.TEXT_PLAIN).build();
    }

}
