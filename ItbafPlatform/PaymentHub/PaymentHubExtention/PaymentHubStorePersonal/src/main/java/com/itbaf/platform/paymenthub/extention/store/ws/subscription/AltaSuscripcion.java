
package com.itbaf.platform.paymenthub.extention.store.ws.subscription;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="suscripcionId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="msisdn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientTid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="canal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="palabraClave" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="credentials" type="{urn:SendatelSoaService}securityInfo"/>
 *         &lt;element name="params" type="{urn:SendatelSoaService}arrayOfExtraParam"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "suscripcionId",
    "msisdn",
    "clientTid",
    "canal",
    "palabraClave",
    "credentials",
    "params"
})
@XmlRootElement(name = "AltaSuscripcion", namespace = "urn:SendatelSuscripcionesService")
public class AltaSuscripcion {

    protected int suscripcionId;
    @XmlElement(required = true)
    protected String msisdn;
    @XmlElement(required = true)
    protected String clientTid;
    protected int canal;
    @XmlElement(required = true)
    protected String palabraClave;
    @XmlElement(required = true)
    protected SecurityInfo credentials;
    @XmlElement(required = true)
    protected ArrayOfExtraParam params;

    /**
     * Gets the value of the suscripcionId property.
     * 
     */
    public int getSuscripcionId() {
        return suscripcionId;
    }

    /**
     * Sets the value of the suscripcionId property.
     * 
     */
    public void setSuscripcionId(int value) {
        this.suscripcionId = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdn() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdn(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the clientTid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientTid() {
        return clientTid;
    }

    /**
     * Sets the value of the clientTid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientTid(String value) {
        this.clientTid = value;
    }

    /**
     * Gets the value of the canal property.
     * 
     */
    public int getCanal() {
        return canal;
    }

    /**
     * Sets the value of the canal property.
     * 
     */
    public void setCanal(int value) {
        this.canal = value;
    }

    /**
     * Gets the value of the palabraClave property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPalabraClave() {
        return palabraClave;
    }

    /**
     * Sets the value of the palabraClave property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPalabraClave(String value) {
        this.palabraClave = value;
    }

    /**
     * Gets the value of the credentials property.
     * 
     * @return
     *     possible object is
     *     {@link SecurityInfo }
     *     
     */
    public SecurityInfo getCredentials() {
        return credentials;
    }

    /**
     * Sets the value of the credentials property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityInfo }
     *     
     */
    public void setCredentials(SecurityInfo value) {
        this.credentials = value;
    }

    /**
     * Gets the value of the params property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public ArrayOfExtraParam getParams() {
        return params;
    }

    /**
     * Sets the value of the params property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public void setParams(ArrayOfExtraParam value) {
        this.params = value;
    }

}
