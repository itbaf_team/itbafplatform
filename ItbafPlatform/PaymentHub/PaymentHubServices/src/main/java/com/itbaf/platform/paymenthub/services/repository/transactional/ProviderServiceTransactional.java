/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPACountryRepository;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPAProviderRepository;
import com.itbaf.platform.paymenthub.services.repository.ProviderService;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class ProviderServiceTransactional implements ProviderService {

    private final JPAProviderRepository providerRepository;
    private final JPACountryRepository countryRepository;

    @Inject
    public ProviderServiceTransactional(final JPAProviderRepository providerRepository,
            final JPACountryRepository countryRepository) {
        this.providerRepository = Validate.notNull(providerRepository, "A JPAProviderRepository class must be provided");
        this.countryRepository = Validate.notNull(countryRepository, "A JPACountryRepository class must be provided");
    }

    @Override
    public List<Provider> getAll() {
        try {
            return providerRepository.getAll();
        } catch (Exception ex) {
            log.error("Error al obtener Provider List. " + ex, ex);
        }
        return new ArrayList();
    }

    @Override
    public Provider findById(Long id) throws Exception {
        return providerRepository.findById(id);
    }

    @Override
    public Provider findByName(String name) {
        return providerRepository.findByName(name);
    }

    @Override
    public void saveOrUpdate(Provider provider) throws Exception {
        if (provider.getId() == null) {
            providerRepository.save(provider);
        } else {
            providerRepository.merge(provider);
        }
    }

    @Override
    public Provider findOrCreateByName(String name) throws Exception {
        Provider p;
        Object auxp = MainCache.memory24Hours().getIfPresent("provider_" + name);
        if (auxp == null) {
            p = findByName(name);
            if (p != null) {
                MainCache.memory24Hours().put("provider_" + name, p);
            } else {
                String[] aux = name.split("\\.");
                Country c = countryRepository.findByCode(aux[0]);
                if (c == null) {
                    log.warn("No hay un Country definido para: [" + aux[0] + "]. Se utilizara el valor por default: [XX]");
                    c = countryRepository.findByCode("XX");
                }
                p = new Provider();
                p.setCountry(c);
                p.setName(name);
                p.setDescription("Proveedor creado dinamicamente para " + aux[1].toUpperCase() + " en " + c.getName());
                saveOrUpdate(p);
            }
        } else {
            p = (Provider) auxp;
        }

        return p;

    }
}
