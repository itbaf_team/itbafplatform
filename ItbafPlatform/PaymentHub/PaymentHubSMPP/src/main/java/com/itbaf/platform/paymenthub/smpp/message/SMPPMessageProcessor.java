/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.smpp.message;

import com.itbaf.platform.paymenthub.messaging.smpp.PaymentHubSMPPMessage;
import com.itbaf.platform.paymenthub.messaging.smpp.SMPPMessageType;
import com.itbaf.platform.rmq.consumer.MessageProcessor;

/**
 *
 * @author javier
 */
public interface SMPPMessageProcessor extends MessageProcessor {
 
    public SMPPMessageType getSMPPMessageType();
    
    public void process(PaymentHubSMPPMessage msg);
}
