/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.billing;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
@Entity(name = "ONDEMAND_BILLING")
@Table(name = "ONDEMAND_BILLING", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(false)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class OnDemandBilling implements Serializable {

    private static final long serialVersionUID = 20180410114401L;

    @Id
    @Column(name = "ONDEMAND_REGISTRY_ID", nullable = false, unique = true)
    private String id;
    @Column(name = "USER_ACCOUNT")
    private String userAccount;
    @OneToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;
    @Column(name = "TRANSACTION_ID")
    private String transactionId;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CHARGED_DATE")
    private Date chargedDate;
    @Column(name = "CURRENCY")
    private String currency;
    @Column(name = "FULL_AMOUNT")
    private BigDecimal fullAmount;
    @Column(name = "NET_AMOUNT")
    private BigDecimal netAmount;
    @Transient
    private FacadeRequest transactionTracking;
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("userAccount", userAccount)
                .add("sop", sop)
                .add("transactionId", transactionId)
                .add("chargedDate", chargedDate)
                .add("currency", currency)
                .add("fullAmount", fullAmount)
                .add("netAmount", netAmount)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userAccount, transactionId, chargedDate, currency, fullAmount, netAmount, sop);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<OnDemandBilling>()
                .over(OnDemandBilling::getId)
                .over(OnDemandBilling::getUserAccount)
                .over(OnDemandBilling::getTransactionId)
                .over(OnDemandBilling::getChargedDate)
                .over(OnDemandBilling::getCurrency)
                .over(OnDemandBilling::getFullAmount)
                .over(OnDemandBilling::getNetAmount)
                .over(OnDemandBilling::getSop)
                .asserts(this, obj);
    }
}
