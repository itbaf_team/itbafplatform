/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.tiaxa.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.resources.FacadeCache;
import com.itbaf.platform.paymenthub.extention.tiaxa.resources.TiaxaResources;
import com.itbaf.platform.paymenthub.extention.tiaxa.resources.TiaxaResponse;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import com.sun.jersey.core.util.Base64;
import org.apache.commons.lang3.Validate;
import org.codehaus.jettison.json.JSONObject;
import org.csapi.schema.parlayx.syncsubscription.v1_0.SyncOrderRelationship;
import javax.xml.ws.Holder;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.itbaf.platform.commons.CommonFunction.*;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final FacadeCache facadeCache;
    private final TiaxaResources tiaxaResources;
    private final Map<String, Provider> providerMap;
    private final PHProfilePropertiesService profilePropertiesService;
    private final SubscriptionService subscriptionService;

    @Inject
    public ServiceProcessorHandler(final FacadeCache facadeCache,
            final TiaxaResources smtResources,
            final Map<String, Provider> providerMap,
            final PHProfilePropertiesService profilePropertiesService,
            final SubscriptionService subscriptionService) {
        this.facadeCache = Validate.notNull(facadeCache, "A FacadeCache class must be provided");
        this.tiaxaResources = Validate.notNull(smtResources, "A SMTResources class must be provided");
        this.providerMap = Validate.notNull(providerMap, "A Map<String, Provider> class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
        this.subscriptionService = Validate.notNull(subscriptionService, "A SubscriptionService class must be provided");
    }

    public void processNotification(String countryCode, SyncOrderRelationship notification) {

        if (countryCode == null || countryCode.length() != 2) {
            log.error("El Codigo del pais no es valido. CountryCode: [" + countryCode + "]");
            return;
        }

        Provider p = providerMap.get(countryCode.toUpperCase());
        if (p == null) {
            log.error("No hay configurado un provider para CountryCode: [" + countryCode + "]");
            return;
        }

        notification.setCountryCode(countryCode);

        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notification);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notification);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
                    //despues al mensaje lo procesan aca mismo en public Boolean processMessage(String message)
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {
        SyncOrderRelationship notification;
        try {
            notification = mapper.readValue(message, SyncOrderRelationship.class);
            Thread.currentThread().setName(notification.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }

        Provider provider = providerMap.get(notification.getCountryCode().toUpperCase());
        if (provider == null) {
            log.error("No hay configurado un provider para CountryCode: [" + notification.getCountryCode() + "]");
            return null;
        }
        final String threadName = Thread.currentThread().getName() + "-" + provider.getName() + ".n";
        Thread.currentThread().setName(threadName);

        PaymentHubMessage msg;
        Boolean res = null;
        if( notification.getUpdateType() == 1 && notification.getRentResult() == 5){
            //retention
            res = processSubscriptionRetentionNotification(notification, provider);
        } else {
            switch (notification.getUpdateType()) {
                case 1: // add -> subscribe
                case 3: // update -> billing
                    msg = processSubscriptionNotification(notification, provider);
                    res = processBillingNotification(notification, msg, provider);
                    break;
                case 2: // delete -> unsubscribe
                    res = processUnsubscriptionNotification(notification, provider);
                    break;
                default:
                    log.error("updateType inesperado: [" + notification.getUpdateType() + "]");
            }
        }

        return res;
    }

    private PaymentHubMessage processSubscriptionNotification(SyncOrderRelationship notification, Provider provider) {

        Map<String, String> settings = new HashMap();
        settings.put("productID", notification.getProductID());
        settings.put("serviceID", notification.getServiceID());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        if (sop == null) {
            log.error("No existe un SOP configurado para ProductID: [" + notification.getProductID() + "] - ServiceID: [" + notification.getServiceID() + "]");
            return null;
        }

        Tariff t = sop.getMainTariff();
        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(notification.getFrequencyType());
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(sdf.parse(notification.getEffectiveTime()));
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(MessageType.SUBSCRIPTION_ACTIVE);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            //  msg.setExternalUserAccount(notification.getUserID().getID());
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getUserID().getID());
            msg.setTransactionId(notification.getExtensionInfoMap().get("TraceUniqueID"));
            msg.setChannelIn(notification.getChannel());
            if (notification.getExtensionInfoMap().get("userPhone") != null) {
                msg.setUserAccount(notification.getExtensionInfoMap().get("userPhone"));
            } else {
                msg.setUserAccount(notification.getUserID().getID());
            }

            String adTracking = null;
            try {
                adTracking = notification.getAdProvider();
                log.info("adProvider recibido ["+ adTracking +"]");
                if (adTracking != null) {
                    // Verificar que el valor de adProvider es un cadena Base64
                    boolean isBase64 = Base64.isArrayByteBase64(adTracking.getBytes());
                    if (isBase64) {
                        byte[] decodedBytes = Base64.decode(adTracking);
                        adTracking = new String(decodedBytes);
                        Map<String, Object> map = new ObjectMapper().readValue(adTracking, Map.class);

                        msg.getTrackingParams().putAll(map);
                    } else {
                        log.info("adProvider no es Base64 ["+ adTracking +"]");
                    }
                    /*
                    Extra extra = new Extra();
                    extra.stringData.put("AdProvider", adTracking);

                    msg.setExtra(extra);

                     */
                    try {
                        FacadeRequest fr = facadeCache.getFacadeRequest(null, adTracking, null, 0L);
                        if (fr != null) {
                            log.info("Encontrado un FacadeRequest: [" + fr + "]");
                            LinkedHashMap<String, Object> adn = (LinkedHashMap<String, Object>) fr.adTracking;
                            msg.getTrackingParams().putAll(adn);
                        }
                    } catch (Exception ex) {
                        log.error("Error al procesar adTracking. " + ex, ex);
                    }
                }
            } catch (Exception ex) {
            }

            try {
                facadeCache.setTemporalData(sop.getProvider().getName() + "_" + adTracking, msg.getUserAccount(), 120);
            } catch (Exception ex) {
            }

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }

            return msg;
        } catch (Exception ex) {
            log.error("Error al procesar TiaxaSMTNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    private Boolean processBillingNotification(SyncOrderRelationship notification, PaymentHubMessage msg, Provider provider) {
        if (msg == null) {
            return null;
        }

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String fee = notification.getExtensionInfoMap().get("fee");
        if (fee == null || "0".equals(fee) || "0.0000".equals(fee)) {
            // log.info("No se procesara la notificacion de cobro con valor CERO");
            return false;
        }

        // log.info("Procesando un cobro: [" + msg.getUserAccount() + "] - Amount: [" + fee + "] - SOP: [" + msg.getSopId() + "]");
        Map<String, String> settings = new HashMap();
        settings.put("productID", notification.getProductID());
        settings.put("serviceID", notification.getServiceID());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        if (sop == null) {
            log.error("No existe un SOP configurado para ProductID: [" + notification.getProductID() + "] - ServiceID: [" + notification.getServiceID() + "]");
            return null;
        }

        try {
            BigDecimal netAmount = new BigDecimal(fee);

            if (BigDecimal.ZERO.equals(netAmount) || BigDecimal.ZERO.compareTo(netAmount) == 0) {
                return false;
            }

            netAmount = netAmount.divide(new BigDecimal(profilePropertiesService.getCommonProperty(provider.getId(), "tiaxa.amount.divisor")));

            Tariff t = sop.findTariffByNetAmount(netAmount);
            if (t == null) {
                //Verificamos si es una tarifa principal nueva
                t = updateMainTariff(sop, null, netAmount);
                if (t == null) {
                    //Creamos o actualizamos la tarifa hija
                    t = updateNetAmountChildrenTariff(sop, netAmount, notification.getServiceID());
                }
            }

            if (t == null) {
                log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
                return null;
            }

            if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }

            msg.setFullAmount(t.getFullAmount());
            msg.setNetAmount(t.getNetAmount());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setChargedDate(sdf.parse(notification.getCreatTime()));
            msg.setTariffId(t.getId());
            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
            }
            return true;
        } catch (Exception ex) {
            log.error("Error al procesar notificacion de cobro TiaxaSMTNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private Boolean processSubscriptionRetentionNotification(SyncOrderRelationship notification, Provider provider) {

        Map<String, String> settings = new HashMap();
        settings.put("productID", notification.getProductID());
        settings.put("serviceID", notification.getServiceID());
        SOP sop = sopService.findBySettings(provider.getName(), settings);

        if (sop == null) {
            log.error("No existe un SOP configurado para ProductID: [" + notification.getProductID() + "] - ServiceID: [" + notification.getServiceID() + "]");
            return null;
        }

        Tariff t = sop.getMainTariff();
        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(notification.getFrequencyType());
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(sdf.parse(notification.getEffectiveTime()));
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(MessageType.SUBSCRIPTION_RETENTION);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            //  msg.setExternalUserAccount(notification.getUserID().getID());
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getUserID().getID());
            msg.setTransactionId(notification.getExtensionInfoMap().get("TraceUniqueID"));
            msg.setChannelIn(notification.getChannel());
            if (notification.getExtensionInfoMap().get("userPhone") != null) {
                msg.setUserAccount(notification.getExtensionInfoMap().get("userPhone"));
            } else {
                msg.setUserAccount(notification.getUserID().getID());
            }

            String adTracking = null;
            try {
                adTracking = notification.getAdProvider();
                log.info("adProvider recibido ["+ adTracking +"]");
                if (adTracking != null) {
                    Extra extra = new Extra();
                    extra.stringData.put("AdProvider", adTracking);
                    msg.setExtra(extra);
                    try {
                        FacadeRequest fr = facadeCache.getFacadeRequest(null, adTracking, null, 0L);
                        if (fr != null) {
                            log.info("Encontrado un FacadeRequest: [" + fr + "]");
                            LinkedHashMap<String, Object> adn = (LinkedHashMap<String, Object>) fr.adTracking;
                            msg.getTrackingParams().putAll(adn);
                        }
                    } catch (Exception ex) {
                        log.error("Error al procesar adTracking. " + ex, ex);
                    }
                }
            } catch (Exception ex) {
            }

            try {
                facadeCache.setTemporalData(sop.getProvider().getName() + "_" + adTracking, msg.getUserAccount(), 120);
            } catch (Exception ex) {
            }

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }

            return true;
        } catch (Exception ex) {
            log.error("Error al procesar TiaxaSMTNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    private Boolean processUnsubscriptionNotification(SyncOrderRelationship notification, Provider provider) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Map<String, String> settings = new HashMap();
        settings.put("productID", notification.getProductID());
        settings.put("serviceID", notification.getServiceID());
        SOP sop = sopService.findBySettings(provider.getName(), settings);
        if (sop == null) {
            log.error("No existe un SOP configurado para ProductID: [" + notification.getProductID() + "] - ServiceID: [" + notification.getServiceID() + "]");
            return null;
        }

        Tariff t = sop.getMainTariff();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(notification.getFrequencyType());
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(sdf.parse(notification.getEffectiveTime()));
            msg.setToDate(sdf.parse(notification.getCreatTime()));
            msg.setFullAmount(t.getFullAmount());
            msg.setNetAmount(t.getNetAmount());
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            // msg.setExternalUserAccount(notification.getUserID().getID());
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(notification.getUserID().getID());
            msg.setTransactionId(notification.getExtensionInfoMap().get("TraceUniqueID"));
            msg.setChannelOut(notification.getChannel());
            if (notification.getExtensionInfoMap().get("userPhone") != null) {
                msg.setUserAccount(notification.getExtensionInfoMap().get("userPhone"));
            } else {
                msg.setUserAccount(notification.getUserID().getID());
            }

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
            return true;
        } catch (Exception ex) {
            log.error("Error al procesar TiaxaSMTNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        log.info(subscription);
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount()));
            log.info(subscription.getUserAccount());
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        SOP sop = subscription.getSop();
        try {
            TiaxaResponse tr = tiaxaResources.subscribeUser(subscription.getUserAccount(), sop);
            log.info(tr);

            if (tr == null || tr.resultCode == null) {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "No fue posible realizar el alta con Tiaxa";
            } else {
                rm.message = "No fue posible realizar el alta. Codigo de error en Tiaxa: [" + tr.resultCode + "] ";

                log.info(tr.resultCode);

                if (Subscription.Status.ACTIVE.equals(subscription.getStatus()) &&
                        (7865 == tr.resultCode || 7201 == tr.resultCode) ) {
                    rm.status = ResponseMessage.Status.ACTIVE;
                    rm.message = "Transaccion exitosa. Code: [" + tr.resultCode + "]";
                }  else {
                    switch (tr.resultCode) {
                        case 0: //success
                            rm.status = ResponseMessage.Status.OK;
                            rm.message = "Transaccion exitosa. Code: [" + tr.resultCode + "]";
                            break;
                        case 5://error interno
                            log.error(rm.message);
                            rm.status = ResponseMessage.Status.INTERNAL_ERROR;
                            break;
                        case 7: //the PIN is expired
                            log.info(rm.message);
                            rm.status = ResponseMessage.Status.PIN_ERROR;
                            break;
                        case 8: //PIN not match
                            log.info(rm.message);
                            rm.status = ResponseMessage.Status.PIN_NOT_MATCH;
                            break;
                        case 7201: //product already subscribed
                            rm.status = ResponseMessage.Status.PENDING;
                            rm.message = "Transaccion exitosa. Code: [" + tr.resultCode + "]";
                            break;
                         case 7306: //black list users can not subscribe service
                            log.info(rm.message);
                            rm.status = ResponseMessage.Status.BLACKLIST;
                            break;                       
                         case 7330: //Isufficient balance
                            log.info(rm.message);
                            rm.status = ResponseMessage.Status.INSUFFICIENT_BALANCE;
                            break;                     
                        case 7629: //user is locked (prevent users concurrent operation)
                            log.info(rm.message);
                            rm.status = ResponseMessage.Status.LOCKED;
                            break;
                        case 7363: //gray list users can not subscribe service
                            log.info(rm.message);
                            rm.status = ResponseMessage.Status.BLACKLIST;
                            break;                            
                        case 7865: //subscription request already sent
                            rm.status = ResponseMessage.Status.PENDING;
                            rm.message = "Transaccion exitosa. Code: [" + tr.resultCode + "]";
                        case 7999:
                            log.info("Error desconocido [" + rm.message + "]");
                            rm.status = ResponseMessage.Status.ERROR;
                            break;
                        default:
                            log.info(rm.message);
                            rm.status = ResponseMessage.Status.ERROR;
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Error en Tiaxa al realizar el alta [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Error en Tiaxa. " + ex.getStackTrace();
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        try {
            userAccountNormalizer(subscription.getUserAccount());
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        switch (subscription.getStatus()) {
            case ACTIVE:
                rm.status = ResponseMessage.Status.ACTIVE;
                break;
            case BLACKLIST:
                rm.status = ResponseMessage.Status.BLACKLIST;
                break;
            default:
                rm.status = ResponseMessage.Status.REMOVED;
                break;
        }
        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex) {
        }
        return rm;
    }

    public String userAccountNormalizer(String userId) throws Exception {
        if (userId != null && (userId.length() == 11 || userId.length() == 12 || userId.length() == 13)) {
            return userId;
        }

        throw new Exception("No es posible normalizar el UserAccount. Formato Invalido!");
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error: " + ex;
            return rm;
        }
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        SOP sop = subscription.getSop();
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        try {
            TiaxaResponse tr = tiaxaResources.unSubscribeUser(subscription.getUserAccount(), sop);
            if (tr == null || tr.resultCode == null) {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "No fue posible realizar la baja con Tiaxa";
            } else {
                switch (tr.resultCode) {
                    case 0: //success
                        rm.status = ResponseMessage.Status.OK;
                        rm.message = "Transaccion exitosa. Code: [" + tr.resultCode + "]";
                        break;
                    default:
                        String r = "No fue posible realizar la baja. Codigo de error en Tiaxa: [" + tr.resultCode + "] ";
                        log.error(r);
                        rm.status = ResponseMessage.Status.ERROR;
                        rm.message = r;
                }
            }
        } catch (Exception ex) {
            log.error("Error en Tiaxa al realizar la baja [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Error en Tiaxa. " + ex;
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public void syncRetention(String providerName){
        List<Subscription> subscriptionsRetention = subscriptionService.getRetentionSubscriptionsBeforeDays(providerName, 42);

        for (Subscription subscription : subscriptionsRetention) {
            log.info("Se procesará baja de suscripción con estado de retention: [" + subscription.getId() + "]");
            this.processUnsubscriptionRetention(subscription);
        }
    }


    private Boolean processUnsubscriptionRetention(Subscription subscription) {
        SOP sop = subscription.getSop();
        if (sop == null) {
            log.error("No existe un SOP configurado para la suscripción: [" + subscription.getId()  + "]");
            return null;
        }

        Provider provider = subscription.getSop().getProvider();
        Tariff t = sop.getMainTariff();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrecuency(t.getFrecuency());
            msg.setFrequencyType(t.getFrecuencyType());
            msg.setToDate(new Date());
            msg.setFullAmount(t.getFullAmount());
            msg.setNetAmount(t.getNetAmount());
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.SYNC_RETENTION);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(subscription.getSubscriptionRegistry().getExternalId());
            msg.setChannelOut("SYNC_RETENTION");
            msg.setUserAccount(subscription.getUserAccount());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
            return true;
        } catch (Exception ex) {
            log.error("Error al procesar TiaxaSyncRetention: [" + subscription + "]. " + ex, ex);
        }

        return null;
    }
    
    public ResponseMessage sendPin(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);
        switch (aux.status) {
            case ACTIVE:
            case PENDING:
            case BLACKLIST:
                rm.status = aux.status;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case REMOVED:
                SOP sop = subscription.getSop();
                try {
                    log.info("subscription : "+subscription.getUserAccount().toString());
                    JSONObject respuestaJson = tiaxaResources.sendPin(subscription.getUserAccount(), sop, subscription.getUserAccount().toString());
                    log.info("resultcode: " +respuestaJson.getString("resultCode") +" resultDesc: "+respuestaJson.getString("resultDesc"));
                    rm.message = "[" + respuestaJson.getString("resultCode") + "][" + respuestaJson.getString("resultDesc") + "]";
                    switch (Integer.parseInt(respuestaJson.getString("resultCode"))) {
                        case 0:
                            rm.status = ResponseMessage.Status.OK_NS;
                            break;
                        case 12:
                            rm.status = ResponseMessage.Status.OK;
                            subscription.setStatus(Subscription.Status.PENDING);
                            break;
                        case 6:
                            rm.status = ResponseMessage.Status.ACTIVE;
                            subscription.setStatus(Subscription.Status.ACTIVE);
                            break;
                        case 7:
                        case 8:
                            rm.status = ResponseMessage.Status.PIN_ERROR;
                            break;
                        case 9:
                        case 10:
                        case 11:
                            rm.status = ResponseMessage.Status.BLACKLIST;
                            subscription.setStatus(Subscription.Status.BLACKLIST);
                            break;
                        default:
                            rm.status = ResponseMessage.Status.ERROR;
                    }
                } catch (Exception ex) {
                    log.error("Error en SMT al enviar PIN [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "Error en SMT. " + ex;
                }
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }
    
    public ResponseMessage validatePin(Subscription subscription, String pin) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);

        switch (aux.status) {
            case ACTIVE:
            case PENDING:
            case BLACKLIST:
                rm.status = aux.status;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case REMOVED:
                SOP sop = subscription.getSop();
                try {
                    String resultCode = null;
                    String resultDesc = null;
                    String transactionID = null;

                    JSONObject respuestaJson = tiaxaResources.validatePin(subscription.getUserAccount(), sop, pin, subscription.getUserAccount().toString());
                    rm.message = "[" + respuestaJson.getString("resultCode") + "][" + respuestaJson.getString("resultDesc") + "]";
                    switch (Integer.parseInt(respuestaJson.getString("resultCode"))) {
                        case 0:
                        case 12:
                            rm.status = ResponseMessage.Status.OK;
                            subscription.setStatus(Subscription.Status.PENDING);
                            break;
                        case 6:
                            rm.status = ResponseMessage.Status.ACTIVE;
                            subscription.setStatus(Subscription.Status.ACTIVE);
                            break;
                        case 7:
                        case 8:
                            rm.status = ResponseMessage.Status.PIN_ERROR;
                            break;
                        case 9:
                        case 10:
                        case 11:
                            rm.status = ResponseMessage.Status.BLACKLIST;
                            subscription.setStatus(Subscription.Status.BLACKLIST);
                            break;
                        default:
                            rm.status = ResponseMessage.Status.ERROR;
                    }
                } catch (Exception ex) {
                    log.error("Error en SMT al validar PIN [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "Error en SMT. " + ex;
                }
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);

        return rm;
    }
}
