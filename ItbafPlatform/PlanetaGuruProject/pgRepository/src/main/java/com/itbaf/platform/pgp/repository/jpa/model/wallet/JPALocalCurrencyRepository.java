/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.repository.jpa.model.wallet;

import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.pgp.model.wallet.LocalCurrency;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
public class JPALocalCurrencyRepository extends AbstractJPARepositorySession<LocalCurrency, Long> {

    @Inject
    public JPALocalCurrencyRepository(final Provider<EntityManager> entityManagerProvider) {
        super(LocalCurrency.class, entityManagerProvider);
    }

}
