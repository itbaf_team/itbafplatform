package com.itbaf.platform.paymenthub.extention.smsc.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.extention.smsc.handler.ServiceProcessorHandler;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/notification")
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationRestService {

    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public NotificationRestService(ServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");

    }

    @POST
    @Path("/{country}/smsc/subscription")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response subscriptionNotification(final @PathParam("country") String countryCode,
            @FormParam("ctr") final String ctr) {

        log.info("Notificacion de SMSC. countryCode: " + countryCode + " - CTR: " + ctr);
        try {
            serviceProcessorHandler.processNotification(countryCode, ctr);
        } catch (Exception ex) {
            log.error("Error al interpretar CTR. CountryCode [" + countryCode + "] - Subscription Notification: [" + ctr + "]. " + ex, ex);
        }

        return Response.status(Status.OK).build();
    }
}
