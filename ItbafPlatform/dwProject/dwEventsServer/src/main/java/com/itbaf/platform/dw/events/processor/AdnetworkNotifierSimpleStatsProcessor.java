/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events.processor;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.stats.model.AdnetworkNotifierSimpleElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.commons.stats.model.StatsMessage;
import com.itbaf.platform.paymenthub.model.AdnetworkNotifierSimple;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.services.repository.AdnetworkNotifierSimpleService;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class AdnetworkNotifierSimpleStatsProcessor extends StatsProcessor {

    private static final ElasticData.ElasticType elasticType = ElasticData.ElasticType.adnetwork_notifier_simple;

    private final AdnetworkNotifierSimpleService adnetworkNotifierSimpleService;

    @Inject
    public AdnetworkNotifierSimpleStatsProcessor(final AdnetworkNotifierSimpleService adnetworkNotifierSimpleService) {
        this.adnetworkNotifierSimpleService = adnetworkNotifierSimpleService;
    }

    @Override
    public Boolean process(String msg) throws Exception {
       // log.info("Procesando DW AdnetworkNotifierSimple: [" + msg + "]");

        StatsMessage smx = mapper.readValue(msg, StatsMessage.class);
        String mm = "dw_" + smx.elasticData.elasticType + "_" + smx.elasticData.elasticId;

        RLock lock = instrumentedObject.lockObject(mm, 60L);
        try {
            String dm = clientCache.getTemporalData(mm);
            if (dm != null) {
                if (smx.lastUpdateMillis < Long.parseLong(dm)) {
                  //  log.info("Ignorando DW AdnetworkNotifierSimple: [" + msg + "]");
                    return true;
                }
            }
            StatsMessage sm = new StatsMessage();
            AdnetworkNotifierSimple adn = adnetworkNotifierSimpleService.findById(smx.elasticData.elasticId);
            if (adn == null) {
               // log.info("No existe el registro en la base. [" + msg + "]");
                Thread.sleep(7000);
                adn = adnetworkNotifierSimpleService.findById(smx.elasticData.elasticId);
                if (adn == null) {
                    log.warn("No existe el registro en la base. [" + msg + "]");
                    Thread.sleep(1000);
                    return null;
                }
            }
            sm.thread = smx.thread;
            AdnetworkNotifierSimpleElastic adne = new AdnetworkNotifierSimpleElastic();
            adne.adnetworkTracking = adn.getAdnetworkTracking();
            adne.notificationSend = adn.getNotificationSend();
            adne.response = adn.getResponse();
            adne.sop = adn.getSop();
            adne.sop.setIntegrationSettings(null);
            adne.sop.setTariffs(null);
            Country c = new Country();
            c.setId(adne.sop.getProvider().getCountry().getId());
            c.setCode(adne.sop.getProvider().getCountry().getCode());
            c.setCurrency(adne.sop.getProvider().getCountry().getCurrency());
            c.setName(adne.sop.getProvider().getCountry().getName());
            adne.sop.getProvider().setCountry(c);
            adne.subscriptionDate = adn.getSubscriptionDate();
            adne.transactionId = adn.getTrackingId();

            sm.elasticData = adne;

            //Enviamos el AdnetworkNotifierSimple actualizado al Rabbit
            String json = mapper.writeValueAsString(sm);
            if (Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.adnetwork_notifier_simple, json))) {
                clientCache.setTemporalData(mm, sm.lastUpdateMillis.toString(), 1L);
                return true;
            }
        } catch (Exception ex) {
            log.error("Error al procesar AdnetworkNotifierSimple [" + msg + "]. " + ex, ex);
        } finally {
            instrumentedObject.unLockObject(lock);
        }

        return null;
    }

    @Override
    public ElasticData.ElasticType getElasticType() {
        return elasticType;
    }

}
