package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.SmsSop;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.inject.Provider;

@lombok.extern.log4j.Log4j2
public class JPASmsSopRepository extends AbstractJPARepositorySession<SmsSop, Long> {

    @Inject
    public JPASmsSopRepository(final Provider<EntityManager> entityManagerProvider) {
        super(SmsSop.class, entityManagerProvider);
    }

    public String getSms(Long sop, String type){
        String messenger = null;

        log.info("Antes de ejecutar smsMesseger: SOP NUM " + sop);

        if (sop != null) {
            Query q;
            EntityManager em = getEntityManager();
            String sql = "SELECT ss.MESSAGE_TEXT FROM paymentHub.SMS_SOP ss WHERE ss.SOP_ID= ? AND ss.TYPE=?" +
                    "ORDER BY RAND() " +
                    "LIMIT 1";

            q = em.createNativeQuery(sql);
            q.setParameter(1,sop);
            q.setParameter(2,type);

            messenger = q.getSingleResult().toString();

        }
        log.info("Resultado consulta isMultipleMessage con querydsl : "+messenger);
        return  messenger;
    }

}
