package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;
import com.google.gson.JsonObject;

import java.util.Date;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Refund {

    private Number id;
    private Number payment_id;
    private Float amount;
    private JsonObject metadata;
    private Source source;
    private Date date_created;
    private String unique_sequence_number;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("payment_id", payment_id)
                .add("amount", amount)
                .add("metadata", metadata)
                .add("source", source)
                .add("date_created", date_created)
                .add("unique_sequence_number", unique_sequence_number)
                .omitNullValues().toString();
    }

}
