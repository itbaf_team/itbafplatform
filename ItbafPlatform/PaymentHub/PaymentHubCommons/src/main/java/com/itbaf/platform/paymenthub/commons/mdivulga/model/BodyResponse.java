/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.mdivulga.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class BodyResponse {

    public String statusSubscription;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("statusSubscription", statusSubscription)
                .omitNullValues().toString();
    }
}
