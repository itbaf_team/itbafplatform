/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.sc.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class SCRequest {

    public String sid;
    public String product;
    public String plan;
    public String campaign;
    public String channel;
    public String validator;
    public String[] subscriptions;
    public Integer take;// Amount of record to be taken [Default: 15]
    public Integer skip;// skip Amount of record to skip [Default: 0]
    public String trial;// Days of trial (Default: 0 or Camapign value)

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("sid", sid)
                .add("product", product)
                .add("plan", plan)
                .add("campaign", campaign)
                .add("channel", channel)
                .add("validator", validator)
                .add("subscriptions", subscriptions)
                .add("take", take)
                .add("skip", skip)
                .add("trial", trial)
                .omitNullValues().toString();
    }
}
