import { Component, ViewChild, ElementRef} from '@angular/core';

import { SubscriptionService } from '../../../../@core/data/subscription.service';
import { SearchComponent } from '../search.component';

@Component({
  selector: 'ngx-subscriptions-search-button',
  styleUrls: ['./button-search.component.scss'],
  templateUrl: './button-search.component.html',
})
export class ButtonSearchComponent {
   
    @ViewChild('buttonSearch') buttonSearch: ButtonSearchComponent;
    @ViewChild('userAccountInput') userAccountInput: ElementRef;
    checked=false;
    subscriptions: any;
    searchComponent: SearchComponent;

    constructor(private subscriptionService: SubscriptionService) { 
    }

    setSearchComponent(searchComponent){
        this.searchComponent=searchComponent;
    }

    onChangeCheckbox(event): void {
        this.checked=event.returnValue;
    }
    
    onClickSearchButton(event): void {
        if(this.userAccountInput.nativeElement.value && this.userAccountInput.nativeElement.value.length > 6){
            this.searchComponent.setSpinnerStatus(true);
            this.subscriptionService.getSubscriptions(this.userAccountInput.nativeElement.value, this.checked)
            .subscribe((sl: any) => this.processData(sl.data));
        }
    }
    
    processData(data){
        this.subscriptions = data;
        this.searchComponent.setSpinnerStatus(false);
    }
    
}
