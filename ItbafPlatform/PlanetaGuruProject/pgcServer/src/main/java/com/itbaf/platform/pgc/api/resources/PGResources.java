package com.itbaf.platform.pgc.api.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.messaging.rest.oauth.ResponseAuthMessage;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.services.repository.GenericPropertyService;
import com.itbaf.platform.pgp.services.repository.UserService;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class PGResources {

    private final ObjectMapper mapper;
    private final UserService userService;
    private final RequestClient requestClient;
    private final GenericPropertyService genericPropertyService;

    @Inject
    public PGResources(final ObjectMapper mapper,
            final UserService userService,
            final RequestClient requestClient,
            final GenericPropertyService genericPropertyService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.userService = Validate.notNull(userService, "An UserService class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.genericPropertyService = Validate.notNull(genericPropertyService, "A GenericPropertyService class must be provided");
    }

    private String getClientToken(String client_id) {
        Object to = MainCache.memory48Hours().getIfPresent("oauth_token_pgj_" + client_id);
        String token = null;
        if (to == null) {
            User client = userService.findByclientId(client_id);
            if (client == null) {
                log.warn("No existe un Client para client_id: [" + client_id + "]");
                return null;
            }

            String url = genericPropertyService.getValueByKey("oauth.url") + "/auth/token";
            String queryString = "grant_type=client_credentials&client_id=" + client_id + "&client_secret=" + client.getCredentialUserProperty().getClient().getClientSecret();

            Map<String, String> headers = new HashMap();
            String response = requestClient.requestPost(url, queryString, MediaType.APPLICATION_FORM_URLENCODED_TYPE, MediaType.APPLICATION_JSON_TYPE, headers);

            log.info("getClientToken. client_id: [" + client_id + "]. Respuesta de PG: [" + response + "]");
            try {
                ResponseAuthMessage rm = mapper.readValue(response, ResponseAuthMessage.class);
                if (rm.error == null) {
                    token = rm.access_token;
                    MainCache.memory48Hours().put("oauth_token_pgj_" + client_id, token);
                }
            } catch (IOException ex) {
                log.error("Error al procesar respuesta de PG: [" + response + "]. " + ex, ex);
            }
        } else {
            token = (String) to;
        }
        return token;
    }

    public ResponseAuthMessage getUserToken(String clientId, String code, String codeVerifier) {

        try {
            String url = genericPropertyService.getValueByKey("oauth.url") + "/auth/token";
            String token = getClientToken(clientId);
            String queryString = "grant_type=authorization_code&access_token=" + token + "&code=" + code + "&code_verifier=" + codeVerifier;

            Map<String, String> headers = new HashMap();
            String response = requestClient.requestPost(url, queryString, MediaType.APPLICATION_FORM_URLENCODED_TYPE, MediaType.APPLICATION_JSON_TYPE, headers);
            log.info("getUserToken. client_id: [" + clientId + "] - code: [" + code + "]. Respuesta de PG: " + response);

            ResponseAuthMessage rm = null;
            try {
                rm = mapper.readValue(response, ResponseAuthMessage.class);
            } catch (Exception e) {
                log.error("Error al procesar respuesta de PG. client_id: [" + clientId + "] - code: [" + code + "]. Response: [" + response + "]. " + e, e);
            }

            return rm;
        } catch (Exception e) {
            throw new IllegalStateException("Error al obtener el token de usuario para el client_id: [" + clientId + "] - code: [" + code + "].", e);
        }
    }

    public PGMessageResponse getUserProfile(String clientId, String userToken) {
        try {
            String token = getClientToken(clientId);
            String url = genericPropertyService.getValueByKey("pg.server.url") + "/rest/api/profile";
            String json = "{\"userToken\":\"" + userToken + "\"}";

            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Bearer " + token);

            String response = requestClient.requestJSONPost(url, json, headers);
            log.info("getUserProfile. client_id: [" + clientId + "] - userToken: [" + userToken + "]. Respuesta de PG: " + response);

            PGMessageResponse rm = null;
            try {
                rm = mapper.readValue(response, PGMessageResponse.class);
            } catch (Exception e) {
                log.error("Error al procesar respuesta de PG: [" + response + "]. " + e, e);
            }

            return rm;
        } catch (Exception e) {
            throw new IllegalStateException("Error al obtener el perfil de usuario para el client_id: [" + clientId + "] - userToken: [" + userToken + "].", e);
        }
    }

    public PGMessageResponse getFunds(String clientId, String userToken) {
        try {
            String token = getClientToken(clientId);
            String url = genericPropertyService.getValueByKey("pg.server.url") + "/rest/api/wallet/funds";
            String json = "{\"userToken\":\"" + userToken + "\"}";

            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Bearer " + token);

            String response = requestClient.requestJSONPost(url, json, headers);
            log.info("getFunds. ClientId: [" + clientId + "] - userToken: [" + userToken + "]. Respuesta de PG: " + response);

            PGMessageResponse rm = null;
            try {
                rm = mapper.readValue(response, PGMessageResponse.class);
            } catch (Exception e) {
                log.error("Error al procesar respuesta de PG: ClientId: [" + clientId + "] - userToken: [" + userToken + "]. Response: [" + response + "]. " + e, e);
            }

            return rm;
        } catch (Exception e) {
            throw new IllegalStateException("Error al obtener creditos del usuario para el client_id: [" + clientId + "] - userToken: [" + userToken + "].", e);
        }
    }

    public PGMessageResponse getUserAuthorizationCode(String clientId, String accessToken, String codeVerifier) {
        try {
            String token = getClientToken(clientId);
            String url = genericPropertyService.getValueByKey("pg.server.url") + "/rest/api/account/token";
            String queryString = "grant_type=authorization_code&access_token=" + accessToken + "&code_verifier=" + codeVerifier;

            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Bearer " + token);

            String response = requestClient.requestPost(url, queryString, MediaType.APPLICATION_FORM_URLENCODED_TYPE, MediaType.APPLICATION_JSON_TYPE, headers);
            log.info("getUserAuthorizationCode. client_id: [" + clientId + "] - userToken: [" + accessToken + "] - codeVerifier: [" + codeVerifier + "]. Respuesta de PG: " + response);

            PGMessageResponse rm = null;
            try {
                rm = mapper.readValue(response, PGMessageResponse.class);
            } catch (Exception e) {
                log.error("Error al procesar respuesta de PG. client_id: [" + clientId + "] - userToken: [" + accessToken + "] - codeVerifier: [" + codeVerifier + "]. Response: [" + response + "]. " + e, e);
            }

            return rm;
        } catch (Exception e) {
            throw new IllegalStateException("Error al obtener el perfil de usuario para el client_id: [" + clientId + "] - userToken: [" + accessToken + "] - codeVerifier: [" + codeVerifier + "].", e);
        }
    }

    public PGMessageResponse getStatus(String clientId, String userToken) {
        try {
            String token = getClientToken(clientId);
            String url = genericPropertyService.getValueByKey("pg.server.url") + "/rest/api/wallet/payment/subscriptions/status";
            String json = "{\"userToken\":\"" + userToken + "\"}";

            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "Bearer " + token);

            String response = requestClient.requestJSONPost(url, json, headers);
            log.info("getFunds. ClientId: [" + clientId + "] - userToken: [" + userToken + "]. Respuesta de PG: " + response);

            PGMessageResponse rm = null;
            try {
                rm = mapper.readValue(response, PGMessageResponse.class);
            } catch (Exception e) {
                log.error("Error al procesar respuesta de PG: ClientId: [" + clientId + "] - userToken: [" + userToken + "]. Response: [" + response + "]. " + e, e);
            }

            return rm;
        } catch (Exception e) {
            throw new IllegalStateException("Error al obtener creditos del usuario para el client_id: [" + clientId + "] - userToken: [" + userToken + "].", e);
        }
    }

}
