/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgc.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage;
import com.itbaf.platform.messaging.rest.oauth.ResponseAuthMessage;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageRequest;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgc.api.resources.PGResources;
import java.util.LinkedHashMap;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ApiUserServiceProcessorHandler {

    private final PGResources pgResources;

    @Inject
    public ApiUserServiceProcessorHandler(final PGResources pgResources) {
        this.pgResources = Validate.notNull(pgResources, "A PGResources class must be provided");
    }

    public ResponseAuthMessage getUserToken(RequestTokenMessage tm) {

        ResponseAuthMessage rm = pgResources.getUserToken(tm.clientId, tm.code, tm.codeVerifier);

        if (rm == null) {
            log.warn("ResponseAuthMessage es nulo");
            rm = new ResponseAuthMessage();
            rm.error = ResponseAuthMessage.Error.unauthorized_client;
        }
        return rm;
    }

    public PGMessageResponse getProfile(PGMessageRequest messageRequest) {

        PGMessageResponse rm = pgResources.getUserProfile(messageRequest.clientId, messageRequest.userToken);
        if (rm == null) {
            log.warn("PGMessageResponse es nulo");
            rm = new PGMessageResponse();
            rm.meta.code = 400;
            rm.meta.status = Meta.Status.unauthorized_client;
            rm.data.message = "Peticion no autorizada";
        }
        //Para no enviar informacion del cliente a la APP
        try {
            LinkedHashMap<String, Object> aux = (LinkedHashMap<String, Object>) rm.data.object;
            aux.remove("clientData");
        } catch (Exception ex) {
        }

        return rm;
    }

    public PGMessageResponse getFunds(PGMessageRequest messageRequest) {
        PGMessageResponse rm = pgResources.getFunds(messageRequest.clientId, messageRequest.userToken);

        if (rm == null) {
            log.warn("PGMessageResponse es nulo");
            rm = new PGMessageResponse();
            rm.meta.code = 400;
            rm.meta.status = Meta.Status.unauthorized_client;
            rm.data.message = "Peticion no autorizada";
        }

        return rm;
    }

    public PGMessageResponse getStatus(PGMessageRequest messageRequest) {
        PGMessageResponse rm = pgResources.getStatus(messageRequest.clientId, messageRequest.userToken);

        if (rm == null) {
            log.warn("PGMessageResponse es nulo");
            rm = new PGMessageResponse();
            rm.meta.code = 400;
            rm.meta.status = Meta.Status.unauthorized_client;
            rm.data.message = "Unauthorized request";
        }

        return rm;
    }

}
