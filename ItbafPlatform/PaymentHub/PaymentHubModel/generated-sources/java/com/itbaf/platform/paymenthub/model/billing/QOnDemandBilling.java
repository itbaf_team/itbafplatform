package com.itbaf.platform.paymenthub.model.billing;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QOnDemandBilling is a Querydsl query type for OnDemandBilling
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOnDemandBilling extends EntityPathBase<OnDemandBilling> {

    private static final long serialVersionUID = -167687306L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QOnDemandBilling onDemandBilling = new QOnDemandBilling("onDemandBilling");

    public final DateTimePath<java.util.Date> chargedDate = createDateTime("chargedDate", java.util.Date.class);

    public final StringPath currency = createString("currency");

    public final NumberPath<java.math.BigDecimal> fullAmount = createNumber("fullAmount", java.math.BigDecimal.class);

    public final StringPath id = createString("id");

    public final NumberPath<java.math.BigDecimal> netAmount = createNumber("netAmount", java.math.BigDecimal.class);

    protected com.itbaf.platform.paymenthub.model.QSOP sop;

    public final StringPath transactionId = createString("transactionId");

    public final StringPath userAccount = createString("userAccount");

    public QOnDemandBilling(String variable) {
        this(OnDemandBilling.class, forVariable(variable), INITS);
    }

    public QOnDemandBilling(Path<? extends OnDemandBilling> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QOnDemandBilling(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QOnDemandBilling(PathMetadata metadata, PathInits inits) {
        this(OnDemandBilling.class, metadata, inits);
    }

    public QOnDemandBilling(Class<? extends OnDemandBilling> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sop = inits.isInitialized("sop") ? new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"), inits.get("sop")) : null;
    }

    public com.itbaf.platform.paymenthub.model.QSOP sop() {
        if (sop == null) {
            sop = new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"));
        }
        return sop;
    }

}

