/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adn.resources;

import java.math.BigDecimal;

/**
 *
 * @author JF
 */
public class AdnetworkNotificationType {

    /**
     * @pends Cualquier estado de suscripcion (solo PENDING, ACTIVE, BLACKLIST, LOCKED).
     * Hay que tener en cuenta que no todas las PENDING terminan el proceso de
     * suscripcion en la TELCO (hay un proceso que 40 minutos despues sincroniza
     * esa informacion) por lo que se notificara a la ADN de todas formas.
     *
     * @subs Cuando la suscripcion está confirmada (ACTIVE).
     *
     * @subsbill Cuando el suscripcion esta confirmada y hubo un primer cobro.
     */
    public String subscription;

    /**
     * Bill percentage. Cuando hubo un primer cobro (@subsbill) y ese cobro
     * supera un porcentaje. Hay que tener cuidado con notificaciones que
     * retornan el valor sin impuestos, el cual es menor al 100%
     *
     * @billPerce va desde 100 a 1
     *
     */
    public BigDecimal billPerce;

    /**
     * Tiempo en dias en el que se hizo la ultima alta para considerar valida la
     * suscripcion y hacer el envio de la notificacion a la ADN.
     */
    public Integer lastSubs;

    /**
     * Tiempo de confirmacion a la Adnetwork. En milisegundos
     */
    public Long ttl = 86400000L;

}
