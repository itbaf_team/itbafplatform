/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.bo.web;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.services.service.TestService;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class BoWebMain implements TestService {

    private final ExecutorService cachedThreadPool;
    private final ScheduledExecutorService scheduledThreadPool;

    @Inject
    public BoWebMain(final ExecutorService cachedThreadPool,
            final ScheduledExecutorService scheduledThreadPool) {
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "An ExecutorService class must be provided");
        this.scheduledThreadPool = Validate.notNull(scheduledThreadPool, "A ScheduledExecutorService class must be provided");
    }

    public void start() {
        log.info("Initializing app... PaymentHubBN");
        log.info("Initializing app OK. PaymentHubBN");
    }

    public void stop() {
        stopThreads();
        log.info("App destroyed. PaymentHubBN");
    }

    public void stopThreads() {
        log.info("Finalizando Threads en 5 segundos... ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }
        try {
            log.info("Finalizando scheduledThreadPool ");
            scheduledThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            log.info("Esperando Threads por 2 segundos... ");
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        try {
            scheduledThreadPool.shutdownNow();
            log.info("Finalizado scheduledThreadPool ");
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdownNow();
        } catch (Exception ex) {
        }
    }

    @Override
    public List getAll() {
        List<Provider> pList = new ArrayList();
        pList.add(new Provider());
        pList.add(new Provider());
        pList.add(new Provider());
        return pList;
    }

}
