
package org.csapi.schema.parlayx.sendpin.v1_0.local;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.csapi.schema.parlayx.sendpin.v1_0.local package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConfirmPIN_QNAME = new QName("http://www.csapi.org/schema/parlayx/sendpin/v1_0/local", "confirmPIN");
    private final static QName _ConfirmPINResponse_QNAME = new QName("http://www.csapi.org/schema/parlayx/sendpin/v1_0/local", "confirmPINResponse");
    private final static QName _SendPINResponse_QNAME = new QName("http://www.csapi.org/schema/parlayx/sendpin/v1_0/local", "sendPINResponse");
    private final static QName _SendPIN_QNAME = new QName("http://www.csapi.org/schema/parlayx/sendpin/v1_0/local", "sendPIN");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.csapi.schema.parlayx.sendpin.v1_0.local
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConfirmPIN }
     * 
     */
    public ConfirmPIN createConfirmPIN() {
        return new ConfirmPIN();
    }

    /**
     * Create an instance of {@link ConfirmPINResponse }
     * 
     */
    public ConfirmPINResponse createConfirmPINResponse() {
        return new ConfirmPINResponse();
    }

    /**
     * Create an instance of {@link SendPINResponse }
     * 
     */
    public SendPINResponse createSendPINResponse() {
        return new SendPINResponse();
    }

    /**
     * Create an instance of {@link SendPIN }
     * 
     */
    public SendPIN createSendPIN() {
        return new SendPIN();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmPIN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/sendpin/v1_0/local", name = "confirmPIN")
    public JAXBElement<ConfirmPIN> createConfirmPIN(ConfirmPIN value) {
        return new JAXBElement<ConfirmPIN>(_ConfirmPIN_QNAME, ConfirmPIN.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmPINResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/sendpin/v1_0/local", name = "confirmPINResponse")
    public JAXBElement<ConfirmPINResponse> createConfirmPINResponse(ConfirmPINResponse value) {
        return new JAXBElement<ConfirmPINResponse>(_ConfirmPINResponse_QNAME, ConfirmPINResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendPINResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/sendpin/v1_0/local", name = "sendPINResponse")
    public JAXBElement<SendPINResponse> createSendPINResponse(SendPINResponse value) {
        return new JAXBElement<SendPINResponse>(_SendPINResponse_QNAME, SendPINResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendPIN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.csapi.org/schema/parlayx/sendpin/v1_0/local", name = "sendPIN")
    public JAXBElement<SendPIN> createSendPIN(SendPIN value) {
        return new JAXBElement<SendPIN>(_SendPIN_QNAME, SendPIN.class, null, value);
    }

}
