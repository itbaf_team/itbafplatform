/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.extention.timwe.PaymentHubMain;
import com.itbaf.platform.paymenthub.extention.timwe.resources.TimweResources;
import com.itbaf.platform.paymenthub.extention.timwe.resources.TimweSMSRequest;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.SmsSopMultipleService;
import com.itbaf.platform.paymenthub.services.repository.SmsSopService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.ws.Holder;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class SMSMessageRenewHandler implements RabbitMQMessageProcessor {

    private final ObjectMapper mapper;
    private final SopService sopService;
    private final TimweResources timweResources;
    private final RabbitMQProducer rabbitMQProducer;
    private final InstrumentedObject instrumentedObject;
    private final SubscriptionManager subscriptionManager;
    private final SubscriptionService subscriptionService;
    private final SmsSopService smsSopService;
    private final SmsSopMultipleService smsSopMultipleService;
    private static final String ENCRYPTION_ALGORITH = "AES/ECB/PKCS5Padding";

    @Inject
    public SMSMessageRenewHandler(final ObjectMapper mapper,
                                  final SopService sopService,
                                  final TimweResources timweResources,
                                  final RabbitMQProducer rabbitMQProducer,
                                  final InstrumentedObject instrumentedObject,
                                  final SubscriptionManager subscriptionManager,
                                  final SubscriptionService subscriptionService,
                                  final SmsSopMultipleService smsSopMultipleService,
                                  final SmsSopService smsSopService){
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.sopService = Validate.notNull(sopService, "A SopService class must be provided");
        this.timweResources = Validate.notNull(timweResources, "A TimweResources class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
        this.subscriptionService = Validate.notNull(subscriptionService, "A SubscriptionService class must be provided");
        this.smsSopService = Validate.notNull(smsSopService,"A smsSopService class must be provided");
        this.smsSopMultipleService = Validate.notNull(smsSopMultipleService,"A smsSopMultipleService class must be provided");
    }

    @Override
    public Boolean processMessage(String message) {
        RLock lock = instrumentedObject.lockObject("RENEW_FIND_" + message, 60);
        try {
            Subscription s;
            //String provider_name;
            String sms;
            Boolean b = Boolean.FALSE;
            Boolean isMultipleSms = Boolean.FALSE;

            ///////armado del msfw///////////////////
            Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            long milis1 = cal1.getTimeInMillis();
            String product_id = null;
            String ani = null;
            String msfw = "";
            ////////////////////////////////////////

            try {
                s = mapper.readValue(message, Subscription.class);
                log.info("SMSMessageRenewHandler.processMessage: Buscando: subscription por id [" + s.getId() + "]");
                s = subscriptionService.findById(s.getId());
                if (!s.getStatus().equals(Subscription.Status.ACTIVE)) {
                    log.info("processMessage: No se envía mensaje porque [" + s.getStatus() + "] no es [" + Subscription.Status.ACTIVE + "]");
                    return false;
                }
            } catch (Exception ex) {
                log.error("Error al procesar el JSON: [" + message + "]. " + ex, ex);
                return null;
            }

            ///////armado del msfw///////////////////
            product_id = s.getSop().getIntegrationSettings().get("product_id");
            ani = s.getUserAccount();
            msfw = generateMsfw(msfw.concat("#").concat(ani).concat("#").concat(product_id).concat("#").concat(Long.toString(milis1)),"pwoINKDgMKGciTPj");
            ////////////////////////////////////////

            long aux = instrumentedObject.getAtomicLong("RENEW_FIND_A_" + s.getId(), 200);
            log.info("aux= " + aux);
            if (aux == 0) {
                Holder<Subscription.Status> status = new Holder();

                isMultipleSms = smsSopMultipleService.isMultipleMessage(s.getSop().getId());
                log.info("Value isMultipleSms: " + isMultipleSms+" ani: "+ani+" sop: "+s.getSop().getId());

                if(isMultipleSms){

                    sms = smsSopService.getSms(s.getSop().getId(),"renewal").concat("?msfw=").concat(msfw);
                }
                else{
                    sms = s.getSop().getIntegrationSettings().get("sms.message.week.renew").concat("?msfw=").concat(msfw);
                }

                //sms = s.getSop().getIntegrationSettings().get("sms.message.week.renew").concat("?msfw=").concat(msfw);
                log.info("valor sms: " + sms + " msisdn " +s.getUserAccount());

                b = timweResources.sendSMS(s.getUserAccount(), s.getSop(),
                        TimweSMSRequest.Context.STATELESS, TimweSMSRequest.Priority.HIGH,
                        sms, status);

                if (b == null) {
                    log.error("No fue posible enviar el MT recordatorio a: [" + message + "]");
                    return b;
                } else {
                    log.info("Respuesta de timweResources.sendSMS [" + b + "] estado [" + status + "]");
                    instrumentedObject.getAndIncrementAtomicLong("RENEW_FIND_A_" + s.getId(), 200);
                    instrumentedObject.getAndIncrementAtomicLong("RENEW_FIND_A_" + s.getId(), 200);
                    if (status.value != null) {
                        try {
                            switch (status.value) {
                                case BLACKLIST:
                                    s.setStatus(Subscription.Status.BLACKLIST);
                                    break;
                                case CANCELLED:
                                    s.setStatus(Subscription.Status.CANCELLED);
                                    break;
                                default:
                                    log.fatal("No es posible procesar el error de envio de sms con status: [" + status.value + "]");
                                    return null;
                            }
                            s.setUnsubscriptionDate(new Date());
                            s.getSubscriptionRegistry().setUnsubscriptionDate(new Date());
                            s.getSubscriptionRegistry().setOriginUnsubscription("SMS_MT");
                        } catch (Exception ex) {
                            log.error("Error al procesar baja de Timwe por envio de MT. " + ex, ex);
                        }
                    }
                }
            }

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                s.getSubscriptionRegistry().getExtra().stringData.put("lastMTSent", sdf.format(new Date()));
                subscriptionManager.subscriptionProcess(s);
                return true;
            } catch (Exception ex) {
                log.fatal("No fue posible actualizar la fecha de envio del MT en DB de: [" + message + "]");
            }

            return null;
        } finally {
            instrumentedObject.unLockObject(lock);
        }
    }

    public void findActiveSMSAttempt(String providerName) {
        Date d = new Date();
        if (d.getHours() < 11 || d.getHours() > 23) {
            return;
        }

        Map<String, String> settings = new HashMap();
        settings.put("frequencyType", "week");
        List<SOP> sops = sopService.findAllBySettings(providerName, settings);

        List<BigInteger> subscriptions = new ArrayList();
        for (SOP sop : sops) {
            try {
                 List<BigInteger> ss = subscriptionService.getSubscriptionsMidRenewTwiceWeek(sop);
                if (ss != null && !ss.isEmpty()) {
                    subscriptions.addAll(ss);
                    log.info("SMS renews: [Twice week][" + sop.getId() + "][" + ss.size() + "][" + ss.get(0) + "]");
                }
            } catch (Exception ex) {
                log.error("No se pudo obtener listado getSubscriptionsMidRenewTwiceWeek SOP [" + sop.getId() + "]", ex);
            }
        }
        for (BigInteger bi : subscriptions) {
            sendSMSQueueProcess(providerName, bi);
        }
    }

    private void sendSMSQueueProcess(String providerName, BigInteger sr) {
        Subscription s = new Subscription();
        s.setId(sr.longValue());
        try {
            String json = mapper.writeValueAsString(s);
            rabbitMQProducer.messageSender(PaymentHubMain.SMS_MESSAGE_REVEW + providerName, json);
        } catch (Exception ex) {
            log.error("No fue posible enviar el SMS de renovacion a la cola. [" + sr + "]. " + ex, ex);
        }
    }

    private String generateMsfw(String valueToCrypt, String preSharedKey){
        String cryptedValue = null;
        String encode_value = null;
        if (valueToCrypt != null && !valueToCrypt.isEmpty()) {
            Cipher cipher;
            try {
                cipher = Cipher.getInstance(ENCRYPTION_ALGORITH);
                SecretKeySpec k = new SecretKeySpec(preSharedKey.getBytes(), "AES");
                cipher.init(Cipher.ENCRYPT_MODE, k);
                final byte[] decrypted = cipher.doFinal(valueToCrypt.getBytes());
                cryptedValue = new String(decrypted);
                encode_value = Base64.getEncoder().encodeToString(cryptedValue.getBytes());
                return encode_value;
            } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
