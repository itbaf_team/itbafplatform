/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository;

import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.pgp.model.wallet.PaymentMethod;
import com.itbaf.platform.services.service.TestService;
import java.util.List;

/**
 *
 * @author javier
 */
public interface PaymentMethodService extends TestService<PaymentMethod>{

    public PaymentMethod findById(Long id) throws Exception;

   // public List<PaymentMethod> getAll() throws Exception;

    public List<PaymentMethod> findByCountry(Country country) throws Exception;
}
