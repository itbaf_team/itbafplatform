package com.itbaf.platform.paymenthub.extention.store.model.subscription;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum Channel {

    @XmlEnumValue("0")
    HTML(0),
    @XmlEnumValue("1")
    SAT(1),
    @XmlEnumValue("2")
    WEB(2),
    @XmlEnumValue("3")
    SMS(3),
    @XmlEnumValue("6")
    USSD(6),
    @XmlEnumValue("7")
    IVR(7);

    private final Integer value;

    Channel(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    public static Channel fromName(String name) {
        for (Channel channel : Channel.values()) {
            if (channel.name().equals(name)) {
                return channel;
            }
        }

        throw new IllegalArgumentException();
    }

    public static Channel fromValue(Integer value) {
        for (Channel channel : Channel.values()) {
            if (channel.getValue().equals(value)) {
                return channel;
            }
        }

        throw new IllegalArgumentException();
    }
}
