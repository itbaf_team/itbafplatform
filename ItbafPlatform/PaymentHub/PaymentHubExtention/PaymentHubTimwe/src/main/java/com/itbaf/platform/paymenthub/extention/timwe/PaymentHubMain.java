/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import static com.itbaf.platform.commons.CommonFunction.BORDER_NOTIFICATION;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import com.itbaf.platform.commons.resources.InstrumentedRunnable;
import com.itbaf.platform.paymenthub.commons.PaymentHubBasic;
import com.itbaf.platform.paymenthub.extention.timwe.handler.SMSMessageRenewHandler;
import com.itbaf.platform.paymenthub.extention.timwe.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.model.Provider;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain extends PaymentHubBasic {

    private final SMSMessageRenewHandler smsMessageRenewHandler;
    private final Map<String, ServiceProcessorHandler> serviceProcessorHandlerMap;
    public static final String SMS_MESSAGE_REVEW = "sms.message.renew.";

    @Inject
    public PaymentHubMain(final SMSMessageRenewHandler smsMessageRenewHandler,
            final Map<String, ServiceProcessorHandler> serviceProcessorHandlerMap) {
        this.smsMessageRenewHandler = Validate.notNull(smsMessageRenewHandler, "A SMSMessageRenewHandler class must be provided");
        this.serviceProcessorHandlerMap = Validate.notNull(serviceProcessorHandlerMap, "A Map<String, ServiceProcessorHandler> class must be provided");
    }

    @Override
    protected void startActions() {
        try {
            rabbitMQConsumer.createFanoutChannel(CHACHE_MEMORY_UPDATE, memoryProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer '" + CHACHE_MEMORY_UPDATE + "'. " + ex, ex);
        }

        try {
            for (Provider p : providerSet) {
                ServiceProcessorHandler sph = serviceProcessorHandlerMap.get(p.getName());
                try {
                    rabbitMQConsumer.createChannel("sync.subscriptions." + p.getName(), 1, rabbitMQMessageProcessor);
                    rabbitMQConsumer.createChannel("sync.subscription." + p.getName(), 1, rabbitMQMessageProcessor);
                } catch (Exception ex) {
                    log.error("Error al crear consumer SYNC para provider: [" + p + "]. " + ex, ex);
                }

                try {
                    rabbitMQConsumer.createChannel(SMS_MESSAGE_REVEW + p.getName(), 1, smsMessageRenewHandler);
                } catch (Exception ex) {
                    log.error("Error al crear consumer de renovacion para provider: [" + p + "]. " + ex, ex);
                }

                try {
                    String name = NOTIFICATION_RECEIVED_NAME + p.getName();
                    rabbitMQConsumer.createChannel(name, sph.getChannelQuantityProvider(p).getSubscriptionConsumerQuantity(), sph);
                } catch (Exception ex) {
                    log.error("Error al crear consumer NOTIFICATION.RECEIVE para provider: [" + p + "]. " + ex, ex);
                }

                try {
                    int q = sph.getChannelQuantityProvider(p).getSubscriptionConsumerQuantity();
                    String name = BORDER_NOTIFICATION + p.getName();
                    rabbitMQConsumer.createChannel(name, q, bnProcessor);
                } catch (Exception ex) {
                    log.error("Error al crear consumer BORDER_NOTIFICATION para provider: [" + p + "]. " + ex, ex);
                }
            }
        } catch (Exception ex) {
        }

        scheduledThreadPool.scheduleWithFixedDelay(new InstrumentedRunnable("renew.find", "pe.entel.timwe", instrumentedObject) {
            @Override
            public boolean exec() {
                if (isStop) {
                    return false;
                }
                Thread.currentThread().setName(CommonFunction.getTID("renew.find." + "pe.entel.timwe"));
                RLock lock = instrumentedObject.lockObject("RENEW_FIND", 60);
                long aux = instrumentedObject.getAtomicLong("RENEW_FIND_A", 90);
                if (aux == 0) {
                    instrumentedObject.getAndIncrementAtomicLong("RENEW_FIND_A", 90);
                    instrumentedObject.getAndIncrementAtomicLong("RENEW_FIND_A", 90);
                    instrumentedObject.getAndIncrementAtomicLong("RENEW_FIND_A", 90);
                    try {
                        smsMessageRenewHandler.findActiveSMSAttempt("pe.entel.timwe");
                        instrumentedObject.unLockObject(lock);
                        return true;
                    } catch (Exception ex) {
                        log.error("Error scheduledThreadPool timwe", ex);
                    }
                }
                instrumentedObject.unLockObject(lock);
                return false;
            }
        }, 1, 120, TimeUnit.MINUTES);
    }
}
