/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.stats.model;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author JF
 */
public class BaseResumenElastic extends ElasticData implements Serializable {

    private static final long serialVersionUID = 20190116145101L;

    public enum FunctionType {
        ALTA, BAJA, COBRO, SUSCRIPCION
    }

    public BaseResumenElastic() {
        super(ElasticData.ElasticType.base_resumen);
    }

    public Long subscriptionId;
    public Long subscriptionRegistryId;
    public Date fecha;
    public FunctionType functionType;
    public SubscriptionRegistryElastic subscriptionRegistryElastic;
    public SubscriptionBillingElastic subscriptionBillingElastic;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("subscriptionId", subscriptionId)
                .add("subscriptionRegistryId", subscriptionRegistryId)
                .add("fecha", fecha)
                .add("functionType", functionType)
                .add("subscriptionRegistryElastic", subscriptionRegistryElastic)
                .add("subscriptionBillingElastic", subscriptionBillingElastic)
                .omitNullValues().toString();
    }
}
