/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events.etl.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class EtlQuery {

    public Long id;
    public String type;
    public String query;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("type", type)
                .add("query", query)
                .omitNullValues().toString();
    }
}
