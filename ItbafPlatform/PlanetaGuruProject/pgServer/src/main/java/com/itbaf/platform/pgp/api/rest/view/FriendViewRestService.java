package com.itbaf.platform.pgp.api.rest.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.pgp.api.handler.AccountViewServiceProcessorHandler;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import io.jsonwebtoken.impl.DefaultClaims;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/friends")
@Singleton
@lombok.extern.log4j.Log4j2
public class FriendViewRestService {

    private final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler;

    @Inject
    public FriendViewRestService(final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler) {
        this.accountViewServiceProcessorHandler = Validate.notNull(accountViewServiceProcessorHandler, "An AccountViewServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public Response friends(@Context HttpServletRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.friends");
        } catch (Exception ex) {
        }
        log.info("friends. [" + request.getQueryString() + "]");

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            String productId = request.getAttribute(CommonAttribute.PRODUCT_ID).toString();
            if (client != null && user != null) {
                String redirectUri = request.getAttribute(CommonAttribute.U_REDIRECT_URI).toString();
                String html = accountViewServiceProcessorHandler.getUserAccountHtml(client, user, productId, redirectUri);
                if (html != null) {
                    String prvdr = null;
                    try {
                        DefaultClaims dc = (DefaultClaims) request.getAttribute(CommonAttribute.U_DEFAULT_CLAIMS);
                        prvdr = dc.get("prvdr", String.class);
                    } catch (Exception ex) {
                    }
                    html = accountViewServiceProcessorHandler.i18n(user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage(), prvdr, html);
                    return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar peticion. " + ex, ex);
        }

        return Response.status(Status.UNAUTHORIZED).build();
    }

}
