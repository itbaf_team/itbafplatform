/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.model;

import java.math.BigDecimal;

/**
 *
 * @author JF
 */
public class WalletTransactionView {

    public enum Type {
        SUBSCRIPTION, ONDEMAND, BONUS_REFUNDS, PURCHASE;

        public static String getI8Nfrom(Type type) {
            String r = "";
            switch (type) {
                case SUBSCRIPTION:
                    r = "<i18n>i18n.ix.px.text.subscription</i18n>";
                    break;
                case ONDEMAND:
                    r = "<i18n>i18n.ix.px.text.ondemand</i18n>";
                    break;
                case BONUS_REFUNDS:
                    r = "<i18n>i18n.ix.px.text.bonus_refunds</i18n>";
                    break;
                case PURCHASE:
                    r = "<i18n>i18n.ix.px.text.purchase</i18n>";
                    break;
            }
            return r;
        }
    }

    public String tid;
    public String date;
    public String typeI8N;
    public Type type;
    public BigDecimal localAmount;
    public String localCurrency;
    public BigDecimal amount;
    public String currency;
    public String symbol;
    public String description;
}
