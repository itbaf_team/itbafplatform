package com.itbaf.platform.paymenthub.messaging.smpp.smppadapter;

import com.google.common.base.MoreObjects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="destinationAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="originationAddr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="deliveryReceipt" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dcsUcs2" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "destinationAddr",
    "originationAddr",
    "deliveryReceipt",
    "dcsUcs2",
    "message"
})
@XmlRootElement(name = "SendSmsRequest")
public class SendSmsRequest {

    protected long id;
    @XmlElement(required = true)
    protected String destinationAddr;
    @XmlElement(required = true)
    protected String originationAddr;
    @XmlElement(defaultValue = "0")
    protected Integer deliveryReceipt;
    @XmlElement(defaultValue = "false")
    protected Boolean dcsUcs2;
    @XmlElement(required = true)
    protected String message;

    /**
     * Gets the value of the id property.
     *
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Gets the value of the destinationAddr property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDestinationAddr() {
        return destinationAddr;
    }

    /**
     * Sets the value of the destinationAddr property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setDestinationAddr(String value) {
        this.destinationAddr = value;
    }

    /**
     * Gets the value of the originationAddr property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getOriginationAddr() {
        return originationAddr;
    }

    /**
     * Sets the value of the originationAddr property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setOriginationAddr(String value) {
        this.originationAddr = value;
    }

    /**
     * Gets the value of the deliveryReceipt property.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getDeliveryReceipt() {
        return deliveryReceipt;
    }

    /**
     * Sets the value of the deliveryReceipt property.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setDeliveryReceipt(Integer value) {
        this.deliveryReceipt = value;
    }

    /**
     * Gets the value of the dcsUcs2 property.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isDcsUcs2() {
        return dcsUcs2;
    }

    /**
     * Sets the value of the dcsUcs2 property.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setDcsUcs2(Boolean value) {
        this.dcsUcs2 = value;
    }

    /**
     * Gets the value of the message property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setMessage(String value) {
        this.message = value;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("destinationAddr", destinationAddr)
                .add("originationAddr", originationAddr)
                .add("deliveryReceipt", deliveryReceipt)
                .add("dcsUcs2", dcsUcs2)
                .add("message", message)
                .omitNullValues().toString();
    }
}
