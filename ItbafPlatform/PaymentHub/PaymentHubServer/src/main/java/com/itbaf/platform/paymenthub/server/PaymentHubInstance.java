/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.server;

import com.google.inject.Injector;
import com.itbaf.platform.paymenthub.commons.rest.ManagerRestServiceImpl;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.servlets.ServletServer;
import com.itbaf.platform.paymenthub.server.modules.GuiceConfigModule;

/**
 *
 * @author jordonez
 */
public class PaymentHubInstance extends CommonDaemon {

    private PaymentHubMain paymentHubMain;
    private ServletServer servletServer;
    private static PaymentHubInstance serverDaemon;

    public static void main(String args[]) throws Exception {
        PaymentHubInstance.getInstance().init(args);
        PaymentHubInstance.getInstance().start();
    }

    public synchronized static PaymentHubInstance getInstance() {
        if (serverDaemon == null) {
            serverDaemon = new PaymentHubInstance();
            ManagerRestServiceImpl.paymentHubDaemon = serverDaemon;
        }
        return serverDaemon;
    }

    @Override
    public void start() throws Exception {
        servletServer = new ServletServer(new GuiceConfigModule(args, this));
        final Injector injector = servletServer.getInject();
        paymentHubMain = injector.getInstance(PaymentHubMain.class);
        paymentHubMain.start();
        servletServer.startServer();
    }

    @Override
    public void stop() throws Exception {
        try {
            servletServer.stopServer();
        } catch (Exception ex) {
        }
        try {
            Thread.sleep(100);
        } catch (Exception ex) {
        }
        try {
            paymentHubMain.stop();
        } catch (Exception ex) {
        }
    }

}
