package com.itbaf.platform.paymenthub.server.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.model.FunnelLogMessage;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.paymenthub.commons.resources.FacadeCache;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.sync.NormalizerService;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.server.exception.LockTimeoutException;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.ProviderService;
import io.jsonwebtoken.lang.Collections;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

@Path("/facade")
@Singleton
@lombok.extern.log4j.Log4j2
public class SubscriptionFacadeRestService extends NormalizerService {

    private final ObjectMapper mapper;
    private final FacadeCache facadeCache;
    private final RequestClient requestClient;
    private final ProviderService providerService;
    private final ExecutorService cachedThreadPool;
    private final InstrumentedObject instrumentedObject;
    private final SubscriptionManager subscriptionManager;
    private final PHProfilePropertiesService profilePropertiesService;

    @Inject
    public SubscriptionFacadeRestService(final ObjectMapper mapper,
            final PHProfilePropertiesService profilePropertiesService,
            final SubscriptionManager subscriptionManager,
            final RequestClient requestClient, final FacadeCache facadeCache,
            final InstrumentedObject instrumentedObject,
            final ProviderService providerService, final ExecutorService cachedThreadPool) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.facadeCache = Validate.notNull(facadeCache, "A FacadeCache class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.providerService = Validate.notNull(providerService, "A ProviderService class must be provided");
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "A ExecutorService class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    private Subscription getSubscription(FacadeRequest request, ResponseMessage rm, boolean isnSubscription) throws Exception {
        Subscription s = null;
        SOP sop;
        try {
            sop = subscriptionManager.getSOP(request.service, request.operator, request.provider);
            if (sop == null) {
                log.error("No hay un SOP definido. Request: [" + request + "]");
                return s;
            }
        } catch (Exception ex) {
            log.error("Error al obtener el SOP. Request: [" + request + "]. " + ex, ex);
            return s;
        }

        Subscription aux = new Subscription();
        aux.setSop(sop);
        aux.setUserAccount(request.userAccount);

        /*  String normalizer = aux.getSop().getIntegrationSettings().get("request.useraccount.normalizer");
        if ("true".equals(normalizer)) {
            String url = profilePropertiesService.getValueByProviderKey(aux.getSop().getProvider().getId(), "internal.service.url") + "subscription/normalizer";
            String response = null;
            try {
                response = requestClient.requestJSONPostFacade(url, mapper.writeValueAsString(aux));
                // log.info(aux.getSop().getProvider().getName() + " response: [" + response + "]");
                ResponseMessage rsp = mapper.readValue(response, ResponseMessage.class);

                if (rsp.status == ResponseMessage.Status.ERROR || rsp.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = rsp.status;
                    rm.message = rsp.message;
                    return null;
                }
                aux = mapper.readValue((String) rsp.data, Subscription.class);
            } catch (Exception ex) {
                rm.message = "Error al procesar response. [" + response + "]. " + ex;
                log.error(rm.message, ex);
                rm.status = ResponseMessage.Status.ERROR;
                return null;
            }
        } else {*/
        try {
            aux.setUserAccount(userAccountNormalizer(aux.getUserAccount(), sop.getProvider()));
        } catch (Exception ex) {
            /* rm.status = ResponseMessage.Status.ERROR_403;
                rm.message = ex.getMessage();
                return null;*/
        }
        //  }
        boolean control = false;
        Long sid = null;
        
        log.info("isnSubscription: "+isnSubscription + ", request.provider: "+request.provider);
        log.info("Busqueda de provider:" +("ar.claro.smt".equals(request.provider)));
        
        if (isnSubscription || "pe.claro.spiralis".equals(request.provider) || "ec.claro.smt".equals(request.provider) || "ar.claro.smt".equals(request.provider)) {
            s = subscriptionManager.getSubscription(request.externalId == null ? aux.getUserAccount() : request.externalId,
                    aux.getUserAccount(), aux.getExternalUserAccount() == null ? aux.getUserAccount() : aux.getExternalUserAccount(),
                    aux.getExternalCustomer() == null ? aux.getUserAccount() : aux.getExternalCustomer(), sop);
        }
        if (s != null) {
            control = true;
            sid = s.getId();
            aux.setExternalUserAccount(s.getExternalUserAccount());
            aux.setExternalCustomer(s.getExternalCustomer());
        }

        s = null;
        String externalId = request.externalId;
        try {
            if (externalId == null && aux.getSubscriptionRegistry() != null) {
                externalId = aux.getSubscriptionRegistry().getExternalId();
            }
        } catch (Exception ex) {
        }

        List<RLock> lock = null;
        Long startLock = System.currentTimeMillis();
        try {
            lock = instrumentedObject.lockSubscription(externalId, aux.getUserAccount(),
                    aux.getExternalUserAccount(), aux.getExternalCustomer(), sop.getId());
        } catch (Exception ex) {
            log.error("Error al sincronizar facade. " + ex, ex);
        }
        Long endLock = System.currentTimeMillis() - startLock;
        if (endLock > 60000) {
            String auxx = "Request finalizado con endLock: [" + endLock + "]ms.";
            throw new LockTimeoutException(auxx);
        }
        if (control) {
            s = subscriptionManager.getSubscription(sid);
        }
        if (s == null) {
            s = new Subscription();
            s.setUserAccount(aux.getUserAccount());
            s.setExternalUserAccount(aux.getExternalUserAccount());
            s.setStatus(Subscription.Status.REMOVED);
            s.setSop(sop);
        }
        s.setObject(request.pin);
        s.setFacadeRequest(request);
        s.setLock(lock);
        return s;
    }

    private Subscription preSubscription(String method, Subscription s, FacadeRequest request) {
        if (Subscription.Status.ACTIVE.equals(s.getStatus()) || Subscription.Status.PENDING.equals(s.getStatus())) {
            log.warn("La suscripcion [" + s + "] ya tiene un estado valido!!");
            s.getSubscriptionRegistry().setOriginSubscription(s.getSubscriptionRegistry().getOriginSubscription() + ";FACADE:" + method);
        } else {
            s.setStatus(Subscription.Status.PENDING);
            s.setSubscriptionDate(new Date());
            SubscriptionRegistry sr = new SubscriptionRegistry();
            sr.setSubscription(s);
            sr.setExternalId(request.externalId);
            sr.setOriginSubscription("FACADE:" + method);
            sr.setSop(s.getSop());
            sr.setSubscriptionDate(s.getSubscriptionDate());
            sr.setUserAccount(s.getUserAccount());
            sr.setChannelIn(request.channel);
            s.setSubscriptionRegistry(sr);
        }
        s.setUnsubscriptionDate(null);
        s.setUserAccount(s.getUserAccount());
        s.setObject(request.pin);
        if (s.getSubscriptionRegistry().getAdnetworkTracking() == null && request.adTracking != null) {
            s.getSubscriptionRegistry().setAdnetworkTracking(request.adTracking);
        }
        s.setBackUrl(request.contentUrl);
        return s;
    }

    @PUT
    @Path("/subscription")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putSubscription(FacadeRequest request) {

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.s");
        } catch (Exception ex) {
        }

        // log.info("Subscription. Facade request: [" + request + "]");
        log.info(new FunnelLogMessage("Subscription. Facade request", "/rest/facade/subscription", request));
        ResponseMessage rm = new ResponseMessage();

        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
        try {
            Subscription s;
            try {
                s = getSubscription(request, rm, false);
            } catch (LockTimeoutException ex) {
                rm.message = ex.getMessage();
                log.info("Facade response: [" + rm + "]");
                return Response.status(504).entity(rm).type(MediaType.APPLICATION_JSON).build();
            } catch (Exception ex) {
                rm.message = ex.getMessage();
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }
            if (s == null) {
                rm.message = "Error al obtener suscripcion. " + (rm.message == null ? "" : rm.message);
                if (rm.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = ResponseMessage.Status.ERROR;
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }
            if (Subscription.Status.ACTIVE.equals(s.getStatus()) || Subscription.Status.PENDING.equals(s.getStatus())) {
                if (Subscription.Status.ACTIVE.equals(s.getStatus())) {
                    rm.status = ResponseMessage.Status.ACTIVE;
                } else {
                    rm.status = ResponseMessage.Status.PENDING;
                }
                rm.message = "La suscripcion ya tiene un estado valido.";
                log.info("Facade response: [" + rm + "]");
                try {
                    if (s.getLock() != null) {
                        instrumentedObject.unLockSubscription((List<RLock>) s.getLock());
                    }
                } catch (Exception ex) {
                }
                return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }
            s = preSubscription("PUT_SUBSCRIPTION", s, request);
            List<RLock> lock = (List<RLock>) s.getLock();
            s.setLock(null);
            if (s.getSubscriptionRegistry().getId() != null && request.externalId != null
                    && request.externalId.equals(s.getSubscriptionRegistry().getExternalId())) {
                // log.info("Se esta realizando una suscripcion de un usuario con igual externalId. Request: [" + request + "] .  Subscription: [" + s + "]");
                log.warn("Se reemplaza el adn: [" + s.getSubscriptionRegistry().getAdnetworkTracking() + "] por: [" + request.adTracking + "]");
                s.getSubscriptionRegistry().setAdnetworkTracking(request.adTracking);
                subscriptionManager.subscriptionProcess(s);
                rm.status = ResponseMessage.Status.OK;
                rm.message = "Subscription actualizada con externalId: [" + request.externalId + "]";
                // log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            String url = profilePropertiesService.getValueByProviderKey(s.getSop().getProvider().getId(), "internal.service.url") + "subscribe";
            String response;
            ResponseMessage rsp;
            try {
                response = requestClient.requestJSONPostFacade(url, mapper.writeValueAsString(s));
                // log.info(s.getSop().getProvider().getName() + " response: [" + response + "]");
                if (response == null) {
                    rm.message = "Error en un proceso externo. ";
                    rm.status = ResponseMessage.Status.ERROR;
                    instrumentedObject.unLockSubscription(lock);
                    //  log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                rsp = mapper.readValue(response, ResponseMessage.class);
                if (rsp.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = rsp.message;
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                } else if (rsp.status == ResponseMessage.Status.ERROR) {
                    log.info("Facade response: [" + rm + "]");
                    rm.message = rsp.message;
                    rm.status = ResponseMessage.Status.ERROR;
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                s = mapper.readValue((String) rsp.data, Subscription.class);
            } catch (Exception ex) {
                rm.message = "Error al interpretar el JSON. " + ex;
                log.error(rm.message, ex);
                rm.status = ResponseMessage.Status.ERROR;
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            s.getSubscriptionRegistry().setSubscription(s);

            switch (rsp.status) {
                case OK:
                    //Alta realizada satisfactoriamente: Pending, Active
                    //El estado de la suscripcion debe ser seteado remotamente
                    try {
                        if (request.externalId == null) {
                            request.externalId = s.getSubscriptionRegistry().getExternalId();
                        }
                        facadeCache.setFacadeRequest(request.externalId, s.getUserAccount(),
                                s.getExternalUserAccount(), s.getSop().getId(), request);
                    } catch (Exception ex) {
                        log.error("Error al setFacadeRequestCache. " + ex, ex);
                    }
                    rm.status = ResponseMessage.Status.OK;
                    if (s.getExternalUrl() != null) {
                        Map<Object, Object> resp = new HashMap();
                        resp.put("externalUrl", s.getExternalUrl());
                        rm.data = resp;
                    }
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case OK_NS:
                    try {
                        if (request.externalId == null) {
                            request.externalId = s.getSubscriptionRegistry().getExternalId();
                        }
                        facadeCache.setFacadeRequest(request.externalId, s.getUserAccount(),
                                s.getExternalUserAccount(), s.getSop().getId(), request);
                    } catch (Exception ex) {
                        log.error("Error al setFacadeRequestCache. " + ex, ex);
                    }
                    rm.status = ResponseMessage.Status.OK;
                    rm.message = rsp.message;
                    if (s.getExternalUrl() != null) {
                        Map<Object, Object> resp = new HashMap();
                        resp.put("provider", s.getSop().getProvider().getName());
                        resp.put("externalUrl", s.getExternalUrl());
                        rm.data = resp;
                    }
                    break;
                case PENDING:
                    //El alta esta como pendiente en el proveedor
                    rm.status = ResponseMessage.Status.PENDING;
                    rm.message = "El usuario ya tiene un proceso de suscripcion pendiente en el proveedor";
                    s.setStatus(Subscription.Status.PENDING);
                    s.getSubscriptionRegistry().setAdnetworkTracking(null);
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case ACTIVE:
                    //El alta ya existia en el Proveedor
                    rm.status = ResponseMessage.Status.ACTIVE;
                    rm.message = "El usuario ya existia en el proveedor";
                    s.getSubscriptionRegistry().setAdnetworkTracking(null);
                    s.setStatus(Subscription.Status.ACTIVE);
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case BLACKLIST:
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    rm.message = rsp.message;
                    s.setStatus(Subscription.Status.BLACKLIST);
                    s.getSubscriptionRegistry().setOriginSubscription(s.getSubscriptionRegistry().getOriginSubscription() + ";PROVIDER:BLACKLIST");
                    s.getSubscriptionRegistry().setOriginUnsubscription("FACADE:SUBSCRIPTION:BLACKLIST");
                    s.getSubscriptionRegistry().setChannelOut("FACADE");
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case LOCKED:
                    rm.status = ResponseMessage.Status.LOCKED;
                    rm.message = rsp.message;
                    s.setStatus(Subscription.Status.LOCKED);
                    s.setUnsubscriptionDate(new Date());
                    s.getSubscriptionRegistry().setUnsubscriptionDate(s.getUnsubscriptionDate());
                    s.getSubscriptionRegistry().setOriginSubscription(s.getSubscriptionRegistry().getOriginSubscription() + ";PROVIDER:LOCKED");
                    s.getSubscriptionRegistry().setOriginUnsubscription("FACADE:SUBSCRIPTION:LOCKED");
                    s.getSubscriptionRegistry().setChannelOut("FACADE");
                    subscriptionManager.subscriptionProcess(s);
                    break;
                default:
                    rm.status = rsp.status;
                    rm.message = rsp.message;
                    break;
            }
            instrumentedObject.unLockSubscription(lock);
            //  log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            rm.message = "Error al procesar request: [" + request + "]. " + ex;
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @POST
    @Path("/subscription/pin/send")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendPin(FacadeRequest request) {

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.ps");
        } catch (Exception ex) {
        }

        //log.info("SendPin. Facade request: [" + request + "]");
        log.info(new FunnelLogMessage("SendPin. Facade request", "/rest/facade/subscription/pin/send", request));
        ResponseMessage rm = new ResponseMessage();

        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            //   log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
        try {
            Subscription s;
            try {
                s = getSubscription(request, rm, true);
                log.info("get subscription: "+s);
            } catch (LockTimeoutException ex) {
                rm.message = ex.getMessage();
                log.info("Facade response: [" + rm + "]");
                return Response.status(504).entity(rm).type(MediaType.APPLICATION_JSON).build();
            } catch (Exception ex) {
                rm.message = ex.getMessage();
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }
            if (s == null) {
                rm.message = "Error al obtener suscripcion. " + (rm.message == null ? "" : rm.message);
                if (rm.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = ResponseMessage.Status.ERROR;
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            s = preSubscription("SEND_PIN", s, request);
            List<RLock> lock = (List<RLock>) s.getLock();
            s.setLock(null);
            String url = profilePropertiesService.getValueByProviderKey(s.getSop().getProvider().getId(), "internal.service.url") + "subscribe/pin/send";
            String response;
            ResponseMessage rsp;
            try {
                s.setObject(request.channel);
                response = requestClient.requestJSONPostFacade(url, mapper.writeValueAsString(s));
                //  log.info(s.getSop().getProvider().getName() + " response: [" + response + "]");
                if (response == null) {
                    rm.message = "Error en un proceso externo. ";
                    rm.status = ResponseMessage.Status.ERROR;
                    instrumentedObject.unLockSubscription(lock);
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                rsp = mapper.readValue(response, ResponseMessage.class);
                if (rsp.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = rsp.message;
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                } else if (rsp.status == ResponseMessage.Status.ERROR) {
                    log.info("Facade response: [" + rm + "]");
                    rm.message = rsp.message;
                    rm.status = ResponseMessage.Status.ERROR;
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                s = mapper.readValue((String) rsp.data, Subscription.class);
            } catch (Exception ex) {
                rm.message = "Error al interpretar el JSON. " + ex;
                log.error(rm.message, ex);
                rm.status = ResponseMessage.Status.ERROR;
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            s.getSubscriptionRegistry().setSubscription(s);

            switch (rsp.status) {
                case OK:
                    //Envio de PIN satisfactorio
                    try {
                        if (request.externalId == null) {
                            request.externalId = s.getSubscriptionRegistry().getExternalId();
                        }
                        facadeCache.setFacadeRequest(request.externalId, s.getUserAccount(),
                                s.getExternalUserAccount(), s.getSop().getId(), request);
                    } catch (Exception ex) {
                        log.error("Error al setFacadeRequestCache. " + ex, ex);
                    }
                    rm.status = ResponseMessage.Status.OK;
                    //   log.info("Se ha enviodo el PIN a: [" + s.getUserAccount() + "]");
                    break;
                case PENDING:
                    //El alta esta como pendiente en el proveedor
                    rm.status = ResponseMessage.Status.PENDING;
                    rm.message = "El usuario ya tiene un proceso de suscripcion pendiente en el proveedor";
                    s.setStatus(Subscription.Status.PENDING);
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case ACTIVE:
                    //El alta ya existia en el Proveedor
                    rm.status = ResponseMessage.Status.ACTIVE;
                    rm.message = "El usuario ya existia en el proveedor";
                    s.getSubscriptionRegistry().setAdnetworkTracking(null);
                    s.setStatus(Subscription.Status.ACTIVE);
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case BLACKLIST:
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    rm.message = rsp.message;
                    s.setStatus(Subscription.Status.BLACKLIST);
                    s.getSubscriptionRegistry().setOriginSubscription(s.getSubscriptionRegistry().getOriginSubscription() + ";PROVIDER:BLACKLIST");
                    s.getSubscriptionRegistry().setOriginUnsubscription("FACADE:SEND_PIN:BLACKLIST");
                    s.getSubscriptionRegistry().setChannelOut("FACADE");
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case LOCKED:
                    rm.status = ResponseMessage.Status.LOCKED;
                    rm.message = rsp.message;
                    s.setStatus(Subscription.Status.LOCKED);
                    s.setUnsubscriptionDate(new Date());
                    s.getSubscriptionRegistry().setUnsubscriptionDate(s.getUnsubscriptionDate());
                    s.getSubscriptionRegistry().setOriginSubscription(s.getSubscriptionRegistry().getOriginSubscription() + ";PROVIDER:LOCKED");
                    s.getSubscriptionRegistry().setOriginUnsubscription("FACADE:SEND_PIN:LOCKED");
                    s.getSubscriptionRegistry().setChannelOut("FACADE");
                    subscriptionManager.subscriptionProcess(s);
                    break;
                default:
                    rm.status = rsp.status;
                    rm.message = rsp.message;
                    break;
            }
            instrumentedObject.unLockSubscription(lock);
            // log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            rm.message = "Error al procesar request: [" + request + "]. " + ex;
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @POST
    @Path("/subscription/pin/validate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validatePin(FacadeRequest request) {

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.pv");
        } catch (Exception ex) {
        }

        //log.info("ValidatePin. Facade request: [" + request + "]");
        log.info(new FunnelLogMessage("ValidatePin. Facade request", "/rest/facade/subscription/pin/validate", request));
        ResponseMessage rm = new ResponseMessage();

        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        if (request.pin == null || request.pin.length() == 0) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Error con el parametro PIN. ";
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } else if ("undefined".equals(request.pin.toLowerCase())) {
            rm.status = ResponseMessage.Status.PIN_ERROR;
            rm.message = "No se esta validando el PIN: [undefined]";
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
        try {
            Subscription s;
            try {
                s = getSubscription(request, rm, true);
            } catch (LockTimeoutException ex) {
                rm.message = ex.getMessage();
                log.info("Facade response: [" + rm + "]");
                return Response.status(504).entity(rm).type(MediaType.APPLICATION_JSON).build();
            } catch (Exception ex) {
                rm.message = ex.getMessage();
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }
            if (s == null) {
                rm.message = "Error al obtener suscripcion. " + (rm.message == null ? "" : rm.message);
                if (rm.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = ResponseMessage.Status.ERROR;
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            s = preSubscription("VALIDATE_PIN", s, request);
            List<RLock> lock = (List<RLock>) s.getLock();
            s.setLock(null);
            String url = profilePropertiesService.getValueByProviderKey(s.getSop().getProvider().getId(), "internal.service.url") + "subscribe/pin/validate?pin=" + request.pin;
            String response = null;
            ResponseMessage rsp = null;
            try {
                response = requestClient.requestJSONPostFacade(url, mapper.writeValueAsString(s));
                log.info(s.getSop().getProvider().getName() + " response: [" + response + "]");
                if (response == null) {
                    rm.message = "Error en un proceso externo. ";
                    rm.status = ResponseMessage.Status.ERROR;
                    instrumentedObject.unLockSubscription(lock);
                    //   log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                rsp = mapper.readValue(response, ResponseMessage.class);
                if (rsp.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = rsp.message;
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                } else if (rsp.status == ResponseMessage.Status.ERROR) {
                    log.info("Facade response: [" + rm + "]");
                    rm.message = rsp.message;
                    rm.status = ResponseMessage.Status.ERROR;
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                s = mapper.readValue((String) rsp.data, Subscription.class);
            } catch (Exception ex) {
                rm.message = "Error al interpretar el JSON. " + ex;
                log.error(rm.message, ex);
                rm.status = ResponseMessage.Status.ERROR;
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            s.getSubscriptionRegistry().setSubscription(s);
            switch (rsp.status) {
                case OK:
                    try {
                        if (request.externalId == null) {
                            request.externalId = s.getSubscriptionRegistry().getExternalId();
                        }
                        facadeCache.setFacadeRequest(request.externalId, s.getUserAccount(),
                                s.getExternalUserAccount(), s.getSop().getId(), request);
                    } catch (Exception ex) {
                        log.error("Error al setFacadeRequestCache. " + ex, ex);
                    }
                    //PIN valido
                    rm.status = ResponseMessage.Status.OK;
                    rm.message = rsp.message;
                    log.info("Se ha validado el PIN de: [" + s.getUserAccount() + "]");
                    s.setStatus(Subscription.Status.PENDING);
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case OK_NS:
                    //PIN valido pero no se suscribe
                    try {
                        if (request.externalId == null) {
                            request.externalId = s.getSubscriptionRegistry().getExternalId();
                        }
                        facadeCache.setFacadeRequest(request.externalId, s.getUserAccount(),
                                s.getExternalUserAccount(), s.getSop().getId(), request);
                    } catch (Exception ex) {
                        log.error("Error al setFacadeRequestCache. " + ex, ex);
                    }
                    rm.status = ResponseMessage.Status.OK;
                    rm.message = rsp.message;
                    log.info("Se ha validado el PIN de: [" + s.getUserAccount() + "]");
                    break;
                case PIN_ERROR:
                    rm.status = rsp.status;
                    rm.message = rsp.message;
                    break;
                case PENDING:
                    //El alta esta como pendiente en el proveedor
                    rm.status = ResponseMessage.Status.PENDING;
                    rm.message = "El usuario ya tiene un proceso de suscripcion pendiente en el proveedor";
                    s.setStatus(Subscription.Status.PENDING);
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case ACTIVE:
                    //El alta ya existia en el Proveedor
                    rm.status = ResponseMessage.Status.ACTIVE;
                    rm.message = "El usuario ya existia en el proveedor";
                    s.getSubscriptionRegistry().setAdnetworkTracking(null);
                    s.setStatus(Subscription.Status.ACTIVE);
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case BLACKLIST:
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    rm.message = rsp.message;
                    s.setStatus(Subscription.Status.BLACKLIST);
                    s.getSubscriptionRegistry().setOriginSubscription(s.getSubscriptionRegistry().getOriginSubscription() + ";PROVIDER:BLACKLIST");
                    s.getSubscriptionRegistry().setOriginUnsubscription("FACADE:VALIDATE_PIN:BLACKLIST");
                    s.getSubscriptionRegistry().setChannelOut("FACADE");
                    subscriptionManager.subscriptionProcess(s);
                    break;
                case LOCKED:
                    rm.status = ResponseMessage.Status.LOCKED;
                    rm.message = rsp.message;
                    s.setStatus(Subscription.Status.LOCKED);
                    s.setUnsubscriptionDate(new Date());
                    s.getSubscriptionRegistry().setUnsubscriptionDate(s.getUnsubscriptionDate());
                    s.getSubscriptionRegistry().setOriginSubscription(s.getSubscriptionRegistry().getOriginSubscription() + ";PROVIDER:LOCKED");
                    s.getSubscriptionRegistry().setOriginUnsubscription("FACADE:VALIDATE_PIN:LOCKED");
                    s.getSubscriptionRegistry().setChannelOut("FACADE");
                    subscriptionManager.subscriptionProcess(s);
                    break;
                default:
                    rm.status = rsp.status;
                    rm.message = rsp.message;
                    break;
            }
            instrumentedObject.unLockSubscription(lock);
            //    log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            rm.message = "Error al procesar request: [" + request + "]. " + ex;
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    /**
     * @param request.
     * @return
     */
    @POST
    @Path("/subscription/status")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptionStatus(FacadeRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.status");
        } catch (Exception ex) {
        }
        try {
            return subscriptionStatus(request);
        } catch (Exception ex) {
            ResponseMessage rm = new ResponseMessage();
            rm.message = "Error al procesar request: [" + request + "]. " + ex;
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR;
            //    log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    private Response subscriptionStatus(FacadeRequest request) {

        log.info("SubscriptionStatus. Facade request: [" + request + "]");
        ResponseMessage rm = new ResponseMessage();
        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        Subscription s;
        Subscription aux = null;
        try {
            String trackingCache = facadeCache.getTemporalData(request.provider + "_" + request.userAccount);
            if (trackingCache != null) {
                request.userAccount = trackingCache;
            }
        } catch (Exception ex) {
        }
        try {
            s = getSubscription(request, rm, true);
        } catch (LockTimeoutException ex) {
            rm.message = ex.getMessage();
            log.info("Facade response: [" + rm + "]");
            return Response.status(504).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            rm.message = ex.getMessage();
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
        if (rm.status == ResponseMessage.Status.ERROR_403) {
            rm.status = ResponseMessage.Status.ERROR;
            //   log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
        List<RLock> lock = (List<RLock>) s.getLock();
        s.setLock(null);

        log.info("Subscripcion status [" + s + "]");

        switch (s.getStatus()) {
            case BLACKLIST:
                if (s.getSubscriptionRegistry().getSyncDate() != null && (System.currentTimeMillis() - s.getSubscriptionRegistry().getSyncDate().getTime()) < 604800000) {
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    break;
                }
            case ACTIVE:
            case PENDING:
            case LOCKED:
            case CANCELLED:
            case REMOVED:
                String url = profilePropertiesService.getValueByProviderKey(s.getSop().getProvider().getId(), "internal.service.url") + "subscription/status";
                String response;
                ResponseMessage rsp;
                aux = s;
                try {
                    String jsonSubs;
                    if (request.customerId != null && request.accountId != null
                            && (s.getExternalCustomer() == null
                            || s.getExternalUserAccount() == null || s.getExternalCustomer().equals(s.getExternalCustomer()))) {
                        String oc = s.getExternalCustomer();
                        String oa = s.getExternalUserAccount();
                        s.setExternalCustomer(request.customerId);
                        s.setExternalUserAccount(request.accountId);
                        jsonSubs = mapper.writeValueAsString(s);
                        s.setExternalCustomer(oc);
                        s.setExternalUserAccount(oa);
                    } else {
                        jsonSubs = mapper.writeValueAsString(s);
                    }

                    response = requestClient.requestJSONPostFacade(url, jsonSubs);
                    //   log.info(s.getSop().getProvider().getName() + " response: [" + response + "]");
                    if (response == null) {
                        rm.message = "Error en un proceso externo. ";
                        rm.status = ResponseMessage.Status.ERROR;
                        instrumentedObject.unLockSubscription(lock);
                        log.info("Facade response: [" + rm + "]");
                        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                    }
                    rsp = mapper.readValue(response, ResponseMessage.class);
                    if (rsp.status == ResponseMessage.Status.ERROR_403) {
                        rm.status = ResponseMessage.Status.ERROR;
                        rm.message = rsp.message;
                        log.info("Facade response: [" + rm + "]");
                        return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                    } else if (rsp.status == ResponseMessage.Status.ERROR) {
                        log.info("Facade response: [" + rm + "]");
                        rm.message = rsp.message;
                        rm.status = ResponseMessage.Status.ERROR;
                        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                    }
                    s = mapper.readValue((String) rsp.data, Subscription.class);
                } catch (Exception ex) {
                    rm.message = "Error al interpretar el JSON. " + ex;
                    log.error(rm.message, ex);
                    rm.status = ResponseMessage.Status.ERROR;
                    instrumentedObject.unLockSubscription(lock);
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                boolean it = false;

                switch (rsp.status) {
                    case ACTIVE:
                        it = true;
                        s.setStatus(Subscription.Status.ACTIVE);
                        break;
                    case PENDING:
                        it = true;
                        s.setStatus(Subscription.Status.PENDING);
                        break;
                    case BLACKLIST:
                        it = true;
                        s.setStatus(Subscription.Status.BLACKLIST);
                        break;
                    case LOCKED:
                        it = true;
                        s.setStatus(Subscription.Status.LOCKED);
                        break;
                    case REMOVED:
                        it = true;
                        s.setStatus(Subscription.Status.REMOVED);
                        break;
                    case OK_NS:
                        Subscription xx = new Subscription();
                        xx.setStatus(s.getStatus());
                        xx.setUserAccount(s.getUserAccount());
                        xx.setExternalUserAccount(s.getExternalUserAccount());
                        xx.setExternalCustomer(s.getExternalCustomer());
                        xx.setThread(null);
                        if (s.getSubscriptionRegistry() != null) {
                            xx.getSubscriptionRegistry().setId(s.getSubscriptionRegistry().getId());
                            xx.setSubscriptionRegistry(new SubscriptionRegistry());
                            if ("SubscriptionRegistry".equals(request.action)) {
                                xx.setSubscriptionDate(s.getSubscriptionDate());
                                xx.setUnsubscriptionDate(s.getUnsubscriptionDate());
                                xx.getSubscriptionRegistry().setSop(s.getSop());
                            } else {
                                xx.getSubscriptionRegistry().setExternalId(s.getSubscriptionRegistry().getExternalId());
                                xx.getSubscriptionRegistry().setSop(new SOP());
                                xx.getSubscriptionRegistry().getSop().setOperator(s.getSop().getOperator());
                                xx.getSubscriptionRegistry().getSop().setServiceMatcher(s.getSop().getServiceMatcher());
                                xx.getSubscriptionRegistry().getSop().setProvider(s.getSop().getProvider());
                            }
                        }
                        switch (s.getStatus()) {
                            case ACTIVE:
                                rm.status = ResponseMessage.Status.ACTIVE;
                                break;
                            case BLACKLIST:
                                rm.status = ResponseMessage.Status.BLACKLIST;
                                break;
                            case REMOVED:
                            case CANCELLED:
                                rm.status = ResponseMessage.Status.REMOVED;
                                break;
                            case LOCKED:
                                rm.status = ResponseMessage.Status.LOCKED;
                                break;
                            case PENDING:
                                rm.status = ResponseMessage.Status.PENDING;
                                break;
                        }
                        rm.data = xx;
                        lastChargedProcess(xx);
                        instrumentedObject.unLockSubscription(lock);
                        //   log.info("Facade response: [" + rm + "]");
                        return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }

                if (it && !aux.getStatus().equals(s.getStatus())) {
                    if (request.externalId == null && s.getSubscriptionRegistry() != null) {
                        if (aux.getSubscriptionRegistry() == null) {
                            request.externalId = s.getSubscriptionRegistry().getExternalId();
                        } else if (s.getSubscriptionRegistry().getExternalId() != null
                                && !s.getSubscriptionRegistry().getExternalId().equals(aux.getSubscriptionRegistry().getExternalId())) {
                            request.externalId = s.getSubscriptionRegistry().getExternalId();
                        } else if (aux.getSubscriptionRegistry() != null && aux.getSubscriptionRegistry().getExternalId() != null
                                && s.getSubscriptionRegistry() != null
                                && aux.getSubscriptionRegistry().getExternalId().equals(s.getSubscriptionRegistry().getExternalId())) {
                            if (request.externalId != null) {
                                log.fatal("Sustituyendo request.externalId: [" + request.externalId + "] por s.SubscriptionRegistry.ExternalId: [" + s.getSubscriptionRegistry().getExternalId() + "]");
                            }
                            request.externalId = s.getSubscriptionRegistry().getExternalId();
                        }
                    }
                    switch (s.getStatus()) {
                        case ACTIVE:
                            if (aux.getSubscriptionRegistry() == null && s.getSubscriptionRegistry() != null) {
                                //  log.info("Se conserva la informacion del ModuloOperador. [" + s.getSubscriptionRegistry() + "]");
                                aux.setSubscriptionRegistry(s.getSubscriptionRegistry());
                                if (s.getSubscriptionRegistry().getOriginSubscription() == null) {
                                    s.getSubscriptionRegistry().setOriginSubscription("FACADE:STATUS");
                                }
                            } else if (!Subscription.Status.PENDING.equals(aux.getStatus()) || s.getSubscriptionRegistry() == null) {
                                s.setStatus(Subscription.Status.CANCELLED);
                                s = this.preSubscription("STATUS", s, request);
                                aux.setSubscriptionRegistry(s.getSubscriptionRegistry());
                            }
                            s.setStatus(Subscription.Status.ACTIVE);
                            break;
                        case PENDING:
                            if (aux.getSubscriptionRegistry() == null && s.getSubscriptionRegistry() != null) {
                                // log.info("Se conserva la informacion del ModuloOperador. [" + s.getSubscriptionRegistry() + "]");
                                aux.setSubscriptionRegistry(s.getSubscriptionRegistry());
                                if (s.getSubscriptionRegistry().getOriginSubscription() == null) {
                                    s.getSubscriptionRegistry().setOriginSubscription("FACADE:STATUS");
                                }
                            } else if (!Subscription.Status.ACTIVE.equals(aux.getStatus()) || s.getSubscriptionRegistry() == null) {
                                s.setStatus(Subscription.Status.CANCELLED);
                                s = this.preSubscription("STATUS", s, request);
                            }
                            aux.setSubscriptionRegistry(s.getSubscriptionRegistry());
                            s.setStatus(Subscription.Status.PENDING);
                            break;
                        case BLACKLIST:
                            if (s.getSubscriptionRegistry() == null) {
                                s.setStatus(Subscription.Status.CANCELLED);
                                s = this.preSubscription("STATUS", s, request);
                            }
                            if (s.getSubscriptionRegistry().getOriginUnsubscription() == null) {
                                s.getSubscriptionRegistry().setOriginUnsubscription("FACADE:STATUS");
                            }
                            aux.setSubscriptionRegistry(s.getSubscriptionRegistry());
                            s.setStatus(Subscription.Status.BLACKLIST);
                            break;
                        case LOCKED:
                            if (s.getSubscriptionRegistry() == null) {
                                s.setStatus(Subscription.Status.CANCELLED);
                                s = this.preSubscription("STATUS", s, request);
                            }
                            if (s.getSubscriptionRegistry().getOriginUnsubscription() == null) {
                                s.getSubscriptionRegistry().setOriginUnsubscription("FACADE:STATUS");
                                s.setUnsubscriptionDate(new Date());
                                s.getSubscriptionRegistry().setUnsubscriptionDate(s.getUnsubscriptionDate());
                            }
                            aux.setSubscriptionRegistry(s.getSubscriptionRegistry());
                            s.setStatus(Subscription.Status.LOCKED);
                            break;
                        case CANCELLED:
                        case REMOVED:
                        case PORTING:
                            if (s.getSubscriptionRegistry() != null && s.getSubscriptionRegistry().getOriginUnsubscription() == null) {
                                s.getSubscriptionRegistry().setOriginUnsubscription("FACADE:STATUS");
                            }
                            aux.setSubscriptionRegistry(s.getSubscriptionRegistry());
                            break;
                    }
                    if (aux.getSubscriptionRegistry() != null) {
                        subscriptionManager.subscriptionProcess(s);
                        //  log.info("Actualizado STATUS de: [" + s + "]");
                    }
                } else if (it && (Subscription.Status.BLACKLIST.equals(s.getStatus())
                        || Subscription.Status.LOCKED.equals(s.getStatus())) || Subscription.Status.PORTING.equals(s.getStatus())) {
                    if (aux.getSubscriptionRegistry() != null) {
                        s.getSubscriptionRegistry().setSyncDate(new Date());
                        subscriptionManager.subscriptionProcess(s);
                        //  log.info("Actualizado STATUS de: [" + s + "]");
                    }
                } else if ("force".equals(request.persist)) {
                    if (s.getSubscriptionRegistry() != null && s.getExternalUserAccount() != null) {
                        subscriptionManager.subscriptionProcess(s);
                    }
                } else if (it && aux.getStatus().equals(s.getStatus()) && s.getSubscriptionRegistry() != null
                        && s.getSubscriptionRegistry().getExternalId() != null
                        && (aux.getSubscriptionRegistry() == null || !s.getSubscriptionRegistry().getExternalId().equals(aux.getSubscriptionRegistry().getExternalId()))) {
                    log.warn("Se fuerza la actualizacion de una suscripcion. [" + s + "]");
                    subscriptionManager.subscriptionProcess(s);
                } else if ((aux.getSubscriptionRegistry() == null || (aux.getSubscriptionRegistry() != null && aux.getSubscriptionRegistry().getAdnetworkTracking() == null))
                        && s.getSubscriptionRegistry() != null && s.getSubscriptionRegistry().getAdnetworkTracking() != null) {
                    subscriptionManager.subscriptionProcess(s);
                }
                rm.status = rsp.status;
                rm.message = rsp.message;
                break;
        }

        Subscription xx = new Subscription();
        xx.setStatus(s.getStatus());
        xx.setUserAccount(s.getUserAccount());
        xx.setExternalUserAccount(s.getExternalUserAccount());
        xx.setExternalCustomer(s.getExternalCustomer());
        xx.setThread(null);
        if (s.getSubscriptionRegistry() != null) {
            xx.setSubscriptionRegistry(new SubscriptionRegistry());
            xx.getSubscriptionRegistry().setId(s.getSubscriptionRegistry().getId());
            if ("SubscriptionRegistry".equals(request.action)) {
                //La peticion viene de PG.Server
                xx.setSubscriptionDate(s.getSubscriptionDate());
                xx.setUnsubscriptionDate(s.getUnsubscriptionDate());
                if (xx.getSubscriptionRegistry().getId() == null && aux != null && aux.getSubscriptionRegistry() != null) {
                    xx.getSubscriptionRegistry().setId(aux.getSubscriptionRegistry().getId());
                }
                xx.getSubscriptionRegistry().setSop(s.getSop());
            } else {
                xx.getSubscriptionRegistry().setExternalId(s.getSubscriptionRegistry().getExternalId());
                xx.getSubscriptionRegistry().setSop(new SOP());
                xx.getSubscriptionRegistry().getSop().setOperator(s.getSop().getOperator());
                xx.getSubscriptionRegistry().getSop().setServiceMatcher(s.getSop().getServiceMatcher());
                xx.getSubscriptionRegistry().getSop().setProvider(s.getSop().getProvider());
            }
        }

        lastChargedProcess(xx);
        rm.data = xx;
        instrumentedObject.unLockSubscription(lock);
        //   log.info("Facade response: [" + rm + "]");
        return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/subscription/status/country/list")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptionStatusCountryList(final FacadeRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.status.country.list");
        } catch (Exception ex) {
        }
        log.info("getSubscriptionStatusCountryList. Facade request: [" + request + "]");

        ResponseMessage rm = new ResponseMessage();
        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response error: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        if (request.dataString == null || request.dataString.isEmpty()) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Alguno de los parametros es erroneo. dataString: [" + request.dataString + "]";
            log.info("Facade response error: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        try {
            for (String msisdn : request.dataString.keySet()) {
                FacadeRequest fr = new FacadeRequest();
                fr.userAccount = msisdn;
                fr.country = request.dataString.get(msisdn);
                getSubscriptionStatusCountry(fr);
            }
        } catch (Exception ex) {
        }

        rm.status = ResponseMessage.Status.OK;
        log.info("Facade response ok: [" + rm + "]");
        return Response.status(Response.Status.ACCEPTED).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/subscription/status/{path}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptionStatus(@PathParam("path") final String path,
            final FacadeRequest request) {

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.status." + path);
        } catch (Exception ex) {
        }
        try {
            switch (path) {
                case "service":
                    return getSubscriptionStatusService(request);
                case "information":
                    return getSubscriptionStatusInformation(request);
                case "country":
                    return getSubscriptionStatusCountry(request);
                case "notify":
                    final String threadName = Thread.currentThread().getName();
                    cachedThreadPool.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.currentThread().setName(threadName + ".T");
                            } catch (Exception ex) {
                            }
                            if (request.urlNotify == null) {
                                log.warn("No hay una urlNotify en el request. [" + request + "]");
                                return;
                            }

                            ResponseMessage rm = getSubscriptionStatusNotify(request);
                            //   log.info("Facade response: [" + rm + "]");

                            try {
                                String json = mapper.writeValueAsString(rm);
                                //  log.info("Facade response JSON: [" + json + "]");
                                String response = requestClient.requestJSONPostFacade(request.urlNotify, json);
                                //  log.info("Respuesta desde: [" + request.urlNotify + "] - Response: [" + response + "]");
                            } catch (Exception ex) {
                                log.error("Error al notificar a: [" + request + "]. " + ex, ex);
                            }
                        }
                    });

                    ResponseMessage rm = new ResponseMessage();
                    rm.status = ResponseMessage.Status.OK;
                    //  log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.ACCEPTED).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }
            ResponseMessage rm = new ResponseMessage();
            rm.status = ResponseMessage.Status.ERROR;
            //    log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            ResponseMessage rm = new ResponseMessage();
            rm.message = "Error al procesar request: [" + request + "]. " + ex;
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    private ResponseMessage getSubscriptionStatusNotify(FacadeRequest request) {
        log.info("SubscriptionStatusService. Facade request: [" + request + "]");

        ResponseMessage rm = new ResponseMessage();

        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            return rm;
        }

        Provider p = subscriptionManager.getProviderByName(request.provider);
        if (p == null) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "No existe un provider con nombre: [" + request.provider + "]";
            return rm;
        }

        try {
            List<SOP> sopList = subscriptionManager.getSOPsByService(request.serviceCode, p);

            if (sopList == null || sopList.isEmpty()) {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "No hay un SOP configurado para [" + request.serviceCode + "] - Provider: [" + p + "]";
                return rm;
            }

            if (request.service != null) {
                List<SOP> aux = new ArrayList();
                for (SOP sop : sopList) {
                    if (sop.getServiceMatcher().getName().equalsIgnoreCase(request.service)) {
                        aux.add(sop);
                    }
                }
                sopList = aux;
            }
            if (request.operator != null) {
                List<SOP> aux = new ArrayList();
                for (SOP sop : sopList) {
                    if (sop.getOperator().getName().equalsIgnoreCase(request.operator)) {
                        aux.add(sop);
                    }
                }
                sopList = aux;
            }
            if (request.country != null) {
                List<SOP> aux = new ArrayList();
                for (SOP sop : sopList) {
                    if (sop.getProvider().getCountry().getCode().equalsIgnoreCase(request.country)) {
                        aux.add(sop);
                    }
                }
                sopList = aux;
            }

            ResponseMessage rmAux;
            Response r = null;
            List<Object> mlist = new ArrayList();
            ResponseMessage.Status st = ResponseMessage.Status.ERROR;
            for (SOP sop : sopList) {
                FacadeRequest requestAux = new FacadeRequest();
                requestAux.externalId = request.externalId;
                requestAux.internalId = request.internalId;
                requestAux.service = sop.getServiceMatcher().getName();
                requestAux.operator = sop.getOperator().getName();
                requestAux.provider = sop.getProvider().getName();
                requestAux.userAccount = request.userAccount;
                requestAux.action = request.action;
                requestAux.adTracking = request.adTracking;
                requestAux.accountId = request.accountId;
                requestAux.customerId = request.customerId;

                try {
                    r = subscriptionStatus(requestAux);
                    rmAux = (ResponseMessage) r.getEntity();
                    if (ResponseMessage.Status.ACTIVE.equals(rmAux.status)
                            || ResponseMessage.Status.BLACKLIST.equals(rmAux.status)
                            || ResponseMessage.Status.LOCKED.equals(rmAux.status)
                            || ResponseMessage.Status.PENDING.equals(rmAux.status)) {
                        mlist.add(rmAux.data);
                        if (ResponseMessage.Status.ACTIVE.equals(rmAux.status)) {
                            st = ResponseMessage.Status.ACTIVE;
                        } else if (!ResponseMessage.Status.ACTIVE.equals(st)) {
                            st = rmAux.status;
                            rm.message = rmAux.message;
                        }
                    }
                } catch (Exception ex) {
                }
            }
            if (mlist.isEmpty()) {
                try {
                    rmAux = (ResponseMessage) r.getEntity();
                    mlist.add(rmAux.data);
                    st = rmAux.status;
                    rm.message = rmAux.message;
                } catch (Exception ex) {
                }
            }
            rm.status = st;
            if (rm.message == null || (!ResponseMessage.Status.PENDING.equals(rm.status) && !ResponseMessage.Status.LOCKED.equals(rm.status) && !ResponseMessage.Status.ERROR.equals(rm.status))) {
                rm.message = "Success";
            }
            rm.data = mlist;
            return rm;
        } catch (Exception ex) {
            log.error("Error al procesar peticion de status Service. [" + request + "]. " + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = ex.getMessage();
            return rm;
        }
    }

    private Response getSubscriptionStatusService(final FacadeRequest request) {
        log.info("SubscriptionStatusService. Facade request: [" + request + "]");

        ResponseMessage rm = new ResponseMessage();

        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        Provider p = subscriptionManager.getProviderByName(request.provider);
        if (p == null) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "No existe un provider con nombre: [" + request.provider + "]";
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        try {
            List<SOP> sopList = subscriptionManager.getSOPsByService(request.serviceCode, p);
            if (sopList == null || sopList.isEmpty()) {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "No hay un SOP configurado para [" + request.serviceCode + "] - Provider: [" + p + "]";
                //   log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            if (request.service != null) {
                List<SOP> aux = new ArrayList();
                for (SOP sop : sopList) {
                    if (sop.getServiceMatcher().getName().equalsIgnoreCase(request.service)) {
                        aux.add(sop);
                    }
                }
                sopList = aux;
            }
            if (request.operator != null) {
                List<SOP> aux = new ArrayList();
                for (SOP sop : sopList) {
                    if (sop.getOperator().getName().equalsIgnoreCase(request.operator)) {
                        aux.add(sop);
                    }
                }
                sopList = aux;
            }
            if (request.country != null) {
                List<SOP> aux = new ArrayList();
                for (SOP sop : sopList) {
                    if (sop.getProvider().getCountry().getCode().equalsIgnoreCase(request.country)) {
                        aux.add(sop);
                    }
                }
                sopList = aux;
            }

            ResponseMessage rmAux;
            Response r = null;
            List<Object> mlist = new ArrayList();
            ResponseMessage.Status st = ResponseMessage.Status.ERROR;
            for (SOP sop : sopList) {
                FacadeRequest requestAux = new FacadeRequest();
                requestAux.externalId = request.externalId;
                requestAux.internalId = request.internalId;
                requestAux.service = sop.getServiceMatcher().getName();
                requestAux.operator = sop.getOperator().getName();
                requestAux.provider = sop.getProvider().getName();
                requestAux.userAccount = request.userAccount;
                requestAux.action = request.action;
                requestAux.adTracking = request.adTracking;
                requestAux.accountId = request.accountId;
                requestAux.customerId = request.customerId;
                log.info("New Facade requestAux: [" + requestAux + "]");
                try {
                    r = subscriptionStatus(requestAux);
                    log.info("Subscription status: "+r);
                    rmAux = (ResponseMessage) r.getEntity();
                    if (ResponseMessage.Status.ACTIVE.equals(rmAux.status)
                            || ResponseMessage.Status.BLACKLIST.equals(rmAux.status)
                            || ResponseMessage.Status.LOCKED.equals(rmAux.status)
                            || ResponseMessage.Status.PENDING.equals(rmAux.status)) {
                        mlist.add(rmAux.data);
                        log.info("rmAux: "+rmAux);
                        if (ResponseMessage.Status.ACTIVE.equals(rmAux.status)) {
                            st = ResponseMessage.Status.ACTIVE;
                        } else if (!ResponseMessage.Status.ACTIVE.equals(st)) {
                            st = rmAux.status;
                            rm.message = rmAux.message;
                        }
                    }
                } catch (Exception ex) {
                }
            }
            if (mlist.isEmpty()) {
                try {
                    rmAux = (ResponseMessage) r.getEntity();
                    mlist.add(rmAux.data);
                    st = rmAux.status;
                    rm.message = rmAux.message;
                } catch (Exception ex) {
                }
            }
            rm.status = st;
            if (rm.message == null || (!ResponseMessage.Status.PENDING.equals(rm.status) && !ResponseMessage.Status.LOCKED.equals(rm.status) && !ResponseMessage.Status.ERROR.equals(rm.status))) {
                rm.message = "Success";
            }
            rm.data = mlist;
            log.info("Facade response mlist: [" + rm + "]");
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al procesar peticion de status Service. [" + request + "]. " + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = ex.getMessage();
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.GONE).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    private Response getSubscriptionStatusInformation(FacadeRequest request) {

        log.info("SubscriptionStatusInformation. Facade request: [" + request + "]");
        ResponseMessage rm = new ResponseMessage();

        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        if (request.userAccount == null || request.userAccount.length() < 4 || request.country == null) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Alguno de los parametros es erroneo. UserAccount: [" + request.userAccount + "] - Country : [" + request.country + "]";
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        try {
            Subscription s = subscriptionManager.getSubscriptionsByUserAccountAndCountry(request.userAccount, request.country);
            if (s != null) {
                Subscription xx = new Subscription();
                xx.setStatus(s.getStatus());
                xx.setUserAccount(s.getUserAccount());
                xx.setExternalUserAccount(s.getExternalUserAccount());
                xx.setExternalCustomer(s.getExternalCustomer());
                xx.setThread(null);
                if (s.getSubscriptionRegistry() != null) {
                    xx.setSubscriptionRegistry(new SubscriptionRegistry());
                    xx.getSubscriptionRegistry().setId(s.getSubscriptionRegistry().getId());
                    xx.getSubscriptionRegistry().setExternalId(s.getSubscriptionRegistry().getExternalId());
                    xx.getSubscriptionRegistry().setSop(new SOP());
                    xx.getSubscriptionRegistry().getSop().setOperator(s.getSop().getOperator());
                    xx.getSubscriptionRegistry().getSop().setServiceMatcher(s.getSop().getServiceMatcher());
                    xx.getSubscriptionRegistry().getSop().setProvider(s.getSop().getProvider());
                }
                switch (s.getStatus()) {
                    case ACTIVE:
                        rm.status = ResponseMessage.Status.ACTIVE;
                        break;
                    case BLACKLIST:
                        rm.status = ResponseMessage.Status.BLACKLIST;
                        break;
                    case CANCELLED:
                    case REMOVED:
                        rm.status = ResponseMessage.Status.REMOVED;
                        break;
                    case LOCKED:
                        rm.status = ResponseMessage.Status.LOCKED;
                        break;
                    case PENDING:
                        rm.status = ResponseMessage.Status.PENDING;
                        break;
                    case TRIAL:
                        rm.status = ResponseMessage.Status.TRIAL;
                        break;
                }

                rm.data = xx;
                lastChargedProcess(xx);
                //    log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            List<SOP> sopList = subscriptionManager.getSOPsByCountry(request.country);

            if (Collections.isEmpty(sopList)) {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "No hay un SOP configurado para el pais: [" + request.country + "]";
                //     log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            ResponseMessage rmAux = null;
            Response r = null;
            for (SOP sop : sopList) {
                FacadeRequest requestAux = new FacadeRequest();
                requestAux.externalId = request.externalId;
                requestAux.internalId = request.internalId;
                requestAux.service = sop.getServiceMatcher().getName();
                requestAux.operator = sop.getOperator().getName();
                requestAux.provider = sop.getProvider().getName();
                requestAux.userAccount = request.userAccount;
                requestAux.action = request.action;
                requestAux.adTracking = request.adTracking;
                requestAux.accountId = request.accountId;
                requestAux.customerId = request.customerId;

                try {
                    r = subscriptionStatus(requestAux);
                    rmAux = (ResponseMessage) r.getEntity();
                    if (ResponseMessage.Status.ACTIVE.equals(rmAux.status)
                            || ResponseMessage.Status.BLACKLIST.equals(rmAux.status)
                            || ResponseMessage.Status.LOCKED.equals(rmAux.status)
                            || ResponseMessage.Status.PENDING.equals(rmAux.status)) {
                        break;
                    }
                } catch (Exception ex) {
                }
            }

            return r;
        } catch (Exception ex) {
            log.error("Error al procesar peticion de status Service. [" + request + "]. " + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = ex.getMessage();
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.GONE).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    private Response getSubscriptionStatusCountry(FacadeRequest request) {

        log.info("SubscriptionStatusCountry. Facade request: [" + request + "]");
        ResponseMessage rm = new ResponseMessage();

        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        if (request.userAccount == null || request.userAccount.length() < 4 || request.country == null) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Alguno de los parametros es erroneo. UserAccount: [" + request.userAccount + "] - Country : [" + request.country + "] - serviceCode: [" + request.serviceCode + "]";
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        try {
            List<SOP> sopList = subscriptionManager.getSOPsByCountry(request.country);
            if (Collections.isEmpty(sopList)) {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "No hay un SOP configurado para el pais: [" + request.country + "]";
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            ResponseMessage rmAux = null;
            Response r = null;
            List<Subscription> subs = new ArrayList();
            rm.status = ResponseMessage.Status.REMOVED;
            for (SOP sop : sopList) {
                if (request.serviceCode != null && !sop.getServiceMatcher().getService().getCode().equals(request.serviceCode)) {
                    continue;
                }

                FacadeRequest requestAux = new FacadeRequest();
                requestAux.externalId = request.externalId;
                requestAux.internalId = request.internalId;
                requestAux.service = sop.getServiceMatcher().getName();
                requestAux.operator = sop.getOperator().getName();
                requestAux.provider = sop.getProvider().getName();
                requestAux.userAccount = request.userAccount;
                requestAux.action = request.action;
                requestAux.adTracking = request.adTracking;
                requestAux.accountId = request.accountId;
                requestAux.customerId = request.customerId;

                try {
                    r = subscriptionStatus(requestAux);
                    rmAux = (ResponseMessage) r.getEntity();
                    Subscription s = (Subscription) rmAux.data;
                    if (s.getSubscriptionRegistry() == null) {
                        continue;
                    }
                    subs.add(s);
                    if (ResponseMessage.Status.ACTIVE.equals(rmAux.status)
                            || ResponseMessage.Status.BLACKLIST.equals(rmAux.status)
                            || ResponseMessage.Status.LOCKED.equals(rmAux.status)
                            || ResponseMessage.Status.PENDING.equals(rmAux.status)) {
                        if (!ResponseMessage.Status.ACTIVE.equals(rm.status)) {
                            rm.status = rmAux.status;
                        }
                    }
                } catch (Exception ex) {
                }
            }
            rm.data = subs;
            //  log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            log.error("Error al procesar peticion de status Service. [" + request + "]. " + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = ex.getMessage();
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.GONE).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @POST
    @Path("/unsubscription")
    @Produces(MediaType.APPLICATION_JSON)
    public Response unsubscribe(FacadeRequest request) {

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.u");
        } catch (Exception ex) {
        }

        //log.info("Unsubscription. Facade request: [" + request + "]");
        log.info(new FunnelLogMessage("Unsubscription. Facade request", "/rest/facade/unsubscription", request));

        ResponseMessage rm = new ResponseMessage();
        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
        try {
            Subscription s;
            try {
                s = getSubscription(request, rm, true);
            } catch (LockTimeoutException ex) {
                rm.message = ex.getMessage();
                log.info("Facade response: [" + rm + "]");
                return Response.status(504).entity(rm).type(MediaType.APPLICATION_JSON).build();
            } catch (Exception ex) {
                rm.message = ex.getMessage();
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }
            if (s == null) {
                rm.message = "Error al obtener suscripcion. " + (rm.message == null ? "" : rm.message);
                if (rm.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = ResponseMessage.Status.ERROR;
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            //Veamos si existe la suscripcion en el proveedor
            if (s.getId() == null) {
                try {
                    subscriptionStatus(request);
                    s = getSubscription(request, rm, true);
                } catch (Exception ex) {
                }
            }

            if (s.getId() == null) {
                rm.data = request.toString();
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "Error al obtener suscripcion. No existe una suscripcion en BD para los parametros dados.";
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            if (!Subscription.Status.ACTIVE.equals(s.getStatus()) && !Subscription.Status.PENDING.equals(s.getStatus())) {
                rm.data = request.toString();
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "Error al realizar la baja de suscripcion. El usuario no esta suscripto en la DB. Status: [" + s.getStatus() + "]";
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            List<RLock> lock = (List<RLock>) s.getLock();
            s.setLock(null);
            String url = profilePropertiesService.getValueByProviderKey(s.getSop().getProvider().getId(), "internal.service.url") + "unsubscribe";
            String response = null;
            ResponseMessage rsp = null;
            try {
                s.getSubscriptionRegistry().setChannelOut(request.channel);
                response = requestClient.requestJSONPostFacade(url, mapper.writeValueAsString(s));
                //  log.info(s.getSop().getProvider().getName() + " response: [" + response + "]");
                if (response == null) {
                    rm.message = "Error en un proceso externo. ";
                    rm.status = ResponseMessage.Status.ERROR;
                    instrumentedObject.unLockSubscription(lock);
                    //  log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                rsp = mapper.readValue(response, ResponseMessage.class);
                if (rsp.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = rsp.message;
                    log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                } else if (rsp.status == ResponseMessage.Status.ERROR) {
                    log.info("Facade response: [" + rm + "]");
                    rm.message = rsp.message;
                    rm.status = ResponseMessage.Status.ERROR;
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                s = mapper.readValue((String) rsp.data, Subscription.class);
            } catch (Exception ex) {
                rm.message = "Error al interpretar el JSON. " + ex;
                log.error(rm.message, ex);
                rm.status = ResponseMessage.Status.ERROR;
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            switch (rsp.status) {
                case OK:
                    s.setStatus(Subscription.Status.REMOVED);
                    String ssaux = (request.channel == null ? "" : ":" + request.channel);
                    s.getSubscriptionRegistry().setOriginUnsubscription("FACADE:UNSUBSCRIPTION" + ssaux);
                    s.getSubscriptionRegistry().setChannelOut(s.getSubscriptionRegistry().getChannelOut() == null ? "FACADE" : "FACADE:" + s.getSubscriptionRegistry().getChannelOut());
                    subscriptionManager.subscriptionProcess(s);
                    break;
                default:
                    break;
            }
            rm.status = rsp.status;
            rm.message = rsp.message;

            instrumentedObject.unLockSubscription(lock);
            //   log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            rm.message = "Error al procesar request: [" + request + "]. " + ex;
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @POST
    @Path("/subscription/sms/send")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendSMS(FacadeRequest request) {

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.sms");
        } catch (Exception ex) {
        }

        //log.info("sendSMS. Facade request: [" + request + "]");
        log.info(new FunnelLogMessage("SendSMS. Facade request", "/rest/facade/subscription/sms/send", request));
        ResponseMessage rm = new ResponseMessage();
        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        if (request.message == null || request.message.length() < 2) {
            rm.data = request.toString();
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "No hay un mensaje valido a enviar.";
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
        try {
            Subscription s;
            try {
                s = getSubscription(request, rm, true);
            } catch (LockTimeoutException ex) {
                rm.message = ex.getMessage();
                log.info("Facade response: [" + rm + "]");
                return Response.status(504).entity(rm).type(MediaType.APPLICATION_JSON).build();
            } catch (Exception ex) {
                rm.message = ex.getMessage();
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }
            if (s == null) {
                rm.message = "Error al obtener suscripcion. " + (rm.message == null ? "" : rm.message);
                if (rm.status == ResponseMessage.Status.ERROR_403) {
                    rm.status = ResponseMessage.Status.ERROR;
                    //  log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                //   log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }

            /*  
            if (s.getId() == null) {
                rm.data = request.toString();
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "Error al obtener suscripcion. No existe una suscripcion en BD para los parametros dados.";
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }
             */
            List<RLock> lock = (List<RLock>) s.getLock();
            s.setLock(null);
            String url = profilePropertiesService.getValueByProviderKey(s.getSop().getProvider().getId(), "internal.service.url") + "subscription/send/sms";
            String response = null;
            ResponseMessage rsp = null;

            try {
                s.setObject(request.message);
                response = requestClient.requestJSONPostFacade(url, mapper.writeValueAsString(s));
                //  log.info(s.getSop().getProvider().getName() + " response: [" + response + "]");
                if (response == null) {
                    rm.message = "Error en un proceso externo. ";
                    rm.status = ResponseMessage.Status.ERROR;
                    instrumentedObject.unLockSubscription(lock);
                    //   log.info("Facade response: [" + rm + "]");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
                }
                rsp = mapper.readValue(response, ResponseMessage.class);
            } catch (Exception ex) {
                rm.message = "Error al interpretar el JSON. " + ex;
                log.error(rm.message, ex);
                rm.status = ResponseMessage.Status.ERROR;
                log.info("Facade response: [" + rm + "]");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
            }
            rm.status = rsp.status;
            rm.message = rsp.message;

            instrumentedObject.unLockSubscription(lock);
            //  log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
        } catch (Exception ex) {
            rm.message = "Error al procesar request: [" + request + "]. " + ex;
            log.error(rm.message, ex);
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }
    }

    @POST
    @Path("/subscription/normalizer")
    @Produces(MediaType.APPLICATION_JSON)
    public Response normalizer(FacadeRequest request) {

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.n");
        } catch (Exception ex) {
        }

        log.info("Normalizer. Facade request: [" + request + "]");
        ResponseMessage rm = new ResponseMessage();
        if (request == null) {
            rm.status = ResponseMessage.Status.ERROR;
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        Provider p = providerService.findByName(request.provider);
        if (p == null) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "No existe el Provider: [" + request.provider + "]. ";
            log.error(rm.message);
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        String msisdn = request.userAccount;
        try {
            msisdn = userAccountNormalizer(msisdn, p);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = ex.getMessage();
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.FORBIDDEN).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        rm.status = ResponseMessage.Status.OK;
        rm.message = msisdn;
        //  log.info("Facade response: [" + rm + "]");
        return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/subscription/tracking")
    @Produces(MediaType.APPLICATION_JSON)
    public Response tracking(FacadeRequest request) {

        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + ".facade.track");
        } catch (Exception ex) {
        }

        //log.info("tracking. Facade request: [" + request + "]");
        log.info(new FunnelLogMessage("Tracking. Facade request", "/rest/facade/subscription/tracking", request));
        ResponseMessage rm = new ResponseMessage();
        if (request == null || request.transactionId == null || request.adTracking == null) {
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = "Alguno de los parametros es NULL";
            log.info("Facade response: [" + rm + "]");
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).build();
        }

        facadeCache.setFacadeRequest(null, request.transactionId, null, 0L, request);

        rm.status = ResponseMessage.Status.OK;
        //  log.info("Facade response: [" + rm + "]");
        return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).build();
    }

    private void lastChargedProcess(Subscription xx) {
        try {
            if (!Subscription.Status.ACTIVE.equals(xx.getStatus())) {
                return;
            }
            Calendar lc = Calendar.getInstance();
            SubscriptionRegistry sr = subscriptionManager.getSubscriptionRegistry(xx.getSubscriptionRegistry().getId());
            lc.setTimeInMillis(Long.parseLong(sr.getExtra().stringData.get("lastCharged")));
            String frecuency = sr.getSop().getIntegrationSettings().get("frequencyType");
            int frec = 100;
            switch (frecuency) {
                case "days":
                    frec = 1;
                    break;
                case "week":
                    frec = 7;
                    break;
                case "month":
                    frec = 30;
                    break;
            }
            lc.add(Calendar.DAY_OF_YEAR, frec);
            Calendar today = Calendar.getInstance();
            xx.setCharged(today.before(lc));
        } catch (Exception ex) {
        }
    }
}
