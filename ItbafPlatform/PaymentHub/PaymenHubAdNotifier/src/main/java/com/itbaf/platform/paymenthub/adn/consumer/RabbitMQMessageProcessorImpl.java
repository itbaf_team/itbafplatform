/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adn.consumer;

import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.paymenthub.adn.processor.AdnetworkProcessor;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQMessageProcessorImpl implements RabbitMQMessageProcessor {

    private final AdnetworkProcessor adnProcessor;

    @AssistedInject
    public RabbitMQMessageProcessorImpl(@Assisted final AdnetworkProcessor adnProcessor) {
        this.adnProcessor = Validate.notNull(adnProcessor, "An AdnetworkProcessor class must be provided");
    }

    @Override
    public Boolean processMessage(String message) {
        try {
            String tid = Thread.currentThread().getName() + ".adn." + adnProcessor.getNotificationType().name();
            Thread.currentThread().setName(tid);
        } catch (Exception ex) {
        }

        try {
            return adnProcessor.process(message);
        } catch (Exception ex) {
            log.error("Error al procesar ADN: [" + message + "]. " + ex, ex);
        }
        return null;
    }

}
