/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.server.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.paymenthub.server.notification.NotificationProcessor;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import java.util.List;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQMessageProcessorImpl implements RabbitMQMessageProcessor {
    
    private final ObjectMapper mapper;
    private final InstrumentedObject instrumentedObject;
    private final SopService sopService;
    private final NotificationProcessor notificationProcessor;
    
    @AssistedInject
    public RabbitMQMessageProcessorImpl(final ObjectMapper mapper,
            final InstrumentedObject instrumentedObject,
            final SopService sopService,
            @Assisted final NotificationProcessor messageProcessor) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.sopService = Validate.notNull(sopService, "A SopService class must be provided");
        this.notificationProcessor = Validate.notNull(messageProcessor, "A NotificationProcessor class must be provided");
    }
    
    @Override
    public Boolean processMessage(String message) {
        
        PaymentHubMessage msg = null;
        try {
            msg = mapper.readValue(message, PaymentHubMessage.class);
            
            try {
                switch (msg.getMessageType()) {
                    case SUBSCRIPTION_ACTIVE:
                        Thread.currentThread().setName(Thread.currentThread().getName() + "-N.S.A");
                        break;
                    case SUBSCRIPTION_BLACKLIST:
                        Thread.currentThread().setName(Thread.currentThread().getName() + "-N.S.B");
                        break;
                    case SUBSCRIPTION_PENDING:
                        Thread.currentThread().setName(Thread.currentThread().getName() + "-N.S.P");
                        break;
                    case SUBSCRIPTION_BY_BILLING:
                        Thread.currentThread().setName(Thread.currentThread().getName() + "-N.S.B.B");
                        break;
                    case SUBSCRIPTION_REMOVED:
                        Thread.currentThread().setName(Thread.currentThread().getName() + "-N.S.R");
                        Thread.sleep(1000);
                        break;
                    case SUBSCRIPTION_CANCELLATION:
                        Thread.currentThread().setName(Thread.currentThread().getName() + "-N.S.C");
                        Thread.sleep(1000);
                        break;
                    case BILLING_PARTIAL:
                        Thread.currentThread().setName(Thread.currentThread().getName() + "-N.B.P");
                        Thread.sleep(1000);
                        break;
                    case BILLING_TOTAL:
                        Thread.currentThread().setName(Thread.currentThread().getName() + "-N.B.T");
                        Thread.sleep(1000);
                        break;
                    case BILLING_PENDING:
                       // log.info("Ignorando notificacion BILLING_PENDING");
                        return true;
                    case ONDEMAND_TOTAL:
                        Thread.currentThread().setName(Thread.currentThread().getName() + "-N.O.T");
                        break;
                }
            } catch (Exception ex) {
            }
        } catch (Exception ex) {
            log.error("Error procesando mensaje. " + ex, ex);
            return false;
        }
        
        SOP sop = null;
        try {
            sop = sopService.findById(msg.getSopId());
        } catch (Exception ex) {
            log.fatal("no hay un SOP configurado para: [" + msg + "]. " + ex, ex);
        }
        
        List<RLock> lock = null;
        try {
            lock = instrumentedObject.lockSubscription(msg.getExternalId(), msg.getUserAccount(),
                    msg.getExternalUserAccount(), msg.getExternalCustomer(), sop.getId());
        } catch (Exception ex) {
            log.error("Error al sincronizar notificacion. " + ex, ex);
        }
        
        Boolean processed = null;
        
        try {
            processed = notificationProcessor.process(message, msg, sop);
        } catch (Exception ex) {
            log.error("Error procesando notificacion: [" + msg + "]. " + ex, ex);
        }
        
        instrumentedObject.unLockSubscription(lock);
        return processed;
    }
    
}
