package com.itbaf.platform.commons.log4j2.appender;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

/**
 * Log4j2 log statements at various log levels
 * <p>
 * Example log4j2 configuration:
 * <pre>
 *   &lt;?xml version="1.0" encoding="UTF-8"?&gt;
 *   &lt;Configuration packages="io.prometheus.client.log4j2"&gt;
 *     &lt;Appenders&gt;
 *       &lt;Prometheus name="PROMETHEUS"/&gt;
 *     &lt;/Appenders&gt;
 *     &lt;Loggers&gt;
 *       &lt;Root level="trace"&gt;
 *         &lt;AppenderRef ref="PROMETHEUS"/&gt;
 *       &lt;/Root&gt;
 *     &lt;/Loggers&gt;
 *   &lt;/Configuration&gt;
 * </pre> Example metrics being exported:
 * <pre>
 *   log4j_appender_total{level="trace",} 1.0
 *   log4j_appender_total{level="debug",} 2.0
 *   log4j_appender_total{level="info",} 3.0
 *   log4j_appender_total{level="warn",} 4.0
 *   log4j_appender_total{level="error",} 5.0
 *   log4j_appender_total{level="fatal",} 6.0
 * </pre>
 */
@Plugin(name = "InstrumentedAppender", category = "Core", elementType = "appender")
public final class InstrumentedAppender extends AbstractAppender {

    public InstrumentedAppender(final String name) {
        super(name, null, null);
    }

    @Override
    public void append(LogEvent event) {
        
        System.out.println("");
        
        /*  if (instrumentedObject.influxDB != null) {
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Point point = Point
                        .measurement("paymenthub_log_event")
                        .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                        .tag("appName", instrumentedObject.appName)
                        .tag("instance", instrumentedObject.profile + "." + instrumentedObject.instance)
                        .tag("container", instrumentedObject.container)
                        .tag("level", event.getLevel().toString())
                        .tag("date", sdf.format(new Date()))
                        .tag("class", event.getSource().getFileName().replace(".", "xxJJxx").split("xxJJxx")[0])
                        .tag("line", String.valueOf(event.getSource().getLineNumber()))
                        .addField("thread", event.getThreadName())
                        .addField("message", event.getMessage().toString())
                        .addField("value", 1)
                        .build();
                instrumentedObject.influxDB.write(GuiceInstrumentedConfigModule.influxDataBase, "autogen", point);
            } catch (Exception ex) {
                System.out.println("Error al procesar el evento para InfluxDB " + ex);
                ex.printStackTrace();
            }
        }*/
    }

    @PluginFactory
    public static InstrumentedAppender createAppender(@PluginAttribute("name") String name) {
        if (name == null) {
            LOGGER.error("No name provided for InstrumentedAppender");
            return null;
        }
        return new InstrumentedAppender(name);
    }

}
