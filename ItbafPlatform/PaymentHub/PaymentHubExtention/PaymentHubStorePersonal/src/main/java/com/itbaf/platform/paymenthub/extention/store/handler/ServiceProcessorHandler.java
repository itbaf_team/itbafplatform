/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.store.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.inject.Inject;

import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;

import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.store.resources.StoreResponse;
import com.itbaf.platform.paymenthub.extention.store.ws.notification.NotificarEvento;
import com.itbaf.platform.paymenthub.extention.store.ws.notification.Notification;
import com.itbaf.platform.paymenthub.extention.store.resources.StoreResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final StoreResources storeResources;
    private final Map<String, Provider> providerMap;
    private final OnDemandProcessorHandler onDemandProcessorHandler;

    @Inject
    public ServiceProcessorHandler(final Map<String, Provider> providerMap,
                                   final StoreResources storeResources,
                                   final OnDemandProcessorHandler onDemandProcessorHandler) {
        this.storeResources = Validate.notNull(storeResources, "A StoreResources class must be provided");
        this.providerMap = Validate.notNull(providerMap, "A Map<String, Provider> class must be provided");
        this.onDemandProcessorHandler = Validate.notNull(onDemandProcessorHandler, "An OnDemandProcessorHandler class must be provided");
    }

    public void processNotification(String countryCode, final NotificarEvento notifications) {

        if (countryCode == null || countryCode.length() != 2) {
            log.error("El Codigo de pais no es valido. countryCode: [" + countryCode + "]");
            return;
        }

        Provider p = providerMap.get(countryCode.toUpperCase());
        if (p == null) {
            log.error("No hay configurado un provider para CountryCode: [" + countryCode + "]");
            return;
        }
        notifications.setCountryCode(countryCode);
        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notifications);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notifications);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notifications + "]. " + ex, ex);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {
        NotificarEvento notifications;
        try {
            notifications = mapper.readValue(message, NotificarEvento.class);
            Thread.currentThread().setName(notifications.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }

        Provider provider = providerMap.get(notifications.getCountryCode().toUpperCase());
        final String threadName = Thread.currentThread().getName();
        for (Notification notification : notifications.getNotifications()) {
            Thread.currentThread().setName(threadName + "-" + notification.getTransactionId());
            try {
                if (processNotification(notification, provider) == null) {
                    if (notifications.getNotifications().size() == 1) {
                        return null;
                    }
                    NotificarEvento aux = new NotificarEvento();
                    aux.setCountryCode(notifications.getCountryCode());
                    aux.getNotifications().add(notification);
                    String jsonResponse;
                    jsonResponse = mapper.writeValueAsString(aux);
                    if (jsonResponse != null) {
                        return rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + provider.getName(), jsonResponse);
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar notificacion: [" + notification + "]. " + ex, ex);
                return null;
            }
        }

        return true;
    }

    private Boolean processNotification(Notification notification, Provider provider) throws Exception {
        Boolean result = null;
        //se valida que la notificación sea exitosa.  Si el cobro fue exitoso, mostrará 1, de lo contrario, mostrará 0.
        //se valida que el return code de la notificación de Personal sea "Billing Request Accepted" o "Pedido Aceptado para Cobro Parcial". Los otros códigos se consideran error
        if( notification.getSuccess() == 1  && ( notification.getBillingCode().equals("000") || notification.getBillingCode().equals("201") ) ){
            if( notification.getPricingModel().toLowerCase().equals("ppd") ){
                switch (notification.getTransactionType()) {
                    case "purchase": //Cobro on demand
                        result = onDemandProcessorHandler.processBillingNotification(notification, provider);
                        break;
                    default:
                        throw new IllegalStateException("transaction type inesperado: [" + notification.getTransactionType() + "]");
                }
            } else if( notification.getPricingModel().toLowerCase().equals("subscription") || ( notification.getPricingModel().toLowerCase().equals("") && notification.getTransactionType().toLowerCase().equals("cancel-subscription") )) {
                switch (notification.getTransactionType().toLowerCase()) {
                    case "cancel-subscription": // Baja de servicio
                        result = processUnsubscriptionNotification(notification, provider);
                        break;
                    case "purchase":
                    case "renew-subscription": // Billing
                        result = processBillingNotification(notification, provider);
                        break;
                    default:
                        throw new IllegalStateException("transaction type inesperado: [" + notification.getTransactionType() + "]");
                }
            } else {
                throw new IllegalStateException("pricing model inesperado: [" + notification.getPricingModel() + "]");
            }
        }
        return result;
    }

    private Boolean processUnsubscriptionNotification(Notification notification, Provider provider) {
        SOP sop = sopService.findBySettings(provider.getName(), "store_serviceId", notification.getContentId());
        if (sop == null) {
            log.error("No existe un SOP configurado para el store_serviceId: [" + notification.getContentId() + "] - Provider: [" + provider.getName() + "]");
            return null;
        }

        BigDecimal fullAmount = null;
        BigDecimal netAmount = null;
        Tariff t = sop.getMainTariff();
        if (t != null) {
            fullAmount = t.getFullAmount();
            netAmount = t.getNetAmount();
        }

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setToDate(notification.getTransactionDateAndTime());
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t != null ? t.getId() : null);
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(null);
            msg.setTransactionId(notification.getTransactionId());
            msg.setUserAccount(notification.getCustomer());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar StoreNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private Boolean processBillingNotification(Notification notification, Provider provider) {
        SOP sop = sopService.findBySettings(provider.getName(), "store_serviceId", notification.getContentId());
        if (sop == null) {
            log.error("No existe un SOP configurado para el store_serviceId: [" + notification.getContentId() + "] - Provider: [" + provider.getName() + "]");
            return null;
        }

        BigDecimal netAmount = notification.getPrice();
        BigDecimal fullAmount;

        if (notification.getPrice() == null || notification.getPrice().compareTo(BigDecimal.ZERO) < 1) {
            // log.info("Billing Notification. No procesada. fullAmount: [" + fullAmount + "]");
            return null;
        }

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            Tariff t = sop.findTariffByNetAmount(netAmount);
            if (t == null) {
                //Verificamos si es una tarifa principal nueva
                t = updateMainTariff(sop, null, netAmount);
                if (t == null) {
                    //Creamos o actualizamos la tarifa hija
                    t = updateNetAmountChildrenTariff(sop, netAmount, notification.getContentId());
                }
            }

            if (t == null) {
                log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
                return null;
            }

            fullAmount = t.getFullAmount();

            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            msg.setFromDate(notification.getTransactionDateAndTime());
            msg.setChargedDate(notification.getTransactionDateAndTime());
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);

            if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }

            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setExternalUserAccount(null);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setSopId(sop.getId());
            msg.setTransactionId(notification.getTransactionId());

            msg.setChannelIn(null);
            msg.setUserAccount(notification.getCustomer());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_BILLING, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar StoreNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;

        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        if (subscription.getSubscriptionRegistry() != null && subscription.getSubscriptionRegistry().getId() != null
                && subscription.getSubscriptionRegistry().getUnsubscriptionDate() != null
                && (Subscription.Status.PENDING.equals(subscription.getStatus()) || Subscription.Status.ACTIVE.equals(subscription.getStatus()))) {
            rm.status = ResponseMessage.Status.OK_NS;
            rm.message = "La suscripcion ya tiene un estado valido";
        } else {
            SOP sop = subscription.getSop();
            try {
                StoreResponse tr = storeResources.validateSubscribe(subscription.getFacadeRequest().externalId, sop);
                if (tr == null || tr.status == null) {
                    rm.status = ResponseMessage.Status.ERROR;
                    rm.message = "No fue posible realizar el alta con Store";
                } else {
                    if (tr.isActive && tr.status.equals("PURCHASED")) {
                        rm.status = ResponseMessage.Status.OK;
                        rm.message = "Transaccion exitosa. El usuario tiene el servicio activo";
                    } else {
                        rm.status = ResponseMessage.Status.ERROR;
                        rm.message = "No fue posible realizar el alta. El usuario no tiene el servicio activo en Store: [ isActive: " + tr.isActive + " status: " + tr.status + "] ";
                    }
                }
            } catch (Exception ex) {
                log.error("Error en Store al realizar el alta [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = "Error en Store. " + ex.getStackTrace();
            }
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();

        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        SOP sop = subscription.getSop();
        try {
            StoreResponse tr = storeResources.consultStatusContent(subscription.getUserAccount(), sop);
            if (tr == null || tr.status == null) {
                subscription.setStatus(Subscription.Status.REMOVED);
                rm.status = ResponseMessage.Status.REMOVED;
                rm.message = "No fue posible realizar el alta con Store";
            } else {
                if (tr.isActive && tr.status.equals("PURCHASED")) {
                    if(subscription.getSubscriptionRegistry() != null && subscription.getStatus().equals(Subscription.Status.ACTIVE)) {
                        subscription.setStatus(Subscription.Status.ACTIVE);
                        rm.status = ResponseMessage.Status.ACTIVE;
                    } else if( subscription.getSubscriptionRegistry() == null &&  sop.getServiceMatcher().getService().getCode().equals("SF") ) {
                        //es la primera suscripción
                        subscription.setStatus(Subscription.Status.ACTIVE);
                        subscription.setSubscriptionRegistry(new SubscriptionRegistry());
                        subscription.getSubscriptionRegistry().setUserAccount(subscription.getUserAccount());
                        subscription.getSubscriptionRegistry().setSop(subscription.getSop());
                        subscription.setSubscriptionDate(new Date());
                        subscription.getSubscriptionRegistry().setSubscriptionDate(subscription.getSubscriptionDate());
                        subscription.getSubscriptionRegistry().setOriginSubscription("FACADE:STATUS-ACTIVATED-FIRST-24HRS");
                        rm.data =
                        rm.status = ResponseMessage.Status.ACTIVE;
                    } else {
                        subscription.setStatus(Subscription.Status.PENDING);
                        rm.status = ResponseMessage.Status.PENDING;
                    }
                } else {
                    subscription.setStatus(Subscription.Status.CANCELLED);
                    rm.status = ResponseMessage.Status.REMOVED;
                }
            }
        } catch (Exception ex) {
            log.error("Error en Store al realizar el alta [" + subscription.getUserAccount() + "] - SOP: [" + sop + "]. " + ex, ex);
            subscription.setStatus(Subscription.Status.REMOVED);
            rm.status = ResponseMessage.Status.ERROR;
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }
        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;

        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        if (subscription.getSubscriptionRegistry().getId() != null) {
            ResponseMessage.Status result =   storeResources.deleteSubscription(subscription.getUserAccount(), subscription.getSop());
            if (result != null) {
                rm.status = result;
            }
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public String userAccountNormalizer(String userId) throws Exception {
        if (userId != null && userId.length() == 12) {
            return userId;
        }

        throw new Exception("No es posible normalizar el UserAccount. Formato Invalido!");
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error: " + ex;
            return rm;
        }
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

}
