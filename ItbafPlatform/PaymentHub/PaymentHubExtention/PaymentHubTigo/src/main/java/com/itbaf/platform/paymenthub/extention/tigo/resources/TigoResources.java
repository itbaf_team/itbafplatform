package com.itbaf.platform.paymenthub.extention.tigo.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.paymenthub.extention.tigo.model.TigoNotification;
import com.itbaf.platform.paymenthub.extention.tigo.modules.GuiceConfigModule;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import java.util.Calendar;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class TigoResources {

    private final RequestClient requestClient;
    private final ObjectMapper mapper;

    @Inject
    public TigoResources(final ObjectMapper mapper,
            final RequestClient requestClient) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
    }

  /*  public boolean sendAcknowledge(TigoNotification n) {

        try {
            logger.info("Enviando Acknowledge de notificacion");
            String response = requestClient.requestJSONPost(buildStringForAcknowledge(n), null);
            logger.info("Respuesta de nPay entity: " + response);
            AcknowledgeResponse ack = mapper.readValue(response, AcknowledgeResponse.class);
            logger.info("Code: [" + ack.getMeta().code + "] - Message: [" + ack.getData().message + "]");
            return "200".equals(ack.getMeta().code) && "VERIFIED".equals(ack.getData().message);
        } catch (Exception ex) {
            logger.error("Error al procesar response", ex);
        }

        return false;
    }

    public ValidateSubscription validateSubscriptionByMsisdn(String msisdn, SOP sop) {
        logger.info("Validando suscripcion del ANI: [" + msisdn + "]");

        try {
            String idService = sop.getIntegrationSettings().get("id_service");
            String clientSecret = sop.getIntegrationSettings().get("client_secret");
            String url = buildStringForValidateSubscription(idService, clientSecret, msisdn, true);
            String response = requestClient.requestGet(url);
            logger.info("Respuesta de nPay: " + response);
            ValidateSubscription vs = null;
            try {
                vs = mapper.readValue(response, ValidateSubscription.class);
            } catch (Exception e) {
                logger.error("Error al obtener el estado de una suscripcion.", e);
            }

            return vs;
        } catch (Exception e) {
            throw new IllegalStateException("Error validateSubscriptionByMsisdn. Ani: [" + msisdn + "] " + e);
        }
    }

    public ValidateSubscription validateSubscriptionByExternalId(String externalId, SOP sop) {
        logger.info("Validando suscripcion del externalId: [" + externalId + "]");

        try {
            String idService = sop.getIntegrationSettings().get("id_service");
            String clientSecret = sop.getIntegrationSettings().get("client_secret");
            String url = buildStringForValidateSubscription(idService, clientSecret, externalId, false);
            String response = requestClient.requestGet(url);
            logger.info("Respuesta de nPay: " + response);
            ValidateSubscription vs = null;
            try {
                vs = mapper.readValue(response, ValidateSubscription.class);
            } catch (Exception e) {
                logger.error("Error al obtener el estado de una suscripcion.", e);
            }

            return vs;
        } catch (Exception e) {
            throw new IllegalStateException("Error validateSubscriptionByMsisdn. externalId: [" + externalId + "] " + e);
        }
    }

    public String suscribe(Subscription subscription) {
        String msisdn = subscription.getUserAccount();
        SOP sop = subscription.getSop();
        logger.info("Realizando alta en NPAY. ANI: [" + msisdn + "]. SOP: [" + sop + "]");

        String externalId = null;
        try {

            String url = buildStringForSubscribe(sop.getIntegrationSettings().get("client_secret"),
                    sop.getIntegrationSettings().get("id_service"),
                    sop.getIntegrationSettings().get("keywordAlta"), msisdn);

            String response = requestClient.requestJSONPost(url, null);

            logger.info("Respuesta desde NPAY: [" + response + "]");

            ValidateSubscription vs = mapper.readValue(response, ValidateSubscription.class);

            if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                Calendar c = Calendar.getInstance();
                switch (vs.getData().status) {
                    case created:
                    case pending:
                        subscription.setStatus(Subscription.Status.PENDING);
                        c.setTimeInMillis(Long.parseLong(vs.getData().created + "000"));
                        subscription.setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().id_subscription);
                        return vs.getData().id_subscription;
                    case active:
                    case successful_renewal:
                    case pending_renewal:
                        subscription.setStatus(Subscription.Status.ACTIVE);
                        c.setTimeInMillis(Long.parseLong(vs.getData().created + "000"));
                        subscription.setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().id_subscription);
                        return vs.getData().id_subscription;
                }
            } else {
                logger.error("Respuesta erronea desde NPAY");
            }

        } catch (Exception e) {
            logger.error("Error al suscribir. ANI: [" + msisdn + "]. SOP: [" + sop + "]", e);
        }
        return externalId;
    }

    public ResponseMessage.Status sendSubscriptionPin(String carrier, Subscription subscription) {

        String msisdn = subscription.getUserAccount();
        SOP sop = subscription.getSop();
        logger.info("Realizando envio de PIN con NPAY. ANI: [" + msisdn + "]. SOP: [" + sop + "]");
        try {
            String url = buildStringForSendSubscriptionPin(
                    sop.getIntegrationSettings().get("id_service"),
                    sop.getIntegrationSettings().get("client_secret"),
                    sop.getProvider().getCountry().getCode().toUpperCase(),
                    carrier,
                    "web",
                    sop.getIntegrationSettings().get("keywordAlta"), msisdn);

            String response = requestClient.requestJSONPost(url, null);
            logger.info("Respuesta desde NPAY: [" + response + "]");
            ValidateSubscription vs = mapper.readValue(response, ValidateSubscription.class);

            if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                if ("true".equals(vs.getData().sent) && "pin".equals(vs.getData().object)) {
                    return ResponseMessage.Status.OK;
                } else {
                    Calendar c = Calendar.getInstance();
                    switch (vs.getData().status) {
                        case created:
                        case pending:
                            subscription.setStatus(Subscription.Status.PENDING);
                            c.setTimeInMillis(Long.parseLong(vs.getData().created + "000"));
                            subscription.setSubscriptionDate(c.getTime());
                            subscription.getSubscriptionRegistry().setSubscriptionDate(c.getTime());
                            return ResponseMessage.Status.OK_PENDING;
                        case active:
                        case successful_renewal:
                        case pending_renewal:
                            subscription.setStatus(Subscription.Status.ACTIVE);
                            c.setTimeInMillis(Long.parseLong(vs.getData().created + "000"));
                            subscription.setSubscriptionDate(c.getTime());
                            subscription.getSubscriptionRegistry().setSubscriptionDate(c.getTime());
                            return ResponseMessage.Status.OK_ACTIVE;
                    }
                }
            } else {
                logger.error("Respuesta erronea desde NPAY:");
            }

        } catch (Exception ex) {
            logger.error("Error al enviar la solicitud de Pin.", ex);
        }
        return null;
    }

    public ResponseMessage validatePin(String carrier, Subscription subscription, String pin) {
        String msisdn = subscription.getUserAccount();
        SOP sop = subscription.getSop();
        logger.info("Validando PIN con NPAY. ANI: [" + msisdn + "]. SOP: [" + sop + "]");

        ResponseMessage rm = new ResponseMessage();
        try {
            String url = buildStringForValidatePin(sop.getIntegrationSettings().get("id_service"),
                    sop.getIntegrationSettings().get("client_secret"),
                    sop.getProvider().getCountry().getCode().toUpperCase(),
                    carrier, msisdn, pin);

            String response = requestClient.requestJSONPost(url, null);
            logger.info("Respuesta desde NPAY: [" + response + "]");
            ValidateSubscription vs = mapper.readValue(response, ValidateSubscription.class);

            if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                Calendar c = Calendar.getInstance();
                switch (vs.getData().subscription.status) {
                    case created:
                    case pending:
                        subscription.setStatus(Subscription.Status.PENDING);
                        c.setTimeInMillis(Long.parseLong(vs.getData().subscription.created + "000"));
                        subscription.setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().subscription.id_subscription);
                        rm.status = ResponseMessage.Status.OK;
                        return rm;
                    case active:
                    case successful_renewal:
                    case pending_renewal:
                        subscription.setStatus(Subscription.Status.ACTIVE);
                        c.setTimeInMillis(Long.parseLong(vs.getData().subscription.created + "000"));
                        subscription.setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().subscription.id_subscription);
                        rm.status = ResponseMessage.Status.OK;
                        return rm;
                }
            } else {
                logger.error("Respuesta erronea desde NPAY: [" + vs.getData().message + "]");
                rm.message = vs.getData().message;
            }

        } catch (Exception ex) {
            logger.error("Error al validar Pin: [" + pin + "].", ex);
        }
        return rm;
    }

    public ResponseMessage.Status removeSubscription(Subscription subscription) {

        String msisdn = subscription.getUserAccount();
        SOP sop = subscription.getSop();
        logger.info("Realizando baja en NPAY. ANI: [" + msisdn + "]. SOP: [" + sop + "]");
        try {

            String url = buildStringForRemoveSubscription(subscription.getSubscriptionRegistry().getExternalId(),
                    sop.getIntegrationSettings().get("client_secret"), sop.getIntegrationSettings().get("keywordBaja"));

            String response = requestClient.requestJSONPost(url, null);
            logger.info("Respuesta desde NPAY: [" + response + "]");
            ValidateSubscription vs = mapper.readValue(response, ValidateSubscription.class);

            if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                return ResponseMessage.Status.OK;
            } else {
                logger.error("Respuesta erronea desde NPAY");
            }

        } catch (Exception ex) {
            logger.error("Error al enviar la solicitud de Pin.", ex);
        }
        return null;
    }

    public ServiceInformationResponse subscriptionServiceInformation(SOP sop) {

        String idService = sop.getIntegrationSettings().get("id_service");
        String clientSecret = sop.getIntegrationSettings().get("client_secret");

        Object obj = MainCache.memory.getIfPresent(idService + "_" + clientSecret);

        if (obj == null) {
            String url = buildStringForSubscriptionServiceInformation(idService, clientSecret);
            try {
                String response = requestClient.requestGet(url);
                logger.info("Respuesta de nPay entity: " + response);
                ServiceInformationResponse sir = mapper.readValue(response, ServiceInformationResponse.class);
                logger.info("Code: [" + sir.getMeta().code + "] - idService: [" + sir.getData().id_service + "]");
                MainCache.memory.put(idService + "_" + clientSecret, sir);
                return sir;
            } catch (Exception ex) {
                logger.error("Error al procesar response", ex);
            }
        } else {
            return (ServiceInformationResponse) obj;
        }
        return null;
    }

    private String buildStringForSubscriptionServiceInformation(String idService, String clientSecret) {
        String uri = String.format("https://api.npay.io/subscriptions/v2/service/%s/?client_secret=%s",
                idService, clientSecret);
        return uri;
    }

    private String buildStringForValidateSubscription(String idService, String clientSecret, String user, Boolean isMsisdn) {
        String uri;
        if (isMsisdn == null) {//Por id_customer
            uri = String.format(
                    "https://api.npay.io/subscriptions/v2/service/%s/customer?client_secret=%s&id_customer=%s", idService,
                    clientSecret, user);
        } else if (isMsisdn) {//Por ani
            uri = String.format(
                    "https://api.npay.io/subscriptions/v2/service/%s/msisdn?client_secret=%s&msisdn=%s", idService,
                    clientSecret, user);
        } else {//Por externalId -> id_subscription
            uri = String.format(
                    "https://api.npay.io/subscriptions/v2/subscription/%s?client_secret=%s&id_service=%s", user,
                    clientSecret, idService);
        }
        return uri;
    }

    private String buildStringForSubscribe(String clientSecret, String idService, String keywordAlta, String msisdn) {
        return "https://api.npay.io/subscriptions/v2/subscription/cb/msisdn?"
                + "client_secret=" + clientSecret
                + "&id_service=" + idService
                + "&media=wap"
                + "&keyword=" + keywordAlta
                + "&msisdn=" + msisdn;
    }

    private String buildStringForSendSubscriptionPin(String idService, String clientSecret,
            String countryCode, String carrier, String media, String keywordAlta, String msisdn) {
        return "https://api.npay.io/subscriptions/v2/pin/" + idService + "/send?"
                + "client_secret=" + clientSecret
                + "&country=" + countryCode
                + "&carrier=" + carrier
                + "&media=" + media
                + "&keyword=" + keywordAlta
                + "&msisdn=" + msisdn;
    }

    private String buildStringForValidatePin(String idService, String clientSecret, String countryCode, String carrier, String msisdn, String pin) {
        return "https://api.npay.io/subscriptions/v2/pin/" + idService + "/validate?"
                + "client_secret=" + clientSecret
                + "&country=" + countryCode
                + "&carrier=" + carrier
                + "&msisdn=" + msisdn
                + "&pin=" + pin;
    }

    private String buildStringForRemoveSubscription(String idSubscription, String clientSecret, String keywordBaja) {
        return "https://api.npay.io/subscriptions/v2/subscription/" + idSubscription + "/cancel?"
                + "client_secret=" + clientSecret
                + "&keyword=" + keywordBaja;
    }

    private String buildStringForAcknowledge(TigoNotification n) {
        return "https://ipn.npay.io/verify?"
                + (n.getId_event() != null ? "id_event=" + n.getId_event() : "")
                + (n.getIpn_url() != null ? "&ipn_url=" + n.getIpn_url() : "")
                + (n.getIpn_type() != null ? "&ipn_type=" + n.getIpn_type() : "")
                + (n.getVerify_sign() != null ? "&verify_sign=" + n.getVerify_sign() : "")
                + (n.getId_app() != null ? "&id_app=" + n.getId_app() : "")
                + (n.getId_customer() != null ? "&id_customer=" + n.getId_customer() : "")
                + (n.getId_transaction() != null ? "&id_transaction=" + n.getId_transaction() : "")
                + (n.getAmount() != null ? "&amount=" + n.getAmount() : "")
                + (n.getCurrency() != null ? "&currency=" + n.getCurrency() : "")
                + (n.getId_subscription() != null ? "&id_subscription=" + n.getId_subscription() : "")
                + (n.getStatus() != null ? "&status=" + n.getStatus() : "")
                + (n.getId_service() != null ? "&id_service=" + n.getId_service() : "")
                + (n.getCreated() != null ? "&created=" + n.getCreated() : "")
                + (n.getUpdated() != null ? "&updated=" + n.getUpdated() : "");
    }*/

}
