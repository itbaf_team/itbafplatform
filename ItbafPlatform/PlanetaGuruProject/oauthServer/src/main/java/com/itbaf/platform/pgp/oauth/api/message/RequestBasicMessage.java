/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth.api.message;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
public class RequestBasicMessage {

    public enum UsernameType {
        PHONE, EMAIL, OTHER
    }

    public UsernameType usernameType;
    public String flowToken;//Token de flujo. Tracking unico por cada acceso
    public String tid;//ID de session. Unico para todos los accesos 
    public String username;
    public String location; // AR, CO, BR, MX
    public String codeValidate; //Enviar un Code de validacion
    public String gResponse; //g-recaptcha-response
    public String code; //Codigo de validacion
    public String vcode; //Codigo de verificacion
    public String provider; //Proveedor de usuario. Google, Facebook...
    public String tokenCode; //Token de usuario valido por Google Firebase
    public String ptokenCode; //Token de usuario del proveedor
    public String password;
    public String rpassword;
    public String kmsi;//Keep me signed in
    public String path;
    public FormRequest form;
    public String locale; //es, en, es_AR, en_US
    public String registry; // Proviene de la pagina de registro

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("usernameType", usernameType)
                .add("flowToken", flowToken)
                .add("tid", tid)
                .add("username", username)
                .add("location", location)
                .add("codeValidate", codeValidate)
                .add("gResponse", gResponse)
                .add("code", code)
                .add("vcode", vcode)
                .add("provider", provider)
                .add("tokenCode", tokenCode)
                .add("ptokenCode", ptokenCode)
                .add("password", password)
                .add("rpassword", rpassword)
                .add("kmsi", kmsi)
                .add("path", path)
                .add("form", form)
                .add("locale", locale)
                .omitNullValues().toString();
    }
}
