/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.instance;

import com.google.inject.Injector;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author JF
 */
public class GuiceInstance {

    private final Map<String, String> args = new HashMap();
    private final ServletListener servletListener;

    private static GuiceInstance guiceInstance;

    public static Injector getInstance() {
        if (guiceInstance == null) {
            guiceInstance = new GuiceInstance();
        }

        return guiceInstance.servletListener.getInjector();
    }

    public GuiceInstance() {

        this.args.put("guice.profiles.active", "prod");
        this.args.put("guice.instance", "1");
        this.args.put("guice.container", "test-" + System.currentTimeMillis());
        this.args.put("influxdb.disabled", "true");
        this.args.put("guice.profile", this.args.get("guice.profiles.active"));

        this.servletListener = new ServletListener(new GuiceConfigModule(args,null));
    }

}
