#!/bin/bash
# bash ./dev/build/build_pg-repository.sh
# mvn -B -e --also-make  --projects PaymentHub/PaymentHubServer/ -s ./settings.xml -Dmaven.test.skip=true  clean compile install package
#-v ~/.m2:/var/maven/.m2
docker run -it --rm  \
-v "$(pwd)"/dev/.m2:/root/.m2 \
-v "$(pwd)":/usr/src  \
-w /usr/src maven:3-jdk-11 \
mvn -B -e --projects PlanetaGuruProject/oauthServer -Dmaven.test.skip=true clean compile install #clean compile install package


# si el contenedor esta corriendo lo reinicio
#if [ "$( docker container inspect -f '{{.State.Running}}' ph-bn )" == "true" ]; then
#  docker-compose -f "$(pwd)"/dev/docker-compose.yml restart ph-bn
#fi
