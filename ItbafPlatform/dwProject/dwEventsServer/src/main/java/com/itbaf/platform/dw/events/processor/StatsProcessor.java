/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.commons.stats.model.SubscriptionRegistryElastic;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.AdnetworkNotifierService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionBillingService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.paymenthub.services.stats.StatsService;
import com.itbaf.platform.pgp.commons.resources.ClientCache;
import com.itbaf.platform.rmq.consumer.MessageProcessor;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public abstract class StatsProcessor implements MessageProcessor {

    @Inject
    protected ObjectMapper mapper;
    @Inject
    protected ClientCache clientCache;
    @Inject
    protected StatsService statsService;
    @Inject
    protected RequestClient requestClient;
    @Inject
    protected InstrumentedObject instrumentedObject;
    @Inject
    protected SubscriptionManager subscriptionManager;
    @Inject
    protected AdnetworkNotifierService adnetworkNotifierService;
    @Inject
    protected SubscriptionBillingService subscriptionBillingService;
    @Inject
    protected SubscriptionRegistryService subscriptionRegistryService;
    @Inject
    protected SopService sopService;

    public abstract ElasticData.ElasticType getElasticType();

    public abstract Boolean process(String msg) throws Exception;

    protected final SubscriptionRegistryElastic getSubscriptionRegistryElastic(SubscriptionRegistry sr) throws Exception {
        SubscriptionRegistryElastic sre = new SubscriptionRegistryElastic();

        try {
            List<Object> resume = subscriptionBillingService.getSubscriptionBillingResume(sr.getId());
            if (resume != null && resume.get(0) != null) {
                Object[] obj = (Object[]) resume.get(0);
                sre.totalFullAmount = new BigDecimal(obj[1].toString()).setScale(5, RoundingMode.HALF_UP);
                sre.totalNetAmount = new BigDecimal(obj[2].toString()).setScale(5, RoundingMode.HALF_UP);
                sre.charges = (BigInteger) obj[3];
                sre.firstCharged = (Date) obj[4];
                sre.lastCharged = (Date) obj[5];
                sre.minSubscriptionBillingId = (BigInteger) obj[6];
            }
        } catch (Exception ex) {
        }

        //Registros que tienen mal asignada la fecha de suscripcion
        if (sre.firstCharged != null && ((sre.firstCharged.getTime() - sr.getSubscriptionDate().getTime()) < -19000)) {
            sr.setSubscriptionDate(sre.firstCharged);
            if (sr.getExtra() != null && sr.getExtra().statusChanged != null) {
                sr.getExtra().statusChanged.put(Subscription.Status.ACTIVE, sr.getSubscriptionDate());
            }
            subscriptionRegistryService.saveOrUpdate(sr, false);
            //  log.info("FC. Actualizado EXTRA de SR: [" + sr + "]");
        }

        boolean save = false;
        if (sr.getExtra() != null && sr.getExtra().statusChanged != null && !sr.getExtra().statusChanged.isEmpty()) {
            Date pending = sr.getExtra().statusChanged.get(Subscription.Status.PENDING);
            Date active = sr.getExtra().statusChanged.get(Subscription.Status.ACTIVE);
            Date cancelled = sr.getExtra().statusChanged.get(Subscription.Status.CANCELLED);
            Date removed = sr.getExtra().statusChanged.get(Subscription.Status.REMOVED);

            if (sr.getUnsubscriptionDate() == null && (cancelled != null || removed != null)) {
                sr.getExtra().statusChanged.remove(Subscription.Status.CANCELLED);
                sr.getExtra().statusChanged.remove(Subscription.Status.REMOVED);
                sr.getExtra().statusChanged.remove(Subscription.Status.BLACKLIST);
                sr.getExtra().statusChanged.remove(Subscription.Status.LOCKED);
                cancelled = null;
                removed = null;
                save = true;
            }
            if (pending != null && active != null && (active.getTime() - pending.getTime() < 0)) {
                sr.getExtra().statusChanged.remove(Subscription.Status.PENDING);
                save = true;
            }
            if (cancelled != null && removed != null
                    && ((removed.getTime() - cancelled.getTime() < 0) || (removed.getTime() - cancelled.getTime() > 86400000))) {
                sr.getExtra().statusChanged.remove(Subscription.Status.CANCELLED);
                cancelled = null;
                save = true;
            }
        }
        try {
            if (sr.getSubscription().getSubscriptionRegistry().getId().equals(sr.getId()) && sr.getUnsubscriptionDate() == null && sr.getSubscription().getUnsubscriptionDate() != null) {
                sr.setUnsubscriptionDate(sr.getSubscription().getUnsubscriptionDate());
                save = true;
            }
        } catch (Exception ex) {
        }
        try {
            if (!sr.getSubscription().getSubscriptionRegistry().getId().equals(sr.getId()) && sr.getUnsubscriptionDate() == null) {
                sr.setUnsubscriptionDate(new Date());
                save = true;
            }
        } catch (Exception ex) {
        }

        if (save) {
            subscriptionRegistryService.saveOrUpdate(sr, false);
            //  log.info("Actualizado EXTRA de SR: [" + sr + "]");
        }

        sre.userAccount = sr.getUserAccount();
        try {
            sre.adnetworkNotifier = adnetworkNotifierService.findById(sr.getId());
        } catch (Exception ex) {
        }
        sre.adnetworkTracking = sr.getAdnetworkTracking();
        sre.channelIn = sr.getChannelIn();
        sre.channelOut = sr.getChannelOut();
        sre.elasticId = "subscription_registry_" + sr.getId().toString();
        sre.externalCustomer = sr.getSubscription().getExternalCustomer();
        sre.externalId = sr.getExternalId();
        sre.externalUserAccount = sr.getSubscription().getExternalUserAccount();
        sre.extra = sr.getExtra();
        sre.id = sr.getId().toString();
        sre.originSubscription = sr.getOriginSubscription();
        sre.originUnsubscription = sr.getOriginUnsubscription();
        sre.sop = sr.getSop();
        sre.sop.setIntegrationSettings(null);

        SOP sop = sopService.findById(sre.sop.getId());
        try {
            sre.tariff = sop.getMainTariff(sr.getSubscriptionDate());
        } catch (Exception ex) {
            log.info("SubscriptionRegistryElastic. No se encontro tariff por fecha");
            sre.tariff = sop.getMainTariff();
        }
        sre.sop.setTariffs(null);
        Country c = new Country();
        c.setId(sre.sop.getProvider().getCountry().getId());
        c.setCode(sre.sop.getProvider().getCountry().getCode());
        c.setCurrency(sre.sop.getProvider().getCountry().getCurrency());
        c.setName(sre.sop.getProvider().getCountry().getName());
        sre.sop.getProvider().setCountry(c);
        sre.subscriptionDate = sr.getSubscriptionDate();
        sre.subscriptionId = sr.getSubscription().getId();
        sre.syncDate = sr.getSyncDate();
        sre.unsubscriptionDate = sr.getUnsubscriptionDate();

        return sre;

    }

}
