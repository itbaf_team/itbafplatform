package com.itbaf.platform.paymenthub.extention.wau.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "response_data")
public class ResponseSubscribePin {

    @JacksonXmlElementWrapper
    private String result_code; //SVC0021

    @JacksonXmlElementWrapper
    private String result_description; //Subscription has been initiated and optIn pin sent to the user. [ip-172-31-4-30.ec2.internal]

    @JacksonXmlElementWrapper
    private String request_id; //194123711

    public ResponseSubscribePin() {
    }

    public ResponseSubscribePin(String result_code, String result_description, String request_id) {
        this.result_code = result_code;
        this.result_description = result_description;
        this.request_id = request_id;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public String getResult_description() {
        return result_description;
    }

    public void setResult_description(String result_description) {
        this.result_description = result_description;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }
}
