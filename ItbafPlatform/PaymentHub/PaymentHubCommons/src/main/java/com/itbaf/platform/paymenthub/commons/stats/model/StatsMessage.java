/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.stats.model;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author JF
 */
public class StatsMessage implements Serializable {

    private static final long serialVersionUID = 20181029094902L;

    public final Date createdDate = new Date();
    public final Date lastUpdate = new Date();
    public Long lastUpdateMillis = System.currentTimeMillis();
    public String thread = Thread.currentThread().getName();
    public ElasticData elasticData;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("createdDate", createdDate)
                .add("lastUpdate", lastUpdate)
                .add("lastUpdateMillis", lastUpdateMillis)
                .add("thread", thread)
                .add("elasticData", elasticData)
                .omitNullValues().toString();
    }
}
