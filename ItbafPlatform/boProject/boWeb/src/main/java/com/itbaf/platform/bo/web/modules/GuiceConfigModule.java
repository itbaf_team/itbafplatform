/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.bo.web.modules;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.itbaf.platform.bo.web.BoWebMain;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.modules.GuiceDefaultConfigModule;
import com.itbaf.platform.services.service.TestService;
import java.util.Arrays;
import java.util.Map;

/**
 *
 * @author jordonez
 */
public class GuiceConfigModule extends GuiceDefaultConfigModule {

    public GuiceConfigModule(final Map<String, String> args, CommonDaemon commonDaemon) {
        super(args, commonDaemon);
    }

    @Override
    protected void configure() {
        super.configure();

        //Binds
        //Modulos de aplicaciones
        install(new GuiceRestServicesConfigModule());
        
        log.info("Configuracion iniciada. " + Arrays.toString(args.entrySet().toArray()));
    }

    @Provides
    @Singleton
    TestService testService(BoWebMain paymentHubMain) {
        return paymentHubMain;
    }
    
}
