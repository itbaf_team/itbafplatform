/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.services.RabbitMQMemoryProcessorImpl;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class OauthMain {

    private final ExecutorService cachedThreadPool;
    private final RabbitMQConsumer rabbitMQConsumer;
    private final BasicDataBaseConnection dataBaseConnection;
    private final RabbitMQMemoryProcessorImpl memoryProcessor;
    private final ScheduledExecutorService scheduledThreadPool;

    @Inject
    public OauthMain(final ExecutorService cachedThreadPool,
            final RabbitMQConsumer rabbitMQConsumer,
            final BasicDataBaseConnection dataBaseConnection,
            final RabbitMQMemoryProcessorImpl memoryProcessor,
            final ScheduledExecutorService scheduledThreadPool) {
        this.rabbitMQConsumer = Validate.notNull(rabbitMQConsumer, "A RabbitMQConsumer class must be provided");
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "An ExecutorService class must be provided");
        this.memoryProcessor = Validate.notNull(memoryProcessor, "A RabbitMQMemoryProcessorImpl class must be provided");
        this.dataBaseConnection = Validate.notNull(dataBaseConnection, "A BasicDataBaseConnection class must be provided");
        this.scheduledThreadPool = Validate.notNull(scheduledThreadPool, "A ScheduledExecutorService class must be provided");
    }

    public void start() throws Exception {
        log.info("Initializing app... ");

        try {
            rabbitMQConsumer.createFanoutChannel(CHACHE_MEMORY_UPDATE, memoryProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer '" + CHACHE_MEMORY_UPDATE + "'. " + ex, ex);
        }

        InputStream is = getClass().getResourceAsStream("/config/key/planeta-guru-firebase-adminsdk-7y5g0-720245cf62.json");
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(is))
                .setDatabaseUrl("https://planeta-guru.firebaseio.com")
                .build();

        FirebaseApp.initializeApp(options);

        log.info("Initializing app OK. ");
    }

    public void stop() {
        stopThreads();
        dataBaseConnection.shutdownConnectionsPool();
        log.info("App destroyed. ");
    }

    public void stopThreads() {
        log.info("Finalizando Threads en 5 segundos... ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }
        try {
            log.info("Finalizando scheduledThreadPool ");
            scheduledThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            log.info("Esperando Threads por 2 segundos... ");
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        try {
            scheduledThreadPool.shutdownNow();
            log.info("Finalizado scheduledThreadPool ");
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdownNow();
        } catch (Exception ex) {
        }
    }

}
