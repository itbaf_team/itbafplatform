/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.messaging.rest.pg.Credit;
import com.itbaf.platform.messaging.rest.pg.PGMessageRequest;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.wallet.PaymentMethod;
import com.itbaf.platform.pgp.model.wallet.PaymentMethod.PaymentMethodType;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import io.jsonwebtoken.impl.DefaultClaims;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class StoreViewServiceProcessorHandler extends ServiceProcessorHandler {

    private final ApiWalletServiceProcessorHandler apiWalletServiceProcessorHandler;
    private final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler;

    @Inject
    public StoreViewServiceProcessorHandler(final ApiWalletServiceProcessorHandler apiWalletServiceProcessorHandler,
            final ApiProfileServiceProcessorHandler apiProfileServiceProcessorHandler) {
        this.apiWalletServiceProcessorHandler = Validate.notNull(apiWalletServiceProcessorHandler, "An ApiWalletServiceProcessorHandler class must be provided");
        this.apiProfileServiceProcessorHandler = Validate.notNull(apiProfileServiceProcessorHandler, "An ApiProfileServiceProcessorHandler class must be provided");
    }

    public String getServicesHtml(User client, User user, String i18n, DefaultClaims defaultClaims, String productId, String redirectUri) throws Exception {
        String html = readResource("/tpl/html/account/store.html", client, productId);
        html = html.replace("<siteHeaderScript/>", readHeaderResource("/tpl/html/header.html", client,user, productId));
        html = html.replace("<siteFooterScript/>", readResource("/tpl/html/footer.html", client, productId));

        UserProfile up = apiProfileServiceProcessorHandler.getUserProfileFromUser(client, user, null, false, false, false);

        if (up.getPhones() != null && !up.getPhones().isEmpty()) {
            Set<Msisdn> aux = new HashSet();
            for (Msisdn m : up.getPhones()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setPhones(aux);
        }
        if (up.getEmails() != null && !up.getEmails().isEmpty()) {
            Set<Email> aux = new HashSet();
            for (Email m : up.getEmails()) {
                if (m.getPrimary()) {
                    aux.add(m);
                    break;
                }
            }
            up.setEmails(aux);
        }

        //Lista de productos del menu
        html = getMenuProducts(html, productId, redirectUri);

        //Lista de Servicios con suscripcion y compra por demanda
        List<User> clients = userService.getAllUserClients();
        if (clients != null) {
            try {

                List<PGMessageRequest> messageRequests = new ArrayList();
                List<BigDecimal> lbd = Arrays.asList(new BigDecimal(5), new BigDecimal(10), new BigDecimal(25), new BigDecimal(50), new BigDecimal(100), new BigDecimal(200));
                PGMessageRequest messageRequest;
                for (BigDecimal bd : lbd) {
                    messageRequest = new PGMessageRequest();
                    messageRequest.credit = new Credit();
                    messageRequest.credit.amount = bd;
                    messageRequest.credit.currency = "GUR";
                    messageRequests.add(messageRequest);
                }
                List<PaymentMethod> lpm = apiWalletServiceProcessorHandler.getPaymentList(user, defaultClaims, messageRequests, clients);
                List<PaymentMethod> lpms = new ArrayList();
                List<PaymentMethod> lpmo = new ArrayList();

                for (PaymentMethod pm : lpm) {
                    if (PaymentMethodType.ONDEMAND.equals(pm.getType())) {
                        lpmo.add(pm);
                    } else {
                        lpms.add(pm);
                    }
                }
                if (!lpmo.isEmpty()) {
                    up.setPaymentMethodsOndemand(lpmo);
                }
                if (!lpms.isEmpty()) {
                    up.setPaymentMethodsSubscription(lpms);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        html = html.replace("uparams = {}", "uparams = " + mapper.writeValueAsString(up));
        return html;
    }

}
