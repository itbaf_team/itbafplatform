/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services;

import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.OnDemandBilling;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import java.util.List;

/**
 *
 * @author javier
 */
public interface OnDemandManager {

    public OnDemandRegistry findOnDemandRegistryById(String id);

    public OnDemandBilling findOnDemandBillingById(String id);

    public void ondemandRegistryProcess(OnDemandRegistry or) throws Exception;

    public void ondemandBillingProcess(OnDemandBilling ob) throws Exception;

    public OnDemandRegistry getAvailableByStatusOptin(String userAccout, List<SOP> sops);

    public OnDemandRegistry getAvailableByStatusPending(String userAccout, List<SOP> sops);
}
