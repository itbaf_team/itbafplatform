/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.AgeRange;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Friend;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.OauthProvider;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author javier
 */
@lombok.Setter
@lombok.Getter
public class ProfileUserProperty extends UserProperty implements Serializable {

    private static final long serialVersionUID = 201812071724013L;

    public enum Gender {
        MALE, FEMALE, UNKNOWN;

        public static Gender getGender(String gender) {

            if (gender != null) {
                gender = gender.toUpperCase().trim();
                switch (gender) {
                    case "M":
                        return Gender.MALE;
                    case "F":
                        return Gender.FEMALE;
                }
            }
            return Gender.UNKNOWN;
        }
    }

    private String firstName;
    private String middleName;
    private String lastName;
    private String photoURL;
    /**
     * Cuando se actualice manualmente pasar a true y no permitir que se
     * actualice el atributo 'photoURL' de forma automatica
     */
    private boolean isUserPhoto;
    private Date birthdate;
    private AgeRange ageRange;
    private Gender gender;
    private Country country;
    private String state;
    private String address;
    private String zip;
    private String language;
    private String occupation;
    private String siteURL;
    private Map<String, OauthProvider> providers = new HashMap();
    private Map<Long, Friend> friends = new HashMap();
    //Permite saber si es un hijo y fue procesado.
    private boolean processed = false;

    public ProfileUserProperty() {
        super(Topic.PROFILE);
        gender = Gender.UNKNOWN;
        photoURL = "https://assets.planeta.guru/images/oauth/photo.jpg";
        isUserPhoto = false;
    }

    public void setCountry(Country country) {
        this.country = Country.basicClone(country);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("firstName", firstName)
                .add("middleName", middleName)
                .add("lastName", lastName)
                .add("photoURL", photoURL)
                .add("processed", processed)
                .add("isUserPhoto", isUserPhoto)
                .add("birthdate", birthdate)
                .add("ageRange", ageRange)
                .add("gender", gender)
                .add("country", country)
                .add("state", state)
                .add("address", address)
                .add("zip", zip)
                .add("language", language)
                .add("occupation", occupation)
                .add("siteURL", siteURL)
                .add("providers", providers)
                .add("friends", friends)
                .omitNullValues().toString();
    }

}
