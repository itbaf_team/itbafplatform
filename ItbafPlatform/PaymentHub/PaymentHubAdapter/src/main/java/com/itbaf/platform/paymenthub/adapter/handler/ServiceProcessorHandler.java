/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adapter.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.paymenthub.messaging.smpp.adapter.AdapterSMSMessage;
import com.itbaf.platform.paymenthub.messaging.smpp.smppadapter.RcvSmsRequest;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import java.util.Map;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler {

    private final ObjectMapper mapper;
    private final Map<String, Provider> providerMap;
    private final RabbitMQProducer rabbitMQProducer;
    

    @Inject
    public ServiceProcessorHandler(final ObjectMapper mapper,
            final Map<String, Provider> providerMap,
            final RabbitMQProducer rabbitMQProducer) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.providerMap = Validate.notNull(providerMap, "A Map<String, Provider> class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
    }

    /**
     * Recibe un MO desde el smppAdapter (SOAP) y lo envia rapidamente al
     * Rabbit. Se realiza por si llega a ocurrir mucho trafico. El metodo
     * {@link com.itbaf.platform.paymenthub.adapter.processor.MoAdapterProcessor#process(String)}
     * leera el mensaje del rabbit y lo procesara.
     */
    public void processMO(String providerName, RcvSmsRequest rcvSmsRequest) throws Exception {
        AdapterSMSMessage am = new AdapterSMSMessage();
        am.providerName = providerName;
        am.rcvSmsRequest = rcvSmsRequest;
        String json = this.mapper.writeValueAsString(am);
        rabbitMQProducer.messageSender(CommonFunction.ADAPTER_SMS_RCV + "in." + providerName, json);
    }

}
