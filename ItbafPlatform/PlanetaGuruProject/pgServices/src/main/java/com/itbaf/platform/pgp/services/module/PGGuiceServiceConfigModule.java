/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.module;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.SubscriptionManagerImpl;
import com.itbaf.platform.paymenthub.services.repository.AdnetworkNotifierService;
import com.itbaf.platform.paymenthub.services.repository.AdnetworkNotifierSimpleService;
import com.itbaf.platform.paymenthub.services.repository.CountryService;
import com.itbaf.platform.paymenthub.services.repository.OnDemandBillingService;
import com.itbaf.platform.paymenthub.services.repository.OnDemandRegistryService;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.ProviderService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionBillingService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import com.itbaf.platform.paymenthub.services.repository.TariffService;
import com.itbaf.platform.paymenthub.services.repository.transactional.AdnetworkNotifierServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.AdnetworkNotifierSimpleServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.CountryServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.OnDemandBillingServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.OnDemandRegistryServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.PHProfilePropertiesServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.ProviderServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.SopServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.SubscriptionBillingServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.SubscriptionRegistryServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.SubscriptionServiceTransactional;
import com.itbaf.platform.paymenthub.services.repository.transactional.TariffServiceTransactional;
import com.itbaf.platform.pgp.model.wallet.PaymentMethod;
import com.itbaf.platform.pgp.services.aws.s3.AWSFileManagerService;
import com.itbaf.platform.pgp.services.aws.s3.AWSS3FileManagerService;
import com.itbaf.platform.pgp.services.repository.GenericPropertyService;
import com.itbaf.platform.pgp.services.repository.LocalCurrencyService;
import com.itbaf.platform.pgp.services.repository.PaymentMethodService;
import com.itbaf.platform.services.service.TestService;
import com.itbaf.platform.pgp.services.repository.UserService;
import com.itbaf.platform.pgp.services.repository.WalletTransactionService;
import com.itbaf.platform.pgp.services.repository.WalletUserService;
import com.itbaf.platform.pgp.services.repository.transactional.GenericPropertyServiceTransactional;
import com.itbaf.platform.pgp.services.repository.transactional.LocalCurrencyServiceTransactional;
import com.itbaf.platform.pgp.services.repository.transactional.LocaleServiceTransactional;
import com.itbaf.platform.pgp.services.repository.transactional.PaymentMethodServiceTransactional;
import com.itbaf.platform.pgp.services.repository.transactional.UserServiceTransactional;
import com.itbaf.platform.pgp.services.repository.transactional.WalletTransactionServiceTransactional;
import com.itbaf.platform.pgp.services.repository.transactional.WalletUserServiceTransactional;
import com.itbaf.platform.services.service.PropertyManagerService;
import com.itbaf.platform.pgp.services.repository.LocaleService;
import com.itbaf.platform.pgp.services.repository.ProductService;
import com.itbaf.platform.pgp.services.repository.transactional.ProductServiceTransactional;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class PGGuiceServiceConfigModule extends AbstractModule {

    @Override
    protected void configure() {

        //Binds services PG
        bind(UserService.class).to(UserServiceTransactional.class);
        bind(GenericPropertyService.class).to(GenericPropertyServiceTransactional.class);
        bind(AWSFileManagerService.class).to(AWSS3FileManagerService.class);
        bind(ProductService.class).to(ProductServiceTransactional.class);
        bind(LocaleService.class).to(LocaleServiceTransactional.class);
        bind(LocalCurrencyService.class).to(LocalCurrencyServiceTransactional.class);
        bind(WalletTransactionService.class).to(WalletTransactionServiceTransactional.class);
        bind(WalletUserService.class).to(WalletUserServiceTransactional.class);

        //Binds services PH
        bind(ProviderService.class).to(ProviderServiceTransactional.class);
        bind(SopService.class).to(SopServiceTransactional.class);
        bind(CountryService.class).to(CountryServiceTransactional.class);
        bind(SubscriptionService.class).to(SubscriptionServiceTransactional.class);
        bind(SubscriptionBillingService.class).to(SubscriptionBillingServiceTransactional.class);
        bind(PHProfilePropertiesService.class).to(PHProfilePropertiesServiceTransactional.class);
        bind(PaymentMethodService.class).to(PaymentMethodServiceTransactional.class);
        bind(SubscriptionRegistryService.class).to(SubscriptionRegistryServiceTransactional.class);
        bind(OnDemandBillingService.class).to(OnDemandBillingServiceTransactional.class);
        bind(OnDemandRegistryService.class).to(OnDemandRegistryServiceTransactional.class);
        bind(TariffService.class).to(TariffServiceTransactional.class);
        bind(SubscriptionManager.class).to(SubscriptionManagerImpl.class);
        bind(AdnetworkNotifierService.class).to(AdnetworkNotifierServiceTransactional.class);
        bind(AdnetworkNotifierSimpleService.class).to(AdnetworkNotifierSimpleServiceTransactional.class);
    }

    @Provides
    @Singleton
    PropertyManagerService propertyManager(PHProfilePropertiesService property) {
        return property;
    }

    @Provides
    @Singleton
    TestService testService(PaymentMethodService paymentMethodService) {
        return paymentMethodService;
    }

}
