/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.mp.handler;

import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.bn.model.MPNotification;
import com.itbaf.platform.paymenthub.extention.mp.model.ExternalReference;
import com.itbaf.platform.paymenthub.extention.mp.model.Payment;
import com.itbaf.platform.paymenthub.extention.mp.model.PaymentResponse;
import com.itbaf.platform.paymenthub.extention.mp.model.Preapproval;
import com.itbaf.platform.paymenthub.extention.mp.model.PreapprovalResponse;
import com.itbaf.platform.paymenthub.extention.mp.resources.MPResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import java.math.BigDecimal;
import java.util.Base64;
import java.util.concurrent.ExecutorService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final MPResources mpResources;
    private final OnDemandProcessorHandler onDemandProcessorHandler;

    @Inject
    public ServiceProcessorHandler(final MPResources mpResources,
            final OnDemandProcessorHandler onDemandProcessorHandler) {
        this.mpResources = Validate.notNull(mpResources, "A MPResources class must be provided");
        this.onDemandProcessorHandler = Validate.notNull(onDemandProcessorHandler, "An OnDemandProcessorHandler class must be provided");
    }

    public void processNotification(MPNotification notification) {

        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notification);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + notification.countryCode + ".mp", jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notification);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + notification.countryCode + ".mp", jsonResponse);
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {

        MPNotification notification;
        try {
            notification = mapper.readValue(message, MPNotification.class);
            Thread.currentThread().setName(notification.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }
        String jsonNotification;
        String provider = notification.countryCode + ".mp";
        switch (notification.mpTopic) {
            case "preapproval":
                try {
                    jsonNotification = mpResources.getPreapproval(provider, notification.id);
                    log.info("Notificacion de MP. Suscripcion-Preapproval: [" + notification.id + "][" + jsonNotification + "]");
                    PreapprovalResponse preapproval = mapper.readValue(jsonNotification, PreapprovalResponse.class);
                    if ("200".equals(preapproval.getStatus()) || "201".equals(preapproval.getStatus())) {
                        return processPreapprovalResponse(provider, preapproval.getResponse());
                    }
                    log.error("No fue posible procesar la noticacion [" + jsonNotification + "]");
                } catch (Exception ex) {
                    log.error("Error al procesar notificacion de MP para: Preapproval: [" + notification.id + "] ");
                }
                break;
            case "payment":
                try {
                    jsonNotification = mpResources.getPayment(provider, notification.id);
                    log.info("Notificacion de MP. Cobros-Payment: [" + notification.id + "][" + jsonNotification + "]");
                    PaymentResponse payment = mapper.readValue(jsonNotification, PaymentResponse.class);
                    if ("200".equals(payment.getStatus()) || "201".equals(payment.getStatus())) {
                        return processPaymentResponse(provider, payment.getResponse().getCollection());
                    }
                    log.error("No fue posible procesar la noticacion [" + jsonNotification + "]");
                } catch (Exception ex) {
                    log.error("Error al procesar notificacion de MP para: Preapproval: [" + notification.id + "] ");
                }
                break;
            case "authorized_payment":
                log.info("Ignorando notificacion authorized_payment: [" + notification.id + "]");
                return true;
            default:
                log.error("No existe un proceso para MP topic: [" + notification.mpTopic + "]");
                break;
        }

        return null;
    }

    private Boolean processPreapprovalResponse(String provider, Preapproval preapproval) throws Exception {

        if (preapproval.getExternal_reference() == null) {
            log.error("No existe un External_reference para: [" + provider + "][" + preapproval + "]");
            return null;
        }

        PaymentHubMessage msg = new PaymentHubMessage();
        switch (preapproval.getStatus()) {
            case pending:
                // msg.setMessageType(MessageType.SUBSCRIPTION_PENDING);
                // break;
                log.warn("Notificacion de suscripcion PENDING. Ignorada");
                return true;
            case authorized:
                msg.setMessageType(MessageType.SUBSCRIPTION_ACTIVE);
                break;
            case cancelled:
                msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
                msg.setToDate(preapproval.getLast_modified());
                break;
            case paused:
                log.warn("Notificacion de suscripcion PAUSED. Ignorada");
                return true;
        }

        byte[] decodedBytes = Base64.getDecoder().decode(preapproval.getExternal_reference());
        ExternalReference er = mapper.readValue(decodedBytes, ExternalReference.class);
        SOP sop = sopService.findById(er.sopId);
        if (sop == null) {
            log.error("No existe un SOP configurado para el ExternalReference: [" + preapproval.getExternal_reference() + "]");
            return null;
        }

        BigDecimal fullAmount = preapproval.getAuto_recurring().getTransaction_amount();

        Tariff t = sop.getMainTariff();
        if (t != null && fullAmount.compareTo(t.getFullAmount()) > 0) {
            t = updateMainTariff(sop, fullAmount, null);
        }

        if (t == null) {
            log.fatal("No hay un Tariff configurado para la notificacion: [" + preapproval + "] -  SOP: [" + sop + "]");
            return null;
        }

        msg.setCodeCountry(sop.getProvider().getCountry().getCode());
        msg.setExternalCustomer(preapproval.getPayer_id().toString());
        msg.setExternalId(preapproval.getExternal_reference());
        msg.setUserAccount(preapproval.getPayer_email());
        msg.setCurrencyId(preapproval.getAuto_recurring().getCurrency_id());
        msg.setFromDate(preapproval.getDate_created());
        msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
        msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
        msg.setFullAmount(fullAmount);
        msg.setTariffId(t.getId());
        msg.setSopId(sop.getId());

        try {
            Extra extra = new Extra();
            extra.stringData.put("preapproval_Id", preapproval.getId());
            extra.stringData.put("preapproval_BackUrl", preapproval.getBack_url());
            extra.stringData.put("preapproval_Reason", preapproval.getReason());
            extra.stringData.put("preapproval_ApplicationId", preapproval.getApplication_id() + "");
            if (preapproval.getPayment_method_id() != null) {
                extra.stringData.put("preapproval_PaymentMethodId", preapproval.getPayment_method_id());
            }
            msg.setExtra(extra);
        } catch (Exception ex) {
        }

        String jsonResponse = mapper.writeValueAsString(msg);
        if (jsonResponse != null) {
            return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
        }

        return null;
    }

    private Boolean processPaymentResponse(String provider, Payment payment) throws Exception {

        if (Payment.OperationType.regular_payment.equals(payment.getOperation_type())) {//Cobros ondemand
            

            /*     
       {"countryCode" :"ar","notification":{"topic" :"MP_NOTIFICATION","topic":"MP_NOTIFICATION",
       "mpTopic":"merchant_order","id":"995272510","thread":"20190321.200716.306.2546"}}
       
       IF("operation_type" : "regular_payment",)//ONDEMAND
       IF("operation_type": "recurring_payment",) //SUSCRIPTION
       USAR status_detail CON PAYMENT STATUS.PARA EL ERROR
             */
            /**
             * Payment status. pending The user has not yet completed the
             * payment process. approved The payment has been approved and
             * accredited. authorized The payment has been authorized but not
             * captured yet. in_process Payment is being reviewed. in_mediation
             * Users have initiated a dispute. rejected Payment was rejected.
             * The user may retry payment. cancelled Payment was cancelled by
             * one of the parties or because time for payment has expired
             * refunded Payment was refunded to the user. charged_back Was made
             * a chargeback in the buyer’s credit card. /
             */
        } else if (Payment.OperationType.recurring_payment.equals(payment.getOperation_type())) { // cobros recurrentes por suscripcion
           
            if (!Payment.Status.approved.equals(payment.getStatus())) {
                log.info("Notificacion de cobros: [" + payment.getStatus() + "]. Ignorada");
                return true;
            }

            if (payment.getExternal_reference() == null) {
                log.error("No existe un External_reference para: [" + provider + "][" + payment + "]");
                return null;
            }

            byte[] decodedBytes = Base64.getDecoder().decode(payment.getExternal_reference());
            ExternalReference er = mapper.readValue(decodedBytes, ExternalReference.class);
            SOP sop = sopService.findById(er.sopId);
            if (sop == null) {
                log.error("No existe un SOP configurado para el ExternalReference: [" + payment.getExternal_reference() + "]");
                return null;
            }

            BigDecimal fullAmount = payment.getTransaction_amount();
            BigDecimal netAmount = payment.getNet_received_amount();

            Tariff t = sop.findTariffByFullAmount(fullAmount);

            if (t == null) {
                t = updateFullAmountChildrenTariff(sop, fullAmount, sop.getId() + "." + System.currentTimeMillis());
            }

            if (t == null) {
                log.fatal("No hay un Tariff configurado para la notificacion: [" + payment + "] -  SOP: [" + sop + "]");
                return null;
            }

            if (t.getFullAmount().compareTo(fullAmount) != 0 || t.getNetAmount().compareTo(netAmount) != 0) {
                t = updateTariff(t, sop, fullAmount, netAmount);
            }

            PaymentHubMessage msg = new PaymentHubMessage();
            msg.setChargedDate(payment.getDate_approved());
            msg.setCodeCountry(sop.getProvider().getCountry().getCode());
            msg.setCurrencyId(payment.getCurrency_id());
            msg.setExternalCustomer(payment.getPayer().getId());
            msg.setExternalId(payment.getExternal_reference());
            msg.setUserAccount(payment.getPayer().getEmail());

            try {
                Extra extra = new Extra();
                extra.stringData.put("payment_SiteId", payment.getSite_id());
                if (payment.getPayer().getFirst_name() != null) {
                    extra.stringData.put("payment_Payer_FirstName", payment.getPayer().getFirst_name());
                }
                if (payment.getPayer().getLast_name() != null) {
                    extra.stringData.put("payment_Payer_LastName", payment.getPayer().getLast_name());
                }
                if (payment.getPayer().getNickname() != null) {
                    extra.stringData.put("payment_Payer_Nickname", payment.getPayer().getNickname());
                }
                if (payment.getPayer().getPhone() != null && payment.getPayer().getPhone().getNumber() != null) {
                    extra.stringData.put("payment_Payer_Phone_Number", payment.getPayer().getPhone().getNumber());
                }
                if (payment.getPayer().getPhone() != null && payment.getPayer().getPhone().getArea_code() != null) {
                    extra.stringData.put("payment_Payer_Phone_AreaCode", payment.getPayer().getPhone().getArea_code());
                }
                if (payment.getPayer().getPhone() != null && payment.getPayer().getPhone().getExtension() != null && !payment.getPayer().getPhone().getExtension().isEmpty()) {
                    extra.stringData.put("payment_Payer_Phone_Extension", payment.getPayer().getPhone().getExtension());
                }
                if (payment.getPayer().getIdentification() != null && payment.getPayer().getIdentification().getType() != null) {
                    extra.stringData.put("payment_Payer_Identification_Type", payment.getPayer().getIdentification().getType());
                }
                if (payment.getPayer().getIdentification() != null && payment.getPayer().getIdentification().getNumber() != null) {
                    extra.stringData.put("payment_Payer_Identification_Number", payment.getPayer().getIdentification().getNumber());
                }
                if (payment.getPayment_type() != null) {
                    extra.stringData.put("payment_PaymentType", payment.getPayment_type().name());
                }
                if (payment.getPayment_method() != null) {
                    extra.stringData.put("payment_PaymentMethod", payment.getPayment_method());
                }
                if (payment.getCardholder() != null && payment.getCardholder().getIdentification() != null && payment.getCardholder().getIdentification().getNumber() != null) {
                    extra.stringData.put("payment_Cardholder_Identification_Number", payment.getCardholder().getIdentification().getNumber());
                }
                if (payment.getCardholder() != null && payment.getCardholder().getIdentification() != null && payment.getCardholder().getIdentification().getType() != null) {
                    extra.stringData.put("payment_Cardholder_Identification_Type", payment.getCardholder().getIdentification().getType());
                }
                if (payment.getCardholder() != null && payment.getCardholder().getName() != null) {
                    extra.stringData.put("payment_Cardholder_Name", payment.getCardholder().getName());
                }
                if (payment.getAuthorization_code() != null) {
                    extra.stringData.put("payment_AuthorizationCode", payment.getAuthorization_code());
                }
                if (payment.getLast_four_digits() != null) {
                    extra.stringData.put("payment_LastFourDigits", payment.getLast_four_digits());
                }
                msg.setExtra(extra);
            } catch (Exception ex) {
            }
            msg.setFrecuency(1);
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            try {
                PreapprovalResponse preapproval = mapper.
                        readValue(mpResources.getPreapprovalByExternalReference(provider, payment.getExternal_reference()), PreapprovalResponse.class);
                if ("200".equals(preapproval.getStatus()) || "201".equals(preapproval.getStatus())) {
                    msg.setFromDate(preapproval.getResponse().getDate_created());
                } else {
                    throw new Exception("Respuesta erronea de MP: [" + preapproval + "]");
                }
            } catch (Exception ex) {
                log.error("Error al obtener preapproval: [" + payment.getExternal_reference() + "]. " + ex, ex);
                msg.setFromDate(payment.getDate_created());
            }
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setSopId(sop.getId());
            msg.setTariffId(t.getId());
            msg.setTransactionId(payment.getSite_id() + "." + payment.getId());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
            }
        } else {
            log.fatal("No hay una implementacion para el OperationType recibido. [" + payment + "]");
        }

        return null;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();

        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        PreapprovalResponse pr = mpResources.cancelSubscription(subscription.getSop(), subscription.getSubscriptionRegistry().getExtra().stringData.get("preapproval_Id"));
        if ("200".equals(pr.getStatus()) && Preapproval.Status.cancelled.equals(pr.getResponse().getStatus())) {
            rm.status = ResponseMessage.Status.OK;
            subscription.setStatus(Subscription.Status.REMOVED);
        } else {
            log.error("No fue posible cancelar la suscripcion de: [" + subscription + "] [" + pr + "]");
            rm.message = "No fue posible cancelar la suscripcion.";
        }
        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);
        switch (aux.status) {
            case ACTIVE:
            case BLACKLIST:
            case LOCKED:
                rm.status = aux.status;
                rm.message = aux.message;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case PENDING:
            case REMOVED:
                PreapprovalResponse pr = mpResources.createSubscription(subscription.getSop(), subscription.getUserAccount(), subscription.getBackUrl());
                if (pr != null && pr.getResponse() != null && pr.getResponse().getInit_point() != null && !pr.getResponse().getInit_point().isEmpty()) {
                    subscription.setStatus(Subscription.Status.PENDING);
                    subscription.setExternalUrl(pr.getResponse().getInit_point());
                    if (subscription.getSubscriptionRegistry() == null) {
                        subscription.setSubscriptionRegistry(new SubscriptionRegistry());
                    }
                    subscription.getSubscriptionRegistry().setExternalId(pr.getResponse().getExternal_reference());
                    rm.status = ResponseMessage.Status.OK_NS;
                } else {
                    rm.message = "Error. No fue posible realizar la suscripcion [" + pr + "].";
                }
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        try {
            userAccountNormalizer(subscription);
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        Preapproval p = mpResources.getActivePreapproval(subscription.getSop(), subscription.getUserAccount());
        if (p == null) {
            subscription.setStatus(Subscription.Status.REMOVED);
            rm.status = ResponseMessage.Status.REMOVED;
        } else {
            rm.status = ResponseMessage.Status.ACTIVE;
            subscription.setStatus(Subscription.Status.ACTIVE);
            if (subscription.getSubscriptionRegistry() == null) {
                subscription.setSubscriptionRegistry(new SubscriptionRegistry());
            }
            if (!p.getExternal_reference().equals(subscription.getSubscriptionRegistry().getExternalId())) {
                subscription.getSubscriptionRegistry().setId(null);
                subscription.getSubscriptionRegistry().setAdnetworkTracking(null);
                subscription.getSubscriptionRegistry().setExtra(null);
            }
            subscription.setSubscriptionDate(p.getDate_created());
            subscription.getSubscriptionRegistry().setSubscriptionDate(p.getDate_created());
            subscription.getSubscriptionRegistry().setExternalId(p.getExternal_reference());
            subscription.setExternalCustomer(p.getPayer_id().toString());
            try {
                if (subscription.getSubscriptionRegistry().getExtra() == null) {
                    subscription.getSubscriptionRegistry().setExtra(new Extra());
                }
                subscription.getSubscriptionRegistry().getExtra().stringData.put("preapproval_Id", p.getId());
                subscription.getSubscriptionRegistry().getExtra().stringData.put("preapproval_BackUrl", p.getBack_url());
                subscription.getSubscriptionRegistry().getExtra().stringData.put("preapproval_Reason", p.getReason());
                subscription.getSubscriptionRegistry().getExtra().stringData.put("preapproval_ApplicationId", p.getApplication_id() + "");
            } catch (Exception ex) {
            }
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (Exception ex) {
        }

        return rm;
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        if (!subscription.getUserAccount().contains("@")) {
            throw new Exception("Formato de UserAccount invalido.");
        }
        rm.status = ResponseMessage.Status.OK;
        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

}
