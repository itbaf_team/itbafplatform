/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.billing.BillerAttemptTransaction;

/**
 *
 * @author javier
 */
public interface BillerAttemptTransactionService {

    public BillerAttemptTransaction findById(Long id) throws Exception;

    public void saveOrUpdate(BillerAttemptTransaction billerAttempt) throws Exception;

}
