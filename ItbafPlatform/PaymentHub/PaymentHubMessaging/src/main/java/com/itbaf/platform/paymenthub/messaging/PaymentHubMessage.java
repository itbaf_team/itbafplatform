/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.messaging;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class PaymentHubMessage {

    public enum NotificationType {
        SUBSCRIPTION, BILLING, ONDEMAND_BILLING, ADN_SIMPLE
    }

    public enum OriginMessage {
        NOTIFICATION, SMS, SYNC_CHECK, SYNC_RETENTION
    }

    private OriginMessage origin;
    private NotificationType notificationType;
    private MessageType messageType;
    private String codeCountry;// AR, CO, BR, MX, CL...
    private Long sopId;
    private String channelIn;
    private String channelOut;
    private String internalId;//ID de servicios internos (OnDemand)
    private String externalId;//subscriptionId del operador
    private String transactionId;//ID unico de transaccion
    private String userAccount;//email, userid, username...
    private boolean realUserAccount = false; // UserAccount es un MSISDN, un EMAIL?
    private String externalUserAccount;//ID unico en todas las suscripciones proveedor
    private String externalCustomer;//ID del cliente en el proveedor
    private Date toDate;
    private Date fromDate;
    private Date chargedDate;
    private String currencyId;//ARS
    private Integer frecuency;
    private String frequencyType;
    private BigDecimal netAmount;
    private BigDecimal fullAmount;
    private Long tariffId;
    private Map<String, Object> trackingParams = new HashMap();
    private FacadeRequest transactionTracking;
    private Object extra;
    private boolean biller = false; //Este mensaje es para PH.Biller
    public String thread = Thread.currentThread().getName();
    private boolean toDateOverride = true; // Sobreescribir la fecha de suscripcion

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("origin", origin)
                .add("notificationType", notificationType)
                .add("messageType", messageType)
                .add("codeCountry", codeCountry)
                .add("sopId", sopId)
                .add("channelIn", channelIn)
                .add("channelOut", channelOut)
                .add("internalId", internalId)
                .add("externalId", externalId)
                .add("transactionId", transactionId)
                .add("userAccount", userAccount)
                .add("realUserAccount", realUserAccount)
                .add("externalUserAccount", externalUserAccount)
                .add("externalCustomer", externalCustomer)
                .add("toDate", toDate)
                .add("fromDate", fromDate)
                .add("chargedDate", chargedDate)
                .add("currencyId", currencyId)
                .add("frecuency", frecuency)
                .add("frequencyType", frequencyType)
                .add("netAmount", netAmount)
                .add("fullAmount", fullAmount)
                .add("tariffId", tariffId)
                .add("trackingParams", trackingParams)
                .add("transactionTracking", transactionTracking)
                .add("toDateOverride", toDateOverride)
                .add("extra", extra)
                .add("thread", thread)
                .omitNullValues().toString();
    }

}
