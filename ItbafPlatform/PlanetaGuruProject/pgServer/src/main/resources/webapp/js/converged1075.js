
var host = window.location.protocol + "//" + window.location.host;
var canonical = $('link[rel=canonical]')[0].href;
var router = new Navigo(host, false);

!function () {
    router.on({'/account/security': accountSecurityInfo,
        '/pg/account/security': accountSecurityInfo,
        '/account/security/edit/:path': accountSecurityEdit,
        '/pg/account/security/edit/:path': accountSecurityEdit
    }).resolve();
}();
function accountSecurityInfo(params, query) {
    $("#security-landing").css("display", "block");
}

function accountSecurityEdit(params, query) {
    switch (params.path) {
        case "username":
            accountSecurityEditUsername();
            $("#security-landing").css("display", "block");
            break;
        default:
            window.location = canonical;
    }
}

function accountSecurityEditUsername() {
    var html = tmpl($("#securityEditUsernameScript").html(), {"params": uparams});
    $("#security-landing").html(html);

    $.each(uparams.phones, function (index, value) {
        $("#un-m-" + index).data("j", value);
    });
    $.each(uparams.emails, function (index, value) {
        $("#un-e-" + index).data("j", value);
    });

    $(".items-un li").sort(sort_li).appendTo('.items-un');
    function sort_li(a, b) {
        var aa = $(a).data('j');
        var bb = $(b).data('j');

        if (aa.primary)
            return -1;
        if (bb.primary)
            return 1;
        if (aa.confirmed)
            return -1;
        if (bb.confirmed)
            return 1;
        if (aa.email)
            return -1;
        if (bb.email)
            return -1;

        return -1;
    }

    $("#iLinkAddMsisdn").click(function (e) {
        e.preventDefault();
        var html = tmpl($("#addMSISDNDialogUsernameScript").html(), {"params": uparams});
        $("#dialogSec").html(html);
        $("#dialogSec").css("display", "block");
        $("#iBtn_close").click(function () {
            $("#dialogSec").css("display", "none");
        });
        $("#msisdn").intlTelInput({
            geoIpLookup: function (callback) {
                $.post("https://oauth.planeta.guru/oauth/auth/country/ip", function () {}, "json").always(function (resp) {
                    respGeoIP = resp;
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            initialCountry: "auto",
            nationalMode: false,
            utilsScript: "https://assets.planeta.guru/js/oauth/utils.js",
            preferredCountries: uparams.codes
        });
        $('#msisdn').keyup(function () {
            $("#alert").html("");
            $("#msisdn").removeClass("has-error");
        });
        $("#iBtn_action_next").click(function () {
            var phoneCountryCode = $("#msisdn").intlTelInput("getSelectedCountryData");
            var msisdn = $("#msisdn").intlTelInput("getNumber");

            if (msisdn !== undefined && msisdn !== "" && phoneCountryCode) {
                msisdn = msisdn.replace("+" + phoneCountryCode.dialCode, "");
                phoneCountryCode = phoneCountryCode.iso2;
                $("#alert").html("");
                $("#msisdn").removeClass("has-error");

                var params = {};
                params.userId = uparams.userId;
                params.result = msisdn;
                params.clientData = phoneCountryCode;

                $.ajax({
                    type: "PUT",
                    url: canonical + "/edit/username/msisdn",
                    contentType: 'application/json; charset=UTF-8',
                    data: JSON.stringify(params),
                    success: function (data) {
                        location.reload();
                    },
                    error: function (data) {
                        console.log(data);
                        html = tmpl($("#usernameAddMsisdnErrorScript").html(), {"params": uparams});
                        $("#alert").html(html);
                        $("#email").addClass("has-error");
                        ajaxStop = true;
                    },
                    dataType: "json"
                });
            } else {
                html = tmpl($("#usernameMsisdnErrorScript").html(), {"params": uparams});
                $("#alert").html(html);
                $("#email").addClass("has-error");
            }
        });
    });

    $("#iLinkAddEmail").click(function (e) {
        e.preventDefault();
        var html = tmpl($("#addEmailDialogUsernameScript").html(), {"params": uparams});
        $("#dialogSec").html(html);
        $("#dialogSec").css("display", "block");
        $("#iBtn_close").click(function () {
            $("#dialogSec").css("display", "none");
        });
        $('#email').keyup(function () {
            $("#alert").html("");
            $("#email").removeClass("has-error");
        });
        $("#iBtn_action_next").click(function () {
            var email = $("#email").val();
            if (isEmail(email)) {
                $("#alert").html("");
                $("#email").removeClass("has-error");
                var params = {};
                params.userId = uparams.userId;
                params.result = email;

                $.ajax({
                    type: "PUT",
                    url: canonical + "/edit/username/email",
                    contentType: 'application/json; charset=UTF-8',
                    data: JSON.stringify(params),
                    success: function (data) {
                        location.reload();
                    },
                    error: function (data) {
                        console.log(data);
                        html = tmpl($("#usernameAddEmailErrorScript").html(), {"params": uparams});
                        $("#alert").html(html);
                        $("#email").addClass("has-error");
                        ajaxStop = true;
                    },
                    dataType: "json"
                });
            } else {
                html = tmpl($("#usernameEmailErrorScript").html(), {"params": uparams});
                $("#alert").html(html);
                $("#email").addClass("has-error");
            }
        });
    });
    $("#iLinkAddMsisdn").click(function (e) {
        e.preventDefault();
        $("#iBtn_close").click(function () {
            $("#dialogSec").css("display", "none");
        });
    });
}


function verifyMsisdn(e, id) {
    e.preventDefault();
    var value = $("#" + id).data("j");
    var html = tmpl($("#verifyMsisdnDialogUsernameScript").html(), {"value": value});
    $("#dialogSec").html(html);
    $("#dialogSec").css("display", "block");

    $("#iBtn_close").click(function () {
        $("#dialogSec").css("display", "none");
    });

    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('iBtn_action_next', {
        'size': 'invisible',
        'callback': function (response) {
        }
    });

    $("#iBtn_action_next").click(function (e) {
        e.preventDefault();
        var phone = "+" + value.msisdn;
        var appVerifier = window.recaptchaVerifier;
        firebase.auth().signInWithPhoneNumber(phone, appVerifier)
                .then(function (confirmationResult) {
                    window.confirmationResult = confirmationResult;
                    $("#dialogSec").css("display", "none");
                    html = tmpl($("#verifyMsisdnDialogUsernameCodeScript").html(), {"value": value});
                    $("#dialogUserContentMsisdn").html(html);
                    $("#iBtn_action_verify").css("display", "inline-block");
                    $("#iBtn_action_next").css("display", "none");
                    $("#dialogSec").css("display", "none");
                    $("#dialogSec").css("display", "block");

                    $("#codeMsisdn").blur(function () {
                        $("#alertCode").html("");
                    });

                    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('iBtn_action_verify', {
                        'size': 'invisible',
                        'callback': function (response) {
                        }
                    });
                    $("#iBtn_action_verify").on("click", function (e) {
                        var code = $("#codeMsisdn").val();
                        if (code === undefined || code.length < 6) {
                            e.preventDefault();
                            var html = tmpl($("#invalidVerificationCodeScript").html(), {"value": value});
                            $("#alertCode").html(html);
                            return;
                        }
                        startAjax();
                        window.confirmationResult.confirm(code).then(function (result) {
                            firebase.auth().currentUser.getIdToken(true).then(function (codex) {
                                stopAjax();
                                verifyCodeMsisdn(value, codex);
                            }).catch(function (error) {
                                e.preventDefault();
                                stopAjax();
                            });
                        }).catch(function (error) {

                            var html = tmpl($("#invalidVerificationCodeScript").html(), {"value": value});
                            $("#alertCode").html(html);
                            window.recaptchaVerifier.render().then(function (widgetId) {
                                grecaptcha.reset(widgetId);
                            });
                            var appVerifier = window.recaptchaVerifier;
                            firebase.auth().signInWithPhoneNumber(phone, appVerifier)
                                    .then(function (confirmationResult) {
                                        window.confirmationResult = confirmationResult;
                                        stopAjax();
                                    }).catch(function (error) {
                                stopAjax();
                            });
                            e.preventDefault();
                        });
                        e.preventDefault();
                    });
                }).catch(function (error) {
            console.log(error);
            location.reload();
        });
    });

}

function verifyEmail(e, id) {
    e.preventDefault();
    var value = $("#" + id).data("j");

    var html = tmpl($("#verifyEmailDialogUsernameScript").html(), {"value": value});
    $("#dialogSec").html(html);
    $("#dialogSec").css("display", "block");

    $("#iBtn_close").click(function () {
        $("#dialogSec").css("display", "none");
    });

    $("#iBtn_action_next").click(function () {
        var email = value.email;
        var params = {};
        params.userId = uparams.userId;
        params.result = email;

        $.ajax({
            type: "POST",
            url: canonical + "/edit/username/email",
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(params),
            success: function (data) {
                $("#dialogSec").css("display", "none");
                html = tmpl($("#verifyEmailDialogUsernameCodeScript").html(), {"value": value});
                $("#dialogUserContentEmail").html(html);
                $("#iBtn_action_verify").css("display", "inline-block");
                $("#iBtn_action_next").css("display", "none");
                $("#dialogSec").css("display", "none");
                $("#dialogSec").css("display", "block");
                verifyCodeEmail(value);
            },
            error: function (data) {
                console.log(data);
                ajaxStop = true;
                location.reload();
            },
            dataType: "json"
        });
    });
}

function verifyCodeMsisdn(value, codex) {
    var msisdn = value.msisdn;
    var params = {};
    params.userId = uparams.userId;
    params.result = msisdn;
    params.clientData = codex;
    $.ajax({
        type: "POST",
        url: canonical + "/edit/username/msisdn",
        contentType: 'application/json; charset=UTF-8',
        data: JSON.stringify(params),
        success: function (data) {
            ajaxStop = true;
            location.reload();
        },
        error: function (data) {
            ajaxStop = true;
            var html = tmpl($("#invalidVerificationCodeScript").html(), {"value": value});
            $("#alertCode").html(html);
        },
        dataType: "json"
    });
}

function verifyCodeEmail(value) {
    $("#iBtn_action_verify").click(function () {
        var code = $("#codeEmail").val();
        if (code.length === 6) {
            var email = value.email;
            var params = {};
            params.userId = uparams.userId;
            params.result = email;
            params.clientData = code;

            $.ajax({
                type: "POST",
                url: canonical + "/edit/username/email",
                contentType: 'application/json; charset=UTF-8',
                data: JSON.stringify(params),
                success: function (data) {
                    ajaxStop = true;
                    location.reload();
                },
                error: function (data) {
                    ajaxStop = true;
                    var html = tmpl($("#invalidVerificationCodeScript").html(), {"value": value});
                    $("#alertCode").html(html);
                },
                dataType: "json"
            });
        } else {
            var html = tmpl($("#invalidVerificationCodeScript").html(), {"value": value});
            $("#alertCode").html(html);
        }
    });
    $("#codeEmail").blur(function () {
        $("#alertCode").html("");
    });
}

function enabledNext() {
    nextReset(false);
}
function disabledNext() {
    nextReset(true);
}

function nextReset(aux) {
    document.getElementById("iBtn_action_next").disabled = aux;
    document.getElementById("iBtn_action_next").disabled = aux;
}

function makePrimary(e, id) {
    e.preventDefault();
    var value = $("#" + id).data("j");
    var params = {};
    params.userId = uparams.userId;
    if (value.email) {
        params.result = value.email;
    } else {
        params.result = value.msisdn;
    }

    $.ajax({
        type: "POST",
        url: canonical + "/edit/username/primary",
        contentType: 'application/json; charset=UTF-8',
        data: JSON.stringify(params),
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            console.log(data);
            alert(tmpl($("#genericErrorScript").html(), {"value": value}));
            ajaxStop = false;
            location.reload();
        },
        dataType: "json"
    });
}

function removeUN(e, id) {
    e.preventDefault();
    var value = $("#" + id).data("j");
    value.id = id;
    var html = tmpl($("#removeDialogUsernameScript").html(), {"value": value});
    $("#dialogSec").html(html);
    $("#dialogSec").css("display", "block");

    $("#iBtn_close").click(function () {
        $("#dialogSec").css("display", "none");
    });
}

function removeUNDialog(e, id) {
    e.preventDefault();
    var value = $("#" + id).data("j");
    var params = {};
    params.userId = uparams.userId;
    if (value.email) {
        params.result = value.email;
    } else {
        params.result = value.msisdn;
    }

    $.ajax({
        type: "POST",
        url: canonical + "/edit/username/remove",
        contentType: 'application/json; charset=UTF-8',
        data: JSON.stringify(params),
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            console.log(data);
            alert(tmpl($("#genericErrorScript").html(), {"value": value}));
            ajaxStop = false;
            location.reload();
        },
        dataType: "json"
    });
}


function redirectSecurity(e, ru) {
    e.preventDefault();
    var url = ru;
    if (!ru) {
        url = canonical;
    }
    window.location = url;
}