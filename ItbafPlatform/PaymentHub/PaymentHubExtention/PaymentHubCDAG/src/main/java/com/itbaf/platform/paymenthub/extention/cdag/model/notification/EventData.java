package com.itbaf.platform.paymenthub.extention.cdag.model.notification;

@lombok.Getter
public abstract class EventData {

    private String data;

    protected EventData(String data) {
        this.data = data;

        this.parseData();
    }

    protected String getSeparatorExpression() {
        return "\\|";
    }

    protected String[] getValues() {
        return this.getData().split(this.getSeparatorExpression());
    }

    protected abstract void parseData();
}
