package com.itbaf.platform.paymenthub.extention.bn.modules;

import com.google.inject.Singleton;
import com.itbaf.platform.commons.filter.RequestFilter;
import com.itbaf.platform.paymenthub.commons.rest.ManagerRestServiceImpl;
import com.itbaf.platform.paymenthub.extention.bn.filter.TerraRequestFilter;
import com.itbaf.platform.paymenthub.extention.bn.modules.rest.NotificationRestService;
import com.itbaf.platform.paymenthub.extention.bn.modules.rest.TimweNotificationRestService;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import java.util.HashMap;

public class GuiceRestServicesConfigModule extends JerseyServletModule {

    @Override
    protected void configureServlets() {

        bind(ManagerRestServiceImpl.class).in(Singleton.class);
        bind(NotificationRestService.class).in(Singleton.class);
        bind(TimweNotificationRestService.class).in(Singleton.class);
        HashMap<String, String> options = new HashMap();
        options.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

        //Filters
        this.filter("/rest/*").through(RequestFilter.class);
        this.filter("/soap/*").through(RequestFilter.class);
        this.filter("/rest/notification/ec/terra/*").through(TerraRequestFilter.class);

        //Servlets
        serve("/rest/*").with(GuiceContainer.class, options);
        serve("/soap/*").with(GuiceContainer.class, options);
    }
}
