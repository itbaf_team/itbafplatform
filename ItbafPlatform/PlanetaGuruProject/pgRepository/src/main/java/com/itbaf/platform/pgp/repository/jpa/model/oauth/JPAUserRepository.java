/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.repository.jpa.model.oauth;

import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.pgp.model.oauth.QUser;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.querydsl.jpa.impl.JPAQuery;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.xml.ws.Holder;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAUserRepository extends AbstractJPARepositorySession<User, Long> {


    @Inject
    public JPAUserRepository(final Provider<EntityManager> entityManagerProvider) {
        super(User.class, entityManagerProvider);
    }

    public User findByUsername(String type, String value) {
        if ((value == null && value.length() < 1) || type == null) {
            String error = "Alguno de los parametros de User es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }

        type = type.trim().toLowerCase();
        QUser qobj = QUser.user;

        String sql = "SELECT OAUTH_USER.ID FROM OAUTH_USER, OAUTH_USER_PROPERTY "
                + "WHERE OAUTH_USER.ID=OAUTH_USER_PROPERTY.OAUTH_USER_ID "
                + "AND OAUTH_USER_PROPERTY.TOPIC='CREDENTIAL' "
                + "AND JSON_SEARCH(VALUE->'$.username." + type + "s.*." + type + "', 'one', :value) IS NOT NULL "
                + "ORDER BY OAUTH_USER.ID ASC ";

        EntityManager em = getEntityManager();
        List<BigInteger> result = em.createNativeQuery(sql)
                .setParameter("value", value)
                .getResultList();

        if (result == null || result.isEmpty()) {
            return null;
        }

        if (result.size() > 1) {
            log.fatal("Hay muchos usuarios con el mismo value: [" + type + "][" + value + "] - result: [" + Arrays.toString(result.toArray()) + "]");
            /*try {
                for (int i = 1; i < result.size(); i++) {
                    JPAQuery<?> query = new JPAQuery(em);
                    User obj = query.select(qobj).from(qobj).
                            where(qobj.id.eq(result.get(0).longValue())).fetchFirst();
                    Long userId = result.get(i).longValue();
                    log.warn("Se procede a eliminar el user: [" + userId + "][" + obj + "]");

                    walletTransactionRepository.deleteByUserId(userId);
                    walletUserRepository.deleteByUserId(userId);
                    delete(result.get(i).longValue());
                }
            } catch (Exception ex) {
            }*/
        }

        JPAQuery<?> query = new JPAQuery(em);
        User obj = query.select(qobj).from(qobj).
                where(qobj.id.eq(result.get(0).longValue())).fetchFirst();

        return obj;
    }

    public User findByclientId(String clientId) {
        if (clientId == null && clientId.length() < 5) {
            String error = "El parametro clientId de User es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }

        QUser qobj = QUser.user;

        String sql = "SELECT OAUTH_USER.ID FROM OAUTH_USER, OAUTH_USER_PROPERTY "
                + "WHERE OAUTH_USER.ID=OAUTH_USER_PROPERTY.OAUTH_USER_ID "
                + "AND OAUTH_USER_PROPERTY.TOPIC='CREDENTIAL' "
                + "AND JSON_CONTAINS(VALUE, '{\"clientId\":\"" + clientId + "\"}', '$.client') ";

        EntityManager em = getEntityManager();
        List<BigInteger> result = em.createNativeQuery(sql)
                .getResultList();

        if (result == null || result.size() != 1) {
            return null;
        }

        JPAQuery<?> query = new JPAQuery(em);
        User obj = query.select(qobj).from(qobj).
                where(qobj.id.eq(result.get(0).longValue())).fetchFirst();

        return obj;
    }

    public User findByMsisdn(String prefix, String msisdn, Holder<String> username) {
        if (prefix == null || msisdn == null) {
            String error = "Alguno de los parametros es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }

        String sql = "SELECT * FROM OAUTH_USER, OAUTH_USER_PROPERTY "
                + "WHERE OAUTH_USER.ID=OAUTH_USER_PROPERTY.OAUTH_USER_ID "
                + "AND OAUTH_USER_PROPERTY.TOPIC='CREDENTIAL' "
                + "AND JSON_SEARCH(VALUE->'$.username.msisdns.*.msisdn', 'all', '%" + msisdn + "') IS NOT NULL "
                + "ORDER BY OAUTH_USER.ID ASC ";

        EntityManager em = getEntityManager();
        List<User> result = em.createNativeQuery(sql, User.class).getResultList();

        if (result == null || result.isEmpty()) {
            return null;
        }

        for (User u : result) {
            for (Msisdn m : u.getCredentialUserProperty().getUsername().getMsisdns().values()) {
                if (m.getMsisdn().startsWith(prefix) && m.getMsisdn().endsWith(msisdn)) {
                    username.value = m.getMsisdn();
                    return u;
                }
            }
        }

        return null;
    }

    public User findByNickname(String nickname) {
        if (nickname == null) {
            String error = "Alguno de los parametros es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }

        nickname = nickname.replace("'", "").replace(" ", "").replace("\"", "");

        String sql = "SELECT * FROM OAUTH_USER, OAUTH_USER_PROPERTY "
                + "WHERE OAUTH_USER.ID=OAUTH_USER_PROPERTY.OAUTH_USER_ID "
                + "AND OAUTH_USER_PROPERTY.TOPIC='CREDENTIAL' "
                + "AND JSON_SEARCH(UPPER(VALUE->'$.username.nickname'), 'one', UPPER('" + nickname + "')) IS NOT NULL ";

        EntityManager em = getEntityManager();
        List<User> result = em.createNativeQuery(sql, User.class).getResultList();

        if (result == null || result.isEmpty()) {
            return null;
        }

        return result.get(0);
    }

    public List<User> getAllUserClients() {
        QUser qobj = QUser.user;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<User> users = query.select(qobj).from(qobj).
                where(qobj.products.isNotEmpty())
                .fetch();

        return users;
    }

    public List<User> findByIds(List<Long> ids) {
        QUser qobj = QUser.user;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<User> users = query.select(qobj).from(qobj).
                where(qobj.id.in(ids))
                .fetch();

        return users;
    }

}
