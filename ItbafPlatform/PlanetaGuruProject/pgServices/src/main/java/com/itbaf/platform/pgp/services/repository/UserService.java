/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository;

import com.itbaf.platform.pgp.model.oauth.User;
import io.jsonwebtoken.impl.DefaultClaims;
import java.util.List;
import javax.xml.ws.Holder;

/**
 *
 * @author javier
 */
public interface UserService {

    public void saveOrUpdate(User user) throws Exception;

    public User findById(Long id);
    
    public List<User> findByIds(List<Long> ids);

    public User findByclientId(String clientId);

    public User findByEmail(String email);

    public User findByMsisdn(String msisdn);

    public User findByMsisdn(String prefix, String msisdn, Holder<String> username);

    public User findByNickname(String nickname);

    public List<User> getAllUserClients();

    public User authenticateUser(User client, String userToken, DefaultClaims defaultClaims);

    public List<User> getAll();
}
