package com.itbaf.platform.pgc.api.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage;
import com.itbaf.platform.messaging.rest.oauth.RequestTokenMessage.GrantType;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgc.api.handler.ApiAccountServiceProcessorHandler;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;

@Path("/api/account")
@Singleton
@lombok.extern.log4j.Log4j2
public class ApiAccountRestService extends CommonRestService {

    private final ApiAccountServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public ApiAccountRestService(ApiAccountServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "An ApiAccountServiceProcessorHandler class must be provided");
    }

    @POST
    @Path("/token")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response tokenAccount(@Context HttpServletRequest request,
            @FormParam("grant_type") final GrantType grantType,
            @FormParam("client_id") final String clientId,
            @FormParam("code_verifier") final String codeVerifier,
            @FormParam("access_token") final String accessToken) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.account.token");
        } catch (Exception ex) {
        }
        RequestTokenMessage tm = new RequestTokenMessage();
        tm.grantType = grantType;
        tm.clientId = clientId;
        tm.codeVerifier = codeVerifier;
        tm.accessToken = accessToken;

        log.info("tokenAccount. RequestTokenMessage: [" + tm + "]");

        PGMessageResponse rm = new PGMessageResponse();

        if (isHeavyUser(request, "tokenAccount." + accessToken)) {
            log.fatal("Request de un HeavyUser. [" + tm + "]");
            rm.meta.code = 400;
            rm.meta.status = Meta.Status.invalid_request;
            rm.data.message = "Peticion no autorizada";
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        if (codeVerifier == null || codeVerifier.length() < 43) {
            log.warn("Request con codeVerifier invalido. [" + tm + "]");
            rm.meta.code = 400;
            rm.meta.status = Meta.Status.unauthorized_client;
            rm.data.message = "Peticion no autorizada";
            return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        try {
            switch (grantType) {
                case authorization_code:
                    rm = serviceProcessorHandler.getUserAuthorizationCode(tm);
                    break;
                default:
                    rm.meta.code = 400;
                    rm.meta.status = Meta.Status.invalid_grant;
                    rm.data.message = "Peticion no autorizada";
            }
        } catch (Exception ex) {
            rm = new PGMessageResponse();
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            rm.data.message = "Error al obtener Codigo de Autorizacion. " + ex;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("OK").cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

}
