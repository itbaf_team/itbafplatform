function checkStrength(password) {
    var strength = 0;
    if (password.length < 8) {
        $('#result').removeClass();
        $('#result').addClass('short');
        return {id: 1, msg: '<i18n>i18n.ix.px.text.too.short</i18n>'};
    }
    if (password.length > 7)
        strength += 1;
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
        strength += 1;
    if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
        strength += 1;
    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
        strength += 1;
    if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
        strength += 1;
    if (strength < 2) {
        $('#result').removeClass();
        $('#result').addClass('weak');
        return {id: 2, msg: '<i18n>i18n.ix.px.text.weak</i18n>'};
    } else if (strength === 2) {
        $('#result').removeClass();
        $('#result').addClass('good');
        return {id: 3, msg: '<i18n>i18n.ix.px.text.good.f</i18n>'};
    } else {
        $('#result').removeClass();
        $('#result').addClass('strong');
        return {id: 4, msg: '<i18n>i18n.ix.px.text.strong.f</i18n>'};
    }
}
