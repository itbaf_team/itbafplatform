package com.itbaf.platform.bo.server.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.bo.server.filter.OauthPermission;
import com.itbaf.platform.bo.server.handler.ApiSubscriptionServiceProcessorHandler;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageRequest;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;

@Path("/api/subscription")
@Singleton
@lombok.extern.log4j.Log4j2
public class SubscriptionApiRestService extends CommonRestService {

    private final ApiSubscriptionServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public SubscriptionApiRestService(final ApiSubscriptionServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "An ApiSubscriptionServiceProcessorHandler class must be provided");
    }

    @POST
    @Path("/search")
    @OauthPermission("api_operations_subscriptions_search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postSearchSubscriptions(@Context HttpServletRequest request, PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.post.subscriptions.search");
        } catch (Exception ex) {
        }
        log.info("postSearchSubscriptions. PGMessageRequest: [" + messageRequest + "]");
        PGMessageResponse rm = new PGMessageResponse();
        try {
            if (messageRequest.userAccount != null) {
                User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
                rm = serviceProcessorHandler.getSubscriptions(user, messageRequest);
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener informacion del perfil. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/delete")
    @OauthPermission("api_operations_subscriptions_cancelar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSubscription(@Context HttpServletRequest request, PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.delete.subscription");
        } catch (Exception ex) {
        }
        log.info("deleteSubscription. PGMessageRequest: [" + messageRequest + "]");
        PGMessageResponse rm = new PGMessageResponse();
        try {
            if (messageRequest.userAccount != null) {
                User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
                rm = serviceProcessorHandler.deleteActiveSubscription(user, messageRequest);
            }
        } catch (Exception ex) {
            rm.data.message = "Error al cancelar suscripcion. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @POST
    @Path("/billing/history")
    @OauthPermission("api_operations_subscriptions_billing_history")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postSearchBillingHistory(@Context HttpServletRequest request, PGMessageRequest messageRequest) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.post.subscriptions.billing.history");
        } catch (Exception ex) {
        }
        log.info("postSearchBillingHistory. PGMessageRequest: [" + messageRequest + "]");
        PGMessageResponse rm = new PGMessageResponse();
        try {
            if (messageRequest.userAccount != null) {
                User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
                rm = serviceProcessorHandler.searchBillingHistory(user, messageRequest);
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener el historial de cobros. " + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }
}
