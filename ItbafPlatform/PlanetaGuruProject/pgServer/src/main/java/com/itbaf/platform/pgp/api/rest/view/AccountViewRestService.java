package com.itbaf.platform.pgp.api.rest.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.pgp.api.handler.AccountViewServiceProcessorHandler;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.commons.resources.CookieFilter;
import com.itbaf.platform.pgp.model.oauth.User;
import io.jsonwebtoken.impl.DefaultClaims;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/")
@Singleton
@lombok.extern.log4j.Log4j2
public class AccountViewRestService {

    private final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler;

    @Inject
    public AccountViewRestService(final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler) {
        this.accountViewServiceProcessorHandler = Validate.notNull(accountViewServiceProcessorHandler, "An AccountViewServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public Response account(@Context HttpServletRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account");
        } catch (Exception ex) {
        }
        log.info("account. QS: [" + request.getQueryString() + "]");

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            String productId = request.getAttribute(CommonAttribute.PRODUCT_ID).toString();
            if (client != null && user != null) {
                String redirectUri = request.getAttribute(CommonAttribute.U_REDIRECT_URI).toString();
                String html = accountViewServiceProcessorHandler.getUserAccountHtml(client, user, productId, redirectUri);
                if (html != null) {
                    String prvdr = null;
                    try {
                        DefaultClaims dc = (DefaultClaims) request.getAttribute(CommonAttribute.U_DEFAULT_CLAIMS);
                        prvdr = dc.get("prvdr", String.class);
                    } catch (Exception ex) {
                    }
                    html = accountViewServiceProcessorHandler.i18n(user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage(), prvdr, html);
                    return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar peticion. " + ex, ex);
        }

        return Response.status(Status.UNAUTHORIZED).build();
    }

    @GET
    @Path("/signout/{path}")
    @Produces(MediaType.TEXT_HTML)
    public Response accountSignOut(@Context HttpServletRequest request,
            @PathParam("path") final String path) throws URISyntaxException {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.signout." + path);
        } catch (Exception ex) {
        }
        log.info("accountSignOut. path: [" + path + "]");

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            CookieFilter cf = (CookieFilter) request.getAttribute(CommonAttribute.U_COOKIE);
            if (client != null && user != null && cf != null) {
                accountViewServiceProcessorHandler.signOutUserAccount(path, client, user, cf);
            }
        } catch (Exception ex) {
            log.error("Error al procesar peticion. " + ex, ex);
        }

        return Response.status(302).location(new URI("/")).build();
    }

}
