/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.mp.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.paymenthub.extention.mp.model.ExternalReference;
import com.itbaf.platform.paymenthub.extention.mp.model.Preapproval;
import com.itbaf.platform.paymenthub.extention.mp.model.PreapprovalListResponse;
import com.itbaf.platform.paymenthub.extention.mp.model.PreapprovalResponse;
import com.itbaf.platform.paymenthub.extention.mp.model.PreferenceResponse;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.mercadopago.MP;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.Validate;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class MPResources {

    private final ObjectMapper mapper;
    private final PHProfilePropertiesService profilePropertiesService;

    private static final Cache<String, MP> mpCache = CacheBuilder.newBuilder()
            .expireAfterWrite(5, TimeUnit.HOURS) //Tiempo de vida
            .build();

    @Inject
    public MPResources(final ObjectMapper mapper,
            final PHProfilePropertiesService profilePropertiesService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    private MP getMP(String provider) {
        MP mp = mpCache.getIfPresent(provider);
        if (mp == null) {
            mp = new MP(profilePropertiesService.getValueByProviderKey(provider, "external.service.client.id"),
                    profilePropertiesService.getValueByProviderKey(provider, "external.service.client.secret"));
            mpCache.put(provider, mp);
        }
        return mp;
    }

    public String getPreapproval(String provider, String id) throws Exception {
        String response = getMP(provider).getPreapprovalPayment(id).toString();
        log.info("MP getPreapproval. [" + provider + "][" + id + "]. Response: [" + response + "] ");
        return response;
    }

    public String getPayment(String provider, String id) throws Exception {
        String response = getMP(provider).getPayment(id).toString();
        log.info("MP getPayment. [" + provider + "][" + id + "]. Response: [" + response + "] ");
        return response;
    }

    public String getPreapprovalByExternalReference(String provider, String externalReference) throws Exception {
        Map<String, Object> props = new HashMap();
        props.put("external_reference", externalReference);
        String response = getMP(provider).get("/preapproval/search", props).toString();
        log.info("MP getPreapprovalByExternalReference. [" + provider + "][" + externalReference + "]. Response: [" + response + "] ");
        return response;
    }

    public PreapprovalResponse createSubscription(SOP sop, String userAccount, String backUrl) throws Exception {
        ExternalReference er = new ExternalReference();
        er.externalId = "MP." + CommonFunction.getTID();
        er.sopId = sop.getId();
        byte[] encodedBytes = Base64.getEncoder().encode(mapper.writeValueAsBytes(er));

        Tariff t = sop.getMainTariff();

        String preapprovalData = "{"
                + "\"payer_email\":\"" + userAccount + "\","
                + "\"back_url\":\"" + backUrl + "\","
                + "\"reason\":\"" + sop.getIntegrationSettings().get("mp.reason") + "\","
                + "\"external_reference\":\"" + new String(encodedBytes) + "\","
                + "\"auto_recurring\": {"
                + "\"frequency\":" + t.getFrecuency() + ","
                + "\"frequency_type\":\"" + t.getFrecuencyType() + "\","
                + "\"transaction_amount\":" + t.getFullAmount().toPlainString() + ","
                + "\"currency_id\":\"" + t.getCurrency() + "\""
                + "}"
                + "}";

        JSONObject preapprovalJson = getMP(sop.getProvider().getName()).createPreapprovalPayment(preapprovalData);
        String json = preapprovalJson.toString();
        log.info("MP createSubscription. [" + userAccount + "][" + sop.getProvider().getName() + "]. Response: [" + json + "] ");
        return mapper.readValue(json, PreapprovalResponse.class);
    }

    public Preapproval getActivePreapproval(SOP sop, String userAccount) throws Exception {

        Map<String, Object> props = new HashMap();
        props.put("payer_email", userAccount);
        props.put("status", "authorized");
        String json = getMP(sop.getProvider().getName()).get("/preapproval/search", props).toString();
        log.info("MP getActivePreapproval. [" + userAccount + "][" + sop.getProvider().getName() + "]. Response: [" + json + "] ");
        PreapprovalListResponse plr = mapper.readValue(json, PreapprovalListResponse.class);
        if ("200".equals(plr.getStatus()) || "201".equals(plr.getStatus())) {
            if (plr.getResponse() != null && plr.getResponse().results != null && plr.getResponse().results.length > 0) {
                for (Preapproval pa : plr.getResponse().results) {
                    try {
                        if (pa.getExternal_reference() != null) {
                            byte[] decodedBytes = Base64.getDecoder().decode(pa.getExternal_reference());
                            ExternalReference er = mapper.readValue(decodedBytes, ExternalReference.class);
                            if (sop.getId().equals(er.sopId)) {
                                return pa;
                            }
                        }
                    } catch (Exception ex) {
                    }
                }
            }
            return null;
        }

        throw new Exception("Error al obtener getActivePreapproval. MP response: [" + json + "]");
    }

    public PreapprovalResponse cancelSubscription(SOP sop, String preapprovalId) throws Exception {
        JSONObject preapprovalJson = getMP(sop.getProvider().getName()).cancelPreapprovalPayment(preapprovalId);
        String json = preapprovalJson.toString();
        log.info("MP cancelSubscription. [" + preapprovalId + "][" + sop.getProvider().getName() + "]. Response: [" + json + "] ");
        return mapper.readValue(json, PreapprovalResponse.class);
    }

    public PreferenceResponse createPreference(SOP sop, OnDemandRegistry or) throws Exception {

        Tariff t = sop.getMainTariff();

        String preference = "{'items': [{"
                + "    'id': '" + or.getTransactionTracking().contentId + "',"
                + "    'title': '" + or.getTransactionTracking().contentName + "',"
                + "    'currency_id': '" + t.getCurrency() + "',"
                + "    'picture_url': '" + or.getTransactionTracking().pictureUrl + "',"
                + "    'description': '" + or.getTransactionTracking().contentDescription + "',"
                + "    'category_id': '" + or.getTransactionTracking().contentType.name().toLowerCase() + "',"
                + "    'quantity': 1,"
                + "    'unit_price': " + t.getFullAmount()
                + " }],"
                + "'payer': {'email': '" + or.getUserAccount() + "'},"
                + "'back_urls': {"
                + "     'success': '" + or.getTransactionTracking().contentUrl + "',"
                + "     'failure': '" + or.getTransactionTracking().failureUrl + "',"
                + "     'pending': '" + or.getTransactionTracking().pendingUrl + "'"
                + "},"
                + "'auto_return': 'approved',"
                + "'external_reference': '" + or.getTransactionId() + "'"
                //  + "'expires': true,"
                // + "'expiration_date_from': '2019-03-21T00:00:00.000-00:00',"
                //  + "'expiration_date_to': '2016-03-22T03:00:00.000-00:00'"
                + "}";

        JSONObject preferenceJson = getMP(sop.getProvider().getName()).createPreference(preference);
        String json = preferenceJson.toString();
        log.info("MP createPreference. [" + or.getUserAccount() + "][" + sop.getProvider().getName() + "]. Response: [" + json + "] ");
        return mapper.readValue(json, PreferenceResponse.class);
    }

}
