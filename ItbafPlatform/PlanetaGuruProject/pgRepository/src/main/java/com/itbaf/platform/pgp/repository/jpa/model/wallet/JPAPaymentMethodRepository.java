/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.repository.jpa.model.wallet;

import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.pgp.model.wallet.PaymentMethod;
import com.itbaf.platform.pgp.model.wallet.QPaymentMethod;
import com.querydsl.jpa.impl.JPAQuery;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAPaymentMethodRepository extends AbstractJPARepositorySession<PaymentMethod, Long> {

    @Inject
    public JPAPaymentMethodRepository(final Provider<EntityManager> entityManagerProvider) {
        super(PaymentMethod.class, entityManagerProvider);
    }

    public List<PaymentMethod> findByCountry(Country country) {
        if (country == null | country.getId() == null) {
            String error = "El valor de countryCode es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }

        List<PaymentMethod> pml = null;
        JPAQuery<?> query = new JPAQuery(getEntityManager());
        QPaymentMethod pm = QPaymentMethod.paymentMethod;
        pml = query.select(pm).from(pm).
                where(pm.sop().provider().country().id.eq(country.getId()))
                .fetch();

        return pml;
    }

}
