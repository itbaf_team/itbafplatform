/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.QServiceMatcher;
import com.itbaf.platform.paymenthub.model.ServiceMatcher;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.querydsl.jpa.impl.JPAQuery;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAServiceMatcherRepository extends AbstractJPARepositorySession<ServiceMatcher, Long> {

    private final JPAServiceRepository jpaServiceRepository;

    @Inject
    public JPAServiceMatcherRepository(final Provider<EntityManager> entityManagerProvider,
            final JPAServiceRepository jpaServiceRepository) {
        super(ServiceMatcher.class, entityManagerProvider);
        this.jpaServiceRepository = jpaServiceRepository;
    }

    public ServiceMatcher findByName(String name) {
        if (name == null) {
            String error = "El nombre del ServiceMatcher es nulo!!";
            log.error(error);

            throw new RuntimeException(error);
        }

        QServiceMatcher service = QServiceMatcher.serviceMatcher;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        ServiceMatcher s = query.select(service).from(service).
                where(service.name.eq(name)).fetchFirst();

        return s;
    }

    @Override
    public void save(ServiceMatcher sm) throws Exception {

        if (sm.getService() == null) {
            //Servicio generico por default.
            sm.setService(jpaServiceRepository.findById(1L));
        }

        super.save(sm);
    }

}
