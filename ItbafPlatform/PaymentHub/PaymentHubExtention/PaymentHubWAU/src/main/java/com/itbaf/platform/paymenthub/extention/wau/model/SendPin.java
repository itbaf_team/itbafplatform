package com.itbaf.platform.paymenthub.extention.wau.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "request_data")
public class SendPin {

    @JacksonXmlElementWrapper
    private String access_token;

    @JacksonXmlElementWrapper
    private String integrator_id;

    @JacksonXmlElementWrapper
    private String identifier;

    @JacksonXmlElementWrapper
    private String pin_code;

    @JacksonXmlElementWrapper
    private String request_id;

    public SendPin() {
    }

    public SendPin(String access_token, String integrator_id, String identifier, String pin_code, String request_id) {
        this.access_token = access_token;
        this.integrator_id = integrator_id;
        this.identifier = identifier;
        this.pin_code = pin_code;
        this.request_id = request_id;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getIntegrator_id() {
        return integrator_id;
    }

    public void setIntegrator_id(String integrator_id) {
        this.integrator_id = integrator_id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getPin_code() {
        return pin_code;
    }

    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }
}
