/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import java.util.concurrent.TimeUnit;
import org.redisson.api.RMapCache;

/**
 *
 * @author JF
 */
public class CacheObject {

    private final Integer duration;
    private final TimeUnit timeUnit;
    private final static String MAP_NAME = "MainMemoryCache_";
    private final String localMapName;
    private final RMapCache<Object, Object> redisMemory;
    private final Cache<Object, Object> localMemory;

    public CacheObject(final Integer duration, final TimeUnit timeUnit) {
        this.duration = duration;
        this.timeUnit = timeUnit;
        this.localMapName = MAP_NAME + duration + "_" + timeUnit.name();
        this.redisMemory = InstrumentedObject.redis.getMapCache(localMapName);
        this.localMemory = CacheBuilder.newBuilder()
                .expireAfterWrite(duration, timeUnit)
                .build();
    }

    public Object getIfPresent(Object key) {
        Object o = localMemory.getIfPresent(key);
        if (o == null) {
            return redisMemory.get(key);
        }
        return o;
    }

    public Object put(Object key, Object object) {
        localMemory.put(key, object);
        return redisMemory.put(key, object, duration, timeUnit);
    }

    public void clear() {
        try {
            localMemory.invalidateAll();
        } catch (Exception ex) {
        }
        try {
            redisMemory.clear();
        } catch (Exception ex) {
        }
    }
}
