
package com.itbaf.platform.paymenthub.extention.cdag.ws.subscription;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="status" type="{urn:SendatelSoaService}statusResponse"/>
 *         &lt;element name="possibleValues" type="{urn:SendatelSoaService}arrayOfExtraParam" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "status",
    "possibleValues"
})
@XmlRootElement(name = "getPossibleValuesResponse")
public class GetPossibleValuesResponse {

    @XmlElement(required = true)
    protected StatusResponse status;
    protected ArrayOfExtraParam possibleValues;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link StatusResponse }
     *     
     */
    public StatusResponse getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusResponse }
     *     
     */
    public void setStatus(StatusResponse value) {
        this.status = value;
    }

    /**
     * Gets the value of the possibleValues property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public ArrayOfExtraParam getPossibleValues() {
        return possibleValues;
    }

    /**
     * Sets the value of the possibleValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfExtraParam }
     *     
     */
    public void setPossibleValues(ArrayOfExtraParam value) {
        this.possibleValues = value;
    }

}
