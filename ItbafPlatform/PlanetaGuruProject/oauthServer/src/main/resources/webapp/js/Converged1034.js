var router = new Navigo(window.location.protocol + "//" + window.location.host, false);

!function () {
    router.on({'/oauth/signin/resetPassword/:tid': resetForm,
        '/oauth/signin/reset/password': codeForm,
        '/oauth/signin/secure/code': codeForm,
        '/oauth/signin/secure/reset': resetPwd
    }).resolve();
}();

function resetForm(params, query) {
    params.locale = getLocaleScript('converged1034');
    $("#iSignIn").attr("href", "/oauth/signin/login/" + params.tid + "?locale=" + params.locale);
    $("#form-data").html("");

    var html = hiddenParameter("tid", params.tid);
    $(html).appendTo("#form-var");
    html = tmpl($("#resetForm").html(), {"params": params});
    $(html).fadeIn("slow").appendTo("#form-data");
    if (query) {
        var un = getParameterByName("un");
        if (un) {
            params.username = un;
            $("#iSigninName").val(params.username);
        }
    }

    $("#resetPwdHipCancel").on("click", function (e) {
        // e.preventDefault();
        //  var url = "/oauth/signin/login/" + params.tid + "?locale=" + params.locale;
        // window.location.href = url;
        parent.history.back();
        return false;
    });
    $("#resetPwdHipAction").on("click", function (e) {
        e.preventDefault();
        var email = $("#iSigninName").val();
        if (isInteger(email) && email.length > 6) {
            pushEvent('/oauth/signin/reset/password/number');
            email = getInteger(email);
            params.phone = email;
            params.username = email;
            phoneLoginForm(params);
        } else if (isEmail(email)) {
            pushEvent('/oauth/signin/reset/password/email');
            params.usernameType = "EMAIL";
            params.username = email;
            preFPost(params);
        } else {
            pushEvent('/oauth/signin/reset/password/input/error');
            html = tmpl($("#usernameErrorScript").html(), {"params": params});
            $("#pMemberNameErr").html(html);
            $("#iSigninName").addClass("has-error");
        }
    });
}

function resetPwd(params, query) {
    params = pparams;
    params.locale = getLocaleScript('converged1034');
    $("#iSignIn").attr("href", "/oauth/signin/login/" + params.tid + "?locale=" + params.locale);
    if (params.credentials && params.credentials.error === 10) {
        var html = tmpl($("#successResetForm").html(), {"params": params});
        $(html).fadeIn("slow").appendTo("#form-data");
        $("#signinButton").on("click", function (e) {
            e.preventDefault();
            var url = "/oauth/signin/login/" + params.tid + "?locale=" + params.locale;
            window.location.href = url;
        });
    } else {
        params.code = getParameterByName("code");
        $("#form-data").html("");
        $("#resetPwdForm").attr("action", "/oauth/signin/secure/reset");
        var html = tmpl($("#pwdResetForm").html(), {"params": params});
        $(html).fadeIn("slow").appendTo("#form-data");
        document.getElementById("signinButton").disabled = true;
        $('#passwd').keyup(function () {
            html = checkStrength($('#passwd').val());
            $('#result').html(html.msg);
            switch (html.id) {
                case 1:
                    html = tmpl($("#tooShortPwd").html(), {"params": params});
                    $("#alertPwd").html(html);
                    document.getElementById("signinButton").disabled = true;
                    break;
                case 2:
                    html = tmpl($("#weakPwd").html(), {"params": params});
                    $("#alertPwd").html(html);
                    document.getElementById("signinButton").disabled = true;
                    break;
                case 3:
                    document.getElementById("signinButton").disabled = false;
                    $("#alertPwd").html("");
                    break;
                case 4:
                    document.getElementById("signinButton").disabled = false;
                    $("#alertPwd").html("");
                    break;
            }
        });
        $("#signinButton").on("click", function (e) {
            var passwd = $("#passwd").val();
            var rpasswd = $("#rpasswd").val();
            if (passwd !== rpasswd) {
                e.preventDefault();
                $("#passwd").addClass("has-error");
                $("#rpasswd").addClass("has-error");
                var html = tmpl($("#pwdnmatch").html(), {"params": params});
                $("#alert").html(html);
                return;
            }
        });
    }
}

function codeForm(params, query) {
    params = pparams;
    params.locale = getLocaleScript('converged1034');
    $("#iSignIn").attr("href", "/oauth/signin/login/" + params.tid + "?locale=" + params.locale);
    $("#form-data").html("");
    var html = hiddenParameter("tid", params.tid);
    $(html).appendTo("#form-var");
    if (params.ifExistsResult === 1) {
        html = tmpl($("#codeResetForm").html(), {"params": params});
        $(html).fadeIn("slow").appendTo("#form-data");
        $("#resetPwdForm").attr("action", "/oauth/signin/secure/code");
        if (params.usernameType && params.usernameType === 'PHONE') {
            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('signinButton', {
                'size': 'invisible',
                'callback': function (response) {
                }
            });

            $("#signinButton").on("click", function (e) {
                var code = $("#code").val();
                if (code === undefined || code.length !== 6) {
                    e.preventDefault();
                    $("#code").addClass("has-error");
                    html = tmpl($("#verificationCodeEmptyScript").html(), {"params": params});
                    $("#alert").html(html);
                    return;
                }
                window.confirmationResult.confirm(code).then(function (result) {
                    firebase.auth().currentUser.getIdToken(true).then(function (code) {
                        params.code = code;
                        var html = hiddenParameter("tokenCode", params.code)
                                + hiddenParameter("username", params.username)
                                + hiddenParameter("usernameType", params.usernameType);
                        $(html).appendTo("#form-var");
                        $("#resetPwdForm").submit();
                    }).catch(function (error) {
                        e.preventDefault();
                    });
                }).catch(function (error) {
                    html = tmpl($("#invalidVerificationCodeScript").html(), {"params": params});
                    $("#alert").html(html);
                    window.recaptchaVerifier.render().then(function (widgetId) {
                        grecaptcha.reset(widgetId);
                    });
                    var appVerifier = window.recaptchaVerifier;
                    firebase.auth().signInWithPhoneNumber("+" + params.username, appVerifier)
                            .then(function (confirmationResult) {
                                $("#alert").html("");
                                window.confirmationResult = confirmationResult;
                            }).catch(function (error) {
                    });
                    e.preventDefault();
                });
                e.preventDefault();
            });
        } else {
            if (params.credentials.error === 1) {
                html = tmpl($("#invalidVerificationCodeScript").html(), {"params": params});
                $("#alert").html(html);
            } else if (params.credentials.error === 2) {
                html = tmpl($("#maxAttemptsScript").html(), {"params": params});
                $("#alert").html(html);
                document.getElementById("signinButton").disabled = true;
            } else {
                $("#signinButton").on("click", function (e) {
                    var code = $("#code").val();
                    if (code === undefined || code.length !== 6) {
                        e.preventDefault();
                        $("#code").addClass("has-error");
                        html = tmpl($("#verificationCodeEmptyScript").html(), {"params": params});
                        $("#alert").html(html);
                        return;
                    }
                    params.code = code;
                    params.codeValidate = undefined;
                });
            }
        }
    } else {
        html = tmpl($("#resetForm").html(), {"params": params});
        $(html).fadeIn("slow").appendTo("#form-data");
        $("#iSigninName").val(params.username);
        html = tmpl($("#usernameErrorScript").html(), {"params": params});
        $("#pMemberNameErr").html(html);
        $("#iSigninName").addClass("has-error");
        $("#resetPwdHipCancel").on("click", function (e) {
            e.preventDefault();
            var url = "/oauth/signin/login/" + params.tid + "?locale=" + params.locale;
            window.location.href = url;
        });
        $("#resetPwdHipAction").on("click", function (e) {
            e.preventDefault();
            var email = $("#iSigninName").val();
            email = email.replace("+", "");
            if (isInteger(email) && email.length > 6) {
                email = getInteger(email);
                params.phone = email;
                params.username = email;
                phoneLoginForm(params);
            } else if (isEmail(email)) {
                params.usernameType = "EMAIL";
                params.username = email;
                preFPost(params);
            } else {
                html = tmpl($("#usernameErrorScript").html(), {"params": params});
                $("#pMemberNameErr").html(html);
                $("#iSigninName").addClass("has-error");
            }
        });
    }
}


function phoneLoginForm(params) {
    $("#pMemberNameErr").html("");
    $("#iSigninName").removeClass("has-error");
    $("#PhoneCountrySection").css("display", "inline");
    $("#resetPAction").css("display", "inline");
    $("#resetPwdHipAction").css("display", "none");

    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('resetPAction', {
        'size': 'invisible',
        'callback': function (response) {
        }
    });

    $("#resetPAction").on("click", function (e) {
        e.preventDefault();
        document.getElementById("resetPwdHipAction").disabled = true;
        var phone = $("#iSigninName").val();
        if (isInteger(phone) && phone.length > 6) {
            phone = getInteger(phone);
            params.phone = phone;
            params.usernameType = "PHONE";
            params.username = phone + "";
            params.location = $("#phoneCountry option:selected").val();


            var ccode = $("#phoneCountry option:selected").text().split("(+")[1].replace(")", "");
            if (!phone.startsWith(ccode)) {
                phone = "+" + ccode + phone;
            }
            var appVerifier = window.recaptchaVerifier;
            firebase.auth().signInWithPhoneNumber(phone, appVerifier)
                    .then(function (confirmationResult) {
                        window.confirmationResult = confirmationResult;
                        getCredentialTypePhone(params);
                    }).catch(function (error) {
                preFPost(params);
            });
        } else {
            html = tmpl($("#phoneErrorScript").html(), {"params": params});
            $("#pMemberNameErr").html(html);
            $("#iSigninName").addClass("has-error");
        }
    });
}

function getCredentialTypePhone(params) {
    $.ajax({
        type: "POST",
        url: "/oauth/auth/basic/credential/type",
        contentType: 'application/json; charset=UTF-8',
        data: JSON.stringify(params),
        success: function (data) {
            pparams = data;
            pparams.usernameType = "PHONE";
            codeForm(params, data);
        },
        error: function (data) {
            alert("Error al obtener los datos.");
        },
        dataType: "json"
    });
}

function preFPost(params) {
    var html = hiddenParameter("usernameType", params.usernameType);
    $(html).appendTo("#form-var");
    $('#resetPwdForm').submit();
}

function processCredentialType(params, data) {
}

function enabledReset() {
    nextReset(false);
}
function disabledReset() {
    nextReset(true);
}

function nextReset(aux) {
    document.getElementById("resetPwdHipAction").disabled = aux;
    document.getElementById("resetPAction").disabled = aux;
}
