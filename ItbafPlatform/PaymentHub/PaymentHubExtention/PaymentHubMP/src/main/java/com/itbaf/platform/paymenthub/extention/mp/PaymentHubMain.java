/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.mp;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.PaymentHubBasic;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
public class PaymentHubMain extends PaymentHubBasic {

    private final SubscriptionManager subscriptionManager;

    @Inject
    public PaymentHubMain(final SubscriptionManager subscriptionManager) {
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
    }

}
