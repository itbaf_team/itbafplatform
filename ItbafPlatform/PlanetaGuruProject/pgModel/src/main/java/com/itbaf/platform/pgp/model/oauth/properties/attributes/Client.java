/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties.attributes;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.pgp.model.oauth.User;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.impl.TextCodec;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class Client implements Serializable {

    private static final long serialVersionUID = 20181207172502L;

    private String clientName;
    private String clientId;
    private String ClientSecret;

    public Client() {

    }

    public void clientCreate() throws UnsupportedEncodingException {
        if (clientId == null) {
            int num1 = 18;
            int num2 = 126;
            String key = "";
            for (int i = 1; i <= 15; i++) {
                int x = (int) Math.floor(Math.random() * (num2 - num1) + num1);
                key = key + (char) x;
            }
            clientId = TextCodec.BASE64.encode(key.getBytes()).replace("+", "f");
            ClientSecret = TextCodec.BASE64.encode(("ITBAF.JF." + clientId + "." + Thread.currentThread().getName()).getBytes("UTF-8")).replace("=", "");
        }
    }

    public static String getNewToken(String secret, User user, Map<String, Object> params, int expirationDays) throws UnsupportedEncodingException {
        return getNewToken(secret, user, params, expirationDays * 24 * 60 * 60L);
    }

    public static String getNewToken(String secret, User user, Map<String, Object> params, long expirationSeconds) throws UnsupportedEncodingException {
        Calendar c = Calendar.getInstance();

        JwtBuilder jb = Jwts.builder()
                .setIssuer(user.getId().toString())
                .setIssuedAt(c.getTime())
                .compressWith(CompressionCodecs.DEFLATE)
                .signWith(SignatureAlgorithm.HS256, (secret).getBytes("UTF-8"));

        if (params != null && !params.isEmpty()) {
            for (String s : params.keySet()) {
                jb.claim(s, params.get(s));
            }
        }

        if (expirationSeconds > 0) {
            Calendar ce = Calendar.getInstance();
            ce.add(Calendar.DATE, Integer.parseInt((expirationSeconds / (60 * 60 * 24)) + ""));
            jb.setExpiration(ce.getTime());
        }
        return jb.compact();
    }

    public static DefaultClaims validateToken(String secret, String token) throws Exception {
        return CommonFunction.validateToken(secret, token);
    }

    public static Map<String, String> validateTokenData(String secret, String token) throws Exception {
        DefaultClaims dc = validateToken(secret, token);
        return dc.get("data", Map.class);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("clientId", clientId)
                .add("ClientSecret", ClientSecret)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, ClientSecret);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (!Objects.equals(this.clientId, other.clientId)) {
            return false;
        }
        if (!Objects.equals(this.ClientSecret, other.ClientSecret)) {
            return false;
        }
        return true;
    }

}
