package com.itbaf.platform.pgp.api.rest.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgp.api.handler.AccountViewServiceProcessorHandler;
import com.itbaf.platform.pgp.api.handler.ProfileViewServiceProcessorHandler;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.services.aws.s3.model.FileProperties;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import io.jsonwebtoken.impl.DefaultClaims;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;

@Path("/profile")
@Singleton
@lombok.extern.log4j.Log4j2
public class ProfileViewRestService extends CommonRestService {

    private final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler;
    private final ProfileViewServiceProcessorHandler profileViewServiceProcessorHandler;

    @Inject
    public ProfileViewRestService(final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler,
            final ProfileViewServiceProcessorHandler profileViewServiceProcessorHandler) {
        this.accountViewServiceProcessorHandler = Validate.notNull(accountViewServiceProcessorHandler, "An AccountViewServiceProcessorHandler class must be provided");
        this.profileViewServiceProcessorHandler = Validate.notNull(profileViewServiceProcessorHandler, "A profileViewServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public Response profile(@Context HttpServletRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.profile");
        } catch (Exception ex) {
        }
        log.info("profile. [" + request.getQueryString() + "]");

        return getProfilePage(request);
    }

    @GET
    @Path("/edit/{path}")
    @Produces(MediaType.TEXT_HTML)
    public Response profileEdit(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            @QueryParam("ru") String redirectUri) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.profile.edit." + path);
        } catch (Exception ex) {
        }
        log.info("profileEdit. path: [" + path + "] - redirectUri: [" + redirectUri + "]");

        return getProfilePage(request);
    }

    @POST
    @Path("/status/nickname")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postProfileEditNickname(@Context HttpServletRequest request,
            UserProfile up) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.profile.status.nickname");
        } catch (Exception ex) {
        }
        log.info("postProfileEditNickname. UserProfile: [" + up + "]");
        PGMessageResponse rm = new PGMessageResponse();
        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);

            if (client != null && user != null && user.getId().equals(up.getUserId())) {
                String result = profileViewServiceProcessorHandler.getNicknameStatus(user, up);
                if (result != null) {
                    up.setResult(result);
                    rm.meta.code = 200;
                    rm.meta.status = Meta.Status.ok;
                    rm.data.object = up;
                } else {
                    rm.meta.code = 400;
                    rm.meta.status = Meta.Status.invalid_request;
                    rm.data.object = result;
                }
            } else {
                rm.data.message = "Solicitud invalida.";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_request;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al guardar informacion del perfil. [" + up + "]" + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();

    }
    
    @POST
    @Path("/edit/{path}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_HTML)
    public Response postProfileEditImage(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            @FormDataParam("pictureFile") InputStream uploadedInputStream,
            @FormDataParam("pictureFile") FormDataContentDisposition fileDetail,
            @FormDataParam("properties") String properties) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.profile.edit." + path);
        } catch (Exception ex) {
        }
        FileProperties fp = null;
        if (properties != null) {
            fp = profileViewServiceProcessorHandler.readObjectFromJson(properties, FileProperties.class);
        }

        log.info("postProfileEditImage. path: [" + path + "] - File: [" + fileDetail.getFileName() + "] - FileProperties: [" + fp + "]");
        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            if (client != null && user != null && fp != null) {
                String filename = profileViewServiceProcessorHandler.saveImagen(path, user, uploadedInputStream, fileDetail, fp);
                if (filename != null) {
                    rm.meta.code = 200;
                    rm.meta.status = Meta.Status.ok;
                    rm.data.object = filename;
                }
            } else {
                rm.data.message = "Solicitud invalida.";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_request;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al guardar Imagen del perfil. [" + fileDetail.getFileName() + "]" + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    @PUT
    @Path("/edit/{path}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putProfileEdit(@Context HttpServletRequest request,
            @PathParam("path") final String path,
            UserProfile up) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.profile.edit." + path);
        } catch (Exception ex) {
        }
        log.info("putProfileEdit. path: [" + path + "] - UserProfile: [" + up + "]");
        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);

            if (client != null && user != null && user.getId().equals(up.getUserId())) {
                /*String html = profileViewServiceProcessorHandler.getUserProfileHtml(client, user);
                if (html != null) {*/
                String result = profileViewServiceProcessorHandler.saveOrUpdateProfile(path, user, up);
                if (result == null) {
                    rm.meta.code = 200;
                    rm.meta.status = Meta.Status.ok;
                    rm.data.object = up;
                } else {
                    rm.meta.code = 400;
                    rm.meta.status = Meta.Status.invalid_request;
                    rm.data.object = result;
                }
            } else {
                rm.data.message = "Solicitud invalida.";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_request;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al guardar informacion del perfil. [" + up + "]" + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    private Response getProfilePage(HttpServletRequest request) {
        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            String productId = request.getAttribute(CommonAttribute.PRODUCT_ID).toString();
            if (client != null && user != null) {
                String redirectUri = request.getAttribute(CommonAttribute.U_REDIRECT_URI).toString();
                String html = profileViewServiceProcessorHandler.getUserProfileHtml(client, user, productId, redirectUri);
                if (html != null) {
                    String prvdr = null;
                    try {
                        DefaultClaims dc = (DefaultClaims) request.getAttribute(CommonAttribute.U_DEFAULT_CLAIMS);
                        prvdr = dc.get("prvdr", String.class);
                    } catch (Exception ex) {
                    }
                    html = accountViewServiceProcessorHandler.i18n(user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage(), prvdr, html);
                    return Response.status(Response.Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar peticion.", ex);
        }

        return Response.status(Response.Status.UNAUTHORIZED).build();
    }
}
