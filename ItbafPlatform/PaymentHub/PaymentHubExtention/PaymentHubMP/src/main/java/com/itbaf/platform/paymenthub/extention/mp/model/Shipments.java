package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Shipments {

    private AddressReceiver receiver_address;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("receiver_address", receiver_address)
                .omitNullValues().toString();
    }
}
