package com.itbaf.platform.pgp.api.rest.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgp.api.handler.AccountViewServiceProcessorHandler;
import com.itbaf.platform.pgp.api.handler.TransactionViewServiceProcessorHandler;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import io.jsonwebtoken.impl.DefaultClaims;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;

@Path("/transactions")
@Singleton
@lombok.extern.log4j.Log4j2
public class TransactionViewRestService extends CommonRestService {

    private final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler;
    private final TransactionViewServiceProcessorHandler transactionViewServiceProcessorHandler;

    @Inject
    public TransactionViewRestService(final AccountViewServiceProcessorHandler accountViewServiceProcessorHandler,
            final TransactionViewServiceProcessorHandler transactionViewServiceProcessorHandler) {
        this.accountViewServiceProcessorHandler = Validate.notNull(accountViewServiceProcessorHandler, "An AccountViewServiceProcessorHandler class must be provided");
        this.transactionViewServiceProcessorHandler = Validate.notNull(transactionViewServiceProcessorHandler, "A TransactionViewServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public Response transactions(@Context HttpServletRequest request) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.transactions");
        } catch (Exception ex) {
        }
        log.info("transactions. [" + request.getQueryString() + "]");

        return getTransactionPage(request);
    }

    @POST
    @Path("/orders")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postOrders(@Context HttpServletRequest request,
            UserProfile up) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-view.account.transactions.orders");
        } catch (Exception ex) {
        }
        log.info("postOrders. UserProfile: [" + up + "]");
        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            if (client != null && user != null && user.getId().equals(up.getUserId()) && up.getResult() != null) {
                String prvdr = null;
                try {
                    DefaultClaims dc = (DefaultClaims) request.getAttribute(CommonAttribute.U_DEFAULT_CLAIMS);
                    prvdr = dc.get("prvdr", String.class);
                } catch (Exception ex) {
                }
                up = transactionViewServiceProcessorHandler.getOrders(user, (String) up.getResult(),
                        user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage(), prvdr);
                if (up != null) {
                    rm.meta.code = 200;
                    rm.meta.status = Meta.Status.ok;
                    rm.data.object = up;
                }
            } else {
                rm.data.message = "Solicitud invalida.";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_request;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener Transacciones. UserProfile: [" + up + "]" + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

    private Response getTransactionPage(HttpServletRequest request) {
        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            String productId = request.getAttribute(CommonAttribute.PRODUCT_ID).toString();
            if (client != null && user != null) {
                String lg = user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage();
                String prvdr = null;
                try {
                    DefaultClaims dc = (DefaultClaims) request.getAttribute(CommonAttribute.U_DEFAULT_CLAIMS);
                    prvdr = dc.get("prvdr", String.class);
                } catch (Exception ex) {
                }
                String redirectUri = request.getAttribute(CommonAttribute.U_REDIRECT_URI).toString();
                String html = transactionViewServiceProcessorHandler.getUserTransactionHtml(client, user, lg, productId, prvdr, redirectUri);
                if (html != null) {
                    html = accountViewServiceProcessorHandler.i18n(lg, prvdr, html);
                    return Response.status(Response.Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar peticion. " + ex, ex);
        }

        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

}
