/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.messaging.rest.oauth;

/**
 *
 * @author JF
 */
public class ResponseAuthMessage {

    public enum Token {
        Bearer
    }

    public enum Error {
        invalid_request, 
        invalid_client, 
        invalid_grant,
        unauthorized_client,
        unsupported_grant_type, 
        invalid_scope, 
        error_description, 
        error_uri
    }

    public Error error;
    public String access_token;
    public Token token_type;
    public String message;
    public Long expires_in;
    public String url;
}
