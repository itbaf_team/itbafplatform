package com.itbaf.platform.pgp.api.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.rest.CommonRestService;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgp.api.handler.TransactionViewServiceProcessorHandler;
import com.itbaf.platform.pgp.api.model.UserProfile;
import com.itbaf.platform.pgp.commons.CommonAttribute;
import com.itbaf.platform.pgp.model.oauth.User;
import io.jsonwebtoken.impl.DefaultClaims;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.Validate;

@Path("/api/transactions")
@Singleton
@lombok.extern.log4j.Log4j2
public class ApiTransactionRestService extends CommonRestService {

    private final TransactionViewServiceProcessorHandler transactionViewServiceProcessorHandler;

    @Inject
    public ApiTransactionRestService(final TransactionViewServiceProcessorHandler transactionViewServiceProcessorHandler) {
        this.transactionViewServiceProcessorHandler = Validate.notNull(transactionViewServiceProcessorHandler, "A TransactionViewServiceProcessorHandler class must be provided");
    }

    @POST
    @Path("/orders")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response postOrders(@Context HttpServletRequest request,
            UserProfile up) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-api.transactions.orders");
        } catch (Exception ex) {
        }
        log.info("postOrders. UserProfile: [" + up + "]");
        PGMessageResponse rm = new PGMessageResponse();

        try {
            User client = (User) request.getAttribute(CommonAttribute.PRINCIPAL);
            User user = (User) request.getAttribute(CommonAttribute.U_PRINCIPAL);
            if (client != null && user != null && user.getId().equals(up.getUserId()) && up.getResult() != null) {
                String prvdr = null;
                try {
                    DefaultClaims dc = (DefaultClaims) request.getAttribute(CommonAttribute.U_DEFAULT_CLAIMS);
                    prvdr = dc.get("prvdr", String.class);
                } catch (Exception ex) {
                }
                up = transactionViewServiceProcessorHandler.getOrders(user, (String) up.getResult(),
                        user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage(), prvdr);
                if (up != null) {
                    rm.meta.code = 200;
                    rm.meta.status = Meta.Status.ok;
                    rm.data.object = up;
                }
            } else {
                rm.data.message = "Solicitud invalida.";
                rm.meta.code = 400;
                rm.meta.status = Meta.Status.invalid_request;
            }
        } catch (Exception ex) {
            rm.data.message = "Error al obtener Transacciones. UserProfile: [" + up + "]" + ex;
            rm.meta.code = 500;
            rm.meta.status = Meta.Status.error_internal;
            log.error(rm.data.message, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rm).cacheControl(getCacheControl()).type(MediaType.APPLICATION_JSON).build();
        }

        if (Meta.Status.ok.equals(rm.meta.status)) {
            return Response.status(Response.Status.OK).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(rm).type(MediaType.APPLICATION_JSON).cacheControl(getCacheControl()).build();
    }

}
