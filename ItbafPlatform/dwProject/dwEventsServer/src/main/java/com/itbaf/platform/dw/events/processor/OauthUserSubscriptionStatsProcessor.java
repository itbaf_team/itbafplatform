/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events.processor;

import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.paymenthub.commons.stats.model.OauthUserSubscriptionElastic;
import com.itbaf.platform.paymenthub.commons.stats.model.StatsMessage;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.UsernameAttribute;
import com.itbaf.platform.pgp.services.repository.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class OauthUserSubscriptionStatsProcessor extends StatsProcessor {

    private static final ElasticData.ElasticType elasticType = ElasticData.ElasticType.oauth_user_subscription;

    private final UserService userService;
    private final SubscriptionService subscriptionService;

    @Inject
    public OauthUserSubscriptionStatsProcessor(final UserService userService,
            final SubscriptionService subscriptionService) {
        this.userService = userService;
        this.subscriptionService = subscriptionService;
    }

    @Override
    public Boolean process(String msg) throws Exception {
        StatsMessage smx = mapper.readValue(msg, StatsMessage.class);

        OauthUserSubscriptionElastic oex = (OauthUserSubscriptionElastic) smx.elasticData;
        String mm = smx.elasticData.elasticType + "_" + smx.elasticData.id;
        RLock lock = instrumentedObject.lockObject(mm, 60L);
        try {
            String dm = clientCache.getTemporalData(mm);
            if (dm != null) {
                if (smx.lastUpdateMillis < Long.parseLong(dm)) {
                    return true;
                }
            }

            StatsMessage sm = new StatsMessage();
            OauthUserSubscriptionElastic oe = new OauthUserSubscriptionElastic();
            sm.thread = smx.thread;
            sm.elasticData = oe;

            List<SubscriptionRegistry> subscriptionRegistries = new ArrayList();
            User user = userService.findById(Long.parseLong(smx.elasticData.id));
            List<Email> emails = null;
            List<Msisdn> msisdns = null;
            Predicate<UsernameAttribute> paux = s -> s.getConfirmed();
            if (user.getCredentialUserProperty().getUsername().getEmails() != null) {
                emails = (user.getCredentialUserProperty().getUsername().getEmails().values()
                        .stream().filter(paux).collect(Collectors.toList()));
            }
            if (user.getCredentialUserProperty().getUsername().getMsisdns() != null) {
                msisdns = (user.getCredentialUserProperty().getUsername().getMsisdns().values()
                        .stream().filter(paux).collect(Collectors.toList()));
            }
            if (emails != null && !emails.isEmpty()) {
                for (Email e : emails) {
                    List<Subscription> ss = subscriptionService.findSubscriptionsByUserAccount(e.getEmail());
                    if (ss != null && !ss.isEmpty()) {
                        for (Subscription s : ss) {
                            List<SubscriptionRegistry> srs = subscriptionRegistryService.getRegistersBySubscription(s.getId());
                            if (srs != null && !srs.isEmpty()) {
                                subscriptionRegistries.addAll(srs);
                            }
                        }
                    }
                }
            }
            if (msisdns != null && !msisdns.isEmpty()) {
                for (Msisdn m : msisdns) {
                    List<Subscription> ss;
                    if (m.getCountry() != null && m.getCountry().getId() != null) {
                        ss = subscriptionService.getByMSISDNAndCountry(m.getMsisdn(), m.getCountry());
                    } else {
                        ss = subscriptionService.findSubscriptionsByUserAccount(m.getMsisdn());
                    }
                    if (ss != null && !ss.isEmpty()) {
                        for (Subscription s : ss) {
                            List<SubscriptionRegistry> srs = subscriptionRegistryService.getRegistersBySubscription(s.getId());
                            if (srs != null && !srs.isEmpty()) {
                                subscriptionRegistries.addAll(srs);
                            }
                        }
                    }
                }
            }

            //////////////////////////////////
            oe.userId = user.getId();
            //Envio de Usuario con Email
            if (user.getCredentialUserProperty().getUsername().getEmails() != null) {
                for (Email e : user.getCredentialUserProperty().getUsername().getEmails().values()) {
                    if (e.getConfirmed()) {
                        oe.userAccount = e.getEmail();
                        oe.id = oe.userId + "_UA_" + oe.userAccount;
                        String json = mapper.writeValueAsString(sm);
                        if (Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.oauth_user_subscription, json))) {
                            if (!Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.oauth_user_subscription, json))) {
                                return false;
                            }
                        }
                    }
                }
            }
            //Envio de Usuarios con MSISDN
            if (user.getCredentialUserProperty().getUsername().getMsisdns() != null) {
                for (Msisdn m : user.getCredentialUserProperty().getUsername().getMsisdns().values()) {
                    if (m.getConfirmed()) {
                        oe.userAccount = m.getMsisdn();
                        oe.id = oe.userId + "_UA_" + oe.userAccount;
                        String json = mapper.writeValueAsString(sm);
                        if (!Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.oauth_user_subscription, json))) {
                            if (!Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.oauth_user_subscription, json))) {
                                return false;
                            }
                        }
                    }
                }
            }

            //Envio de Usuarios con suscripcion
            for (SubscriptionRegistry sr : subscriptionRegistries) {
                oe.userAccount = sr.getUserAccount();
                oe.SubscriptionRegistryId = sr.getId();
                oe.subscriptionId = sr.getSubscription().getId();
                oe.id = oe.userId + "_SR_" + oe.SubscriptionRegistryId;
                String json = mapper.writeValueAsString(sm);
                if (!Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.oauth_user_subscription, json))) {
                    if (!Boolean.TRUE.equals(statsService.sendETLToRabbit(ElasticData.ElasticType.oauth_user_subscription, json))) {
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception ex) {
            log.error("Error al procesar OauthUserSubscriptionElastic: [" + msg + "]. " + ex, ex);
        } finally {
            instrumentedObject.unLockObject(lock);
        }

        return null;
    }

    @Override
    public ElasticData.ElasticType getElasticType() {
        return elasticType;
    }

}
