package com.itbaf.platform.paymenthub.model.billing;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QBillerAttempt is a Querydsl query type for BillerAttempt
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBillerAttempt extends EntityPathBase<BillerAttempt> {

    private static final long serialVersionUID = -1413321762L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QBillerAttempt billerAttempt = new QBillerAttempt("billerAttempt");

    public final com.itbaf.platform.paymenthub.model.QBaseEntity _super = new com.itbaf.platform.paymenthub.model.QBaseEntity(this);

    public final SimplePath<com.itbaf.platform.paymenthub.model.subscription.Extra> extra = createSimple("extra", com.itbaf.platform.paymenthub.model.subscription.Extra.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.util.Date> lastAttempt = createDateTime("lastAttempt", java.util.Date.class);

    public final DateTimePath<java.util.Date> lastCharged = createDateTime("lastCharged", java.util.Date.class);

    public final DateTimePath<java.util.Date> nextAttempt = createDateTime("nextAttempt", java.util.Date.class);

    protected com.itbaf.platform.paymenthub.model.QSOP sop;

    public final NumberPath<java.math.BigDecimal> targetAmount = createNumber("targetAmount", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> totalAmount = createNumber("totalAmount", java.math.BigDecimal.class);

    public QBillerAttempt(String variable) {
        this(BillerAttempt.class, forVariable(variable), INITS);
    }

    public QBillerAttempt(Path<? extends BillerAttempt> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QBillerAttempt(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QBillerAttempt(PathMetadata metadata, PathInits inits) {
        this(BillerAttempt.class, metadata, inits);
    }

    public QBillerAttempt(Class<? extends BillerAttempt> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sop = inits.isInitialized("sop") ? new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"), inits.get("sop")) : null;
    }

    public com.itbaf.platform.paymenthub.model.QSOP sop() {
        if (sop == null) {
            sop = new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"));
        }
        return sop;
    }

}

