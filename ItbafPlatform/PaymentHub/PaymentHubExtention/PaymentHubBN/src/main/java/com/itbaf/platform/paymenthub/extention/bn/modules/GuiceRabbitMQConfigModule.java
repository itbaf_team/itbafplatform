/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.bn.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import com.itbaf.platform.rmq.producer.RabbitMQProducerImpl;
import com.rabbitmq.client.ConnectionFactory;

/**
 *
 * @author jordonez
 */
public class GuiceRabbitMQConfigModule extends AbstractModule {

    public GuiceRabbitMQConfigModule() {

    }

    @Override
    protected void configure() {

        //Binds
        bind(RabbitMQProducer.class).to(RabbitMQProducerImpl.class).asEagerSingleton();
    }

    @Provides
    ConnectionFactory connectionFactory(@Named("rabbitmq.url") String rmqUrl,
            @Named("rabbitmq.port") Integer rmqPort,
            @Named("rabbitmq.username") String rmqUsername,
            @Named("rabbitmq.password") String rmqPassword,
            @Named("rabbitmq.virtualHost") String rmqVirtualhost) {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(rmqUrl);
        connectionFactory.setPort(rmqPort);
        connectionFactory.setUsername(rmqUsername);
        connectionFactory.setPassword(rmqPassword);
        connectionFactory.setVirtualHost(rmqVirtualhost);

        return connectionFactory;
    }

}
