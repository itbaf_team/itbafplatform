package com.itbaf.platform.paymenthub.extention.wau.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "request_data")
public class Subscribe {

    @JacksonXmlElementWrapper
    private String access_token;

    @JacksonXmlElementWrapper
    private String integrator_id;

    @JacksonXmlElementWrapper
    private String identifier; // ani 52xxxxxxxxxx

    @JacksonXmlElementWrapper
    private String channel_id; // 1. Web opt-in 2. Sms opt-in 3. Wap opt-in

    @JacksonXmlElementWrapper
    private String product_id;

    public Subscribe() {
    }

    public Subscribe(String access_token, String integrator_id, String identifier, String channel_id,
            String product_id) {
        this.access_token = access_token;
        this.integrator_id = integrator_id;
        this.identifier = identifier;
        this.channel_id = channel_id;
        this.product_id = product_id;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getIntegrator_id() {
        return integrator_id;
    }

    public void setIntegrator_id(String integrator_id) {
        this.integrator_id = integrator_id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
