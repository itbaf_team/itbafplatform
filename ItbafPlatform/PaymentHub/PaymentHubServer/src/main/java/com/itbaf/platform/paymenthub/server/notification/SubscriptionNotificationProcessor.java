/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.server.notification;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.BILLER_NEW;
import com.itbaf.platform.paymenthub.commons.messaging.BillerMessage;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class SubscriptionNotificationProcessor implements NotificationProcessor {

    private final ObjectMapper mapper;
    private final SopService sopService;
    private final RabbitMQProducer rabbitMQProducer;
    private final SubscriptionManager subscriptionManager;
    private final SubscriptionRegistryService subscriptionRegistryService;
    private static final PaymentHubMessage.NotificationType notificationType = PaymentHubMessage.NotificationType.SUBSCRIPTION;

    @Inject
    public SubscriptionNotificationProcessor(final ObjectMapper mapper,
            final SopService sopService,
            final RabbitMQProducer rabbitMQProducer,
            final SubscriptionManager subscriptionManager,
            final SubscriptionRegistryService subscriptionRegistryService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.sopService = Validate.notNull(sopService, "A SopService class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
        this.subscriptionRegistryService = Validate.notNull(subscriptionRegistryService, "A SubscriptionRegistryService class must be provided");
    }

    @Override
    public PaymentHubMessage.NotificationType getNotificationType() {
        return notificationType;
    }

    @Override
    public Boolean process(String smsg, PaymentHubMessage msg, SOP sop) {

        Subscription s = subscriptionManager.getSubscription(msg.getExternalId(),
                msg.getUserAccount(), msg.getExternalUserAccount(), msg.getExternalCustomer(), sop);

        boolean isWritter = true;
        if (s == null) {
            if (msg.getUserAccount() == null && msg.getExternalUserAccount() != null) {
                msg.setUserAccount(msg.getExternalUserAccount());
            }
            if (msg.getUserAccount() == null && msg.getExternalId() != null) {
                msg.setUserAccount(msg.getExternalId());
            }

            // log.info("Se creara una Suscripcion para [" + msg.getUserAccount() + "] - SOP: [" + sop.getId() + "]");
            s = new Subscription();
            s.setSop(sop);
            s.setUserAccount(msg.getUserAccount());
            s.setSubscriptionDate(msg.getFromDate());
            s.setExternalUserAccount(msg.getExternalUserAccount());
            s.setExternalCustomer(msg.getExternalCustomer());
            SubscriptionRegistry sr = new SubscriptionRegistry();
            sr.setExternalId(msg.getExternalId());
            sr.setSop(sop);
            sr.setSubscription(s);
            sr.setUserAccount(msg.getUserAccount());
            sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
            sr.setSubscriptionDate(msg.getFromDate());
            s.setSubscriptionRegistry(sr);
            sr.setSyncDate(null);
            sr.setChannelIn(msg.getChannelIn());
            sr.setChannelOut(msg.getChannelOut());
            trackingParamsProcess(msg, s);
            switch (msg.getMessageType()) {
                case SUBSCRIPTION_TRIAL:
                    s.setStatus(Subscription.Status.TRIAL);
                    break;
                case SUBSCRIPTION_PENDING:
                    s.setStatus(Subscription.Status.PENDING);
                    break;
                case SUBSCRIPTION_ACTIVE:
                case SUBSCRIPTION_BY_BILLING:
                    s.setStatus(Subscription.Status.ACTIVE);
                    break;
                case SUBSCRIPTION_REMOVED:
                    s.setStatus(Subscription.Status.REMOVED);
                    s.setUnsubscriptionDate(msg.getToDate() == null ? new Date() : msg.getToDate());
                    s.setSubscriptionDate(msg.getFromDate() == null ? s.getUnsubscriptionDate() : msg.getFromDate());
                    sr.setOriginUnsubscription(msg.getOrigin() + ":" + msg.getMessageType());
                    sr.setSubscriptionDate(s.getSubscriptionDate());
                    sr.setUnsubscriptionDate(s.getUnsubscriptionDate());
                    if (sr.getOriginSubscription() == null) {
                        sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                    }
                    break;
                case SUBSCRIPTION_BLACKLIST:
                    s.setStatus(Subscription.Status.BLACKLIST);
                    s.setUnsubscriptionDate(msg.getToDate() == null ? new Date() : msg.getToDate());
                    s.setSubscriptionDate(msg.getFromDate() == null ? s.getUnsubscriptionDate() : msg.getFromDate());
                    sr.setOriginUnsubscription(msg.getOrigin() + ":" + msg.getMessageType());
                    sr.setSubscriptionDate(s.getSubscriptionDate());
                    sr.setUnsubscriptionDate(s.getUnsubscriptionDate());
                    if (sr.getOriginSubscription() == null) {
                        sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                    }
                    break;
                case SUBSCRIPTION_CANCELLATION:
                    s.setStatus(Subscription.Status.CANCELLED);
                    s.setUnsubscriptionDate(msg.getToDate() == null ? new Date() : msg.getToDate());
                    s.setSubscriptionDate(msg.getFromDate() == null ? s.getUnsubscriptionDate() : msg.getFromDate());
                    sr.setOriginUnsubscription(msg.getOrigin() + ":" + msg.getMessageType());
                    sr.setSubscriptionDate(s.getSubscriptionDate());
                    sr.setUnsubscriptionDate(s.getUnsubscriptionDate());
                    if (sr.getOriginSubscription() == null) {
                        sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                    }
                    break;
                case SUBSCRIPTION_RETENTION:
                    s.setStatus(Subscription.Status.RETENTION);
                    break;
            }
        } else {
            if (msg.getUserAccount() == null) {
                msg.setUserAccount(s.getUserAccount());
            }

            // log.info("Suscripcion en DB: [" + s + "]");
            SubscriptionRegistry sr = s.getSubscriptionRegistry();

            switch (msg.getMessageType()) {
                case SUBSCRIPTION_RETENTION:
                    if (!Subscription.Status.ACTIVE.equals(s.getStatus())
                            && !Subscription.Status.RETENTION.equals(s.getStatus()) && !Subscription.Status.PENDING.equals(s.getStatus())) {
                        sr = new SubscriptionRegistry();
                        sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                        sr.setSop(sop);
                        s.setStatus(Subscription.Status.RETENTION);
                        s.setSubscriptionDate(msg.getFromDate());
                        sr.setSubscription(s);
                        s.setSubscriptionRegistry(sr);
                        s.setUnsubscriptionDate(null);
                        sr.setExternalId(msg.getExternalId());
                        sr.setChannelIn(msg.getChannelIn());
                        sr.setChannelOut(msg.getChannelOut());
                        sr.setUserAccount(s.getUserAccount());
                        sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                        sr.setSubscriptionDate(msg.getFromDate());
                        trackingParamsProcess(msg, s);
                    }
                    break;
                case SUBSCRIPTION_TRIAL:
                    if (!Subscription.Status.ACTIVE.equals(s.getStatus())
                            && !Subscription.Status.TRIAL.equals(s.getStatus()) && !Subscription.Status.PENDING.equals(s.getStatus())) {
                        sr = new SubscriptionRegistry();
                        sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                        sr.setSop(sop);
                        s.setStatus(Subscription.Status.TRIAL);
                        s.setSubscriptionDate(msg.getFromDate());
                        sr.setSubscription(s);
                        s.setSubscriptionRegistry(sr);
                        s.setUnsubscriptionDate(null);
                        sr.setExternalId(msg.getExternalId());
                        sr.setChannelIn(msg.getChannelIn());
                        sr.setChannelOut(msg.getChannelOut());
                        sr.setUserAccount(s.getUserAccount());
                        sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                        sr.setSubscriptionDate(msg.getFromDate());
                        trackingParamsProcess(msg, s);
                    }
                    break;
                case SUBSCRIPTION_PENDING:
                    if ((msg.getFromDate().getTime() - s.getSubscriptionDate().getTime() > 0) && !Subscription.Status.PENDING.equals(s.getStatus())) {
                        if (!Subscription.Status.PENDING.equals(s.getStatus()) &&
                                ( !Subscription.Status.ACTIVE.equals(s.getStatus()) || (Subscription.Status.ACTIVE.equals(s.getStatus()) && s.getSubscriptionRegistry().getOriginSubscription().contains("ACTIVATED-FIRST-24HRS") )  )
                                && !(msg.getExternalId() != null && msg.getExternalId().equals(s.getSubscriptionRegistry().getExternalId()))) {
                            try {
                                boolean csr = false;
                                if (sr.getUnsubscriptionDate() == null) {
                                    sr.setUnsubscriptionDate(new Date());
                                    csr = true;
                                }
                                if (sr.getOriginUnsubscription() == null) {
                                    sr.setOriginUnsubscription("NOTIFICATION:SUBSCRIPTION_PENDING");
                                    csr = true;
                                }
                                if (csr) {
                                    subscriptionRegistryService.saveOrUpdate(sr);
                                }
                            } catch (Exception ex) {
                                log.error("Error al actualizar subscriptionRegistry: [" + sr + "]. " + ex, ex);
                            }
                            sr = new SubscriptionRegistry();
                            sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                            sr.setSop(sop);
                            s.setStatus(Subscription.Status.PENDING);
                            s.setSubscriptionDate(msg.getFromDate());
                        } else {
                            if (sr.getExternalId() != null && msg.getExternalId() != null && !msg.getExternalId().equals(sr.getExternalId())
                                    && (msg.getFromDate().getTime() - s.getSubscriptionDate().getTime() > 0)) {
                                try {
                                    if (Subscription.Status.PENDING.equals(s.getStatus()) || Subscription.Status.ACTIVE.equals(s.getStatus())) {
                                        s.setStatus(Subscription.Status.CANCELLED);
                                    }

                                    if (sr.getUnsubscriptionDate() == null) {
                                        sr.setUnsubscriptionDate(new Date());
                                    }
                                    if (sr.getOriginUnsubscription() == null) {
                                        sr.setOriginUnsubscription("NOTIFICATION:SUBSCRIPTION_PENDING");
                                    }
                                    subscriptionRegistryService.saveOrUpdate(sr);
                                } catch (Exception ex) {
                                    log.error("Error al actualizar subscriptionRegistry: [" + sr + "]. " + ex, ex);
                                }
                                sr = new SubscriptionRegistry();
                                sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                                sr.setSop(sop);
                                s.setStatus(Subscription.Status.PENDING);
                                s.setSubscriptionDate(msg.getFromDate());
                                sr.setSubscription(s);
                                s.setSubscriptionRegistry(sr);
                            }
                            if (sr.getExternalId() == null && msg.getExternalId() != null
                                    && (msg.getFromDate().getTime() - s.getSubscriptionDate().getTime() >= 0)
                                    && (Subscription.Status.PENDING.equals(s.getStatus()) || Subscription.Status.ACTIVE.equals(s.getStatus()))) {
                                sr.setExternalId(msg.getExternalId());
                            } else {
                                if (!Subscription.Status.ACTIVE.equals(s.getStatus())) {
                                    if ((sr.getOriginUnsubscription() != null && sr.getOriginUnsubscription().contains("SUBSCRIPTION_REMOVED"))
                                            || (sr.getUnsubscriptionDate() != null && msg.getFromDate() != null
                                            && (sr.getUnsubscriptionDate().getTime() - msg.getFromDate().getTime()) > 0)) {
                                        log.warn("No se actualizara el STATUS [" + s.getStatus() + "] a PENDING. Suscripcion: [" + s + "]");
                                        return false;
                                    }
                                    log.error("Se pretende cambiar una suscripcion con STATUS [" + s.getStatus() + "] a PENDING. Suscripcion: [" + s + "]");
                                    s.setStatus(Subscription.Status.PENDING);
                                }
                                s.getSubscriptionRegistry().setSyncDate(null);
                            }
                        }
                        if (s.getExternalCustomer() == null) {
                            s.setExternalCustomer(msg.getExternalCustomer());
                        }
                        if (s.getExternalUserAccount() == null) {
                            s.setExternalUserAccount(msg.getExternalUserAccount());
                        }
                        s.setUnsubscriptionDate(null);
                        if (sr.getSubscriptionDate() == null) {
                            sr.setSubscriptionDate(s.getSubscriptionDate());
                        }
                        if (sr.getExternalId() == null) {
                            sr.setExternalId(msg.getExternalId());
                        }
                        if (sr.getChannelIn() == null) {
                            sr.setChannelIn(msg.getChannelIn());
                        }
                        sr.setUserAccount(s.getUserAccount());
                        if (sr.getOriginSubscription() == null) {
                            sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                        }
                        sr.setSubscription(s);
                        s.setSubscriptionRegistry(sr);
                        trackingParamsProcess(msg, s);
                    } else if ((msg.getFromDate().getTime() - s.getSubscriptionDate().getTime() > 0) && Subscription.Status.PENDING.equals(s.getStatus())
                            && msg.getExternalId() != null && s.getSubscriptionRegistry().getExternalId() != null
                            && !msg.getExternalId().equals(s.getSubscriptionRegistry().getExternalId())) {
                        try {
                            sr.setUnsubscriptionDate(new Date());
                            sr.setOriginUnsubscription("NOTIFICATION:SUBSCRIPTION_PENDING");
                            subscriptionRegistryService.saveOrUpdate(sr);
                            //  log.info("Actualizado subscriptionRegistry: [" + sr + "].");
                        } catch (Exception ex) {
                            log.error("Error al actualizar subscriptionRegistry: [" + sr + "]. " + ex, ex);
                        }
                        sr = new SubscriptionRegistry();
                        sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                        sr.setSop(sop);
                        s.setStatus(Subscription.Status.PENDING);
                        s.setSubscriptionDate(msg.getFromDate());
                        sr.setSubscription(s);
                        s.setSubscriptionRegistry(sr);
                        if (s.getExternalCustomer() == null) {
                            s.setExternalCustomer(msg.getExternalCustomer());
                        }
                        if (s.getExternalUserAccount() == null) {
                            s.setExternalUserAccount(msg.getExternalUserAccount());
                        }
                        s.setUnsubscriptionDate(null);
                        sr.setSubscriptionDate(s.getSubscriptionDate());
                        sr.setExternalId(msg.getExternalId());
                        sr.setChannelIn(msg.getChannelIn());
                        sr.setChannelOut(msg.getChannelOut());
                        sr.setUserAccount(s.getUserAccount());
                        trackingParamsProcess(msg, s);
                    } else {
                        /* log.info("Se ha recibido una notificacion de suscripcion anterior o igual a la existente en DB. s.date: ["
                                + s.getSubscriptionDate().getTime() + "][" + s.getSubscriptionDate() + "] - n.fromdate: [" + msg.getFromDate().getTime() + "][" + msg.getFromDate() + "]");
                         */
                        if (sr.getExternalId() == null && msg.getExternalId() != null) {
                            sr.setExternalId(msg.getExternalId());
                            isWritter = true;
                        } else {
                            isWritter = false;
                        }
                    }
                    break;
                case SUBSCRIPTION_ACTIVE:
                    //-60 segs porque NPAY tiene unos segundos de diferencia ¬¬
                    if ((msg.getFromDate().getTime() - s.getSubscriptionDate().getTime() > 60000)
                            || (msg.getFromDate().getTime() - s.getSubscriptionDate().getTime() > 0 && s.getUnsubscriptionDate() != null
                            && msg.getFromDate().getTime() - s.getUnsubscriptionDate().getTime() > 0)//Suscripcion existente en baja
                            || (msg.getFromDate().getTime() - s.getSubscriptionDate().getTime() > -60000 && Subscription.Status.PENDING.equals(s.getStatus()))
                            || (msg.getExternalId() != null && msg.getExternalId().equals(s.getSubscriptionRegistry().getExternalId()))) {
                        if (!Subscription.Status.PENDING.equals(s.getStatus()) && !Subscription.Status.ACTIVE.equals(s.getStatus())
                                && !(msg.getExternalId() != null && msg.getExternalId().equals(s.getSubscriptionRegistry().getExternalId()))) {
                            try {
                                boolean csr = false;
                                if (sr.getUnsubscriptionDate() == null) {
                                    sr.setUnsubscriptionDate(new Date());
                                    csr = true;
                                }
                                if (sr.getOriginUnsubscription() == null) {
                                    sr.setOriginUnsubscription("NOTIFICATION:SUBSCRIPTION_ACTIVE");
                                    csr = true;
                                }
                                if (csr) {
                                    subscriptionRegistryService.saveOrUpdate(sr);
                                }
                            } catch (Exception ex) {
                                log.error("Error al actualizar subscriptionRegistry: [" + sr + "]. " + ex, ex);
                            }
                            sr = new SubscriptionRegistry();
                            sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                            sr.setSop(sop);
                            s.setSubscriptionDate(msg.getFromDate());
                        } else if (sr.getExternalId() != null && msg.getExternalId() != null && !msg.getExternalId().equals(sr.getExternalId())
                                && (msg.getFromDate().getTime() - s.getSubscriptionDate().getTime() > 0)) {
                            try {
                                if (Subscription.Status.PENDING.equals(s.getStatus()) || Subscription.Status.ACTIVE.equals(s.getStatus())) {
                                    s.setStatus(Subscription.Status.CANCELLED);
                                }

                                boolean csr = false;
                                if (sr.getUnsubscriptionDate() == null) {
                                    sr.setUnsubscriptionDate(new Date());
                                    csr = true;
                                }
                                if (sr.getOriginUnsubscription() == null) {
                                    sr.setOriginUnsubscription("NOTIFICATION:SUBSCRIPTION_ACTIVE");
                                    csr = true;
                                }
                                if (csr) {
                                    subscriptionRegistryService.saveOrUpdate(sr);
                                }
                            } catch (Exception ex) {
                                log.error("Error al actualizar subscriptionRegistry: [" + sr + "]. " + ex, ex);
                            }
                            sr = new SubscriptionRegistry();
                            sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                            sr.setSop(sop);
                            sr.setSubscription(s);
                            s.setSubscriptionRegistry(sr);
                            s.setSubscriptionDate(msg.getFromDate());
                        }
                        if ((!Subscription.Status.PENDING.equals(s.getStatus()) && !Subscription.Status.ACTIVE.equals(s.getStatus())) && msg.isToDateOverride()) {
                            //Para evitar sobreescribir la fecha de suscripcion Pending
                            s.setSubscriptionDate(msg.getFromDate());
                        } else if (s.getSubscriptionDate() == null) {
                            s.setSubscriptionDate(msg.getFromDate());
                        }
                        s.setStatus(Subscription.Status.ACTIVE);
                        s.setUnsubscriptionDate(null);
                        if (s.getExternalCustomer() == null) {
                            s.setExternalCustomer(msg.getExternalCustomer());
                        }
                        if (s.getExternalUserAccount() == null) {
                            s.setExternalUserAccount(msg.getExternalUserAccount());
                        }
                        sr.setSubscriptionDate(s.getSubscriptionDate());
                        sr.setExternalId(msg.getExternalId());
                        sr.setChannelIn(sr.getChannelIn() == null ? msg.getChannelIn() : sr.getChannelIn());
                        sr.setSubscription(s);
                        sr.setUserAccount(s.getUserAccount());
                        sr.setSyncDate(null);
                        s.setSubscriptionRegistry(sr);
                        trackingParamsProcess(msg, s);
                    } else {
                        /* log.info("Se ha recibido una notificacion de suscripcion anterior o igual a la existente en DB. s.date: ["
                                + s.getSubscriptionDate().getTime() + "][" + s.getSubscriptionDate() + "] - n.fromdate: [" + msg.getFromDate().getTime() + "][" + msg.getFromDate() + "]");
                         */
                        if (sr.getExternalId() == null && msg.getExternalId() != null) {
                            sr.setExternalId(msg.getExternalId());
                            isWritter = true;
                        } else {
                            isWritter = false;
                        }
                    }
                    break;
                case SUBSCRIPTION_REMOVED:
                    if ((s.getUnsubscriptionDate() == null || Subscription.Status.CANCELLED.equals(s.getStatus()))
                            && (msg.getToDate().getTime() - s.getSubscriptionDate().getTime() > 0)) {
                        s.setStatus(Subscription.Status.REMOVED);
                        s.setUnsubscriptionDate(msg.getToDate() == null ? new Date() : msg.getToDate());
                        if (sr.getOriginUnsubscription() != null) {
                            sr.setOriginUnsubscription(sr.getOriginUnsubscription() + ";" + msg.getOrigin() + ":" + msg.getMessageType());
                        } else {
                            sr.setOriginUnsubscription(msg.getOrigin() + ":" + msg.getMessageType());
                        }
                        if (sr.getExternalId() == null && msg.getExternalId() != null) {
                            sr.setExternalId(msg.getExternalId());
                        }
                        sr.setUnsubscriptionDate(s.getUnsubscriptionDate());
                        sr.setChannelOut(msg.getChannelOut());
                        if (sr.getOriginSubscription() == null) {
                            sr.setOriginSubscription(msg.getOrigin() + ":" + msg.getMessageType());
                        }
                    } else {
                        /*  log.info("Se ha recibido una notificacion de baja anterior o igual a la existente en DB. s.date: ["
                                + s.getSubscriptionDate().getTime() + "][" + s.getSubscriptionDate() + "] - n.todate: [" + msg.getToDate().getTime() + "][" + msg.getToDate() + "]");*/
                        isWritter = false;
                    }
                    break;
                case SUBSCRIPTION_CANCELLATION:
                    if (s.getUnsubscriptionDate() == null && (msg.getToDate().getTime() - s.getSubscriptionDate().getTime() > 0)) {
                        s.setStatus(Subscription.Status.CANCELLED);
                        s.setUnsubscriptionDate(msg.getToDate() == null ? new Date() : msg.getToDate());
                        sr.setOriginUnsubscription(msg.getOrigin() + ":" + msg.getMessageType());
                        sr.setUnsubscriptionDate(s.getUnsubscriptionDate());
                        sr.setChannelIn(sr.getChannelIn() == null ? msg.getChannelIn() : sr.getChannelIn());
                        sr.setChannelOut(msg.getChannelOut());
                    } else {
                        log.warn("Se ha recibido una notificacion de cancelacion anterior o igual a la existente en DB. s.date: ["
                                + s.getSubscriptionDate().getTime() + "][" + s.getSubscriptionDate() + "] - n.todate: [" + msg.getToDate().getTime() + "][" + msg.getToDate() + "]");
                        isWritter = false;
                    }
                    break;
                case SUBSCRIPTION_BLACKLIST:
                    if (s.getUnsubscriptionDate() == null && (msg.getToDate().getTime() - s.getSubscriptionDate().getTime() > 0)) {
                        s.setStatus(Subscription.Status.BLACKLIST);
                        s.setUnsubscriptionDate(msg.getToDate() == null ? new Date() : msg.getToDate());
                        sr.setOriginUnsubscription(msg.getOrigin() + ":" + msg.getMessageType());
                        sr.setUnsubscriptionDate(s.getUnsubscriptionDate());
                        sr.setChannelIn(sr.getChannelIn() == null ? msg.getChannelIn() : sr.getChannelIn());
                        sr.setChannelOut(msg.getChannelOut());
                    } else {
                        log.warn("Se ha recibido una notificacion de blacklist anterior o igual a la existente en DB. s.date: ["
                                + s.getSubscriptionDate().getTime() + "][" + s.getSubscriptionDate() + "] - n.todate: [" + msg.getToDate().getTime() + "][" + msg.getToDate() + "]");
                        isWritter = false;
                    }
                    break;
            }
        }

        boolean rest = false;
        if (isWritter) {
            if (msg.isRealUserAccount()) {
                s.setUserAccount(msg.getUserAccount());
                s.getSubscriptionRegistry().setUserAccount(msg.getUserAccount());
            }
            if (msg.getExtra() != null) {
                try {
                    Extra extra = mapper.convertValue(msg.getExtra(), Extra.class);
                    if (s.getSubscriptionRegistry().getExtra() == null) {
                        s.getSubscriptionRegistry().setExtra(extra);
                    } else {
                        if (s.getSubscriptionRegistry().getExtra().serviceId == null) {
                            s.getSubscriptionRegistry().getExtra().serviceId = extra.serviceId;
                        }
                        if (s.getSubscriptionRegistry().getExtra().keyword == null) {
                            s.getSubscriptionRegistry().getExtra().keyword = extra.keyword;
                        }
                        if (s.getSubscriptionRegistry().getExtra().stringData == null) {
                            s.getSubscriptionRegistry().getExtra().stringData = extra.stringData;
                        } else {
                            s.getSubscriptionRegistry().getExtra().stringData.putAll(extra.stringData);
                        }
                    }
                } catch (Exception ex) {
                }
            }

            subscriptionManager.subscriptionProcess(s);
            try {
                boolean control = false;
                if (msg.getFrequencyType() != null && sop.getIntegrationSettings().get("frequencyType") == null) {
                    sop.getIntegrationSettings().put("frequencyType", msg.getFrequencyType().toLowerCase());
                    control = true;
                }
                if (msg.getCurrencyId() != null && sop.getIntegrationSettings().get("currency") == null) {
                    sop.getIntegrationSettings().put("currency", msg.getCurrencyId().toUpperCase());
                    control = true;
                }
                if (sop.getIntegrationSettings().get("fullAmount") == null) {
                    sop.getIntegrationSettings().put("fullAmount", msg.getFullAmount().toPlainString());
                    control = true;
                } else {
                    BigDecimal aux = new BigDecimal(sop.getIntegrationSettings().get("fullAmount"));
                    if (msg.getFullAmount() != null && msg.getFullAmount().compareTo(aux) == 1) {
                        sop.getIntegrationSettings().put("fullAmount", msg.getFullAmount().toPlainString());
                        control = true;
                    }
                }

                if (control) {
                    sopService.saveOrUpdate(sop);
                }
            } catch (Exception ex) {
                log.error("Error al actualizar SOP_INTEGRATION_SETTINGS", ex);
            }
            rest = true;
        }
        /*else {
            log.info("No se realizará ningun cambio en el registro de la suscripcion: [" + s.getId() + "]");
        }*/
        sendToBiller(msg, s);
        return rest;
    }

    private void trackingParamsProcess(PaymentHubMessage msg, Subscription s) {
        if (msg.getTrackingParams() != null && !msg.getTrackingParams().isEmpty()) {
            if (s.getSubscriptionRegistry().getAdnetworkTracking() == null) {
                s.getSubscriptionRegistry().setAdnetworkTracking(msg.getTrackingParams());
            } else {
                try {
                    HashMap map = (LinkedHashMap) s.getSubscriptionRegistry().getAdnetworkTracking();
                    map.putAll(msg.getTrackingParams());
                    s.getSubscriptionRegistry().setAdnetworkTracking(map);
                } catch (Exception ex) {
                    log.error("Error al procesar AdnetworkTracking. [" + s + "]. " + ex, ex);
                }
            }
        }
    }

    private void sendToBiller(PaymentHubMessage msg, Subscription s) {
        if (msg.isBiller() && s.getSubscriptionRegistry() != null && s.getSubscriptionRegistry().getId() != null) {
            try {
                BillerMessage bm = new BillerMessage();
                bm.setBillerType(BillerMessage.BillerType.SUBSCRIPTION);
                bm.setSubscription(s);
                bm.setSopId(s.getSop().getId());
                try {
                    String jsonResponse = mapper.writeValueAsString(bm);
                    if (jsonResponse != null) {
                        rabbitMQProducer.messageSender(BILLER_NEW + s.getSop().getProvider().getName(), jsonResponse);
                    }
                } catch (Exception ex) {
                    log.error("Error al enviar mensaje de Biller al rabbit. " + ex, ex);
                }
            } catch (Exception ex) {
                log.error("Error al enviar mensaje de suscripcion al Biller. [" + s + "]. " + ex, ex);
            }
        }
    }

}
