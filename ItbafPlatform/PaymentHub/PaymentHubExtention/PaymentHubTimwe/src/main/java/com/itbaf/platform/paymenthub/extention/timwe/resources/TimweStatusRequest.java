/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.resources;

/**
 *
 * @author JF
 */
public class TimweStatusRequest {
    
    public String userIdentifier;
    public String userIdentifierType;
    public Long productId;
    public String mcc;
    public String mnc;
    public String entryChannel;

}
