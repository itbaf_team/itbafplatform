/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.store.model.notification;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public enum ServiceType {
    SUBSCRIPTION, PACK;

        public static ServiceType getserviceType(String code) {
            if (code != null && code.length() > 0) {
                switch (code) {
                    case "17":
                        return SUBSCRIPTION;
                    case "36":
                        return PACK;
                }
                log.error("No existe un ServiceType para: [" + code + "]");
            }
            return null;
        }
}
