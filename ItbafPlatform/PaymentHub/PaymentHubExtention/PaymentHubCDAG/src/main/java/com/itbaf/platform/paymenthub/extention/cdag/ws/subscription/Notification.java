
package com.itbaf.platform.paymenthub.extention.cdag.ws.subscription;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for notification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="notification">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoEvento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="datoEvento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "notification", namespace = "urn:CdagNotificationService", propOrder = {
    "tid",
    "tipoEvento",
    "datoEvento"
})
public class Notification {

    @XmlElement(required = true)
    protected String tid;
    protected int tipoEvento;
    @XmlElement(required = true)
    protected String datoEvento;

    /**
     * Gets the value of the tid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTid() {
        return tid;
    }

    /**
     * Sets the value of the tid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTid(String value) {
        this.tid = value;
    }

    /**
     * Gets the value of the tipoEvento property.
     * 
     */
    public int getTipoEvento() {
        return tipoEvento;
    }

    /**
     * Sets the value of the tipoEvento property.
     * 
     */
    public void setTipoEvento(int value) {
        this.tipoEvento = value;
    }

    /**
     * Gets the value of the datoEvento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatoEvento() {
        return datoEvento;
    }

    /**
     * Sets the value of the datoEvento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatoEvento(String value) {
        this.datoEvento = value;
    }

    @Override
    public String toString() {
    	return String.format("Notification(%s, %s, %s)", this.tid, this.tipoEvento, this.datoEvento);
    }
}
