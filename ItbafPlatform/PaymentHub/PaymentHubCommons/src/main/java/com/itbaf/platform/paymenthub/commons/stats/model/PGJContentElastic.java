/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.stats.model;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author JF
 */
public class PGJContentElastic extends ElasticData implements Serializable {

    private static final long serialVersionUID = 20190204145101L;

    public enum FunctionType {
        ORDER, DOWNLOAD, ORDER_LEGACY
    }

    public PGJContentElastic() {
        super(ElasticData.ElasticType.pgj_content);
    }

    public Long userId;
    public String clientId;
    public String clientName;
    public Long orderId;
    public Long downloadId;
    public Date fecha;
    public FunctionType functionType;
    public Set<String> developers = new HashSet();
    public Long contentId;
    public String contentName;
    public Long contentTypeId;
    public String contentTypeName;
    public Long gameLegacyId;
    public String gameLegacyName;
    public Long userLegacyId;
    public Long developerLegacyId;
    public String developerLegacyName;
    public Long orderLegacyId;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userId", userId)
                .add("clientId", clientId)
                .add("clientName", clientName)
                .add("orderId", orderId)
                .add("downloadId", downloadId)
                .add("fecha", fecha)
                .add("functionType", functionType)
                .add("developers", developers)
                .add("contentTypeId", contentTypeId)
                .add("contentTypeName", contentTypeName)
                .add("contentId", contentId)
                .add("contentName", contentName)
                .omitNullValues().toString();
    }
}
