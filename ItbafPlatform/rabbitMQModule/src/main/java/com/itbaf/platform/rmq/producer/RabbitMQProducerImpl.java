/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.rmq.producer;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.google.inject.Inject;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author javier
 */
public class RabbitMQProducerImpl implements RabbitMQProducer {

    private final ConnectionFactory connectionFactory;

    private static final org.apache.logging.log4j.Logger LOG = LogManager.getLogger(RabbitMQProducerImpl.class);
    private final static Object connectionsToken = new Object();
    private static Connection connection;
    private static final Cache<String, Cache<String, Channel>> channels = CacheBuilder.newBuilder()
            .build();

    private static boolean stopService = false;

    @Inject
    public RabbitMQProducerImpl(final ConnectionFactory connectionFactory) {
        this.connectionFactory = Validate.notNull(connectionFactory, "A ConnectionFactory class must be provided");
        stopService = false;
    }

    private Channel getChannel(String exchangeName) {

        Channel c = null;
        Exception e = null;

        if (stopService) {
            LOG.warn("Se esta realizando una solicitud de un Producer con el servicio detenido...");
            return null;
        }

        synchronized (connectionsToken) {
            try {
                Cache<String, Channel> cn = channels.getIfPresent(exchangeName);
                if (cn != null) {
                    Set<String> aux = cn.asMap().keySet();
                    String id = aux.iterator().next();
                    c = cn.getIfPresent(id);
                    cn.invalidate(id);
                    cn.cleanUp();
                } else {
                    channels.put(exchangeName, CacheBuilder.newBuilder()
                            .expireAfterAccess(15, TimeUnit.MINUTES)
                            .removalListener(new RemovalListener() {
                                @Override
                                public void onRemoval(RemovalNotification notification) {
                                    try {
                                        if (notification.wasEvicted() || !stopService) {
                                            Channel c = (Channel) notification.getValue();
                                            c.close();
                                        }
                                    } catch (Exception ex) {
                                    }
                                }
                            }).build());
                }
            } catch (Exception ex) {
                e = ex;
            }

            try {
                if (c == null || !c.isOpen()) {
                    if (connection == null || !connection.isOpen()) {
                        connection = connectionFactory.newConnection();
                    }
                    c = connection.createChannel();
                    if (exchangeName.startsWith("fanout.")) {
                        c.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT);
                    } else {
                        c.queueDeclare(exchangeName, true, false, false, null);
                        c.exchangeDeclare(exchangeName, "topic", true, false, null);
                        c.queueBind(exchangeName, exchangeName, exchangeName);
                    }
                }
            } catch (Exception ex) {
                e = ex;
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (Exception exx) {
                    e = exx;
                }
                connection = null;
            }
        }

        if (c == null) {
            LOG.error("No fue posible obtener una conexion al RabbitMQ!!!!. " + e, e);
        }
        // LOG.info("Retornando channel: [" + c.getChannelNumber() + "] - exchangeName: [" + exchangeName + "]");
        return c;
    }

    private void closeChannel(String exchangeName, Channel c) {
        try {
            if (c != null && c.isOpen()) {
                channels.getIfPresent(exchangeName).put(System.currentTimeMillis() + "-" + c.getChannelNumber(), c);
            }
        } catch (Exception ex) {
        }
    }

    @Override
    public Boolean messageSender(String exchangeName, String message) throws Exception {
        Boolean control = messageSender(exchangeName, message, 0);
        if (control == null) {
            throw new Exception("Error al enviar el mensaje [" + message + "] a la cola [" + exchangeName + "]. ");
        }
        return control;
    }

    private Boolean messageSender(String exchangeName, String message, int cont) throws Exception {

        Channel channel = getChannel(exchangeName);
        Boolean control = sender(exchangeName, message, channel);
        closeChannel(exchangeName, channel);

        if (control == null && cont < 2) {
            cont++;
            control = messageSender(exchangeName, message, cont);
        }
        return control;
    }

    private Boolean sender(String exchangeName, String message, Channel channel) {
        if (channel == null || message == null || message.isEmpty() || exchangeName == null || exchangeName.isEmpty()) {
            LOG.error("Alguno de los parametros del sender a Rabbit son NULL. channel: [" + channel + "] - exchangeName: [" + exchangeName + "] - message: [" + message + "]");
            return null;
        }

        try {
            if (exchangeName.startsWith("fanout.")) {
                channel.basicPublish(exchangeName, "", null, message.getBytes());
            } else {
                channel.basicPublish(exchangeName, exchangeName, null, message.getBytes());
            }
            LOG.info("Rabbit.Sender - Enviado [" + exchangeName + "]:[" + message + "]");
            return true;
        } catch (Exception e) {
            LOG.fatal("Error al enviar el mensaje [" + message + "] a la cola [" + exchangeName + "]. " + e, e);
        }
        return null;
    }

    @Override
    public void stopProducers() {
        LOG.info("Finalizando conexiones Producer...");
        stopService = true;

        try {
            connection.close(1000);
        } catch (Exception ex) {
        }
        channels.invalidateAll();
        LOG.info("Finalizadas conexiones Producer.");
    }

}
