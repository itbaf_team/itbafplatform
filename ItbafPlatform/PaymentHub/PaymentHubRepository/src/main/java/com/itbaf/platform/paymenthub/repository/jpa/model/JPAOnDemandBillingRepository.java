/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.billing.OnDemandBilling;
import com.itbaf.platform.paymenthub.model.billing.QOnDemandBilling;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.querydsl.jpa.impl.JPAQuery;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAOnDemandBillingRepository extends AbstractJPARepositorySession<OnDemandBilling, String> {

    @Inject
    public JPAOnDemandBillingRepository(final Provider<EntityManager> entityManagerProvider) {
        super(OnDemandBilling.class, entityManagerProvider);
    }

    public List<OnDemandBilling> getByUserAccount(String msisdn, Country country) {
        if (msisdn == null || country == null || country.getId() == null) {
            String error = "Uno de los parametros es nulo!! msisdn: [" + msisdn + "] - country: [" + country + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QOnDemandBilling qOnDemandBilling = QOnDemandBilling.onDemandBilling;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<OnDemandBilling> list = query.select(qOnDemandBilling).from(qOnDemandBilling).
                where(ObjectPredicates.combine(qOnDemandBilling.sop().provider().country().id.eq(country.getId()),
                        qOnDemandBilling.userAccount.like("%" + msisdn)))
                .fetch();

        return list;
    }

    public List<OnDemandBilling> getByUserAccount(String userAccount) {
        if (userAccount == null) {
            String error = "Algunos de los atributos son nulos... userAccount: [" + userAccount + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QOnDemandBilling qOnDemandBilling = QOnDemandBilling.onDemandBilling;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<OnDemandBilling> list = query.select(qOnDemandBilling).from(qOnDemandBilling).
                where(qOnDemandBilling.userAccount.eq(userAccount))
                .fetch();

        return list;
    }

    public List<OnDemandBilling> getByUserAccount(Date time, String userAccount) {
        if (userAccount == null || time == null) {
            String error = "Algunos de los atributos son nulos... userAccount: [" + userAccount + "] - time: [" + time + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QOnDemandBilling qOnDemandBilling = QOnDemandBilling.onDemandBilling;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<OnDemandBilling> list = query.select(qOnDemandBilling).from(qOnDemandBilling).
                where(ObjectPredicates.combine(qOnDemandBilling.chargedDate.after(time),
                        qOnDemandBilling.userAccount.eq(userAccount)))
                .fetch();

        return list;
    }

    public List<OnDemandBilling> getByUserAccount(Date time, String msisdn, Country country) {
        if (msisdn == null || country == null || country.getId() == null || time == null) {
            String error = "Uno de los parametros es nulo!! msisdn: [" + msisdn + "] - country: [" + country + "] - time: [" + time + "]";
            log.error(error);
            throw new RuntimeException(error);
        }

        QOnDemandBilling qOnDemandBilling = QOnDemandBilling.onDemandBilling;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        List<OnDemandBilling> list = query.select(qOnDemandBilling).from(qOnDemandBilling).
                where(ObjectPredicates.combine(qOnDemandBilling.sop().provider().country().id.eq(country.getId()),
                        qOnDemandBilling.chargedDate.after(time), qOnDemandBilling.userAccount.like("%" + msisdn)))
                .fetch();

        return list;
    }

}
