package com.itbaf.platform.paymenthub.model.ondemand;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QOnDemandRegistry is a Querydsl query type for OnDemandRegistry
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOnDemandRegistry extends EntityPathBase<OnDemandRegistry> {

    private static final long serialVersionUID = 1377673475L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QOnDemandRegistry onDemandRegistry = new QOnDemandRegistry("onDemandRegistry");

    public final com.itbaf.platform.paymenthub.model.QBaseEntity _super = new com.itbaf.platform.paymenthub.model.QBaseEntity(this);

    public final StringPath id = createString("id");

    public final DateTimePath<java.util.Date> ondemandDate = createDateTime("ondemandDate", java.util.Date.class);

    public final StringPath originOndemand = createString("originOndemand");

    protected com.itbaf.platform.paymenthub.model.QSOP sop;

    public final EnumPath<OnDemandRegistry.Status> status = createEnum("status", OnDemandRegistry.Status.class);

    public final StringPath statusMessage = createString("statusMessage");

    public final StringPath transactionId = createString("transactionId");

    public final SimplePath<com.itbaf.platform.messaging.rest.FacadeRequest> transactionTracking = createSimple("transactionTracking", com.itbaf.platform.messaging.rest.FacadeRequest.class);

    public final StringPath userAccount = createString("userAccount");

    public QOnDemandRegistry(String variable) {
        this(OnDemandRegistry.class, forVariable(variable), INITS);
    }

    public QOnDemandRegistry(Path<? extends OnDemandRegistry> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QOnDemandRegistry(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QOnDemandRegistry(PathMetadata metadata, PathInits inits) {
        this(OnDemandRegistry.class, metadata, inits);
    }

    public QOnDemandRegistry(Class<? extends OnDemandRegistry> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sop = inits.isInitialized("sop") ? new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"), inits.get("sop")) : null;
    }

    public com.itbaf.platform.paymenthub.model.QSOP sop() {
        if (sop == null) {
            sop = new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"));
        }
        return sop;
    }

}

