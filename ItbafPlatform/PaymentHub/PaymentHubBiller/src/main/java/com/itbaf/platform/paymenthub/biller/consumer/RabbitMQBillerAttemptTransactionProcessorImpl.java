/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.biller.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.model.billing.BillerAttemptTransaction;
import com.itbaf.platform.paymenthub.services.repository.BillerAttemptTransactionService;
import java.sql.SQLIntegrityConstraintViolationException;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQBillerAttemptTransactionProcessorImpl implements RabbitMQBillerAttemptTransactionProcessor {

    @Inject
    private ObjectMapper mapper;
    @Inject
    private BillerAttemptTransactionService attemptTransactionService;

    @Override
    public Boolean processMessage(String message) {
        Boolean rest = null;
        try {
            BillerAttemptTransaction bat = mapper.readValue(message, BillerAttemptTransaction.class);
            attemptTransactionService.saveOrUpdate(bat);
            rest = true;
        } catch (ConstraintViolationException | SQLIntegrityConstraintViolationException ex) {
            log.warn("Registro duplicado. [" + message + "]. " + ex);
            rest = false;
        } catch (Exception ex) {
            boolean control = true;
            try {
                if (ex.getCause() instanceof ConstraintViolationException) {
                    control = false;
                    log.warn("Registro duplicado. [" + message + "]. " + ex.getCause().getCause());
                }
            } catch (Exception exx) {
            }

            if (control) {
                log.error("Error al procesar notificacion de cobro: [" + message + "]. " + ex, ex);
            } else {
                rest = false;
            }
        }
        return rest;
    }

}
