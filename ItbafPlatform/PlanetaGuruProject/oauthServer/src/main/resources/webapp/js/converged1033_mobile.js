var router = new Navigo(window.location.protocol + "//" + window.location.host, false);

!function () {
    router.on({'/oauth/signin/login/:tid': phoneLoginForm,
        '/oauth/signin/secure/login': invalidLogin,
        '/oauth/signin/secure/verify/:tid': verifyBox
    }).resolve();
}();

function verifyBox(p, query) {
    var un = getLocaleScript('converged1033');
    var html;
    if (pparams.credentials !== undefined && pparams.credentials.error === 13) {
        phoneLoginForm(pparams);
        html = tmpl($("#verifySuccessful").html(), {"params": pparams});
        $(html).fadeIn("slow").appendTo("#alert-Ext");
    } else {
        if (pparams.credentials !== undefined && pparams.credentials.hasPin === 1) {
            html = tmpl($("#verifyInfoCode").html(), {"params": pparams});
            $("#form-data").html("");
            $(html).fadeIn("slow").appendTo("#form-data");

            if (pparams.credentials.error === 1) {
                html = tmpl($("#invalidVerificationCodeScript").html(), {"params": pparams});
                $("#alert").html(html);
            } else if (pparams.credentials.error === 2) {
                html = tmpl($("#maxAttemptsScript").html(), {"params": pparams});
                $("#alert").html(html);
            } else {
                $("#alert").html("");
            }

            $("#i0281").attr("action", "/oauth/signin/secure/verify/" + pparams.path);
            $("#vnextButton").on("click", function (e) {
                e.preventDefault();
                var code = $("#code").val();
                if (code === undefined || code.length !== 6) {
                    e.preventDefault();
                    $("#code").addClass("has-error");
                    html = tmpl($("#verificationCodeEmptyScript").html(), {"params": pparams});
                    $("#alert").html(html);
                    return;
                }

                $("#i0281").attr("action", "/oauth/signin/secure/verify/" + pparams.path);
                $("#i0281").submit();
                return;
            });
        } else {
            html = tmpl($("#verifyInfo").html(), {"params": pparams});
            $(html).fadeIn("slow").appendTo("#form-data");
            $("#signinButton").on("click", function (e) {
                e.preventDefault();
                window.location.href = '../../login/' + pparams.tid + "?locale=" + un;
            });
        }
    }
}

function pProcess(providerName) {
    var provider;
    switch (providerName) {
        case "g":
            provider = new firebase.auth.GoogleAuthProvider();
            provider.addScope('email');
            provider.addScope('profile');
            provider.addScope('https://www.googleapis.com/auth/plus.me');
            provider.addScope('https://www.googleapis.com/auth/userinfo.email');
            provider.addScope('https://www.googleapis.com/auth/userinfo.profile');
            break;
        case "fb":
            provider = new firebase.auth.FacebookAuthProvider();
            provider.addScope('email');
            provider.addScope('public_profile');
            break;
    }


    if (isMobile()) {
        firebase.auth().signInWithRedirect(provider);
    } else {
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var user = result.user;
            user.getIdToken(true).then(function (token) {
                var ptoken = result.credential.accessToken;
                sendTProvider(token, ptoken, result.credential.providerId);
            }).catch(function (error) {
            });
        }).catch(function (error) {
        });
    }

}

function sendTProvider(token, ptoken, provider) {
    var html = hiddenParameter("tokenCode", token)
            + hiddenParameter("ptokenCode", ptoken)
            + hiddenParameter("provider", provider);
    $(html).appendTo("#form-var");
    $("#i0281").attr("action", "/oauth/signin/secure/provider");
    $("#i0281").attr("action", "/oauth/signin/secure/provider");
    $("#i0281").attr("action", "/oauth/signin/secure/provider");
    $("#i0281").submit();
}

function invalidLogin(params, query) {
    params = pparams;
    params.locale = getLocaleScript('converged1033');

    $("#form-data").html("");
    var html = hiddenParameter("tid", params.tid) + hiddenParameter("username", params.username);
    $(html).appendTo("#form-var");
    processCredentialType(params, pparams);
}

function phoneLoginForm(params, query) {
    pushEvent('/oauth/signin/login/phone');
    $("#form-data").html("");
    params.locale = getLocaleScript('converged1033');
    firebase.auth().languageCode = params.locale;
    firebase.auth().getRedirectResult().then(function (result) {
        var user = result.user;
        user.getIdToken(true).then(function (token) {
            if (result.credential) {
                var ptoken = result.credential.accessToken;
                sendTProvider(token, ptoken, result.credential.providerId);
            }
        }).catch(function (error) {
        });
    }).catch(function (error) {
    });

    params.kmsi = 1;
    $("#form-data").html("");
    var html = hiddenParameter("tid", params.tid);
    $(html).appendTo("#form-var");

    var html = tmpl($("#loginForm").html(), {"params": params});
    $(html).fadeIn("slow").appendTo("#form-data");

    $("#PhoneNumber").intlTelInput({
        geoIpLookup: function (callback) {
            $.post("/oauth/auth/country/ip", function () {}, "json").always(function (resp) {
                respGeoIP = resp;
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);

                if (countryCode.length > 1) {
                    $("#Country").val(countryCode);
                    params.countryCode = countryCode;
                    if (params.phone) {
                        $("#PhoneNumber").intlTelInput("setNumber", params.phone);
                    }
                    $("#PhoneNumber").intlTelInput("setCountry", countryCode);
                    params.phone = $("#PhoneNumber").intlTelInput("getNumber");
                    if (params.phone) {
                        $("#PhoneNumber").intlTelInput("setNumber", params.phone);
                    }
                }
            });
        },
        initialCountry: "auto",
        nationalMode: false,
        separateDialCode: true,
        utilsScript: "https://assets.planeta.guru/js/oauth/utils.js",
        preferredCountries: pparams.codes
    });

    if (params.phone) {
        $("#PhoneNumber").intlTelInput("setNumber", params.phone);
    }
    if (params.countryCode) {
        $("#PhoneNumber").intlTelInput("setCountry", params.countryCode);
    }

    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('signinButton', {
        'size': 'invisible',
        'callback': function (response) {
        }
    });

    /*Boton NEXT del formulario Phone*/
    $("#signinButton").on("click", function (e) {
        document.getElementById("signinButton").disabled = true;
        var phone = $("#PhoneNumber").intlTelInput("getNumber");
        var phoneCountryCode = $("#PhoneNumber").intlTelInput("getSelectedCountryData");
        phone = phone.replace("+", "");
        if (isInteger(phone) && phone.length > 6) {
            pushEvent('/oauth/signin/login/phone/number');
            phone = getInteger(phone);
            params.phone = phone;
            params.usernameType = "PHONE";
            params.username = phone + "";
            params.location = phoneCountryCode.iso2;
            getCredentialType(params);
        } else {
            e.preventDefault();
            pushEvent('/oauth/signin/login/phone/input/error');
            html = tmpl($("#phoneErrorScript").html(), {"params": params});
            $("#alert").html(html);
            $("#phone").addClass("has-error");
            document.getElementById("signinButton").disabled = false;
        }
    });
}

function processCredentialType(params, data) {
    if (data.ifExistsResult === 0) {
        if (params.usernameType === "PHONE") {
            if (data.credentials.error === 4) {
                var html = tmpl($("#phoneExistErrorScript").html(), {"params": params});
                $("#alert").html(html);
                document.getElementById("signinButton").disabled = false;
                return;
            }
        } else {
            var html = tmpl($("#usernameExistErrorScript").html(), {"params": params});
            $("#alert").html(html);
            document.getElementById("signinButton").disabled = false;
            return;
        }
    }
    params.username = data.username;
    params.credentials = data.credentials;
    $("#form-data").html("");
    var html = tmpl($("#passwordCodeLoginForm").html(), {"params": params});
    $(html).fadeIn("slow").appendTo("#form-data");

    if (params.usernameType === "PHONE") {
        if (params.credentials.error === 1) {
            var html = tmpl($("#invalidVerificationCodeScript").html(), {"params": params});
            if (data.credentials.hasPassword === 1) {
                html = tmpl($("#invalidPasswordScript").html(), {"params": params});
            }
            $("#alert").html(html);
        } else if (params.credentials.error === 2) {
            var html = tmpl($("#phoneMaxAttemptsScript").html(), {"params": params});
            if (data.credentials.hasPassword === 1) {
                html = tmpl($("#usernameMaxAttemptsScript").html(), {"params": params});
            }
            $("#alert").html(html);
        }

        $("#pcBackButton").on("click", function (e) {
            e.preventDefault();
            params.credentials.send = undefined;
            phoneLoginForm(params);
        });

        if (data.credentials.hasPassword === 1) {
            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('switchToPhoneCode', {
                'size': 'invisible',
                'callback': function (response) {
                }
            });
            $("#switchToPhoneCode").on("click", function (e) {
                e.preventDefault();
                data.credentials.hasPassword = 0;
                var html = hiddenParameter("tid", params.tid) + hiddenParameter("username", params.username);
                $(html).appendTo("#form-var");
                processCredentialType(params, data);
            });
            data.credentials.send = undefined;
        } else if (!data.credentials.send || data.credentials.send === undefined) {
            if (data.credentials.hasPin === 1) {
                spp(params);
            } else {
                spg(params);
            }
        }
        $("#signinButton").on("click", function (e) {
            if (data.credentials.hasPassword === 1) {
                var pass = $("#passwd").val();
                if (pass === undefined || pass.length < 7) {
                    e.preventDefault();
                    $("#passwd").addClass("has-error");
                    html = tmpl($("#passwordEmptyScript").html(), {"params": params});
                    $("#alert").html(html);
                    return;
                }
                params.pass = pass;
                params.codeValidate = undefined;
                $("#i0281").submit();
            } else {
                if (data.credentials.hasPin === 1) {
                    var code = $("#code4").val();
                    if (code === undefined || code.length < 4) {
                        e.preventDefault();
                        $("#code4").addClass("has-error");
                        html = tmpl($("#verificationCodeEmpty4Script").html(), {"params": params});
                        $("#alert").html(html);
                        return;
                    }
                    params.code = code;
                    var html = hiddenParameter("vcode", params.code)
                            + hiddenParameter("username", params.username)
                            + hiddenParameter("usernameType", params.usernameType)
                            + hiddenParameter("tid", params.tid);
                    $(html).appendTo("#form-var");
                    $("#i0281").submit();
                } else {
                    var code = $("#code").val();
                    if (code === undefined || code.length !== 6) {
                        e.preventDefault();
                        $("#code").addClass("has-error");
                        html = tmpl($("#verificationCodeEmptyScript").html(), {"params": params});
                        $("#alert").html(html);
                        return;
                    }
                    params.code = code;
                    params.codeValidate = undefined;
                    window.confirmationResult.confirm(code).then(function (result) {
                        firebase.auth().currentUser.getIdToken(true).then(function (code) {
                            params.code = code;
                            var html = hiddenParameter("tokenCode", params.code)
                                    + hiddenParameter("username", params.username)
                                    + hiddenParameter("usernameType", params.usernameType)
                                    + hiddenParameter("tid", params.tid);
                            $(html).appendTo("#form-var");
                            $("#i0281").submit();
                        }).catch(function (error) {
                            data.credentials.error = 1;
                            data.credentials.send = 1;
                            e.preventDefault();
                        });
                    }).catch(function (error) {
                        window.recaptchaVerifier.render().then(function (widgetId) {
                            grecaptcha.reset(widgetId);
                        });
                        if (error.code === 'auth/code-expired') {
                            spg(params);
                        }

                        data.credentials.error = 1;
                        data.credentials.send = 1;

                        $("#code").addClass("has-error");
                        html = tmpl($("#invalidVerificationCodeScript").html(), {"params": params});
                        $("#alert").html(html);
                        e.preventDefault();
                    });
                }
            }
            e.preventDefault();
        });
    } else if (params.usernameType === "EMAIL") {
        if (params.credentials.error === 1) {
            var html = tmpl($("#invalidPasswordScript").html(), {"params": params});
            $("#alert").html(html);
        } else if (params.credentials.error === 2) {
            var html = tmpl($("#usernameMaxAttemptsScript").html(), {"params": params});
            $("#alert").html(html);
        }

        $("#pcBackButton").on("click", function (e) {
            e.preventDefault();
            phoneLoginForm(params, null);
        });
        $("#signinButton").on("click", function (e) {
            var pass = $("#passwd").val();
            if (pass === undefined || pass.length < 7) {
                e.preventDefault();
                $("#passwd").addClass("has-error");
                html = tmpl($("#passwordEmptyScript").html(), {"params": params});
                $("#alert").html(html);
                return;
            }
            params.pass = pass;
            params.codeValidate = undefined;
        });
    }
}

function spp(params) {
    $("#code4Id").css("display", "block");
    $("#codeId").css("display", "none");
    if (params.credentials.error === 1) {
        $("#code4").addClass("has-error");
        html = tmpl($("#invalidVerificationCodeScript").html(), {"params": params});
        $("#alert").html(html);
    }

    getSecure("spin", "usernameType=" + params.usernameType + "&username=" + params.username
            + "&tid=" + params.tid + "&locale=" + params.locale + "&location=" + params.location + "&kmsi=" + params.kmsi);
}

function spg(params) {
    var appVerifier = window.recaptchaVerifier;
    if (appVerifier !== null) {
        firebase.auth().signInWithPhoneNumber('+' + params.username, appVerifier)
                .then(function (confirmationResult) {
                    window.confirmationResult = confirmationResult;
                }).catch(function (error) {
        });
    }
}

function getCredentialType(params) {
    $("#usernameProgress").css("display", "inline");
    $.ajax({
        type: "POST",
        url: "/oauth/auth/basic/credential/type",
        contentType: 'application/json; charset=UTF-8',
        data: JSON.stringify(params),
        success: function (data) {
            $("#usernameProgress").css("display", "none");
            if (data.ifExistsResult === 1 && data.redirectUrl) {
                window.location.href = data.redirectUrl;
            } else {
                var html = hiddenParameter("tid", params.tid) + hiddenParameter("username", params.username);
                $(html).appendTo("#form-var");
                processCredentialType(params, data);
            }
        },
        error: function (data) {
            $("#usernameProgress").css("display", "inline");
            alert("Error al obtener los datos.");
        },
        dataType: "json"
    });
}

function getSecure(path, params) {
    $("#usernameProgress").css("display", "inline");
    $.ajax({
        type: "POST",
        url: "/oauth/signin/secure/" + path,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        data: params,
        success: function (data) {
            $("#usernameProgress").css("display", "none");
        },
        error: function (data) {
            $("#usernameProgress").css("display", "inline");
            alert("Error al obtener los datos.");
        },
        dataType: "json"
    });
}