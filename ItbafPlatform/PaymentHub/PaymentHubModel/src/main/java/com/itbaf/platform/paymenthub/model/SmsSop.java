package com.itbaf.platform.paymenthub.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.persistence.util.HibernateEquality;
import com.querydsl.core.annotations.Config;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity(name = "SMS_SOP")
@Table(name = "SMS_SOP", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors = true)
public class SmsSop implements Serializable {

    private static final long serialVersionUID = -6470600685848259462L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;

    @Column(name = "MESSAGE_TEXT", nullable = true)
    private String message;

    @Column(name = "LAST_SENDED", nullable = true)
    private Boolean last_sended;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("sop", sop)
                .add("multipleSms", last_sended)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sop, last_sended);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<SmsSop>()
                .over(SmsSop::getId)
                .over(SmsSop::getSop)
                .over(SmsSop::getLast_sended)
                .asserts(this, obj);
    }
}
