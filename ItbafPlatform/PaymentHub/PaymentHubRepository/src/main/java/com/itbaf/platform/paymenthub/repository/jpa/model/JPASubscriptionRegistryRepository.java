/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.persistence.util.predicates.ObjectPredicates;
import com.itbaf.platform.paymenthub.model.subscription.QSubscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.model.subscription.QSubscriptionRegistry;
import com.querydsl.jpa.impl.JPAQuery;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPASubscriptionRegistryRepository extends AbstractJPARepositorySession<SubscriptionRegistry, Long> {

    @Inject
    public JPASubscriptionRegistryRepository(final Provider<EntityManager> entityManagerProvider) {
        super(SubscriptionRegistry.class, entityManagerProvider);
    }

    public SubscriptionRegistry findByExternalId(String externalId) {
        SubscriptionRegistry sr = null;
        if (externalId != null) {
            QSubscriptionRegistry subscription = QSubscriptionRegistry.subscriptionRegistry;
            JPAQuery<?> query = new JPAQuery(getEntityManager());

            sr = query.select(subscription).from(subscription).
                    where(ObjectPredicates.combine(subscription.externalId.eq(externalId)))
                    .orderBy(subscription.id.asc())
                    .fetchFirst();
        }

        return sr;
    }

    public List<SubscriptionRegistry> getOrphanRegisters() {
        List<SubscriptionRegistry> srl = null;

        QSubscriptionRegistry subscriptionRegistry = QSubscriptionRegistry.subscriptionRegistry;
        QSubscription subscription = QSubscription.subscription;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        srl = query.select(subscriptionRegistry).from(subscriptionRegistry)
                .innerJoin(subscription).on(subscriptionRegistry.subscription().eq(subscription))
                .where(ObjectPredicates.combine(subscription.subscriptionRegistry().id.isNotNull(),
                        subscription.subscriptionRegistry().id.ne(subscriptionRegistry.id),
                        subscriptionRegistry.unsubscriptionDate.isNull()))
                .fetch();

        return srl;
    }

    public List<SubscriptionRegistry> getRegistersBySubscription(Long subscriptionId) {
        List<SubscriptionRegistry> srl = null;

        QSubscriptionRegistry subscriptionRegistry = QSubscriptionRegistry.subscriptionRegistry;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        srl = query.select(subscriptionRegistry).from(subscriptionRegistry)
                .where(subscriptionRegistry.subscription().id.eq(subscriptionId))
                .orderBy(subscriptionRegistry.id.asc())
                .fetch();

        return srl;
    }

    public SubscriptionRegistry getLastRegisterBySubscription(Long subscriptionId, boolean isPenult) {
        List<SubscriptionRegistry> srl = null;

        QSubscriptionRegistry subscriptionRegistry = QSubscriptionRegistry.subscriptionRegistry;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        srl = query.select(subscriptionRegistry).from(subscriptionRegistry)
                .where(subscriptionRegistry.subscription().id.eq(subscriptionId))
                .orderBy(subscriptionRegistry.subscriptionDate.desc())
                .fetch();

        if (srl.size() > 1 && isPenult) {
            return srl.get(1);
        }

        if (srl.size() > 0) {
            return srl.get(0);
        }

        return null;
    }

}
