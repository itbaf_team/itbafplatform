/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.biller.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.BILLER_PROCESS;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.commons.messaging.BillerMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.billing.BillerAttempt;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.BillerAttemptService;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author JF
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler {

    private final ObjectMapper mapper;
    private final RabbitMQProducer rabbitMQProducer;
    private final InstrumentedObject instrumentedObject;
    private final SubscriptionManager subscriptionManager;
    private final BillerAttemptService billerAttemptService;

    @Inject
    public ServiceProcessorHandler(final ObjectMapper mapper,
            final RabbitMQProducer rabbitMQProducer,
            final InstrumentedObject instrumentedObject,
            final SubscriptionManager subscriptionManager,
            final BillerAttemptService billerAttemptService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.subscriptionManager = Validate.notNull(subscriptionManager, "A SubscriptionManager class must be provided");
        this.billerAttemptService = Validate.notNull(billerAttemptService, "A BillerAttemptService class must be provided");
    }

    public BillerAttempt createBillerAttempt(Subscription s) {
        BillerAttempt ba = null;
        try {
            ba = new BillerAttempt();
            ba.setId(s.getSubscriptionRegistry().getId());
            Date d = new Date();
            ba.setLastAttempt(d);
            ba.setNextAttempt(d);
            ba.setSop(s.getSop());
            ba.setTargetAmount(s.getSop().getMainTariff().getFullAmount());
            ba.setTotalAmount(BigDecimal.ZERO);
            billerAttemptService.saveOrUpdate(ba);
            log.info("Creado registro de Biller: [" + ba + "]");
        } catch (Exception ex) {
            log.error("Error al crear registro de Biller. [" + s + "]. " + ex, ex);
        }
        return ba;
    }

    public Boolean sendBillerProcess(BillerAttempt ba) {
        Boolean result = null;

        if (ba == null || ba.getId() == null || ba.getSop() == null) {
            return result;
        }

        BillerMessage bm = null;
        RLock lock = null;
        try {
            lock = instrumentedObject.lockObject(ba.getId() + "_attempt_" + ba.getSop().getId(), 5);
            if (instrumentedObject.getAtomicLong(ba.getId() + "_attempts_" + ba.getSop().getId(), 60) == 0l) {
                bm = new BillerMessage();
                bm.setBillerAttempt(ba);
                bm.setBillerType(BillerMessage.BillerType.BILLING);

                SubscriptionRegistry sr = subscriptionManager.getSubscriptionRegistry(ba.getId());
                if (sr.getId().equals(sr.getSubscription().getSubscriptionRegistry().getId()) && Subscription.Status.ACTIVE.equals(sr.getSubscription().getStatus())) {
                    bm.setExternalCustomer(sr.getSubscription().getExternalCustomer());
                    bm.setExternalId(sr.getExternalId());
                    bm.setExternalUserAccount(sr.getSubscription().getExternalUserAccount());
                    bm.setUserAccount(sr.getSubscription().getUserAccount());
                    bm.setSopId(ba.getSop().getId());

                    String jsonResponse = mapper.writeValueAsString(bm);
                    if (jsonResponse != null) {
                        result = rabbitMQProducer.messageSender(BILLER_PROCESS + ba.getSop().getProvider().getName(), jsonResponse);
                        if (result != null && result) {
                            instrumentedObject.getAndIncrementAtomicLong(ba.getId() + "_attempts_" + ba.getSop().getId(), 15);
                            instrumentedObject.getAndIncrementAtomicLong("BILLER_NEXT_ATTEPMTS_" + ba.getSop().getProvider().getName(), 60);
                        }
                    }
                } else {
                    result = false;
                }
            } else {
                result = false;
            }
        } catch (Exception ex) {
            log.error("Error al enviar BillerMessage. [" + bm + "]. " + ex, ex);
        }
        instrumentedObject.unLockObject(lock);

        return result;
    }

    public void findActiveBillingAttempt(Provider p) {
        Long val = instrumentedObject.getAtomicLong("BILLER_NEXT_ATTEPMTS_" + p.getName(), 60);
        log.info("[" + val + "]. Next Attempts. [" + p.getName() + "]");
        if (val <= 5) {
            instrumentedObject.resetAtomicLong("BILLER_NEXT_ATTEPMTS_" + p.getName());
            List<BillerAttempt> bal = billerAttemptService.getNextAttempts(p);
            if (bal == null) {
                return;
            }
            log.info("Biller attempts: [" + bal.size() + "]");
            for (BillerAttempt ba : bal) {
                sendBillerProcess(ba);
            }
        }
    }
}
