/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.base.MoreObjects;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 *
 * @author javier
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "topic")
@JsonSubTypes({
    @JsonSubTypes.Type(value = ProfileUserProperty.class, name = "PROFILE")
    ,
    @JsonSubTypes.Type(value = SessionUserProperty.class, name = "SESSION")
    ,
    @JsonSubTypes.Type(value = ExternalDataUserProperty.class, name = "EXTERNAL_DATA")
    ,
    @JsonSubTypes.Type(value = CredentialUserProperty.class, name = "CREDENTIAL")})
public abstract class UserProperty {

    public enum Topic {
        PROFILE,
        CREDENTIAL,
        SESSION,
        EXTERNAL_DATA
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "TOPIC")
    private final Topic topic;

    public UserProperty(Topic topic) {
        this.topic = topic;
    }

    public Topic getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("topic", topic)
                .omitNullValues().toString();
    }
}
