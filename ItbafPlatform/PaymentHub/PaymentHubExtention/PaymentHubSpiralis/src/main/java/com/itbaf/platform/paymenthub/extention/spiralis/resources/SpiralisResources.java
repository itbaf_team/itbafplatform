package com.itbaf.platform.paymenthub.extention.spiralis.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class SpiralisResources {

    private final RequestClient requestClient;
    private final ObjectMapper mapper;
    private final PHProfilePropertiesService profilePropertiesService;
    private static final String SPIRALIS_URL = "http://besubscription.com/BeSubscription";
    //private static final String SPIRALIS_URL = "http://162.209.99.39/BeSubscription";

    @Inject
    public SpiralisResources(final ObjectMapper mapper,
            final RequestClient requestClient,
            final PHProfilePropertiesService profilePropertiesService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    public ResponseMessage validateSubscriptionByMsisdn(String msisdn, SOP sop) {
        ResponseMessage rm = new ResponseMessage();
        try {
            String providerId = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "external.service.providerid");
            String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "external.service.password");
            String serviceId = sop.getIntegrationSettings().get("serviceId");
            String url = String.format("%s/UserStatus?providerId=%s&password=%s&serviceId=%s&msisdn=%s",
                    SPIRALIS_URL,
                    providerId, password, serviceId, msisdn);
            String response = requestClient.requestSimpleGet(url, RequestClient.TIMEOUT_MS_7000);
            log.info("validateSubscriptionByMsisdn. ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. Respuesta de Spiralis: [" + response + "]");
            if (response == null) {
                rm.message = "Respuesta NULL desde Spiralis";
                return rm;
            }
            String aux[] = response.split("\\|");

            switch (aux[0]) {
                case "1"://suscrito
                    rm.status = ResponseMessage.Status.ACTIVE;
                    break;
                case "2"://cancelado
                case "-11"://La suscripción no existe o no está activa.
                    rm.status = ResponseMessage.Status.REMOVED;
                    break;
                case "3"://retencion
                    rm.status = ResponseMessage.Status.PENDING;
                    break;
                case "-16"://Usuario se encuentra en lista gris del Operador.
                case "-17"://Usuario se encuentra en lista negra del Operador.
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    break;
            }
            rm.message = aux[1];
        } catch (Exception ex) {
            rm.message = "Error al validar estado de ani: [" + msisdn + "]. " + ex;
            log.error(rm.message, ex);

        }
        return rm;
    }

    public ResponseMessage sendSubscriptionPin(String msisdn, SOP sop) {
        ResponseMessage rm = new ResponseMessage();
        try {
            String providerId = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "external.service.providerid");
            String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "external.service.password");
            String serviceId = sop.getIntegrationSettings().get("serviceId");
            String url = String.format("%s/SendPIN?providerId=%s&password=%s&serviceId=%s&msisdn=%s",
                    SPIRALIS_URL,
                    providerId, password, serviceId, msisdn);
            String response = requestClient.requestSimpleGet(url);
            log.info("sendSubscriptionPin. ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. Respuesta de Spiralis: [" + response + "]");
            String aux[] = response.split("\\|");

            switch (aux[0]) {
                case "0"://pin enviado... 
                    rm.status = ResponseMessage.Status.OK;
                    break;
                case "1"://suscrito
                    rm.status = ResponseMessage.Status.ACTIVE;
                    break;
                case "3"://retencion
                    rm.status = ResponseMessage.Status.PENDING;
                    break;
                case "-16"://Usuario se encuentra en lista gris del Operador.
                case "-17"://Usuario se encuentra en lista negra del Operador.
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    break;
            }
            rm.message = aux[1];
        } catch (Exception ex) {
            rm.message = "Error al enviar la solicitud de PIN. ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. " + ex.getMessage();
            log.error(rm.message, ex);
        }
        return rm;
    }

    public ResponseMessage validatePin(String msisdn, SOP sop, String pin) {

        ResponseMessage rm = new ResponseMessage();
        try {
            String providerId = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "external.service.providerid");
            String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "external.service.password");
            String serviceId = sop.getIntegrationSettings().get("serviceId");
            String url = String.format("%s/ConfirmPIN?providerId=%s&password=%s&serviceId=%s&msisdn=%s&pin=%s",
                    SPIRALIS_URL,
                    providerId, password, serviceId, msisdn, pin);
            String response = requestClient.requestSimpleGet(url);

            log.info("validatePin. ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. PIN: [" + pin + "]. Respuesta de Spiralis: [" + response + "]");
            String aux[] = response.split("\\|");
            switch (aux[0]) {
                case "0"://pin valido... validar esto
                    rm.status = ResponseMessage.Status.OK;
                    break;
                case "1"://suscrito
                    rm.status = ResponseMessage.Status.ACTIVE;
                    break;
                case "3"://retencion
                    rm.status = ResponseMessage.Status.PENDING;
                    break;
                case "-14"://El PIN ha caducado, solicitar nuevamente envío de PIN.
                case "-15"://El PIN es incorrecto.
                    rm.status = ResponseMessage.Status.PIN_ERROR;
                    break;
                case "-16"://Usuario se encuentra en lista gris del Operador.
                case "-17"://Usuario se encuentra en lista negra del Operador.
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    break;
            }
            rm.message = aux[1];
        } catch (Exception ex) {
            rm.message = "Error al validar ANI: [" + msisdn + "]. SOP: [" + sop + "]. PIN: [" + pin + "]. " + ex.getMessage();
            log.error(rm.message, ex);
        }
        return rm;
    }

    public ResponseMessage.Status removeSubscription(Subscription subscription) {

        String msisdn = subscription.getUserAccount();
        SOP sop = subscription.getSop();
        try {
            String providerId = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "external.service.providerid");
            String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "external.service.password");
            String serviceId = sop.getIntegrationSettings().get("serviceId");
            String url = String.format("%s/Unsubscribe?providerId=%s&password=%s&serviceId=%s&msisdn=%s",
                    SPIRALIS_URL,
                    providerId, password, serviceId, msisdn);
            String response = requestClient.requestSimpleGet(url);
            log.info("removeSubscription. ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. Respuesta de Spiralis: [" + response + "]");
            return ResponseMessage.Status.OK;
        } catch (Exception ex) {
            log.error("Error al dar de baja la suscripcion. ANI: [" + msisdn + "]. SOP: [" + sop + "]. " + ex, ex);
        }
        return null;
    }

    public ResponseMessage sendSMS(String msisdn, SOP sop, String message) {
        ResponseMessage rm = new ResponseMessage();
        try {
            String providerId = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "external.service.providerid");
            String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "external.service.password");
            String serviceId = sop.getIntegrationSettings().get("serviceId");
            String url = String.format("%s/SendSMS?providerId=%s&password=%s&serviceId=%s&destinations=%s&message=%s&type=0",
                    SPIRALIS_URL,
                    providerId, password, serviceId, msisdn, URLEncoder.encode(message, StandardCharsets.UTF_8.toString()));
            String response = requestClient.requestSimpleGet(url);
            log.info("sendSMS. ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. Respuesta desde Spiralis: [" + response + "]");

            String aux[] = response.split("\\|");
            switch (aux[0]) {
                case "0":// Mensaje enviado
                    rm.status = ResponseMessage.Status.OK;
                    break;
                case "-16"://Usuario se encuentra en lista gris del Operador.
                case "-17"://Usuario se encuentra en lista negra del Operador.
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    break;
            }
            rm.message = aux[1];
            return rm;
        } catch (Exception ex) {
            log.error("Error al enviar SMS. ANI: [" + msisdn + "]. SOP: [" + sop + "]. " + ex, ex);
        }
        return null;
    }

}
