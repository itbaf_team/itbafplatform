/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.smpp.modules;

import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.modules.GuiceDefaultConfigModule;
import com.itbaf.platform.paymenthub.repository.module.GuiceRepositoryConfigModule;
import com.itbaf.platform.paymenthub.services.module.GuiceServiceConfigModule;
import com.itbaf.platform.rmq.module.GuiceRabbitMQConfigModule;
import java.util.Arrays;
import java.util.Map;

/**
 *
 * @author jordonez
 */
public class GuiceConfigModule extends GuiceDefaultConfigModule {

    public GuiceConfigModule(final Map<String, String> args, CommonDaemon commonDaemon) {
        super(args,commonDaemon);
    }

    @Override
    protected void configure() {
        super.configure();
       
        //Binds
        /*   final Multibinder<NotificationProcessor> multi = Multibinder.newSetBinder(binder(), NotificationProcessor.class);
        multi.addBinding().to(SubscriptionNotificationProcessor.class).asEagerSingleton();
        multi.addBinding().to(BillingNotificationProcessor.class).asEagerSingleton();
         */
        //Modulos de aplicaciones
        install(new GuiceRepositoryConfigModule());
        install(new GuiceServiceConfigModule());
        install(new GuiceConsumersConfigModule());
        install(new GuiceRabbitMQConfigModule());
        install(new GuiceRestServicesConfigModule());
        
        log.info("Configuracion iniciada. " + Arrays.toString(args.entrySet().toArray()));
    }

}
