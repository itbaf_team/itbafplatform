/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.paymenthub.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.inject.Injector;
import com.itbaf.platform.instance.GuiceInstance;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.ProviderService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionBillingService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.ws.rs.core.MediaType;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

/**
 *
 * @author javier
 */
public class MainTest {

    final Injector injector;

    public MainTest() {
        injector = GuiceInstance.getInstance();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    @Ignore
    public void syncSubscriptions() {
        ProviderService providerService = injector.getInstance(ProviderService.class);
        Provider provider = providerService.findByName("mx.claro.npay");

        SubscriptionManager subscriptionManager = injector.getInstance(SubscriptionManager.class);
        List<Subscription> slist = subscriptionManager.getSyncSubscriptions(provider);

        assertNotNull(slist);
        System.out.println("Size: [" + slist.size() + "]");
    }

    @Test
    @Ignore
    public void sopServiceTest() {
        SopService sopService = injector.getInstance(SopService.class);
        SOP sop = sopService.findWithTariffBySettings("co.movistar", "id_suscripcion", "1882");
        System.out.println("SOP: [" + sop + "]");
    }

    @Test
    @Ignore
    public void getOrphanRegistersTest() {
        SubscriptionRegistryService subscriptionRegistryService = injector.getInstance(SubscriptionRegistryService.class);
        List<SubscriptionRegistry> lst = subscriptionRegistryService.getOrphanRegisters();
        System.out.println("List<SubscriptionRegistry>: [" + lst == null ? lst : lst.size() + "]");
    }

    @Test
    @Ignore
    public void externalUserAccountNPAYUpdate() throws ParseException {

        SubscriptionService subscriptionService = injector.getInstance(SubscriptionService.class);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date start = sdf.parse("2017-09-01");
        Date end = sdf.parse("2017-10-01");
        System.out.println("Buscando suscripciones");
        List<Subscription> sl = subscriptionService.findSubscriptionsByProvider("mx.claro.npay", start, end);
        System.out.println("Suscripciones: [" + sl.size() + "]");
        assertNotNull(sl);
        assertFalse(sl.isEmpty());

        final AtomicInteger ai = new AtomicInteger(0);
        final AtomicInteger xx = new AtomicInteger(0);
        for (Subscription s : sl) {
            xx.getAndIncrement();
            if (s.getUserAccount().length() > 20) {
                continue;
            }
            while (120 < ai.get()) {

            }
            int x = ai.getAndIncrement();
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        String url;

                        if (System.currentTimeMillis() % 2 == 0) {
                            url = "http://server.ph.svc.cluster.local:8070/rest/facade/subscription/status";
                            // url="http://localhost:8070/rest/facade/subscription/status";
                        } else {
                            url = "http://server.ph.svc.cluster.local:8070/rest/facade/subscription/status";
                            // url="http://localhost:8070/rest/facade/subscription/status";
                        }
                        System.out.println("[" + xx.get() + "][" + x + "] Response: [" + requestJSONPost(url, "{"
                                + "\"userAccount\":\"" + s.getUserAccount() + "\","
                                + "\"service\":\"" + s.getSop().getServiceMatcher().getName() + "\","
                                + "\"operator\":\"ITBAF\","
                                + "\"provider\":\"" + s.getSop().getProvider().getName() + "\","
                                + "\"action\":\"RequestModule\","
                                + "\"persist\":\"force\""
                                + "}") + "]");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    ai.getAndDecrement();
                }
            }).start();
        }
        System.out.println("ihhh");
        while (ai.get() > 0) {

        }
        System.out.println("ehhh");
        try {
            Thread.sleep(27000);
        } catch (InterruptedException ex) {
        }
        System.out.println("ahhh");
    }

    @Test
    @Ignore
    public void jsonTest() throws Exception {
        String json = "{\"medium\": \"sf_movistar_ar\", \"source\": \"adkaudal\", \"campaign\": \"sf\", \"adnNotType\": {\"ttl\": 86400000, \"lastSubs\": 6, \"billPerce\": 10, \"subscription\": \"SubsBill\"}, \"adnTrackingId\": [\"HYDlr6AK7\"]}";
        ObjectMapper mapper = injector.getInstance(ObjectMapper.class);

        JsonObject jo = mapper.readValue(json, JsonObject.class);
        System.out.println(jo);
    }

    @Test
    @Ignore
    public void emtelTimwe() throws ParseException, InterruptedException {
        //  SELECT * FROM SUBSCRIPTION SS WHERE SS.SOP_ID IN (70,74,75,76) 
        //  AND SS.USER_ACCOUNT IN (SELECT S.USER_ACCOUNT FROM SUBSCRIPTION_REGISTRY S 
        //  WHERE S.SOP_ID IN (70,74,75,76) AND S.ORIGIN_UNSUBSCRIPTION='NOTIFICATION:SUBSCRIPTION_ACTIVE');
        //--- --- ---
        //   UPDATE SUBSCRIPTION_BILLING SBX 
        //  INNER JOIN
        //      (SELECT S.ID,S.SUBSCRIPTION_REGISTRY_ID SRID, SB.SUBSCRIPTION_REGISTRY_ID, SB.ID SBID 
        //       FROM SUBSCRIPTION_BILLING SB, SUBSCRIPTION_REGISTRY SR, SUBSCRIPTION S 
        //       WHERE SB.SOP_ID IN (70,74,75,76) AND SB.SUBSCRIPTION_REGISTRY_ID=SR.ID AND S.ID=SR.SUBSCRIPTION_ID 
        //       AND SR.ID!=S.SUBSCRIPTION_REGISTRY_ID AND SB.CHARGED_DATE <='2019-09-25')X
        //   ON X.SBID=SBX.ID
        //   SET SBX.SUBSCRIPTION_REGISTRY_ID=X.SRID
        //   ;
        //--- --- ---
        //  DELETE FROM SUBSCRIPTION_REGISTRY  WHERE ID IN (SELECT * FROM(SELECT SR.ID FROM SUBSCRIPTION S, SUBSCRIPTION_REGISTRY SR 
        //  WHERE SR.SUBSCRIPTION_ID=S.ID AND S.SUBSCRIPTION_REGISTRY_ID!=SR.ID AND S.SOP_ID IN (70,74,75,76) AND SR.SUBSCRIPTION_DATE<'2019-09-24')x)
        //  ;
        //--- --- ---
        /*  DELETE FROM SUBSCRIPTION_REGISTRY  WHERE ID IN (
	SELECT * FROM (
		select SR.ID FROM SUBSCRIPTION_REGISTRY SR WHERE SR.SOP_ID IN (70,74,75,76) AND SR.USER_ACCOUNT IN(
		51977498441,51924823945,51989336206,51998629075,51988344195,51933395062,51974329915,51924092898,519815072290
		)
	)X
        ) */
        //--- --- ---
        // DELETE FROM BASE_RESUMEN WHERE PROVIDER_NAME = 'pe.entel.timwe' AND F_TYPE IN ('SUSCRIPCION','BAJA');
        String ua = "51952675969,51955578763,51941029112,51977723792,51970476731,51974971685,51964822308,51992410377,51983503352,51981365774,51945235903,51949911963,"
                + "51923309555,51977187459,51954953873,51999492595,51995753781,51985208702,51997040553,51924505481,51993017828,51991715593,51992850022,51936029750,"
                + "51952085314,51945774161,51942692234,51993544590,51980587058,51955378442,51985763328,51977525591,51926951810,51945194189,51980847148,51941915990,"
                + "51995205100,51947386695,51934161142,51998629004,51993700029,51927487147,51964419713,51959066113,51991681770,51974628953,51962703177,51970515636,"
                + "51971186002,51983451793,51955083517,51968788540,51973537179,51984901414,51992597269,51964625139,51954027599,51934151306,51986235559,51949042222,51934237290";

        String[] aux = ua.split(",");

        SopService sopService = injector.getInstance(SopService.class);
        SubscriptionService subscriptionService = injector.getInstance(SubscriptionService.class);
        SubscriptionRegistryService subscriptionRegistryService = injector.getInstance(SubscriptionRegistryService.class);
        SubscriptionBillingService subscriptionBillingService = injector.getInstance(SubscriptionBillingService.class);

        SOP sop = sopService.findById(75l);

        int i = 0;
        for (String ani : aux) {
            i++;
            if (i % 5 == 0) {
                Thread.sleep(5000);
            }
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Subscription s = subscriptionService.findByUserAccount(ani, sop);
                        List<SubscriptionRegistry> srs = subscriptionRegistryService.getRegistersBySubscription(s.getId());
                        SubscriptionRegistry a;
                        SubscriptionRegistry b;
                        SubscriptionRegistry ab;
                        if (srs.size() == 2) {
                            a = srs.get(0);
                            b = srs.get(1);
                        } else if (srs.size() == 3 && srs.get(0).getOriginUnsubscription().equals(srs.get(1).getOriginUnsubscription())) {
                            a = srs.get(0);
                            b = srs.get(2);
                        } else {
                            System.out.println("sid extraño: [" + s.getId() + "]");
                            return;
                        }

                        if (a.getId() > b.getId()) {
                            ab = a;
                            a = b;
                            b = ab;
                        }

                        a.setUnsubscriptionDate(b.getUnsubscriptionDate());
                        a.setOriginUnsubscription(b.getOriginUnsubscription());
                        if (a.getUnsubscriptionDate() == null) {
                            a.getExtra().statusChanged.remove(Subscription.Status.CANCELLED);
                        } else {
                            a.getExtra().statusChanged.put(Subscription.Status.CANCELLED, a.getUnsubscriptionDate());
                        }
                        s.setSubscriptionRegistry(a);
                        s.setSubscriptionDate(a.getSubscriptionDate());
                        try {
                            subscriptionRegistryService.saveOrUpdate(a);
                            subscriptionService.saveOrUpdate(s);
                        } catch (Exception ex) {
                            System.out.println("Error: [" + s.getId() + "]");
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }).start();
        }
        Thread.sleep(20000);
        assertFalse(false);
    }

    @Test
    @Ignore
    public void smtSubscriptionMigration() throws ParseException {

        SopService sopService = injector.getInstance(SopService.class);
        SubscriptionService subscriptionService = injector.getInstance(SubscriptionService.class);
        SubscriptionRegistryService subscriptionRegystryService = injector.getInstance(SubscriptionRegistryService.class);

        SOP sop = sopService.findById(28l);

        String path = "/home/javier/Desktop/SMT.txt";
        String aux = null;
        try ( BufferedReader br = new BufferedReader(new FileReader(path))) {
            while ((aux = br.readLine()) != null) {
                System.out.println("MSISDN: [" + aux + "]");
                aux = aux.trim();

                Subscription s = new Subscription();
                s.setSop(sop);
                s.setSubscriptionDate(new Date());
                s.setSubscriptionRegistry(new SubscriptionRegistry());
                s.setUserAccount(aux);
                s.setStatus(Subscription.Status.ACTIVE);

                SubscriptionRegistry sr = s.getSubscriptionRegistry();
                sr.setOriginSubscription("SMT. Migracion del Biller");
                sr.setSop(sop);
                sr.setSubscriptionDate(s.getSubscriptionDate());
                sr.setUserAccount(aux);

                System.out.println("----- ----- -----");
                s.setSubscriptionRegistry(null);
                subscriptionService.saveOrUpdate(s);
                System.out.println("Creado S: [" + s + "]");
                sr.setSubscription(s);
                subscriptionRegystryService.saveOrUpdate(sr);
                System.out.println("Creado SR: [" + sr + "]");
                s.setSubscriptionRegistry(sr);
                subscriptionService.saveOrUpdate(s);
                System.out.println("Actualizado S: [" + s + "]");
                System.out.println("----- ----- -----");
            }
            assertFalse(false);
        } catch (Exception ex) {
            System.out.println("Error [" + aux + "]");
            ex.printStackTrace();
            assertFalse(true);
        }

    }

    private static String requestJSONPost(String url, String json) {
        String result = null;
        Client client = Client.create();
        if (json == null) {
            json = "";
        }
        // System.out.println("Request a: [" + url + "] - JSON: [" + json + "]");
        try {
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                    .post(ClientResponse.class,
                            json);

            if (response.getStatus() == 201 || response.getStatus() == 200) {
            } else {
                result = response.getEntity(String.class
                );
                if (result == null) {
                    result = "";
                }
                System.out.println("Request [" + url + "] - JSON: [" + json + "]. Codigo: [" + response.getStatus() + "] - Error: " + result);

            }
            if (result == null) {
                result = response.getEntity(String.class
                );
            }
        } catch (Exception ex) {
            System.out.println("Error request: [" + url + "] - JSON: [" + json + "]. " + ex);
        }

        return result;
    }
}
