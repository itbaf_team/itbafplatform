/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons;

import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.DefaultClaims;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author javier
 */
public class CommonFunction {

    public static final String PARTIAL_ADN_NOTIFICATION = "adn.notification.partial";
    public static final String NOTIFICATION_SUBSCRIPTION = ".subscription";
    public static final String NOTIFICATION_BILLING = ".billing";
    public static final String NOTIFICATION_BILLING_OD = ".billing.od";
    public static final String NOTIFICATION_RECEIVED_NAME = "n.r.";
    public static final String BORDER_NOTIFICATION = "bn.";
    public static final String NPAY_QUEUE_NAME = "claro.npay";
    public static final String BILLER_NEW = "biller.new.";
    public static final String BILLER_PROCESS = "biller.process.";
    public static final String BILLER_PROCESSED = "biller.processed.";
    public static final String BILLER_PROCESSED_ERROR = "biller.processed.error.";
    public static final String BILLER_TRANSACTION = "biller.transaction";
    public static final String CHACHE_MEMORY_UPDATE = "fanout.memory.update";
    public static final String HTTP_REQUEST_URL = "httpRequestURL";
    public static final String HTTP_MESSAGE_TYPE = "httpMessageType";
    public static final String PROCESS_DW_EVENTS_QUEUE = "dw.events.process.";
    public static final String PROCESS_DW_ETL_QUEUE = "dw.etl.process.";
    public static final String ADAPTER_SMS_SEND = "adptr.sms.send.";
    public static final String ADAPTER_SMS_RCV = "adptr.sms.rcv.";
    public static final String ADAPTER_SMS_PORTING = "adptr.sms.porting.";
    private static final AtomicInteger ai = new AtomicInteger(0);
    private static final Object aiToken = new Object();

    public static synchronized String getTID() {

        String tid;

        synchronized (aiToken) {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.HHmmss.SSS");
            ai.compareAndSet(10000, 0);
            tid = sdf.format(new Date()) + "." + ai.getAndIncrement();
        }

        return tid;
    }

    public static synchronized String getTID(String name) {
        String tid;

        synchronized (aiToken) {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.HHmmss.SSS");
            ai.compareAndSet(10000, 0);
            tid = sdf.format(new Date()) + "." + ai.getAndIncrement() + "-" + name;
        }

        return tid;
    }

    public static DefaultClaims validateToken(String secret, String jws) throws Exception {
        if (jws == null) {
            return null;
        }
        Jwt jwt = Jwts.parser().setSigningKey((secret).getBytes("UTF-8")).parse(jws);
        return (DefaultClaims) jwt.getBody();
    }
}
