/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;
import java.util.Date;
import java.util.List;

/**
 *
 * @author javier
 */
public interface SubscriptionBillingService {

    public SubscriptionBilling findById(Long id) throws Exception;

    public void save(SubscriptionBilling sb) throws Exception;

    public List<SubscriptionBilling> getSubscriptionBillingsByUserAccount(String userAccount);

    public List<SubscriptionBilling> getSubscriptionBillingsByUserAccount(Date time, String userAccount);

    public List<Object> getSubscriptionBillingResume(Long id);
}
