/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties.attributes;

import java.util.Date;
import java.io.Serializable;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public abstract class UsernameAttribute implements Serializable {

    private static final long serialVersionUID = 20181207172507L;

    protected Date date;
    protected Boolean confirmed;
    protected Boolean primary;

}
