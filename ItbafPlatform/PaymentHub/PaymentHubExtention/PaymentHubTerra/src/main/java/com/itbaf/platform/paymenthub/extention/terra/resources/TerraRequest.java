/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.terra.resources;

/**
 *
 * @author JF
 */
public class TerraRequest {

    public TerraRequest[] REQUEST;
    public String OUTPUTFORMAT;
    public String VERIFYCODE;

    public Long PIN;
    public String ORDER;
    public Long MSISDN;
    public Long IDPRODUCT;
    public Long IDOPERATOR;
    public String SERVICE;
    public String DEVICEID;
    public String DEVICENAME;
    public String DESCRIPTION;
    public String SMSTEXT;
    public String SECURE;
    public Integer GETSHORTCODE;

    public TerraRequest() {
        OUTPUTFORMAT = "json";
        REQUEST = new TerraRequest[1];
        REQUEST[0] = new TerraRequest(false);
    }

    public TerraRequest(boolean j) {
    }
}
