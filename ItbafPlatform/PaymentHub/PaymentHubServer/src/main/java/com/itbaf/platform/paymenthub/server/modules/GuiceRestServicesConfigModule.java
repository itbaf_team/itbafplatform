package com.itbaf.platform.paymenthub.server.modules;

import com.google.inject.Singleton;
import com.itbaf.platform.commons.filter.RequestFilter;
import com.itbaf.platform.paymenthub.commons.rest.ManagerRestServiceImpl;
import com.itbaf.platform.paymenthub.server.rest.OnDemandFacadeRestService;
import com.itbaf.platform.paymenthub.server.rest.SubscriptionFacadeRestService;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import java.util.HashMap;

public class GuiceRestServicesConfigModule extends JerseyServletModule {

    @Override
    protected void configureServlets() {
        
        bind(ManagerRestServiceImpl.class).in(Singleton.class);
        bind(SubscriptionFacadeRestService.class).in(Singleton.class);
        bind(OnDemandFacadeRestService.class).in(Singleton.class);

        HashMap<String, String> options = new HashMap();
        options.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

        //Filters
        this.filter("/rest/*").through(RequestFilter.class);

        //Servlets
        serve("/rest/*").with(GuiceContainer.class, options);

    }
}
