package com.itbaf.platform.paymenthub.extention.store.model.notification;

public enum BillingEventDataStatus {

    Ok(0, "Ok"),
    SessionExpired(2, "Sesión vencida"),
    ConnectionLimitExceeded(19, "Límite de Conexiones Excedido"),
    SystemError(100, "Error de Sistema"),
    DoesNotApply(128, "No Aplica"),
    ConnectionErrorWithAppraiser(129, "Error de Conexión con el Tasador"),
    InsufficientBalance(2000, "Saldo Insuficiente"),
    InvalidNumber(2001, "Número Inválido"),
    InvalidParameters(2002, "Parámetros no válidos"),
    ProfileError(2003, "Error de Perfil"),
    BillingError(2004, "Error al Realizar el Cobro"),
    UnknownTransaction(2005, "Transacción Desconocida"),
    UnsupportedTransaction(2006, "Transacción no Soportada"),
    NoResponse(2007, "Sin Respuesta"),
    InvalidResponse(2008, "Respuesta Inválida"),
    QueueFull(2009, "Cola Llena"),
    AbsorbidoExterno(2010, "Absorbido Externo"),
    BannedUser(2011, "Usuario Bloqueado"),
    MaxCreditExceeded(2012, "Crédito Máximo Excedido"),
    Discarded(4000, "Descartado"),
    SaturationDiscarded(4001, "Descartado Saturado (Usuario supera el límite de crédito máximo");

    private Integer code;
    private String description;

    BillingEventDataStatus(Integer code, String description) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    @Override
    public String toString() {
        return this.description;
    }

    public static BillingEventDataStatus fromCode(Integer code) {
        for (BillingEventDataStatus item : BillingEventDataStatus.values()) {
            if (item.getCode().equals(code)) {
                return item;
            }
        }

        throw new IllegalArgumentException(String.format("unrecognized code: %s", code));
    }
}
