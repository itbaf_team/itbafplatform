/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.dw.events;

import com.google.inject.Inject;
import com.itbaf.platform.dw.events.consumer.RabbitMQStatsMessageProcessorFactory;
import com.itbaf.platform.dw.events.processor.StatsProcessor;
import com.itbaf.platform.commons.CommonFunction;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import com.itbaf.platform.dw.events.etl.EtlRunnable;
import com.itbaf.platform.paymenthub.commons.stats.model.ElasticData;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.rmq.services.RabbitMQMemoryProcessorImpl;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class DwServerMain {

    private final EtlRunnable etlRunnable;
    private final ExecutorService cachedThreadPool;
    private final RabbitMQConsumer rabbitMQConsumer;
    private final BasicDataBaseConnection dataBaseConnection;
    private final RabbitMQMemoryProcessorImpl memoryProcessor;
    private final ScheduledExecutorService scheduledThreadPool;
    private final PHProfilePropertiesService profilePropertiesService;
    private final RabbitMQStatsMessageProcessorFactory rabbitMQStatsMessageProcessorFactory;
    private final Map<ElasticData.ElasticType, StatsProcessor> statsProcessorMap;

    @Inject
    public DwServerMain(final EtlRunnable etlRunnable,
            final ExecutorService cachedThreadPool,
            final RabbitMQConsumer rabbitMQConsumer,
            final Set<StatsProcessor> statsProcessorSet,
            final BasicDataBaseConnection dataBaseConnection,
            final RabbitMQMemoryProcessorImpl memoryProcessor,
            final ScheduledExecutorService scheduledThreadPool,
            final PHProfilePropertiesService profilePropertiesService,
            final RabbitMQStatsMessageProcessorFactory rabbitMQStatsMessageProcessorFactory) {
        this.etlRunnable = Validate.notNull(etlRunnable, "An EtlRunnable class must be provided");
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "An ExecutorService class must be provided");
        this.rabbitMQConsumer = Validate.notNull(rabbitMQConsumer, "A RabbitMQConsumer class must be provided");
        this.memoryProcessor = Validate.notNull(memoryProcessor, "A RabbitMQMemoryProcessorImpl class must be provided");
        this.dataBaseConnection = Validate.notNull(dataBaseConnection, "A BasicDataBaseConnection class must be provided");
        this.scheduledThreadPool = Validate.notNull(scheduledThreadPool, "A ScheduledExecutorService class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A ProfilePropertiesService class must be provided");
        this.rabbitMQStatsMessageProcessorFactory = Validate.notNull(rabbitMQStatsMessageProcessorFactory, "A RabbitMQStatsMessageProcessorFactory class must be provided");

        statsProcessorMap = new HashMap();
        if (statsProcessorSet != null) {
            log.info("statsProcessorSet size: " + statsProcessorSet.size());
            for (StatsProcessor nr : statsProcessorSet) {
                log.info("JF- StatsProcessor item: [" + nr.getElasticType() + "] - [" + nr.getClass().getSimpleName() + "]");
                statsProcessorMap.put(nr.getElasticType(), nr);
            }
        }
    }

    public void start() {
        log.info("Initializing app... ");
        try {
            rabbitMQConsumer.createFanoutChannel(CHACHE_MEMORY_UPDATE, memoryProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer '" + CHACHE_MEMORY_UPDATE + "'. " + ex, ex);
        }

        Integer statsQuantity = Integer.parseInt(profilePropertiesService.getCommonProperty("rabbitmq.bo.dw.events.consumer"));
        for (ElasticData.ElasticType et : statsProcessorMap.keySet()) {
            StatsProcessor st = statsProcessorMap.get(et);
            if (ElasticData.ElasticType.query.equals(et)) {
                createConsumer(CommonFunction.PROCESS_DW_EVENTS_QUEUE + et, 1, st);
            } else {
                createConsumer(CommonFunction.PROCESS_DW_EVENTS_QUEUE + et, statsQuantity, st);
            }
        }

        scheduledThreadPool.scheduleWithFixedDelay(etlRunnable, 1, 60, TimeUnit.MINUTES);
        log.info("Initializing app OK. ");
    }

    private RabbitMQConsumer createConsumer(String exchangeName, Integer consumerQuantity, StatsProcessor statsProcessor) {
        rabbitMQConsumer.createChannel(exchangeName, consumerQuantity, createRabbitMQMessageProcessor(statsProcessor));
        return rabbitMQConsumer;
    }

    private RabbitMQMessageProcessor createRabbitMQMessageProcessor(StatsProcessor statsProcessor) {
        return rabbitMQStatsMessageProcessorFactory.create(statsProcessor);
    }

    public void stop() {
        rabbitMQConsumer.stopConsumers();
        stopThreads();
        dataBaseConnection.shutdownConnectionsPool();
        log.info("App destroyed. ");
    }

    public void stopThreads() {
        log.info("Finalizando Threads en 5 segundos... ");
        try {
            Thread.sleep(4000);
        } catch (InterruptedException ex) {
        }
        try {
            log.info("Finalizando scheduledThreadPool ");
            scheduledThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            log.info("Esperando Threads por 2 segundos... ");
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
        }

        try {
            scheduledThreadPool.shutdownNow();
            log.info("Finalizado scheduledThreadPool ");
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdownNow();
        } catch (Exception ex) {
        }
    }

}
