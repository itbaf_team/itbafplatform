/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.model.AdnetworkNotifier;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPAAdnetworkNotifierRepository;
import com.itbaf.platform.paymenthub.services.repository.AdnetworkNotifierService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class AdnetworkNotifierServiceTransactional implements AdnetworkNotifierService {

    private final JPAAdnetworkNotifierRepository adnetworkNotifierRepository;

    @Inject
    public AdnetworkNotifierServiceTransactional(final JPAAdnetworkNotifierRepository adnetworkNotifierRepository) {
        this.adnetworkNotifierRepository = Validate.notNull(adnetworkNotifierRepository, "A JPAAdnetworkNotifierRepository class must be provided");
    }

    @Override
    public AdnetworkNotifier findById(Long id) throws Exception {
        return adnetworkNotifierRepository.findById(id);
    }

    @Override
    public void saveOrUpdate(AdnetworkNotifier adnetworkNotifier) throws Exception {
        try {
            adnetworkNotifierRepository.merge(adnetworkNotifier);
        } catch (Exception ex) {
            try {
                adnetworkNotifierRepository.save(adnetworkNotifier);
            } catch (Exception exx) {
                log.error("Error al guardar AdnetworkNotifier: [" + adnetworkNotifier + "]. " + exx, exx);
            }
        }
    }

}
