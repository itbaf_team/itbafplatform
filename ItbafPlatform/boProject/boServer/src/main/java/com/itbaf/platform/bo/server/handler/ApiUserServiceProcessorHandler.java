/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.bo.server.handler;

import com.google.inject.Inject;
import com.itbaf.platform.bo.server.model.UserResponse;
import com.itbaf.platform.messaging.rest.pg.Meta;
import com.itbaf.platform.messaging.rest.pg.PGMessageResponse;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ApiUserServiceProcessorHandler extends ServiceProcessorHandler {

    @Inject
    public ApiUserServiceProcessorHandler() {
        /*    this.productService = Validate.notNull(productService, "A ProductService class must be provided");
        this.apiWalletServiceProcessorHandler = Validate.notNull(apiWalletServiceProcessorHandler, "An ApiWalletServiceProcessorHandler class must be provided");
        this.apiProfileServiceProcessorHandler = Validate.notNull(apiProfileServiceProcessorHandler, "An ApiProfileServiceProcessorHandler class must be provided");*/
    }

    public PGMessageResponse getUserInformation(User user) {
        UserResponse ur = new UserResponse();
        ur.id = user.getId();
        ur.nickname = user.getCredentialUserProperty().getUsername().getNickname();
        ur.avatar = user.getCredentialUserProperty().getUsername().getAvatarURL();
        ur.permissions = user.getLimitPermissions();
        
        String url=genericPropertyService.getValueByKey("pg.server.url");
        
        ur.params.put("logoutURL", url+"/account/signout/slo");
        ur.params.put("profileURL", url+"/account/");

        PGMessageResponse rm = new PGMessageResponse();
        rm.meta.code = 200;
        rm.meta.status = Meta.Status.ok;
        rm.data.content = ur;
        return rm;
    }

}
