/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.adapter.processor;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.paymenthub.adapter.resources.SmppAdapterResource;
import com.itbaf.platform.paymenthub.messaging.smpp.adapter.AdapterSMSMessage;
import com.itbaf.platform.paymenthub.model.sms.AdapterSmsLogSent;
import com.itbaf.platform.paymenthub.services.repository.AdapterSmsLogSentService;
import java.util.Date;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class MtAdapterProcessor extends AdapterProcessor {

    private final SmppAdapterResource smppAdapterResource;
    private final AdapterSmsLogSentService adapterSmsLogSentService;

    @Inject
    public MtAdapterProcessor(final SmppAdapterResource smppAdapterResource,
            final AdapterSmsLogSentService adapterSmsLogSentService) {
        super(AdapterType.MT_ADAPTER);
        this.smppAdapterResource = Validate.notNull(smppAdapterResource, "A SmppAdapterResource class must be provided");
        this.adapterSmsLogSentService = Validate.notNull(adapterSmsLogSentService, "An AdapterSmsLogSentService class must be provided");
    }

    @Override
    public Boolean process(String msg) throws Exception {
        log.info("Procesando MT_ADAPTER [" + msg + "]");

        AdapterSMSMessage am = mapper.readValue(msg, AdapterSMSMessage.class);
        AdapterSmsLogSent as = new AdapterSmsLogSent();
        as.setMessage(am);
        as.setTid("TID_" + am.sendSmsRequest.getOriginationAddr() + "_" + am.sendSmsRequest.getDestinationAddr()
                + "_" + am.createdDate.getTime() + "_" + (++am.SendSmsResponseAttempt));

        if (am.SendSmsResponseAttempt > 3) {
            log.fatal("Exceso de intentos de envio de SMSs. [" + am + "]");
            return null;
        }

        smppAdapterResource.sendSmsService(am);
        try {
            adapterSmsLogSentService.save(as);
        } catch (Exception ex) {
            as.setTid(as.getTid() + "_e_" + (new Date().getTime()));
            try {
                adapterSmsLogSentService.save(as);
            } catch (Exception exx) {
                log.error("No fue posible persistir el mensaje: [" + as + "]. " + exx, exx);
            }
        }
        if (am.sendSmsResponse == null) {
            log.error("Error al enviar SMS al SmppAdapter. Message: [" + am + "] ");
            return rabbitMQProducer.messageSender(CommonFunction.ADAPTER_SMS_SEND + am.providerName, mapper.writeValueAsString(am));
        } else if (am.sendSmsResponse.getResultCode() == 0) {
            return true;
        } else if (am.sendSmsResponse.getResultCode() == -1 || am.sendSmsResponse.getResultCode() == 20 || am.sendSmsResponse.getResultCode() == 88) {
            log.warn("Fallo el envio del SMS. Se hara otro intento de envio. [" + as + "]");
            Thread.sleep(5000);
            am.sendSmsResponse = null;
            return rabbitMQProducer.messageSender(CommonFunction.ADAPTER_SMS_SEND + am.providerName, mapper.writeValueAsString(am));
        } else if (am.sendSmsResponse.getResultCode() == 102) {
            log.info("El MSISDN [" + am.sendSmsRequest.getDestinationAddr() + "] - No pertenece al proveedor: [" + am.providerName + "] ");
            return rabbitMQProducer.messageSender(CommonFunction.ADAPTER_SMS_PORTING + am.providerName, mapper.writeValueAsString(am));
        }
        return null;
    }

}
