/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model;

import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPATariffRepository extends AbstractJPARepositorySession<Tariff, Long> {

    @Inject
    public JPATariffRepository(final Provider<EntityManager> entityManagerProvider) {
        super(Tariff.class, entityManagerProvider);
    }

}
