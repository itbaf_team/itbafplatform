/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.repository.jpa.model.wallet;

import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.itbaf.platform.pgp.model.wallet.WalletUser;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAWalletUserRepository extends AbstractJPARepositorySession<WalletUser, Long> {

    @Inject
    public JPAWalletUserRepository(final Provider<EntityManager> entityManagerProvider) {
        super(WalletUser.class, entityManagerProvider);
    }

    public void deleteByUserId(Long userId) {
        String sql = "DELETE FROM W_USER WHERE USER_ID = :userId ";

        EntityManager em = getEntityManager();
        int i = em.createNativeQuery(sql)
                .setParameter("userId", userId)
                .executeUpdate();
        log.info("Delete WalletUser: [" + userId + "][" + i + "]");
    }
}
