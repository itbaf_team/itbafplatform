/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.log4j2.appender.cloud;

import com.google.cloud.logging.LogEntry;
import com.google.cloud.logging.LoggingOptions;
import com.itbaf.platform.commons.log4j2.appender.CloudAppender;
import com.itbaf.platform.commons.model.ConsoleLogEntry;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import java.io.Serializable;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;

/**
 *
 * @author JF
 */
public class JsonConsoleCloudAppender extends CloudAppender {

    public JsonConsoleCloudAppender(String name, Filter filter, Layout<? extends Serializable> layout, LoggingOptions options) {
        super(name, filter, layout, options);
    }

    public JsonConsoleCloudAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions, LoggingOptions options) {
        super(name, filter, layout, ignoreExceptions, options);
    }

    @Override
    public void append(LogEvent event) {
        try {
            if (Level.FATAL.equals(event.getLevel()) || Level.ERROR.equals(event.getLevel())) {
                final ConsoleLogEntry cle = errorLogEntryFor(event);
                String json=InstrumentedObject.mapper.writeValueAsString(cle);
                System.err.println(json);
            } else {
                final ConsoleLogEntry cle = infoLogEntryFor(event);
                String json=InstrumentedObject.mapper.writeValueAsString(cle);
                System.out.println(json);
            }
        } catch (Exception ex) {
            System.out.print(providerId + new String(getLayout().toByteArray(event)));
        }
    }

}
