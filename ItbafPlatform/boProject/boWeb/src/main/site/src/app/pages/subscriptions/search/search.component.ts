import { Component, ViewChild } from '@angular/core';
import { ButtonSearchComponent } from './button-search/button-search.component';
import { TableSearchComponent } from './table-search/table-search.component';

@Component({
  selector: 'ngx-subscriptions-search',
  styleUrls: ['./search.component.scss'],
  templateUrl: './search.component.html',
})
export class SearchComponent {
    
    spinner=false;
    @ViewChild('buttonSearch') buttonSearch: ButtonSearchComponent;
    @ViewChild('tableSearch') tableSearch: TableSearchComponent;
    
    constructor() { 
    }
  
    getSpinnerStatus(): any {
        this.buttonSearch.setSearchComponent(this);
        this.tableSearch.setSearchComponent(this);
        return this.spinner;
    }
    
    setSpinnerStatus(spinner) {
        this.spinner=spinner;
    }
}
