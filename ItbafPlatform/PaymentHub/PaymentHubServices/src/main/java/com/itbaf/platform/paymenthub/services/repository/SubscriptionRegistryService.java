/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import java.util.List;

/**
 *
 * @author javier
 */
public interface SubscriptionRegistryService {

    public void saveOrUpdate(SubscriptionRegistry sr) throws Exception;

    public void saveOrUpdate(SubscriptionRegistry sr, boolean event) throws Exception;

    public SubscriptionRegistry findByExternalId(String externalId);

    public SubscriptionRegistry findById(Long id) throws Exception;

    public List<SubscriptionRegistry> getOrphanRegisters();

    public List<SubscriptionRegistry> getRegistersBySubscription(Long subscriptionId);

    public SubscriptionRegistry getLastRegisterBySubscription(Long subscriptionId, boolean isPenult);
    
}
