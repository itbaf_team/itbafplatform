/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.store.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.paymenthub.extention.store.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.sms.AdapterSmsLogReceived;
import com.itbaf.platform.paymenthub.services.repository.AdapterSmsLogReceivedService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import io.jsonwebtoken.lang.Collections;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class RabbitMQMoProcessor implements RabbitMQMessageProcessor {

    private final ObjectMapper mapper;
    private final SopService sopService;
    private final ServiceProcessorHandler serviceProcessor;
    private final AdapterSmsLogReceivedService adapterSmsLogReceivedtService;

    @Inject
    public RabbitMQMoProcessor(final ObjectMapper mapper,
            final SopService sopService,
            final ServiceProcessorHandler serviceProcessor,
            final AdapterSmsLogReceivedService adapterSmsLogReceivedtService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.sopService = Validate.notNull(sopService, "A SopService class must be provided");
        this.serviceProcessor = Validate.notNull(serviceProcessor, "A ServiceProcessorHandler class must be provided");
        this.adapterSmsLogReceivedtService = Validate.notNull(adapterSmsLogReceivedtService, "An AdapterSmsLogReceivedtService class must be provided");
    }

    @Override
    public Boolean processMessage(String message) {
        try {
            String tid = Thread.currentThread().getName() + ".mo";
            Thread.currentThread().setName(tid);
        } catch (Exception ex) {
        }

        try {
            AdapterSmsLogReceived ar = mapper.readValue(message, AdapterSmsLogReceived.class);
            SOP sop = getSopFromMO(ar);
            if (sop != null) {
                log.info("SOP: [" + sop.getId() + "] para el message: [" + message + "]");
                ar.getMessage().sopId = sop.getId();
                ar.getMessage().serviceName = sop.getServiceMatcher().getName();
                ar.getMessage().operatorName = sop.getOperator().getName();
                ar.getMessage().providerName = sop.getProvider().getName();

                try {
                    adapterSmsLogReceivedtService.saveOrUpdate(ar);
                } catch (Exception ex) {
                }

                //TODO ver que debería pasar
             //   return serviceProcessor.smsSubscribe(sop, ar);
            }
            return true;
        } catch (Exception ex) {
            log.error("Error al procesar MO: [" + message + "]");
        }

        return null;
    }

    private SOP getSopFromMO(AdapterSmsLogReceived ar) throws IOException {
        SOP sop = null;

        String msg = ar.getMessage().rcvSmsRequest.getMessage();

        if (msg == null || msg.length() < 3) {
            return null;
        }
        msg = msg.toLowerCase().trim();
        if (msg.contains("  ")) {
            String[] ma = msg.split(" ");
            msg = "";
            for (String s : ma) {
                if (s.length() > 1) {
                    msg = msg + s + " ";
                }
            }
            msg = msg.trim();
        }

        Object aux = MainCache.memory48Hours().getIfPresent("Patterns_providerName_" + ar.getMessage().providerName);
        Map<Pattern, SOP> patterns;
        if (aux == null) {
            patterns = new HashMap();
            List<SOP> sops = sopService.getSOPsByProviderName(ar.getMessage().providerName);
            if (!Collections.isEmpty(sops)) {
                for (SOP sa : sops) {
                    String text = sa.getIntegrationSettings().get("sms.mo.pattern.keyword.alta");
                    if (text != null) {
                        String[] txts = mapper.readValue(text, String[].class);
                        if (txts != null) {
                            for (String s : txts) {
                                patterns.put(Pattern.compile(s, Pattern.CASE_INSENSITIVE), sa);
                            }
                        }
                    }
                }
            }
            if (!patterns.isEmpty()) {
                MainCache.memory48Hours().put("Patterns_providerName_" + ar.getMessage().providerName, patterns);
            }
        } else {
            patterns = (Map<Pattern, SOP>) aux;
        }

        if (!patterns.isEmpty()) {
            for (Entry<Pattern, SOP> entry : patterns.entrySet()) {
                if (entry.getKey().matcher(msg).matches()) {
                    sop = entry.getValue();
                    break;
                }
            }
        }

        return sop;
    }

}
