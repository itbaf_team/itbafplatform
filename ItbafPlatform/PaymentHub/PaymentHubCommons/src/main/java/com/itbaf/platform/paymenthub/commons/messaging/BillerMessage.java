/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.messaging;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.model.billing.BillerAttempt;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import java.math.BigDecimal;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class BillerMessage {

    public enum BillerType {
        SUBSCRIPTION, BILLING
    }

    private BillerType billerType;
    private MessageType messageType;
    private BillerAttempt billerAttempt;
    private Subscription subscription;
    private Long sopId;
    private BigDecimal netAmount;
    private BigDecimal fullAmount;
    private String externalId;
    private String userAccount;
    private String externalUserAccount;
    private String externalCustomer;
    private String transactionId;//ID unico de transaccion
    public String thread = Thread.currentThread().getName();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("billerType", billerType)
                .add("messageType", messageType)
                .add("sopId", sopId)
                .add("billerAttempt", billerAttempt)
                .add("subscription", subscription)
                .add("netAmount", netAmount)
                .add("fullAmount", fullAmount)
                .add("userAccount", userAccount)
                .add("externalId", externalId)
                .add("externalUserAccount", externalUserAccount)
                .add("externalCustomer", externalCustomer)
                .add("transactionId", transactionId)
                .add("thread", thread)
                .omitNullValues().toString();
    }

}
