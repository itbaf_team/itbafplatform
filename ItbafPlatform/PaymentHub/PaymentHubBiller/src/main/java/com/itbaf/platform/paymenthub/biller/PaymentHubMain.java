/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.biller;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import static com.itbaf.platform.commons.CommonFunction.BILLER_NEW;
import static com.itbaf.platform.commons.CommonFunction.BILLER_PROCESSED;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.commons.resources.InstrumentedRunnable;
import com.itbaf.platform.paymenthub.biller.consumer.RabbitMQBillerAttemptTransactionProcessor;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import org.apache.commons.lang3.Validate;
import com.itbaf.platform.paymenthub.biller.processor.BillerMessageProcessor;
import com.itbaf.platform.paymenthub.commons.messaging.BillerMessage;
import com.itbaf.platform.paymenthub.biller.consumer.RabbitMQBillerMessageProcessorFactory;
import com.itbaf.platform.paymenthub.biller.handler.ServiceProcessorHandler;
import com.itbaf.platform.rmq.services.RabbitMQMemoryProcessorImpl;
import java.util.concurrent.TimeUnit;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain {

    private final PHInstance instance;
    private final ExecutorService cachedThreadPool;
    private final RabbitMQConsumer rabbitMQConsumer;
    private final InstrumentedObject instrumentedObject;
    private final BasicDataBaseConnection dataBaseConnection;
    private final RabbitMQMemoryProcessorImpl memoryProcessor;
    private final ScheduledExecutorService scheduledThreadPool;
    private final ServiceProcessorHandler serviceProcessorHandler;
    private final RabbitMQBillerMessageProcessorFactory rabbitMQMessageProcessorFactory;
    private final Map<BillerMessage.BillerType, BillerMessageProcessor> billerMessageProcessorMap;
    private final RabbitMQBillerAttemptTransactionProcessor rabbitMQBillerAttemptTransactionProcessor;

    private static boolean isStop = false;

    @Inject
    public PaymentHubMain(final PHInstance instance,
            final ExecutorService cachedThreadPool,
            final RabbitMQConsumer rabbitMQConsumer,
            final InstrumentedObject instrumentedObject,
            final BasicDataBaseConnection dataBaseConnection,
            final RabbitMQMemoryProcessorImpl memoryProcessor,
            final ScheduledExecutorService scheduledThreadPool,
            final ServiceProcessorHandler serviceProcessorHandler,
            final Set<BillerMessageProcessor> billerMessageProcessorSet,
            final RabbitMQBillerMessageProcessorFactory rabbitMQMessageProcessorFactory,
            final RabbitMQBillerAttemptTransactionProcessor rabbitMQBillerAttemptTransactionProcessor) {
        this.instance = Validate.notNull(instance, "A PHInstance class must be provided");
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "An ExecutorService class must be provided");
        this.rabbitMQConsumer = Validate.notNull(rabbitMQConsumer, "A RabbitMQConsumer class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.memoryProcessor = Validate.notNull(memoryProcessor, "A RabbitMQMemoryProcessorImpl class must be provided");
        this.dataBaseConnection = Validate.notNull(dataBaseConnection, "A BasicDataBaseConnection class must be provided");
        this.scheduledThreadPool = Validate.notNull(scheduledThreadPool, "A ScheduledExecutorService class must be provided");
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
        this.rabbitMQMessageProcessorFactory = Validate.notNull(rabbitMQMessageProcessorFactory, "A RabbitMQMessageProcessorFactory class must be provided");
        this.rabbitMQBillerAttemptTransactionProcessor = Validate.notNull(rabbitMQBillerAttemptTransactionProcessor, "A RabbitMQBillerAttemptTransactionProcessor class must be provided");

        billerMessageProcessorMap = new HashMap();
        if (billerMessageProcessorSet != null) {
            log.info("BillerMessageProcessor size: " + billerMessageProcessorSet.size());
            for (BillerMessageProcessor nr : billerMessageProcessorSet) {
                log.info("JF- BillerMessageProcessor item: [" + nr.getBillerType() + "] - [" + nr.getClass().getSimpleName() + "]");
                billerMessageProcessorMap.put(nr.getBillerType(), nr);
            }
        }
    }

    public void start() {
        log.info("Initializing app... PaymentHubBiller");
        try {
            rabbitMQConsumer.createFanoutChannel(CHACHE_MEMORY_UPDATE, memoryProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer '" + CHACHE_MEMORY_UPDATE + "'. " + ex, ex);
        }
        int consumerQuantity = 0;
        Set<Provider> providers = instance.getConfigurations().keySet();
        for (Provider p : providers) {
            ChannelQuantityProvider cqp = instance.getConfigurations().get(p);
            if (cqp.getBillingConsumerQuantity() > 0) {
                createConsumer(BILLER_NEW + p.getName(), cqp.getSubscriptionConsumerQuantity(),
                        billerMessageProcessorMap.get(BillerMessage.BillerType.SUBSCRIPTION));
            }
            if (cqp.getBillingConsumerQuantity() > 0) {
                createConsumer(BILLER_PROCESSED + p.getName(), cqp.getBillingConsumerQuantity(),
                        billerMessageProcessorMap.get(BillerMessage.BillerType.BILLING));
            }
            if (cqp.getBillingConsumerQuantity() > 0) {
                createConsumer(CommonFunction.BILLER_PROCESSED_ERROR + p.getName(), cqp.getBillingConsumerQuantity(),
                        billerMessageProcessorMap.get(BillerMessage.BillerType.BILLING));
            }
            consumerQuantity = consumerQuantity + cqp.getBillingConsumerQuantity();

            scheduledThreadPool.scheduleWithFixedDelay(new InstrumentedRunnable("biller.find", p.getName(), instrumentedObject) {
                @Override
                public boolean exec() {
                    if (isStop) {
                        return false;
                    }
                    Thread.currentThread().setName(CommonFunction.getTID("biller.find." + p.getName()));
                    RLock lock = instrumentedObject.lockObject("BILLER_FIND", 40);
                    long aux = instrumentedObject.getAtomicLong("BILLER_FIND_A", 1);
                    if (aux == 0) {
                        instrumentedObject.getAndIncrementAtomicLong("BILLER_FIND_A", 1);
                        try {
                            serviceProcessorHandler.findActiveBillingAttempt(p);
                            instrumentedObject.unLockObject(lock);
                            return true;
                        } catch (Exception ex) {
                        }
                    }
                    instrumentedObject.unLockObject(lock);
                    return false;
                }
            }, 30, 30, TimeUnit.SECONDS);

        }
        rabbitMQConsumer.createChannel(CommonFunction.BILLER_TRANSACTION, consumerQuantity, rabbitMQBillerAttemptTransactionProcessor);

        log.info("Initializing app OK. PaymentHubBiller");
    }

    private RabbitMQConsumer createConsumer(String exchangeName, Integer consumerQuantity, BillerMessageProcessor notificationProcessor) {
        rabbitMQConsumer.createChannel(exchangeName, consumerQuantity, createRabbitMQMessageProcessor(notificationProcessor));
        return rabbitMQConsumer;
    }

    private RabbitMQMessageProcessor createRabbitMQMessageProcessor(BillerMessageProcessor messageProcessor) {
        return rabbitMQMessageProcessorFactory.create(messageProcessor);
    }

    public void stop() {
        isStop = true;
        rabbitMQConsumer.stopConsumers();
        stopThreads();
        dataBaseConnection.shutdownConnectionsPool();
        log.info("App destroyed. PaymentHubBiller");
    }

    public void stopThreads() {
        log.info("Finalizando Threads en 5 segundos... ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }
        try {
            log.info("Finalizando scheduledThreadPool ");
            scheduledThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            log.info("Esperando Threads por 2 segundos... ");
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        try {
            scheduledThreadPool.shutdownNow();
            log.info("Finalizado scheduledThreadPool ");
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdownNow();
        } catch (Exception ex) {
        }
    }

}
