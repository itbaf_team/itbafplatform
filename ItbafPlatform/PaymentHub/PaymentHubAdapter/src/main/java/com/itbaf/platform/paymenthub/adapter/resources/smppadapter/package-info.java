/**
 * 
 * 		Smpp Adapter SOAP interface - Copyright (c) 2013 by Software Consulting. All rights reserved.
 * 		Esta interface permite el envio y recepcion de SMS
 * 	
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://ws.sms.qubit.tv/")
package com.itbaf.platform.paymenthub.adapter.resources.smppadapter;
