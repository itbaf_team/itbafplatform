/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.persistence.util.HibernateEquality;
import com.querydsl.core.annotations.Config;
import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author javier
 */
@Entity(name = "PRODUCT")
@Table(name = "PRODUCT")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class Product implements Serializable {

    private static final long serialVersionUID = 20170926173001L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "LOGO_URL")
    private String logoURL;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "PRODUCT_SOP", joinColumns = {
        @JoinColumn(name = "PRODUCT_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "SOP_ID", nullable = false, updatable = false)})
    Set<SOP> sops;

    @Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(catalog = "pg_schema", name = "PRODUCT_SETTING",
            joinColumns = @JoinColumn(name = "PRODUCT_ID"))
    @MapKeyColumn(name = "PKEY")
    @Column(name = "PVALUE")
    private Map<String, String> productSettings;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("name", name)
                .add("logoURL", logoURL)
                .add("description", description)
                .add("productSettings", productSettings)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, logoURL, description, productSettings);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<Product>()
                .over(Product::getId)
                .over(Product::getName)
                .over(Product::getLogoURL)
                .over(Product::getDescription)
                .asserts(this, obj);
    }
}
