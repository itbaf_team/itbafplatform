/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.ph;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import java.util.Map;
import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyJoinColumn;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
@Entity(name = "PH_INSTANCE")
@Table(name = "PH_INSTANCE", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class PHInstance implements Serializable {

    private static final long serialVersionUID = 20170809155408L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false, unique = true)
    private String description;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(catalog = "paymentHub", name = "PH_INSTANCE_PROVIDER",
            joinColumns = @JoinColumn(name = "PH_INSTANCE_ID"))
    @MapKeyJoinColumn(name = "PROVIDER_ID")
    private Map<Provider, ChannelQuantityProvider> configurations;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("name", name)
                .add("description", description)
                .add("configurations", configurations)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, configurations);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<PHInstance>()
                .over(PHInstance::getId)
                .over(PHInstance::getName)
                .over(PHInstance::getDescription)
                .over(PHInstance::getConfigurations)
                .asserts(this, obj);
    }
}
