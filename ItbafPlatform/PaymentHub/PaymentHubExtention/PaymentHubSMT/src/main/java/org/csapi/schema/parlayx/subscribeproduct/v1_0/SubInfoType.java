package org.csapi.schema.parlayx.subscribeproduct.v1_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for SubInfoType complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="SubInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="productID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extensionInfo" type="{http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0}NamedParameterList" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubInfoType", propOrder = {
    "productID",
    "extensionInfo"
})
public class SubInfoType {

    @XmlElement(required = true)
    protected String productID;
    protected NamedParameterList extensionInfo;

    public SubInfoType() {
    }

    public SubInfoType(String productID) {
        this.productID = productID;
    }

    /**
     * Gets the value of the productID property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getProductID() {
        return productID;
    }

    /**
     * Sets the value of the productID property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setProductID(String value) {
        this.productID = value;
    }

    /**
     * Gets the value of the extensionInfo property.
     *
     * @return possible object is {@link NamedParameterList }
     *
     */
    public NamedParameterList getExtensionInfo() {
        return extensionInfo;
    }

    /**
     * Sets the value of the extensionInfo property.
     *
     * @param value allowed object is {@link NamedParameterList }
     *
     */
    public void setExtensionInfo(NamedParameterList value) {
        this.extensionInfo = value;
    }

}
