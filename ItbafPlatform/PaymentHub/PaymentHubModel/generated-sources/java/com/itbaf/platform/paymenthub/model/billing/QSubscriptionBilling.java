package com.itbaf.platform.paymenthub.model.billing;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSubscriptionBilling is a Querydsl query type for SubscriptionBilling
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSubscriptionBilling extends EntityPathBase<SubscriptionBilling> {

    private static final long serialVersionUID = 1349569923L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSubscriptionBilling subscriptionBilling = new QSubscriptionBilling("subscriptionBilling");

    public final DateTimePath<java.util.Date> chargedDate = createDateTime("chargedDate", java.util.Date.class);

    public final StringPath currency = createString("currency");

    public final NumberPath<java.math.BigDecimal> fullAmount = createNumber("fullAmount", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<java.math.BigDecimal> netAmount = createNumber("netAmount", java.math.BigDecimal.class);

    protected com.itbaf.platform.paymenthub.model.QSOP sop;

    protected com.itbaf.platform.paymenthub.model.subscription.QSubscriptionRegistry subscriptionRegistry;

    public final StringPath transactionId = createString("transactionId");

    public QSubscriptionBilling(String variable) {
        this(SubscriptionBilling.class, forVariable(variable), INITS);
    }

    public QSubscriptionBilling(Path<? extends SubscriptionBilling> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSubscriptionBilling(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSubscriptionBilling(PathMetadata metadata, PathInits inits) {
        this(SubscriptionBilling.class, metadata, inits);
    }

    public QSubscriptionBilling(Class<? extends SubscriptionBilling> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sop = inits.isInitialized("sop") ? new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"), inits.get("sop")) : null;
        this.subscriptionRegistry = inits.isInitialized("subscriptionRegistry") ? new com.itbaf.platform.paymenthub.model.subscription.QSubscriptionRegistry(forProperty("subscriptionRegistry"), inits.get("subscriptionRegistry")) : null;
    }

    public com.itbaf.platform.paymenthub.model.QSOP sop() {
        if (sop == null) {
            sop = new com.itbaf.platform.paymenthub.model.QSOP(forProperty("sop"));
        }
        return sop;
    }

    public com.itbaf.platform.paymenthub.model.subscription.QSubscriptionRegistry subscriptionRegistry() {
        if (subscriptionRegistry == null) {
            subscriptionRegistry = new com.itbaf.platform.paymenthub.model.subscription.QSubscriptionRegistry(forProperty("subscriptionRegistry"));
        }
        return subscriptionRegistry;
    }

}

