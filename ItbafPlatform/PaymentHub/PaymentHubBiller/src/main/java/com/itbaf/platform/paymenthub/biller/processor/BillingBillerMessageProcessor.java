/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.biller.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.paymenthub.commons.messaging.BillerMessage;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.services.repository.BillerAttemptService;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import org.apache.commons.lang3.Validate;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author javier
 *
 * Precesa todos los cobros realizados por el modulo correspondiente
 */
@lombok.extern.log4j.Log4j2
public class BillingBillerMessageProcessor extends BillerMessageProcessor {

    private final ObjectMapper mapper;
    private final RabbitMQProducer rabbitMQProducer;
    private final BillerAttemptService billerAttemptService;

    @Inject
    public BillingBillerMessageProcessor(final ObjectMapper mapper,
            final RabbitMQProducer rabbitMQProducer,
            final BillerAttemptService billerAttemptService) {
        super(BillerMessage.BillerType.BILLING);
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.billerAttemptService = Validate.notNull(billerAttemptService, "A BillerAttemptService class must be provided");
    }

    @Override
    public Boolean process(String message, BillerMessage bm, SOP sop) {
        Boolean result = null;
        if (message == null || bm == null || sop == null) {
            log.error("Alguno de los parametros es nulo. [" + message + "][" + bm + "][" + sop + "]");
            return null;
        }

        try {
            switch (bm.getMessageType()) {
                case BILLING_PENDING:
                    break;
                case BILLING_TOTAL:
                case BILLING_PARTIAL:
                    billingProcess(bm, sop);
                    break;
                case SUBSCRIPTION_BLACKLIST:
                case SUBSCRIPTION_CANCELLATION:
                    subscriptionProcess(bm, sop);
                    break;
            }

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
                bm.getBillerAttempt().getExtra().stringData.put(bm.getMessageType().name(), sdf.format(bm.getBillerAttempt().getLastAttempt()));
            } catch (Exception ex) {
            }
            billerAttemptService.saveOrUpdate(bm.getBillerAttempt());
            result = true;
        } catch (ConstraintViolationException | SQLIntegrityConstraintViolationException ex) {
            log.warn("Registro duplicado. [" + message + "]. " + ex);
            result = false;
        } catch (Exception ex) {
            boolean control = true;
            try {
                if (ex.getCause() instanceof ConstraintViolationException) {
                    control = false;
                    log.warn("Registro duplicado. [" + message + "]. " + ex.getCause().getCause());
                }
            } catch (Exception exx) {
            }

            if (control) {
                log.error("Error al procesar notificacion de cobro: [" + message + "]. " + ex, ex);
            } else {
                result = false;
            }
        }

        return result;
    }

    private void subscriptionProcess(BillerMessage bm, SOP sop) throws Exception {
        PaymentHubMessage msg = new PaymentHubMessage();
        msg.setChannelOut("BILLER");
        msg.setCodeCountry(sop.getProvider().getCountry().getCode());
        msg.setExternalCustomer(bm.getExternalCustomer());
        msg.setExternalId(bm.getExternalId());
        msg.setExternalUserAccount(bm.getExternalUserAccount());
        msg.setUserAccount(bm.getUserAccount());
        msg.setToDate(bm.getBillerAttempt().getLastAttempt());
        msg.setFromDate(msg.getToDate());
        msg.setMessageType(bm.getMessageType());
        msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
        msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
        msg.setSopId(sop.getId());
        msg.setTransactionId(bm.getTransactionId());

        String jsonResponse = mapper.writeValueAsString(msg);
        rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
    }

    private void billingProcess(BillerMessage bm, SOP sop) throws Exception {
        PaymentHubMessage msg = new PaymentHubMessage();
        msg.setCodeCountry(sop.getProvider().getCountry().getCode());
        msg.setCurrencyId(sop.getIntegrationSettings().get("currency"));
        msg.setExternalCustomer(bm.getExternalCustomer());
        msg.setExternalId(bm.getExternalId());
        msg.setExternalUserAccount(bm.getExternalUserAccount());
        msg.setUserAccount(bm.getUserAccount());
        msg.setChargedDate(bm.getBillerAttempt().getLastCharged());
        msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
        msg.setFullAmount(bm.getFullAmount());
        msg.setNetAmount(bm.getNetAmount());
        msg.setMessageType(bm.getMessageType());
        msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
        msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
        msg.setSopId(sop.getId());
        msg.setTransactionId(bm.getTransactionId());

        Tariff t = sop.findTariffByFullAmount(bm.getFullAmount());
        if (t != null) {
            msg.setTariffId(t.getId());
        }

        String jsonResponse = mapper.writeValueAsString(msg);
        rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
    }
}
