/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;
import java.util.Date;

/**
 *
 * @author javier
 */
@Entity(name = "SOP")
@Table(name = "SOP", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors = true)
@lombok.extern.log4j.Log4j2
public class SOP implements Serializable {

    private static final long serialVersionUID = 20170809155404L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "STATUS", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Status status;
    @ManyToOne(optional = false)
    @JoinColumn(name = "SERVICE_MATCHER_ID")
    private ServiceMatcher serviceMatcher;
    @ManyToOne(optional = false)
    @JoinColumn(name = "OPERATOR_ID")
    private Operator operator;
    @ManyToOne(optional = false)
    @JoinColumn(name = "PROVIDER_ID")
    private Provider provider;

    @Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(catalog = "paymentHub", name = "SOP_TARIFF", joinColumns = {
        @JoinColumn(name = "SOP_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "TARIFF_ID", nullable = false, updatable = false)})
    Set<Tariff> tariffs;

    @Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(catalog = "paymentHub", name = "SOP_INTEGRATION_SETTING",
            joinColumns = @JoinColumn(name = "SOP_ID"))
    @MapKeyColumn(name = "PKEY")
    @Column(name = "PVALUE")
    private Map<String, String> integrationSettings;
    @Transient
    private String tyc;
    @Transient
    private TreeSet<Tariff> tsTariff;
    @Transient
    private Tariff mainTariff;

    public SOP() {
    }

    public SOP(Long id) {
        this.id = id;
    }

    public SOP(String codeCountry, String serviceMatcher, String operator, String provider) {
        this.serviceMatcher = new ServiceMatcher();
        this.operator = new Operator();
        this.provider = new Provider();
        this.serviceMatcher.setName(serviceMatcher);
        this.operator.setName(operator);
        this.provider.setName(provider);
        this.provider.setCountry(new Country());
        this.provider.getCountry().setCode(codeCountry);
    }

    public SOP(String serviceMatcher, String operator, String provider) {
        this.serviceMatcher = new ServiceMatcher();
        this.operator = new Operator();
        this.provider = new Provider();
        this.serviceMatcher.setName(serviceMatcher);
        this.operator.setName(operator);
        this.provider.setName(provider);
        this.provider.setCountry(new Country());
        //Default value. Table: Country
        this.provider.getCountry().setCode("XX");
    }

    @JsonIgnore
    public Tariff getMainTariff() {
        log.info("getMainTariff() 1 mainTariff: ["+mainTariff+"] tariffs ["+tariffs+"]");
        if (mainTariff == null && tariffs != null && !tariffs.isEmpty()) {
            mainTariff = getTsTariff().first();
        }
        Long current = System.currentTimeMillis();
        log.info("getMainTariff() 2 mainTariff: ["+mainTariff+"] tariffs ["+tariffs+"] current: ["+current+"]");
        if (mainTariff != null && (current < mainTariff.getFromDate().getTime() || current >= mainTariff.getToDate().getTime())) {
            mainTariff = null;
            tsTariff = null;
            mainTariff = getTsTariff().first();
        }
        return mainTariff;
    }

    @JsonIgnore
    public Tariff getMainTariff(Date date) {
        log.info("getMainTariff(Date) tariffs ["+tariffs+"]");
        if (tariffs != null) {
            for (Tariff t : tariffs) {
                if (Tariff.TariffType.MAIN.equals(t.getTariffType()) && t.getFromDate().before(date) && t.getToDate().after(date)) {
                    return t;
                }
            }
        }
        return getMainTariff();
    }

    @JsonIgnore
    public TreeSet<Tariff> getTsTariff() {
        if (tsTariff == null && tariffs != null && !tariffs.isEmpty()) {
            tsTariff = new TreeSet();
            long current = System.currentTimeMillis();
            for (Tariff t : tariffs) {
                if (current >= t.getFromDate().getTime() && current < t.getToDate().getTime()) {
                    tsTariff.add(t);
                }
            }

            if (mainTariff == null) {
                log.info("SOP. getTsTariff: tsTariff.isEmpty() " + tsTariff.isEmpty());
                mainTariff = tsTariff.first();

            }
        }
        return tsTariff;
    }

    @JsonIgnore
    public Tariff findTariffById(Long tariffId) {
        Tariff t = null;
        if (tariffId != null && tariffs != null && !tariffs.isEmpty()) {
            for (Tariff ta : tariffs) {
                if (ta.getId().equals(tariffId)) {
                    t = ta;
                    break;
                }
            }
        }
        return t;
    }

    @JsonIgnore
    public Tariff findTariffByNetAmount(BigDecimal netAmount) {
        Tariff t = null;
        if (netAmount != null && tariffs != null && !tariffs.isEmpty()) {
            Long current = System.currentTimeMillis();
            for (Tariff ta : tariffs) {
                if (ta.getNetAmount() != null && ta.getNetAmount().compareTo(netAmount) == 0
                        && (current >= ta.getFromDate().getTime() && current < ta.getToDate().getTime())) {
                    t = ta;
                    break;
                }
            }
        }
        return t;
    }

    @JsonIgnore
    public Tariff findTariffByFullAmount(BigDecimal fullAmount) {
        Tariff t = null;
        if (fullAmount != null && tariffs != null && !tariffs.isEmpty()) {
            Long current = System.currentTimeMillis();
            for (Tariff ta : tariffs) {
                if (ta.getFullAmount() != null && ta.getFullAmount().compareTo(fullAmount) == 0
                        && (current >= ta.getFromDate().getTime() && current < ta.getToDate().getTime())) {
                    t = ta;
                    break;
                }
            }
        }
        return t;
    }

    @JsonIgnore
    public Tariff findTariffByPercentage(BigDecimal percentage) {
        Tariff t = null;
        if (percentage != null && tariffs != null && !tariffs.isEmpty()) {
            Long current = System.currentTimeMillis();
            for (Tariff ta : tariffs) {
                if (ta.getPercentage() != null && ta.getPercentage().compareTo(percentage) == 0
                        && (current >= ta.getFromDate().getTime() && current < ta.getToDate().getTime())) {
                    t = ta;
                    break;
                }
            }
        }
        return t;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("status", status)
                .add("service", serviceMatcher)
                .add("operator", operator)
                .add("provider", provider)
                .add("integrationSettings", integrationSettings)
                .add("tariffs", tariffs)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status, serviceMatcher, operator, provider, tariffs, integrationSettings);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<SOP>()
                .over(SOP::getId)
                .over(SOP::getStatus)
                .over(SOP::getServiceMatcher)
                .over(SOP::getOperator)
                .over(SOP::getProvider)
                .over(SOP::getIntegrationSettings)
                .over(SOP::getTariffs)
                .asserts(this, obj);
    }
}
