import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';

import { ModalComponent } from './modal/modal.component';
import { SubscriptionService } from '../../../../@core/data/subscription.service';
import { PermissionsService } from '../../../../@core/data/permissions.service';
import { SearchComponent } from '../search.component';

@Component({
  selector: 'ngx-subscriptions-search-table',
  templateUrl: './table-search.component.html',
  styleUrls: ['./table-search.component.scss'],
})
export class TableSearchComponent {

    subscriptions: any;
    permissions: any;
    billings: any;
    disabledCancelarBtn: boolean;
    disabledBillingHistoryBtn: boolean;
    config: ToasterConfig;
    searchComponent: SearchComponent;

    constructor(private subscriptionService: SubscriptionService, 
        private permissionsService: PermissionsService, private modalService: NgbModal,
        private toasterService: ToasterService) {
      subscriptionService.setComponent(this);
      permissionsService.setComponent(this);
      this.permissionsService.getPermissions().subscribe((p: any) => this.permissions=p);
      this.disabledCancelarBtn=(this.permissions && this.permissions.api_operations_subscriptions_cancelar?false:true);
      this.disabledBillingHistoryBtn=(this.permissions && this.permissions.api_operations_subscriptions_billing_history?false:true);
    }
    
    setSearchComponent(searchComponent){
        this.searchComponent=searchComponent;
    }

    remoteSubscriptionProcess(subscriptions) {
        console.log(subscriptions);
        this.subscriptions=subscriptions;
        this.billings=undefined;
        if(subscriptions.message && subscriptions.processedCode){
            switch(subscriptions.processedCode){
                case 1000:
                    this.showToast("success", subscriptions.userAccount, subscriptions.message);
                break;
                case 1001:
                    this.showToast("error", subscriptions.userAccount, subscriptions.message);
                break;
            }
        }
        this.searchComponent.setSpinnerStatus(false);
    }
  
    remoteProcess(permissions) {
        this.permissions=permissions;
        this.disabledCancelarBtn=(this.permissions && this.permissions.api_operations_subscriptions_cancelar?false:true);
        this.disabledBillingHistoryBtn=(this.permissions && this.permissions.api_operations_subscriptions_billing_history?false:true);
    }
    
    remoteBillingHistoryProcess(billings) { 
        this.billings=billings;
        console.log("--- BILLING ---");
        console.log(this.billings);
        console.log("--- ------- ---");
        this.searchComponent.setSpinnerStatus(false);
    }
  
    clickDeleteBtn(event, s) {
        const activeModal = this.modalService.open(ModalComponent, {
            size: 'lg',
            backdrop: 'static',
            container: 'nb-layout',
        });

      activeModal.componentInstance.modalHeader = 'Cancelar Suscripcion';
      activeModal.componentInstance.modalContent = `Confirme que desea cancelar la suscripción de ` + s.userAccount + ` a ` + s.sop.serviceMatcher.name;
      activeModal.componentInstance.modalComponent = this;
      activeModal.componentInstance.modalSubscription = s;
    }

    deleteSubscription(s) {
        this.searchComponent.setSpinnerStatus(true);
        this.subscriptionService.deleteSubscription(s.subscriptionRegistry.id, s.userAccount).subscribe((s: any) => console.log(s));      
    }
    
    onClickBillingHistory(e,userAccount){
        this.searchComponent.setSpinnerStatus(true);
        this.subscriptionService.getBillingHistory(userAccount).subscribe((s: any) => console.log(s));
    }
    
    private showToast(type: string, title: string, body: string) {
        this.config = new ToasterConfig({
          positionClass: 'toast-top-right',
          timeout: 0,
          newestOnTop: true,
          tapToDismiss: true,
          preventDuplicates: false,
          animation: 'fade',
          limit: 5,
        });
        const toast: Toast = {
          type: type,
          title: title,
          body: body,
          timeout: 0,
          showCloseButton: true,
          bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toasterService.popAsync(toast);
    }
}
