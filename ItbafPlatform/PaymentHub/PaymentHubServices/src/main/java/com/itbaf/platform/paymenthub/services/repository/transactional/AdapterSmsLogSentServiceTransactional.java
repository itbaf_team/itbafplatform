/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.model.sms.AdapterSmsLogSent;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPAAdapterSmsLogSentRepository;
import com.itbaf.platform.paymenthub.services.repository.AdapterSmsLogSentService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class AdapterSmsLogSentServiceTransactional implements AdapterSmsLogSentService {

    private final JPAAdapterSmsLogSentRepository adapterSmsLogSentRepository;

    @Inject
    public AdapterSmsLogSentServiceTransactional(final JPAAdapterSmsLogSentRepository adapterSmsLogSentRepository) {
        this.adapterSmsLogSentRepository = Validate.notNull(adapterSmsLogSentRepository, "A JPAAdapterSmsLogSentRepository class must be provided");
    }

    @Override
    public void save(AdapterSmsLogSent adapterSmsLogSent) throws Exception {
        adapterSmsLogSentRepository.save(adapterSmsLogSent);
    }

}
