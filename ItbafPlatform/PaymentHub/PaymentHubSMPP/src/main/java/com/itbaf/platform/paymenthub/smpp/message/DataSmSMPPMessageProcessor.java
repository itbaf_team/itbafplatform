/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.smpp.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.paymenthub.messaging.smpp.PaymentHubSMPPMessage;
import com.itbaf.platform.paymenthub.messaging.smpp.SMPPMessageType;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class DataSmSMPPMessageProcessor implements SMPPMessageProcessor {

    private final ObjectMapper mapper;
    private final RabbitMQProducer rabbitMQProducer;
    private static final SMPPMessageType smppMessageType = SMPPMessageType.DataSm;

    @Inject
    public DataSmSMPPMessageProcessor(final ObjectMapper mapper,
            final RabbitMQProducer rabbitMQProducer) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");

    }


    public SMPPMessageType getMessageType() {
        return smppMessageType;
    }

    @Override
    public void process(PaymentHubSMPPMessage msg) {
        log.info("Procesando PaymentHubSMPPMessage [" + smppMessageType + "]");
        try {

        } catch (Exception ex) {
            log.error("Error al procesar PaymentHubSMPPMessage [" + smppMessageType + "]: [" + msg + "]. " + ex, ex);
        }
    }

    @Override
    public SMPPMessageType getSMPPMessageType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
