/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth.api.message;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class Credentials {

    public Integer hasPassword = 0;
    public Integer hasPhone;
    public Integer hasPin = 0;
    /**
     * 1. Credenciales incorrectas.............................................
     * 2. Exceso de intentos de login..........................................
     * 3. Error al validar recaptcha...........................................
     * 4. MSISDN con longitud invalida.........................................
     * 5. Longitus MSIDN del Pais no configurado en DB.........................
     * 6. Error al crear USER..................................................
     * 7. El TOKEN obtenido por FIREBASE no resuelve algun Perfil..............
     * 10. Nuevo password seteado OK...........................................
     * 11. Nuevo USER creado...................................................
     * 12. Email a verificar....................................................
     * 13. Email verificado....................................................
     */
    public Integer error;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("hasPassword", hasPassword)
                .add("hasPhone", hasPhone)
                .add("hasPin", hasPin)
                .add("error", error)
                .omitNullValues().toString();
    }
}
