package com.itbaf.platform.paymenthub.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProvider is a Querydsl query type for Provider
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProvider extends EntityPathBase<Provider> {

    private static final long serialVersionUID = -100660711L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QProvider provider = new QProvider("provider");

    protected QCountry country;

    public final StringPath description = createString("description");

    public final StringPath genericName = createString("genericName");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public QProvider(String variable) {
        this(Provider.class, forVariable(variable), INITS);
    }

    public QProvider(Path<? extends Provider> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QProvider(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QProvider(PathMetadata metadata, PathInits inits) {
        this(Provider.class, metadata, inits);
    }

    public QProvider(Class<? extends Provider> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.country = inits.isInitialized("country") ? new QCountry(forProperty("country")) : null;
    }

    public QCountry country() {
        if (country == null) {
            country = new QCountry(forProperty("country"));
        }
        return country;
    }

}

