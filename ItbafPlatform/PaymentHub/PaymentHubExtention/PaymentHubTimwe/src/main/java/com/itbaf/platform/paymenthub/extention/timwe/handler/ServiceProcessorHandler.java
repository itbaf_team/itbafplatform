/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.timwe.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.timwe.resources.TimweResources;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import java.math.BigDecimal;
import java.util.*;

import com.itbaf.platform.paymenthub.model.subscription.Extra;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.paymenthub.commons.bn.model.TimweNotification;
import com.itbaf.platform.paymenthub.extention.timwe.resources.TimweSMSRequest;
import com.itbaf.platform.paymenthub.model.subscription.Subscription.Status;
import com.itbaf.platform.paymenthub.services.SubscriptionManager;
import com.itbaf.platform.paymenthub.services.repository.SmsSopMultipleService;
import com.itbaf.platform.paymenthub.services.repository.SmsSopService;

import javax.xml.ws.Holder;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public abstract class ServiceProcessorHandler extends PreServiceProcessor {

    @Inject
    protected Set<Provider> providers;
    @Inject
    protected TimweResources timweResources;
    @Inject
    protected SmsSopMultipleService smsSopMultipleService;
    @Inject
    protected SmsSopService smsSopService;
    @Inject
    protected PHProfilePropertiesService profilePropertiesService;
    @Inject
    protected SubscriptionManager subscriptionManager;

    public final String provider;

    public ServiceProcessorHandler(final String provider) {
        this.provider = provider;
    }

    public final void processNotification(final TimweNotification notification) {

        log.info("Notificacion recibida: [" + notification + "]");

        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notification);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(CommonFunction.NOTIFICATION_RECEIVED_NAME + notification.getProvider(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notification);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(CommonFunction.NOTIFICATION_RECEIVED_NAME + notification.getProvider(), jsonResponse);
                }
            } catch (Exception ex1) {
                log.fatal("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {

        TimweNotification notification;
        try {
            notification = mapper.readValue(message, TimweNotification.class);
            Thread.currentThread().setName(notification.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }

        final SOP sop = sopService.findWithTariffBySettings(provider, "product_id", notification.getProductId().toString());

        if (sop == null) {
            log.error("No existe un SOP configurado para el product_id: [" + notification.getProductId() + "] - Notificacion: [" + notification + "]");
            return null;
        }

        final String threadName = Thread.currentThread().getName() + "-" + sop.getProvider().getName() + ".n";
        Thread.currentThread().setName(threadName);

        Subscription s = subscriptionManager.getSubscriptionByUserAccount(notification.getMsisdn(), sop);

        switch (notification.getNotificationType()) {
            case "user-optin": //Alta
                return processSubscriptionNotification(notification, sop, s, true);
            case "charge-dob": //Primer cobro o cobro ondemand
            case "user-renewed": //Cobro por renovacion de suscripcion
                if (Boolean.TRUE.equals(processSubscriptionNotification(notification, sop, s, false))) {
                    return processBillingNotification(notification, sop, s);
                }
                break;
            case "user-optout": //Baja
                return processUnsubscriptionNotification(notification, sop, s);
            case "mo": //Notificacion de MO recibido
                log.info("Notificacion de MO ignorada. [" + message + "]");
                return false;
            case "mt-dn": //Notificacion del status del MT enviado
                log.info("Notificacion de MT ignorada. [" + message + "]");
                return false;
            default:
                String aux = "Notificacion desconocida. NotificationType: [" + notification.getNotificationType() + "] - Notificacion: [" + notification + "]";
                log.error(aux);
                throw new IllegalArgumentException(aux);
        }
        return null;
    }

    protected Boolean processSubscriptionNotification(TimweNotification notification, SOP sop, Subscription s, boolean sendSMS) {
        log.info("processSubscriptionNotification [" + notification.getMsisdn() + "], sendSms "+sendSMS);
        return processSubscriptionNotification(notification, sop, s);
    }

    protected Boolean processSubscriptionNotification(TimweNotification notification, SOP sop, Subscription s) {
        PaymentHubMessage phm = new PaymentHubMessage();
        try {
            log.info("notification [" + notification + "]");
            log.info("sop [" + sop + "]");
            log.info("Subscription [" + s + "]");
            phm.setChannelIn(notification.getEntryChannel());
            phm.setCodeCountry(sop.getProvider().getCountry().getCode());
            phm.setExternalId(notification.getTransactionUUID());
            if (s == null || s.getUnsubscriptionDate() != null) {
                phm.setFromDate(new Date());
            } else {
                phm.setFromDate(s.getSubscriptionDate());
            }
            if ("true".equals(sop.getIntegrationSettings().get("trial.validate.status"))) {
                phm.setMessageType(MessageType.SUBSCRIPTION_ACTIVE);
            } else {
                phm.setMessageType(MessageType.SUBSCRIPTION_TRIAL);
            }
            phm.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            phm.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            phm.setSopId(sop.getId());
            phm.setTariffId(sop.getMainTariff().getId());
            phm.setFullAmount(sop.getMainTariff().getFullAmount());
            phm.setNetAmount(sop.getMainTariff().getNetAmount());
            phm.setTransactionId(notification.getExternal_tx_id());
            phm.setUserAccount(notification.getMsisdn());
            phm.setToDateOverride(false);

            Extra extra = new Extra();
            if (notification.getExternal_tx_id() != null) {
                extra.stringData.put("External_tx_id." + notification.getExternal_tx_id(), notification.getExternal_tx_id());
            }
            if (notification.getApikey() != null) {
                extra.stringData.put("Apikey." + notification.getApikey(), notification.getApikey());
            }
            if (notification.getAuthentication() != null) {
                extra.stringData.put("Authentication." + notification.getApikey(), notification.getAuthentication());
            }
            if (notification.getText() != null) {
                extra.stringData.put("Text." + notification.getText(), notification.getText());
            }
            if (notification.getRealm() != null) {
                extra.stringData.put("Realm." + notification.getRealm(), notification.getRealm());
            }
            if (notification.getTags() != null && !notification.getTags().isEmpty()) {
                String aux = Arrays.toString(notification.getTags().toArray());
                extra.stringData.put("Tags." + aux, aux);
            }

            String adTracking = null;
            try {
                adTracking = notification.getTrackingId();
                log.info("adProvider recibido [" + adTracking + "]");
                if (adTracking != null) {
                    Map<String, Object> map = new ObjectMapper().readValue(adTracking, Map.class);
                    phm.getTrackingParams().putAll(map);
                }
            }
            catch (Exception e) {
                log.error("No se recibio TrackingId: [" + notification.getTrackingId() + "]");
            }

            if (!extra.stringData.isEmpty()) {
                phm.setExtra(extra);
            }

            String jsonResponse = mapper.writeValueAsString(phm);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + CommonFunction.NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar TimweNotification: [" + notification + "]. " + ex, ex);
            log.error("Error al procesar TimweNotification: [" + sop + "]. " + ex, ex);
            log.error("Error al procesar TimweNotification: [" + s + "]. " + ex, ex);
        }
        return null;
    }

    protected Boolean processBillingNotification(TimweNotification notification, SOP sop, Subscription s) {

        /////////////////////////////////BORRAR/////////////////////////////////
        //   if ("user-renewed".equals(notification.getNotificationType())) {
        //Verificar el atributo: "totalCharged" y quiza compararlo con Tariff
        //       log.fatal("Notificacion de renovacion... analizar y procesar... [" + notification + "]");
        //  }
        ////////////////////////////////////////////////////////////////////////
        if (!"DELIVERED".equals(notification.getMnoDeliveryCode())) {
            log.info("No se procesa la notificacion de cobros: [" + notification + "]");
            return true;
        }
        if (notification.getPricepointId() == null) {
            log.error("Notificacion PricepointId : [" + notification + "]");
            return null;
        }

        Tariff t = null;
        for (Tariff tt : sop.getTariffs()) {
            if (tt.getExternalId().equals(provider + "." + notification.getProductId() + "." + notification.getPricepointId())) {
                t = tt;
                break;
            }
        }
        if (t == null) {
            log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
            return null;
        }

        BigDecimal fullAmount = t.getFullAmount();
        BigDecimal netAmount = t.getNetAmount();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(sop.getProvider().getCountry().getCode());
            msg.setCurrencyId(sop.getProvider().getCountry().getCurrency());
            msg.setFrequencyType(sop.getIntegrationSettings().get("frequencyType"));
            msg.setFrecuency(msg.getFrequencyType() != null ? 1 : null);
            if (s == null || s.getUnsubscriptionDate() != null) {
                msg.setFromDate(new Date());
            } else {
                msg.setFromDate(s.getSubscriptionDate());
            }
            msg.setTransactionId(notification.getTransactionUUID() + "_" + notification.getExternal_tx_id());
            msg.setChargedDate(new Date());
            msg.setFullAmount(fullAmount);
            msg.setNetAmount(netAmount);
            msg.setToDateOverride(false);
            if (Tariff.TariffType.MAIN.equals(t.getTariffType())) {
                msg.setMessageType(MessageType.BILLING_TOTAL);
            } else {
                msg.setMessageType(MessageType.BILLING_PARTIAL);
            }
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setTariffId(t.getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setSopId(sop.getId());
            //  msg.setExternalId(notification.getExternal_tx_id());

            msg.setUserAccount(notification.getMsisdn());
            msg.setRealUserAccount(true);

            Extra extra = new Extra();
            /*   if (notification.getExternal_tx_id() != null) {
                extra.stringData.put("External_tx_id." + notification.getExternal_tx_id(), notification.getExternal_tx_id());
            }*/
            if (notification.getApikey() != null) {
                extra.stringData.put("Apikey." + notification.getApikey(), notification.getApikey());
            }
            if (notification.getAuthentication() != null) {
                extra.stringData.put("Authentication." + notification.getApikey(), notification.getAuthentication());
            }
            if (notification.getText() != null) {
                extra.stringData.put("Text." + notification.getText(), notification.getText());
            }
            if (notification.getRealm() != null) {
                extra.stringData.put("Realm." + notification.getRealm(), notification.getRealm());
            }
            if (notification.getTags() != null && !notification.getTags().isEmpty()) {
                String aux = Arrays.toString(notification.getTags().toArray());
                extra.stringData.put("Tags." + aux, aux);
            }
            if (!extra.stringData.isEmpty()) {
                msg.setExtra(extra);
            }

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + CommonFunction.NOTIFICATION_BILLING, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar WauNotification: [" + notification + "]. " + ex, ex);
        }

        return null;
    }

    private Boolean processUnsubscriptionNotification(TimweNotification notification, SOP sop, Subscription s) {
        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(sop.getProvider().getCountry().getCode());
            msg.setToDate(new Date());
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setTariffId(sop.getMainTariff().getId());
            msg.setFullAmount(sop.getMainTariff().getFullAmount());
            msg.setNetAmount(sop.getMainTariff().getNetAmount());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            // msg.setExternalId(notification.getTransactionUUID());
            msg.setTransactionId(notification.getExternal_tx_id());
            //Verificar si el channel q llega es el correspondiente al canal de baja
            msg.setChannelOut(null);
            msg.setUserAccount(notification.getMsisdn());
            msg.setRealUserAccount(true);
            msg.setToDateOverride(false);

            Extra extra = new Extra();
            extra.stringData = new HashMap();
            /*   if (notification.getExternal_tx_id() != null) {
                extra.stringData.put("External_tx_id." + notification.getExternal_tx_id(), notification.getExternal_tx_id());
            }*/
            if (notification.getApikey() != null) {
                extra.stringData.put("Apikey." + notification.getApikey(), notification.getApikey());
            }
            if (notification.getAuthentication() != null) {
                extra.stringData.put("Authentication." + notification.getApikey(), notification.getAuthentication());
            }
            if (notification.getText() != null) {
                extra.stringData.put("Text." + notification.getText(), notification.getText());
            }
            if (notification.getRealm() != null) {
                extra.stringData.put("Realm." + notification.getRealm(), notification.getRealm());
            }
            if (notification.getTags() != null && !notification.getTags().isEmpty()) {
                String aux = Arrays.toString(notification.getTags().toArray());
                extra.stringData.put("Tags." + aux, aux);
            }
            if (!extra.stringData.isEmpty()) {
                msg.setExtra(extra);
            }

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
        } catch (Exception ex) {
            log.error("Error al procesar WauNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ResponseMessage sendPin(Subscription subscription) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public abstract ResponseMessage unsubscribe(Subscription subscription) throws Exception;

    public ResponseMessage validatePin(Subscription subscription, String pin) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));

        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription s) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            s.setUserAccount(userAccountNormalizer(s.getUserAccount(), s.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        switch (s.getStatus()) {
            /*case ACTIVE:
                rm.status = ResponseMessage.Status.ACTIVE;
                break;*/
            case BLACKLIST:
                rm.status = ResponseMessage.Status.BLACKLIST;
                break;
            default:
                rm.status = timweResources.subscriptionStatus(s.getUserAccount(), s.getSop(), s.getSubscriptionRegistry() != null ? s.getSubscriptionRegistry().getChannelIn() : "WEB");
                if (Status.TRIAL.equals(s.getStatus()) && ResponseMessage.Status.ACTIVE.equals(rm.status)) {
                    rm.status = ResponseMessage.Status.TRIAL;
                }

                break;
        }
        try {
            rm.data = mapper.writeValueAsString(s);
        } catch (JsonProcessingException ex) {
        }
        return rm;
    }

    protected Boolean sendSMS(TimweNotification notification, SOP sop, String textParam) {
        String sms;
        Boolean b = null;
        Boolean isMultipleSms = Boolean.FALSE;

        try {
            Holder<Subscription.Status> status = new Holder();
            /*
            isMultipleSms = smsSopMultipleService.isMultipleMessage(sop.getId());
            if(isMultipleSms){
                sms = smsSopService.getSms(sop.getId(),"renewal");
            }
            else{
                sms = sop.getIntegrationSettings().get(textParam);
            }
            b = timweResources.sendSMS(notification.getMsisdn(), sop,
                    TimweSMSRequest.Context.STATELESS, TimweSMSRequest.Priority.HIGH,
                    sms, status);

            if (b == null) {
                log.error("No fue posible enviar el MT para: [" + notification + "]");
            }*/

            if (status.value != null) {
                PaymentHubMessage msg = new PaymentHubMessage();
                try {
                    msg.setCodeCountry(sop.getProvider().getCountry().getCode());
                    msg.setToDate(new Date());

                    switch (status.value) {
                        case BLACKLIST:
                            msg.setMessageType(MessageType.SUBSCRIPTION_BLACKLIST);
                            break;
                        case CANCELLED:
                            msg.setMessageType(MessageType.SUBSCRIPTION_CANCELLATION);
                            break;
                        default:
                            log.fatal("No es posible procesar el error de envio de sms con status: [" + status.value + "]");
                            return null;
                    }

                    msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
                    msg.setTariffId(sop.getMainTariff().getId());
                    msg.setFullAmount(sop.getMainTariff().getFullAmount());
                    msg.setNetAmount(sop.getMainTariff().getNetAmount());
                    msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
                    msg.setSopId(sop.getId());
                    msg.setExternalId(notification.getTransactionUUID());
                    msg.setTransactionId(notification.getExternal_tx_id());
                    //Verificar si el channel q llega es el correspondiente al canal de baja
                    msg.setChannelOut("SMS_MT");
                    msg.setUserAccount(notification.getMsisdn());
                    msg.setRealUserAccount(true);
                    msg.setToDateOverride(false);

                    Extra extra = new Extra();
                    extra.stringData = new HashMap();
                    if (notification.getExternal_tx_id() != null) {
                        extra.stringData.put("External_tx_id." + notification.getExternal_tx_id(), notification.getExternal_tx_id());
                    }
                    if (notification.getApikey() != null) {
                        extra.stringData.put("Apikey." + notification.getApikey(), notification.getApikey());
                    }
                    if (notification.getAuthentication() != null) {
                        extra.stringData.put("Authentication." + notification.getApikey(), notification.getAuthentication());
                    }
                    if (notification.getText() != null) {
                        extra.stringData.put("Text." + notification.getText(), notification.getText());
                    }
                    if (notification.getRealm() != null) {
                        extra.stringData.put("Realm." + notification.getRealm(), notification.getRealm());
                    }
                    if (notification.getTags() != null && !notification.getTags().isEmpty()) {
                        String aux = Arrays.toString(notification.getTags().toArray());
                        extra.stringData.put("Tags." + aux, aux);
                    }
                    if (!extra.stringData.isEmpty()) {
                        msg.setExtra(extra);
                    }

                    String jsonResponse = mapper.writeValueAsString(msg);
                    if (jsonResponse != null) {
                        return rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
                    }
                } catch (Exception ex) {
                    log.error("Error al procesar baja de Timwe por envio de MT: [" + notification + "]. " + ex, ex);
                }
            }
        } catch (Exception ex) {
            log.error("Error al enviar el SMS para Timwe. [" + notification + "]. " + ex, ex);
        }

        return b;
    }

    public abstract ResponseMessage sendSMS(Subscription subscription);
}
