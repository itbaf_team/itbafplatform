/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties.attributes;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class AgeRange implements Serializable {

    private static final long serialVersionUID = 20181207172501L;

    private Integer min;
    private Integer max;

    public AgeRange() {
    }

    public AgeRange(Integer min, Integer max) {
        this.min = min;
        this.max = max;
    }
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("min", min)
                .add("max", max)
                .omitNullValues().toString();
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.min);
        hash = 53 * hash + Objects.hashCode(this.max);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AgeRange other = (AgeRange) obj;
        if (!Objects.equals(this.min, other.min)) {
            return false;
        }
        if (!Objects.equals(this.max, other.max)) {
            return false;
        }
        return true;
    }

}
