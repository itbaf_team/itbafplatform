/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.cache;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class MainCache {

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final CacheObject memory = new CacheObject(10, TimeUnit.MINUTES);

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final CacheObject memory1Hour = new CacheObject(1, TimeUnit.HOURS);

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final CacheObject memory2Hours = new CacheObject(2, TimeUnit.HOURS);

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final CacheObject memory5Hours = new CacheObject(5, TimeUnit.HOURS);

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final CacheObject memory12Hours = new CacheObject(12, TimeUnit.HOURS);

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final CacheObject memory24Hours = new CacheObject(24, TimeUnit.HOURS);

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final CacheObject memory48Hours = new CacheObject(48, TimeUnit.HOURS);

    /*Almacena temporalmente las instancias existentes en DB*/
    private static final CacheObject memory96Hours = new CacheObject(96, TimeUnit.HOURS);

    public static CacheObject memory() {
        return memory;
    }

    public static CacheObject memory1Hour() {
        return memory1Hour;
    }

    public static CacheObject memory2Hours() {
        return memory2Hours;
    }

    public static CacheObject memory5Hours() {
        return memory5Hours;
    }

    public static CacheObject memory12Hours() {
        return memory12Hours;
    }

    public static CacheObject memory24Hours() {
        return memory24Hours;
    }

    public static CacheObject memory48Hours() {
        return memory48Hours;
    }

    public static CacheObject memory96Hours() {
        return memory96Hours;
    }

    public static synchronized void clearCache() {
        memory.clear();
        memory1Hour.clear();
        memory2Hours.clear();
        memory5Hours.clear();
        memory12Hours.clear();
        memory24Hours.clear();
        memory48Hours.clear();
        memory96Hours.clear();
    }

}
