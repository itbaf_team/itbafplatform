/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties.attributes;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Status;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author JF
 */
@lombok.Setter
@lombok.Getter
public class Friend implements Serializable {

    private static final long serialVersionUID = 20181207172503L;
    
    private Long userId;
    private String nickname;
    private Date createdDate;
    private Status status;
    private String avatarURL;

    public Friend() {
        status = Status.PENDING;
        createdDate = new Date();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userId", userId)
                .add("nickname", nickname)
                .add("createdDate", createdDate)
                .add("status", status)
                .add("avatarURL", avatarURL)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.userId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Friend other = (Friend) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        return true;
    }

}
