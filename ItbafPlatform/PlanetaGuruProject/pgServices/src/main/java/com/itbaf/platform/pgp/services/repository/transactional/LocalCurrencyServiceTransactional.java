/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.pgp.model.wallet.LocalCurrency;
import com.itbaf.platform.pgp.repository.jpa.model.wallet.JPALocalCurrencyRepository;
import com.itbaf.platform.pgp.services.repository.LocalCurrencyService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
public class LocalCurrencyServiceTransactional implements LocalCurrencyService {

    private final JPALocalCurrencyRepository localCurrencyRepository;

    @Inject
    public LocalCurrencyServiceTransactional(final JPALocalCurrencyRepository localCurrencyRepository) {
        this.localCurrencyRepository = Validate.notNull(localCurrencyRepository, "A JPALocalCurrencyRepository class must be provided");
    }

    @Override
    public void saveOrUpdate(LocalCurrency localCurrency) throws Exception {
        if (localCurrency.getId() == null) {
            localCurrencyRepository.save(localCurrency);
        } else {
            localCurrencyRepository.merge(localCurrency);
        }
    }

}
