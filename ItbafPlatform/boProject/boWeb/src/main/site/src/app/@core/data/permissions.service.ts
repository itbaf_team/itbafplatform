import { of as observableOf,  Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';


@Injectable()
export class PermissionsService {

  private permissions: any;
  private components: any[];

  constructor() {
      this.permissions={test:'test'};
      this.components=[];
  }
  
  setComponent(component){
    this.components.push(component);
  }
  
  setPermissions(permissions) {
      console.log("---- PERMISSIONS ----");
      console.log(permissions);
      console.log("---- ----------- ----");
      this.permissions=permissions;
      this.components.forEach(c => {c.remoteProcess(this.permissions);});
  }

  getPermissions(): Observable<any> {
    return observableOf(this.permissions);
  }
 
}
