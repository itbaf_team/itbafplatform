/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Language;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPACountryRepository;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPALanguageRepository;
import com.itbaf.platform.paymenthub.services.repository.CountryService;
import io.jsonwebtoken.lang.Collections;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class CountryServiceTransactional implements CountryService {

    private final JPACountryRepository countryRepository;
    private final JPALanguageRepository languageRepository;

    @Inject
    public CountryServiceTransactional(final JPACountryRepository countryRepository,
            final JPALanguageRepository languageRepository) {
        this.countryRepository = Validate.notNull(countryRepository, "A JPACountryRepository class must be provided");
        this.languageRepository = Validate.notNull(languageRepository, "A JPALanguageRepository class must be provided");
    }

    @Override
    public Country findById(Long id) throws Exception {
        return countryRepository.findById(id);
    }

    @Override
    public Country findByCode(String code) {
        code = code.toUpperCase().trim();
        Object o = MainCache.memory96Hours().getIfPresent("country_code_" + code);
        if (o == null) {
            Country c = countryRepository.findByCode(code);
            if (c != null) {
                MainCache.memory96Hours().put("country_code_" + code, c);
            }
            return c;
        }

        return (Country) o;
    }

    @Override
    public List<Country> getByStatus(Status status) {
        Object o = MainCache.memory5Hours().getIfPresent("country_status_" + status);
        if (o == null) {
            List<Country> countries = countryRepository.getByStatus(status);
            if (!Collections.isEmpty(countries)) {
                MainCache.memory5Hours().put("country_status_" + status, countries);
                return countries;
            }
            return new ArrayList();
        }
        return (List<Country>) o;
    }

    @Override
    public Country findByInternationalNumberPhone(String phone) {
        return countryRepository.findByInternationalNumberPhone(phone);
    }

    @Override
    public void saveOrUpdate(Country country) throws Exception {
        if (country.getLanguages() != null) {
            for (Language l : country.getLanguages()) {
                try {
                    languageRepository.merge(l);
                } catch (Exception ex) {
                    try {
                        languageRepository.save(l);
                    } catch (Exception exx) {
                        log.error("Error al guardar Language: [" + l + "]. " + exx, exx);
                    }
                }
            }
        }

        if (country.getId() == null) {
            countryRepository.save(country);
        } else {
            countryRepository.merge(country);
        }
    }

    @Override
    public Country getCountryByCurrencyCode(String currencyCode) {
        Object obj = MainCache.memory48Hours().getIfPresent("Currency_" + currencyCode);
        if (obj == null) {
            Country c = countryRepository.getCountryByCurrencyCode(currencyCode);
            if (c != null) {
                MainCache.memory48Hours().put("Currency_" + currencyCode, c);
                return c;
            }
        }

        return (Country) obj;
    }

}
