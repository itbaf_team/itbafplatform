/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.model;

import com.google.cloud.MonitoredResource;
import com.google.cloud.logging.Operation;
import com.google.cloud.logging.Payload;
import com.google.cloud.logging.Severity;
import com.google.cloud.logging.SourceLocation;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author JF
 */
@lombok.Getter
@lombok.Setter
public class ConsoleLogEntry {

    private String logName;
    private MonitoredResource resource;
    private Long timestamp;
    private Long receiveTimestamp;
    private Severity severity;
    private String insertId;
    private ConsoleHttpRequest httpRequest;
    private Map<String, String> labels = new HashMap();
    private Operation operation;
    private String trace;
    private String spanId;
    private SourceLocation sourceLocation;
    private Payload<?> payload;
    /////////
    private String eventTime;
    private ConsoleServiceContext serviceContext;
    private String message;
    private Object context;
    
}
