/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.model.oauth.properties;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Datos guardados por el cliente.
 * @author javier
 */
@lombok.Setter
@lombok.Getter
public class ExternalDataUserProperty extends UserProperty implements Serializable {

    private static final long serialVersionUID = 20181207172402L;

    //<UserId, Object>
    private Map<String, Object> objects;

    public ExternalDataUserProperty() {
        super(Topic.EXTERNAL_DATA);
        objects = new HashMap();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("objects", objects)
                .omitNullValues().toString();
    }
}
