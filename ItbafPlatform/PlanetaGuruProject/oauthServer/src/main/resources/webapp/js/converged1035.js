var router = new Navigo(window.location.protocol + "//" + window.location.host, false);

!function () {
    router.on({'/oauth/signup/new/:tid': newAccount
    }).resolve();
}();

function newAccount(params, query) {
    params.locale = getLocaleScript('converged1035');

    $("#form-data").html("");
    var html = hiddenParameter("tid", params.tid);
    $(html).appendTo("#form-var");
    html = tmpl($("#newAccount").html(), {"params": params});
    $(html).fadeIn("slow").appendTo("#form-data");
    $("#iSignIn").attr("href", "/oauth/signin/login/" + params.tid + "?locale=" + params.locale);

    $("#PhoneNumber").intlTelInput({
        geoIpLookup: function (callback) {
            $.post("/oauth/auth/country/ip", function () {}, "json").always(function (resp) {
                respGeoIP = resp;
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);

                if (countryCode.length > 1) {
                    $("#Country").val(countryCode);
                }
            });
        },
        initialCountry: "auto",
        nationalMode: false,
        utilsScript: "https://assets.planeta.guru/js/oauth/utils.js",
        preferredCountries: pparams.codes
    });

    disabledCredentialsAction();

    params.validate = {};
    params.validate.password = false;

    $('#passwd').keyup(function () {
        $("#passwd").removeClass("has-error");
        $("#rpasswd").removeClass("has-error");
        $("#alertPwdnm").html("");
        html = checkStrength($('#passwd').val());
        $('#result').html(html.msg);
        switch (html.id) {
            case 1:
                html = tmpl($("#tooShortPwd").html(), {"params": params});
                $("#alertPwd").html(html);
                params.validate.password = false;
                break;
            case 2:
                html = tmpl($("#weakPwd").html(), {"params": params});
                $("#alertPwd").html(html);
                params.validate.password = false;
                break;
            case 3:
                $("#alertPwd").html("");
                params.validate.password = true;
                break;
            case 4:
                $("#alertPwd").html("");
                params.validate.password = true;
                break;
        }
    });

    $('#FirstName').keyup(function () {
        $("#FirstName").removeClass("has-error");
        $("#alertName").html("");
    });
    $('#LastName').keyup(function () {
        $("#LastName").removeClass("has-error");
        $("#alertName").html("");
    });
    $('#email').keyup(function () {
        $("#email").removeClass("has-error");
        $("#alertEmail").html("");
    });

    $('#email').change(function () {
        var username = $("#email").val();
        if (isEmail(username)) {
            params.codeValidate = undefined;
            params.username = username;
            params.usernameType = "EMAIL";
            params.registry = "1";
            getCredentialType(params);
        } else {
            html = tmpl($("#emailError").html(), {"params": params});
            $("#alertEmail").html(html);
            $("#email").addClass("has-error");
        }
    });

    $('#PhoneNumber').change(function () {
        var phoneCountryCode = $("#PhoneNumber").intlTelInput("getSelectedCountryData");
        var username = $("#PhoneNumber").intlTelInput("getNumber");
        if (username !== undefined && username !== "" && phoneCountryCode) {
            username = username.replace("+" + phoneCountryCode.dialCode, "");
            params.codeValidate = undefined;
            params.username = username;
            params.usernameType = "PHONE";
            params.location = phoneCountryCode.iso2;
            params.registry = "1";
            getCredentialType(params);
        }
    });
    $('#PhoneNumber').keyup(function () {
        $("#PhoneNumber").removeClass("has-error");
        $("#alertPhone").html("");
    });
    $('#rpasswd').keyup(function () {
        $("#passwd").removeClass("has-error");
        $("#rpasswd").removeClass("has-error");
        $("#alertPwdnm").html("");
    });

    $('#BirthYear').change(function () {
        $("#BirthYear").removeClass("has-error");
        $("#alertBirthdate").html("");
    });
    $('#BirthMonth').change(function () {
        $("#BirthMonth").removeClass("has-error");
        $("#alertBirthdate").html("");
    });
    $('#BirthDay').change(function () {
        $("#BirthDay").removeClass("has-error");
        $("#alertBirthdate").html("");
    });
    $('#Gender').change(function () {
        $("#Gender").removeClass("has-error");
        $("#alertGender").html("");
    });


    $("#CredentialsAction").on("click", function (e) {

        params.form = {};
        params.form.firstname = $("#FirstName").val();
        params.form.lastname = $("#LastName").val();
        params.form.email = $("#email").val();
        params.form.password = $("#passwd").val();
        params.form.rpassword = $("#rpasswd").val();
        params.form.countryCode = $("#Country option:selected").val();
        params.form.birthdate = {};
        params.form.birthdate.year = $("#BirthYear option:selected").val();
        params.form.birthdate.month = $("#BirthMonth option:selected").val();
        params.form.birthdate.day = $("#BirthDay option:selected").val();
        params.form.gender = $("#Gender option:selected").val();
        params.form.phoneCountryCode = $("#PhoneNumber").intlTelInput("getSelectedCountryData");
        params.form.phoneNumber = $("#PhoneNumber").intlTelInput("getNumber");
        params.validate.error = false;

        if (!isEmail(params.form.email) || params.validate.username === true) {
            html = tmpl($("#emailError").html(), {"params": params});
            $("#alertEmail").html(html);
            $("#email").addClass("has-error");
            e.preventDefault();
            params.validate.error = true;
        }

        if (params.form.firstname === undefined || params.form.firstname.length < 2
                || params.form.lastname === undefined || params.form.lastname.length < 2) {
            html = tmpl($("#tiir").html(), {"params": params});
            $("#alertName").html(html);
            if (params.form.firstname === undefined || params.form.firstname.length < 2) {
                $("#FirstName").addClass("has-error");
            }
            if (params.form.lastname === undefined || params.form.lastname.length < 2) {
                $("#LastName").addClass("has-error");
            }
            params.validate.error = true;
            e.preventDefault();
        }

        if (!params.validate.password || params.form.password !== params.form.rpassword) {
            html = tmpl($("#pwdnmatch").html(), {"params": params});
            $("#alertPwdnm").html(html);
            $("#passwd").addClass("has-error");
            $("#rpasswd").addClass("has-error");
            e.preventDefault();
            params.validate.error = true;
        }
        if (params.form.birthdate.year === "" || params.form.birthdate.month === "" || params.form.birthdate.day === "") {
            html = tmpl($("#tiir").html(), {"params": params});
            $("#alertBirthdate").html(html);
            if (params.form.birthdate.year === "") {
                $("#BirthYear").addClass("has-error");
            }
            if (params.form.birthdate.month === "") {
                $("#BirthMonth").addClass("has-error");
            }
            if (params.form.birthdate.day === "") {
                $("#BirthDay").addClass("has-error");
            }
            e.preventDefault();
            params.validate.error = true;
        }
        if (params.form.gender === "") {
            html = tmpl($("#tiir").html(), {"params": params});
            $("#alertGender").html(html);
            $("#Gender").addClass("has-error");
            e.preventDefault();
            params.validate.error = true;
        }

        if (params.validate.error === false) {
            params.form.birthdate = params.form.birthdate.year + "-" + params.form.birthdate.month + "-" + params.form.birthdate.day;
            var html = hiddenParameter("form_firstname", params.form.firstname) +
                    hiddenParameter("form_lastname", params.form.lastname) +
                    hiddenParameter("form_email", params.form.email) +
                    hiddenParameter("form_password", params.form.password) +
                    hiddenParameter("form_rpassword", params.form.rpassword) +
                    hiddenParameter("form_countryCode", params.form.countryCode) +
                    hiddenParameter("form_birthdate", params.form.birthdate) +
                    hiddenParameter("form_gender", params.form.gender);

            if (params.form.phoneNumber !== undefined && params.form.phoneNumber !== "" && params.form.phoneCountryCode) {
                params.form.phoneNumber = params.form.phoneNumber.replace("+" + params.form.phoneCountryCode.dialCode, "");
                params.form.phoneCountryCode = params.form.phoneCountryCode.iso2;
                html = html + hiddenParameter("form_phoneNumber", params.form.phoneNumber) +
                        hiddenParameter("form_phoneCountryCode", params.form.phoneCountryCode);
            }

            $(html).appendTo("#form-var");
            $(html).appendTo("#form-var");
            $(html).appendTo("#form-var");
            pushEvent('/oauth/signup/new/form');
            $("#createForm").submit();
        }

    });

}

function enabledCredentialsAction() {
    credentialsAction(false);
}
function disabledCredentialsAction() {
    credentialsAction(true);
}
function credentialsAction(aux) {
    document.getElementById("CredentialsAction").disabled = aux;
}

function getCredentialType(params) {
    params.form = undefined;
    $("#usernameProgress").css("display", "inline");
    $.ajax({
        type: "POST",
        url: "/oauth/auth/basic/credential/type",
        contentType: 'application/json; charset=UTF-8',
        data: JSON.stringify(params),
        success: function (data) {
            if (data.ifExistsResult === 1) {
                params.validate.username = true;
                params.validate.error = true;
                if (params.usernameType === 'EMAIL') {
                    var html = tmpl($("#emailErrorFound").html(), {"params": params});
                    $("#alertEmail").html(html);
                    $("#email").addClass("has-error");
                } else if (params.usernameType === 'PHONE') {
                    var html = tmpl($("#phoneErrorFound").html(), {"params": params});
                    $("#alertPhone").html(html);
                    $("#PhoneNumber").addClass("has-error");
                }
            } else if (data.credentials && data.credentials.error === 4) {
                var html = tmpl($("#phoneErrorFormat").html(), {"params": params});
                $("#alertPhone").html(html);
                $("#PhoneNumber").addClass("has-error");
                params.validate.error = true;
            } else {
                params.validate.username = false;
            }
        },
        error: function (data) {
            $("#usernameProgress").css("display", "inline");
            alert("Error al obtener los datos.");
        },
        dataType: "json"
    });
}

