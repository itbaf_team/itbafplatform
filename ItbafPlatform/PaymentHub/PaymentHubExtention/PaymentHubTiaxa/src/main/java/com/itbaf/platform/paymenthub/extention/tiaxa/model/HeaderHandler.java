package com.itbaf.platform.paymenthub.extention.tiaxa.model;

import java.io.ByteArrayOutputStream;
import java.util.Set;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import lombok.Getter;

/**
 *
 * @author JF
 */
@Getter
@lombok.extern.log4j.Log4j2
public class HeaderHandler implements SOAPHandler<SOAPMessageContext> {

    private final UsernameToken token;

    public HeaderHandler(final UsernameToken token) {
        this.token = token;
    }

    public UsernameToken getToken() {
        return token;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext smc) {

        Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        if (outboundProperty.booleanValue()) {
            SOAPMessage message = smc.getMessage();
            try {
                SOAPEnvelope envelope = smc.getMessage().getSOAPPart().getEnvelope();
                SOAPHeader header = envelope.getHeader();

                SOAPElement security = header.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
                SOAPElement username = usernameToken.addChildElement("Username", "wsse");
                username.addTextNode(token.getUsername());
                SOAPElement password = usernameToken.addChildElement("Password", "wsse");
                password.addTextNode(token.getPasswordDigest());
                SOAPElement nonce = usernameToken.addChildElement("Nonce", "wsse");
                nonce.addTextNode(token.getNonce());
                SOAPElement created = usernameToken.addChildElement("Created", "wsse");
                created.addTextNode(token.getCreated());
                SOAPElement requestSOAPHeader = header.addChildElement("RequestSOAPHeader", "tns", "http://www.huawei.com/schema/osg/common/v2_1");
                SOAPElement appId = requestSOAPHeader.addChildElement("AppId", "tns");
                appId.addTextNode("app_tt");
                SOAPElement transId = requestSOAPHeader.addChildElement("TransId", "tns");
                transId.addTextNode(token.getRandTransId());

                requestSOAPHeader.addChildElement("token", "tns");

                try {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    message.writeTo(out);
                    String strMsg = new String(out.toByteArray());
                    log.info("Request SOAP Message: " + strMsg);
                } catch (Exception ex) {
                }
            } catch (Exception e) {
                log.error("Error al asignar Header. " + e, e);
            }
        } else {
            try {
                SOAPMessage message = smc.getMessage();
            } catch (Exception ex) {
                log.error("Error al obtener mensaje. " + ex, ex);
            }
        }
        return outboundProperty;
    }

    @Override
    public Set getHeaders() {
        return null;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {
    }
}
