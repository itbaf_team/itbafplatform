/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.ph;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author javier
 */
@Embeddable
@lombok.Setter
@lombok.Getter
public class ChannelQuantityProvider implements Serializable {

    private static final long serialVersionUID = 20170809155455L;

    @Column(name = "SUBSCRIPTION_CONSUMER_QUANTITY")
    private int subscriptionConsumerQuantity;
    @Column(name = "BILLING_CONSUMER_QUANTITY")
    private int billingConsumerQuantity;
    @Column(name = "ONDEMAND_BILLING_CONSUMER_QUANTITY")
    private int onDemandBillingConsumerQuantity;
    @Column(name = "SMPP_CONSUMER_QUANTITY")
    private int smppConsumerQuantity;
    @Column(name = "SYNC_CONSUMER_ENABLED")
    private boolean syncConsumer;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("subscriptionConsumerQuantity", subscriptionConsumerQuantity)
                .add("billingConsumerQuantity", billingConsumerQuantity)
                .add("onDemandBillingConsumerQuantity", onDemandBillingConsumerQuantity)
                .add("smppConsumerQuantity", smppConsumerQuantity)
                .add("isSyncConsumer", syncConsumer)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.subscriptionConsumerQuantity;
        hash = 59 * hash + this.billingConsumerQuantity;
        hash = 59 * hash + this.onDemandBillingConsumerQuantity;
        hash = 59 * hash + this.smppConsumerQuantity;
        hash = 59 * hash + (this.syncConsumer ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChannelQuantityProvider other = (ChannelQuantityProvider) obj;
        if (this.subscriptionConsumerQuantity != other.subscriptionConsumerQuantity) {
            return false;
        }
        if (this.billingConsumerQuantity != other.billingConsumerQuantity) {
            return false;
        }
        if (this.onDemandBillingConsumerQuantity != other.onDemandBillingConsumerQuantity) {
            return false;
        }
        if (this.smppConsumerQuantity != other.smppConsumerQuantity) {
            return false;
        }
        if (this.syncConsumer != other.syncConsumer) {
            return false;
        }
        return true;
    }

}
