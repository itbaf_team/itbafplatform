/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons;

import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.commons.sync.ServiceProcessor;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.persistence.connection.DataBaseConnection;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import static com.itbaf.platform.commons.CommonFunction.BORDER_NOTIFICATION;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import com.itbaf.platform.paymenthub.commons.bn.BNProcessor;
import com.itbaf.platform.rmq.services.RabbitMQMemoryProcessorImpl;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public abstract class PaymentHubBasic {

    @Inject
    protected Set<Provider> providerSet;
    @Inject
    private ServiceProcessor serviceProcessor;
    @Inject
    protected BNProcessor bnProcessor;
    @Inject
    protected ExecutorService cachedThreadPool;
    @Inject
    protected RabbitMQProducer rabbitMQProducer;
    @Inject
    protected RabbitMQConsumer rabbitMQConsumer;
    @Inject
    protected DataBaseConnection dataBaseConnection;
    @Inject
    protected InstrumentedObject instrumentedObject;
    @Inject
    protected RabbitMQMemoryProcessorImpl memoryProcessor;
    @Inject
    protected ScheduledExecutorService scheduledThreadPool;
    @Inject
    protected RabbitMQMessageProcessor rabbitMQMessageProcessor;

    protected Integer consumerQuantity = 0;
    protected volatile static boolean isStop = false;
    //Se crea una cola de BN para cada pais del mismo proveedor
    protected Boolean isBNProcessor = true;

    public final void start() {
        log.info("Initializing app... ");
        startActions();
        log.info("Initializing app OK. ");
    }

    protected void startActions() {
        try {
            rabbitMQConsumer.createFanoutChannel(CHACHE_MEMORY_UPDATE, memoryProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer '" + CHACHE_MEMORY_UPDATE + "'. " + ex, ex);
        }

        try {
            for (Provider p : providerSet) {
                try {
                    rabbitMQConsumer.createChannel("sync.subscriptions." + p.getName(), 1, rabbitMQMessageProcessor);
                    rabbitMQConsumer.createChannel("sync.subscription." + p.getName(), 1, rabbitMQMessageProcessor);
                } catch (Exception ex) {
                    log.error("Error al crear consumer SYNC para provider: [" + p + "]. " + ex, ex);
                }

                try {
                    String name = NOTIFICATION_RECEIVED_NAME + p.getName();
                    rabbitMQConsumer.createChannel(name, serviceProcessor.getChannelQuantityProvider(p).getSubscriptionConsumerQuantity(), serviceProcessor);
                } catch (Exception ex) {
                    log.error("Error al crear consumer NOTIFICATION.RECEIVE para provider: [" + p + "]. " + ex, ex);
                }

                try {
                    int q = serviceProcessor.getChannelQuantityProvider(p).getSubscriptionConsumerQuantity();
                    if (isBNProcessor) {
                        String name = BORDER_NOTIFICATION + p.getName();
                        rabbitMQConsumer.createChannel(name, q, bnProcessor);
                    }
                    consumerQuantity = consumerQuantity + q;
                } catch (Exception ex) {
                    log.error("Error al crear consumer BORDER_NOTIFICATION para provider: [" + p + "]. " + ex, ex);
                }
            }
        } catch (Exception ex) {
        }
    }

    public void stop() {
        if (rabbitMQConsumer != null) {
            rabbitMQConsumer.stopConsumers();
        }
        if (rabbitMQProducer != null) {
            rabbitMQProducer.stopProducers();
        }
        stopThreads();
        dataBaseConnection.shutdownConnectionsPool();
        log.info("App destroyed. ");
    }

    protected final void stopThreads() {
        isStop = true;
        log.info("Finalizando Threads en 5 segundos... ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }
        try {
            log.info("Finalizando scheduledThreadPool ");
            scheduledThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            log.info("Esperando Threads por 2 segundos... ");
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        try {
            scheduledThreadPool.shutdownNow();
            log.info("Finalizado scheduledThreadPool ");
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdownNow();
        } catch (Exception ex) {
        }
    }
}
