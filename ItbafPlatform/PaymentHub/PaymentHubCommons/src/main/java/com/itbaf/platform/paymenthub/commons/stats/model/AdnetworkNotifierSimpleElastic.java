/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.stats.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.SOP;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author JF
 */
public class AdnetworkNotifierSimpleElastic extends ElasticData implements Serializable {

    private static final long serialVersionUID = 20181029094901L;

    public AdnetworkNotifierSimpleElastic() {
        super(ElasticData.ElasticType.adnetwork_notifier_simple);
    }
    public String transactionId;
    public Date subscriptionDate;
    public SOP sop;
    public Object adnetworkTracking;
    public Boolean notificationSend;
    public String response;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("transactionId", transactionId)
                .add("subscriptionDate", subscriptionDate)
                .add("sop", sop)
                .add("adnetworkTracking", adnetworkTracking)
                .add("notificationSend", notificationSend)
                .add("response", response)
                .omitNullValues().toString();
    }
}
