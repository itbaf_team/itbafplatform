/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.filter;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.model.LogMessage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jordonez
 */
@Singleton
@lombok.extern.log4j.Log4j2
public class RequestFilter implements Filter {

    @Inject
    public RequestFilter() {
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {
        try {
            Long start = System.currentTimeMillis();
            HttpServletRequest req = ((HttpServletRequest) request);

            String uri = req.getRequestURI();
            if (req.getMethod().toLowerCase().equals("get") && "/".equals(uri)) {
                HttpServletResponse resp = ((HttpServletResponse) response);
                resp.setStatus(200);
                return;
            }

            String method = req.getMethod();
            String ip = null;
            String tid = CommonFunction.getTID();
            Thread.currentThread().setName(tid);
            String referer = "-";
            String queryString = null;
            String userAgent = req.getHeader("User-Agent");
            String contentType = null;

            try {
                ip = req.getHeader("X-Forwarded-For");
                if (ip == null || ip.length() < 1) {
                    ip = req.getRemoteAddr();
                }

                try {
                    referer = req.getHeader("referer");
                } catch (Exception ex) {
                }
                try {
                    contentType = req.getHeader("Content-Type");
                } catch (Exception ex) {
                }

                try {
                    queryString = req.getQueryString();
                } catch (Exception ex) {
                }
                if (uri.contains("/infra/test")) {
                    /*  log.debug(new LogMessage("Request. IP: [" + ip + "] - Method: [" + method + "]"
                        + (referer == null ? "" : "- referer: [" + referer + "] ")
                        + "- URI: [" + uri + "] "
                        + (queryString == null ? "" : "- queryString: [" + queryString + "] "), req, null, null));*/
                } else {
                    log.info(new LogMessage("Request. IP: [" + ip + "] - Method: [" + method + "] " + "- UserAgent: [" + userAgent + "] " + "- Content-Type: [" + contentType + "] "
                            + (referer == null ? "" : "- referer: [" + referer + "] ")
                            + "- URI: [" + uri + "] "
                            + (queryString == null ? "" : "- queryString: [" + queryString + "] "), req, null, null));
                }
            } catch (Exception ex) {
                log.error("RequestFilter error. " + ex, ex);
            }

            filterChain.doFilter(request, response);

            HttpServletResponse resp = ((HttpServletResponse) response);
            if (uri.contains("/infra/test")) {
                /* log.debug(new LogMessage("Response [" + resp.getStatus() + "]. IP: [" + ip + "] "
                    + (referer == null ? "" : "- referer: [" + referer + "] ")
                    + "- URI: [" + uri + "] "
                    + (queryString == null ? "" : "- queryString: [" + queryString + "] "), req, resp, System.currentTimeMillis() - start));*/
            } else {
                String respStatus = resp.getStatus() + "";
                if ((respStatus).startsWith("2") || (respStatus).startsWith("3")) {
                    log.info(new LogMessage("Response [" + resp.getStatus() + "]. IP: [" + ip + "] - Time: [" + (System.currentTimeMillis() - start) + "] "
                            + (referer == null ? "" : "- referer: [" + referer + "] ")
                            + "- URI: [" + uri + "] "
                            + (queryString == null ? "" : "- queryString: [" + queryString + "]"),
                            req, resp, System.currentTimeMillis() - start));
                } else {
                    log.warn(new LogMessage("Response [" + resp.getStatus() + "]. IP: [" + ip + "] - Time: [" + (System.currentTimeMillis() - start) + "] "
                            + (referer == null ? "" : "- referer: [" + referer + "] ")
                            + "- URI: [" + uri + "] "
                            + (queryString == null ? "" : "- queryString: [" + queryString + "]"),
                            req, resp, System.currentTimeMillis() - start));
                    if ((respStatus).startsWith("4")) {
                        String body = "";
                        try {
                            BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
                            String aux;
                            while ((aux = br.readLine()) != null) {
                                body = body + aux;
                            }
                            log.warn("Response [" + resp.getStatus() + "]. Request-Body: [" + body + "]");
                        } catch (Exception ex) {
                            //log.error("Response [" + resp.getStatus() + "]. No fue posible leer el Request-Body.");
                        }

                    }
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar request. " + ex, ex);
            HttpServletResponse resp = ((HttpServletResponse) response);
            resp.setStatus(500);
        }
    }

    @Override
    public void destroy() {
    }

}
