/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.tigo.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.resources.InstrumentedRunnable;
import com.itbaf.platform.paymenthub.extention.tigo.model.TigoNotification;
import com.itbaf.platform.paymenthub.extention.tigo.resources.TigoResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final ExecutorService cachedThreadPool;
    private final PHProfilePropertiesService profilePropertiesService;
    private final Map<String, Provider> providerMap;
    private final TigoResources tigoResources;
    private final String profile;

    @Inject
    public ServiceProcessorHandler(@Named("guice.profile") String profile,
            final Map<String, Provider> providerMap,
            final ObjectMapper mapper,
            final ExecutorService cachedThreadPool, final RequestClient requestClient,
            final PHProfilePropertiesService profilePropertiesService,
            final SubscriptionRegistryService subscriptionRegistryService,
            final TigoResources tigoResources) {
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "An ExecutorService class must be provided");
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.tigoResources = Validate.notNull(tigoResources, "A TigoResources class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
        this.providerMap = Validate.notNull(providerMap, "A Map<String, Provider> class must be provided");
        this.profile = profile;
    }

    public void processNotification(String codeCountry, TigoNotification notification) {

        if (codeCountry == null || codeCountry.length() != 2) {
            log.error("El Codigo de pais no es valido. CodeCountry: [" + codeCountry + "]");
            return;
        }
        Provider provider = providerMap.get(codeCountry.toUpperCase());
        if (provider == null) {
            log.error("No hay configurado un provider para CodeCountry: [" + codeCountry + "]");
            return;
        }

        try {
            final String threadName = Thread.currentThread().getName() + "-" + provider.getName();
            cachedThreadPool.execute(new InstrumentedRunnable(notification.type.toString(), provider.getName(), instrumentedObject) {
                @Override
                public boolean exec() {
                    try {
                        Thread.currentThread().setName(threadName);
                        processNotification(notification, provider);
                        return true;
                    } catch (Exception ex) {
                        log.error("Error al procesar notificacion: [" + notification + "]. " + ex, ex);
                    }
                    return false;
                }
            });
        } catch (Exception ex) {
            log.error("Error al crear Thread para procesar notificacion Tigo. codeCountry: [" + codeCountry + "] - Notification: [" + notification + "]. " + ex, ex);
        }
    }

    private void processNotification(TigoNotification notification, Provider provider) throws Exception {

        notification.setAddress(userAccountNormalizer(notification.getAddress(), provider));

        switch (notification.type) {
            case subscription:
               // log.info("Notificacion de alta. MSISDN: [" + notification.getAddress() + "] - ServiceCode: [" + notification.getServiceCode() + "] - ShortCode: [" + notification.getGroupCode() + "]");
                processSubscriptionNotification(notification, provider);
                break;
            case unsubscription:
               // log.info("Notificacion de baja. MSISDN: [" + notification.getAddress() + "] - ServiceCode: [" + notification.getServiceCode() + "] - ShortCode: [" + notification.getGroupCode() + "]");
                processUnsubscriptionNotification(notification, provider);
                break;
        }
    }

    private void processSubscriptionNotification(TigoNotification notification, Provider provider) {

        Map<String, String> settings = new HashMap();
        settings.put("serviceCode", notification.getServiceCode());
        settings.put("groupCode", notification.getGroupCode());
        SOP sop = sopService.findWithTariffBySettings(provider.getName(), settings);
        if (sop == null) {
            log.error("No existe un SOP configurado para el settings: [" + Arrays.toString(settings.entrySet().toArray()) + "] - Provider: [" + provider.getName() + "]");
            return;
        }

        Tariff t = sop.getMainTariff();

        if (t == null) {
            log.error("No hay una tarifa principal configurada para: [" + sop + "]");
            return;
        }

        PaymentHubMessage msg = new PaymentHubMessage();

        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(t.getCurrency());
            msg.setFrecuency(t.getFrecuency());
            msg.setFrequencyType(t.getFrecuencyType());
            msg.setFromDate(notification.getReceivedDateTime());
            msg.setFullAmount(t.getFullAmount());
            msg.setMessageType(MessageType.SUBSCRIPTION_ACTIVE);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setNetAmount(t.getFullAmount());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(null);
            msg.setTransactionId(notification.getReceivedDateTime().getTime() + "");
            msg.setUserAccount(notification.getAddress());
            msg.setChannelIn(notification.getChannel());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }

        } catch (Exception ex) {
            log.error("Error al procesar TigoNotification: [" + notification + "]. " + ex, ex);
        }
    }

    private void processUnsubscriptionNotification(TigoNotification notification, Provider provider) {
        Map<String, String> settings = new HashMap();
        settings.put("serviceCode", notification.getServiceCode());
        settings.put("groupCode", notification.getGroupCode());
        SOP sop = sopService.findWithTariffBySettings(provider.getName(), settings);
        if (sop == null) {
            log.error("No existe un SOP configurado para el settings: [" + Arrays.toString(settings.entrySet().toArray()) + "] - Provider: [" + provider.getName() + "]");
            return;
        }

        Tariff t = sop.getMainTariff();

        if (t == null) {
            log.error("No hay una tarifa principal configurada para: [" + sop + "]");
            return;
        }

        PaymentHubMessage msg = new PaymentHubMessage();

        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(t.getCurrency());
            msg.setFrecuency(t.getFrecuency());
            msg.setFrequencyType(t.getFrecuencyType());
            msg.setFromDate(null);
            msg.setToDate(notification.getReceivedDateTime());
            msg.setFullAmount(t.getFullAmount());
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setNetAmount(t.getFullAmount());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setExternalId(null);
            msg.setTransactionId(notification.getReceivedDateTime().getTime() + "");
            msg.setUserAccount(notification.getAddress());
            msg.setChannelOut(notification.getChannel());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }

        } catch (Exception ex) {
            log.error("Error al procesar TigoNotification: [" + notification + "]. " + ex, ex);
        }
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        /*   if (subscription.getSubscriptionRegistry().getExternalId() != null && subscription.getSubscriptionRegistry().getExternalId().length() > 2) {
            ValidateSubscription vs = npayResources.validateSubscriptionByExternalId(subscription.getSubscriptionRegistry().getExternalId(), subscription.getSop());
            if ("404".equals(vs.getMeta().code) && "Not Found".equals(vs.getMeta().status)) {
                subscription.setStatus(Subscription.Status.REMOVED);
                rm.status = ResponseMessage.Status.OK_NOT_ACTIVE;
                rm.message = vs.getMeta().description;
            } else if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                rm.message = vs.getMeta().description;
                switch (vs.getData().status) {
                    case pending:
                        rm.status = ResponseMessage.Status.OK_PENDING;
                        subscription.setStatus(Subscription.Status.PENDING);
                        break;
                    case active:
                    case pending_renewal:
                    case successful_renewal:
                        rm.status = ResponseMessage.Status.OK_ACTIVE;
                        subscription.setStatus(Subscription.Status.ACTIVE);
                        break;
                    case created:
                    case canceled:
                    case canceling:
                        rm.status = ResponseMessage.Status.OK_NOT_ACTIVE;
                        subscription.setStatus(Subscription.Status.REMOVED);
                        break;
                }
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(Long.parseLong(vs.getData().created + "000"));
                subscription.setSubscriptionDate(c.getTime());
                subscription.getSubscriptionRegistry().setSubscriptionDate(c.getTime());
                subscription.getSubscriptionRegistry().setExternalId(vs.getData().id_subscription);
            } else {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = vs.getMeta().description;
            }
        } else if (subscription.getUserAccount() != null && subscription.getUserAccount().length() > 2) {
            ValidateSubscription vs = npayResources.validateSubscriptionByMsisdn(subscription.getUserAccount(), subscription.getSop());
            
            if ("404".equals(vs.getMeta().code) && "Not Found".equals(vs.getMeta().status)) {
                subscription.setStatus(Subscription.Status.REMOVED);
                rm.status = ResponseMessage.Status.OK_NOT_ACTIVE;
                rm.message = vs.getMeta().description;
            } else if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                rm.message = vs.getMeta().description;
                
                if (vs.getData().active_subscriptions > 0) {
                    rm.status = ResponseMessage.Status.OK_ACTIVE;
                    SubscriptionData subs = null;
                    for (SubscriptionData s : vs.getData().data.get(0).subscriptions) {
                        if (ValidateSubscription.ValidateSubscriptionStatus.active.equals(s.status)) {
                            subs = s;
                        }
                    }
                    if (subs != null) {
                        Calendar c = Calendar.getInstance();
                        c.setTimeInMillis(Long.parseLong(subs.created + "000"));
                        subscription.setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setExternalId(subs.id_subscription);
                    }
                    
                } else {
                    SubscriptionData subs = null;
                    if (vs.getData().data != null && !vs.getData().data.isEmpty() && vs.getData().data.get(0).subscriptions != null) {
                        for (SubscriptionData s : vs.getData().data.get(0).subscriptions) {
                            if (ValidateSubscription.ValidateSubscriptionStatus.pending.equals(s.status)) {
                                subs = s;
                                break;
                            }
                        }
                    }
                    if (subs != null) {
                        rm.status = ResponseMessage.Status.OK_PENDING;
                        Calendar c = Calendar.getInstance();
                        c.setTimeInMillis(Long.parseLong(subs.created + "000"));
                        subscription.setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setSubscriptionDate(c.getTime());
                        subscription.getSubscriptionRegistry().setExternalId(subs.id_subscription);
                    } else {
                        rm.status = ResponseMessage.Status.OK_NOT_ACTIVE;
                    }
                }
            } else {
                rm.status = ResponseMessage.Status.ERROR;
                rm.message = vs.getMeta().description;
            }
        } else {
            rm.message = "No es posible evaluar el estado del ani";
            try {
                rm.data = mapper.writeValueAsString(subscription);
            } catch (JsonProcessingException ex1) {
            }
            return rm;
        }
        
        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex1) {
        }*/
        return rm;
    }

    public ResponseMessage sendPin(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        return rm;
    }

    public ResponseMessage validatePin(Subscription subscription, String pin) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        return rm;
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    @Override
    public Boolean processMessage(String message) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
