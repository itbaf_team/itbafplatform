package com.itbaf.platform.paymenthub.extention.store.model;

public enum ResponseCodeReport {

    BillingAccepted("000", "Billing Request Accepted", "The authorization has been granted."),
    UnknownCSP("001", "Unknown CSP", "CSP is not authenticated."),
    DisabledCSP("002", "Disabled CSP","CSP has not enogh authorization"),
    UnknownCustomer("003", "Unknown Customer", "It is not TP Customer."),
    UnknownService("004","Unknown Service", "Service_id request is not related to any service of CSP catalogue."),
    UserDisabled("005", "User disabled", "User has not subscribed the purchasing service (It is not applicable in this release)."),
    CreditNotEnough("006","Credit not enough", "Customer has not enough credit to perform the purchase."),
    SystemNoAvailable("007","System no available", "The system is not available"),
    UnabledTariffCosts("009", "Unable to calculate the tariff costs", "System could not calculate the tariff costs for this service"),
    ServiceNotAllowed("010", "Service not allowed","The number of destination customers is greater than the maximun allowed number."),
    MandatoryCSP("016", "Mandatory CSP","CSP field is empty"),
    MandatoryUser("017", "Mandatory CSP user", "USER field is empty"),
    MandatoryPassword("018", "Mandatory CSP password", "PASSWORD field is empty"),
    MandatoryDestination("019", "Mandatory destination", "Destination user field empty"),
    MandatoryMSISDN("020","Mandatory Msisdn", "MSISDN field is empty"),
    MandatoryServiceId("021", "Mandatory Service_id", "service_id  field empty"),
    MandatoryServiceClass("022","Mandatory Service_class", "service_class field is empty"),
    MandatoryEventId("023","Mandatory Event_id", "Event_id field is empty"),
    WrongRequest("024","Wrong request", "The request performed is wrong"),
    UnknownFunction("025","Unknown function", "The function requested is not in the system available list."),
    ProblemOPSC("028","Problem with OPSC system", "OPSC and MCSS are disconnected"),
    CardNotEnabledVASCANONE("029","Card not enabled to use the service VAS CANONE", "The card is not availabel to the Vas Canone service requested"),
    BlockedUser("049","Blocked user", "User is in blocked state"),
    UserNotEnabledService("055","user not enabled to service", "User type (TOU) is not enabled to screening logic"),
    UserNotCertified("056","Service not enabled for not certified users", "Service is not enabled for user with not certified identify"),
    UserInPrePreactivation("057","Service not enabled for users in state of pre-preactivation", "Service not enabled for users in state of pre-preactivation."),
    PrepaiduserNotEnabledNetwork("058","Prepaid user not enabled on network", "User is in a network state not enabled to use service"),
    BillingNotAllowed("068", "Billing not allowed", "Billing request not allowed."),
    CommandNotAllowedPinServices("106", "Command not Allowed for Pin Services", "Services configured for PIN Billing do not allow Check and Bill command."),
    CobroParcial("201", "Pedido Aceptado para Cobro Parcial", "Service charged with Tarifa Hija."),
    AparcadoParaReintento("202", "Pedido Aparcado para reintento", "Remappping for 003 response codes (User Unknown) when rebilling is configured."),
    OperacionNoPermitida("301", "Operación no Permitida. Nivel de Servicio Superado", "Maximum number of parallel request allowed for to the CSP is exceeded.");

    private String code;
    private String message;
    private String description;

    ResponseCodeReport(String code, String message, String description) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return this.description;
    }

    public static ResponseCodeReport fromCode(String code) {
        for (ResponseCodeReport item : ResponseCodeReport.values()) {
            if (item.getCode().equals(code)) {
                return item;
            }
        }

        throw new IllegalArgumentException(String.format("unrecognized code: %s", code));
    }
}
