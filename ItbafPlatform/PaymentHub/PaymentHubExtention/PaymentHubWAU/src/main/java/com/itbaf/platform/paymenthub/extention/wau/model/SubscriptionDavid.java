package com.itbaf.platform.paymenthub.extention.wau.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "request_data")
public class SubscriptionDavid {

    @JacksonXmlElementWrapper
    private String integrator_id;

    @JacksonXmlElementWrapper
    private String access_token;

    @JacksonXmlElementWrapper
    private String page_size;

    @JacksonXmlElementWrapper
    private String current_page;

    @JacksonXmlElementWrapper
    private String product_id;

    public SubscriptionDavid() {
    }

    public SubscriptionDavid(String integrator_id, String access_token, String page_size, String current_page, String product_id) {
        this.integrator_id = integrator_id;
        this.access_token = access_token;
        this.page_size = page_size;
        this.current_page = current_page;
        this.product_id = product_id;
    }

    public String getIntegrator_id() {
        return integrator_id;
    }

    public void setIntegrator_id(String integrator_id) {
        this.integrator_id = integrator_id;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getPage_size() {
        return page_size;
    }

    public void setPage_size(String page_size) {
        this.page_size = page_size;
    }

    public String getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(String current_page) {
        this.current_page = current_page;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

}
