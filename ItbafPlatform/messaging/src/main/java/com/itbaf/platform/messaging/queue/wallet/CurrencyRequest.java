/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.messaging.queue.wallet;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author javier
 */
public class CurrencyRequest implements Serializable {

    private static final long serialVersionUID = 20170905112101L;

    enum Source {
        SUBSCRIPTION, ON_DEMAND, PG_PROMO, CLIENT_PROMO
    }

    public String userAccount;
    public Long sopId;
    public Long countryId;
    public Long tariffId;
    public String currency;
    public BigDecimal amount;
    public Date date;
    public String transactionId;
    public String localCurrency;
    public BigDecimal localAmount;
    public Object tracking;
    public String description;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("userAccount", userAccount)
                .add("sopId", sopId)
                .add("countryId", countryId)
                .add("tariffId", tariffId)
                .add("currency", currency)
                .add("amount", amount)
                .add("localCurrency", localCurrency)
                .add("localAmount", localAmount)
                .add("date", date)
                .add("transactionId", transactionId)
                .add("tracking", tracking)
                .add("description", description)
                .omitNullValues().toString();
    }

}
