package com.itbaf.platform.paymenthub.extention.timwe.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class TimweResources {

    private final ObjectMapper mapper;
    private final RequestClient requestClient;
    private final PHProfilePropertiesService profilePropertiesService;

    private static final String EXTERNAL_SERVICE_URL_SMS_MT = "external.service.sms.url";
    private static final String EXTERNAL_SERVICE_URL_SUBSCRIPTION_STATUS = "external.service.subscription.status.url";

    @Inject
    public TimweResources(final ObjectMapper mapper,
            final RequestClient requestClient,
            final PHProfilePropertiesService profilePropertiesService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    public Boolean sendSMS(String msisdn, SOP sop, TimweSMSRequest.Context context, TimweSMSRequest.Priority priority, String message, Holder<Subscription.Status> status) {
        String response = null;
        try {
            String url = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), EXTERNAL_SERVICE_URL_SMS_MT) + sop.getIntegrationSettings().get("partner_role");
            String code = sop.getProvider().getCountry().getCode().toLowerCase();
            TimweSMSRequest request = new TimweSMSRequest();
            request.context = context;
            request.largeAccount = sop.getIntegrationSettings().get("shortcode");
            request.mcc = profilePropertiesService.getCommonProperty("mobile.country.code." + code + ".mcc");
            request.mnc = profilePropertiesService.getCommonProperty("mobile.network.code." + code + ".mnc");
            request.msisdn = msisdn;
            request.pricepointId = Long.parseLong(sop.getIntegrationSettings().get("sms.message.mt.pricepointId"));
            request.priority = priority;
            request.productId = Long.parseLong(sop.getIntegrationSettings().get("product_id"));
            request.text = message;
            String json = mapper.writeValueAsString(request);

            Map<String, String> headers = new HashMap();
            headers.put("apikey", sop.getIntegrationSettings().get("apikey_mt"));
            headers.put("authentication", getAuthentication(sop));
            //headers.put("external-tx-id", CommonFunction.getTID());

            response = requestClient.requestJSONPost(url, json, headers);

            log.info("Request MSM timwe " + " msisdn " +msisdn + " Request " +response);

            if (response != null) {
                TimweResponse tr = mapper.readValue(response, TimweResponse.class);

                switch (tr.code) {
                    case "SUCCESS":
                        return true;
                    case "BLACKLISTED_USER":
                        status.value = Subscription.Status.BLACKLIST;
                        return true;
                    case "INVALID_SUB_MT":
                        status.value = Subscription.Status.CANCELLED;
                        return true;
                }

            }
        } catch (Exception ex) {
            log.error("No fue posible realizar el envio del SMS. [" + sop.getId() + "][" + response + "]. " + ex, ex);
        }

        return null;
    }

    public ResponseMessage.Status subscriptionStatus(String msisdn, SOP sop, String channel) {
        ResponseMessage.Status status = ResponseMessage.Status.REMOVED;
        String response = null;
        try {
            String url = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), EXTERNAL_SERVICE_URL_SUBSCRIPTION_STATUS) + sop.getIntegrationSettings().get("partner_role");
            String code = sop.getProvider().getCountry().getCode().toLowerCase();
            TimweStatusRequest request = new TimweStatusRequest();
            request.mcc = profilePropertiesService.getCommonProperty("mobile.country.code." + code + ".mcc");
            request.mnc = profilePropertiesService.getCommonProperty("mobile.network.code." + code + ".mnc");
            request.userIdentifier = msisdn;
            request.productId = Long.parseLong(sop.getIntegrationSettings().get("product_id"));
            request.userIdentifierType = "MSISDN";
            request.entryChannel = (channel == null ? "WEB" : channel.toUpperCase().trim());
            String json = mapper.writeValueAsString(request);

            Map<String, String> headers = new HashMap();
            headers.put("apikey", sop.getIntegrationSettings().get("apikey_status"));
            headers.put("authentication", getAuthentication(sop));
            //headers.put("external-tx-id", CommonFunction.getTID());
            headers.put("external-tx-id", msisdn + "#" + request.productId + "#" + new Date().getTime());

            response = requestClient.requestJSONPost(url, json, headers);
            if (response != null) {
                TimweResponse tr = mapper.readValue(response, TimweResponse.class);
                if (tr.responseData != null) {
                    if ("GET_STATUS_OK".equals(tr.responseData.subscriptionResult)) {
                        status = ResponseMessage.Status.ACTIVE;
                    } else if ("BLOCKED".equals(tr.responseData.subSubStatus)) {
                        status = ResponseMessage.Status.BLACKLIST;
                    } else if ("Invalid Account, no account Id".equals(tr.message)) {
                        status = ResponseMessage.Status.PORTING;
                    }
                }
            }
        } catch (Exception ex) {
            log.error("No fue posible obtener el estado de la suscripcion. [" + sop.getId() + "][" + response + "]. " + ex, ex);
        }

        return status;
    }

    private String getAuthentication(SOP sop) throws Exception {
        SecretKeySpec keySpecification = new SecretKeySpec(sop.getIntegrationSettings().get("preshared-key").getBytes("UTF-8"), "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keySpecification);
        long reportDate = new Date().getTime();
        String enrypti = sop.getIntegrationSettings().get("partner_service") + "#" + reportDate;
        byte[] encryptedBytes = cipher.doFinal(enrypti.getBytes());
        return java.util.Base64.getEncoder().encodeToString(encryptedBytes);
    }

}
