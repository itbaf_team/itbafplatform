/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.repository.jpa.model.ph;

import com.itbaf.platform.paymenthub.model.ph.PHProfile;
import com.itbaf.platform.paymenthub.model.ph.QPHProfile;
import com.itbaf.platform.persistence.jpa.AbstractJPARepositorySession;
import com.querydsl.jpa.impl.JPAQuery;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;

/**
 *
 * @author jordonez
 */
@lombok.extern.log4j.Log4j2
public class JPAPHProfileRepository extends AbstractJPARepositorySession<PHProfile, Long> {

    @Inject
    public JPAPHProfileRepository(final Provider<EntityManager> entityManagerProvider) {
        super(PHProfile.class, entityManagerProvider);
    }

    public PHProfile findByName(String name) {

        if (name == null) {
            String error = "El nombre del perfil es nulo!!";
            log.error(error);
            throw new RuntimeException(error);
        }
        name = name.toLowerCase();
        QPHProfile profile = QPHProfile.pHProfile;
        JPAQuery<?> query = new JPAQuery(getEntityManager());

        PHProfile p = query.select(profile).from(profile).
                where(profile.name.eq(name)).fetchFirst();

        return p;
    }

}
