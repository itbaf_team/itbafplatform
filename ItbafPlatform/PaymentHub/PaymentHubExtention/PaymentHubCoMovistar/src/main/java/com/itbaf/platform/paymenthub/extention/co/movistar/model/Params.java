/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.co.movistar.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import java.util.List;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class Params {

    private Integer resultCode;
    private String desc;
    private String transactionId;
    @JacksonXmlProperty(localName = "subscription")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Subscription> subscriptions;
    @JacksonXmlProperty(localName = "dictionary")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Dictionary> dictionaries;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("resultCode", resultCode)
                .add("desc", desc)
                .add("transactionId",transactionId)
                .add("subscriptions", subscriptions)
                .add("dictionaries", dictionaries)
                .omitNullValues().toString();
    }
}
