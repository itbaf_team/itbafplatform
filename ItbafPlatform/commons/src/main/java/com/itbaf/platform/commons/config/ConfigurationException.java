package com.itbaf.platform.commons.config;

/**
 * Signals errors during configuration load up.
 *
 * @author juan.palacios
 */
@SuppressWarnings("serial")
public class ConfigurationException extends RuntimeException {

  /**
   * Initializes this exception with a cause and an error message.
   *
   * @see RuntimeException#RuntimeException(String, Throwable)
   */
  public ConfigurationException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
