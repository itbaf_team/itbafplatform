package com.itbaf.platform.paymenthub.extention.sc.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.paymenthub.extention.sc.model.SCRequest;
import com.itbaf.platform.paymenthub.extention.sc.model.SCResponse;
import com.itbaf.platform.paymenthub.model.SOP;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class SCResources {

    private final ObjectMapper mapper;
    private final RequestClient requestClient;
    private static final String URL_BASE = "xxx";
    private static final String STATUS_URL = URL_BASE + "/api/v2/subscription";
    private static final String SEND_PIN_URL = URL_BASE + "/api/v2/subscription";
    private static final String VALIDATE_PIN_URL = URL_BASE + "/api/v2/subscription";
    private static final String UNSUBSCRIBE_URL = URL_BASE + "/api/v2/subscription";

    @Inject
    public SCResources(final ObjectMapper mapper,
            final RequestClient requestClient) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
    }

    public SCResponse checkStatusByMSISDN(String msisdn, SOP sop) {

        SCRequest request = new SCRequest();
        request.sid = "+" + msisdn;
        request.product = sop.getIntegrationSettings().get("sc_productId");
        request.take = 15;
        request.skip = 0;
        SCResponse sr = null;
        try {
            String req = mapper.writeValueAsString(request);
            String response = requestClient.requestJSONPost(STATUS_URL, req);
            log.info("SmartContents. checkStatusByMSISDN Request: [" + request + "]. Response: [" + response + "]");
            sr = mapper.readValue(response, SCResponse.class);
        } catch (Exception ex) {
            log.error("SmartContents. Error checkStatusByMSISDN ANI: [" + msisdn + "] - SOP: [" + sop + "]. " + ex, ex);
        }
        return sr;
    }

    public SCResponse checkStatusBySubsriptionId(String subscriptionId, SOP sop) {
        SCRequest request = new SCRequest();
        request.subscriptions = new String[1];
        request.subscriptions[0] = subscriptionId;
        request.product = sop.getIntegrationSettings().get("sc_productId");
        request.take = 15;
        request.skip = 0;
        SCResponse sr = null;
        try {
            String req = mapper.writeValueAsString(request);
            String response = requestClient.requestJSONPost(STATUS_URL, req);
            log.info("SmartContents. checkStatusBySubsriptionId Request: [" + request + "]. Response: [" + response + "]");
            sr = mapper.readValue(response, SCResponse.class);
        } catch (Exception ex) {
            log.error("SmartContents. Error checkStatusBySubsriptionId subscriptionId: [" + subscriptionId + "] - SOP: [" + sop + "]. " + ex, ex);
        }
        return sr;
    }

    public SCResponse sendPin(String msisdn, SOP sop, String channel) {
        SCRequest request = new SCRequest();
        request.sid = "+" + msisdn;
        request.product = sop.getIntegrationSettings().get("sc_productId");
        if (channel == null || (!"wap".equals(channel) && !"sms".equals(channel) && !"mobile_web".equals(channel) && !"web".equals(channel))) {
            channel = "web";
        }
        request.channel = channel;
        request.trial = "0";
        SCResponse sr = null;
        try {
            String req = mapper.writeValueAsString(request);
            String response = requestClient.requestJSONPost(SEND_PIN_URL, req);
            log.info("SmartContents. sendPin Request: [" + request + "]. Response: [" + response + "]");
            sr = mapper.readValue(response, SCResponse.class);
        } catch (Exception ex) {
            log.error("SmartContents. Error sendPin ANI: [" + msisdn + "] - SOP: [" + sop + "] - Channel: [" + channel + "]. " + ex, ex);
        }
        return sr;
    }

    public SCResponse validatePin(String msisdn, SOP sop, String pin) {
        SCRequest request = new SCRequest();
        request.sid = "+" + msisdn;
        request.validator = pin;
        SCResponse sr = null;
        try {
            String req = mapper.writeValueAsString(request);
            String response = requestClient.requestJSONPost(VALIDATE_PIN_URL, req);
            log.info("SmartContents. validatePin Request: [" + request + "]. Response: [" + response + "]");
            sr = mapper.readValue(response, SCResponse.class);
        } catch (Exception ex) {
            log.error("SmartContents. Error validatePin ANI: [" + msisdn + "] - SOP: [" + sop + "] - PIN: [" + pin + "]. " + ex, ex);
        }
        return sr;
    }

    public SCResponse unsubscribe(String msisdn, SOP sop) {
        SCRequest request = new SCRequest();
        request.sid = "+" + msisdn;
        request.product = sop.getIntegrationSettings().get("sc_productId");
        SCResponse sr = null;
        try {
            String req = mapper.writeValueAsString(request);
            String response = requestClient.requestJSONPost(UNSUBSCRIBE_URL, req);
            log.info("SmartContents. unsubscribe Request: [" + request + "]. Response: [" + response + "]");
            sr = mapper.readValue(response, SCResponse.class);
        } catch (Exception ex) {
            log.error("SmartContents. Error unsubscribe ANI: [" + msisdn + "] - SOP: [" + sop + "]. " + ex, ex);
        }
        return sr;
    }

}
