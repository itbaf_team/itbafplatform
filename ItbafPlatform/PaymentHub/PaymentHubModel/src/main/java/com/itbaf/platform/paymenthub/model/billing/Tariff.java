/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.billing;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.paymenthub.model.Status;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author javier
 */
@Entity(name = "TARIFF")
@Table(name = "TARIFF", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class Tariff implements Serializable, Comparable {

    private static final long serialVersionUID = 20170809155407L;

    public enum TariffType {
        MAIN, CHILDREN
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "TARIFF_TYPE", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private TariffType tariffType;
    @Column(name = "STATUS", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Status status;
    @Column(name = "EXTERNAL_ID")
    private String externalId;
    @Column(name = "FRECUENCY")
    private Integer frecuency;
    @Column(name = "FRECUENCY_TYPE")
    private String frecuencyType;
    @Column(name = "CURRENCY")
    private String currency;
    @Column(name = "FULL_AMOUNT")
    private BigDecimal fullAmount;
    @Column(name = "NET_AMOUNT")
    private BigDecimal netAmount;
    @Column(name = "PERCENTAGE")
    private BigDecimal percentage;
    @Column(name = "LOCAL_CURRENCY")
    private String localCurrency;
    @Column(name = "LOCAL_AMOUNT")
    private BigDecimal localAmount;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FROM_DATE")
    private Date fromDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TO_DATE")
    private Date toDate;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("tariffType", tariffType)
                .add("status", status)
                .add("externalId", externalId)
                .add("frecuency", frecuency)
                .add("frecuencyType", frecuencyType)
                .add("currency", currency)
                .add("fullAmount", fullAmount)
                .add("netAmount", netAmount)
                .add("percentage", percentage)
                .add("localCurrency", localCurrency)
                .add("localAmount", localAmount)
                .add("fromDate", fromDate)
                .add("toDate", toDate)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.id);
        hash = 29 * hash + Objects.hashCode(this.tariffType);
        hash = 29 * hash + Objects.hashCode(this.status);
        hash = 29 * hash + Objects.hashCode(this.externalId);
        hash = 29 * hash + Objects.hashCode(this.frecuency);
        hash = 29 * hash + Objects.hashCode(this.frecuencyType);
        hash = 29 * hash + Objects.hashCode(this.currency);
        hash = 29 * hash + Objects.hashCode(this.fullAmount);
        hash = 29 * hash + Objects.hashCode(this.netAmount);
        hash = 29 * hash + Objects.hashCode(this.percentage);
        hash = 29 * hash + Objects.hashCode(this.localCurrency);
        hash = 29 * hash + Objects.hashCode(this.localAmount);
        hash = 29 * hash + Objects.hashCode(this.fromDate);
        hash = 29 * hash + Objects.hashCode(this.toDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tariff other = (Tariff) obj;
        if (!Objects.equals(this.externalId, other.externalId)) {
            return false;
        }
        if (!Objects.equals(this.frecuencyType, other.frecuencyType)) {
            return false;
        }
        if (!Objects.equals(this.currency, other.currency)) {
            return false;
        }
        if (!Objects.equals(this.localCurrency, other.localCurrency)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (this.tariffType != other.tariffType) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.frecuency, other.frecuency)) {
            return false;
        }
        if (!Objects.equals(this.fullAmount, other.fullAmount)) {
            return false;
        }
        if (!Objects.equals(this.netAmount, other.netAmount)) {
            return false;
        }
        if (!Objects.equals(this.percentage, other.percentage)) {
            return false;
        }
        if (!Objects.equals(this.localAmount, other.localAmount)) {
            return false;
        }
        if (!Objects.equals(this.fromDate, other.fromDate)) {
            return false;
        }
        if (!Objects.equals(this.toDate, other.toDate)) {
            return false;
        }
        return true;
    }

    



    @Override
    public int compareTo(Object o) {
        return ((Tariff) o).fullAmount.compareTo(fullAmount);
    }

}
