package com.itbaf.platform.paymenthub.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QNotification is a Querydsl query type for Notification
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QNotification extends EntityPathBase<Notification> {

    private static final long serialVersionUID = -991413997L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QNotification notification = new QNotification("notification");

    public final StringPath externalId = createString("externalId");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath internalId = createString("internalId");

    public final DateTimePath<java.util.Date> notificationDate = createDateTime("notificationDate", java.util.Date.class);

    public final EnumPath<com.itbaf.platform.paymenthub.messaging.MessageType> notificationType = createEnum("notificationType", com.itbaf.platform.paymenthub.messaging.MessageType.class);

    public final StringPath originalNotification = createString("originalNotification");

    protected QSOP sop;

    public final StringPath transactionId = createString("transactionId");

    public final StringPath userAccount = createString("userAccount");

    public QNotification(String variable) {
        this(Notification.class, forVariable(variable), INITS);
    }

    public QNotification(Path<? extends Notification> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QNotification(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QNotification(PathMetadata metadata, PathInits inits) {
        this(Notification.class, metadata, inits);
    }

    public QNotification(Class<? extends Notification> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sop = inits.isInitialized("sop") ? new QSOP(forProperty("sop"), inits.get("sop")) : null;
    }

    public QSOP sop() {
        if (sop == null) {
            sop = new QSOP(forProperty("sop"));
        }
        return sop;
    }

}

