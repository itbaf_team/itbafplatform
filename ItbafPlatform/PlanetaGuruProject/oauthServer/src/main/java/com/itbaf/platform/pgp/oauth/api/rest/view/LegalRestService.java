package com.itbaf.platform.pgp.oauth.api.rest.view;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.pgp.oauth.api.handler.LegalServiceProcessorHandler;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/legal")
@Singleton
@lombok.extern.log4j.Log4j2
public class LegalRestService {

    private final LegalServiceProcessorHandler legalServiceProcessorHandler;

    @Inject
    public LegalRestService(LegalServiceProcessorHandler legalServiceProcessorHandler) {
        this.legalServiceProcessorHandler = Validate.notNull(legalServiceProcessorHandler, "A LegalServiceProcessorHandler class must be provided");
    }

    @GET
    @Path("/{i18n}/{page}")
    @Produces(MediaType.TEXT_HTML)
    public Response login(@Context HttpServletRequest request,
            @PathParam("i18n") String i18n,
            @PathParam("page") String page) {
        try {
            Thread.currentThread().setName(Thread.currentThread().getName() + "-legal." + page);
        } catch (Exception ex) {
        }

        log.info("Legal.  i18n: [" + i18n + "] - Page: [" + page + "]");

        String html = legalServiceProcessorHandler.getLegalHtml(i18n, page);
        if (html == null) {
            html = legalServiceProcessorHandler.getLegalHtml("es", "privacy-statement");
        }

        return Response.status(Status.OK).entity(html).type(MediaType.TEXT_HTML).build();
    }

}
