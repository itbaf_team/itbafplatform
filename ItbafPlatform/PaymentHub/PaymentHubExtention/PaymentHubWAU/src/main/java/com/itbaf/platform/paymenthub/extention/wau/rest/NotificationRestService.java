package com.itbaf.platform.paymenthub.extention.wau.rest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.commons.bn.model.WAUNotification;
import com.itbaf.platform.paymenthub.extention.wau.handler.ServiceProcessorHandler;
import java.util.Arrays;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.lang3.Validate;

@Path("/notification")
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationRestService {

    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public NotificationRestService(ServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");

    }

    @GET
    @Path("/{country}/{telco}/wau/subscription")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public Response subscriptionNotification(final @PathParam("country") String countryCode,
            final @PathParam("telco") String telco, final @Context UriInfo uriInfo) {

        log.info("WAU Notification. CountryCode: [" + countryCode + "] - telco: [" + telco + "] - Notification: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]");

        try {
            WAUNotification not = new WAUNotification(WAUNotification.Type.valueOf(uriInfo.getQueryParameters().getFirst("type")),
                    uriInfo.getQueryParameters().getFirst("request_id"), uriInfo.getQueryParameters().getFirst("result_code"),
                    uriInfo.getQueryParameters().getFirst("subscription_id"), uriInfo.getQueryParameters().getFirst("event"),
                    uriInfo.getQueryParameters().getFirst("event_date"), uriInfo.getQueryParameters().getFirst("expire_date"),
                    uriInfo.getQueryParameters().getFirst("channel_id"), uriInfo.getQueryParameters().getFirst("mobile_number"),
                    uriInfo.getQueryParameters().getFirst("product_id"), uriInfo.getQueryParameters().getFirst("keyword"),
                    uriInfo.getQueryParameters().getFirst("shorcode"), uriInfo.getQueryParameters().getFirst("status"),
                    uriInfo.getQueryParameters().getFirst("tariff"), uriInfo.getQueryParameters().getFirst("fraction"),
                    uriInfo.getQueryParameters().getFirst("reason_id"));
            not.countryCode = countryCode;
            not.telco = telco;
            serviceProcessorHandler.processNotification(not);
        } catch (Exception ex) {
            log.error("Error al interpretar CTR. CountryCode [" + countryCode + "] - Notification: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]. " + ex, ex);
        }

        return Response.status(Status.OK).entity("Ok").build();
    }
}
