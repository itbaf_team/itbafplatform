/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.sync;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import static com.itbaf.platform.commons.CommonFunction.CHACHE_MEMORY_UPDATE;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.ph.ChannelQuantityProvider;
import com.itbaf.platform.paymenthub.model.ph.PHInstance;
import com.itbaf.platform.paymenthub.services.repository.PHInstanceService;
import com.itbaf.platform.paymenthub.sync.handler.Activated24HoursProcessorHandler;
import com.itbaf.platform.persistence.connection.BasicDataBaseConnection;
import com.itbaf.platform.rmq.producer.RabbitMQProducer;
import com.itbaf.platform.commons.resources.InstrumentedRunnable;
import com.itbaf.platform.paymenthub.model.subscription.SubscriptionRegistry;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionRegistryService;
import com.itbaf.platform.rmq.consumer.RabbitMQConsumer;
import com.itbaf.platform.rmq.services.RabbitMQMemoryProcessorImpl;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain {

    private final PHInstance instance;
    private final ObjectMapper mapper;
    private final ExecutorService cachedThreadPool;
    private final RabbitMQProducer rabbitMQProducer;
    private final RabbitMQConsumer rabbitMQConsumer;
    private final InstrumentedObject instrumentedObject;
    private final BasicDataBaseConnection dataBaseConnection;
    private final RabbitMQMemoryProcessorImpl memoryProcessor;
    private final ScheduledExecutorService scheduledThreadPool;
    private final SubscriptionRegistryService subscriptionRegistryService;
    private final Activated24HoursProcessorHandler activated24HoursHandler;
    private static boolean isStop = false;

    @Inject
    public PaymentHubMain(final PHInstance instance,
            final ObjectMapper mapper,
            final ExecutorService cachedThreadPool,
            final PHInstanceService instanceService,
            final RabbitMQProducer rabbitMQProducer,
            final RabbitMQConsumer rabbitMQConsumer,
            final InstrumentedObject instrumentedObject,
            final BasicDataBaseConnection dataBaseConnection,
            final RabbitMQMemoryProcessorImpl memoryProcessor,
            final ScheduledExecutorService scheduledThreadPool,
            final SubscriptionRegistryService subscriptionRegistryService,
            final Activated24HoursProcessorHandler activated24HoursHandler){
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.instance = Validate.notNull(instance, "A PHInstance class must be provided");
        this.cachedThreadPool = Validate.notNull(cachedThreadPool, "An ExecutorService class must be provided");
        this.rabbitMQProducer = Validate.notNull(rabbitMQProducer, "A RabbitMQProducer class must be provided");
        this.rabbitMQConsumer = Validate.notNull(rabbitMQConsumer, "A RabbitMQConsumer class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");
        this.memoryProcessor = Validate.notNull(memoryProcessor, "A RabbitMQMemoryProcessorImpl class must be provided");
        this.dataBaseConnection = Validate.notNull(dataBaseConnection, "A BasicDataBaseConnection class must be provided");
        this.scheduledThreadPool = Validate.notNull(scheduledThreadPool, "A ScheduledExecutorService class must be provided");
        this.subscriptionRegistryService = Validate.notNull(subscriptionRegistryService, "An SubscriptionRegistryService class must be provided");
        this.activated24HoursHandler = Validate.notNull(activated24HoursHandler, "A Actived24HoursProcessorHandler class must be provided");
    }

    public void start() {
        log.info("Initializing app... ");
        try {
            rabbitMQConsumer.createFanoutChannel(CHACHE_MEMORY_UPDATE, memoryProcessor);
        } catch (Exception ex) {
            log.error("Error al crear consumer '" + CHACHE_MEMORY_UPDATE + "'. " + ex, ex);
        }
        startThreads();
        log.info("Initializing app OK. ");
    }

    private void startThreads() {
        isStop = false;
        scheduledThreadPool.scheduleWithFixedDelay(new InstrumentedRunnable("sync", null, instrumentedObject) {
            @Override
            public boolean exec() {
                if (isStop) {
                    return false;
                }
                Thread.currentThread().setName(CommonFunction.getTID("sync.T"));
                try {
                    Set<Provider> providers = instance.getConfigurations().keySet();
                    for (Provider p : providers) {
                        ChannelQuantityProvider cqp = instance.getConfigurations().get(p);
                        if (cqp.isSyncConsumer()) {
                            try {
                                String jsonProvider = mapper.writeValueAsString(p);
                                if (jsonProvider != null) {
                                    rabbitMQProducer.messageSender("sync.subscriptions." + p.getName(), jsonProvider);
                                }
                            } catch (Exception ex) {
                                log.error("Error al enviar sincronizacion al provider: [" + p + "]. " + ex, ex);
                            }
                        }
                    }
                    return true;
                } catch (Exception ex) {
                    log.error("Error al procesar sincronizacion. " + ex, ex);
                }
                return false;
            }
        }, 1, 45, TimeUnit.MINUTES);

        scheduledThreadPool.scheduleWithFixedDelay(new InstrumentedRunnable("orphan", null, instrumentedObject) {
            @Override
            public boolean exec() {
                if (isStop) {
                    return false;
                }
                Thread.currentThread().setName(CommonFunction.getTID("orphan.T"));
                try {
                    List<SubscriptionRegistry> lst = subscriptionRegistryService.getOrphanRegisters();
                    log.info("Orphan found; List<SubscriptionRegistry>: [" + (lst == null ? "0" : lst.size()) + "]");

                    if (lst == null) {
                        return true;
                    }
                    for (SubscriptionRegistry sr : lst) {
                        try {
                            sr.setOriginUnsubscription("SYNC:ORPHAN");
                            sr.setUnsubscriptionDate(sr.getSubscription().getSubscriptionDate());
                            subscriptionRegistryService.saveOrUpdate(sr);
                            log.info("Actualizado OrphanRegister: [" + sr + "]");
                        } catch (Exception ex) {
                            log.error("Error al procesar OrphanRegister: [" + sr + "]. " + ex, ex);
                        }
                    }
                    return true;
                } catch (Exception ex) {
                    log.error("Error al procesar OrphanRegisters. " + ex, ex);
                }
                return false;
            }
        }, 1, 300, TimeUnit.MINUTES);

        scheduledThreadPool.scheduleWithFixedDelay(new InstrumentedRunnable("activated_24_hours_without_billing", null, instrumentedObject) {
            @Override
            public boolean exec() {
                if (isStop) {
                    return false;
                }
                Thread.currentThread().setName(CommonFunction.getTID("activated_24_hours_without_billing.T"));

                return activated24HoursHandler.process();
            }
        }, 15, 750, TimeUnit.MINUTES);

    }

    public void stop() {
        stopThreads();
        rabbitMQProducer.stopProducers();
        dataBaseConnection.shutdownConnectionsPool();
        log.info("App destroyed. ");
    }

    public void stopThreads() {
        isStop = true;
        log.info("Finalizando Threads en 5 segundos... ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
        }
        try {
            log.info("Finalizando scheduledThreadPool ");
            scheduledThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdown();
        } catch (Exception ex) {
        }
        try {
            log.info("Esperando Threads por 2 segundos... ");
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        }

        try {
            scheduledThreadPool.shutdownNow();
            log.info("Finalizado scheduledThreadPool ");
        } catch (Exception ex) {
        }
        try {
            cachedThreadPool.shutdownNow();
        } catch (Exception ex) {
        }
    }

}
