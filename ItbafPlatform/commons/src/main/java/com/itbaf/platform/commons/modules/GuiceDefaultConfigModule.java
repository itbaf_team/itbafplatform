/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.modules;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.itbaf.platform.commons.CommonDaemon;
import com.itbaf.platform.commons.DebugSignalHandler;
import com.itbaf.platform.commons.config.PropertiesLoader;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import org.apache.logging.log4j.LogManager;
import org.redisson.Redisson;
import org.redisson.config.Config;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;

/**
 *
 * @author jordonez
 */
public abstract class GuiceDefaultConfigModule extends AbstractModule {

    private final String path;
    private final String SETTINGS_PATH;
    private final CommonDaemon commonDaemon;
    private final String SETTINGS_SESSION_PATH;
    protected final Map<String, String> args;
    protected org.apache.logging.log4j.Logger log;

    public GuiceDefaultConfigModule(final Map<String, String> args, CommonDaemon commonDaemon) {
        this.args = args;
        this.path = "";
        this.commonDaemon = commonDaemon;
        this.SETTINGS_PATH = path + "settings.properties";
        this.SETTINGS_SESSION_PATH = path + "settings-" + args.get("guice.profile") + ".properties";
    }

    private void startSignalHandler(CommonDaemon commonDaemon) {
        if (commonDaemon == null) {
            return;
        }

        DebugSignalHandler.listenTo("TERM", commonDaemon);
        DebugSignalHandler.listenTo("KILL", commonDaemon);
        DebugSignalHandler.listenTo("QUIT", commonDaemon);
        DebugSignalHandler.listenTo("STOP", commonDaemon);
    }

    @Override
    protected void configure() {
        InstrumentedObject.commonDaemon = commonDaemon;

        //Properties
        Properties settings = new PropertiesLoader(SETTINGS_PATH).loadSProperties();
        settings.putAll(new PropertiesLoader(SETTINGS_SESSION_PATH).loadSProperties());
        settings.putAll(new PropertiesLoader(SETTINGS_PATH).loadProperties());
        settings.putAll(new PropertiesLoader(SETTINGS_SESSION_PATH).loadProperties());

        Map<String, String> propertiesMap = new HashMap();
        settings.stringPropertyNames().forEach((key) -> {
            propertiesMap.put(key, settings.getProperty(key));
        });

        propertiesMap.putAll(args);

        //Buscamos parametros en las variables de entorno
        try {
            propertiesMap.keySet().forEach((a) -> {
                String aux = System.getenv(a);
                if (aux != null) {
                    propertiesMap.put(a, aux);
                }
            });
        } catch (Exception ex) {
        }

        //Binds
        propertiesMap.keySet().forEach((key) -> {
            bindConstant().annotatedWith(Names.named(key)).to(propertiesMap.get(key));
        });
        bind(MessageBodyReader.class).to(JacksonJsonProvider.class);
        bind(MessageBodyWriter.class).to(JacksonJsonProvider.class);

        //Estaba asi en el constructor de InstrumentedObject
        //@Named("app.name") String appName
        InstrumentedObject.appName = propertiesMap.get("app.name");
        InstrumentedObject.profile = propertiesMap.get("guice.profile");
        InstrumentedObject.instance = propertiesMap.get("guice.instance");
        InstrumentedObject.container = propertiesMap.get("guice.container");
        InstrumentedObject.logType = propertiesMap.get("guice.logs.type");

        InstrumentedObject.redisConfigLocalPath = propertiesMap.get("redisson.config.json.local.path");
        InstrumentedObject.redisConfigRemotePath = propertiesMap.get("redisson.config.json.remote.path");
        try {
            Config config = loadConfig(GuiceDefaultConfigModule.class.getClassLoader(), InstrumentedObject.redisConfigLocalPath);
            InstrumentedObject.redis = Redisson.create(config);
            System.out.println("Create local redis: [" + InstrumentedObject.redisConfigLocalPath + "]");
        } catch (Exception ex) {
            System.err.println("Error al cargar la configuracion de redis-local: [" + InstrumentedObject.redisConfigLocalPath + "]. " + ex);
            ex.printStackTrace();
        }
        for (String key : propertiesMap.keySet()) {
            if (key.startsWith("META_")) {
                InstrumentedObject.parameterMap.put(key, propertiesMap.get(key));
            }
        }

        //Modulos de aplicaciones
        // install(new GuiceInstrumentedConfigModule());
        install(new GuiceThreadsConfigModule());

        log = LogManager.getLogger(GuiceDefaultConfigModule.class);
        this.startSignalHandler(commonDaemon);
    }

    private Config loadConfig(String configPath) throws Exception {
        try {
            return Config.fromJSON(new File(configPath));
        } catch (IOException e) {
            throw new Exception("Can't parse default json config", e);
        }
    }

    private Config loadConfig(ClassLoader classLoader, String fileName) throws Exception {
        Config config = null;
        try {
            InputStream is = classLoader.getResourceAsStream(fileName);
            if (is != null) {
                try {
                    config = Config.fromJSON(is);
                } finally {
                    is.close();
                }
            }
        } catch (IOException e) {
            throw new Exception("Can't parse json config", e);
        }
        if (config == null) {
            config = loadConfig(fileName);
        }
        return config;
    }

    @Provides
    @Singleton
    Server serverProvider(@Named("server.port") Integer serverPort) {
        return new Server(serverPort);
    }

    @Provides
    @Singleton
    ServletContextHandler servletContextHandlerProvider(Server server) {
        return new ServletContextHandler(server, "/");
    }

    @Provides
    @Singleton
    ObjectMapper objectMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.setDefaultPropertyInclusion(JsonInclude.Value.construct(JsonInclude.Include.NON_NULL, JsonInclude.Include.ALWAYS));
        if ("json".equals(InstrumentedObject.logType)) {
            InstrumentedObject.mapper = mapper;
        }
        return mapper;
    }

    @Provides
    @Singleton
    JacksonJsonProvider jacksonJsonProvider(ObjectMapper mapper) {
        return new JacksonJsonProvider(mapper);
    }

}
