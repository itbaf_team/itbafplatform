/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.tiaxa;

import com.google.inject.Inject;
import com.itbaf.platform.commons.CommonFunction;
import com.itbaf.platform.commons.resources.InstrumentedRunnable;
import com.itbaf.platform.paymenthub.commons.PaymentHubBasic;
import com.itbaf.platform.paymenthub.extention.tiaxa.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.model.Provider;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class PaymentHubMain extends PaymentHubBasic {
    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public PaymentHubMain(final ServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
    }

    @Override
    protected void startActions() {
        super.startActions();
        for (Provider p : providerSet) {
            try {
                scheduledThreadPool.scheduleWithFixedDelay(new InstrumentedRunnable("sync.retention", p.getName(), instrumentedObject) {
                    @Override
                    public boolean exec() {
                        if (isStop) {
                            return false;
                        }
                        Thread.currentThread().setName(CommonFunction.getTID("sync.retention" + p.getName() ));
                        RLock lock = instrumentedObject.lockObject("TIAXA_RETENTION_" + p.getName(), 40);
                        long aux = instrumentedObject.getAtomicLong("TIAXA_RETENTION_A_" + p.getName(), 1);
                        if (aux == 0) {
                            instrumentedObject.getAndIncrementAtomicLong("TIAXA_RETENTION_A_" + p.getName(), 1);
                            try {
                                serviceProcessorHandler.syncRetention(p.getName());
                                instrumentedObject.unLockObject(lock);
                                return true;
                            } catch (Exception ex) {
                            }
                        }
                        instrumentedObject.unLockObject(lock);
                        return false;
                    }
                }, 15, 1440, TimeUnit.MINUTES);
            } catch (Exception ex) {
                log.error("Error al crear schedule para provider: [" + p + "]. " + ex, ex);
            }
        }
    }
}
