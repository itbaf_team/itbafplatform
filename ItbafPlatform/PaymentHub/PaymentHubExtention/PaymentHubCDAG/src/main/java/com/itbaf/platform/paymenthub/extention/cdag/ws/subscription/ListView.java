
package com.itbaf.platform.paymenthub.extention.cdag.ws.subscription;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listView complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listView">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{urn:SendatelSoaService}arrayOfFieldList" maxOccurs="unbounded"/>
 *         &lt;element name="values" type="{urn:SendatelSoaService}matrixOfFieldList" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listView", propOrder = {
    "header",
    "values"
})
public class ListView {

    @XmlElement(required = true)
    protected List<ArrayOfFieldList> header;
    @XmlElement(required = true)
    protected List<MatrixOfFieldList> values;

    /**
     * Gets the value of the header property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the header property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHeader().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrayOfFieldList }
     * 
     * 
     */
    public List<ArrayOfFieldList> getHeader() {
        if (header == null) {
            header = new ArrayList<ArrayOfFieldList>();
        }
        return this.header;
    }

    /**
     * Gets the value of the values property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the values property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MatrixOfFieldList }
     * 
     * 
     */
    public List<MatrixOfFieldList> getValues() {
        if (values == null) {
            values = new ArrayList<MatrixOfFieldList>();
        }
        return this.values;
    }

}
