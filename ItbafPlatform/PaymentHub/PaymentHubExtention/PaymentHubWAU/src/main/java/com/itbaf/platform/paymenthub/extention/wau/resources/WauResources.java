package com.itbaf.platform.paymenthub.extention.wau.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.extention.wau.model.ResponseSubscribePin;
import com.itbaf.platform.paymenthub.extention.wau.model.ResponseSubscriptionStatus;
import com.itbaf.platform.paymenthub.extention.wau.model.SendPin;
import com.itbaf.platform.paymenthub.extention.wau.model.Subscribe;
import com.itbaf.platform.paymenthub.extention.wau.model.Subscriber;
import com.itbaf.platform.paymenthub.extention.wau.model.SubscriptionDavid;
import com.itbaf.platform.paymenthub.extention.wau.model.SubscriptionDavidResponse;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.MediaType;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class WauResources {

    private final XmlMapper xmlMapper;
    private final RequestClient requestClient;
    private static final String WAU_ENDPOINT = "http://api01.vas-pass.com:8380/DavidCore/";

    @Inject
    public WauResources(final XmlMapper xmlMapper, final ObjectMapper mapper,
            final RequestClient requestClient) {
        this.xmlMapper = Validate.notNull(xmlMapper, "A XmlMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
    }

    private String sendPost(String url, String data, int cont) {
        String response = requestClient.requestSimplePost(url, data, MediaType.APPLICATION_XML_TYPE, MediaType.APPLICATION_XML_TYPE, null);
        if (response == null) {
            if (cont > 7) {
                return null;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            cont++;
            return sendPost(url, data, cont);
        }
        return response;
    }

    public ResponseSubscriptionStatus subscriptionStatus(Subscription s, ResponseMessage rm) {
        SOP sop = s.getSop();
        Subscribe subscribe = new Subscribe(sop.getIntegrationSettings().get("access_token"),
                sop.getIntegrationSettings().get("integrator_id"),
                s.getUserAccount(), null, sop.getIntegrationSettings().get("wau_productId"));

        ResponseSubscriptionStatus responseObject;
        String response = null;
        String request = null;
        try {
            String url = WAU_ENDPOINT + "subscribers/getone";
            request = xmlMapper.writeValueAsString(subscribe).replace("<channel_id/>", "");
            response = sendPost(url, request, 0);
            log.info("Wau. subscriptionStatus [" + request + "]. Response: [" + response + "]");
            if (response == null || response.isEmpty()) {
                return null;
            }
            responseObject = xmlMapper.readValue(response, ResponseSubscriptionStatus.class);
            return responseObject;
        } catch (Exception ex) {
            String er = "Wau. subscriptionStatus [" + request + "]. Response: [" + response + "]. ";
            log.error(er + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = er;
            return null;
        }
    }

    public ResponseSubscribePin subscribeOptIn(Subscription s, ResponseMessage rm) {
        SOP sop = s.getSop();
        String channel = "1";
        
        /* WSD-7113: Alta Suscripción No llega pin code 
            Ariel Garcia (2019-06-18, 12:54 PM)
            Estimado,
            El problema en este caso es, las altas via WAP (Channel 3) no pueden realizarce a través de un request. Estas altas se deben generar exclusivamente por medio de la navegación del usuario.
            El request optin con channel 3 ya no son permitidas.
            El cambio aplico a las suscripciones via wap-sms. Estos canales pueden generar altas únicamente a través de la terminal.
            Slds

        ======================================================================================

        if (s.getObject() != null) {
            switch (s.getObject().toString().toLowerCase().trim()) {
                case "web":
                    channel = "1";
                    break;
                case "sms":
                    channel = "2";
                    break;
                case "wap":
                    channel = "3";
            }
        }

        ====================================================================================== */
        Subscribe subscribe = new Subscribe(sop.getIntegrationSettings().get("access_token"),
                sop.getIntegrationSettings().get("integrator_id"),
                s.getUserAccount(), channel, sop.getIntegrationSettings().get("wau_productId"));
        String response = null;
        String request = null;
        ResponseSubscribePin responseObject;
        try {
            String url = WAU_ENDPOINT + "request/optin";
            request = xmlMapper.writeValueAsString(subscribe);
            response = sendPost(url, request, 0);
            log.info("Wau. subscribeOptIn [" + request + "]. Response: [" + response + "]");
            responseObject = xmlMapper.readValue(response, ResponseSubscribePin.class);
            return responseObject;
        } catch (Exception ex) {
            String er = "Wau. subscribeOptIn [" + request + "]. Response: [" + response + "]. ";
            log.error(er + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = er;
            return null;
        }
    }

    public ResponseSubscribePin validatePin(Subscription s, String pin, String requestId, ResponseMessage rm) {
        SOP sop = s.getSop();
        SendPin sendPin = new SendPin(sop.getIntegrationSettings().get("access_token"),
                sop.getIntegrationSettings().get("integrator_id"), s.getUserAccount(), pin, requestId);
        String response = null;
        String request = null;
        ResponseSubscribePin responseObject;
        try {
            String url = WAU_ENDPOINT + "request/validatepin";
            request = xmlMapper.writeValueAsString(sendPin);
            response = sendPost(url, request, 0);
            log.info("Wau. validatePin [" + request + "]. Response: [" + response + "]");
            responseObject = xmlMapper.readValue(response, ResponseSubscribePin.class);
            return responseObject;
        } catch (Exception ex) {
            String er = "Wau. validatePin [" + request + "]. Response: [" + response + "]. ";
            log.error(er + ex, ex);
            rm.status = ResponseMessage.Status.ERROR;
            rm.message = er;
            return null;
        }
    }

    public List<Subscriber> getmany(SOP sop) {
        String integrator_id = sop.getIntegrationSettings().get("integrator_id");
        String access_token = sop.getIntegrationSettings().get("access_token");
        String product_id = sop.getIntegrationSettings().get("wau_productId");
        String url = WAU_ENDPOINT + "subscribers/getmany";

        SubscriptionDavid sd = new SubscriptionDavid(integrator_id, access_token, "500", "1", product_id);

        List<Subscriber> subs = new ArrayList();
        String request = null;
        String response = null;
        try {

            request = xmlMapper.writeValueAsString(sd).replace("SubscriptionDavid", "request_data");
            response = sendPost(url, request, 0);

            log.info("Wau. getmany [1][" + request + "]. Response: [" + response + "]");
            SubscriptionDavidResponse sdr = xmlMapper.readValue(response, SubscriptionDavidResponse.class);

            if (sdr == null || sdr.getSubscribers() == null) {
                return subs;
            }

            Long pages = sdr.getPages();
            int current = 1;
            do {
                if (sdr != null) {
                    for (Subscriber s : sdr.getSubscribers()) {
                        if ("1".equals(s.getSubscription_status()) || "4".equals(s.getSubscription_status())) {
                            subs.add(s);
                        }
                    }
                }
                sdr = null;
                current++;
                sd = new SubscriptionDavid(integrator_id, access_token, "500", current + "", product_id);
                try {
                    request = xmlMapper.writeValueAsString(sd).replace("SubscriptionDavid", "request_data");
                    response = sendPost(url, request, 0);
                    log.info("Wau. getmany [" + current + "][" + request + "]. Response: [" + response + "]");
                    sdr = xmlMapper.readValue(response, SubscriptionDavidResponse.class);
                } catch (Exception ex) {
                    log.error("Wau. getmany [" + current + "][" + request + "]. Response: [" + response + "]. " + ex, ex);
                }
            } while (current <= pages);
        } catch (Exception ex) {
            log.error("Wau. getmany [" + request + "]. Response: [" + response + "]. " + ex, ex);
        }

        return subs;
    }

}
