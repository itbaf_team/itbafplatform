package com.itbaf.platform.paymenthub.extention.tiaxa.soap;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.extention.tiaxa.handler.ServiceProcessorHandler;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import javax.ws.rs.PathParam;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;
import org.apache.commons.lang3.Validate;
import org.csapi.schema.parlayx.syncsubscription.v1_0.SyncOrderRelationship;

//@Path("/notification")
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationSoapService {

    private final XmlMapper xmlMapper;
    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public NotificationSoapService(final XmlMapper xmlMapper,
            final ServiceProcessorHandler serviceProcessorHandler) {
        this.xmlMapper = Validate.notNull(xmlMapper, "A XmlMapper class must be provided");
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
    }

    /*@POST
    @Path("/{country}/tiaxa/subscription")
    @Produces(MediaType.TEXT_XML)*/
    public void subscriptionNotification(final @PathParam("country") String countryCode, String soap) throws Exception {

        soap = soap.replace("\n", "").replace("\r", "");
        try {
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(soap.getBytes(Charset.forName("UTF-8"))));
            JAXBContext jbc = JAXBContext.newInstance(SyncOrderRelationship.class);
            Unmarshaller um = jbc.createUnmarshaller();
            JAXBElement<SyncOrderRelationship> element = um.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument(), SyncOrderRelationship.class);
            SyncOrderRelationship notification = element.getValue();
            log.info("Notificacion de Suscripcion Tiaxa. CountryCode [" + countryCode + "] - Notification: [" + notification + "]");
            serviceProcessorHandler.processNotification(countryCode, notification);
        } catch (Exception ex) {
            log.error("Error al interpretar SOAP. CountryCode [" + countryCode + "] - Subscription Notification: [" + soap + "]. " + ex, ex);
            throw new Exception("Error al interpretar SOAP. CountryCode [" + countryCode + "] - Subscription Notification: [" + soap + "]. " + ex);
        }

       /* String soapResponse = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body>"
                + "<ns2:syncOrderRelationshipResponse xmlns:ns2=\"http://www.csapi.org/schema/parlayx/syncsubscription/v1_0/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\">"
                + "<ns2:result><resultCode>00000000</resultCode><resultMessage>success</resultMessage></ns2:result>"
                + "</ns2:syncOrderRelationshipResponse></soap:Body></soap:Envelope>";
        return Response.status(Status.OK).entity(soapResponse).type(MediaType.TEXT_XML).build();*/
    }

    /*

    @GET
    @Path("/{country}/smt/mdivulga/subscription/status")
    @Produces(MediaType.TEXT_XML)
    public Response mDivulgaGetSubscriptionStatus(@Context HttpServletRequest request,
            final @PathParam("country") String countryCode,
            final @Context UriInfo uriInfo) throws Exception {

        log.info("SubscriptionStatus. m-Divulga. CountryCode [" + countryCode + "] - QueryString: [" + Arrays.toString(uriInfo.getQueryParameters().entrySet().toArray()) + "]");
        MDivulgaResponse xmlResponse = null;
        String basic = request.getHeader("Authorization");

        MDivulgaNotification sStatus = new MDivulgaNotification();
        sStatus.setMsisdn(uriInfo.getQueryParameters().getFirst("msisdn"));
        sStatus.setServicecode(uriInfo.getQueryParameters().getFirst("servicecode"));

        try {
            if (serviceProcessorHandler.mDivulgaIsAuthorized(basic)) {
                xmlResponse = serviceProcessorHandler.mDivulgaGetSubscriptionStatus(sStatus);
            }
        } catch (Exception ex) {
            log.error("Error al procesar SubscriptionStatus: [" + sStatus + "]. " + ex, ex);
            xmlResponse = new MDivulgaResponse();
            xmlResponse.status = "500";
            xmlResponse.description = Status.INTERNAL_SERVER_ERROR.toString();
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity(getXmlResponse(xmlResponse)).type(MediaType.TEXT_XML).build();
        }

        if (xmlResponse == null) {
            return Response.status(Status.UNAUTHORIZED).build();
        }

        return Response.status(Status.OK).entity(getXmlResponse(xmlResponse)).type(MediaType.TEXT_XML).build();
    }*/
    /**
     * Esto adiciona <?xml version='1.0' encoding='UTF-8'?> para no hardcodearlo
     * Sin este metodo, se puede hacer:
     * Response.status(Status.OK).entity(xmlResponse).type(MediaType.TEXT_XML).build()
     */
    /*  private String getXmlResponse(MDivulgaResponse xmlResponse) throws Exception {
        StringWriter stringWriter = new StringWriter();
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
        XMLStreamWriter sw = xmlOutputFactory.createXMLStreamWriter(stringWriter);

        sw.writeStartDocument();
        // sw.writeStartElement("root");
        xmlMapper.writeValue(sw, xmlResponse);
        // sw.writeEndElement();
        sw.writeEndDocument();
        return stringWriter.toString();
    }*/
}
