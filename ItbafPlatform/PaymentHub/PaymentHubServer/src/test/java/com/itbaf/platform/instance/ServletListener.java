/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.instance;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.servlet.GuiceServletContextListener;

/**
 *
 * @author jordonez
 */
public class ServletListener extends GuiceServletContextListener {

    private Injector injector;

    public ServletListener(Module... modules) {
        injector = Guice.createInjector(modules);
    }

    @Override
    protected Injector getInjector() {
        return injector;
    }

}
