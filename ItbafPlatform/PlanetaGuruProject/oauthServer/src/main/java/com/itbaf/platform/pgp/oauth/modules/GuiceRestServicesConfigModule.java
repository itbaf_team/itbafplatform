package com.itbaf.platform.pgp.oauth.modules;

import com.google.inject.Singleton;
import com.itbaf.platform.commons.filter.RequestFilter;
import com.itbaf.platform.pgp.commons.rest.ManagerRestServiceImpl;
import com.itbaf.platform.pgp.oauth.api.rest.AuthenticationRestService;
import com.itbaf.platform.pgp.oauth.api.rest.BasicAuthenticationRestService;
import com.itbaf.platform.pgp.oauth.api.rest.view.SignInViewAuthenticationRestService;
import com.itbaf.platform.pgp.oauth.api.rest.view.SignUpViewAuthenticationRestService;
import com.itbaf.platform.pgp.oauth.api.rest.view.LegalRestService;
import com.itbaf.platform.pgp.oauth.filter.BasicAuthenticationRequestFilter;
import com.itbaf.platform.pgp.services.rest.FileRestService;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import java.util.HashMap;

public class GuiceRestServicesConfigModule extends JerseyServletModule {

    @Override
    protected void configureServlets() {
        super.configureServlets();

        HashMap<String, String> options = new HashMap();
        options.put("com.sun.jersey.api.json.POJOMappingFeature", "true");

        //Filters
        filter("/*").through(RequestFilter.class);
        filter("/*").through(BasicAuthenticationRequestFilter.class);
        // filter("/auth/authorize/*").through(GzipRequestFilter.class);

        //Servlets
        serve("/rest/*").with(GuiceContainer.class, options);
        serve("/oauth/*").with(GuiceContainer.class, options);
        serve("/file/*").with(GuiceContainer.class, options);

        //Binds        
        bind(FileRestService.class).in(Singleton.class);
        bind(AuthenticationRestService.class).in(Singleton.class);
        bind(BasicAuthenticationRestService.class).in(Singleton.class);
        bind(ManagerRestServiceImpl.class).in(Singleton.class);
        bind(SignInViewAuthenticationRestService.class).in(Singleton.class);
        bind(SignUpViewAuthenticationRestService.class).in(Singleton.class);
        bind(LegalRestService.class).in(Singleton.class);
    }

}
