/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository;

import com.itbaf.platform.pgp.model.wallet.WalletUser;

/**
 *
 * @author javier
 */
public interface WalletUserService {

    public WalletUser findById(Long id);

    public void saveOrUpdate(WalletUser walletUser) throws Exception;
}
