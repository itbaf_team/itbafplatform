/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.bn.model.timwe;

import java.util.List;

/**
 *
 * @author JF
 */
public class PartnerNotifResponse {
    public List<String> responseData;
    public String message;
    public boolean inError;
    public String requestId;
    public String code;
}
