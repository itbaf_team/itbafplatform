/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository;

import com.itbaf.platform.paymenthub.model.Notification;


/**
 *
 * @author javier
 */
public interface NotificationService {
    public Notification saveNotification(Notification notification);
}
