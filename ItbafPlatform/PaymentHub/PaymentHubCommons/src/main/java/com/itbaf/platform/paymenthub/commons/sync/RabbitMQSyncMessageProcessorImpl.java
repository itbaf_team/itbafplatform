/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.commons.sync;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.resources.InstrumentedObject;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.rmq.consumer.RabbitMQMessageProcessor;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class RabbitMQSyncMessageProcessorImpl implements RabbitMQMessageProcessor {

    private final ObjectMapper mapper;
    private final ServiceProcessor serviceProcessor;
    private final InstrumentedObject instrumentedObject;

    @Inject
    public RabbitMQSyncMessageProcessorImpl(final ObjectMapper mapper,
            final ServiceProcessor serviceProcessor,
            final InstrumentedObject instrumentedObject) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.serviceProcessor = Validate.notNull(serviceProcessor, "A ServiceProcessor class must be provided");
        this.instrumentedObject = Validate.notNull(instrumentedObject, "An InstrumentedObject class must be provided");

    }

    @Override
    public Boolean processMessage(String message) {
        try {
            String tid = Thread.currentThread().getName() + ".sync";
            Thread.currentThread().setName(tid);
        } catch (Exception ex) {
        }

        try {
            Subscription s = mapper.readValue(message, Subscription.class);
            if (s.getSubscriptionRegistry() != null) {
                serviceProcessor.syncSubscription(s);
            } else {
                Provider provider = mapper.readValue(message, Provider.class);
                RLock lock = instrumentedObject.lockObject("SYNC_S_" + provider.getName(), 10);
                Long aux = instrumentedObject.getAtomicLong("SYNC_Sx_" + provider.getName(), 10);
                if (aux <= 1) {
                    instrumentedObject.getAndIncrementAtomicLong("SYNC_Sx_" + provider.getName(), 10);
                    instrumentedObject.getAndIncrementAtomicLong("SYNC_Sx_" + provider.getName(), 10);
                    instrumentedObject.getAndIncrementAtomicLong("SYNC_Sx_" + provider.getName(), 10);
                    instrumentedObject.getAndIncrementAtomicLong("SYNC_Sx_" + provider.getName(), 10);
                    serviceProcessor.syncSubscriptions(provider);
                }
                instrumentedObject.unLockObject(lock);
            }
        } catch (Exception ex) {
            log.error("Error procesando mensaje de SYNC: [" + message + "]. " + ex, ex);
        }
        return true;
    }

}
