/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.commons.cache;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class CacheMessage {

    public enum CacheMessageType {
        UPDATE
    }
    
    public CacheMessageType type;
    public String thread = Thread.currentThread().getName();

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("type", type)
                .add("thread", thread)
                .omitNullValues().toString();
    }
}
