/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.api.handler;

import com.google.inject.Inject;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.messaging.queue.wallet.CurrencyRequest;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.messaging.rest.pg.PGMessageRequest;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Language;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.OnDemandBilling;
import com.itbaf.platform.paymenthub.model.billing.SubscriptionBilling;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.ondemand.OnDemandRegistry;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.repository.OnDemandBillingService;
import com.itbaf.platform.paymenthub.services.repository.OnDemandRegistryService;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.SopService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionBillingService;
import com.itbaf.platform.paymenthub.services.repository.TariffService;
import com.itbaf.platform.pgp.model.Product;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.wallet.PaymentMethod;
import com.itbaf.platform.pgp.model.wallet.WalletTransaction;
import com.itbaf.platform.pgp.model.wallet.WalletUser;
import com.itbaf.platform.pgp.model.wallet.properties.Credit;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import com.itbaf.platform.pgp.services.repository.PaymentMethodService;
import com.itbaf.platform.pgp.services.repository.WalletTransactionService;
import io.jsonwebtoken.impl.DefaultClaims;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;
import org.hibernate.exception.ConstraintViolationException;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ApiWalletServiceProcessorHandler extends ServiceProcessorHandler {

    private final SopService sopService;
    private final TariffService tariffService;
    private final PaymentMethodService paymentMethodService;
    private final OnDemandBillingService onDemandBillingService;
    private final OnDemandRegistryService onDemandRegistryService;
    private final WalletTransactionService walletTransactionService;
    private final PHProfilePropertiesService phprofilePropertiesService;
    private final SubscriptionBillingService subscriptionBillingService;

    public static String TYC_URL = "https://oauth-st.planeta.guru/oauth/auth/tyc";

    @Inject
    public ApiWalletServiceProcessorHandler(final SopService sopService,
            final TariffService tariffService,
            final PaymentMethodService paymentMethodService,
            final OnDemandBillingService onDemandBillingService,
            final OnDemandRegistryService onDemandRegistryService,
            final WalletTransactionService walletTransactionService,
            final PHProfilePropertiesService phprofilePropertiesService,
            final SubscriptionBillingService subscriptionBillingService) {
        this.sopService = Validate.notNull(sopService, "A SopService class must be provided");
        this.tariffService = Validate.notNull(tariffService, "A TariffService class must be provided");
        this.paymentMethodService = Validate.notNull(paymentMethodService, "A PaymentMethodService class must be provided");
        this.onDemandBillingService = Validate.notNull(onDemandBillingService, "An OnDemandBillingService class must be provided");
        this.onDemandRegistryService = Validate.notNull(onDemandRegistryService, "An OnDemandRegistryService class must be provided");
        this.walletTransactionService = Validate.notNull(walletTransactionService, "A WalletTransactionService class must be provided");
        this.phprofilePropertiesService = Validate.notNull(phprofilePropertiesService, "A PHProfilePropertiesService class must be provided");
        this.subscriptionBillingService = Validate.notNull(subscriptionBillingService, "A SubscriptionBillingService class must be provided");
    }

    public List<WalletTransaction> getWalletTransaction(User user, Date from, Date to) {
        return walletTransactionService.getWalletTransaction(user, from, to);
    }

    private User getPrincipalUser(CurrencyRequest cr, User a, User b) {
        User aa = a;
        User bb = b;

        while (aa.getPrincipal() != null) {
            aa = aa.getPrincipal();
        }
        while (bb.getPrincipal() != null) {
            bb = bb.getPrincipal();
        }

        if (!aa.getId().equals(bb.getId())) {
            log.error("Hay dos usuarios reclamando la propiedad de un cobro. Se procede a realizar un merge: [" + cr + "] - [" + a + "] - [" + b + "]");
        }

        List<User> aux = new ArrayList();
        aux.add(aa);
        aux.add(bb);
        a = super.funUsers(aux);
        return a;
    }

    public User getUserById(String userId) {
        return userService.findById(Long.parseLong(userId));
    }

    public WalletTransaction walletTransactionCreate(CurrencyRequest cr, User user, User client, WalletTransaction.Type type) throws Exception {
        RLock lock = instrumentedObject.lockObject("walletTransactionCreate_" + cr.transactionId, 30);
        try {
            if (user == null) {
                if (cr.tracking != null) {
                    try {
                        LinkedHashMap<String, String> lhm = (LinkedHashMap<String, String>) cr.tracking;
                        //Esto puede causar problemas... Un user con ID de otro.. 
                        String userId = lhm.get("userId");
                        if (userId != null) {
                            user = userService.findById(Long.parseLong(userId));
                            if (user != null) {
                                try {
                                    if (cr.userAccount.contains("@") && user.getCredentialUserProperty().getUsername().getEmail(cr.userAccount) == null) {
                                        User xuser = userService.findByEmail(cr.userAccount);
                                        if (xuser != null && !xuser.getId().equals(user.getId())) {
                                            user = getPrincipalUser(cr, user, xuser);
                                        } else if (xuser == null) {
                                            log.info("Se procede a adicionar el userAccount: [" + cr.userAccount + "] al User: [" + user.getId() + "]");
                                            user.getCredentialUserProperty().getUsername().setEmail(new Email(cr.userAccount, Boolean.TRUE));
                                            userService.saveOrUpdate(user);
                                        }
                                    } else if (user.getCredentialUserProperty().getUsername().getMsisdn(cr.userAccount) == null && isNumber(cr.userAccount)) {
                                        User xuser = userService.findByMsisdn(cr.userAccount);
                                        if (xuser != null && !xuser.getId().equals(user.getId())) {
                                            user = getPrincipalUser(cr, user, xuser);
                                        } else if (xuser == null) {
                                            log.info("Se procede a adicionar el userAccount: [" + cr.userAccount + "] al User: [" + user.getId() + "]");
                                            SOP sop = sopService.findById(cr.sopId);
                                            user.getCredentialUserProperty().getUsername()
                                                    .setMsisdn(new Msisdn(cr.userAccount, Boolean.TRUE, sop.getProvider().getCountry(), sop.getProvider()));
                                            userService.saveOrUpdate(user);
                                        }
                                    } else {
                                        user = null;
                                    }
                                } catch (Exception ex) {
                                    log.error("Error al adicionar el userAccount: [" + cr.userAccount + "] al User: [" + user + "]. " + ex, ex);
                                }
                            }
                        }
                    } catch (Exception ex) {
                        log.error("Error al determinar usuario 1. " + ex, ex);
                    }
                }
                if (user == null) {
                    if (cr.userAccount.contains("@")) {
                        user = userService.findByEmail(cr.userAccount);
                        if (user != null && !user.getCredentialUserProperty().getUsername().getEmail(cr.userAccount).getConfirmed()) {
                            log.info("El user: [" + cr.userAccount + "] no ha sido confirmado. No se registraran creditos. [" + cr + "]");
                            return null;
                        }
                    } else if (isNumber(cr.userAccount)) {
                        try {
                            Country c = countryService.findById(cr.countryId);

                            String localLength = phprofilePropertiesService.getCommonProperty("msisdn."
                                    + c.getCode().toLowerCase() + ".length.local");

                            Integer msisdnLengthLocal = Integer.parseInt(localLength);
                            String msisdn = cr.userAccount;
                            msisdn = msisdn.replace(" ", "").replace("+", "");
                            msisdn = msisdn.substring(Math.max(0, msisdn.length() - msisdnLengthLocal), msisdn.length());
                            Holder<String> username = new Holder();
                            user = userService.findByMsisdn(c.getPhoneCode().toString(), msisdn, username);
                            if (user != null && !cr.userAccount.equals(username.value)) {
                                //Actualizamos el MSISDN en el usuario
                                Msisdn m = user.getCredentialUserProperty().getUsername().getMsisdns().remove(username.value);
                                m.setMsisdn(cr.userAccount);
                                if (cr.sopId != null) {
                                    SOP sop = sopService.findById(cr.sopId);
                                    m.setProvider(sop.getProvider());
                                    m.setCountry(sop.getProvider().getCountry());
                                }
                                user.getCredentialUserProperty().getUsername().setMsisdn(m);
                                userService.saveOrUpdate(user);
                            }
                        } catch (Exception ex) {
                            log.error("Error al determinar usuario 2. " + ex, ex);
                        }
                        if (user != null && !user.getCredentialUserProperty().getUsername().getMsisdn(cr.userAccount).getConfirmed()) {
                            log.info("El user: [" + cr.userAccount + "] no ha sido confirmado. No se registraran creditos. [" + cr + "]");
                            return null;
                        }
                    }
                }
            }

            if (user == null) {
                log.info("No hay un usuario creado para el userAccount: [" + cr.userAccount + "]");
                return null;
            }

            WalletTransaction wt = new WalletTransaction();
            wt.setAmount(cr.amount);
            wt.setTid(cr.transactionId);
            wt.setType(type);
            wt.setUserAccount(cr.userAccount);
            wt.setUser(user);
            wt.setTracking(cr.tracking);
            wt.setDescription(cr.description);
            wt.setChargedDate(cr.date);

            if (cr.localAmount == null && cr.tariffId != null) {
                Tariff t = tariffService.findById(cr.tariffId);
                if (t != null) {
                    cr.localAmount = t.getLocalAmount();
                    cr.localCurrency = t.getLocalCurrency();
                    if (cr.currency == null) {
                        cr.currency = t.getCurrency();
                    }
                }
            }

            //Redondeamos a favor del Usuario
            if (cr.localAmount != null) {
                cr.localAmount = cr.localAmount.setScale(0, RoundingMode.UP);
            }

            wt.setCurrency(cr.currency);
            if (cr.sopId != null) {
                wt.setSop(sopService.findById(cr.sopId));
            }
            wt.setClient(client);
            wt.setLocalAmount(cr.localAmount);
            wt.setLocalCurrency(cr.localCurrency);
            wt.setLocalCurrency(wt.getLocalCurrency().toUpperCase());

            try {
                walletTransactionService.saveOrUpdate(wt);
            } catch (ConstraintViolationException | SQLIntegrityConstraintViolationException ex) {
                log.warn("Registro duplicado. [" + wt + "]. " + ex);
                return null;
            } catch (Exception ex) {
                boolean control = true;
                try {
                    if (ex.getCause() instanceof ConstraintViolationException) {
                        control = false;
                        log.warn("Registro duplicado. [" + wt + "]. " + ex.getCause().getCause());
                    }
                } catch (Exception exx) {
                }
                if (control) {
                    log.error("Error al procesar notificacion de cobro: [" + wt + "]. " + ex, ex);
                    throw new Exception(ex);
                } else {
                    return null;
                }
            }
            return wt;
        } finally {
            instrumentedObject.unLockObject(lock);
        }
    }

    public Boolean processWalletTransaction(WalletTransaction wt) {
        return setWalletFunds(wt.getUser(), wt.getType(), wt.getLocalCurrency(), wt.getLocalAmount(), wt.getSop(), wt.getClient());
    }

/*
    public Map<String, BigDecimal> getWalletFundsNew(User client, User user) {

        //  log.info("getWalletFunds. ClientId: [" + client.getId() + "] UserId: [" + user.getId() + "]");
        if (client.getPrincipal() != null)
            client = client.getPrincipal();

        WalletUser wu = null;
        try {
            wu = walleUserService.findById(user.getId());
            if (wu == null) {
                //  log.info("No existe un WalletUser: [" + user.getId() + "]");

                //Buscamos si existen cobros para este usuario
                if (hasEmail(user) )
                    processFundsByEmail(client, user);

                if (hasMsisdns(user))
                    processFundsByMsisdns(client, user);

                wu = walleUserService.findById(user.getId());
                if (wu == null)
                    wu = createWalletUser(client, user);

            } else {
                //Buscamos cobros de ultimo momento...
                RLock lock = instrumentedObject.lockObject("USER_SYNC_FUNDS_" + user.getId(), 10);
                long auxAt = instrumentedObject.getAtomicLong("USER_SYNC_FUNDS_A_" + user.getId(), 5);

                if (auxAt == 0) {
                    instrumentedObject.getAndIncrementAtomicLong("USER_SYNC_FUNDS_A_" + user.getId(), 5);
                    instrumentedObject.getAndIncrementAtomicLong("USER_SYNC_FUNDS_A_" + user.getId(), 5);

                    if (hasMsisdns(user))
                        processFundsByMsisdns(client, user, false);
                }

                instrumentedObject.unLockObject(lock);
                wu = walleUserService.findById(user.getId());
                // log.info("WalletUser: [" + wu + "]");
            }

            Map<String, BigDecimal> credits = this.getCredits(user, client);
            log.info("Creditos: [" + credits + "] - WalletUser: [" + wu + "]");
            return credits;

        } catch (Exception ex) {
            log.error("Error al procesar getWalletFunds. User: [" + user + "] - Client: [" + client + "]. " + ex, ex);

            Map<String, BigDecimal> credits = new HashMap();
            credits.put("GUR", new BigDecimal(BigInteger.ZERO));
            return credits;
        }
    }
*/

    public Map<String, BigDecimal> getWalletFunds(User client, User user) {

        //  log.info("getWalletFunds. ClientId: [" + client.getId() + "] UserId: [" + user.getId() + "]");
        if (client.getPrincipal() != null) {
            client = client.getPrincipal();
        }

        Map<String, BigDecimal> credits = new HashMap();
        credits.put("GUR", new BigDecimal(BigInteger.ZERO));

        WalletUser wu = null;
        try {
            wu = walleUserService.findById(user.getId());
            if (wu == null) {
                //  log.info("No existe un WalletUser: [" + user.getId() + "]");

                //Buscamos si existen cobros para este usuario
                if (user.getCredentialUserProperty() != null && user.getCredentialUserProperty().getUsername() != null
                        && user.getCredentialUserProperty().getUsername().getEmails() != null && !user.getCredentialUserProperty().getUsername().getEmails().isEmpty()) {
                    Set<String> keys = user.getCredentialUserProperty().getUsername().getEmails().keySet();
                    for (String email : keys) {
                        if (!user.getCredentialUserProperty().getUsername().getEmails().get(email).getConfirmed()) {
                            continue;
                        }
                        try {
                            List<SubscriptionBilling> list = subscriptionBillingService.getSubscriptionBillingsByUserAccount(email);
                            if (list != null && !list.isEmpty()) {
                                //   log.info("Se han encontrado [" + list.size() + "] cobros para [" + email + "]");
                                for (SubscriptionBilling sb : list) {
                                    CurrencyRequest cr = new CurrencyRequest();
                                    cr.currency = sb.getCurrency();
                                    cr.amount = sb.getFullAmount();
                                    cr.countryId = sb.getSop().getProvider().getCountry().getId();
                                    cr.date = sb.getChargedDate();
                                    cr.sopId = sb.getSop().getId();
                                    cr.transactionId = "PH_SB_" + sb.getId();
                                    cr.userAccount = email;
                                    cr.tracking = sb.getSubscriptionRegistry().getAdnetworkTracking();
                                    Tariff t = sb.getSop().findTariffByFullAmount(sb.getFullAmount());
                                    if (t == null) {
                                        t = sb.getSop().getMainTariff();
                                        BigDecimal aux = (sb.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
                                        cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
                                        cr.localCurrency = t.getLocalCurrency();
                                    } else {
                                        cr.localAmount = t.getLocalAmount();
                                        cr.localCurrency = t.getLocalCurrency();
                                        cr.tariffId = t.getId();
                                    }
                                    try {
                                        WalletTransaction wt = walletTransactionCreate(cr, user, client, WalletTransaction.Type.ADD);
                                        if (wt != null) {
                                            processWalletTransaction(wt);
                                        }
                                    } catch (Exception ex) {
                                        log.warn("Error al registrar cobro. " + ex);
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al procesar cobros por suscripcion para: [" + email + "]. " + ex);
                        }

                        try {
                            List<OnDemandBilling> list = onDemandBillingService.getByUserAccount(email);
                            if (list != null && !list.isEmpty()) {
                                // log.info("Se han encontrado [" + list.size() + "] cobros para [" + email + "]");
                                for (OnDemandBilling ob : list) {
                                    CurrencyRequest cr = new CurrencyRequest();
                                    cr.currency = ob.getCurrency();
                                    cr.amount = ob.getFullAmount();
                                    cr.countryId = ob.getSop().getProvider().getCountry().getId();
                                    cr.date = ob.getChargedDate();
                                    cr.sopId = ob.getSop().getId();
                                    cr.transactionId = "PH_OB_" + ob.getId();
                                    cr.userAccount = email;
                                    OnDemandRegistry or = onDemandRegistryService.findById(ob.getId());
                                    if (or != null && or.getTransactionTracking() != null) {
                                        cr.tracking = or.getTransactionTracking().adTracking;
                                        cr.description = or.getTransactionTracking().contentName;
                                    }
                                    Tariff t = ob.getSop().findTariffByFullAmount(ob.getFullAmount());
                                    if (t == null) {
                                        t = ob.getSop().getMainTariff();
                                        BigDecimal aux = (ob.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
                                        cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
                                        cr.localCurrency = t.getLocalCurrency();
                                    } else {
                                        cr.localAmount = t.getLocalAmount();
                                        cr.localCurrency = t.getLocalCurrency();
                                        cr.tariffId = t.getId();
                                    }
                                    try {
                                        WalletTransaction wt = walletTransactionCreate(cr, user, client, WalletTransaction.Type.ADD);
                                        if (wt != null) {
                                            processWalletTransaction(wt);
                                        }
                                    } catch (Exception ex) {
                                        log.warn("Error al registrar cobro. " + ex);
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al procesar cobros por on demand para: [" + email + "]. " + ex);
                        }
                    }
                }
                if (user.getCredentialUserProperty() != null && user.getCredentialUserProperty().getUsername() != null
                        && user.getCredentialUserProperty().getUsername().getMsisdns() != null && !user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                    Set<String> keys = user.getCredentialUserProperty().getUsername().getMsisdns().keySet();
                    for (String msisdn : keys) {
                        Msisdn m = user.getCredentialUserProperty().getUsername().getMsisdns().get(msisdn);
                        if (!m.getConfirmed()) {
                            continue;
                        }
                        try {
                            Subscription s = subscriptionService.getSubscriptionInformationFacadeRequest(msisdn, m.getCountry().getCode());
                            if (s != null && s.getSubscriptionRegistry() != null) {
                                //   log.info("Suscripcion en PaymentHUb encontrada: [" + (s.getId() == null ? s : s.getId()) + "]");
                                List<SubscriptionBilling> list = subscriptionBillingService.getSubscriptionBillingsByUserAccount(s.getUserAccount());
                                if (list != null && !list.isEmpty()) {
                                    //   log.info("Se han encontrado [" + list.size() + "] cobros para [" + s.getUserAccount() + "]");
                                    for (SubscriptionBilling sb : list) {
                                        CurrencyRequest cr = new CurrencyRequest();
                                        cr.currency = sb.getCurrency();
                                        cr.amount = sb.getFullAmount();
                                        cr.countryId = sb.getSop().getProvider().getCountry().getId();
                                        cr.date = sb.getChargedDate();
                                        cr.sopId = sb.getSop().getId();
                                        cr.transactionId = "PH_SB_" + sb.getId();
                                        cr.userAccount = s.getUserAccount();
                                        cr.tracking = sb.getSubscriptionRegistry().getAdnetworkTracking();
                                        Tariff t = sb.getSop().findTariffByFullAmount(sb.getFullAmount());
                                        if (t == null) {
                                            t = sb.getSop().getMainTariff();
                                            BigDecimal aux = (sb.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
                                            cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
                                            cr.localCurrency = t.getLocalCurrency();
                                        } else {
                                            cr.localAmount = t.getLocalAmount();
                                            cr.localCurrency = t.getLocalCurrency();
                                            cr.tariffId = t.getId();
                                        }
                                        try {
                                            WalletTransaction wt = walletTransactionCreate(cr, user, client, WalletTransaction.Type.ADD);
                                            if (wt != null) {
                                                processWalletTransaction(wt);
                                            }
                                        } catch (Exception ex) {
                                            log.warn("Error al registrar cobro. " + ex);
                                        }

                                        try {
                                            if (m.getProvider() == null || !sb.getSop().getProvider().getId().equals(m.getProvider().getId())) {
                                                m.setProvider(sb.getSop().getProvider());
                                                userService.saveOrUpdate(user);
                                            }
                                        } catch (Exception ex) {
                                        }
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al procesar cobros por suscripcion para: [" + msisdn + "]. " + ex);
                        }
                        try {
                            String localLength = phprofilePropertiesService.getCommonProperty("msisdn." + m.getCountry().getCode().toLowerCase() + ".length.local");
                            Integer msisdnLengthLocal = Integer.parseInt(localLength);
                            msisdn = msisdn.replace(" ", "").replace("+", "");
                            msisdn = msisdn.substring(Math.max(0, msisdn.length() - msisdnLengthLocal), msisdn.length());

                            List<OnDemandBilling> list = onDemandBillingService.getByUserAccount(msisdn, m.getCountry());
                            if (list != null && !list.isEmpty()) {
                                //  log.info("Se han encontrado [" + list.size() + "] cobros on demand para [" + m.getMsisdn() + "]");
                                for (OnDemandBilling ob : list) {
                                    CurrencyRequest cr = new CurrencyRequest();
                                    cr.currency = ob.getCurrency();
                                    cr.amount = ob.getFullAmount();
                                    cr.countryId = ob.getSop().getProvider().getCountry().getId();
                                    cr.date = ob.getChargedDate();
                                    cr.sopId = ob.getSop().getId();
                                    cr.transactionId = "PH_OB_" + ob.getId();
                                    cr.userAccount = ob.getUserAccount();

                                    Tariff t = ob.getSop().findTariffByFullAmount(ob.getFullAmount());
                                    if (t == null) {
                                        t = ob.getSop().getMainTariff();
                                        BigDecimal aux = (ob.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
                                        cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
                                        cr.localCurrency = t.getLocalCurrency();
                                    } else {
                                        cr.localAmount = t.getLocalAmount();
                                        cr.localCurrency = t.getLocalCurrency();
                                        cr.tariffId = t.getId();
                                    }

                                    OnDemandRegistry or = onDemandRegistryService.findById(ob.getId());
                                    if (or != null && or.getTransactionTracking() != null) {
                                        cr.tracking = or.getTransactionTracking().adTracking;
                                        cr.description = or.getTransactionTracking().contentName;
                                    }

                                    try {
                                        if (!m.getMsisdn().equals(ob.getUserAccount())) {
                                            log.fatal("Hey!! estos MSISDNs son ligeramente diferentes: [" + m.getMsisdn() + "] [" + ob.getUserAccount() + "]");
                                            //si ocurre el error ver de actualizar el MSISDN
                                        }
                                    } catch (Exception ex) {
                                    }

                                    try {
                                        if (m.getProvider() == null || !ob.getSop().getProvider().getName().equals(m.getProvider().getName())) {
                                            m.setCountry(ob.getSop().getProvider().getCountry());
                                            m.setProvider(ob.getSop().getProvider());
                                            userService.saveOrUpdate(user);
                                        }
                                    } catch (Exception ex) {
                                    }

                                    try {
                                        WalletTransaction wt = walletTransactionCreate(cr, user, client, WalletTransaction.Type.ADD);
                                        if (wt != null) {
                                            processWalletTransaction(wt);
                                        }
                                    } catch (Exception ex) {
                                        log.warn("Error al registrar cobro. " + ex);
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al procesar cobros por on demand para: [" + m.getMsisdn() + "]. " + ex);
                        }
                    }
                }
                wu = walleUserService.findById(user.getId());
                if (wu == null) {
                    //  log.info("Realmente no existe un WalletUser: [" + user.getId() + "]");
                    wu = new WalletUser();
                    wu.setId(user.getId());
                    walleUserService.saveOrUpdate(wu);

                    CurrencyRequest cr = new CurrencyRequest();
                    cr.date = new Date();
                    cr.localAmount = BigDecimal.valueOf(Long.parseLong(genericPropertyService.getCommonProperty("login.credits.start.value")));
                    cr.localCurrency = genericPropertyService.getCommonProperty("login.credits.start.currency");
                    cr.transactionId = "CLIENT_LOGIN_START_" + client.getId() + "_" + user.getId();
                    cr.userAccount = "" + user.getId();

                    if (BigDecimal.ZERO.compareTo(cr.localAmount) < 0) {
                        String i18n = "es";
                        if (user.getProfileUserProperty().getCountry() != null) {
                            Country c = countryService.findById(user.getProfileUserProperty().getCountry().getId());
                            if (c.getLanguages().size() > 1) {
                                Set<Language> list = new TreeSet();
                                list.addAll(c.getLanguages());
                                i18n = list.iterator().next().getCode().toLowerCase().trim() + "-" + c.getCode().toUpperCase().trim();
                            } else {
                                i18n = c.getLanguages().iterator().next().getCode() + "-" + c.getCode().toUpperCase().trim();
                            }
                        }
                        cr.description = i18n(i18n, null, "<i18n>i18n.ix.px.text.firts.login.bonus</i18n>");

                        WalletTransaction wt = this.walletTransactionCreate(cr, user, client, WalletTransaction.Type.ADD);
                        processWalletTransaction(wt);
                    }
                    wu = walleUserService.findById(user.getId());
                }
            } else {
                //Buscamos cobros de ultimo momento...
                RLock lock = instrumentedObject.lockObject("USER_SYNC_FUNDS_" + user.getId(), 10);
                long auxAt = instrumentedObject.getAtomicLong("USER_SYNC_FUNDS_A_" + user.getId(), 5);
                if (auxAt == 0) {
                    instrumentedObject.getAndIncrementAtomicLong("USER_SYNC_FUNDS_A_" + user.getId(), 5);
                    instrumentedObject.getAndIncrementAtomicLong("USER_SYNC_FUNDS_A_" + user.getId(), 5);
                    if (user.getCredentialUserProperty() != null && user.getCredentialUserProperty().getUsername() != null
                            && user.getCredentialUserProperty().getUsername().getMsisdns() != null && !user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                        Set<String> keys = user.getCredentialUserProperty().getUsername().getMsisdns().keySet();
                        syncSubscriptionsRun(user);
                        for (String msisdn : keys) {
                            Msisdn m = user.getCredentialUserProperty().getUsername().getMsisdns().get(msisdn);
                            if (!m.getConfirmed()) {
                                continue;
                            }
                            Calendar c = Calendar.getInstance();
                            c.add(Calendar.DATE, -1);
                            try {
                                Subscription s = subscriptionService.findByMSISDNAndCountry(msisdn, m.getCountry());
                                if (s != null && s.getSubscriptionRegistry() != null) {
                                    //  log.info("Suscripcion en PaymentHUb DB encontrada: [" + s.getId() + "]");

                                    List<SubscriptionBilling> list = subscriptionBillingService.getSubscriptionBillingsByUserAccount(c.getTime(), s.getUserAccount());
                                    if (list != null && !list.isEmpty()) {
                                        //  log.info("Se han encontrado [" + list.size() + "] cobros para [" + s.getUserAccount() + "]");
                                        for (SubscriptionBilling sb : list) {
                                            CurrencyRequest cr = new CurrencyRequest();
                                            cr.currency = sb.getCurrency();
                                            cr.amount = sb.getFullAmount();
                                            cr.countryId = sb.getSop().getProvider().getCountry().getId();
                                            cr.date = sb.getChargedDate();
                                            cr.sopId = sb.getSop().getId();
                                            cr.transactionId = "PH_SB_" + sb.getId();
                                            cr.userAccount = s.getUserAccount();
                                            cr.tracking = sb.getSubscriptionRegistry().getAdnetworkTracking();

                                            Tariff t = sb.getSop().findTariffByFullAmount(sb.getFullAmount());
                                            if (t == null) {
                                                t = sb.getSop().getMainTariff();
                                                BigDecimal aux = (sb.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
                                                cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
                                                cr.localCurrency = t.getLocalCurrency();
                                            } else {
                                                cr.localAmount = t.getLocalAmount();
                                                cr.localCurrency = t.getLocalCurrency();
                                                cr.tariffId = t.getId();
                                            }
                                            try {
                                                WalletTransaction wt = walletTransactionCreate(cr, user, client, WalletTransaction.Type.ADD);
                                                if (wt != null) {
                                                    processWalletTransaction(wt);
                                                }
                                            } catch (Exception ex) {
                                                log.warn("Error al registrar cobro. " + ex);
                                            }
                                            try {
                                                if (m.getProvider() == null || !sb.getSop().getProvider().getId().equals(m.getProvider().getId())) {
                                                    m.setProvider(sb.getSop().getProvider());
                                                    userService.saveOrUpdate(user);
                                                }
                                            } catch (Exception ex) {
                                            }
                                        }
                                    }
                                }
                            } catch (Exception ex) {
                                log.error("Error al procesar cobros por suscripcion para: [" + msisdn + "]. " + ex, ex);
                            }
                            try {
                                String localLength = phprofilePropertiesService.getCommonProperty("msisdn." + m.getCountry().getCode().toLowerCase() + ".length.local");
                                Integer msisdnLengthLocal = Integer.parseInt(localLength);
                                msisdn = msisdn.replace(" ", "").replace("+", "");
                                msisdn = msisdn.substring(Math.max(0, msisdn.length() - msisdnLengthLocal), msisdn.length());

                                List<OnDemandBilling> list = onDemandBillingService.getByUserAccount(c.getTime(), msisdn, m.getCountry());
                                if (list != null && !list.isEmpty()) {
                                    // log.info("Se han encontrado [" + list.size() + "] cobros on demand para [" + m.getMsisdn() + "]");
                                    for (OnDemandBilling ob : list) {
                                        CurrencyRequest cr = new CurrencyRequest();
                                        cr.currency = ob.getCurrency();
                                        cr.amount = ob.getFullAmount();
                                        cr.countryId = ob.getSop().getProvider().getCountry().getId();
                                        cr.date = ob.getChargedDate();
                                        cr.sopId = ob.getSop().getId();
                                        cr.transactionId = "PH_OB_" + ob.getId();
                                        cr.userAccount = ob.getUserAccount();
                                        OnDemandRegistry or = onDemandRegistryService.findById(ob.getId());
                                        if (or != null && or.getTransactionTracking() != null) {
                                            cr.tracking = or.getTransactionTracking().adTracking;
                                            cr.description = or.getTransactionTracking().contentName;
                                        }
                                        Tariff t = ob.getSop().findTariffByFullAmount(ob.getFullAmount());
                                        if (t == null) {
                                            t = ob.getSop().getMainTariff();
                                            BigDecimal aux = (ob.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
                                            cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
                                            cr.localCurrency = t.getLocalCurrency();
                                        } else {
                                            cr.localAmount = t.getLocalAmount();
                                            cr.localCurrency = t.getLocalCurrency();
                                            cr.tariffId = t.getId();
                                        }
                                        try {
                                            if (!m.getMsisdn().equals(ob.getUserAccount())) {
                                                log.fatal("Hey!! estos MSISDNs son ligeramente diferentes: [" + m.getMsisdn() + "] [" + ob.getUserAccount() + "]");
                                                //Si ocurre esto hay que ver de actualizar el MSISDN
                                            }
                                        } catch (Exception ex) {
                                        }

                                        try {
                                            if (m.getProvider() == null || !ob.getSop().getProvider().getName().equals(m.getProvider().getName())) {
                                                m.setCountry(ob.getSop().getProvider().getCountry());
                                                m.setProvider(ob.getSop().getProvider());
                                                userService.saveOrUpdate(user);
                                            }
                                        } catch (Exception ex) {
                                        }

                                        try {
                                            WalletTransaction wt = walletTransactionCreate(cr, user, client, WalletTransaction.Type.ADD);
                                            if (wt != null) {
                                                processWalletTransaction(wt);
                                            }
                                        } catch (Exception ex) {
                                            log.warn("Error al registrar cobro. " + ex);
                                        }
                                    }
                                }
                            } catch (Exception ex) {
                                log.error("Error al procesar cobros por on demand para: [" + m.getMsisdn() + "]. " + ex);
                            }
                        }
                    }
                }
                instrumentedObject.unLockObject(lock);
                wu = walleUserService.findById(user.getId());
                // log.info("WalletUser: [" + wu + "]");
            }

            //Sumamos los creditos del usuario con base en los SOPs del producto y el ID del cliente
            for (Product p : client.getProducts()) {
                for (SOP sop : p.getSops()) {
                    Credit c = wu.getCredits().get("SOP_" + sop.getId());
                    if (c != null) {
                        Set<String> keys = c.getValues().keySet();
                        for (String currency : keys) {
                            BigDecimal bd = credits.get(currency);
                            if (bd == null) {
                                bd = new BigDecimal(BigInteger.ZERO);
                            }
                            bd = bd.add(c.getValues().get(currency));
                            credits.put(currency, bd);
                        }
                    }
                }
            }
            Credit c = wu.getCredits().get("CLIENT_" + client.getId());
            if (c != null) {
                Set<String> keys = c.getValues().keySet();
                for (String currency : keys) {
                    BigDecimal bd = credits.get(currency);
                    if (bd == null) {
                        bd = new BigDecimal(BigInteger.ZERO);
                    }
                    bd = bd.add(c.getValues().get(currency));
                    credits.put(currency, bd);
                }
            }

        } catch (Exception ex) {
            log.error("Error al procesar getWalletFunds. User: [" + user + "] - Client: [" + client + "]. " + ex, ex);
        }
        log.info("Creditos: [" + credits + "] - WalletUser: [" + wu + "]");

        return credits;
    }

    public Map<String, BigDecimal> getWalletFunds(User user) {

        //  log.info("getWalletFunds. User id: [" + user.getId() + "]");
        Map<String, BigDecimal> credits = new HashMap();
        credits.put("GUR", new BigDecimal(BigInteger.ZERO));

        WalletUser wu = null;
        try {
            wu = walleUserService.findById(user.getId());
            if (wu == null) {
                // log.info("No existe un WalletUser: [" + user.getId() + "]");

                //Buscamos si existen cobros para este usuario
                if (user.getCredentialUserProperty() != null && user.getCredentialUserProperty().getUsername() != null
                        && user.getCredentialUserProperty().getUsername().getEmails() != null && !user.getCredentialUserProperty().getUsername().getEmails().isEmpty()) {
                    Set<String> keys = user.getCredentialUserProperty().getUsername().getEmails().keySet();
                    for (String email : keys) {
                        if (!user.getCredentialUserProperty().getUsername().getEmails().get(email).getConfirmed()) {
                            continue;
                        }
                        try {
                            List<SubscriptionBilling> list = subscriptionBillingService.getSubscriptionBillingsByUserAccount(email);
                            if (list != null && !list.isEmpty()) {
                                //  log.info("Se han encontrado [" + list.size() + "] cobros para [" + email + "]");
                                for (SubscriptionBilling sb : list) {
                                    CurrencyRequest cr = new CurrencyRequest();
                                    cr.currency = sb.getCurrency();
                                    cr.amount = sb.getFullAmount();
                                    cr.countryId = sb.getSop().getProvider().getCountry().getId();
                                    cr.date = sb.getChargedDate();
                                    cr.sopId = sb.getSop().getId();
                                    cr.transactionId = "PH_SB_" + sb.getId();
                                    cr.userAccount = email;
                                    cr.tracking = sb.getSubscriptionRegistry().getAdnetworkTracking();
                                    Tariff t = sb.getSop().findTariffByFullAmount(sb.getFullAmount());
                                    if (t == null) {
                                        t = sb.getSop().getMainTariff();
                                        BigDecimal aux = (sb.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
                                        cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
                                        cr.localCurrency = t.getLocalCurrency();
                                    } else {
                                        cr.localAmount = t.getLocalAmount();
                                        cr.localCurrency = t.getLocalCurrency();
                                        cr.tariffId = t.getId();
                                    }
                                    try {
                                        WalletTransaction wt = walletTransactionCreate(cr, user, null, WalletTransaction.Type.ADD);
                                        if (wt != null) {
                                            processWalletTransaction(wt);
                                        }
                                    } catch (Exception ex) {
                                        log.warn("Error al registrar cobro. " + ex);
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al procesar cobros por suscripcion para: [" + email + "]. " + ex, ex);
                        }

                        try {
                            List<OnDemandBilling> list = onDemandBillingService.getByUserAccount(email);
                            if (list != null && !list.isEmpty()) {
                                //  log.info("Se han encontrado [" + list.size() + "] cobros para [" + email + "]");
                                for (OnDemandBilling ob : list) {
                                    CurrencyRequest cr = new CurrencyRequest();
                                    cr.currency = ob.getCurrency();
                                    cr.amount = ob.getFullAmount();
                                    cr.countryId = ob.getSop().getProvider().getCountry().getId();
                                    cr.date = ob.getChargedDate();
                                    cr.sopId = ob.getSop().getId();
                                    cr.transactionId = "PH_OB_" + ob.getId();
                                    cr.userAccount = email;
                                    Tariff t = ob.getSop().findTariffByFullAmount(ob.getFullAmount());
                                    if (t == null) {
                                        t = ob.getSop().getMainTariff();
                                        BigDecimal aux = (ob.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
                                        cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
                                        cr.localCurrency = t.getLocalCurrency();
                                    } else {
                                        cr.localAmount = t.getLocalAmount();
                                        cr.localCurrency = t.getLocalCurrency();
                                        cr.tariffId = t.getId();
                                    }
                                    OnDemandRegistry or = onDemandRegistryService.findById(ob.getId());
                                    if (or != null && or.getTransactionTracking() != null) {
                                        cr.tracking = or.getTransactionTracking().adTracking;
                                        cr.description = or.getTransactionTracking().contentName;
                                    }
                                    try {
                                        WalletTransaction wt = walletTransactionCreate(cr, user, null, WalletTransaction.Type.ADD);
                                        if (wt != null) {
                                            processWalletTransaction(wt);
                                        }
                                    } catch (Exception ex) {
                                        log.warn("Error al registrar cobro. " + ex);
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al procesar cobros por on demand para: [" + email + "]. " + ex, ex);
                        }
                    }
                }
                if (user.getCredentialUserProperty() != null && user.getCredentialUserProperty().getUsername() != null
                        && user.getCredentialUserProperty().getUsername().getMsisdns() != null && !user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                    Set<String> keys = user.getCredentialUserProperty().getUsername().getMsisdns().keySet();
                    for (String msisdn : keys) {
                        Msisdn m = user.getCredentialUserProperty().getUsername().getMsisdns().get(msisdn);
                        if (!m.getConfirmed()) {
                            continue;
                        }
                        try {
                            Subscription s = subscriptionService.getSubscriptionInformationFacadeRequest(msisdn, m.getCountry().getCode());
                            if (s != null && s.getSubscriptionRegistry() != null) {
                                //  log.info("Suscripcion en PaymentHUb encontrada: [" + (s.getId() == null ? s : s.getId()) + "]");
                                List<SubscriptionBilling> list = subscriptionBillingService.getSubscriptionBillingsByUserAccount(s.getUserAccount());
                                if (list != null && !list.isEmpty()) {
                                    //  log.info("Se han encontrado [" + list.size() + "] cobros para [" + s.getUserAccount() + "]");
                                    for (SubscriptionBilling sb : list) {
                                        CurrencyRequest cr = new CurrencyRequest();
                                        cr.currency = sb.getCurrency();
                                        cr.amount = sb.getFullAmount();
                                        cr.countryId = sb.getSop().getProvider().getCountry().getId();
                                        cr.date = sb.getChargedDate();
                                        cr.sopId = sb.getSop().getId();
                                        cr.transactionId = "PH_SB_" + sb.getId();
                                        cr.userAccount = s.getUserAccount();
                                        cr.tracking = sb.getSubscriptionRegistry().getAdnetworkTracking();
                                        Tariff t = sb.getSop().findTariffByFullAmount(sb.getFullAmount());
                                        if (t == null) {
                                            t = sb.getSop().getMainTariff();
                                            BigDecimal aux = (sb.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
                                            cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
                                            cr.localCurrency = t.getLocalCurrency();
                                        } else {
                                            cr.localAmount = t.getLocalAmount();
                                            cr.localCurrency = t.getLocalCurrency();
                                            cr.tariffId = t.getId();
                                        }
                                        try {
                                            WalletTransaction wt = walletTransactionCreate(cr, user, null, WalletTransaction.Type.ADD);
                                            if (wt != null) {
                                                processWalletTransaction(wt);
                                            }
                                        } catch (Exception ex) {
                                            log.warn("Error al registrar cobro. " + ex);
                                        }
                                        try {
                                            if (m.getProvider() == null || !sb.getSop().getProvider().getId().equals(m.getProvider().getId())) {
                                                m.setProvider(sb.getSop().getProvider());
                                                userService.saveOrUpdate(user);
                                            }
                                        } catch (Exception ex) {
                                        }
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al procesar cobros por suscripcion para: [" + m.getMsisdn() + "]. " + ex, ex);
                        }
                        try {
                            String localLength = phprofilePropertiesService.getCommonProperty("msisdn." + m.getCountry().getCode().toLowerCase() + ".length.local");
                            Integer msisdnLengthLocal = Integer.parseInt(localLength);
                            msisdn = msisdn.replace(" ", "").replace("+", "");
                            msisdn = msisdn.substring(Math.max(0, msisdn.length() - msisdnLengthLocal), msisdn.length());

                            List<OnDemandBilling> list = onDemandBillingService.getByUserAccount(msisdn, m.getCountry());
                            if (list != null && !list.isEmpty()) {
                                // log.info("Se han encontrado [" + list.size() + "] cobros on demand para [" + msisdn + "]");
                                for (OnDemandBilling ob : list) {
                                    CurrencyRequest cr = new CurrencyRequest();
                                    cr.currency = ob.getCurrency();
                                    cr.amount = ob.getFullAmount();
                                    cr.countryId = ob.getSop().getProvider().getCountry().getId();
                                    cr.date = ob.getChargedDate();
                                    cr.sopId = ob.getSop().getId();
                                    cr.transactionId = "PH_OB_" + ob.getId();
                                    cr.userAccount = ob.getUserAccount();
                                    OnDemandRegistry or = onDemandRegistryService.findById(ob.getId());
                                    if (or != null && or.getTransactionTracking() != null) {
                                        cr.tracking = or.getTransactionTracking().adTracking;
                                        cr.description = or.getTransactionTracking().contentName;
                                    }
                                    Tariff t = ob.getSop().findTariffByFullAmount(ob.getFullAmount());
                                    if (t == null) {
                                        t = ob.getSop().getMainTariff();
                                        BigDecimal aux = (ob.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
                                        cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
                                        cr.localCurrency = t.getLocalCurrency();
                                    } else {
                                        cr.localAmount = t.getLocalAmount();
                                        cr.localCurrency = t.getLocalCurrency();
                                        cr.tariffId = t.getId();
                                    }
                                    try {
                                        if (!m.getMsisdn().equals(ob.getUserAccount())) {
                                            log.fatal("Hey!! estos MSISDNs son ligeramente diferentes: [" + m.getMsisdn() + "] [" + ob.getUserAccount() + "]");
                                        }
                                    } catch (Exception ex) {
                                    }

                                    try {
                                        WalletTransaction wt = walletTransactionCreate(cr, user, null, WalletTransaction.Type.ADD);
                                        if (wt != null) {
                                            processWalletTransaction(wt);
                                        }
                                    } catch (Exception ex) {
                                        log.warn("Error al registrar cobro. " + ex);
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al procesar cobros por on demand para: [" + m.getMsisdn() + "]. " + ex, ex);
                        }
                    }
                }
                wu = walleUserService.findById(user.getId());
                if (wu == null) {
                    log.error("Realmente no existe un WalletUser: [" + user.getId() + "]. ");
                    throw new Exception("Realmente no existe un WalletUser: [" + user.getId() + "]");
                }
            }
            /*else {
                log.info("WalletUser: [" + wu + "]");
            }*/

            //Sumamos todos los creditos del usuario
            for (Credit c : wu.getCredits().values()) {
                Set<String> keys = c.getValues().keySet();
                for (String currency : keys) {
                    BigDecimal bd = credits.get(currency);
                    if (bd == null) {
                        bd = new BigDecimal(BigInteger.ZERO);
                    }
                    bd = bd.add(c.getValues().get(currency));
                    credits.put(currency, bd);
                }
            }
        } catch (Exception ex) {
            log.error("Error al procesar getWalletFunds. User: [" + user + "]. " + ex, ex);
        }
        log.info("Creditos: [" + credits + "] - WalletUser: [" + wu + "]");

        return credits;
    }

    private Boolean setWalletFunds(User user, WalletTransaction.Type type, String localCurrency, BigDecimal localAmount, SOP sop, User client) {
        while (user.getPrincipal() != null) {
            user = user.getPrincipal();
        }
        Boolean result = null;
        try {
            WalletUser wu = walleUserService.findById(user.getId());
            if (wu == null) {
                // log.info("No existe un WalletUser: [" + user.getId() + "]");
                wu = new WalletUser();
                wu.setId(user.getId());
            }/* else {
                log.info("WalletUser: [" + wu + "]");
            }*/

            switch (type) {
                case ADD:
                    addWalletFunds(wu, localCurrency, localAmount, sop, client);
                    break;
                case WITHDRAW:
                    if (client != null) {
                        withdrawWalletFunds(wu, localCurrency, localAmount, client);
                    } else {
                        withdrawWalletFunds(wu, localCurrency, localAmount, sop);
                    }
                    break;
            }
            walleUserService.saveOrUpdate(wu);
            result = true;
        } catch (Exception ex) {
            log.error("Error en setWalletFunds User: [" + user.getId() + "] - type: [" + type + "] - localCurrency: [" + localCurrency + "] - localAmount: [" + localAmount + "] - [" + sop + "][" + client + "]. " + ex, ex);
        }

        return result;
    }

    private void addWalletFunds(WalletUser wu, String localCurrency, BigDecimal localAmount, SOP sop, User client) {
        String key;
        if (sop != null) {
            key = "SOP_" + sop.getId();
        } else {
            if (client.getPrincipal() != null) {
                client = client.getPrincipal();
            }
            key = "CLIENT_" + client.getId();
        }

        if (wu.getCredits() == null) {
            wu.setCredits(new HashMap());
        }

        Credit c = wu.getCredits().get(key);
        if (c == null) {
            c = new Credit();
            if (client != null) {
                c.setClientId(client.getId());
            }
            if (sop != null) {
                c.setSopId(sop.getId());
            }
            wu.getCredits().put(key, c);
        } else {
            BigDecimal aux = c.getValues().get(localCurrency);
            if (aux != null) {
                localAmount = aux.add(localAmount);
            }
        }
        c.getValues().put(localCurrency, localAmount);
    }

    private void withdrawWalletFunds(WalletUser wu, String localCurrency, BigDecimal localAmount, User client) throws Exception {

        if (wu.getCredits() == null) {
            throw new Exception("WalletUser [" + wu + "] no tiene creditos disponibles.");
        }

        String key = "CLIENT_" + client.getId();
        Credit c = wu.getCredits().get(key);
        if (c != null) {
            BigDecimal amount = c.getValues().get(localCurrency);
            if (amount != null) {
                if (amount.compareTo(localAmount) < 0) {
                    localAmount = localAmount.subtract(amount);
                    amount = amount.subtract(amount);
                } else {
                    amount = amount.subtract(localAmount);
                    localAmount = localAmount.subtract(localAmount);
                }
                c.getValues().put(localCurrency, amount);
            }
            if (localAmount.compareTo(BigDecimal.ZERO) == 0) {
                return;
            }
        }

        for (Product p : client.getProducts()) {
            for (SOP sop : p.getSops()) {
                key = "SOP_" + sop.getId();
                c = wu.getCredits().get(key);
                if (c != null) {
                    BigDecimal amount = c.getValues().get(localCurrency);
                    if (amount != null) {
                        if (amount.compareTo(localAmount) < 0) {
                            localAmount = localAmount.subtract(amount);
                            amount = amount.subtract(amount);
                        } else {
                            amount = amount.subtract(localAmount);
                            localAmount = localAmount.subtract(localAmount);
                        }
                        c.getValues().put(localCurrency, amount);
                    }
                    if (localAmount.compareTo(BigDecimal.ZERO) == 0) {
                        return;
                    }
                }
            }
        }

        if (localAmount.compareTo(BigDecimal.ZERO) != 0) {
            throw new Exception("WalletUser [" + wu + "] no tiene creditos suficientes para localAmount: [" + localAmount + "].");
        }
    }

    private void withdrawWalletFunds(WalletUser wu, String localCurrency, BigDecimal localAmount, SOP sop) throws Exception {

        if (wu.getCredits() == null) {
            throw new Exception("WalletUser [" + wu + "] no tiene creditos disponibles.");
        }
        String key = "SOP_" + sop.getId();
        Credit c = wu.getCredits().get(key);
        if (c != null) {
            BigDecimal amount = c.getValues().get(localCurrency);
            if (amount != null) {
                if (amount.compareTo(localAmount) < 0) {
                    localAmount = localAmount.subtract(amount);
                    amount = amount.subtract(amount);
                } else {
                    amount = amount.subtract(localAmount);
                    localAmount = localAmount.subtract(localAmount);
                }
                c.getValues().put(localCurrency, amount);
            }
            if (localAmount.compareTo(BigDecimal.ZERO) == 0) {
                return;
            }
        }

        if (localAmount.compareTo(BigDecimal.ZERO) != 0) {
            throw new Exception("WalletUser [" + wu + "] no tiene creditos suficientes para localAmount: [" + localAmount + "].");
        }
    }

    public Boolean putWalletFunds(User client, User user, PGMessageRequest messageRequest) {
        Boolean result = false;
        log.info("Asignando creditos. Cliend: [" + client.getId() + "] - User: [" + user.getId() + "] - PGMessageRequest: [" + messageRequest + "]");

        if (messageRequest.credit.currency.length() < 2 || messageRequest.credit.currency.length() > 3) {
            return false;
        }

        if (client.getPrincipal() != null) {
            client = client.getPrincipal();
        }
        try {
            CurrencyRequest cr = new CurrencyRequest();
            cr.date = new Date();
            cr.localAmount = messageRequest.credit.amount;
            cr.localCurrency = messageRequest.credit.currency;
            cr.transactionId = "CLIENT_" + client.getId() + "_" + user.getId() + "_" + messageRequest.transactionId;
            cr.tracking = messageRequest;
            cr.userAccount = user.getId().toString();
            cr.description = messageRequest.description;
            WalletTransaction wt = walletTransactionCreate(cr, user, client, WalletTransaction.Type.ADD);
            if (wt != null) {
                return processWalletTransaction(wt);
            }
            result = false;
        } catch (Exception ex) {
            log.error("Error al asignar creditos. Client: [" + client.getId() + "] - User: [" + user.getId() + "] - PGMessageRequest: [" + messageRequest + "]. " + ex, ex);
            result = false;
        }

        return result;
    }

    public Boolean removeWalletFunds(User client, User user, PGMessageRequest messageRequest) {
        Boolean result = false;
        //  log.info("Consumiendo creditos. Cliend: [" + client.getId() + "] - User: [" + user.getId() + "] - PGMessageRequest: [" + messageRequest + "]");
        if (client.getPrincipal() != null) {
            client = client.getPrincipal();
        }
        try {
            Map<String, BigDecimal> credits = getWalletFunds(client, user);
            BigDecimal credit = credits.get(messageRequest.credit.currency.toUpperCase());

            //  log.info("Creditos disponibles: [" + Arrays.toString(credits.entrySet().toArray()) + "]. A consumir: [" + messageRequest.credit + "]");
            if (credit == null || credit.compareTo(messageRequest.credit.amount) < 0) {
                return result;
            }

            CurrencyRequest cr = new CurrencyRequest();
            cr.date = new Date();
            cr.localAmount = messageRequest.credit.amount;
            cr.localCurrency = messageRequest.credit.currency;
            cr.transactionId = "CLIENT_" + client.getId() + "_" + user.getId() + "_" + messageRequest.transactionId;
            cr.tracking = messageRequest;
            cr.userAccount = user.getId().toString();
            cr.description = messageRequest.description;
            WalletTransaction wt = walletTransactionCreate(cr, user, client, WalletTransaction.Type.WITHDRAW);
            if (wt != null) {
                return processWalletTransaction(wt);
            }
            result = false;
        } catch (Exception ex) {
            log.error("Error al consumir creditos. Client: [" + client.getId() + "] - User: [" + user.getId() + "] - PGMessageRequest: [" + messageRequest + "]. " + ex, ex);
            result = false;
        }
        return result;
    }

    public Set<PaymentMethod> getPaymentList(User client, User user, DefaultClaims defaultClaims, PGMessageRequest messageRequest) {
        Set<PaymentMethod> spm = new HashSet();
        List<User> clients = new ArrayList();
        clients.add(client);
        List<PGMessageRequest> messageRequests = new ArrayList();
        messageRequests.add(messageRequest);

        List<PaymentMethod> lpm = getPaymentList(user, defaultClaims, messageRequests, clients, false);
        spm.addAll(lpm);
        return spm;
    }

    public List<PaymentMethod> getPaymentList(User user, DefaultClaims defaultClaims, List<PGMessageRequest> messageRequests, List<User> clients) {
        return getPaymentList(user, defaultClaims, messageRequests, clients, true);
    }

    public Set<PaymentMethod> getOuterPaymentList(User client, PGMessageRequest messageRequest) {
        List<User> clients = new ArrayList();
        clients.add(client);
        List<PGMessageRequest> messageRequests = new ArrayList();
        messageRequests.add(messageRequest);
        List<PaymentMethod> lpm = getPaymentList(null, null, messageRequests, clients, false);
        Set<PaymentMethod> spm = new HashSet();
        spm.addAll(lpm);
        return spm;
    }

    /**
     * @param isList. true Proviene de la vista. false. Proviene de la API
     */
    private List<PaymentMethod> getPaymentList(User user, DefaultClaims defaultClaims, List<PGMessageRequest> messageRequests, List<User> clients, boolean isList) {

        List<PaymentMethod> pml = new ArrayList();
        Map<Long, PaymentMethod> pmm = new HashMap();
        String lg = null;
        if (user != null) {
            Map<Long, Country> cs = new HashMap();
            Country c = null;
            try {
                String cc = defaultClaims.get("country", String.class);
                if (cc != null) {
                    c = countryService.findByCode(cc);
                }
            } catch (Exception ex) {
            }

            if (c != null) {
                try {
                    cs.put(c.getId(), c);
                    List<PaymentMethod> aux = paymentMethodService.findByCountry(c);
                    if (aux != null && !aux.isEmpty()) {
                        for (PaymentMethod pm : aux) {
                            pmm.put(pm.getId(), pm);
                        }
                    }
                } catch (Exception ex) {
                    log.error("Error al obtener PaymentMethod por pais. [" + c + "]. " + ex, ex);
                }
            }

            if (user.getProfileUserProperty().getCountry() != null && cs.get(user.getProfileUserProperty().getCountry().getId()) == null) {
                try {
                    cs.put(user.getProfileUserProperty().getCountry().getId(), user.getProfileUserProperty().getCountry());
                    List<PaymentMethod> aux = paymentMethodService.findByCountry(user.getProfileUserProperty().getCountry());
                    if (aux != null && !aux.isEmpty()) {
                        for (PaymentMethod pm : aux) {
                            pmm.put(pm.getId(), pm);
                        }
                    }
                } catch (Exception ex) {
                    log.error("Error al obtener PaymentMethod por pais. [" + user.getProfileUserProperty().getCountry() + "]. " + ex, ex);
                }
            }
            try {
                if (!user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                    for (Msisdn m : user.getCredentialUserProperty().getUsername().getMsisdns().values()) {
                        try {
                            //  log.info("Msisdn: [" + m + "]  - cs: [" + Arrays.toString(cs.entrySet().toArray()) + "]");
                            if (m.getCountry() != null && cs.get(m.getCountry().getId()) == null) {
                                cs.put(m.getCountry().getId(), m.getCountry());
                                List<PaymentMethod> aux = paymentMethodService.findByCountry(m.getCountry());
                                if (aux != null && !aux.isEmpty()) {
                                    for (PaymentMethod pm : aux) {
                                        pmm.put(pm.getId(), pm);
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            log.error("Error al obtener PaymentMethod por pais. [" + m.getCountry() + "]. " + ex, ex);
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("Error al obtener PaymentMethod por Msisdns. [" + user + "]. " + ex, ex);
            }

            try {
                if (!user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty()) {
                    if (isList) {
                        //Viene de la vista
                        for (Msisdn m : user.getCredentialUserProperty().getUsername().getMsisdns().values()) {
                            if (m.getCountry() != null) {
                                List<Subscription> sl = subscriptionService.getSubscriptionCountryFacadeRequest(m.getMsisdn(), m.getCountry().getCode());
                                if (sl != null && !sl.isEmpty()) {
                                    for (Subscription s : sl) {
                                        if (!Subscription.Status.CANCELLED.equals(s.getStatus())
                                                && !Subscription.Status.REMOVED.equals(s.getStatus())
                                                && !Subscription.Status.LOCKED.equals(s.getStatus())) {
                                            pmm.remove(s.getSubscriptionRegistry().getSop().getId());
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        //Viene de la API
                        for (Msisdn m : user.getCredentialUserProperty().getUsername().getMsisdns().values()) {
                            if (m.getCountry() != null) {
                                List<Subscription> sl = subscriptionService.findSubscriptionsByUserAccount(m.getMsisdn());
                                if (sl != null && !sl.isEmpty()) {
                                    for (Subscription s : sl) {
                                        if (!Subscription.Status.CANCELLED.equals(s.getStatus())
                                                && !Subscription.Status.REMOVED.equals(s.getStatus())
                                                && !Subscription.Status.LOCKED.equals(s.getStatus())) {
                                            try {
                                                pmm.remove(s.getSubscriptionRegistry().getSop().getId());
                                            } catch (Exception ex) {
                                                log.error("No existe un W_PAYMENT_METHOD con figurado para: [" + s.getSubscriptionRegistry().getSop().getId() + "]");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar suscripciones por Msisdns. [" + user + "]. " + ex, ex);
            }

            try {
                if (!user.getCredentialUserProperty().getUsername().getEmails().isEmpty()) {
                    for (Email e : user.getCredentialUserProperty().getUsername().getEmails().values()) {
                        List< Subscription> sl = subscriptionService.findSubscriptionsByUserAccount(e.getEmail());
                        if (sl != null && !sl.isEmpty()) {
                            for (Subscription s : sl) {
                                if (cs.get(s.getSop().getProvider().getCountry().getId()) == null) {
                                    cs.put(s.getSop().getProvider().getCountry().getId(), s.getSop().getProvider().getCountry());
                                    List<PaymentMethod> aux = paymentMethodService.findByCountry(s.getSop().getProvider().getCountry());
                                    if (aux != null && !aux.isEmpty()) {
                                        for (PaymentMethod pm : aux) {
                                            pmm.put(pm.getId(), pm);
                                        }
                                    }
                                }
                                if (!Subscription.Status.CANCELLED.equals(s.getStatus())
                                        && !Subscription.Status.REMOVED.equals(s.getStatus())
                                        && !Subscription.Status.LOCKED.equals(s.getStatus())) {
                                    pmm.remove(s.getSubscriptionRegistry().getSop().getId());
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar suscripciones por Emails. [" + user + "]. " + ex, ex);
            }
            lg = user.getProfileUserProperty().getLanguage() == null ? "es" : user.getProfileUserProperty().getLanguage();
        } else {
            try {
                PGMessageRequest mr = messageRequests.get(0);
                if (mr.provider != null && mr.service != null && mr.operator != null) {
                    SOP sop = sopService.findByNames(mr.service, mr.operator, mr.provider);
                    if (sop != null) {
                        Country c = sop.getProvider().getCountry();
                        if (c.getLanguages().size() > 1) {
                            Set<Language> list = new TreeSet();
                            list.addAll(c.getLanguages());
                            lg = (list.iterator().next().getCode().toLowerCase().trim() + "-" + c.getCode().toUpperCase().trim());
                        } else {
                            lg = (c.getLanguages().iterator().next().getCode().toLowerCase().trim() + "-" + c.getCode().toUpperCase().trim());
                        }
                        PaymentMethod pm = paymentMethodService.findById(sop.getId());
                        if (pm != null) {
                            pmm.put(pm.getId(), pm);
                        }
                    }
                } else {
                    List<PaymentMethod> aux = paymentMethodService.getAll();
                    List<SOP> sops = new ArrayList();

                    for (PaymentMethod pm : aux) {
                        Object o = MainCache.memory24Hours().getIfPresent("SOP_ID_T_" + pm.getId());
                        SOP sop;
                        if (o == null) {
                            sop = sopService.findById(pm.getId());
                            MainCache.memory24Hours().put("SOP_ID_T_" + pm.getId(), sop);
                        } else {
                            sop = (SOP) o;
                        }
                        sops.add(sop);
                    }
                    if (mr.provider != null) {
                        List<SOP> asop = new ArrayList();
                        for (SOP sop : sops) {
                            if (mr.provider.toUpperCase().equals(sop.getProvider().getName().toUpperCase())) {
                                asop.add(sop);
                            }
                        }
                        sops = asop;
                    } else if (mr.countryCode != null) {
                        List<SOP> asop = new ArrayList();
                        for (SOP sop : sops) {
                            if (mr.countryCode.toUpperCase().equals(sop.getProvider().getCountry().getCode().toUpperCase())) {
                                asop.add(sop);
                            }
                        }
                        sops = asop;
                    }
                    if (mr.service != null) {
                        List<SOP> asop = new ArrayList();
                        for (SOP sop : sops) {
                            if (mr.service.toUpperCase().equals(sop.getServiceMatcher().getName().toUpperCase())) {
                                asop.add(sop);
                            }
                        }
                        sops = asop;
                    }
                    if (mr.operator != null) {
                        List<SOP> asop = new ArrayList();
                        for (SOP sop : sops) {
                            if (mr.operator.toUpperCase().equals(sop.getOperator().getName().toUpperCase())) {
                                asop.add(sop);
                            }
                        }
                        sops = asop;
                    }

                    PaymentMethod xx = new PaymentMethod();
                    for (SOP sop : sops) {
                        pmm.put(sop.getId(), xx);

                        if (lg == null) {
                            Country c = sop.getProvider().getCountry();
                            if (c.getLanguages().size() > 1) {
                                Set<Language> list = new TreeSet();
                                list.addAll(c.getLanguages());
                                lg = (list.iterator().next().getCode().toLowerCase().trim() + "-" + c.getCode().toUpperCase().trim());
                            } else {
                                lg = (c.getLanguages().iterator().next().getCode().toLowerCase().trim() + "-" + c.getCode().toUpperCase().trim());
                            }
                        }
                    }
                    for (PaymentMethod pm : aux) {
                        if (pmm.containsKey(pm.getId())) {
                            pmm.put(pm.getId(), pm);
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("No fue posible procesar todos los metodos de pago. " + ex, ex);
            }
        }

        String source = "";
        if (isList) {
            source = "";
            source = "_PG_Account";
        }
        for (User cli : clients) {
            User aux = cli;
            if (aux.getPrincipal() != null) {
                aux = aux.getPrincipal();
            }
            for (Product p : aux.getProducts()) {
                for (SOP sop : p.getSops()) {
                    PaymentMethod pm = pmm.get(sop.getId());
                    try {
                        if (pm != null) {
                            if (isList) {
                                pm.product = new Product();
                                pm.product.setName(p.getName());
                                pm.product.setLogoURL(p.getLogoURL());

                                /* if (p.getDescription() != null) {
                                    String prvdr = null;
                                    try {
                                        prvdr = defaultClaims.get("prvdr", String.class);
                                    } catch (Exception ex) {
                                    }
                                     pm.description = i18n(lg, prvdr, p.getDescription());
                                }*/
                            }

                            LinkedHashMap<String, String> lhm = (LinkedHashMap<String, String>) pm.getFacadeParams().adTracking;
                            if (user != null) {
                                lhm.put("userId", user.getId().toString());
                            }
                            lhm.put("source", cli.getProfileUserProperty().getFirstName() + "_" + cli.getProfileUserProperty().getLastName() + source);
                            lhm.put("medium", cli.getProfileUserProperty().getFirstName() + "" + cli.getProfileUserProperty().getLastName() + "_" + pm.getName().toUpperCase() + "_" + sop.getProvider().getCountry().getCode().toUpperCase());
                            lhm.put("campaign", cli.getProfileUserProperty().getLastName() + "_" + pm.getType() + "_" + sop.getId());
                            try {
                                if (pm.getLandingUrl() != null && pm.getLandingUrl().contains("campaignParamsTest")) {
                                    pm.setLandingUrl(pm.getLandingUrl().replace("campaignParamsTest",
                                            URLEncoder.encode(mapper.writeValueAsString(pm.getFacadeParams().adTracking), StandardCharsets.UTF_8.toString())));
                                }
                            } catch (Exception ex) {
                            }
                            pm.getFacadeParams().service = sop.getServiceMatcher().getName();
                            pm.getFacadeParams().operator = sop.getOperator().getName();
                            pm.getFacadeParams().provider = sop.getProvider().getName();

                            pm.setCountry(new Country());
                            pm.getCountry().setCode(sop.getProvider().getCountry().getCode());
                            pm.getCountry().setName(sop.getProvider().getCountry().getName());

                            Tariff t = sop.getMainTariff();
                            if (t != null) {
                                pm.setCredit(new com.itbaf.platform.messaging.rest.pg.Credit());
                                Country cAux = countryService.getCountryByCurrencyCode(t.getCurrency());
                                pm.getCredit().symbol = cAux.getSymbolCurrency();
                                pm.getCredit().currency = cAux.getCurrency();
                                pm.getCredit().countryCode = sop.getProvider().getCountry().getCode();
                                pm.getCredit().amount = t.getFullAmount();

                                pm.setVirtualCredit(new com.itbaf.platform.messaging.rest.pg.Credit());
                                pm.getVirtualCredit().amount = t.getLocalAmount();
                                pm.getVirtualCredit().currency = t.getLocalCurrency();
                            }
                            SOP sopAux = new SOP();
                            try {
                                sopAux.setTyc(sop.getIntegrationSettings().get("tyc"));
                            } catch (Exception ex) {
                            }
                            if (sopAux.getTyc() == null) {
                                sopAux.setTyc(TYC_URL);
                            }
                            pm.setUserAccountType(sop.getIntegrationSettings().get("userAccountType"));
                            pm.setSop(sopAux);
                            if (PaymentMethod.PaymentMethodType.ONDEMAND.equals(pm.getType())) {
                                pm.setFrecuencyType(null);
                                if (t == null) {
                                    for (PGMessageRequest messageRequest : messageRequests) {
                                        try {
                                            PaymentMethod auxPM = clonePaymentMethod(pm);

                                            auxPM.setCredit(new com.itbaf.platform.messaging.rest.pg.Credit());
                                            Country cAux = countryService.getCountryByCurrencyCode(messageRequest.credit.currency);
                                            auxPM.getCredit().symbol = cAux.getSymbolCurrency();
                                            auxPM.getCredit().currency = cAux.getCurrency();
                                            auxPM.getCredit().countryCode = sop.getProvider().getCountry().getCode();
                                            auxPM.getCredit().amount = messageRequest.credit.amount;

                                            auxPM.setVirtualCredit(new com.itbaf.platform.messaging.rest.pg.Credit());
                                            auxPM.getVirtualCredit().amount = messageRequest.virtualCredit.amount;
                                            auxPM.getVirtualCredit().currency = messageRequest.virtualCredit.currency;

                                            pml.add(auxPM);
                                        } catch (Exception ex) {
                                            log.error("Error al procesar PGMessageRequest: [" + messageRequest + "]. " + ex, ex);
                                        }
                                    }
                                } else {
                                    pm.getVirtualCredit().amount = pm.getVirtualCredit().amount.setScale(0, RoundingMode.UP);
                                    pml.add(pm);
                                }
                            } else {
                                if (pm.getFrecuencyType() != null) {
                                    String prvdr = null;
                                    try {
                                        prvdr = defaultClaims.get("prvdr", String.class);
                                    } catch (Exception ex) {
                                    }
                                    pm.frecuencyI18n = i18n(lg, prvdr, "<i18n>i18n.ix.px.text." + pm.getFrecuencyType().name().toLowerCase() + "</i18n>");
                                }
                                pm.getVirtualCredit().amount = pm.getVirtualCredit().amount.setScale(0, RoundingMode.UP);
                                pml.add(pm);
                            }
                        }
                    } catch (Exception ex) {
                        log.error("Error al procesar PaymentMethod [" + pm + "]. " + ex, ex);
                    }
                }
            }
        }

        //  log.info("PaymentMethod List: [" + Arrays.toString(pml.toArray()) + "]");
        return pml;
    }

    private PaymentMethod clonePaymentMethod(PaymentMethod pm) {
        PaymentMethod aux = new PaymentMethod();
        FacadeRequest fr = new FacadeRequest();
        fr.adTracking = pm.getFacadeParams().adTracking;
        fr.service = pm.getFacadeParams().service;
        fr.operator = pm.getFacadeParams().operator;
        fr.provider = pm.getFacadeParams().provider;
        fr.userAccount = pm.getFacadeParams().userAccount;
        fr.channel = pm.getFacadeParams().channel;
        fr.contentId = pm.getFacadeParams().contentId;
        fr.urlNotify = pm.getFacadeParams().urlNotify;
        fr.contentUrl = pm.getFacadeParams().contentUrl;
        fr.contentName = pm.getFacadeParams().contentName;
        fr.contentType = pm.getFacadeParams().contentType;
        fr.ammoParams = pm.getFacadeParams().ammoParams;

        aux.setFacadeParams(fr);
        aux.setUserAccountType(aux.getUserAccountType());
        aux.setCountry(pm.getCountry());
        if (pm.getCredit() != null) {
            aux.setCredit(new com.itbaf.platform.messaging.rest.pg.Credit());
            aux.getCredit().amount = pm.getCredit().amount;
            aux.getCredit().currency = pm.getCredit().currency;
            aux.getCredit().symbol = pm.getCredit().symbol;
        }
        if (pm.getVirtualCredit() != null) {
            aux.setVirtualCredit(new com.itbaf.platform.messaging.rest.pg.Credit());
            aux.getVirtualCredit().amount = pm.getVirtualCredit().amount;
            aux.getVirtualCredit().currency = pm.getVirtualCredit().currency;
            aux.getVirtualCredit().symbol = pm.getVirtualCredit().symbol;
        }

        aux.setFrecuencyType(pm.getFrecuencyType());
        aux.setId(pm.getId());
        aux.setLandingUrl(pm.getLandingUrl());
        aux.setLogoUrl(pm.getLogoUrl());
        aux.setName(pm.getName());
        aux.setSop(pm.getSop());
        aux.setType(pm.getType());
        aux.product = pm.product;
        aux.description = pm.description;

        return aux;
    }

    public Boolean subscriptionProcess(CurrencyRequest cr) {

        log.info("ApiWalletServiceProcessorHandler.subscriptionProcess [" + cr + "]");

        WalletTransaction lastWithdraw = null;
        if (cr.tracking != null) {
            try {
                if (cr.tracking != null) {
                    LinkedHashMap<String, String> lhm = (LinkedHashMap<String, String>) cr.tracking;
                    //Esto puede causar problemas... Un user con ID de otro.. 
                    String userId = lhm.get("userId");                     //CAM-297 - cr no tiene un campo llamado userId, sino userAccount. Es correcto usar userId?
                    if (userId != null) {
                        User user = userService.findById(Long.parseLong(userId));
                        if (user != null) {
                            try {
                                if (cr.userAccount.contains("@") && user.getCredentialUserProperty().getUsername().getEmail(cr.userAccount) == null) {
                                    User xuser = userService.findByEmail(cr.userAccount);
                                    if (xuser != null && !xuser.getId().equals(user.getId())) {
                                        user = getPrincipalUser(cr, user, xuser);
                                    } else if (xuser == null) {
                                        log.info("Se procede a adicionar el userAccount: [" + cr.userAccount + "] al User: [" + user.getId() + "]");
                                        user.getCredentialUserProperty().getUsername().setEmail(new Email(cr.userAccount, Boolean.TRUE));
                                        userService.saveOrUpdate(user);
                                    }
                                } else if (user.getCredentialUserProperty().getUsername().getMsisdn(cr.userAccount) == null && isNumber(cr.userAccount)) {
                                    User xuser = userService.findByMsisdn(cr.userAccount);
                                    if (xuser != null && !xuser.getId().equals(user.getId())) {
                                        user = getPrincipalUser(cr, user, xuser);
                                    } else if (xuser == null) {
                                        log.info("Se procede a adicionar el userAccount: [" + cr.userAccount + "] al User: [" + user.getId() + "]");
                                        SOP sop = sopService.findById(cr.sopId);
                                        user.getCredentialUserProperty().getUsername()
                                                .setMsisdn(new Msisdn(cr.userAccount, Boolean.TRUE, sop.getProvider().getCountry(), sop.getProvider()));
                                        userService.saveOrUpdate(user);
                                    }
                                }
                            } catch (Exception ex) {
                                log.error("Error al adicionar el userAccount: [" + cr.userAccount + "] al User: [" + user + "]. " + ex, ex);
                            }

                            /*
                                Buscar saldo previo a ultima baja
                                Solo para Entel Peru (Timwe) SOP_ID =
                             */
                            try {

                                SOP peSop = sopService.findOrCreateByNames("JuegosMensual", "ITBAF", "pe.entel.timwe");
                                if (peSop != null) {

                                    // Si el SOP del CurrencyRequest actual es JuegosMensual de entel timwe
                                    if (peSop.getId() == cr.sopId) {
                                        Calendar c = Calendar.getInstance();
                                        c.add(Calendar.MONTH, -1);
                                        // Buscar la ultima baja desde hace un mes
                                        log.info("Buscando baja anterior. Usuario: [" + user + "] Desde: [" + c.getTime() + "+] Hasta:[" + new Date() + "]");
                                        lastWithdraw = walletTransactionService.getLastWithdraw(user, peSop.getId(), c.getTime(), new Date());

                                        if (lastWithdraw != null) {
                                            log.info("Encontrada baja anterior. Usuario: [\" + user + \"] WT: [" + lastWithdraw + "]");

                                        }
                                    }
                                }

                            } catch (Exception ex) {
                                log.error("Error Buscando baja anterior " + ex, ex);
                            }

                        } else {
                            log.fatal("No existe un usuario para: [" + cr + "]");
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar request de suscripcion. [" + cr + "]. " + ex, ex);
                return null;
            }
        }

        else {
            User user = userService.findByMsisdn(cr.userAccount);
            if (user != null) {
                    /*
                    Buscar saldo previo a ultima baja
                    Solo para Entel Peru (Timwe) SOP_ID = 75
                    */
                    try {
                        SOP peSop = sopService.findOrCreateByNames("JuegosMensual", "ITBAF", "pe.entel.timwe");
                        if (peSop != null) {
                            // Si el SOP del CurrencyRequest actual es JuegosMensual de entel timwe
                            if (peSop.getId() == cr.sopId) {
                                Calendar c = Calendar.getInstance();
                                c.add(Calendar.MONTH, -1);
                                // Buscar la ultima baja desde hace un mes
                                log.info("Buscando baja anterior. Usuario: [" + user + "] Desde: [" + c.getTime() + "+] Hasta:[" + new Date() + "]");
                                lastWithdraw = walletTransactionService.getLastWithdraw(user, peSop.getId(), c.getTime(), new Date());
                                if (lastWithdraw != null) {
                                    log.info("Encontrada baja anterior. Usuario: [\" + user + \"] WT: [" + lastWithdraw + "]");

                                }
                            }
                        }

                    } catch (Exception ex) {
                        log.error("Error Buscando baja anterior " + ex, ex);
                    }
            }
        }


        cr.amount = BigDecimal.ZERO;
        // Asignar el saldo que tenia antes de la ultima baja
        if (lastWithdraw != null) {
            log.info("Asignando saldo por suscripcion mensual timwe: ["+lastWithdraw.getLocalAmount()+"]");
            cr.localAmount = lastWithdraw.getLocalAmount();
        } else {
            if (cr.localAmount != null && BigDecimal.ZERO.compareTo(cr.localAmount) != 0) {
                cr.localAmount = new BigDecimal(2000L);
            }
            log.info("Asignando saldo: ["+cr.localAmount+"]");
        }
        if (cr.sopId != null && cr.description == null) {
            SOP sop = sopService.findById(cr.sopId);
            Country c = sop.getProvider().getCountry();

            String i18n;
            if (c.getLanguages().size() > 1) {
                Set<Language> list = new TreeSet();
                list.addAll(c.getLanguages());
                i18n = list.iterator().next().getCode().toLowerCase().trim() + "-" + c.getCode().toUpperCase().trim();
            } else {
                i18n = c.getLanguages().iterator().next().getCode() + "-" + c.getCode().toUpperCase().trim();
            }
            cr.description = i18n(i18n, sop.getProvider().getId().toString(), "<i18n>i18n.ix.px.text.subscription.bonus</i18n>") + " " + sop.getServiceMatcher().getGenericName();
        }

        return true;
    }

    public Boolean unsubscriptionProcess(CurrencyRequest cr) {

        WalletTransaction wt = walletTransactionService.findByTransactionId(cr.transactionId);

        if (wt != null) {
            return false;
        }

        User user = null;
        if (cr.userAccount.contains("@")) {
            user = userService.findByEmail(cr.userAccount);
        } else {
            user = userService.findByMsisdn(cr.userAccount);
        }

        if (user == null) {
            return false;
        }

        WalletUser wu = walleUserService.findById(user.getId());
        if (wu == null) {
            return false;
        }

        String key = "SOP_" + cr.sopId;
        Credit c = wu.getCredits().get(key);

        if (c == null) {
            return false;
        }

        BigDecimal bd = c.getValues().get(cr.localCurrency);
        if (bd == null || bd.compareTo(BigDecimal.ZERO) <= 0) {
            return false;
        }

        SOP sop = sopService.findById(cr.sopId);

        cr.localAmount = bd;
        cr.description = "Cancelacion creditos por baja de suscripcion: " + sop.getServiceMatcher().getGenericName();

        try {
            wt = walletTransactionCreate(cr, user, null, WalletTransaction.Type.WITHDRAW);
            if (wt != null) {
                return processWalletTransaction(wt);
            }
        } catch (Exception ex) {
            log.error("Error al crear WalletTransactionCreate [" + cr + "]. " + ex, ex);
        }
        return null;
    }

    private Boolean hasMsisdns(User user){
        return user.getCredentialUserProperty() != null &&
                user.getCredentialUserProperty().getUsername() != null &&
                user.getCredentialUserProperty().getUsername().getMsisdns() != null &&
                !user.getCredentialUserProperty().getUsername().getMsisdns().isEmpty();
    }

    private Boolean hasEmail(User user){
        return user.getCredentialUserProperty() != null &&
                user.getCredentialUserProperty().getUsername() != null &&
                user.getCredentialUserProperty().getUsername().getEmails() != null &&
                !user.getCredentialUserProperty().getUsername().getEmails().isEmpty();
    }

    /*private CurrencyRequest createCurrencyRequest(SubscriptionBilling sb, String userAccount) {
        CurrencyRequest cr = new CurrencyRequest();
        cr.currency = sb.getCurrency();
        cr.amount = sb.getFullAmount();
        cr.countryId = sb.getSop().getProvider().getCountry().getId();
        cr.date = sb.getChargedDate();
        cr.sopId = sb.getSop().getId();
        cr.transactionId = "PH_SB_" + sb.getId();
        cr.userAccount = userAccount;
        cr.tracking = sb.getSubscriptionRegistry().getAdnetworkTracking();

        Tariff t = sb.getSop().findTariffByFullAmount(sb.getFullAmount());
        if (t == null) {
            t = sb.getSop().getMainTariff();
            BigDecimal aux = (sb.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
            cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
            cr.localCurrency = t.getLocalCurrency();
        } else {
            cr.localAmount = t.getLocalAmount();
            cr.localCurrency = t.getLocalCurrency();
            cr.tariffId = t.getId();
        }

        return cr;
    }

    private CurrencyRequest createCurrencyRequest(OnDemandBilling ob, String userAccount) {
        CurrencyRequest cr = new CurrencyRequest();
        cr.currency = ob.getCurrency();
        cr.amount = ob.getFullAmount();
        cr.countryId = ob.getSop().getProvider().getCountry().getId();
        cr.date = ob.getChargedDate();
        cr.sopId = ob.getSop().getId();
        cr.transactionId = "PH_OB_" + ob.getId();
        cr.userAccount = userAccount;
        OnDemandRegistry or = onDemandRegistryService.findById(ob.getId());
        if (or != null && or.getTransactionTracking() != null) {
            cr.tracking = or.getTransactionTracking().adTracking;
            cr.description = or.getTransactionTracking().contentName;
        }

        Tariff t = ob.getSop().findTariffByFullAmount(ob.getFullAmount());
        if (t == null) {
            t = ob.getSop().getMainTariff();
            BigDecimal aux = (ob.getFullAmount().multiply(BigDecimal.valueOf(100))).divide(t.getFullAmount(), 2, RoundingMode.CEILING);
            cr.localAmount = aux.multiply(t.getLocalAmount()).divide(BigDecimal.valueOf(100), 0, RoundingMode.UP);
            cr.localCurrency = t.getLocalCurrency();
        } else {
            cr.localAmount = t.getLocalAmount();
            cr.localCurrency = t.getLocalCurrency();
            cr.tariffId = t.getId();
        }

        return cr;
    }

    private void createWalletTransaction(CurrencyRequest cr, User user, User client){
        try {
            WalletTransaction wt = walletTransactionCreate(cr, user, client, WalletTransaction.Type.ADD);
            if (wt != null) {
                processWalletTransaction(wt);
            }
        } catch (Exception ex) {
            log.warn("Error al registrar cobro. " + ex);
        }
    }

    private void processFundsByEmail(User client, User user){
        Set<String> keys = user.getCredentialUserProperty().getUsername().getEmails().keySet();
        for (String email : keys) {
            if (!user.getCredentialUserProperty().getUsername().getEmails().get(email).getConfirmed())
                continue;

            try {
                List<SubscriptionBilling> list = subscriptionBillingService.getSubscriptionBillingsByUserAccount(email);
                if (list != null && !list.isEmpty()) {
                    //   log.info("Se han encontrado [" + list.size() + "] cobros para [" + email + "]");
                    for (SubscriptionBilling sb : list) {
                        CurrencyRequest cr = createCurrencyRequest(sb, email);
                        createWalletTransaction(cr, user, client);
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar cobros por suscripcion para: [" + email + "]. " + ex);
            }

            try {
                List<OnDemandBilling> list = onDemandBillingService.getByUserAccount(email);
                if (list != null && !list.isEmpty()) {
                    // log.info("Se han encontrado [" + list.size() + "] cobros para [" + email + "]");
                    for (OnDemandBilling ob : list) {
                        CurrencyRequest cr = createCurrencyRequest(ob, email);
                        createWalletTransaction(cr, user, client);
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar cobros por on demand para: [" + email + "]. " + ex);
            }
        }
    }

    private void processFundsByMsisdns(User client, User user, boolean ondemand = true) {
        Set<String> keys = user.getCredentialUserProperty().getUsername().getMsisdns().keySet();

        if (!ondemand)
            syncSubscriptionsRun(user);

        for (String msisdn : keys) {
            Msisdn m = user.getCredentialUserProperty().getUsername().getMsisdns().get(msisdn);
            if (!m.getConfirmed()) {
                continue;
            }

            if (!ondemand) {
                Calendar c = Calendar.getInstance();
                c.add(Calendar.DATE, -1);
            }

            try {
                if (ondemand)
                    Subscription s = subscriptionService.getSubscriptionInformationFacadeRequest(msisdn, m.getCountry().getCode());
                else
                    Subscription s = subscriptionService.findByMSISDNAndCountry(msisdn, m.getCountry());

                if (s != null && s.getSubscriptionRegistry() != null) {
                    //   log.info("Suscripcion en PaymentHUb encontrada: [" + (s.getId() == null ? s : s.getId()) + "]");
                    if (ondemand)
                        List<SubscriptionBilling> list = subscriptionBillingService.getSubscriptionBillingsByUserAccount(s.getUserAccount());
                    else
                        List<SubscriptionBilling> list = subscriptionBillingService.getSubscriptionBillingsByUserAccount(c.getTime(), s.getUserAccount());

                    if (list != null && !list.isEmpty()) {
                        //   log.info("Se han encontrado [" + list.size() + "] cobros para [" + s.getUserAccount() + "]");
                        for (SubscriptionBilling sb : list) {
                            CurrencyRequest cr = createCurrencyRequest(sb, s.getUserAccount());
                            createWalletTransaction(cr, user, client);

                            try {
                                if (m.getProvider() == null || !sb.getSop().getProvider().getId().equals(m.getProvider().getId())) {
                                    m.setProvider(sb.getSop().getProvider());
                                    userService.saveOrUpdate(user);
                                }
                            } catch (Exception ex) {
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar cobros por suscripcion para: [" + msisdn + "]. " + ex);
            }

            try {
                String localLength = phprofilePropertiesService.getCommonProperty("msisdn." + m.getCountry().getCode().toLowerCase() + ".length.local");
                Integer msisdnLengthLocal = Integer.parseInt(localLength);
                msisdn = msisdn.replace(" ", "").replace("+", "");
                msisdn = msisdn.substring(Math.max(0, msisdn.length() - msisdnLengthLocal), msisdn.length());

                if (ondemand)
                    List<OnDemandBilling> list = onDemandBillingService.getByUserAccount(msisdn, m.getCountry());
                else
                    List<OnDemandBilling> list = onDemandBillingService.getByUserAccount(c.getTime(), msisdn, m.getCountry());

                if (list != null && !list.isEmpty()) {
                    //  log.info("Se han encontrado [" + list.size() + "] cobros on demand para [" + m.getMsisdn() + "]");
                    for (OnDemandBilling ob : list) {
                        if (ondemand) {
                            CurrencyRequest cr = createCurrencyRequest(ob, ob.getUserAccount());
                            OnDemandRegistry or = onDemandRegistryService.findById(ob.getId());

                            if (or != null && or.getTransactionTracking() != null) {
                                cr.tracking = or.getTransactionTracking().adTracking;
                                cr.description = or.getTransactionTracking().contentName;
                            }

                        }
                        else {
                            CurrencyRequest cr = createCurrencyRequest(ob);
                        }

                        try {
                            if (!m.getMsisdn().equals(ob.getUserAccount())) {
                                log.fatal("Hey!! estos MSISDNs son ligeramente diferentes: [" + m.getMsisdn() + "] [" + ob.getUserAccount() + "]");
                                //si ocurre el error ver de actualizar el MSISDN
                            }
                        } catch (Exception ex) {
                        }

                        try {
                            if (m.getProvider() == null || !ob.getSop().getProvider().getName().equals(m.getProvider().getName())) {
                                m.setCountry(ob.getSop().getProvider().getCountry());
                                m.setProvider(ob.getSop().getProvider());
                                userService.saveOrUpdate(user);
                            }
                        } catch (Exception ex) {
                        }

                        createWalletTransaction(cr, user, client);
                    }
                }
            } catch (Exception ex) {
                log.error("Error al procesar cobros por on demand para: [" + m.getMsisdn() + "]. " + ex);
            }
        }
    }

    private WalletUser createWalletUser(User client, User user) {
        //  log.info("Realmente no existe un WalletUser: [" + user.getId() + "]");
        wu = new WalletUser();
        wu.setId(user.getId());
        walleUserService.saveOrUpdate(wu);

        CurrencyRequest cr = new CurrencyRequest();
        cr.date = new Date();
        cr.localAmount = BigDecimal.valueOf(Long.parseLong(genericPropertyService.getCommonProperty("login.credits.start.value")));
        cr.localCurrency = genericPropertyService.getCommonProperty("login.credits.start.currency");
        cr.transactionId = "CLIENT_LOGIN_START_" + client.getId() + "_" + user.getId();
        cr.userAccount = "" + user.getId();

        if (BigDecimal.ZERO.compareTo(cr.localAmount) < 0) {
            String i18n = "es";
            if (user.getProfileUserProperty().getCountry() != null) {
                Country c = countryService.findById(user.getProfileUserProperty().getCountry().getId());
                if (c.getLanguages().size() > 1) {
                    Set<Language> list = new TreeSet();
                    list.addAll(c.getLanguages());
                    i18n = list.iterator().next().getCode().toLowerCase().trim() + "-" + c.getCode().toUpperCase().trim();
                } else {
                    i18n = c.getLanguages().iterator().next().getCode() + "-" + c.getCode().toUpperCase().trim();
                }
            }
            cr.description = i18n(i18n, null, "<i18n>i18n.ix.px.text.firts.login.bonus</i18n>");

            WalletTransaction wt = this.walletTransactionCreate(cr, user, client, WalletTransaction.Type.ADD);
            processWalletTransaction(wt);
        }

        return walleUserService.findById(user.getId());
    }

    private Map<String, BigDecimal> getCredits(User client, User user) {
        //Sumamos los creditos del usuario con base en los SOPs del producto y el ID del cliente
        for (Product p : client.getProducts()) {
            for (SOP sop : p.getSops()) {
                Credit c = wu.getCredits().get("SOP_" + sop.getId());
                if (c != null) {
                    Set<String> keys = c.getValues().keySet();
                    for (String currency : keys) {
                        BigDecimal bd = credits.get(currency);
                        if (bd == null) {
                            bd = new BigDecimal(BigInteger.ZERO);
                        }
                        bd = bd.add(c.getValues().get(currency));
                        credits.put(currency, bd);
                    }
                }
            }
        }

        Credit c = wu.getCredits().get("CLIENT_" + client.getId());
        if (c != null) {
            Set<String> keys = c.getValues().keySet();
            for (String currency : keys) {
                BigDecimal bd = credits.get(currency);
                if (bd == null) {
                    bd = new BigDecimal(BigInteger.ZERO);
                }
                bd = bd.add(c.getValues().get(currency));
                credits.put(currency, bd);
            }
        }
    }*/
}
