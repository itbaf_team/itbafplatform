package com.itbaf.platform.paymenthub.extention.smsc.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.paymenthub.extention.smsc.model.ActivosProductoResponse;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import java.io.IOException;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class SMSCResources {

    private final ObjectMapper mapper;
    private final RequestClient requestClient;
    private final PHProfilePropertiesService profilePropertiesService;

    @Inject
    public SMSCResources(final PHProfilePropertiesService profilePropertiesService,
            final ObjectMapper mapper,
            final RequestClient requestClient) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    public String subscriptionStatus(String msisdn, SOP sop) throws Exception {
        String url = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "smsc.ar.service.apiactivos.url");
        String user = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "smsc.ar.service.apiactivos.user");
        String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "smsc.ar.service.apiactivos.password");
        String productId = sop.getIntegrationSettings().get("smsc_productId");

        String serviceId = null;

        String response = null;
        url = url + "?user=" + user + "&pass=" + password + "&idProducto=" + productId + "&msisdn=" + msisdn;

        response = requestClient.requestJsonGet(url);
        log.info("subscriptionStatus. msisdn: [" + msisdn + "] - SOP: [" + sop.getId() + "]. SMSC response: [" + response + "]");
        ActivosProductoResponse resp = mapper.readValue(response, ActivosProductoResponse.class);
        if (resp.code != null) {
            if (resp.code == 1) {
                log.info("El MSISDN: [" + msisdn + "] no pertenece al proveedor: [" + sop.getProvider().getName() + "]");
                serviceId="1";
            } else {
                throw new Exception("Error subscriptionStatus. SMSC response: [" + response + "]. ");
            }
        } else if (resp.code == null && resp.totalActivos != null && resp.totalActivos > 0) {
            serviceId = resp.listado.get(0).idServicio;
        }

        return serviceId;
    }

    public ActivosProductoResponse subscriptionSync(SOP sop) throws Exception {
        String url = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "smsc.ar.service.apiactivos.url");
        String user = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "smsc.ar.service.apiactivos.user");
        String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), "smsc.ar.service.apiactivos.password");
        String productId = sop.getIntegrationSettings().get("smsc_productId");

        String response = null;
        try {
            url = url + "?user=" + user + "&pass=" + password + "&idProducto=" + productId;
            response = requestClient.requestJsonGet(url);
            return mapper.readValue(response, ActivosProductoResponse.class);
        } catch (IOException ex) {
            log.error("Error subscriptionStatus. SOP: [" + sop + "]. SMSC response: [" + response + "]. " + ex, ex);
        }

        return null;
    }
}
