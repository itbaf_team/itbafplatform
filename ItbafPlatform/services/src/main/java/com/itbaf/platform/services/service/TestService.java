/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.services.service;

import java.util.List;

/**
 *
 * @author javier
 * @param <T>
 */
public interface TestService<T> {

    public List<T> getAll();

}
