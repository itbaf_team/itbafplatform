package com.itbaf.platform.paymenthub.extention.npay.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.bn.model.NPAYNotification;
import com.itbaf.platform.paymenthub.extention.npay.model.AcknowledgeResponse;
import com.itbaf.platform.paymenthub.extention.npay.model.ServiceInformationResponse;
import com.itbaf.platform.paymenthub.extention.npay.model.ValidateSubscription;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.apache.commons.lang3.Validate;

@lombok.extern.log4j.Log4j2
public class NPAYResources {

    private final ObjectMapper mapper;
    private final RequestClient requestClient;

    @Inject
    public NPAYResources(final ObjectMapper mapper,
            final RequestClient requestClient) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
    }

    public boolean sendAcknowledge(NPAYNotification n, SOP sop) {

        if ("1".equals(sop.getIntegrationSettings().get("version"))) {
            try {
                String response = requestClient.requestJSONPost(buildStringForAcknowledge(n), null);
                log.info("sendAcknowledge. Respuesta de nPay entity: " + response);
                AcknowledgeResponse ack = mapper.readValue(response, AcknowledgeResponse.class);
                return "200".equals(ack.getMeta().code) && "VERIFIED".equals(ack.getData().message);
            } catch (Exception ex) {
                log.error("Error al procesar response. " + ex, ex);
            }
        }
        return false;
    }

    public ValidateSubscription validateSubscriptionByMsisdn(String msisdn, SOP sop) {
        try {
            String idService = sop.getIntegrationSettings().get("npay_id_service");
            String clientSecret = sop.getIntegrationSettings().get("client_secret");
            String url = buildStringForValidateSubscription(idService, clientSecret, msisdn, true);
            String response = requestClient.requestJsonGetNPAY(url);
            log.info("validateSubscriptionByMsisdn. ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. Respuesta de nPay: " + response);
            ValidateSubscription vs = null;
            try {
                vs = mapper.readValue(response, ValidateSubscription.class);
            } catch (Exception e) {
                log.error("Error validateSubscriptionByMsisdn [" + msisdn + "]. " + e, e);
            }

            return vs;
        } catch (Exception e) {
            throw new IllegalStateException("Error validateSubscriptionByMsisdn. Ani: [" + msisdn + "]. " + e);
        }
    }

    public ValidateSubscription validateSubscriptionByExternalId(String externalId, SOP sop) {
        try {
            String idService = sop.getIntegrationSettings().get("npay_id_service");
            String clientSecret = sop.getIntegrationSettings().get("client_secret");
            String url = buildStringForValidateSubscription(idService, clientSecret, externalId, false);
            String response = requestClient.requestJsonGetNPAY(url);
            log.info("validateSubscriptionByExternalId [" + externalId + "] - SOP: [" + sop.getId() + "]. Respuesta de nPay: " + response);
            ValidateSubscription vs = null;
            try {
                vs = mapper.readValue(response, ValidateSubscription.class);
            } catch (Exception e) {
                log.error("Error validateSubscriptionByExternalId [" + externalId + "]. " + e, e);
            }

            return vs;
        } catch (Exception e) {
            throw new IllegalStateException("Error validateSubscriptionByExternalId. externalId: [" + externalId + "]. " + e);
        }
    }

    public ValidateSubscription validateSubscriptionByExternalCustomer(String externalCustomer, SOP sop) {
        try {
            String idService = sop.getIntegrationSettings().get("npay_id_service");
            String clientSecret = sop.getIntegrationSettings().get("client_secret");
            String url = buildStringForValidateSubscription(idService, clientSecret, externalCustomer, null);
            String response = requestClient.requestJsonGetNPAY(url);
            log.info("validateSubscriptionByExternalCustomer [" + externalCustomer + "] - SOP: [" + sop.getId() + "]. Respuesta de nPay: " + response);
            if (response == null) {
                return null;
            }
            ValidateSubscription vs = null;
            try {
                vs = mapper.readValue(response, ValidateSubscription.class);
            } catch (Exception e) {
                log.error("Error validateSubscriptionByExternalCustomer [" + externalCustomer + "]. " + e, e);
            }

            return vs;
        } catch (Exception e) {
            throw new IllegalStateException("Error validateSubscriptionByExternalCustomer. externalId: [" + externalCustomer + "]. " + e);
        }
    }

    public String suscribe(Subscription subscription) {
        String msisdn = subscription.getUserAccount();
        SOP sop = subscription.getSop();
        String externalId = null;
        try {
            String url = buildStringForSubscribe(sop.getIntegrationSettings().get("client_secret"),
                    sop.getIntegrationSettings().get("npay_id_service"),
                    sop.getIntegrationSettings().get("keywordAlta"), msisdn);

            String response = requestClient.requestJSONPost(url, null);
            log.info("suscribe ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. Respuesta desde NPAY: [" + response + "]");
            ValidateSubscription vs = mapper.readValue(response, ValidateSubscription.class);

            if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                Date d = null;
                switch (vs.getData().status) {
                    case created:
                    case pending:
                        d = locateDate(vs.getData().created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                        subscription.setStatus(Subscription.Status.PENDING);
                        subscription.setSubscriptionDate(d);
                        subscription.setExternalCustomer(vs.getData().id_customer);
                        subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().id_subscription);
                        return vs.getData().id_subscription;
                    case active:
                    case successful_renewal:
                    case pending_renewal:
                        subscription.setStatus(Subscription.Status.ACTIVE);
                        d = locateDate(vs.getData().created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                        subscription.setSubscriptionDate(d);
                        subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                        subscription.setExternalCustomer(vs.getData().id_customer);
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().id_subscription);
                        return vs.getData().id_subscription;
                    case blacklist:
                        subscription.setStatus(Subscription.Status.BLACKLIST);
                        d = locateDate(vs.getData().created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                        subscription.setSubscriptionDate(d);
                        subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                        subscription.setExternalCustomer(vs.getData().id_customer);
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().id_subscription);
                        return null;
                    case locked_P001:
                    case locked_P002:
                    case locked_P003:
                        subscription.setStatus(Subscription.Status.LOCKED);
                        d = locateDate(vs.getData().created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                        subscription.setSubscriptionDate(d);
                        subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                        subscription.setExternalCustomer(vs.getData().id_customer);
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().id_subscription);
                        return null;
                }
            } else {
                log.error("Respuesta erronea desde NPAY. ANI: [" + msisdn + "]. SOP: [" + sop + "]");
            }

        } catch (Exception e) {
            log.error("Error al suscribir. ANI: [" + msisdn + "]. SOP: [" + sop + "]. " + e, e);
        }
        return externalId;
    }

    public Date locateDate(String dateMilis, TimeZone timeZone) {
        Calendar cx = Calendar.getInstance(timeZone);
        cx.setTimeInMillis(Long.parseLong(dateMilis));
        Calendar aux = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        aux.setTimeInMillis(cx.getTime().getTime());
        return aux.getTime();
    }

    public ResponseMessage.Status sendSubscriptionPin(String carrier, Subscription subscription) {
        return sendSubscriptionPin(carrier, subscription, subscription.getUserAccount(), 0);
    }

    private ResponseMessage.Status sendSubscriptionPin(String carrier, Subscription subscription, String msisdn, int count) {

        SOP sop = subscription.getSop();
        try {
            String url = buildStringForSendSubscriptionPin(
                    sop.getIntegrationSettings().get("npay_id_service"),
                    sop.getIntegrationSettings().get("client_secret"),
                    sop.getProvider().getCountry().getCode().toUpperCase(),
                    carrier, "web", sop.getIntegrationSettings().get("keywordAlta"), msisdn);

            String response = requestClient.requestJSONPost(url, null);
            log.info("sendSubscriptionPin. MSISDN: [" + msisdn + "]. SOP: [" + sop.getId() + "]. Respuesta desde NPAY: [" + response + "]");
            if (response == null) {
                return null;
            }

            ValidateSubscription vs = mapper.readValue(response, ValidateSubscription.class);
            if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                if ("true".equals(vs.getData().sent) && "pin".equals(vs.getData().object)) {
                    return ResponseMessage.Status.OK;
                } else {
                    Date d = null;
                    switch (vs.getData().status) {
                        case created:
                        case pending:
                            subscription.setStatus(Subscription.Status.PENDING);
                            d = locateDate(vs.getData().created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                            subscription.setSubscriptionDate(d);
                            subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                            return ResponseMessage.Status.PENDING;
                        case active:
                        case successful_renewal:
                        case pending_renewal:
                            subscription.setStatus(Subscription.Status.ACTIVE);
                            d = locateDate(vs.getData().created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                            subscription.setSubscriptionDate(d);
                            subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                            return ResponseMessage.Status.ACTIVE;
                        case blacklist:
                            subscription.setStatus(Subscription.Status.BLACKLIST);
                            d = locateDate(vs.getData().created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                            subscription.setSubscriptionDate(d);
                            subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                            return ResponseMessage.Status.BLACKLIST;
                        case locked_P001:
                        case locked_P002:
                        case locked_P003:
                            subscription.setStatus(Subscription.Status.LOCKED);
                            d = locateDate(vs.getData().created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                            subscription.setSubscriptionDate(d);
                            subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                            return ResponseMessage.Status.LOCKED;
                    }
                }
            } else {
                log.error("Error sendSubscriptionPin [" + msisdn + "][" + sop.getId() + "]. Respuesta erronea desde NPAY: [" + response + "]");
                if ("Claro-AR".equals(carrier) && vs.getData().message.contains("Wrong msisdn format for country provided") && count < 2) {
                    msisdn = msisdn.replace("549", "54");
                    count++;
                    return sendSubscriptionPin(carrier, subscription, msisdn, count);
                }
            }
        } catch (Exception ex) {
            log.error("Error sendSubscriptionPin [" + msisdn + "][" + sop.getId() + "]. " + ex, ex);
        }
        return null;
    }

    public ResponseMessage validatePin(String carrier, Subscription subscription, String pin) {
        String msisdn = subscription.getUserAccount();
        return validatePin(carrier, subscription, pin, msisdn, 0);
    }

    private ResponseMessage validatePin(String carrier, Subscription subscription, String pin, String msisdn, int count) {
        SOP sop = subscription.getSop();

        ResponseMessage rm = new ResponseMessage();
        try {
            String url = buildStringForValidatePin(sop.getIntegrationSettings().get("npay_id_service"),
                    sop.getIntegrationSettings().get("client_secret"),
                    sop.getProvider().getCountry().getCode().toUpperCase(),
                    carrier, msisdn, pin);

            String response = requestClient.requestJSONPost(url, null);
            log.info("validatePin ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. PIN: [" + pin + "]. Respuesta desde NPAY: [" + response + "]");
            ValidateSubscription vs = mapper.readValue(response, ValidateSubscription.class);

            if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                Date d = null;
                switch (vs.getData().subscription.status) {
                    case created:
                    case pending:
                        subscription.setStatus(Subscription.Status.PENDING);
                        d = locateDate(vs.getData().subscription.created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                        subscription.setSubscriptionDate(d);
                        subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                        subscription.setExternalCustomer(vs.getData().subscription.id_customer);
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().subscription.id_subscription);
                        rm.status = ResponseMessage.Status.OK;
                        return rm;
                    case active:
                    case successful_renewal:
                    case pending_renewal:
                        subscription.setStatus(Subscription.Status.ACTIVE);
                        d = locateDate(vs.getData().subscription.created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                        subscription.setSubscriptionDate(d);
                        subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                        subscription.setExternalCustomer(vs.getData().subscription.id_customer);
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().subscription.id_subscription);
                        rm.status = ResponseMessage.Status.OK;
                        return rm;
                    case blacklist:
                        subscription.setStatus(Subscription.Status.BLACKLIST);
                        d = locateDate(vs.getData().subscription.created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                        subscription.setSubscriptionDate(d);
                        subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                        subscription.setExternalCustomer(vs.getData().subscription.id_customer);
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().subscription.id_subscription);
                        rm.status = ResponseMessage.Status.BLACKLIST;
                        return rm;
                    case locked_P001:
                    case locked_P002:
                    case locked_P003:
                        subscription.setStatus(Subscription.Status.LOCKED);
                        d = locateDate(vs.getData().subscription.created + "000", TimeZone.getTimeZone("America/Mexico_City"));
                        subscription.setSubscriptionDate(d);
                        subscription.getSubscriptionRegistry().setSubscriptionDate(d);
                        subscription.setExternalCustomer(vs.getData().subscription.id_customer);
                        subscription.getSubscriptionRegistry().setExternalId(vs.getData().subscription.id_subscription);
                        rm.status = ResponseMessage.Status.LOCKED;
                        return rm;
                }
            } else if ("401".equals(vs.getMeta().code)
                    && ("Invalid PIN.".equals(vs.getData().message) || "Pin expired.".equals(vs.getData().message))) {
                rm.status = ResponseMessage.Status.PIN_ERROR;
                rm.message = vs.getData().message;
            } else {
                log.error("Error validatePin ANI: [" + msisdn + "]. SOP: [" + sop + "]. PIN: [" + pin + "]. Respuesta erronea desde NPAY: [" + response + "]");
                if ("Claro-AR".equals(carrier) && vs.getData().message.contains("Wrong msisdn format for country provided") && count < 2) {
                    msisdn = msisdn.replace("549", "54");
                    count++;
                    return validatePin(carrier, subscription, pin, msisdn, count);
                }
                rm.message = vs.getData().message;
            }

        } catch (Exception ex) {
            log.error("Error validatePin ANI: [" + msisdn + "]. SOP: [" + sop + "]. PIN: [" + pin + "]. " + ex, ex);
        }
        return rm;
    }

    public ResponseMessage.Status removeSubscription(Subscription subscription) {

        String msisdn = subscription.getUserAccount();
        SOP sop = subscription.getSop();
        try {
            String url = buildStringForRemoveSubscription(subscription.getSubscriptionRegistry().getExternalId(),
                    sop.getIntegrationSettings().get("client_secret"), sop.getIntegrationSettings().get("keywordBaja"));

            String response = requestClient.requestJSONPost(url, null);
            log.info("removeSubscription ANI: [" + msisdn + "]. SOP: [" + sop.getId() + "]. Respuesta desde NPAY: [" + response + "]");
            ValidateSubscription vs = mapper.readValue(response, ValidateSubscription.class);

            if ("200".equals(vs.getMeta().code) && "OK".equals(vs.getMeta().status)) {
                return ResponseMessage.Status.OK;
            } else {
                log.error("Error removeSubscription ANI: [" + msisdn + "]. SOP: [" + sop + "].");
            }

        } catch (Exception ex) {
            log.error("Error removeSubscription ANI: [" + msisdn + "]. SOP: [" + sop + "]. " + ex, ex);
        }
        return null;
    }

    public ServiceInformationResponse subscriptionServiceInformation(SOP sop) {

        String idService = sop.getIntegrationSettings().get("npay_id_service");
        String clientSecret = sop.getIntegrationSettings().get("client_secret");

        Object obj = MainCache.memory5Hours().getIfPresent(idService + "_" + clientSecret);

        if (obj == null) {
            String url = buildStringForSubscriptionServiceInformation(idService, clientSecret);
            try {
                String response = requestClient.requestJsonGetNPAY(url);
                ServiceInformationResponse sir = mapper.readValue(response, ServiceInformationResponse.class);
                if (sir.getData().id_service == null) {
                    return null;
                }
                MainCache.memory5Hours().put(idService + "_" + clientSecret, sir);
                return sir;
            } catch (Exception ex) {
                log.error("Error subscriptionServiceInformation al procesar response. sop: [" + sop + "]. " + ex, ex);
            }
        } else {
            return (ServiceInformationResponse) obj;
        }
        return null;
    }

    private String buildStringForSubscriptionServiceInformation(String idService, String clientSecret) {
        String uri = String.format("http://api.npay.io/subscriptions/v2/service/%s/?client_secret=%s",
                idService, clientSecret);
        return uri;
    }

    private String buildStringForValidateSubscription(String idService, String clientSecret, String user, Boolean isMsisdn) {
        String uri;
        if (isMsisdn == null) {//Por id_customer
            uri = String.format(
                    "http://api.npay.io/subscriptions/v3/service/%s/customer?client_secret=%s&id_customer=%s", idService,
                    clientSecret, user);
        } else if (isMsisdn) {//Por ani
            uri = String.format(
                    "http://api.npay.io/subscriptions/v3/service/%s/msisdn?client_secret=%s&msisdn=%s", idService,
                    clientSecret, user);
        } else {//Por externalId -> id_subscription
            uri = String.format(
                    "http://api.npay.io/subscriptions/v3/subscription/%s?client_secret=%s&id_service=%s", user,
                    clientSecret, idService);
        }
        return uri;
    }

    private String buildStringForSubscribe(String clientSecret, String idService, String keywordAlta, String msisdn) {
        return "http://api.npay.io/subscriptions/v2/subscription/cb/msisdn?"
                + "client_secret=" + clientSecret
                + "&id_service=" + idService
                + "&media=wap"
                + "&keyword=" + keywordAlta
                + "&msisdn=" + msisdn;
    }

    private String buildStringForSendSubscriptionPin(String idService, String clientSecret,
            String countryCode, String carrier, String media, String keywordAlta, String msisdn) {
        if ("AR".equals(countryCode.toUpperCase())) {
            return "http://api.npay.io/subscriptions/v1/pin/" + idService + "/send?"
                    + "client_secret=" + clientSecret
                    + "&country=" + countryCode
                    + "&carrier=" + carrier
                    + "&media=" + media
                    + "&msisdn=" + msisdn;
        }
        return "http://api.npay.io/subscriptions/v2/pin/" + idService + "/send?"
                + "client_secret=" + clientSecret
                + "&country=" + countryCode
                + "&carrier=" + carrier
                + "&media=" + media
                + "&keyword=" + keywordAlta
                + "&msisdn=" + msisdn;

    }

    private String buildStringForValidatePin(String idService, String clientSecret, String countryCode, String carrier, String msisdn, String pin) {
        if ("AR".equals(countryCode.toUpperCase())) {
            return "http://api.npay.io/subscriptions/v1/pin/" + idService + "/validate?"
                    + "client_secret=" + clientSecret
                    + "&country=" + countryCode
                    + "&carrier=" + carrier
                    + "&msisdn=" + msisdn
                    + "&pin=" + pin;
        }
        return "http://api.npay.io/subscriptions/v2/pin/" + idService + "/validate?"
                + "client_secret=" + clientSecret
                + "&country=" + countryCode
                + "&carrier=" + carrier
                + "&msisdn=" + msisdn
                + "&pin=" + pin;
    }

    private String buildStringForRemoveSubscription(String idSubscription, String clientSecret, String keywordBaja) {
        return "http://api.npay.io/subscriptions/v2/subscription/" + idSubscription + "/cancel?"
                + "client_secret=" + clientSecret
                + "&keyword=" + keywordBaja;
    }

    private String buildStringForAcknowledge(NPAYNotification n) {
        return "http://ipn.npay.io/verify?"
                + (n.getId_event() != null ? "id_event=" + n.getId_event() : "")
                + (n.getIpn_url() != null ? "&ipn_url=" + n.getIpn_url() : "")
                + (n.getIpn_type() != null ? "&ipn_type=" + n.getIpn_type() : "")
                + (n.getVerify_sign() != null ? "&verify_sign=" + n.getVerify_sign() : "")
                + (n.getId_app() != null ? "&id_app=" + n.getId_app() : "")
                + (n.getId_customer() != null ? "&id_customer=" + n.getId_customer() : "")
                + (n.getId_transaction() != null ? "&id_transaction=" + n.getId_transaction() : "")
                + (n.getAmount() != null ? "&amount=" + n.getAmount() : "")
                + (n.getCurrency() != null ? "&currency=" + n.getCurrency() : "")
                + (n.getId_subscription() != null ? "&id_subscription=" + n.getId_subscription() : "")
                + (n.getStatus() != null ? "&status=" + n.getStatus() : "")
                + (n.getId_service() != null ? "&id_service=" + n.getId_service() : "")
                + (n.getCreated() != null ? "&created=" + n.getCreated() : "")
                + (n.getUpdated() != null ? "&updated=" + n.getUpdated() : "");
    }

}
