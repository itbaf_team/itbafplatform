#!/bin/bash

for DEPLOYMENT in $(kubectl -n ph get deploy -o name)
do
    AVAILABLE_REPLICAS=$(kubectl -n ph get deployment.extensions/ph-npay -o jsonpath='{.status.availableReplicas}')
    #echo $DEPLOYMENT
    kubectl -n ph scale deploy $DEPLOYMENT --replicas 0 && \
        sleep 3 && \
        kubectl -n ph scale deploy $DEPLOYMENT --replicas $AVAILABLE_REPLICAS

done
