package com.itbaf.platform.persistence.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import static java.util.function.Function.identity;
import java.util.function.Supplier;

public final class Collections {
	private Collections() {}

	public static <E,K> Map<K,List<E>> groupBy(Iterable<E> elements, Function<? super E,? extends K> project) {
		return groupBy(elements, project, identity(), ArrayList<E>::new, (l,e) -> { l.add(e); return l; });
	}
	
	public static <E,K,V,P> Map<K,V> groupBy(Iterable<E> elements, Function<? super E,? extends K> projectKey, Function<? super E,? extends P> projectValue, Supplier<? extends V> initialValue, BiFunction<? super V, ? super P, ? extends V> reduce) {
		Map<K,V> result = new HashMap<>();
		for (E element : elements) {
			K key = projectKey.apply(element);
			P projection = projectValue.apply(element);
			result.putIfAbsent(key, initialValue.get());
			result.compute(key, (__, current) -> reduce.apply(current, projection) );
		}
		return result;
	}
}
