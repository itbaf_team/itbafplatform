/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.commons.api.message.udi;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class UdiMessage {

    public UdiGeo geo;
    public UdiPhone phone;
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("geo", geo)
                .add("phone", phone)
                .omitNullValues().toString();
    }
}
