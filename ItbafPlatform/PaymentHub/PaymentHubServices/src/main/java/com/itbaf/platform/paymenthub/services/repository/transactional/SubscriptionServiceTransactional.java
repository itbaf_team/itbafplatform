/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.services.repository.transactional;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.repository.jpa.model.JPASubscriptionRepository;
import com.itbaf.platform.paymenthub.services.repository.CountryService;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.SubscriptionService;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class SubscriptionServiceTransactional implements SubscriptionService {

    private final ObjectMapper mapper;
    private final RequestClient requestClient;
    private final CountryService countryService;
    private final JPASubscriptionRepository subscriptionRepository;
    private final PHProfilePropertiesService profilePropertiesService;

    @Inject
    public SubscriptionServiceTransactional(final ObjectMapper mapper,
            final RequestClient requestClient,
            final CountryService countryService,
            final JPASubscriptionRepository subscriptionRepository,
            final PHProfilePropertiesService profilePropertiesService) {
        this.mapper = Validate.notNull(mapper, "An ObjectMapper class must be provided");
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.countryService = Validate.notNull(countryService, "A CountryService class must be provided");
        this.subscriptionRepository = Validate.notNull(subscriptionRepository, "A JPASubscriptionRepository class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");

    }

    @Override
    public Subscription findByExternalId(String externalId, SOP sop) {
        return subscriptionRepository.findByExternalId(externalId, sop);
    }

    @Override
    public Subscription findByUserAccount(String userAccout, SOP sop) {
        return subscriptionRepository.findByUserAccount(userAccout, sop);
    }

    @Override
    public Subscription findByExternalUserAccount(String externalUserAccount, SOP sop) {
        return subscriptionRepository.findByExternalUserAccount(externalUserAccount, sop);
    }

    @Override
    public Subscription findByExternalCustomer(String externalCustomer, SOP sop) {
        return subscriptionRepository.findByExternalCustomer(externalCustomer, sop);
    }

    @Override
    public void saveOrUpdate(Subscription s) throws Exception {
        if (s.getId() == null) {
            subscriptionRepository.save(s);
        } else {
            subscriptionRepository.merge(s);
        }
    }

    @Override
    public List<Subscription> getSyncSubscriptions(Provider provider, Date time) {
        return subscriptionRepository.getSyncSubscriptions(provider, time);
    }

    @Override
    public void delete(Long id) throws Exception {
        subscriptionRepository.delete(id);
    }

    @Override
    public List<Subscription> findByUserAccount(String userAccout, String serviceCode) {
        return subscriptionRepository.findByUserAccount(userAccout, serviceCode);
    }

    private String validateMsisdn(String msisdn, Country country) {
        msisdn = msisdn.replace(" ", "").replace("+", "");

        String localLength = profilePropertiesService.getCommonProperty("msisdn."
                + country.getCode().toLowerCase() + ".length.local");

        if (localLength == null) {
            log.warn("No existe en DB la configuracion: [msisdn." + country.getCode().toLowerCase() + ".length.local]");
            return null;
        }
        Integer msisdnLengthLocal = Integer.parseInt(localLength);

        if (msisdn.length() < msisdnLengthLocal) {
            return null;
        }
        if (country.getPhoneCode() == null) {
            try {
                country = countryService.findById(country.getId());
            } catch (Exception ex) {
            }
        }
        if (msisdn.startsWith(country.getPhoneCode().toString())) {
            msisdn = msisdn.substring(country.getPhoneCode().toString().length(), msisdn.length());
        }
        msisdn = msisdn.substring(Math.max(0, msisdn.length() - msisdnLengthLocal), msisdn.length());

        if (msisdn.length() < msisdnLengthLocal) {
            return null;
        }
        return msisdn;
    }

    @Override
    public Subscription findByMSISDNAndCountry(String msisdn, Country country) {
        msisdn = validateMsisdn(msisdn, country);
        if (msisdn != null) {
            return subscriptionRepository.findByMSISDNAndCountry(msisdn, country);
        }
        return null;
    }

    @Override
    public List<Subscription> findSubscriptionsByProvider(String providerName, Date start, Date end) {
        return subscriptionRepository.findSubscriptionsByProvider(providerName, start, end);
    }

    @Override
    public List<Subscription> findSubscriptionsByUserAccount(String userAccount) {
        return subscriptionRepository.findSubscriptionsByUserAccount(userAccount);
    }

    @Override
    public Subscription findById(Long id) throws Exception {
        Subscription s = subscriptionRepository.findById(id);
        if (s != null && s.getSubscriptionRegistry() == null) {
            log.warn("Una suscripcion sin SubscriptionRegistry. [" + s + "]");
        }

        return s;
    }

    @Override
    public List<Subscription> getByExternalUserAccount(String externalUserAccount, SOP sop) {
        return subscriptionRepository.getByExternalUserAccount(externalUserAccount, sop);
    }

    @Override
    public void sendSMSFacadeRequest(String userName, SOP sop, String message) throws Exception {
        FacadeRequest fr = new FacadeRequest();
        fr.userAccount = userName;
        fr.service = sop.getServiceMatcher().getName();
        fr.operator = sop.getOperator().getName();
        fr.provider = sop.getProvider().getName();
        fr.message = message;
        String data = facadeRequest(fr, "subscription/sms/send");
    }

    @Override
    public Subscription getSubscriptionStatusFacadeRequest(String userName, SOP sop) throws Exception {
        FacadeRequest fr = new FacadeRequest();
        fr.userAccount = userName;
        fr.service = sop.getServiceMatcher().getName();
        fr.operator = sop.getOperator().getName();
        fr.provider = sop.getProvider().getName();
        String data = facadeRequest(fr, "subscription/status");
        if (data == null) {
            return null;
        }
        return mapper.readValue(data, Subscription.class);
    }

    @Override
    public Subscription getSubscriptionInformationFacadeRequest(String userName, String countryCode) throws Exception {
        FacadeRequest fr = new FacadeRequest();
        fr.userAccount = userName;
        fr.country = countryCode;
        String data = facadeRequest(fr, "subscription/status/information");
        if (data == null) {
            return null;
        }
        return mapper.readValue(data, Subscription.class);
    }

    @Override
    public List<Subscription> getSubscriptionCountryFacadeRequest(String userName, String countryCode) throws Exception {
        FacadeRequest fr = new FacadeRequest();
        fr.userAccount = userName;
        fr.country = countryCode;
        fr.action = "SubscriptionRegistry";

        String data = facadeRequest(fr, "subscription/status/country");
        if (data == null) {
            return null;
        }
        try {
            JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, Subscription.class);
            return mapper.readValue(data, type);
        } catch (Exception ex) {
        }

        return null;
    }

    @Override
    public void getSubscriptionCountryFacadeRequest(Map<String, String> userNameCountryCode) throws Exception {
        FacadeRequest fr = new FacadeRequest();
        fr.dataString = userNameCountryCode;
        facadeRequest(fr, "subscription/status/country/list");
    }

    @Override
    public ResponseMessage unsubscribeFacadeRequest(String userAccount, SOP sop, String channel) throws Exception {
        FacadeRequest fr = new FacadeRequest();
        fr.userAccount = userAccount;
        fr.service = sop.getServiceMatcher().getName();
        fr.operator = sop.getOperator().getName();
        fr.provider = sop.getProvider().getName();
        fr.channel = channel;

        String response = facadeRequest(fr, "unsubscription");
        if (response == null) {
            return null;
        }
        return mapper.readValue(response, ResponseMessage.class);
    }

    private String facadeRequest(FacadeRequest facadeRequest, String path) throws Exception {
        String phURL = profilePropertiesService.getGeneralProperty("paymenthub.server.facade") + path;
        log.info("Path phURL= "+phURL);
        String json = mapper.writeValueAsString(facadeRequest);
        log.info("facadeRequest json= "+json);
        String response = requestClient.requestJSONPostFacade(phURL, json);
        log.info("Facade response: [" + response + "]");

        if (response != null) {
            JsonNode data = mapper.readTree(response).findValue("data");
            if (data != null) {
                return mapper.readTree(response).findValue("data").toString();
            }
        }
        log.info("Facade Json response: [" + response + "]");

        return response;
    }

    @Override
    public List<Subscription> getActiveSubscriptions(List<String> userAccounts, Collection<SOP> sops) {
        return subscriptionRepository.getActiveSubscriptions(userAccounts, sops);
    }

    @Override
    public Subscription getLastSubscription(List<String> userAccounts, Collection<SOP> sops) {
        return subscriptionRepository.getLastSubscription(userAccounts, sops);
    }

    @Override
    public List<Subscription> getSubscriptionsWithoutSubscriptionRegistry(Provider provider) {
        return subscriptionRepository.getSubscriptionsWithoutSubscriptionRegistry(provider);
    }

    @Override
    public List<Subscription> getActiveSubscriptions(String userAccount, String providerName) {
        return subscriptionRepository.getActiveSubscriptions(userAccount, providerName);
    }

    @Override
    public List<Subscription> getByMSISDNAndCountry(String msisdn, Country country) {
        return subscriptionRepository.getByMSISDNAndCountry(msisdn, country);
    }

    @Override
    public List<BigInteger> getSubscriptionsRenew(SOP sop, Integer frecuency) {
        return subscriptionRepository.getSubscriptionsRenew(sop, frecuency);
    }
    
    @Override
    public List<BigInteger> getSubscriptionsMidRenew(SOP sop, Integer frecuency) {
        return subscriptionRepository.getSubscriptionsMidRenew(sop, frecuency);
    }

    @Override
    public List<BigInteger> getSubscriptionsMidRenewTwiceWeek(SOP sop) {
        return subscriptionRepository.getSubscriptionsMidRenewTwiceWeek(sop);
    }

    @Override
    public List<Subscription> getRetentionSubscriptionsBeforeDays(String providerName, int days) {
        return subscriptionRepository.getRetentionSubscriptionsBeforeDays(providerName, days);
    }

    @Override
    public List<Subscription> getActived24hsAndWithoutBilling() {
        return subscriptionRepository.getActived24hsAndWithoutBilling();
    }

}
