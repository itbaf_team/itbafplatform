/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.services.service;

/**
 *
 * @author javier
 */
public interface PropertyManagerService {

    /**
     * Retorna una propiedad de PH_PROFILE_PROPERTIES con base en el Perfil y el
     * Proveedor=1
     * @param key
     * @return 
     */
    public String getGeneralProperty(String key);
    
    /**
     * Retorna una propiedad comun para todos los perfiles (profile=all) y el 
     * Proveedor=1
     * @param providerId
     * @param key
     * @return 
     */
    public String getCommonProperty(String key);
    
    /**
     * Retorna una propiedad comun para todos los perfiles (profile=all) y para 
     * un Proveedor determinado
     * @param providerId
     * @param key
     * @return 
     */
    public String getCommonProperty(Long providerId, String key);
}
