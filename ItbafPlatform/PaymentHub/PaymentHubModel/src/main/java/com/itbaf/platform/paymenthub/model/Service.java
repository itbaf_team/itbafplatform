/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
@Entity(name = "SERVICE")
@Table(name = "SERVICE", indexes = {
    @Index(unique = true, columnList = "NAME")}, schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region="common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors=true)
public class Service  implements Serializable {
    private static final long serialVersionUID = 20170809155405L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE", nullable = false)
    private String code;
    
    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("code", code)
                .add("name", name)
                .add("description", description)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code,name, description);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<Service>()
                .over(Service::getId)
                .over(Service::getCode)
                .over(Service::getName)
                .over(Service::getDescription)
                .asserts(this, obj);
    }
}
