package com.itbaf.platform.paymenthub.model;

import com.google.common.base.MoreObjects;
import com.itbaf.platform.persistence.util.HibernateEquality;
import com.querydsl.core.annotations.Config;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;



@Entity(name = "SMS_SOP_MULTIPLE")
@Table(name = "SMS_SOP_MULTIPLE", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors = true)
public class SmsSopMultiple extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 3283730352855286686L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;

    @Column(name = "MULTIPLE_SMS", nullable = true)
    private Boolean multipleSms;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("sop", sop)
                .add("multipleSms", multipleSms)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sop, multipleSms);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<SmsSopMultiple>()
                .over(SmsSopMultiple::getId)
                .over(SmsSopMultiple::getSop)
                .over(SmsSopMultiple::getMultipleSms)
                .asserts(this, obj);
    }
}

