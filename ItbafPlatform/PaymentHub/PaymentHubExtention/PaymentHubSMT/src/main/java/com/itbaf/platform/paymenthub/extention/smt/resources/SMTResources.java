package com.itbaf.platform.paymenthub.extention.smt.resources;

import com.google.inject.Inject;
import com.itbaf.platform.commons.RequestClient;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.paymenthub.extention.smt.model.HeaderHandlerResolver;
import com.itbaf.platform.paymenthub.extention.smt.model.UsernameToken;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;
import org.csapi.schema.parlayx.common.v2_1.ChargingInformation;
import org.csapi.schema.parlayx.payment.amount_charging.v2_1.local.ChargeAmountResponse;
import org.csapi.schema.parlayx.payment.v2_1.Parameters;
import org.csapi.schema.parlayx.payment.v2_1.Property;
import org.csapi.schema.parlayx.sendpin.v1_0.local.ConfirmPINResponse;
import org.csapi.schema.parlayx.sendpin.v1_0.local.SendPINResponse;
import org.csapi.schema.parlayx.sms.send.v2_2.local.SendSmsResponse;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.SubScriptionInfoType;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.local.SubscibeProductResponse;
import org.csapi.schema.parlayx.subscribeproduct.v1_0.local.UnSubscribeProductResponse;
import org.csapi.wsdl.parlayx.payment.amount_charging.v2_1.service.AmountChargingService;
import org.csapi.schema.parlayx.common.v2_1.PolicyException;
import org.csapi.schema.parlayx.common.v2_1.ServiceException;
import org.csapi.wsdl.parlayx.sendpin.v1_0.service.SendPIN;
import org.csapi.wsdl.parlayx.sendpin.v1_0.service.SendPINService;

@lombok.extern.log4j.Log4j2
public class SMTResources {

    private final RequestClient requestClient;
    private final PHProfilePropertiesService profilePropertiesService;
    private static final String EXTERNAL_SERVICE_USERNAME = "external.service.username";
    private static final String EXTERNAL_SERVICE_PASSWORD = "external.service.password";

    @Inject
    public SMTResources(final RequestClient requestClient,
            final PHProfilePropertiesService profilePropertiesService) {
        this.requestClient = Validate.notNull(requestClient, "A RequestClient class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    private SendPIN getSendPin(Provider provider) {

        String username = profilePropertiesService.getCommonProperty(provider.getId(), EXTERNAL_SERVICE_USERNAME);
        String password = profilePropertiesService.getCommonProperty(provider.getId(), EXTERNAL_SERVICE_PASSWORD);
        HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver(username, password);
        SendPINService service = new SendPINService();
        service.setHandlerResolver(handlerResolver);
        return service.getSendPIN();
    }

    private int getChannelID(String channel) {
        int channelID = 1;

        if (channel == null) {
            return channelID;
        }
        switch (channel.toUpperCase().trim()) {
            case "WEB":
                channelID = 1;
                break;
            case "SMS":
                channelID = 2;
                break;
            case "IVR":
                channelID = 4;
                break;
            case "WAP":
                channelID = 7;
                break;
            case "OTHER":
                channelID = 99;
                break;
            case "SMT_CONSOLE":
                channelID = 102;
                break;
            case "SYSTEM":
                channelID = 128;
                break;
        }

        return channelID;
    }

    public int subscribeProduct(String msisdn, SOP sop, String channel) throws Exception {
        int channelID = getChannelID(channel);

        String url = profilePropertiesService.getCommonProperty("smt.service.url.subscription");
        String soap = buildSOAPStringForSubscribeProduct(msisdn, sop, channelID);
        log.info("subscribeProduct. SMT Request. URL: [" + url + "] - SOAP: [" + soap + "]");
        String result = requestClient.requestSOAP(url, soap);
        log.info("subscribeProduct. SMT Response. MSISDN: [" + msisdn + "] - SOAP: [" + result + "]");

        MessageFactory factory = MessageFactory.newInstance();
        SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(result.getBytes(Charset.forName("UTF-8"))));
        JAXBContext jbc = JAXBContext.newInstance(SubscibeProductResponse.class);
        Unmarshaller um = jbc.createUnmarshaller();
        JAXBElement<SubscibeProductResponse> element = um.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument(), SubscibeProductResponse.class);
        SubscibeProductResponse subscibeProductResponse = element.getValue();

        return subscibeProductResponse.getResultCode();
    }

    private UsernameToken getTokenHeader(SOP sop) throws Exception {
        ///////////////////////////////////////////////

        ///////////////////////////////////////////////
        String username = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), EXTERNAL_SERVICE_USERNAME);
        String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), EXTERNAL_SERVICE_PASSWORD);
        return new UsernameToken(username, password);
    }

    public void getSubscriptionList(String msisdn, SOP sop, Holder<Integer> resultCode,
            Holder<List<SubScriptionInfoType>> subScriptionInfos) throws Exception {

        int actionType = -1;
        String url = profilePropertiesService.getCommonProperty("smt.service.url.subscription");
        String soap = buildSOAPStringForSubscriptionList(msisdn, sop, actionType);
        log.info("getSubscriptionList. SMT Request. URL: [" + url + "] - SOAP: [" + soap + "]");
        String result = requestClient.requestSOAP(url, soap);
        log.info("getSubscriptionList. SMT Response. MSISDN: [" + msisdn + "] - SOAP: [" + result + "]");

    }

    public int unsubscribeProduct(String msisdn, SOP sop, String channel) throws Exception {

        int channelID = getChannelID(channel);

        String url = profilePropertiesService.getCommonProperty("smt.service.url.subscription");
        String soap = buildSOAPStringForUnsubscribeProduct(msisdn, sop, channelID);
        log.info("unsubscribeProduct. SMT Request. URL: [" + url + "] - SOAP: [" + soap + "]");
        String result = requestClient.requestSOAP(url, soap);
        log.info("unsubscribeProduct. SMT Response. MSISDN: [" + msisdn + "] - SOAP: [" + result + "]");

        // Cuando a api devuelve un error el body de respuesta no posee la estructura definida en UnSubscribeProductResponse
        try {
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(result.getBytes(Charset.forName("UTF-8"))));
            JAXBContext jbc = JAXBContext.newInstance(UnSubscribeProductResponse.class);
            Unmarshaller um = jbc.createUnmarshaller();
            JAXBElement<UnSubscribeProductResponse> element = um.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument(), UnSubscribeProductResponse.class);
            UnSubscribeProductResponse unSubscribeProductResponse = element.getValue();

            return unSubscribeProductResponse.getResultCode();

        } catch (Exception ex)
        {
            log.error("SMT Error al interpretar respuesta [" + result + "]", ex);
            return 9999;
        }
    }

    public void sendSms(String from, String to, String message, SOP sop) throws Exception {

        Integer esmClass = 11;
        Integer dataCoding = 11;

        String url = profilePropertiesService.getCommonProperty("smt.service.url.messaging");
        String soap = buildSOAPStringForSendSms(from, to, sop, message, esmClass, dataCoding);
        log.info("sendSms. SMT Request. URL: [" + url + "] - SOAP: [" + soap + "]");
        String result = requestClient.requestSOAP(url, soap);
        log.info("sendSms. SMT Response. [" + from + "] --> [" + to + "]: [" + result + "]");

        MessageFactory factory = MessageFactory.newInstance();
        SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(result.getBytes(Charset.forName("UTF-8"))));
        JAXBContext jbc = JAXBContext.newInstance(SendSmsResponse.class);
        Unmarshaller um = jbc.createUnmarshaller();
        JAXBElement<SendSmsResponse> element = um.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument(), SendSmsResponse.class);
        SendSmsResponse sendSmsResponse = element.getValue();
        sendSmsResponse.getResult();
    }

    public void sendPin(String msisdn, SOP sop, Holder<String> resultCode, Holder<String> resultDesc) throws Exception {

        String url = profilePropertiesService.getCommonProperty("smt.service.url.pin");
        String soap = buildSOAPStringForSendPin(msisdn, sop);
        log.info("SMT Request. URL: [" + url + "] - SOAP: [" + soap + "]");

        Map<String, String> headers = new HashMap();
        headers.put("SOAPAction", "");

        String result = requestClient.requestSOAPWithHeader(url, soap, headers);
        log.info("SMT Response. MSISDN: [" + msisdn + "] - SOAP: [" + result + "]");

        //Si certificado instalado en la instancia, utilizar metodo sendPin privado de arriba
        //Esto para probar XD
        result = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" >"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + "<loc:sendPINResponse xmlns:loc=\"http://www.csapi.org/schema/parlayx/sendpin/v1_0/local\">"
                + "<loc:resultCode>0</loc:resultCode>"
                + "<loc:resultDesc>success</loc:resultDesc>"
                + "</loc:sendPINResponse>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        MessageFactory factory = MessageFactory.newInstance();
        SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(result.getBytes(Charset.forName("UTF-8"))));
        JAXBContext jbc = JAXBContext.newInstance(SendPINResponse.class);
        Unmarshaller um = jbc.createUnmarshaller();
        JAXBElement<SendPINResponse> element = um.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument(), SendPINResponse.class);
        SendPINResponse sendPINResponse = element.getValue();

        resultCode.value = sendPINResponse.getResultCode();
        resultDesc.value = sendPINResponse.getResultDesc();
    }

    public void validatePin(String msisdn, SOP sop, String pin, Holder<String> resultCode, Holder<String> resultDesc, Holder<String> transactionID) throws Exception {
        String url = profilePropertiesService.getCommonProperty("smt.service.url.pin");
        String soap = buildSOAPStringForValidatePin(msisdn, sop, pin);
        log.info("SMT Request. URL: [" + url + "] - SOAP: [" + soap + "]");

        Map<String, String> headers = new HashMap();
        headers.put("SOAPAction", "");

        String result = requestClient.requestSOAPWithHeader(url, soap, headers);
        log.info("SMT Response. MSISDN: [" + msisdn + "] - SOAP: [" + result + "]");

        //Esto para probar XD
        /*
        result = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:loc=\"http://www.csapi.org/schema/parlayx/sendpin/v1_0/local\">"
                + "   <soapenv:Header/>"
                + "   <soapenv:Body>"
                + "      <loc:confirmPINResponse>"
                + "         <loc:resultCode>0</loc:resultCode>"
                + "         <loc:resultDesc>success</loc:resultDesc>"
                + "         <loc:transactionID>" + System.currentTimeMillis() + "</loc:transactionID>"
                + "      </loc:confirmPINResponse>"
                + "   </soapenv:Body>"
                + "</soapenv:Envelope>";*/

        MessageFactory factory = MessageFactory.newInstance();
        SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(result.getBytes(Charset.forName("UTF-8"))));
        JAXBContext jbc = JAXBContext.newInstance(ConfirmPINResponse.class);
        Unmarshaller um = jbc.createUnmarshaller();
        JAXBElement<ConfirmPINResponse> element = um.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument(), ConfirmPINResponse.class);
        ConfirmPINResponse confirmPINResponse = element.getValue();

        resultCode.value = confirmPINResponse.getResultCode();
        resultDesc.value = confirmPINResponse.getResultDesc();
        transactionID.value = confirmPINResponse.getTransactionID();
    }

    public String charge(String msisdn, SOP sop, FacadeRequest fr, String smsText, Holder<String> resultMessage) throws Exception {

        String url = profilePropertiesService.getCommonProperty("smt.service.url.chargeAmount");
        String soap = buildSOAPStringForCharge(msisdn, sop, fr, smsText);

        String result = null;
        try {
            /*result = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
                    + "<soapenv:Body><soapenv:Fault><faultcode>soapenv:Server</faultcode><faultstring>ServerException</faultstring><detail><ns2:ServiceException xmlns:ns2=\"http://www.csapi.org/schema/parlayx/common/v2_1\"><messageId>SVC0002</messageId>"
                    + "<text>Invalid input value for the message part retailPrice.</text></ns2:ServiceException></detail></soapenv:Fault></soapenv:Body></soapenv:Envelope>";
             */
            result = requestClient.requestSOAP(url, soap);
            log.info("SMT charge. MSISDN [" + msisdn + "] - SOP: [" + sop.getId() + "] - Request. URL: [" + url + "] - SOAP: [" + soap + "] - Response: [" + result + "]");
        } catch (Exception ex) {
            log.error("SMT charge. MSISDN [" + msisdn + "] - SOP: [" + sop.getId() + "] - Request. URL: [" + url + "] - SOAP: [" + soap + "] - Response: [" + result + "]. " + ex, ex);
        }
        if (result != null) {
            try {
                MessageFactory factory = MessageFactory.newInstance();
                SOAPMessage soapMessage = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(result.getBytes(Charset.forName("UTF-8"))));
                if (result.contains("chargeAmountResponse")) {
                    JAXBContext jbc = JAXBContext.newInstance(ChargeAmountResponse.class);
                    Unmarshaller um = jbc.createUnmarshaller();
                    JAXBElement<ChargeAmountResponse> element = um.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument(), ChargeAmountResponse.class);
                    ChargeAmountResponse chargeAmountResponse = element.getValue();
                    if (chargeAmountResponse != null) {
                        return "0";
                    }
                } else if (result.contains("PolicyException")) {
                    JAXBContext jbc = JAXBContext.newInstance(PolicyException.class);
                    Unmarshaller um = jbc.createUnmarshaller();
                    JAXBElement<PolicyException> element = um.unmarshal(soapMessage.getSOAPBody().getFault().getDetail().getFirstChild(), PolicyException.class);
                    PolicyException pe = element.getValue();
                    resultMessage.value = pe.getText();
                    return pe.getMessageId();
                } else if (result.contains("ServiceException")) {
                    JAXBContext jbc = JAXBContext.newInstance(ServiceException.class);
                    Unmarshaller um = jbc.createUnmarshaller();
                    JAXBElement<ServiceException> element = um.unmarshal(soapMessage.getSOAPBody().getFault().getDetail().getFirstChild(), ServiceException.class);
                    ServiceException se = element.getValue();
                    resultMessage.value = se.getText();
                    return se.getMessageId();
                }
            } catch (Exception ex) {
                log.error("Error al procesar SOAP: [" + result + "]. " + ex, ex);
            }
        }
        return null;
    }

    public void stopSMSNotification(SOP sop) throws Exception {
        UsernameToken token = getTokenHeader(sop);
        String soap = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                + "<soapenv:Header>"
                + "<tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\">"
                + "<tns:AppId>app_tt</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId>"
                + "<tns:token/>"
                + "</tns:RequestSOAPHeader>"
                + "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
                + "<wsse:Username>" + token.getUsername() + "</wsse:Username>"
                + "<wsse:Password Type=\"...#PasswordDigest\">" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce>" + token.getNonce() + "</wsse:Nonce>"
                + "<wsse:Created>" + token.getCreated() + "</wsse:Created>"
                + "</wsse:UsernameToken>"
                + "</wsse:Security>"
                + "</soapenv:Header>"
                + "<soapenv:Body>"
                + "<ns3:stopSmsNotification xmlns:ns3=\"http://www.csapi.org/schema/parlayx/sms/notification_manager/v2_3/local\">"
                + "<correlator>741</correlator>"
                + "</ns3:stopSmsNotification>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        String url = "https://200.95.168.211:443/asg/services/SmsNotificationManagerService";
        String result = null;
        try {
            result = requestClient.requestSOAP(url, soap);
            log.info("SMT stopSMSNotification. SOP: [" + sop.getId() + "] - Request. URL: [" + url + "] - SOAP: [" + soap + "] - Response: [" + result + "]");
        } catch (Exception ex) {
            log.error("SMT stopSMSNotification. SOP: [" + sop.getId() + "] - Request. URL: [" + url + "] - SOAP: [" + soap + "] - Response: [" + result + "]. " + ex, ex);
        }

    }

    public void startSMSNotification(SOP sop) throws Exception {
        UsernameToken token = getTokenHeader(sop);
        String soap = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                + "<soapenv:Header>"
                + "<tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\">"
                + "<tns:AppId>app_tt</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId>"
                + "<tns:token/>"
                + "</tns:RequestSOAPHeader>"
                + "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
                + "<wsse:Username>" + token.getUsername() + "</wsse:Username>"
                + "<wsse:Password Type=\"...#PasswordDigest\">" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce>" + token.getNonce() + "</wsse:Nonce>"
                + "<wsse:Created>" + token.getCreated() + "</wsse:Created>"
                + "</wsse:UsernameToken>"
                + "</wsse:Security>"
                + "</soapenv:Header>"
                + "<soapenv:Body>"
                + "<ns3:startSmsNotification xmlns:ns3=\"http://www.csapi.org/schema/parlayx/sms/notification_manager/v2_3/local\">"
                + "<ns3:reference>"
                + "<endpoint>http://api.hztm.mobi:8080/soap/notification/co/smt/sms</endpoint>"
                + "<interfaceName>startSmsNotification</interfaceName>"
                + "<correlator>12977</correlator>" //Aleatorio, recordar para el stop
                + "</ns3:reference>"
                + "<ns3:smsServiceActivationNumber>5737223</ns3:smsServiceActivationNumber>"
                + "<ns3:criteria></ns3:criteria>"
                + "</ns3:startSmsNotification>"
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";

        String url = "https://200.95.168.211:443/asg/services/SmsNotificationManagerService";
        String result = null;
        try {
            result = requestClient.requestSOAP(url, soap);
            log.info("SMT startSMSNotification. SOP: [" + sop.getId() + "] - Request. URL: [" + url + "] - SOAP: [" + soap + "] - Response: [" + result + "]");
        } catch (Exception ex) {
            log.error("SMT startSMSNotification. SOP: [" + sop.getId() + "] - Request. URL: [" + url + "] - SOAP: [" + soap + "] - Response: [" + result + "]. " + ex, ex);
        }
    }


    /*Para obtener SOAP*/
    private String chargex(String msisdn, SOP sop, FacadeRequest fr, String smsText, Holder<String> resultMessage) throws Exception {
        String contentDescription = fr.contentName;
        if (contentDescription.length() > 100) {
            contentDescription = contentDescription.substring(0, 100);
        }

        String contentId = fr.contentId;
        if (contentId.length() > 20) {
            contentId = contentId.substring(0, 20);
        }

        String username = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), EXTERNAL_SERVICE_USERNAME);
        String password = profilePropertiesService.getCommonProperty(sop.getProvider().getId(), EXTERNAL_SERVICE_PASSWORD);
        HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver(username, password);
        AmountChargingService service = new AmountChargingService();
        service.setHandlerResolver(handlerResolver);
        try {
            ChargingInformation ci = new ChargingInformation();
            ci.setAmount(fr.amount);
            ci.setCurrency(fr.currency);
            ci.setDescription(fr.contentName);
            Parameters pr = new Parameters();
            Property p = new Property();
            p.setName("providerId");
            p.setValue(sop.getIntegrationSettings().get("providerId"));
            pr.getParam().add(p);
            p = new Property();
            p.setName("serviceType");
            p.setValue(sop.getIntegrationSettings().get("serviceID"));
            pr.getParam().add(p);
            p = new Property();
            p.setName("contentId");
            p.setValue(contentId);
            pr.getParam().add(p);
            p = new Property();
            p.setName("contentDescription");
            p.setValue(contentDescription);
            pr.getParam().add(p);

            service.getAmountCharging().chargeAmount(msisdn, ci, pr, fr.transactionId);
            return "0";
        } catch (org.csapi.wsdl.parlayx.payment.amount_charging.v2_1.service.PolicyException ex) {
            resultMessage.value = ex.getFaultInfo().getText();
            return ex.getFaultInfo().getMessageId();
        } catch (org.csapi.wsdl.parlayx.payment.amount_charging.v2_1.service.ServiceException ex) {
            resultMessage.value = ex.getFaultInfo().getText();
            return ex.getFaultInfo().getMessageId();
        }
    }

    private String buildSOAPStringForCharge(String msisdn, SOP sop, FacadeRequest fr, String smsText) throws Exception {

        String contentType = "";

        String contentDescription = fr.contentName;
        if (contentDescription.length() > 100) {
            contentDescription = contentDescription.substring(0, 100);
        }

        String contentId = fr.contentId;
        if (contentId.length() > 20) {
            contentId = contentId.substring(0, 20);
        }

        UsernameToken token = getTokenHeader(sop);
        String soap = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
                + "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soapenv:Header><tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/common/v2_1\">"
                + "<tns:AppId>ITBAF_APP</tns:AppId><tns:TransId>" + token.getRandTransId() + "</tns:TransId></tns:RequestSOAPHeader>"
                + "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">"
                + "<wsse:Username>" + token.getUsername() + "</wsse:Username><wsse:Password Type=\"...#PasswordDigest\">" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce>" + token.getNonce() + "</wsse:Nonce><wsu:Created>" + token.getCreated() + "</wsu:Created></wsse:UsernameToken> </wsse:Security></soapenv:Header>"
                + "<soapenv:Body><chargeAmount xmlns=\"http://www.csapi.org/schema/parlayx/payment/amount_charging/v2_1/local\">"
                + "<endUserIdentifier>tel:" + msisdn + "</endUserIdentifier>"
                + "<charge><description xmlns=\"\">" + fr.contentName + "</description>"
                + "<currency xmlns=\"\">" + fr.currency + "</currency><amount xmlns=\"\">" + fr.amount.toPlainString() + "</amount></charge>"
                + "<extraParams>"
                + "<param xmlns=\"\"><name>serviceType</name><value>" + sop.getIntegrationSettings().get("serviceID") + "</value></param>"
                + "<param xmlns=\"\"><name>providerId</name><value>" + sop.getIntegrationSettings().get("providerId") + "</value></param>"
                + "<param xmlns=\"\"><name>contentId</name><value>" + contentId + "</value></param>"
                + "<param xmlns=\"\"><name>contentDescription</name><value>" + contentDescription + "</value></param>"
                + "<param xmlns=\"\"><name>retailPrice</name><value>" + fr.amount.toPlainString() + "</value></param>"
                + "<param xmlns=\"\"><name>calculatedPromo</name><value>0</value></param>"
                + "<param xmlns=\"\"><name>calculatedTax</name><value>0</value></param>"
                + "<param xmlns=\"\"><name>downloadFee</name><value>0</value></param>"
                + "</extraParams>"
                + "<referenceCode>" + fr.transactionId + "</referenceCode></chargeAmount></soapenv:Body></soapenv:Envelope>";

        return soap;
    }

    private String buildSOAPStringForSubscribeProduct(String msisdn, SOP sop, int channel) throws Exception {

        UsernameToken token = getTokenHeader(sop);
        String soap = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                + "<SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                + "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken><wsse:Username>" + token.getUsername() + "</wsse:Username><wsse:Password>" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce>" + token.getNonce() + "</wsse:Nonce><wsse:Created>" + token.getCreated() + "</wsse:Created>"
                + "</wsse:UsernameToken></wsse:Security><tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\"><tns:AppId>app_tt</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId><tns:token/></tns:RequestSOAPHeader></SOAP-ENV:Header>"
                + "<soap:Body><ns2:subscibeProduct xmlns:ns2=\"http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\">"
                + "<ns2:version>5.0</ns2:version><ns2:userID><ID>" + msisdn + "</ID><type>0</type></ns2:userID>"
                + "<ns2:subInfo><productID>" + sop.getIntegrationSettings().get("productID") + "</productID></ns2:subInfo>"
                + "<ns2:channelID>" + channel + "</ns2:channelID></ns2:subscibeProduct></soap:Body></soap:Envelope>";

        return soap;
    }

    private String buildSOAPStringForSubscriptionList(String msisdn, SOP sop, int actionType) throws Exception {

        UsernameToken token = getTokenHeader(sop);
        String soap = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken><wsse:Username>" + token.getUsername() + "</wsse:Username>"
                + "<wsse:Password>" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce>" + token.getNonce() + "</wsse:Nonce>"
                + "<wsse:Created>" + token.getCreated() + "</wsse:Created>"
                + "</wsse:UsernameToken></wsse:Security><tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\"><tns:AppId>app_tt</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId><tns:token/></tns:RequestSOAPHeader></SOAP-ENV:Header><soap:Body>"
                + "<ns2:getSubScriptionList xmlns:ns2=\"http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\">"
                + "<ns2:version>5.0</ns2:version><ns2:userID><ID>" + msisdn + "</ID><type>0</type></ns2:userID>"
                + "<ns2:actionType>" + actionType + "</ns2:actionType><ns2:extensionInfo>"
                + "<NamedParameters>"
                + "<key>productID</key>"
                + "<value>MDSP2000017421</value>"
                + "</NamedParameters>"
                + "</ns2:extensionInfo></ns2:getSubScriptionList></soap:Body></soap:Envelope>";

        return soap;
    }

    private String buildSOAPStringForUnsubscribeProduct(String msisdn, SOP sop, int channel) throws Exception {

        UsernameToken token = getTokenHeader(sop);

        String soap = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken><wsse:Username>" + token.getUsername() + "</wsse:Username>"
                + "<wsse:Password>" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce>" + token.getNonce() + "</wsse:Nonce>"
                + "<wsse:Created>" + token.getCreated() + "</wsse:Created>"
                + "</wsse:UsernameToken></wsse:Security><tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\"><tns:AppId>app_tt</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId>"
                + "<tns:token/></tns:RequestSOAPHeader></SOAP-ENV:Header><soap:Body>"
                + "<ns2:unSubscribeProduct xmlns:ns2=\"http://www.csapi.org/schema/parlayx/subscribeproduct/v1_0/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\"><ns2:version>5.0</ns2:version>"
                + "<ns2:userID><ID>" + msisdn + "</ID><type>0</type></ns2:userID><ns2:subInfo>"
                + "<productID>" + sop.getIntegrationSettings().get("productID") + "</productID></ns2:subInfo><ns2:channelID>" + channel + "</ns2:channelID></ns2:unSubscribeProduct></soap:Body></soap:Envelope>";

        return soap;
    }

    private String buildSOAPStringForSendSms(String from, String to, SOP sop, String message, int esm_class, int data_coding) throws Exception {

        UsernameToken token = getTokenHeader(sop);

        String soap = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken><wsse:Username>" + token.getUsername() + "</wsse:Username>"
                + "<wsse:Password>" + token.getPasswordDigest() + "</wsse:Password><wsse:Nonce>" + token.getNonce() + "</wsse:Nonce>"
                + "<wsse:Created>" + token.getCreated() + "</wsse:Created>"
                + "</wsse:UsernameToken></wsse:Security><tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\"><tns:AppId>app_tt</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId><tns:token/></tns:RequestSOAPHeader></SOAP-ENV:Header>"
                + "<soap:Body><ns2:sendSms xmlns:ns2=\"http://www.csapi.org/schema/parlayx/sms/send/v2_2/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\">"
                + "<ns2:addresses>" + to + "</ns2:addresses><ns2:senderName>" + from + "</ns2:senderName>"
                + "<ns2:message>" + message + "</ns2:message><ns2:esm_class>" + esm_class + "</ns2:esm_class>"
                + "<ns2:data_coding>" + data_coding + "</ns2:data_coding></ns2:sendSms></soap:Body></soap:Envelope>";

        return soap;
    }

    private String buildSOAPStringForSendPin(String msisdn, SOP sop) throws Exception {

        UsernameToken token = getTokenHeader(sop);

        String soap = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken><wsse:Username>" + token.getUsername() + "</wsse:Username><wsse:Password>" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce>" + token.getNonce() + "</wsse:Nonce><wsse:Created>" + token.getCreated() + "</wsse:Created></wsse:UsernameToken>"
                + "</wsse:Security><tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\"><tns:AppId>app_tt</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId><tns:token/></tns:RequestSOAPHeader></SOAP-ENV:Header><soap:Body>"
                + "<ns2:sendPIN xmlns:ns2=\"http://www.csapi.org/schema/parlayx/sendpin/v1_0/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\">"
                + "<ns2:msisdn>" + msisdn + "</ns2:msisdn><ns2:productID>" + sop.getIntegrationSettings().get("productID") + "</ns2:productID></ns2:sendPIN></soap:Body></soap:Envelope>";

        return soap;
    }

    private String buildSOAPStringForValidatePin(String msisdn, SOP sop, String pin) throws Exception {

        UsernameToken token = getTokenHeader(sop);

        String soap = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">"
                + "<wsse:UsernameToken><wsse:Username>" + token.getUsername() + "</wsse:Username><wsse:Password>" + token.getPasswordDigest() + "</wsse:Password>"
                + "<wsse:Nonce>" + token.getNonce() + "</wsse:Nonce><wsse:Created>" + token.getCreated() + "</wsse:Created></wsse:UsernameToken></wsse:Security>"
                + "<tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\"><tns:AppId>app_tt</tns:AppId>"
                + "<tns:TransId>" + token.getRandTransId() + "</tns:TransId><tns:token/></tns:RequestSOAPHeader></SOAP-ENV:Header><soap:Body>"
                + "<ns2:confirmPIN xmlns:ns2=\"http://www.csapi.org/schema/parlayx/sendpin/v1_0/local\" xmlns:ns3=\"http://www.csapi.org/schema/parlayx/common/v2_1\">"
                + "<ns2:pin>" + pin + "</ns2:pin><ns2:msisdn>" + msisdn + "</ns2:msisdn><ns2:productID>" + sop.getIntegrationSettings().get("productID") + "</ns2:productID></ns2:confirmPIN></soap:Body></soap:Envelope>";
        return soap;
    }
}
