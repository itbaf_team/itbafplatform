#!/bin/bash
# Revisar la url de rabbit en la base de datos pg_schema
# UPDATE  paymentHub.PH_PROFILE_PROPERTIES SET VALUE ='itbafrabbitmq' WHERE KEY = 'rabbitmq.url'
docker run --rm -p 8071:8070 \
--name=ph-tiaxa \
-e "paymenthub.datasource.url=jdbc:mysql://itbafmysql:3306/paymentHub?verifyServerCertificate=false&useSSL=false&requireSSL=false&useUnicode=true&characterEncoding=UTF-8&characterSetResult=UTF-8" \
-v "$PWD":/usr/src/myapp \
-w /usr/src/myapp \
--network=paymenthub_itbaf_red \
openjdk:11-jre java -jar PaymentHub/PaymentHubExtention/PaymentHubTiaxa/target/PaymentHubTiaxa.jar