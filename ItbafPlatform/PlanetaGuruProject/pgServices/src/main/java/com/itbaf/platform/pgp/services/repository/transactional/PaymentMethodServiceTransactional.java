/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository.transactional;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.pgp.model.wallet.PaymentMethod;
import com.itbaf.platform.pgp.repository.jpa.model.wallet.JPAPaymentMethodRepository;
import com.itbaf.platform.pgp.services.repository.PaymentMethodService;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;

/**
 *
 * @author javier
 */
@Transactional
@Singleton
@lombok.extern.log4j.Log4j2
public class PaymentMethodServiceTransactional implements PaymentMethodService {

    private final JPAPaymentMethodRepository paymentMethodRepository;

    @Inject
    public PaymentMethodServiceTransactional(final JPAPaymentMethodRepository paymentMethodRepository) {
        this.paymentMethodRepository = Validate.notNull(paymentMethodRepository, "A JPAPaymentMethodRepository class must be provided");
    }

    @Override
    public PaymentMethod findById(Long id) throws Exception {
        return paymentMethodRepository.findById(id);
    }

    @Override
    public List<PaymentMethod> getAll() {
        try {
            return paymentMethodRepository.getAll();
        } catch (Exception ex) {
            log.error("Error al obtener PaymentMethods. " + ex, ex);
        }
        return new ArrayList();
    }

    @Override
    public List<PaymentMethod> findByCountry(Country country) throws Exception {
        return paymentMethodRepository.findByCountry(country);
    }
}
