/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.model.subscription;

import com.itbaf.platform.paymenthub.model.*;
import com.google.common.base.MoreObjects;
import com.itbaf.platform.messaging.rest.FacadeRequest;
import com.itbaf.platform.persistence.util.HibernateEquality;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.querydsl.core.annotations.Config;

/**
 *
 * @author javier
 */
@Entity(name = "SUBSCRIPTION")
@Table(name = "SUBSCRIPTION", schema = "paymentHub", catalog = "paymentHub")
@Cacheable(true)
@Cache(region = "common", usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@lombok.Setter
@lombok.Getter
@Config(entityAccessors = true)
public class Subscription implements Serializable {

    private static final long serialVersionUID = 20170809155412L;

    public enum Status {
        ACTIVE, PENDING, REMOVED, BLACKLIST, CANCELLED,
        TRIAL, LOCKED, ADN_LOCKED, PORTING, RETENTION
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "USER_ACCOUNT")
    private String userAccount;
    // Identificador unico del userAccount en el proveedor (Unico por EXTERNAL_CUSTOMER)
    @Column(name = "EXTERNAL_USER_ACCOUNT")
    private String externalUserAccount;
    // Identificador del Cliente en el proveedor
    @Column(name = "EXTERNAL_CUSTOMER")
    private String externalCustomer;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private Status status;
    @OneToOne(optional = false)
    @JoinColumn(name = "SOP_ID")
    private SOP sop;
    @OneToOne
    @JoinColumn(name = "SUBSCRIPTION_REGISTRY_ID")
    private SubscriptionRegistry subscriptionRegistry;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SUBSCRIPTION_DATE")
    private Date subscriptionDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UNSUBSCRIPTION_DATE")
    private Date unsubscriptionDate;
    @Transient
    private String thread = Thread.currentThread().getName();
    @Transient
    private Object object;
    @Transient
    private Object lock;
    @Transient
    private FacadeRequest facadeRequest;
    @Transient
    private boolean charged = false;
    @Transient
    private String backUrl;//URL que redirecciona el operador despues suscribir/cobrar
    @Transient
    private String externalUrl;//URL para suscribir/cobrar con MP

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("id", id)
                .add("userAccount", userAccount)
                .add("externalUserAccount", externalUserAccount)
                .add("externalCustomer", externalCustomer)
                .add("status", status)
                .add("sop", sop)
                .add("subscriptionDate", subscriptionDate)
                .add("unsubscriptionDate", unsubscriptionDate)
                .add("subscriptionRegistry", subscriptionRegistry)
                .add("facadeRequest", facadeRequest)
                .add("thread", thread)
                .add("charged", charged)
                .add("backUrl", backUrl)
                .add("externalUrl", externalUrl)
                .add("object", object)
                .omitNullValues().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, externalUserAccount, externalCustomer, status, sop, subscriptionDate, subscriptionRegistry, unsubscriptionDate, userAccount);
    }

    @Override
    public boolean equals(Object obj) {
        return new HibernateEquality<Subscription>()
                .over(Subscription::getId)
                .over(Subscription::getSop)
                .over(Subscription::getStatus)
                .over(Subscription::getSubscriptionDate)
                .over(Subscription::getSubscriptionRegistry)
                .over(Subscription::getUserAccount)
                .over(Subscription::getUnsubscriptionDate)
                .over(Subscription::getExternalUserAccount)
                .over(Subscription::getExternalCustomer)
                .asserts(this, obj);
    }
}
