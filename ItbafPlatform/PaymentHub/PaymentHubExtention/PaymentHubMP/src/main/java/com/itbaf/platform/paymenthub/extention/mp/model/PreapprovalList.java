/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;

/**
 *
 * @author JF
 */
public class PreapprovalList {

    public Paging paging;
    public Preapproval[] results;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("paging", paging)
                .add("results", results)
                .omitNullValues().toString();
    }
}
