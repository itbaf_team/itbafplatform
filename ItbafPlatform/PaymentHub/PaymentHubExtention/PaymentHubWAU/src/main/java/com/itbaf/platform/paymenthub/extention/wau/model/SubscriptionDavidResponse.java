package com.itbaf.platform.paymenthub.extention.wau.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.List;

@JacksonXmlRootElement(localName = "response_data")
public class SubscriptionDavidResponse {

    @JacksonXmlElementWrapper
    private String result_code;

    @JacksonXmlElementWrapper
    private String result_description;

    @JacksonXmlElementWrapper
    private Long pages;

    @JacksonXmlElementWrapper
    private String product_id;

    @JacksonXmlProperty(localName = "subscribers")
    @JacksonXmlElementWrapper(useWrapping = true)
    private List<Subscriber> subscribers;

    @JacksonXmlElementWrapper
    private String total;

    public SubscriptionDavidResponse() {
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public String getResult_description() {
        return result_description;
    }

    public void setResult_description(String result_description) {
        this.result_description = result_description;
    }

    public Long getPages() {
        return pages;
    }

    public void setPages(Long pages) {
        this.pages = pages;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public List<Subscriber> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<Subscriber> subscribers) {
        this.subscribers = subscribers;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

}
