
var host = window.location.protocol + "//" + window.location.host;
var canonical = $('link[rel=canonical]')[0].href;
var router = new Navigo(host, false);

!function () {
    router.on({'/account/profile': accountProfileInfo,
        '/pg/account/profile': accountProfileInfo,
        '/account/profile/edit/:path': accountProfileInfoEdit,
        '/pg/account/profile/edit/:path': accountProfileInfoEdit
    }).resolve();
    var html = tmpl($("#profilePicScript").html(), {"params": uparams, "urlPathname": window.location.pathname});
    $("#profilePic-module").html(html);
}();


function accountProfileInfo(params, query) {
    ajaxSt = true;
    var html = tmpl($("#profileInfoScript").html(), {"params": uparams});
    $("#profileInfo-module").html(html);
}

function accountProfileInfoEdit(params, query) {
    ajaxSt = true;
    switch (params.path) {
        case "name":
            accountProfileInfoEditName();
            break;
        case "nickname":
            ajaxSt = false;
            accountProfileInfoEditNickname();
            break;
        case "picture":
        case "avatar":
            accountProfileInfoEditImage(params.path);
            break;
        case "personal":
            accountProfileInfoEditPersonal();
            break;
        default:
            window.location = canonical;
    }
}

function accountProfileInfoEditPersonal() {
    var ru = getParameterByName("ru");
    var html = tmpl($("#profileEditPersonalScript").html(), {"params": uparams});
    $("#profileInfo-module").html(html);

    if (uparams.birthdate) {
        $('#BirthMonth').val(uparams.birthdate.month);
        $('#BirthDay').val(uparams.birthdate.day);
        $('#BirthYear').val(uparams.birthdate.year);
    }
    if (uparams.gender) {
        $('#Gender').val(uparams.gender);
    }
    if (uparams.country) {
        $('#Country').val(uparams.country.code);
    }
    if (uparams.language) {
        $('#Language').val(uparams.language);
    }

    $("#idCancel").click(function (e) {
        redirectProfile(e, ru);
    });
    $('#BirthYear').change(function () {
        $("#BirthYear").removeClass("has-error");
        $("#idBirthdateErr").html("");
    });
    $('#BirthMonth').change(function () {
        $("#BirthMonth").removeClass("has-error");
        $("#idBirthdateErr").html("");
    });
    $('#BirthDay').change(function () {
        $("#BirthMonth").removeClass("has-error");
        $("#idBirthdateErr").html("");
    });
    $('#Gender').change(function () {
        $("#Gender").removeClass("has-error");
        $("#idGenderErr").html("");
    });
    $('#Country').change(function () {
        $("#Country").removeClass("has-error");
        $("#idCountryErr").html("");
    });
    $('#Language').change(function () {
        $("#Language").removeClass("has-error");
        $("#idLanguageErr").html("");
    });

    $("#idSave").click(function (e) {
        e.preventDefault();
        var error = true;
        var params = {};
        params.birthdate = {};
        params.birthdate.year = $("#BirthYear option:selected").val();
        params.birthdate.month = $("#BirthMonth option:selected").val();
        params.birthdate.day = $("#BirthDay option:selected").val();
        params.gender = $("#Gender option:selected").val();
        params.country={};
        params.country.code = $("#Country option:selected").val();
        params.language = $("#Language option:selected").val();

        if (params.birthdate.year === "" || params.birthdate.month === "" || params.birthdate.day === "") {
            html = tmpl($("#tiir").html(), {"params": params});
            $("#idBirthdateErr").html(html);
            if (params.birthdate.year === "") {
                $("#BirthYear").addClass("has-error");
            }
            if (params.birthdate.month === "") {
                $("#BirthMonth").addClass("has-error");
            }
            if (params.birthdate.day === "") {
                $("#BirthDay").addClass("has-error");
            }
            error = false;
        }
        if (params.gender === "") {
            html = tmpl($("#tiir").html(), {"params": params});
            $("#idGenderErr").html(html);
            $("#Gender").addClass("has-error");
            error = false;
        }
        if (params.country === "") {
            html = tmpl($("#tiir").html(), {"params": params});
            $("#idCountryErr").html(html);
            $("#Country").addClass("has-error");
            error = false;
        }
        if (params.language === "") {
            html = tmpl($("#tiir").html(), {"params": params});
            $("#idLanguageErr").html(html);
            $("#Language").addClass("has-error");
            error = false;
        }

        if (error) {
             try {
                params.userId = uparams.userId;
                params.e = e;
                params.ru = ru;
                soup(canonical + "/edit/personal", params);
            } catch (ex) {
                redirectProfile(e, ru);
            }
        }
    });
}

function accountProfileInfoEditImage(pathImg) {
    var ru = getParameterByName("ru");
    var html = tmpl($("#profileEditPictureScript").html(), {"params": uparams, "botonName": pathImg});
    $("#profilePicture-module").html(html);
    if ("picture" === pathImg) {
        uparams.tmpURL = uparams.photoURL;
    } else {
        uparams.tmpURL = uparams.avatarURL;
    }
    $(window).resize(function () {
        imageProperties(uparams.tmpURL);
    });

    imageProperties(uparams.tmpURL);

    $("#edit-picture-cancel").click(function (e) {
        redirectProfile(e, ru);
    });

    $("#edit-picture-upload").click(function (e) {
        $("#picture-picker").trigger('click');
    });

    $('#picture-picker').change(function (e) {

        const file = e.target.files[0];
        if (!file) {
            return;
        }
        uparams.tmpFile = file;
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function (e) {
            uparams.tmpURL = e.target.result;
            imageProperties(e.target.result);
            $('#edit-picture-save').removeClass("hidden");
        };
    });

    $('#edit-picture-save').click(function (e) {
        e.preventDefault();

        new ImageCompressor(uparams.tmpFile, {
            quality: .8,
            maxWidth: 900,
            maxHeight: 900,
            success(result) {
                var data = new FormData();
                data.append('pictureFile', result, result.name);
                data.append('properties', '{"size":"' + result.size + '","type":"' + result.type + '"}');
                jQuery.ajax({
                    url: $('#picture-upload-form').attr("action"),
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        redirectProfile(e, ru);
                    }
                });
            },
            error(e) {
                console.log(e.message);
                redirectProfile(e, ru);
            }
        });

        // $('#picture-upload-form').submit();
    });

    function imageProperties(urlImage) {
        var image = new Image();
        image.src = urlImage;
        $('#img-pic').attr('src', image.src);
        $('#img-pic-bg').attr('src', image.src);
        image.onload = function () {
            var height = this.height;
            var width = this.width;
            if (height > 512) {
                width = (width * 512) / height;
                height = 512;
            }

            var peWidth = $('#picture-editor').width();
            if (width > peWidth) {
                height = (height * peWidth) / width;
                width = peWidth;
            }

            var params = {};
            params.url = image.src;
            params.width = width;
            params.height = height;
            params.c = {};
            params.c.l = 0;
            params.c.p = 0;
            if (width > height) {
                params.c.r = height;
                params.c.l = (width / 2) - (height / 2);
            } else {
                params.c.r = width;
                params.c.p = (height / 2) - (width / 2);
            }

            html = tmpl($("#profileEditPictureEditorScript").html(), {"params": params});
            $("#picture-editor-owner").html(html);


            return true;
        };

    }
}

function accountProfileInfoEditNickname() {
    var ru = getParameterByName("ru");
    var html = tmpl($("#profileEditNicknameScript").html(), {"params": uparams});
    $("#profileInfo-module").html(html);

    $("#edit-nickname-cancel").click(function (e) {
        redirectProfile(e, ru);
    });

    $('#edit-nickname').keypress(function (e) {
        if (e.key === ' ' || e.key === '"' || e.key === "'" || e.key === '.' || e.key === '_') {
            e.preventDefault();
            e.stopPropagation();
        }
    });

    $('#edit-nickname').keyup(function (e) {
        if (e.which !== 0 && e.key !== 'Enter' && e.key !== ' ' && e.key !== '"' && e.key !== "'" && e.key !== '.' && e.key !== '_' && e.key !== 'Escape') {
            editNicknameKeyup();
        }
    });

    $('#edit-nickname').change(function () {
        editNicknameChange();
    });

    $('#edit-nickname-save').click(function (e) {
        ajaxSt = true;
        e.preventDefault();
        if (!editNicknameKeyup()) {
            try {
                var params = {};
                params.userId = uparams.userId;
                params.nickname = $("#edit-nickname").val().trim();
                params.e = e;
                params.ru = ru;
                soup(canonical + "/edit/nickname", params);
            } catch (ex) {
                redirectProfile(e, ru);
            }
        }
        document.getElementById("edit-nickname-save").disabled = true;
    });

    function editNicknameChange() {
        var params = {};
        params.userId = uparams.userId;
        params.nickname = $("#edit-nickname").val().trim();

        $.ajax({
            type: "POST",
            url: canonical + "/status/nickname",
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(params),
            success: function (data) {
                if (data && data.data && data.data.object && data.data.object.result === 'available') {
                    if (data.data.object.nickname === $("#edit-nickname").val().trim()) {
                        document.getElementById("edit-nickname-save").disabled = false;
                        $("#nickname-msg").removeClass("msg-6c");
                        $("#nickname-msg").addClass("msg-a");
                        $("#nickname-msg").removeClass("msg-na");
                    }
                } else {
                    $("#nickname-msg").removeClass("msg-6c");
                    $("#nickname-msg").removeClass("msg-a");
                    $("#nickname-msg").addClass("msg-na");
                    document.getElementById("edit-nickname-save").disabled = true;
                }
            },
            error: function (data) {
                console.log(data);
                document.getElementById("edit-nickname-save").disabled = true;
            },
            dataType: "json"
        });
    }

    function editNicknameKeyup() {
        var control = true;
        var value = $("#edit-nickname").val().trim();
        value = value.replace(/[.\'\"\ \_/|[\]\\]/g, "");
        $("#edit-nickname").val(value);
        if (value.length > 5 && value !== uparams.nickname) {
            control = false;
        }

        if (value.length > 5) {
            editNicknameChange();
        } else {
            $("#nickname-msg").addClass("msg-6c");
            $("#nickname-msg").removeClass("msg-a");
            $("#nickname-msg").removeClass("msg-na");
        }

        return control;
    }
}


function accountProfileInfoEditName() {
    var ru = getParameterByName("ru");
    var html = tmpl($("#profileEditNameScript").html(), {"params": uparams});
    $("#profileInfo-module").html(html);

    $("#edit-name-cancel").click(function (e) {
        redirectProfile(e, ru);
    });

    $('#edit-name-first').keyup(function () {
        editNameChange();
    });
    $('#edit-name-middle').keyup(function () {
        editNameChange();
    });
    $('#edit-name-last').keyup(function () {
        editNameChange();
    });

    $('#edit-name-save').click(function (e) {
        e.preventDefault();
        if (!editNameChange()) {
            try {
                var params = {};
                params.userId = uparams.userId;
                params.firstName = $("#edit-name-first").val().trim();
                params.middleName = $("#edit-name-middle").val().trim();
                params.lastName = $("#edit-name-last").val().trim();
                params.e = e;
                params.ru = ru;
                soup(canonical + "/edit/name", params);
            } catch (ex) {
                redirectProfile(e, ru);
            }
        }
        document.getElementById("edit-name-save").disabled = true;
    });

    function editNameChange() {

        var control = true;
        var first = $("#edit-name-first").val().trim();
        var middle = $("#edit-name-middle").val().trim();
        var last = $("#edit-name-last").val().trim();

        if (first !== null && first !== undefined && first.length > 0 && first !== uparams.firstName) {
            control = false;
        }
        if (middle !== null && middle !== undefined && middle !== uparams.middleName) {
            control = false;
        }
        if (last !== null && last !== undefined && last.length > 0 && last !== uparams.lastName) {
            control = false;
        }
        if ((first === null || first === undefined || last === null || last === undefined || first.length === 0 || last.length === 0)
                && middle === uparams.middleName) {
            control = true;
        }
        if (first.length === 0 && last.length === 0 && middle.length === 0) {
            control = false;
        }
        document.getElementById("edit-name-save").disabled = control;
        return control;
    }
}

function redirectProfile(e, ru) {
    e.preventDefault();
    var url = ru;
    if (!ru) {
        url = canonical;
    }
    window.location = url;
}

function soup(path, params) {
    $.ajax({
        type: "PUT",
        url: path,
        contentType: 'application/json; charset=UTF-8',
        data: JSON.stringify(params),
        success: function (data) {
            if (params.e)
                redirectProfile(params.e, params.ru);
        },
        error: function (data) {
            console.log(data);
            if (data && data.responseJSON && data.responseJSON.data && data.responseJSON.data.object) {
                if ("nickname:not_available" === data.responseJSON.data.object) {
                    console.log("Nickname is not available");
                }

            }
        },
        dataType: "json"
    });
}


