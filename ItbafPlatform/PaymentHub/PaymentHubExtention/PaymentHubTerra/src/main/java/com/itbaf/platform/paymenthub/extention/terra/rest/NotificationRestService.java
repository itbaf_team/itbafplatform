package com.itbaf.platform.paymenthub.extention.terra.rest;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.itbaf.platform.paymenthub.extention.terra.handler.ServiceProcessorHandler;
import com.itbaf.platform.paymenthub.extention.terra.resources.TerraNotification;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.commons.lang3.Validate;

@Path("/notification")
@Singleton
@lombok.extern.log4j.Log4j2
public class NotificationRestService {

    private final XmlMapper xmlMapper;
    private final ServiceProcessorHandler serviceProcessorHandler;

    @Inject
    public NotificationRestService(final XmlMapper xmlMapper,
            ServiceProcessorHandler serviceProcessorHandler) {
        this.serviceProcessorHandler = Validate.notNull(serviceProcessorHandler, "A ServiceProcessorHandler class must be provided");
        this.xmlMapper = xmlMapper;
    }

    @POST
    @Path("{country}/terra/event")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_XML)
    public Response terraNotificationPost(final @PathParam("country") String countryCode,
            @Context HttpServletRequest request) {

        String xml = "";

        try {
            xml = (String) request.getAttribute("notification");
            TerraNotification notification = xmlMapper.readValue(xml, TerraNotification.class);
            notification.fechaLocale = serviceProcessorHandler.locateDate(notification.fecha, countryCode);
            log.info("Notificacion de Terra: [" + countryCode + "]. XML: [" + xml + "] - Notification: [" + notification + "]");
            serviceProcessorHandler.processNotification(notification, countryCode);
        } catch (Exception ex) {
            log.error("No fue posible procesar el mensaje XML [" + xml + "]. " + ex, ex);
        }

        String response = "<Evento><Resultado>OK</Resultado><Descripcion></Descripcion></Evento>";
        return Response.status(Status.OK).entity(response).type(MediaType.APPLICATION_XML).build();
    }

}
