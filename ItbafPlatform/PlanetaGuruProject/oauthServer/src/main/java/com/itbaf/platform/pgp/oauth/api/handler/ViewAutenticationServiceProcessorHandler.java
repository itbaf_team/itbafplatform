/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.oauth.api.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.model.Person;
import com.google.api.services.plus.model.Person.Emails;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.auth.UserRecord;
import com.google.inject.Inject;
import com.itbaf.platform.commons.EmailUtility;
import com.itbaf.platform.commons.cache.MainCache;
import com.itbaf.platform.paymenthub.model.Country;
import com.itbaf.platform.paymenthub.model.Language;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.Status;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import com.itbaf.platform.paymenthub.services.repository.ProviderService;
import com.itbaf.platform.pgp.commons.resources.ClientCache;
import com.itbaf.platform.pgp.model.GenericProperty;
import com.itbaf.platform.pgp.model.Product;
import com.itbaf.platform.pgp.model.oauth.User;
import com.itbaf.platform.pgp.model.oauth.properties.ProfileUserProperty;
import com.itbaf.platform.pgp.model.oauth.properties.ProfileUserProperty.Gender;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.AgeRange;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Client;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Email;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.Msisdn;
import com.itbaf.platform.pgp.model.oauth.properties.attributes.OauthProvider;
import com.itbaf.platform.pgp.oauth.api.message.RequestBasicMessage;
import com.itbaf.platform.pgp.oauth.api.message.RequestBasicMessage.UsernameType;
import com.itbaf.platform.pgp.oauth.api.message.ResponseBasicMessage;
import com.itbaf.platform.pgp.services.ServiceProcessorHandler;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.lang.Collections;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Holder;
import org.apache.commons.lang3.Validate;
import org.redisson.api.RLock;

/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ViewAutenticationServiceProcessorHandler extends ServiceProcessorHandler {

    private final ClientCache clientCache;
    private final EmailUtility emailUtility;
    private final ProviderService providerService;
    private final PHProfilePropertiesService phprofilePropertiesService;
    private final AutenticationServiceProcessorHandler autenticationServiceProcessorHandler;
    private static final HttpTransport TRANSPORT = new NetHttpTransport();
    private static final JacksonFactory JSON_FACTORY = new JacksonFactory();

    @Inject
    public ViewAutenticationServiceProcessorHandler(final ClientCache clientCache,
            final EmailUtility emailUtility,
            final ProviderService providerService,
            final PHProfilePropertiesService phprofilePropertiesService,
            final AutenticationServiceProcessorHandler autenticationServiceProcessorHandler) {
        this.clientCache = Validate.notNull(clientCache, "A ClientCache class must be provided");
        this.emailUtility = Validate.notNull(emailUtility, "An EmailUtility class must be provided");
        this.providerService = Validate.notNull(providerService, "A ProviderService class must be provided");
        this.phprofilePropertiesService = Validate.notNull(phprofilePropertiesService, "A PHProfilePropertiesService class must be provided");
        this.autenticationServiceProcessorHandler = Validate.notNull(autenticationServiceProcessorHandler, "An autenticationServiceProcessorHandler class must be provided");
    }

    /**
     * Codigo para validar propiedad de email
     */
    private void sendEmailVerifyCode(User user, ResponseBasicMessage rm) {
        String code = clientCache.getTemporalData(user.getId() + "_ECODE_VERIFY");
        if (code == null || "xx".equals(code)) {
            code = generateDigitCode(6).toUpperCase();
            // log.info("ValidationCode: [" + code + "] - User: [" + user.getId() + "]");
            try {
                String html = readResource("/tpl/email/verifyEmailCode.html", validateTID(rm.tid));
                html = html.replace("<scode></scode>", code);
                String name = "User".equals(user.getProfileUserProperty().getFirstName()) ? "" : user.getProfileUserProperty().getFirstName();
                html = html.replace("<sname></sname>", name);
                html = localeService.i18n(rm.locale, 1L, html);
                emailUtility.sendHTMLEmail("no-reply@planeta.guru", rm.username, localeService.i18n(rm.locale, 1L, "<i18n>i18n.ix.px.email.verify.text.pvye</i18n>"), html);
                rm.credentials.hasPin = 1;
                clientCache.setTemporalData(user.getId() + "_ECODE_VERIFY", code, 40);
                clientCache.setTemporalData(user.getId() + "_" + code, code, 45);
            } catch (Exception ex) {
                log.error("Error al enviar EMAIL. " + ex, ex);
            }
        } else {
            // log.info("ValidationCode: [" + code + "] - User: [" + user.getId() + "]");
            rm.credentials.hasPin = 1;
        }
    }

    public ResponseBasicMessage validatePasswordOrCode(Map<String, String> prop,
            RequestBasicMessage requestBasicMessage, HttpServletRequest httpServletRequest) {

        ResponseBasicMessage rm = new ResponseBasicMessage();
        rm.ifExistsResult = 0;
        rm.username = requestBasicMessage.username;
        rm.usernameType = requestBasicMessage.usernameType;
        rm.tid = requestBasicMessage.tid;
        rm.locale = requestBasicMessage.locale;
        rm.credentials.error = 1;

        User cliente = userService.findByclientId(prop.get("clientId"));
        if (cliente == null) {
            log.error("No existe el clientId: [" + prop.get("clientId") + "]");
            return rm;
        }
        RLock lock = null;
        if (requestBasicMessage.username != null && !requestBasicMessage.username.isEmpty()) {
            lock = instrumentedObject.lockObject("user_lock_sou_" + requestBasicMessage.username, 10);
        }
        try {
            switch (requestBasicMessage.path) {
                case "reset":
                    //Setea un nuevo password al usuario
                    rm = validatePwdResetPassword(cliente, requestBasicMessage, rm);
                    break;
                case "code":
                    //Valida el Codigo de verificacion de la pagina de resetPassword
                    rm = validateCodeResetPassword(cliente, requestBasicMessage, rm);
                    break;
                case "login":
                    //Valida el Password o el Codigo de verificacion de la pagina de login
                    rm = validatePasswordOrCodeLogin(cliente, prop, requestBasicMessage, rm, httpServletRequest);
                    break;
                case "provider":
                    //Valida el Token proporsionado por el proveedor con Firebase
                    rm = validateProviderToken(cliente, prop, requestBasicMessage, rm, httpServletRequest);
                    break;
                case "spin":
                    //Envia un PIN por SMS con el proveedor-telco
                    sendSMSVerificationCode(cliente, prop, rm, httpServletRequest);
                    break;
                default:
                    break;

            }
            return rm;
        } finally {
            instrumentedObject.unLockObject(lock);
        }
    }

    private ResponseBasicMessage validateProviderToken(User cliente, Map<String, String> prop,
            RequestBasicMessage requestBasicMessage, ResponseBasicMessage rm, HttpServletRequest httpServletRequest) {
        RLock lock = null;
        if (requestBasicMessage.tokenCode != null) {
            try {
                FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdTokenAsync(requestBasicMessage.tokenCode).get();
                String uid = decodedToken.getUid();
                log.info("Google Firebase UserId: [" + uid + "]");
                UserRecord ur = FirebaseAuth.getInstance().getUserAsync(uid).get();
                /**
                 * Proveedores OAUTH
                 */
                User user = null;
                Person person = null;//Google API
                com.restfb.types.User fbUser = null; //FB
                /**
                 * ****************
                 */
                try {
                    switch (requestBasicMessage.provider) {
                        case "google.com":
                            GoogleCredential credential = new GoogleCredential.Builder()
                                    .setJsonFactory(JSON_FACTORY)
                                    .setTransport(TRANSPORT)
                                    .setClientSecrets(genericPropertyService.getCommonProperty("google.oauth.key.public"),
                                            genericPropertyService.getCommonProperty("google.oauth.key.secret"))
                                    .build()
                                    .setAccessToken(requestBasicMessage.ptokenCode);
                            // Create a new authorized API client.
                            Plus service = new Plus.Builder(TRANSPORT, JSON_FACTORY, credential)
                                    .setApplicationName("Planeta Guru")
                                    .build();
                            person = service.people().get("me").execute();

                            if (person == null) {
                                log.warn("No fue posible obtener el usuario de Google para: [" + requestBasicMessage.ptokenCode + "]");
                                break;
                            }

                            List<Emails> emails = person.getEmails();
                            if (emails == null || emails.isEmpty()) {
                                log.warn("No hay un email asociado al usuario de Google ID: [" + person.getId() + "] - Token: [" + requestBasicMessage.ptokenCode + "]");
                                break;
                            }
                            List<User> users = new ArrayList();
                            for (Emails e : emails) {
                                lock = instrumentedObject.lockObject("user_lock_sou_" + e.getValue(), 10);
                                user = userService.findByEmail(e.getValue());
                                if (user != null) {
                                    log.info("Encontrado usuario en DB. Email: [" + e.getValue() + "]");
                                    users.add(user);
                                }
                            }
                            user = null;
                            if (users.isEmpty()) {
                                log.info("Usuario nuevo.. Google ID: [" + person.getId() + "]");
                            } else if (users.size() > 1) {
                                user = funUsers(users);
                            } else {
                                user = users.get(0);
                            }
                            break;
                        case "facebook.com":
                            //http://restfb.com/documentation/
                            FacebookClient fc = null;
                            FacebookClient facebookClient = new DefaultFacebookClient(requestBasicMessage.ptokenCode,
                                    genericPropertyService.getCommonProperty("facebook.oauth.key.secret"),
                                    Version.LATEST);

                            fbUser = facebookClient.fetchObject("me", com.restfb.types.User.class, Parameter.with("metadata", 1));

                            if (fbUser == null) {
                                log.warn("No fue posible obtener el usuario de Facebook para: [" + requestBasicMessage.ptokenCode + "]");
                                break;
                            }
                            if (fbUser.getEmail() != null) {
                                lock = instrumentedObject.lockObject("user_lock_sou_" + requestBasicMessage.username, 10);
                                user = userService.findByEmail(fbUser.getEmail());
                            }
                            if (user == null) {
                                log.info("Usuario nuevo.. Facebook ID: [" + fbUser.getId() + "]");
                            }
                            break;
                    }
                } catch (Exception ex) {
                    log.error("Error al procesar proveedor: [" + requestBasicMessage.provider + "]. " + ex, ex);
                }

                if (user == null && ur != null && ur.getEmail() != null) {
                    user = userService.findByEmail(ur.getEmail());
                }

                //Aca se deben adicionar todos los proveedores OAUTH
                if (ur == null && person == null && fbUser == null) {
                    rm.credentials.error = 7;
                    return rm;
                }

                if (user == null) {
                    user = new User();
                }

                if (fbUser != null) {
                    if (fbUser.getAgeRange() != null) {
                        user.getProfileUserProperty().setAgeRange(new AgeRange(fbUser.getAgeRange().getMin(), fbUser.getAgeRange().getMax()));
                    }

                    if (fbUser.getBirthdayAsDate() != null) {
                        user.getProfileUserProperty().setBirthdate(fbUser.getBirthdayAsDate());
                    }
                    if (fbUser.getEmail() != null) {
                        Email email = user.getCredentialUserProperty().getUsername().getEmail(fbUser.getEmail());
                        if (email == null) {
                            email = new Email(fbUser.getEmail(), Boolean.TRUE);
                            user.getCredentialUserProperty().getUsername().setEmail(email);
                        } else {
                            email.setConfirmed(Boolean.TRUE);
                        }
                    }

                    if (fbUser.getFirstName() != null && (user.getProfileUserProperty().getFirstName() == null || !"User".equals(user.getProfileUserProperty().getFirstName()))) {
                        user.getProfileUserProperty().setFirstName(fbUser.getFirstName());
                    }

                    if (fbUser.getMiddleName() != null && user.getProfileUserProperty().getMiddleName() == null) {
                        user.getProfileUserProperty().setMiddleName(fbUser.getMiddleName());
                    }

                    if (fbUser.getLastName() != null && user.getProfileUserProperty().getLastName() == null) {
                        user.getProfileUserProperty().setMiddleName(fbUser.getLastName());
                    }

                    if (fbUser.getWebsite() != null && user.getProfileUserProperty().getSiteURL() == null) {
                        user.getProfileUserProperty().setSiteURL(fbUser.getWebsite());
                    }

                    if (fbUser.getGender() != null && (user.getProfileUserProperty().getGender() == null
                            || Gender.UNKNOWN.equals(user.getProfileUserProperty().getGender()))) {
                        switch (fbUser.getGender().toLowerCase()) {
                            case "male":
                                user.getProfileUserProperty().setGender(Gender.MALE);
                                break;
                            case "female":
                                user.getProfileUserProperty().setGender(Gender.FEMALE);
                                break;
                            default:
                                user.getProfileUserProperty().setGender(Gender.UNKNOWN);
                        }
                    }
                    if (fbUser.getPicture() != null && fbUser.getPicture().getUrl() != null
                            && !fbUser.getPicture().getIsSilhouette() && !user.getProfileUserProperty().isUserPhoto()) {
                        user.getProfileUserProperty().setUserPhoto(true);
                        user.getProfileUserProperty().setPhotoURL(fbUser.getPicture().getUrl());
                    }
                }

                if (person != null) {
                    if (person.getAgeRange() != null) {
                        user.getProfileUserProperty().setAgeRange(new AgeRange(person.getAgeRange().getMin(), person.getAgeRange().getMax()));
                    }
                    if (person.getBirthday() != null) {
                        final SimpleDateFormat gsdf = new SimpleDateFormat("yyyy-MM-dd");
                        user.getProfileUserProperty().setBirthdate(gsdf.parse(person.getBirthday()));
                    }

                    List<Emails> emails = person.getEmails();
                    if (!Collections.isEmpty(emails)) {
                        for (Emails e : emails) {
                            Email email = user.getCredentialUserProperty().getUsername().getEmail(e.getValue());
                            if (email == null) {
                                email = new Email(e.getValue(), Boolean.TRUE);
                                user.getCredentialUserProperty().getUsername().setEmail(email);
                            } else {
                                email.setConfirmed(Boolean.TRUE);
                            }
                        }
                    }

                    if (person.getGender() != null
                            && (user.getProfileUserProperty().getGender() == null
                            || Gender.UNKNOWN.equals(user.getProfileUserProperty().getGender()))) {
                        switch (person.getGender().toLowerCase()) {
                            case "male":
                                user.getProfileUserProperty().setGender(Gender.MALE);
                                break;
                            case "female":
                                user.getProfileUserProperty().setGender(Gender.FEMALE);
                                break;
                            default:
                                user.getProfileUserProperty().setGender(Gender.UNKNOWN);
                        }
                    }

                    if (user.getCredentialUserProperty().getUsername().getAvatarURL().contains("assets.planeta.guru/images/oauth/avatar.png")
                            && person.getImage() != null && (person.getImage().getIsDefault() == null || !person.getImage().getIsDefault())) {
                        user.getCredentialUserProperty().getUsername().setAvatarURL(person.getImage().getUrl());
                    }

                    if (person.getLanguage() != null && user.getProfileUserProperty().getLanguage() == null) {
                        user.getProfileUserProperty().setLanguage(person.getLanguage());
                    }

                    if (person.getName() != null && person.getName().getMiddleName() != null
                            && (user.getProfileUserProperty().getFirstName() == null || !"User".equals(user.getProfileUserProperty().getFirstName()))) {
                        user.getProfileUserProperty().setFirstName(person.getName().getGivenName());
                    }

                    if (person.getName() != null && person.getName().getMiddleName() != null
                            && (user.getProfileUserProperty().getMiddleName() == null)) {
                        user.getProfileUserProperty().setMiddleName(person.getName().getMiddleName());
                    }

                    if (person.getName() != null && person.getName().getFamilyName() != null
                            && user.getProfileUserProperty().getLastName() == null) {
                        user.getProfileUserProperty().setLastName(person.getName().getFamilyName());
                    }

                    if (person.getOccupation() != null) {
                        user.getProfileUserProperty().setOccupation(person.getOccupation());
                    }
                }

                if (ur != null) {
                    if (user.getProfileUserProperty().getFirstName() == null) {
                        user.getProfileUserProperty().setFirstName("User");
                    }

                    if (ur.getEmail() != null) {
                        Email email = user.getCredentialUserProperty().getUsername().getEmail(ur.getEmail());
                        if (email == null) {
                            email = new Email(ur.getEmail(), Boolean.TRUE);
                            user.getCredentialUserProperty().getUsername().setEmail(email);
                        } else {
                            email.setConfirmed(Boolean.TRUE);
                        }
                    }
                    if (ur.getPhoneNumber() != null) {
                        Msisdn phone = user.getCredentialUserProperty().getUsername().getMsisdn(ur.getPhoneNumber());
                        if (phone == null) {
                            phone = new Msisdn(ur.getPhoneNumber(), Boolean.FALSE, countryService.findByInternationalNumberPhone(ur.getPhoneNumber()), null);
                            user.getCredentialUserProperty().getUsername().setMsisdn(phone);
                        }
                    }
                    OauthProvider op = user.getProfileUserProperty().getProviders().get(ur.getUid());

                    if (op == null) {
                        op = new OauthProvider();
                        user.getProfileUserProperty().getProviders().put(ur.getUid(), op);
                    }

                    op.setUserRecord(ur);

                    if (person != null) {
                        op.setPerson(person);
                    }
                    if (fbUser != null) {
                        op.setUser(fbUser);
                    }
                }

                try {
                    if (user.getProfileUserProperty().getCountry() == null) {
                        String ip = httpServletRequest.getHeader("X-Forwarded-For");
                        if (ip == null || ip.length() < 1) {
                            ip = httpServletRequest.getRemoteAddr();
                        }
                        user.getProfileUserProperty().setCountry(getCountryByIP(ip));
                    }
                    userService.saveOrUpdate(user);
                    try {
                        String ua = httpServletRequest.getHeader("User-Agent");
                        rm.uct = Client.getNewToken(user.getCredentialUserProperty().getPassword() + "_" + user.getId() + "_" + ua, user, null, 365);
                        Map<String, Object> params = new HashMap();
                        params.put("inner", rm.uct);
                        rm.uct = Client.getNewToken(genericPropertyService.getValueByKey("user.sign.secret"), user, params, 365);
                        requestBasicMessage.kmsi = "OK";
                        autenticationServiceProcessorHandler.generateAuthenticationSessionCode(prop, cliente, user, rm, requestBasicMessage.tid, rm.uct, httpServletRequest, null);
                    } catch (Exception ex) {
                        log.error("Error al generar Cookie/token. " + ex, ex);
                    }
                } catch (Exception ex) {
                    log.error("Error al persistir user: [" + user + "]. " + ex, ex);
                }
            } catch (Exception ex) {
                log.error("Error al validar ProviderToken. Request: [" + requestBasicMessage + "]. " + ex, ex);
            }
        }

        instrumentedObject.unLockObject(lock);
        return rm;
    }

    public ResponseBasicMessage validatePwdResetPassword(User cliente, RequestBasicMessage requestBasicMessage, ResponseBasicMessage rm) {

        String uid = clientCache.getTemporalData(cliente.getId() + "_r_" + requestBasicMessage.code);
        if (uid != null && !"xx".equals(uid)) {
            rm.username = clientCache.getTemporalData(cliente.getId() + "_run_" + requestBasicMessage.code);
            rm.usernameType = RequestBasicMessage.UsernameType.valueOf(clientCache.getTemporalData(cliente.getId() + "_runt_" + requestBasicMessage.code));
            //  log.info("CODE: [" + requestBasicMessage.code + "] - UID: [" + uid + "] - Username: [" + rm.username + "] - usernameType: [" + rm.usernameType + "]");
            rm.ifExistsResult = 1;
            User user = null;
            if (requestBasicMessage.rpassword != null && requestBasicMessage.password.length() > 7
                    && requestBasicMessage.rpassword.equals(requestBasicMessage.password)) {
                try {
                    user = userService.findById(Long.parseLong(uid));
                    user.getCredentialUserProperty().setPassword(requestBasicMessage.password);
                    log.info("Actualizado nuevo password para: [" + user.getId() + "]");
                    clientCache.setTemporalData(cliente.getId() + "_r_" + requestBasicMessage.code, "xx", 60);
                    userService.saveOrUpdate(user);
                    rm.credentials.error = 10;
                } catch (Exception ex) {
                    log.error("Error al actualizar el password: [" + requestBasicMessage.password + "] para: [" + user + "]. " + ex);
                    rm.redirectUrl = "/oauth/signin/resetPassword/" + requestBasicMessage.tid + "?locale=" + requestBasicMessage.locale;
                }
            }
        } else {
            log.warn("No existe un UID valido para CODE: [" + requestBasicMessage.code + "]");
            rm.redirectUrl = "/signin/resetPassword/" + requestBasicMessage.tid + "?locale=" + requestBasicMessage.locale;
        }

        return rm;
    }

    public ResponseBasicMessage validateCodeResetPassword(User cliente, RequestBasicMessage requestBasicMessage, ResponseBasicMessage rm) {
        User user = null;
        switch (requestBasicMessage.usernameType) {
            case EMAIL:
                rm.username = rm.username.toLowerCase();
                user = userService.findByEmail(rm.username);
                break;
            case PHONE:
                if (rm.username.length() > 10) {
                    if (rm.username.startsWith("5415")) {
                        rm.username = "5411" + rm.username.substring(4, rm.username.length());
                    } else if (rm.username.startsWith("54915")) {
                        rm.username = "54911" + rm.username.substring(5, rm.username.length());
                    }
                }
                user = userService.findByMsisdn(rm.username);
                break;
        }
        if (user != null) {
            rm.ifExistsResult = 1;
            if (RequestBasicMessage.UsernameType.EMAIL.equals(requestBasicMessage.usernameType)) {
                String code = clientCache.getTemporalData(user.getId() + "_" + requestBasicMessage.code);
                if (code != null && code.equals(requestBasicMessage.code)) {
                    clientCache.setTemporalData(user.getId() + "_" + code, "xx", 15);
                    clientCache.setTemporalData(user.getId() + "_CODE", "xx", 10);
                    clientCache.setTemporalData(user.getId() + "_ECODE", "xx", 10);
                    rm.credentials.error = 0;
                    Msisdn msisdn = user.getCredentialUserProperty().getUsername().getMsisdn(rm.username);
                    if (msisdn != null) {
                        if (!msisdn.getConfirmed()) {
                            msisdn.setConfirmed(Boolean.TRUE);
                            try {
                                userService.saveOrUpdate(user);
                            } catch (Exception ex) {
                                log.error("Error al actualizar MSISDN para el usuario: [" + user + "]. " + ex, ex);
                            }
                        }
                    }
                }
            } else if (requestBasicMessage.tokenCode != null) {
                try {
                    FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdTokenAsync(requestBasicMessage.tokenCode).get();
                    String uid = decodedToken.getUid();
                    log.info("Google UserId: [" + uid + "]");
                    UserRecord ur = FirebaseAuth.getInstance().getUserAsync(uid).get();
                    String msisdnAux = ur.getPhoneNumber().replace("+", "");
                    boolean reg = false;
                    try {
                        Country c = countryService.findByInternationalNumberPhone(msisdnAux);
                        Integer msisdnLengthLocal = Integer.parseInt(propertyManagerService.getCommonProperty("msisdn." + c.getCode().toLowerCase() + ".length.local"));
                        String uag = msisdnAux.substring(Math.max(0, msisdnAux.length() - msisdnLengthLocal), msisdnAux.length());
                        String uap = rm.username.substring(Math.max(0, rm.username.length() - msisdnLengthLocal), rm.username.length());
                        reg = uag.equals(uap);
                    } catch (Exception ex) {
                    }

                    if (rm.username.equals(msisdnAux) || reg) {
                        //  log.info("Encontrado user en Google. [" + ur.getUid() + "]");
                        rm.credentials.error = 0;
                        Msisdn msisdn = user.getCredentialUserProperty().getUsername().getMsisdn(rm.username);
                        if (!msisdn.getConfirmed()) {
                            msisdn.setConfirmed(Boolean.TRUE);
                            try {
                                userService.saveOrUpdate(user);
                            } catch (Exception ex) {
                                log.error("Error al actualizar MSISDN para el usuario: [" + user + "]. " + ex, ex);
                            }
                        }
                        rm.credentials.error = 0;
                    } else {
                        log.fatal("No coincide el MSISDN: [" + msisdnAux + "] con el MSISDN: [" + rm.username + "]");
                    }
                } catch (Exception ex) {
                }
            }
        }
        if (clientCache.getInstrumentedObject().getAtomicLong(rm.username + "_r_attempts", 15) > 5l) {
            log.warn("Intentos de reset validacion excedidos. username: [" + requestBasicMessage.username + "]");
            rm.credentials.error = 2;
            rm.redirectUrl = null;
        } else if (rm.credentials.error == 0) {
            String ccode = generateCode(15);
            rm.redirectUrl = "/signin/secure/reset";
            rm.redirectUrl = rm.redirectUrl + "?tid=" + requestBasicMessage.tid + "&code=" + ccode + "&locale=" + requestBasicMessage.locale;
            clientCache.setTemporalData(cliente.getId() + "_r_" + ccode, user.getId().toString(), 60);
            clientCache.setTemporalData(cliente.getId() + "_run_" + ccode, rm.username, 60);
            clientCache.setTemporalData(cliente.getId() + "_runt_" + ccode, rm.usernameType.toString(), 60);
        } else {
            clientCache.getInstrumentedObject().getAndIncrementAtomicLong(rm.username + "_r_attempts", 15);
        }

        return rm;
    }

    private ResponseBasicMessage validatePasswordOrCodeLogin(User cliente,
            Map<String, String> prop, RequestBasicMessage requestBasicMessage, ResponseBasicMessage rm, HttpServletRequest httpServletRequest) {
        User user = null;

        if (rm.username == null) {
            rm.credentials.error = 1;
            return rm;
        }

        if (requestBasicMessage.usernameType == null) {
            if (rm.username.contains("@")) {
                rm.username = rm.username.toLowerCase();
                requestBasicMessage.usernameType = UsernameType.EMAIL;
            } else {
                requestBasicMessage.usernameType = UsernameType.PHONE;
            }
        }
        rm.locale = requestBasicMessage.locale;
        String provr = null;
        switch (requestBasicMessage.usernameType) {
            case EMAIL:
                user = userService.findByEmail(rm.username);
                if (user != null) {
                    rm.ifExistsResult = 1;
                    if (user.getCredentialUserProperty().getUsername().getEmails() != null
                            && !user.getCredentialUserProperty().getUsername().getEmails().isEmpty()) {
                        rm.credentials.hasPhone = 1;
                    }
                    if (user.getCredentialUserProperty().getPassword() != null) {
                        rm.credentials.hasPassword = 1;
                        if (user.getCredentialUserProperty().validatePassword(requestBasicMessage.password)) {
                            rm.credentials.error = 0;
                        }
                    }
                }
                break;
            case PHONE:
                if (rm.username.length() > 10) {
                    if (rm.username.startsWith("5415")) {
                        rm.username = "5411" + rm.username.substring(4, rm.username.length());
                    } else if (rm.username.startsWith("54915")) {
                        rm.username = "54911" + rm.username.substring(5, rm.username.length());
                    }
                }
                user = userService.findByMsisdn(rm.username);
                if (user != null) {
                    rm.ifExistsResult = 1;
                    rm.credentials.hasPhone = 1;
                    if (requestBasicMessage.password != null) {
                        rm.credentials.hasPassword = 1;
                        if (user.getCredentialUserProperty().validatePassword(requestBasicMessage.password)) {
                            rm.credentials.error = 0;
                            Msisdn msisdn = user.getCredentialUserProperty().getUsername().getMsisdn(rm.username);
                            if (msisdn.getProvider() == null) {
                                try {
                                    Subscription s = subscriptionService.findByMSISDNAndCountry(msisdn.getMsisdn(), msisdn.getCountry());
                                    if (s != null) {
                                        msisdn.setProvider(s.getSop().getProvider());
                                        userService.saveOrUpdate(user);
                                    }
                                } catch (Exception ex) {
                                    log.error("Error al obtener Suscripcion para: [" + msisdn + "]. " + ex, ex);
                                }
                            }
                            if (msisdn.getProvider() != null) {
                                provr = msisdn.getProvider().getName();
                            }
                        }
                    } else if (requestBasicMessage.tokenCode != null) {
                        provr = firebaseLogin(requestBasicMessage.tokenCode, user, rm);
                    } else if (requestBasicMessage.vcode != null) {
                        //Valida el PIN enviado por SMS con el proveedor-telco
                        validateSMSVerificationCode(cliente, prop, requestBasicMessage, rm, httpServletRequest, user);
                        provr = prop.get("prvdr");
                    }
                } else if (requestBasicMessage.tokenCode != null) {
                    // log.info("Creando un nuevo user para: [" + rm.username + "]");
                    user = new User();
                    provr = firebaseLogin(requestBasicMessage.tokenCode, user, rm);
                } else if (requestBasicMessage.vcode != null) {
                    //Valida el PIN enviado por SMS con el proveedor-telco
                    user = new User();
                    validateSMSVerificationCode(cliente, prop, requestBasicMessage, rm, httpServletRequest, user);
                    provr = prop.get("prvdr");
                }
                break;
        }
        if (clientCache.getInstrumentedObject().getAtomicLong(rm.username + "_attempts", 15) > 5l) {
            log.warn("Intentos de login validacion excedidos. username: [" + requestBasicMessage.username + "] - Prop: [" + Arrays.toString(prop.entrySet().toArray()) + "]");
            rm.credentials.error = 2;
            rm.redirectUrl = null;
        } else if (rm.credentials.error == 0) {
            if (requestBasicMessage.usernameType.equals(UsernameType.EMAIL)
                    && !user.getCredentialUserProperty().getUsername().getEmail(rm.username).getConfirmed()) {
                rm.tid = requestBasicMessage.tid;
                return autenticationServiceProcessorHandler.createVerifyCodePage(rm);
            }
            try {
                String ua = httpServletRequest.getHeader("User-Agent");
                rm.uct = Client.getNewToken(user.getCredentialUserProperty().getPassword() + "_" + user.getId() + "_" + ua, user, null, 365);
                Map<String, Object> params = new HashMap();
                params.put("inner", rm.uct);
                if (provr != null) {
                    params.put("userAccountProvider", provr);
                }
                rm.uct = Client.getNewToken(genericPropertyService.getValueByKey("user.sign.secret"), user, params, 365);
                autenticationServiceProcessorHandler.generateAuthenticationSessionCode(prop, cliente, user, rm, requestBasicMessage.tid, rm.uct, httpServletRequest, provr);
            } catch (Exception ex) {
                log.error("Error al generar Cookie/token. " + ex, ex);
            }
        } else {
            clientCache.getInstrumentedObject().getAndIncrementAtomicLong(rm.username + "_attempts", 15);
        }
        return rm;
    }

    private String firebaseLogin(String ftoken, User user, ResponseBasicMessage rm) {
        String prov = null;
        try {
            FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdTokenAsync(ftoken).get();
            String uid = decodedToken.getUid();
            log.info("Google UserId: [" + uid + "]");
            UserRecord ur = FirebaseAuth.getInstance().getUserAsync(uid).get();
            String msisdnAux = ur.getPhoneNumber().replace("+", "");
            if (rm.username.equals(msisdnAux)) {
                // log.info("Encontrado user en Google. [" + ur.getUid() + "]");
                rm.credentials.error = 0;
                Msisdn msisdn = user.getCredentialUserProperty().getUsername().getMsisdn(rm.username);
                //if (user.getId() == null) {
                Country c = null;
                if (msisdn == null) {
                    c = countryService.findByInternationalNumberPhone(rm.username);
                    if (c == null) {
                        log.error("No fue encontrado un Country para: [" + rm.username + "]");
                    }
                    msisdn = new Msisdn(rm.username, true, c, null);
                    user.getCredentialUserProperty().getUsername().setMsisdn(msisdn);
                    if (user.getProfileUserProperty().getCountry() == null) {
                        c = countryService.findByInternationalNumberPhone(rm.username);
                    }
                    try {
                        userService.saveOrUpdate(user);
                        // log.info("Creado user por login: [" + user + "]");
                    } catch (Exception ex) {
                        log.error("Error al actualizar MSISDN para el usuario: [" + user + "]. " + ex, ex);
                    }
                }
                if (user.getProfileUserProperty().getCountry() == null) {
                    if (c == null) {
                        c = countryService.findByInternationalNumberPhone(rm.username);
                    }
                    if (c == null) {
                        log.error("No fue encontrado un Country para: [" + rm.username + "]");
                    }
                    user.getProfileUserProperty().setCountry(c);
                    try {
                        userService.saveOrUpdate(user);
                        // log.info("Actualizado country: [" + user + "]");
                    } catch (Exception ex) {
                        log.error("Error al actualizar MSISDN para el usuario: [" + user + "]. " + ex, ex);
                    }
                }
                if (msisdn.getProvider() == null) {
                    try {
                        Subscription s = subscriptionService.findByMSISDNAndCountry(msisdn.getMsisdn(), msisdn.getCountry());
                        if (s != null) {
                            msisdn.setProvider(s.getSop().getProvider());
                            userService.saveOrUpdate(user);
                        }
                    } catch (Exception ex) {
                        log.error("Error al obtener Suscripcion para: [" + msisdn + "]. " + ex, ex);
                    }
                }

                if (msisdn.getProvider() != null) {
                    prov = msisdn.getProvider().getName();
                }

                if (!msisdn.getConfirmed()) {
                    msisdn.setConfirmed(Boolean.TRUE);
                    try {
                        userService.saveOrUpdate(user);
                    } catch (Exception ex) {
                        log.error("Error al actualizar MSISDN para el usuario: [" + user + "]. " + ex, ex);
                    }
                }
            } else {
                log.fatal("No coincide el MSISDN: [" + msisdnAux + "] con el MSISDN: [" + rm.username + "]");
            }
        } catch (Exception ex) {
        }
        return prov;
    }

    public String toJson(Object object) throws JsonProcessingException {
        return mapper.writeValueAsString(object);
    }

    /**
     * Obtiene el HTML de la pagina de LOGIN
     *
     * @param prop
     * @param tid
     * @return
     */
    public String getUserLoginHtml(Map<String, String> prop, String tid, HttpServletRequest httpServletRequest) {
        String prvdr = prop.get("prvdr");

        String html;
        if (prvdr == null) {
            html = readResource("/tpl/html/signin/login.html", prop);
        } else {
            // html = readResource("/tpl/html/signin/login_prvdr_" + prvdr + ".html", prop);
            html = readResource("/tpl/html/signin/login_mobile.html", prop);
            if (html == null || html.isEmpty()) {
                html = readResource("/tpl/html/signin/login.html", prop);
            }
        }

        User client = userService.findByclientId(prop.get("clientId"));
        Long productId = Long.parseLong(prop.get("productId"));
        String mxTelcelSS0 = "style=\"display:none;\"";
        String urlMxTelcelSS0 = "";
        for (Product p : client.getProducts()) {
            if (p.getId().equals(productId)) {
                // Buscar provider por prvdr_name
                try {
                    String prvdr_name = prop.get("prvdr_name");
                    if (prvdr_name != null) {
                        String providerId = phprofilePropertiesService.getCommonProperty(prvdr_name);
                        Long providerIdLng;
                        log.info("oauth - getUserLoginHtml - providerId ["+providerId+"]");
                        try {
                            providerIdLng = Long.parseLong(providerId);
                        }
                        catch (Exception ex){
                            providerIdLng = 0l;
                        }
                        if (providerIdLng > 0) {
                            Provider pr = providerService.findById(providerIdLng);
                            String name = pr.getName();
                            String[] parts = name.split("\\.");
                            String shortName= parts[0]+'.'+parts[2];
                            urlMxTelcelSS0 = p.getProductSettings().get("oauth.sso."+shortName);
                        }else {
                            urlMxTelcelSS0 = p.getProductSettings().get("oauth.sso.mx.tiaxa");
                        }
                    } else {
                        urlMxTelcelSS0 = p.getProductSettings().get("oauth.sso.mx.tiaxa");
                    }
                } catch (Exception ex){
                    log.error("Error al obtener oauth.sso" + ex, ex);
                }
                if (urlMxTelcelSS0 == null) {
                    urlMxTelcelSS0 = "";
                }
                // urlMxTelcelSS0 = p.getProductSettings().get("oauth.sso.mx.tiaxa");
                break;
            }
        }
        html = html.replace("<mxTelcelSSO>", mxTelcelSS0);
        html = html.replace("<mxTelcelSSO.url>", urlMxTelcelSS0);

        /* if (prvdr == null) {
            String arPersonalSS0 = "";
            String urlArPersonalSS0 = "";

            //  if (client.getProducts() == null || client.getProducts().isEmpty()) {
            arPersonalSS0 = "style=\"display:none;\"";
            // }
            /*else {
                  for (Product p : client.getProducts()) {
                    if (p.getId().equals(productId)) {
                        urlArPersonalSS0 = p.getProductSettings().get("oauth.internal.ar.personal.cdag")
                                + httpServletRequest.getRequestURI() + "?" + httpServletRequest.getQueryString();
                    }
                }
            }*/
 /*     html = html.replace("<arPersonalSSO>", arPersonalSS0);
            html = html.replace("<arPersonalSSO.url>", urlArPersonalSS0);
        }*/
        Object o = MainCache.memory5Hours().getIfPresent("country_status_json_codes");
        if (o == null) {
            List<Country> activeCountries = countryService.getByStatus(Status.ENABLE);
            if (!activeCountries.isEmpty()) {
                String[] codes = new String[activeCountries.size()];
                int i = 0;
                for (Country c : activeCountries) {
                    codes[i] = c.getCode().toLowerCase();
                    i++;
                }
                try {
                    String codex = toJson(codes);
                    MainCache.memory5Hours().put("country_status_json_codes", codex);
                    html = html.replace("var srtc", "pparams.codes=" + codex);
                } catch (JsonProcessingException ex) {
                }
            }
        } else {
            html = html.replace("var srtc", "pparams.codes=" + o);
        }

        return html;
    }

    public String getHtmlResourse(String page) {
        return genericPropertyService.getValueByProfileKey(GenericProperty.Profile.ALL, "user.html." + page);
    }

    /**
     * Obtiene el HTML de la pagina de RESET_PASSWORD
     *
     * @param prop
     * @param tid
     * @return
     */
    public String getResetPasswordHtml(Map<String, String> prop, String tid) {
        String html = readResource("/tpl/html/signin/resetPassword.html", prop);

        List<Country> countries = countryService.getByStatus(Status.ENABLE);

        String aux = "";
        for (Country c : countries) {
            aux = aux + "<option value=\"" + c.getCode() + "\">" + (c.getName().length() == c.getNativeName().length() && !c.getNativeView() ? c.getNativeName() : c.getName() + " (" + c.getNativeName() + ")") + " (+" + c.getPhoneCode() + ")</option>";
        }

        html = html.replace("<countryList></countryList>", aux);

        aux = genericPropertyService.getValueByKey("google.recaptcha.key.public");
        html = html.replace("<captcha></captcha>", aux);

        return html;
    }

    /**
     * Obtiene el HTML de la pagina de New Account
     *
     * @param prop
     * @param tid
     * @return
     */
    public String getNewAccountHtml(Map<String, String> prop, String tid) {

        String html = readResource("/tpl/html/signup/newAccount.html", prop);
        List<Country> countries = countryService.getByStatus(null);
        String countryList = "";
        for (Country c : countries) {
            countryList = countryList + "<option value=\"" + c.getCode() + "\">" + (c.getName().length() == c.getNativeName().length() && !c.getNativeView() ? c.getNativeName() : c.getName() + " (" + c.getNativeName() + ")") + "</option>";
        }
        List<Country> activeCountries = countryService.getByStatus(Status.ENABLE);
        if (!activeCountries.isEmpty()) {
            String[] codes = new String[activeCountries.size()];
            int i = 0;
            for (Country c : activeCountries) {
                codes[i] = c.getCode().toLowerCase();
                i++;
            }
            try {
                html = html.replace("var srtc", "pparams.codes=" + toJson(codes));
            } catch (JsonProcessingException ex) {
            }
        }
        html = html.replace("<countryList></countryList>", countryList);

        String aux = genericPropertyService.getValueByKey("google.recaptcha.key.public");
        html = html.replace("<captcha></captcha>", aux);

        html = html.replace("<years/>", getYears());

        return html;
    }

    public ResponseBasicMessage createNewAccount(Map<String, String> prop, RequestBasicMessage requestBasicMessage) throws Exception {

        ResponseBasicMessage rmr = new ResponseBasicMessage();
        rmr.locale = requestBasicMessage.locale;

        if (requestBasicMessage.form.firstname == null || requestBasicMessage.form.lastname == null || requestBasicMessage.form.countryCode == null
                || requestBasicMessage.form.gender == null || requestBasicMessage.form.password == null || requestBasicMessage.form.rpassword == null) {
            log.fatal("NDDS. Alguno de los parametros es nulo!!! [" + requestBasicMessage + "]");
            return rmr;
        }

        if (!requestBasicMessage.form.password.equals(requestBasicMessage.form.rpassword) || requestBasicMessage.form.password.length() < 7) {
            log.fatal("NDDS. Los passwords no coninciden: [" + requestBasicMessage + "]");
            return rmr;
        }

        if (requestBasicMessage.form.email == null || requestBasicMessage.form.email.length() < 4 || !requestBasicMessage.form.email.contains("@")) {
            log.fatal("NDDS. Invalido Formato de Email: [" + requestBasicMessage.form.email + "]");
            return rmr;
        }

        User client = userService.findByclientId(prop.get("clientId"));
        if (client == null) {
            log.error("No existe el clientId: [" + prop.get("clientId") + "]");
            return rmr;
        }

        requestBasicMessage.form.email = filterCaractersToLowerCase(requestBasicMessage.form.email);
        RLock lock = instrumentedObject.lockObject(requestBasicMessage.form.email, 60);

        //Validamos si el EMAIL ya esta registrado en DB como User
        User user = userService.findByEmail(requestBasicMessage.form.email);
        if (user != null) {
            log.warn("Ya existe un usuario con el email: [" + requestBasicMessage.form.email + "]");
            return rmr;
        }

        //Validamos si el MSISDN ya esta registrado en DB como User
        Country c = null;
        if (requestBasicMessage.form.phoneNumber != null) {
            if (requestBasicMessage.form.phoneCountryCode == null) {
                requestBasicMessage.form.phoneCountryCode = requestBasicMessage.form.countryCode.toUpperCase().trim();
            }
            requestBasicMessage.form.phoneNumber = requestBasicMessage.form.phoneNumber.replace("+", "");
            if ("undefined".equals(requestBasicMessage.form.phoneCountryCode)) {
                requestBasicMessage.form.phoneCountryCode = requestBasicMessage.form.countryCode.toUpperCase().trim();
            }
            c = countryService.findByCode(requestBasicMessage.form.phoneCountryCode.toUpperCase());
            String localLength = phprofilePropertiesService.getCommonProperty("msisdn."
                    + c.getCode().toLowerCase() + ".length.local");

            if (localLength == null) {
                log.warn("No existe en DB la configuracion: [msisdn." + c.getCode().toLowerCase() + ".length.local]");
            } else {
                Integer msisdnLengthLocal = Integer.parseInt(localLength);
                requestBasicMessage.form.phoneNumber = requestBasicMessage.form.phoneNumber.replace(" ", "").replace("+", "");
                if (!requestBasicMessage.form.phoneNumber.startsWith(c.getPhoneCode().toString())) {
                    requestBasicMessage.form.phoneNumber = c.getPhoneCode() + requestBasicMessage.form.phoneNumber;
                }
                if (requestBasicMessage.form.phoneNumber.length() > 10) {
                    if (requestBasicMessage.form.phoneNumber.startsWith("5415")) {
                        requestBasicMessage.form.phoneNumber = "5411" + requestBasicMessage.form.phoneNumber.substring(4, requestBasicMessage.form.phoneNumber.length());
                    } else if (requestBasicMessage.form.phoneNumber.startsWith("54915")) {
                        requestBasicMessage.form.phoneNumber = "54911" + requestBasicMessage.form.phoneNumber.substring(5, requestBasicMessage.form.phoneNumber.length());
                    }
                }

                if (requestBasicMessage.form.phoneNumber.startsWith(c.getPhoneCode().toString())) {
                    requestBasicMessage.form.phoneNumber = requestBasicMessage.form.phoneNumber.substring(c.getPhoneCode().toString().length(), requestBasicMessage.form.phoneNumber.length());
                }
                requestBasicMessage.form.phoneNumber = requestBasicMessage.form.phoneNumber
                        .substring(Math.max(0, requestBasicMessage.form.phoneNumber.length() - msisdnLengthLocal),
                                requestBasicMessage.form.phoneNumber.length());

                if (requestBasicMessage.form.phoneNumber.length() < msisdnLengthLocal) {
                    log.warn("Invalido Formato del MSISDN: [" + requestBasicMessage.form.phoneNumber + "]");
                    requestBasicMessage.form.phoneNumber = null;
                } else {
                    Holder<String> username = new Holder();
                    user = userService.findByMsisdn(c.getPhoneCode().toString(), requestBasicMessage.form.phoneNumber, username);
                    if (user != null) {
                        log.error("Ya existe un usuario con el MSISDN: [" + c.getPhoneCode().toString() + requestBasicMessage.form.phoneNumber + "]");
                        return rmr;
                    }
                    requestBasicMessage.form.phoneNumber = c.getPhoneCode().toString() + requestBasicMessage.form.phoneNumber;
                }
            }
        }

        user = new User();
        user.getProfileUserProperty().setFirstName(requestBasicMessage.form.firstname.trim());
        user.getProfileUserProperty().setLastName(requestBasicMessage.form.lastname.trim());

        Calendar cs = Calendar.getInstance();
        cs.add(Calendar.YEAR, -103);
        Calendar ce = Calendar.getInstance();
        cs.add(Calendar.YEAR, -3);
        if (requestBasicMessage.form.birthdate != null && cs.getTime().before(requestBasicMessage.form.birthdate)
                && ce.getTime().after(requestBasicMessage.form.birthdate)) {
            user.getProfileUserProperty().setBirthdate(requestBasicMessage.form.birthdate);
        }
        user.getProfileUserProperty().setGender(ProfileUserProperty.Gender.getGender(requestBasicMessage.form.gender));
        Country caux = countryService.findByCode(requestBasicMessage.form.countryCode.toUpperCase().trim());
        user.getProfileUserProperty().setCountry(caux);
        if (caux.getLanguages() != null && !caux.getLanguages().isEmpty()) {
            if (caux.getLanguages().size() > 1) {
                Set<Language> list = new TreeSet();
                list.addAll(caux.getLanguages());
                user.getProfileUserProperty().setLanguage(list.iterator().next().getCode().toLowerCase().trim() + "-" + caux.getCode().toUpperCase().trim());
            } else {
                user.getProfileUserProperty().setLanguage(caux.getLanguages().iterator().next().getCode().toLowerCase().trim() + "-" + caux.getCode().toUpperCase().trim());
            }
        }
        user.getCredentialUserProperty().getUsername().setEmail(requestBasicMessage.form.email.trim(), false);
        user.getCredentialUserProperty().setPassword(requestBasicMessage.form.password.trim());

        //No tenemos la Telco, asi que buscamos el MSISDN en PaymentHub-DB
        if (c != null && requestBasicMessage.form.phoneNumber != null) {
            Provider p = null;
            Subscription s = null;
            s = subscriptionService.findByMSISDNAndCountry(requestBasicMessage.form.phoneNumber, c);
            if (s != null) {
                // log.info("Suscripcion encontrada: [" + s.getId() + "]");
                p = s.getSubscriptionRegistry().getSop().getProvider();
                requestBasicMessage.form.phoneNumber = s.getUserAccount();
            }

            user.getCredentialUserProperty().getUsername().setMsisdn(requestBasicMessage.form.phoneNumber, false, c, p);
        }

        try {
            userService.saveOrUpdate(user);
            // log.info("Creado user: [" + user + "]");
            //sendemail
            //pagina de enviado email y continuar a login
            rmr.credentials.error = 11;
            rmr.username = requestBasicMessage.form.email.trim();
        } catch (Exception ex) {
            log.error("Error al crear el USER: [" + user + "]. " + ex, ex);
            rmr.credentials.error = 6;
        }
        instrumentedObject.unLockObject(lock);
        return rmr;
    }

    public String verifyEmail(DefaultClaims prop, RequestBasicMessage rbm, ResponseBasicMessage rm, HttpServletRequest httpServletRequest) {
        rm.username = prop.get("username", String.class);
        rm.tid = prop.get("tid", String.class);

        String verifyMail = prop.get("verifyMail", String.class);
        String verifyCode = prop.get("verify", String.class);

        User user = userService.findByEmail(rm.username);
        if (user == null) {
            log.error("Error al obtener User con email: [" + rm.username + "]");
            return null;
        }

        if (user.getCredentialUserProperty().getUsername().getEmail(rm.username).getConfirmed()) {
            rm.redirectUrl = "/signin/login/" + rm.tid + "?locale=" + rm.locale;
            return null;
        }

        //Es un request de verificacion de email por link
        if (verifyCode != null) {
            try {
                user.getCredentialUserProperty().getUsername().getEmail(rm.username).setConfirmed(true);
                userService.saveOrUpdate(user);
                String html = getUserLoginHtml(validateTID(rm.tid), rm.tid, httpServletRequest);
                rm.credentials.error = 13;
                return html;
            } catch (Exception ex) {
                log.error("Error al actualizar user: [" + user + "]. " + ex, ex);
            }
        }

        if (user.getProfileUserProperty().getFirstName() == null) {
            user.getProfileUserProperty().setFirstName("User");
            try {
                userService.saveOrUpdate(user);
            } catch (Exception ex) {
                log.error("Error al actualizar User: [" + user + "]. " + ex, ex);
            }
        }
        //Es un request de envio de verificacion de email
        if (verifyMail == null) {
            verifyMail = "vcode";
        }
        switch (verifyMail) {
            //Verificacion por codigo
            case "vcode":
                if (rbm.code == null) {
                    sendEmailVerifyCode(user, rm);
                } else {
                    String code = clientCache.getTemporalData(user.getId() + "_ECODE_VERIFY");
                    if (code == null || "xx".equals(code)) {
                        sendEmailVerifyCode(user, rm);
                    } else {
                        rm.credentials.hasPin = 1;
                        rm.credentials.error = 1;

                        if (clientCache.getInstrumentedObject().getAtomicLong(rm.username + "_r_attempts_code", 15) > 5l) {
                            log.warn("Intentos de validacion de codigo excedidos. username: [" + rm.username + "]");
                            rm.credentials.error = 2;
                        } else {
                            clientCache.getInstrumentedObject().getAndIncrementAtomicLong(rm.username + "_r_attempts_code", 15);
                            if (code.equals(rbm.code)) {
                                try {
                                    // log.info("Codigo de verificacion OK: [" + code + "]");
                                    clientCache.setTemporalData(user.getId() + "_ECODE_VERIFY", "xx", 40);
                                    clientCache.setTemporalData(user.getId() + "_" + code, "xx", 45);
                                    user.getCredentialUserProperty().getUsername().getEmail(rm.username).setConfirmed(true);
                                    userService.saveOrUpdate(user);

                                    Map<String, String> propf = validateTID(rm.tid);
                                    User client = userService.findByclientId(propf.get("clientId"));
                                    autenticationServiceProcessorHandler.generateAuthenticationSessionCode(propf, client, user, rm, rm.tid, "verification_code", httpServletRequest, null);
                                    rm.credentials.error = 13;
                                    rm.credentials.hasPin = null;
                                    return null;
                                } catch (Exception ex) {
                                    log.error("Error al validar codigo. " + ex, ex);
                                }
                            }
                        }
                    }

                    String html = null;
                    try {
                        html = getUserLoginHtml(validateTID(rm.tid), rm.tid, httpServletRequest);
                    } catch (Exception ex) {
                        log.error("Error al obtener codigo HTML login page. " + ex, ex);
                    }
                    return html;
                }
                break;
            //verificacion por link    
            case "vlink":
                verifyCode = clientCache.getTemporalData(rm.username);
                if (verifyCode == null) {
                    try {
                        Map<String, Object> params = new HashMap();
                        params.put("username", rm.username);
                        params.put("tid", rm.tid);
                        params.put("verify", "1");
                        verifyCode = Client.getNewToken(genericPropertyService.getValueByKey("user.sign.secret"), user, params, 15);
                        String url = getOauthURL() + "/signin/secure/verify/" + verifyCode + "?locale=" + rm.locale;
                        String html = readResource("/tpl/email/verifyEmail.html", validateTID(rm.tid));
                        String name = "User".equals(user.getProfileUserProperty().getFirstName()) ? "" : user.getProfileUserProperty().getFirstName();
                        html = html.replace("<sname></sname>", name);

                        html = html.replace("<sredirect></sredirect>", url);
                        String prvdr = null;
                        try {
                            prvdr = prop.get("prvdr", String.class);
                        } catch (Exception ex) {
                        }
                        html = localeService.i18n(rm.locale, prvdr, html);
                        emailUtility.sendHTMLEmail("no-reply@planeta.guru", rm.username, localeService.i18n(rm.locale, prvdr, "<i18n>i18n.ix.px.email.verify.text.pvye</i18n>"), html);
                        clientCache.setTemporalData(rm.username, verifyCode, 15);
                    } catch (Exception ex) {
                        log.error("Error al enviar email de verificacion. " + ex, ex);
                    }
                }
                break;
        }

        log.info("Enviado codigo de verificacion. EMAIL: [" + rm.username + "] verifyMail: [" + verifyMail + "] - code: [" + verifyCode + "]");

        String html = null;
        try {
            html = getUserLoginHtml(validateTID(rm.tid), rm.tid, httpServletRequest);
        } catch (Exception ex) {
            log.error("Error al obtener codigo HTML login page. " + ex, ex);
        }

        rm.credentials.error = 12;
        return html;
    }

    public String getOauthURL() {
        return genericPropertyService.getValueByKey("oauth.url");
    }

    private Map<String, String> validateTID(String tid) throws Exception {
        return autenticationServiceProcessorHandler.validateTID(tid);
    }

    /**
     * Envio del Codigo de verificacion por SMS (4 digitos)
     */
    private void sendSMSVerificationCode(User cliente, Map<String, String> prop, ResponseBasicMessage rm, HttpServletRequest httpServletRequest) {
        if (rm.username.length() > 10) {
            if (rm.username.startsWith("5415")) {
                rm.username = "5411" + rm.username.substring(4, rm.username.length());
            } else if (rm.username.startsWith("54915")) {
                rm.username = "54911" + rm.username.substring(5, rm.username.length());
            }
        }

        rm.credentials.hasPin = 0;
        Long auxl = instrumentedObject.getAndIncrementAtomicLong(rm.username + "_SMS_CODE", 5);
        if (auxl > 15) {
            rm.credentials.error = 2;
            rm.credentials.hasPin = 1;
            log.warn("Exceso de intentos de envio de PIN por SMS para el User: [" + rm.username + "]");
            return;
        }

        Product p = null;
        while (cliente.getPrincipal() != null) {
            cliente = cliente.getPrincipal();
        }
        if (cliente.getProducts() != null && !cliente.getProducts().isEmpty()) {
            String productId = prop.get("productId");
            for (Product aux : cliente.getProducts()) {
                if (aux.getId().toString().equals(productId)) {
                    p = aux;
                    break;
                }
            }
        }
        String providerName = null;
        String prvdr = prop.get("prvdr_name");
        User user = userService.findByMsisdn(rm.username);
        if (user == null) {
            if (rm.credentials.hasPassword == 0 && p != null && prvdr != null) {
                providerName = p.getProductSettings().get("oauth." + prvdr + ".pin");
                if (providerName != null && "true".equals(p.getProductSettings().get("oauth." + providerName + ".pin"))) {
                    //Envio de PIN por SMS del proveedor
                    rm.credentials.hasPin = 1;
                }
            }
        } else {
            Msisdn mm = user.getCredentialUserProperty().getUsername().getMsisdn(rm.username);
            if (mm.getProvider() != null) {
                providerName = mm.getProvider().getName();
            }
            if (rm.credentials.hasPassword == 0 && p != null && prvdr != null) {
                providerName = p.getProductSettings().get("oauth." + prvdr + ".pin");
                if (providerName != null && "true".equals(p.getProductSettings().get("oauth." + providerName + ".pin"))) {
                    //Envio de PIN por SMS del proveedor
                    rm.credentials.hasPin = 1;
                }
            } else if (rm.credentials.hasPassword == 0 && p != null && providerName != null
                    && "true".equals(p.getProductSettings().get("oauth." + providerName + ".pin"))) {
                //Envio de PIN por SMS del proveedor
                rm.credentials.hasPin = 1;
            }
        }

        if (rm.credentials.hasPin != 1) {
            rm.credentials.error = 1;
            return;
        }

        /* String code = clientCache.getTemporalData(rm.username + "_SMS_CODE");
        if (code == null || "xx".equals(code)) {*/
        String code = generateDigitCode(4);
        log.info("SMSVerificationCode: [" + code + "] - proveedor: [" + providerName + "] - User: [" + rm.username + "]");
        try {
            rm.credentials.hasPin = 1;
            clientCache.setTemporalData(rm.username + "_SMS_CODE", code, 5);
            clientCache.setTemporalData(rm.username + "_SMS_" + code, code, 5);
            clientCache.setTemporalData(rm.username + "_SMS_P_" + code, providerName, 5);
            SOP sop = null;
            for (SOP sa : p.getSops()) {
                if (sa.getProvider().getName().equals(providerName)) {
                    sop = sa;
                }
            }
            if (sop == null) {
                log.fatal("No hay un SOP configurado para providerName: [" + providerName + "]");
            } else {
                Country c = sop.getProvider().getCountry();
                rm.locale = c.getLanguages().iterator().next().getCode() + "-" + c.getCode();
                subscriptionService.sendSMSFacadeRequest(rm.username, sop, code + " " + localeService.i18n(rm.locale, sop.getProvider().getId(), "<i18n>i18n.ix.px.sms.verification.code</i18n>") + " " + sop.getServiceMatcher().getService().getName());
            }
        } catch (Exception ex) {
            log.error("Error al enviar SMS. " + ex, ex);
        }
        /*  } else {
            log.info("SMSVerificationCode: [" + code + "] - proveedor: [" + providerName + "] - User: [" + rm.username + "]");
            rm.credentials.hasPin = 1;
        }*/

    }

    /**
     * Validacion del Codigo de verificacion por SMS
     */
    private void validateSMSVerificationCode(User cliente, Map<String, String> prop,
            RequestBasicMessage requestBasicMessage, ResponseBasicMessage rm, HttpServletRequest httpServletRequest, User user) {
        rm.credentials.error = 1;
        // log.info("Validando PIN para: [" + requestBasicMessage + "]");
        Long auxl = instrumentedObject.getAndIncrementAtomicLong(rm.username + "_SMS_CODE", 5);
        if (auxl > 15) {
            rm.credentials.error = 2;
            rm.credentials.hasPin = 1;
            log.warn("Exceso de intentos de envio de PIN por SMS para el User: [" + rm.username + "]");
            return;
        }

        String code = clientCache.getTemporalData(rm.username + "_SMS_CODE");
        // log.info("PIN del cache: [" + code + "][" + rm.username + "]");
        if (code != null && !"xx".equals(code) && requestBasicMessage.vcode != null && code.equals(requestBasicMessage.vcode)) {
            log.info("El PIN [" + code + "] es valido para el Username: [" + rm.username + "]");
            clientCache.setTemporalData(rm.username + "_SMS_CODE", "xx", 10);
            clientCache.setTemporalData(rm.username + "_SMS_" + code, "xx", 10);
            rm.credentials.error = 0;
        } else {
            rm.credentials.hasPin = 1;
        }

        if (user.getId() == null && rm.credentials.error == 0) {
            String providerName = clientCache.getTemporalData(rm.username + "_SMS_P_" + code);
            Provider p = providerService.findByName(providerName);
            Country c = p.getCountry();
            if (c == null) {
                c = countryService.findByInternationalNumberPhone(rm.username);
            }
            Msisdn msisdn = new Msisdn(rm.username, true, c, p);
            user.getCredentialUserProperty().getUsername().setMsisdn(msisdn);
            try {
                userService.saveOrUpdate(user);
                // log.info("Creado user por login-sms: [" + user + "]");
            } catch (Exception ex) {
                log.error("Error al actualizar MSISDN para el usuario: [" + user + "]. " + ex, ex);
            }
        } else if (rm.credentials.error == 0) {
            Msisdn mm = user.getCredentialUserProperty().getUsername().getMsisdn(rm.username);
            String providerNameR = clientCache.getTemporalData(rm.username + "_SMS_P_" + code);
            if (providerNameR != null) {
                Provider p = providerService.findByName(providerNameR);
                mm.setProvider(p);
                mm.setCountry(p.getCountry());
                mm.setConfirmed(true);
                try {
                    userService.saveOrUpdate(user);
                    //  log.info("Actualizado user por login-sms: [" + user + "]");
                } catch (Exception ex) {
                    log.error("Error al actualizar MSISDN para el usuario: [" + user + "]. " + ex, ex);
                }
            }
        }
    }

}
