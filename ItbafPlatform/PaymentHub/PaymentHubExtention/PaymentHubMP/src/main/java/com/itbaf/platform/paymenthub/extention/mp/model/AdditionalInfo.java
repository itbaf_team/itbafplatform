package com.itbaf.platform.paymenthub.extention.mp.model;

import com.google.common.base.MoreObjects;
import java.util.ArrayList;

/**
 *
 * @author javier
 */
@lombok.Getter
@lombok.Setter
public class AdditionalInfo {

    private ArrayList<Item> items;
    private AdditionalInfoPayer payer;
    private Shipments shipments;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("items", items)
                .add("payer", payer)
                .add("shipments", shipments)
                .omitNullValues().toString();
    }

}
