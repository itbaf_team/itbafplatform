/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.spiralis.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_BILLING;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_RECEIVED_NAME;
import static com.itbaf.platform.commons.CommonFunction.NOTIFICATION_SUBSCRIPTION;
import com.itbaf.platform.paymenthub.extention.spiralis.resources.SpiralisResources;
import com.itbaf.platform.paymenthub.messaging.MessageType;
import com.itbaf.platform.paymenthub.messaging.PaymentHubMessage;
import com.itbaf.platform.messaging.rest.ResponseMessage;
import com.itbaf.platform.paymenthub.commons.bn.model.SpiralisNotification;
import com.itbaf.platform.paymenthub.model.Provider;
import com.itbaf.platform.paymenthub.model.SOP;
import com.itbaf.platform.paymenthub.model.billing.Tariff;
import com.itbaf.platform.paymenthub.model.subscription.Subscription;
import com.itbaf.platform.paymenthub.services.PreServiceProcessor;
import com.itbaf.platform.paymenthub.services.repository.PHProfilePropertiesService;
import java.math.BigDecimal;
import java.util.Map;
import java.util.TimeZone;
import org.apache.commons.lang3.Validate;


/**
 *
 * @author javier
 */
@lombok.extern.log4j.Log4j2
public class ServiceProcessorHandler extends PreServiceProcessor {

    private final Map<String, Provider> providerMap;
    private final SpiralisResources spiralisResources;
    private final PHProfilePropertiesService profilePropertiesService;

    @Inject
    public ServiceProcessorHandler(final Map<String, Provider> providerMap,
            final SpiralisResources spiralisResources,
            final PHProfilePropertiesService profilePropertiesService) {
        this.providerMap = Validate.notNull(providerMap, "A Map<String, Provider> class must be provided");
        this.spiralisResources = Validate.notNull(spiralisResources, "A SpiralisResources class must be provided");
        this.profilePropertiesService = Validate.notNull(profilePropertiesService, "A PHProfilePropertiesService class must be provided");
    }

    public void processNotification(String countryCode, SpiralisNotification notification) {

        if (countryCode == null || countryCode.length() != 2) {
            log.error("El Codigo de pais no es valido. countryCode: [" + countryCode + "]");
            return;
        }

        Provider p = providerMap.get(countryCode.toUpperCase());
        if (p == null) {
            log.error("No hay configurado un provider para countryCode: [" + countryCode + "]");
            return;
        }

        notification.setCountryCode(countryCode);
        String jsonResponse;
        try {
            jsonResponse = mapper.writeValueAsString(notification);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
            }
        } catch (Exception ex) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex1) {
            }
            try {
                jsonResponse = mapper.writeValueAsString(notification);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(NOTIFICATION_RECEIVED_NAME + p.getName(), jsonResponse);
                }
            } catch (Exception ex1) {
                log.error("No fue posible enviar al Rabbit: [" + notification + "]. " + ex, ex);
            }
        }
    }

    @Override
    public Boolean processMessage(String message) {
        SpiralisNotification notification;
        try {
            notification = mapper.readValue(message, SpiralisNotification.class);
            Thread.currentThread().setName(notification.thread);
        } catch (Exception ex) {
            log.fatal("No fue posible interpretar el JSON: [" + message + "]. " + ex, ex);
            return null;
        }

        Provider provider = providerMap.get(notification.getCountryCode().toUpperCase());
        if (provider == null) {
            log.error("No hay configurado un provider para CountryCode: [" + notification.getCountryCode() + "]");
            return null;
        }
        final String threadName = Thread.currentThread().getName() + "-" + provider.getName() + ".n";
        Thread.currentThread().setName(threadName);

        notification.setOriginalMsisdn(notification.getMsisdn());
        try {
            notification.setMsisdn(userAccountNormalizer(notification.getMsisdn(), provider));
        } catch (Exception ex) {
            log.error("No fue posible normalizar el MSISDN: [" + notification.getMsisdn() + "].  " + ex);
        }

        if ("0".equals(notification.getStatus())) {
            // log.info("No se procesara la notificacion.");
            return false;
        }
        PaymentHubMessage msg = null;
        switch (notification.getType()) {
            case "1":
                //  log.info("Notificacion de alta.");
                msg = processSubscriptionNotification(notification, provider, true);
                msg = processBillingNotification(notification, msg, provider);
                break;
            case "3":
                //  log.info("Notificacion de renovacion.");
                msg = processSubscriptionNotification(notification, provider, false);
                msg = processBillingNotification(notification, msg, provider);
                break;
            case "2":
                // log.info("Notificacion de baja.");
                msg = processUnsubscriptionNotification(notification, provider);
                break;
            case "4":
                //  log.info("Notificacion de Cobro por contenido. Ignorada");
                return false;
            case "7":
                msg = processSubscriptionNotification(notification, provider, MessageType.SUBSCRIPTION_PENDING, true);
                break;
            default:
                String aux = "Notificacion desconocida.";
                log.error(aux);
                return null;
        }
        return msg == null ? null : true;
    }

    private PaymentHubMessage processSubscriptionNotification(SpiralisNotification notification, Provider provider, boolean toServer) {
        return processSubscriptionNotification(notification, provider, MessageType.SUBSCRIPTION_ACTIVE, toServer);
    }

    private PaymentHubMessage processSubscriptionNotification(SpiralisNotification notification, Provider provider, MessageType messageType, boolean toServer) {
        SOP sop = sopService.findBySettings(provider.getName(), "serviceId", notification.getService());
        if (sop == null) {
            log.error("No existe un SOP configurado para el serviceId: [" + notification.getService() + "] - Provider: [" + provider.getName() + "]");
            return null;
        }

        Tariff t = sop.getMainTariff();

        PaymentHubMessage msg = new PaymentHubMessage();
        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setFromDate(locateDate("yyyyMMddHHmmss", notification.getCreated(), TimeZone.getTimeZone("America/Mexico_City")));
            msg.setMessageType(messageType);
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setTariffId(sop.getMainTariff().getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setTariffId(t.getId());
            msg.setUserAccount(notification.getMsisdn());

            String adTracking = null;
            try {
                adTracking = notification.getAdProvider();
                if (adTracking != null) {
                    log.info("adProvider recibido [" + adTracking + "]");

                    Map<String, Object> map = new ObjectMapper().readValue(adTracking, Map.class);

                    msg.getTrackingParams().putAll(map);
                }
            }
            catch (Exception e) {
                log.error("No se recibio adProvider: [" + notification.getAdProvider() + "]");
            }

            msg.setChannelIn(processChannel(notification.getChannelIn()));
            msg.setTransactionId(notification.getCreated() + "_" + notification.getService() + "_" + notification.getMsisdn());

            if (toServer) {
                try {
                    BigDecimal amount = new BigDecimal(0);
                    if (notification.getFee() != null) {
                        amount = new BigDecimal(notification.getFee());
                        amount = amount.divide(new BigDecimal(profilePropertiesService.getCommonProperty(provider.getId(), "spiralis.amount.divisor")));
                    }
                } catch (Exception ex) {
                    log.error("Error al enviar Mensaje SMS a Spiralis. " + ex, ex);
                }
                String jsonResponse = mapper.writeValueAsString(msg);
                if (jsonResponse != null) {
                    rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
                }
            }

            return msg;
        } catch (Exception ex) {
            log.error("Error al procesar SpiralisNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private PaymentHubMessage processBillingNotification(SpiralisNotification notification, PaymentHubMessage msg, Provider provider) {

        if (msg == null) {
            return null;
        }

        SOP sop = sopService.findBySettings(provider.getName(), "serviceId", notification.getService());
        if (sop == null) {
            log.error("No existe un SOP configurado para el serviceId: [" + notification.getService() + "] - Provider: [" + provider.getName() + "]");
            return null;
        }

        try {
            BigDecimal amount = new BigDecimal(0);
            if (notification.getFee() != null) {
                amount = new BigDecimal(notification.getFee());
                amount = amount.divide(new BigDecimal(profilePropertiesService.getCommonProperty(provider.getId(), "spiralis.amount.divisor")));
            }

            if (amount.compareTo(BigDecimal.ZERO) == 0) {
                // log.info("No se procesara la notificacion de cobro con valor CERO");
                return msg;
            } else {

                String message;
                if(notification.getType() == "3"){
                    log.info("Enviando mensaje de renovación para el ani "+notification.getMsisdn());
                    message = sop.getIntegrationSettings().get("renewMessage");
                }
                else{
                    log.info("Enviando mensaje de suscripción para el ani "+notification.getMsisdn());
                    message = sop.getIntegrationSettings().get("subscriptionMessage");
                }

                if (message == null) {
                    throw new Exception("No hay un SMS de bienvenida configurado para el SOP: [" + sop + "]");
                }
                spiralisResources.sendSMS(notification.getOriginalMsisdn(), sop, message);
            }

            Tariff t = sop.findTariffByNetAmount(amount);
            if (t == null) {
                //Verificamos si es una tarifa principal nueva
                t = updateMainTariff(sop, null, amount);
                if (t == null) {
                    //Creamos o actualizamos la tarifa hija
                    t = updateNetAmountChildrenTariff(sop, amount, notification.getService());
                }
            }

            if (t == null) {
                log.fatal("No hay un Tariff configurado para la notificacion: [" + notification + "] -  SOP: [" + sop + "]");
                return null;
            }

            msg.setNetAmount(t.getNetAmount());
            msg.setFullAmount(t.getFullAmount());
            msg.setNotificationType(PaymentHubMessage.NotificationType.BILLING);
            msg.setChargedDate(locateDate("yyyyMMddHHmmss", notification.getCreated(), TimeZone.getTimeZone("America/Mexico_City")));
            msg.setMessageType(MessageType.BILLING_TOTAL);
            msg.setTariffId(t.getId());
            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(sop.getProvider().getName() + NOTIFICATION_BILLING, jsonResponse);
            }
            return msg;
        } catch (Exception ex) {
            log.error("Error al procesar notificacion de cobro SpiralisNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    private PaymentHubMessage processUnsubscriptionNotification(SpiralisNotification notification, Provider provider) {
        SOP sop = sopService.findBySettings(provider.getName(), "serviceId", notification.getService());
        if (sop == null) {
            log.error("No existe un SOP configurado para el serviceId: [" + notification.getService() + "] - Provider: [" + provider.getName() + "]");
            return null;
        }
        PaymentHubMessage msg = new PaymentHubMessage();

        try {
            msg.setCodeCountry(provider.getCountry().getCode());
            msg.setCurrencyId(provider.getCountry().getCurrency());
            msg.setToDate(locateDate("yyyyMMddHHmmss", notification.getCreated(), TimeZone.getTimeZone("America/Mexico_City")));
            msg.setMessageType(MessageType.SUBSCRIPTION_REMOVED);
            msg.setTariffId(sop.getMainTariff().getId());
            msg.setNotificationType(PaymentHubMessage.NotificationType.SUBSCRIPTION);
            msg.setSopId(sop.getId());
            msg.setOrigin(PaymentHubMessage.OriginMessage.NOTIFICATION);
            msg.setChannelOut(processChannel(notification.getChannelOut()));
            msg.setUserAccount(notification.getMsisdn());

            String jsonResponse = mapper.writeValueAsString(msg);
            if (jsonResponse != null) {
                rabbitMQProducer.messageSender(provider.getName() + NOTIFICATION_SUBSCRIPTION, jsonResponse);
            }
            return msg;
        } catch (Exception ex) {
            log.error("Error al procesar SpiralisNotification: [" + notification + "]. " + ex, ex);
        }
        return null;
    }

    public ResponseMessage subscribe(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }

        if (subscription.getSubscriptionRegistry() != null && subscription.getSubscriptionRegistry().getId() != null
                && subscription.getSubscriptionRegistry().getUnsubscriptionDate() != null
                && (Subscription.Status.PENDING.equals(subscription.getStatus()) || Subscription.Status.ACTIVE.equals(subscription.getStatus()))) {
            rm.status = ResponseMessage.Status.OK_NS;
            rm.message = "La suscripcion ya tiene un estado valido";
        } else {
            /*  ResponseMessage aux = getSubscriptionStatus(subscription);
            switch (aux.status) {
                case ACTIVE:
                case PENDING:
                    subscription.setStatus(Subscription.Status.PENDING);
                    rm.status = ResponseMessage.Status.OK;
                    break;
                case REMOVED:*/
            subscription.setStatus(Subscription.Status.REMOVED);
            rm.status = ResponseMessage.Status.OK_NS;
            /*    break;
                case BLACKLIST:
                    subscription.setStatus(Subscription.Status.BLACKLIST);
                    rm.status = ResponseMessage.Status.BLACKLIST;
                    break;
                case ERROR:
                    rm.message = aux.message;
                    break;
            }*/
        }
        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    @Override
    public ResponseMessage getSubscriptionStatus(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        rm = spiralisResources.validateSubscriptionByMsisdn(subscription.getUserAccount(), subscription.getSop());
        switch (rm.status) {
            case ACTIVE:
                subscription.setStatus(Subscription.Status.ACTIVE);
                break;
            case PENDING:
                subscription.setStatus(Subscription.Status.PENDING);
                break;
            case REMOVED:
                subscription.setStatus(Subscription.Status.REMOVED);
                break;
            case BLACKLIST:
                subscription.setStatus(Subscription.Status.BLACKLIST);
                break;
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (JsonProcessingException ex) {
        }

        return rm;
    }

    public ResponseMessage sendPin(Subscription subscription) throws Exception {

        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);

        switch (aux.status) {
            case ACTIVE:
            case PENDING:
            case BLACKLIST:
                rm.status = aux.status;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case REMOVED:
                rm = spiralisResources.sendSubscriptionPin(subscription.getUserAccount(), subscription.getSop());
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage unsubscribe(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        if (subscription.getSubscriptionRegistry().getId() != null) {
            ResponseMessage.Status result = spiralisResources.removeSubscription(subscription);
            if (result != null) {
                rm.status = result;
            }
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    private String processChannel(String channel) {
        if (channel != null) {
            switch (channel) {
                case "1":
                    channel = "WEB";
                    break;
                case "2":
                    channel = "SMS";
                    break;
                case "7":
                    channel = "WAP";
                    break;
                case "99":
                    channel = "WEB-PIN";
                    break;
                case "128":
                    channel = "System";
                    break;
                default:
                    log.error("No hay un channel definido para el valor: [" + channel + "]");
            }
        }
        return channel;
    }

    public ResponseMessage validatePin(Subscription subscription, String pin) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        try {
            subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        } catch (Exception ex) {
            rm.status = ResponseMessage.Status.ERROR_403;
            rm.message = "Error al normalizar UserAccount. " + ex;
            return rm;
        }
        ResponseMessage aux = getSubscriptionStatus(subscription);
        switch (aux.status) {
            case ACTIVE:
            case PENDING:
            case BLACKLIST:
                rm.status = aux.status;
                break;
            case ERROR:
                rm.message = aux.message;
                break;
            case REMOVED:
                ResponseMessage result = spiralisResources.validatePin(subscription.getUserAccount(),
                        subscription.getSop(), pin);
                if (result != null) {
                    rm.status = result.status;
                    rm.message = result.message;
                }
                break;
        }

        rm.data = mapper.writeValueAsString(subscription);
        return rm;
    }

    public ResponseMessage userAccountNormalizer(Subscription subscription) throws Exception {
        ResponseMessage rm = new ResponseMessage();
        subscription.setUserAccount(userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));
        rm.data = mapper.writeValueAsString(subscription);
        rm.status = ResponseMessage.Status.OK;
        return rm;
    }

    public ResponseMessage sendSMS(Subscription subscription, String message) {
        ResponseMessage rm = new ResponseMessage();
        rm.status = ResponseMessage.Status.ERROR;
        try {
            subscription.setUserAccount(this.userAccountNormalizer(subscription.getUserAccount(), subscription.getSop().getProvider()));

            rm = spiralisResources.sendSMS(subscription.getUserAccount(),
                    subscription.getSop(), message);
        } catch (Exception ex) {
            rm.message = "Error al enviar SMS. " + ex;
            log.error(rm.message, ex);
        }

        try {
            rm.data = mapper.writeValueAsString(subscription);
        } catch (Exception ex) {
        }
        return rm;
    }

}
