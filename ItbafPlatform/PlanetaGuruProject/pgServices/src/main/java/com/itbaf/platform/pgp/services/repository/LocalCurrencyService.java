/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.pgp.services.repository;

import com.itbaf.platform.pgp.model.wallet.LocalCurrency;

/**
 *
 * @author javier
 */
public interface LocalCurrencyService {

    public void saveOrUpdate(LocalCurrency localCurrency) throws Exception;
}
