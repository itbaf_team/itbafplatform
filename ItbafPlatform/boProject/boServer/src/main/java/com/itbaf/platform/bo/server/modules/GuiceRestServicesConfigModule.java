package com.itbaf.platform.bo.server.modules;

import com.google.inject.Singleton;
import com.itbaf.platform.bo.server.filter.PermissionFilterFactory;
import com.itbaf.platform.bo.server.rest.HomeApiRestService;
import com.itbaf.platform.bo.server.rest.SubscriptionApiRestService;
import com.itbaf.platform.commons.filter.RequestFilter;
import com.itbaf.platform.pgp.commons.rest.ManagerRestServiceImpl;
import com.itbaf.platform.pgp.services.filter.ViewRequestFilter;
import com.itbaf.platform.pgp.services.rest.FileRestService;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import java.util.HashMap;
import com.itbaf.platform.bo.server.rest.UserApiRestService;

public class GuiceRestServicesConfigModule extends JerseyServletModule {

    @Override
    protected void configureServlets() {
        super.configureServlets();

        //Filters
        filter("/bo/server/rest/*").through(RequestFilter.class);
        filter("/bo/server/rest/api/*").through(ViewRequestFilter.class);
        
        //Servlets
        HashMap<String, String> options = new HashMap();
        options.put("com.sun.jersey.api.json.POJOMappingFeature", "true");
        options.put("com.sun.jersey.spi.container.ResourceFilters", PermissionFilterFactory.class.getCanonicalName());
        serve("/bo/server/file/*").with(GuiceContainer.class, options);
        serve("/bo/server/rest/*").with(GuiceContainer.class, options);
        
        //Binds
        bind(FileRestService.class).in(Singleton.class);
        bind(ManagerRestServiceImpl.class).in(Singleton.class);
        bind(HomeApiRestService.class).in(Singleton.class);
        bind(UserApiRestService.class).in(Singleton.class);
        bind(SubscriptionApiRestService.class).in(Singleton.class);
    }

}
