package com.itbaf.platform.paymenthub.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSmsSop is a Querydsl query type for SmsSop
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSmsSop extends EntityPathBase<SmsSop> {

    private static final long serialVersionUID = -1152267357L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSmsSop smsSop = new QSmsSop("smsSop");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final BooleanPath last_sended = createBoolean("last_sended");

    public final StringPath message = createString("message");

    protected QSOP sop;

    public QSmsSop(String variable) {
        this(SmsSop.class, forVariable(variable), INITS);
    }

    public QSmsSop(Path<? extends SmsSop> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSmsSop(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSmsSop(PathMetadata metadata, PathInits inits) {
        this(SmsSop.class, metadata, inits);
    }

    public QSmsSop(Class<? extends SmsSop> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.sop = inits.isInitialized("sop") ? new QSOP(forProperty("sop"), inits.get("sop")) : null;
    }

    public QSOP sop() {
        if (sop == null) {
            sop = new QSOP(forProperty("sop"));
        }
        return sop;
    }

}

