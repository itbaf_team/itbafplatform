/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itbaf.platform.paymenthub.extention.smsc.model;

import com.google.common.base.MoreObjects;
import java.util.List;

/**
 *
 * @author JF
 */
public class ActivosProductoResponse {

    public Integer code;
    public String description;
    public Integer totalActivos;
    public String idProducto;
    public List<ValueResponse> listado;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("code", code)
                .add("description", description)
                .add("totalActivos", totalActivos)
                .add("idProducto", idProducto)
                .add("listado", listado)
                .omitNullValues().toString();
    }
}
